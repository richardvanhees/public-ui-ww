import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Provider, connect } from 'react-redux';
import { Route, Redirect } from 'react-router';
import { ConnectedRouter, push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { changeBreakpoint, breakpoint } from './../../modules/browser';
import WealthWizards from '../../modules/wealthwizards';

import NoHashContainer from './components/NoHash';
import RegisterContainer from './components/Register/RegisterContainer';
import LoginContainer from './components/Login/LoginContainer';
import QuestionsContainer from './components/Questions/index';
import GenerateContainer from './components/GenerateContainer';
import PrioritiesContainer from './components/Priorities';
import TipsContainer from './components/Tips/TipsContainer';
import DashboardNewContainer from './components/DashboardNew';

class App extends PureComponent {
  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentBreakpoint: props.browser.breakpoint,
      currentLocation: props.history.location.pathname,
    };
  }

  checkBreakpoint = () => {
    const currentBreakpoint = breakpoint();

    if (this.state.currentBreakpoint !== currentBreakpoint) {
      this.setState({ currentBreakpoint });
      this.props.setBreakpoint(currentBreakpoint);
    }
  };

  userIsTabbing = e =>
    e.keyCode === 9 && document.body.classList.add('user-is-tabbing');
  userIsNotTabbing = () => document.body.classList.remove('user-is-tabbing');

  componentDidMount() {
    this.checkBreakpoint();
    window.addEventListener('resize', this.checkBreakpoint);
    window.addEventListener('keydown', this.userIsTabbing);
    window.addEventListener('mousedown', this.userIsNotTabbing);
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <div style={{ height: '100%' }}>
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/`}
              component={NoHashContainer}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/`}
              render={props => (
                <Redirect
                  to={`${WealthWizards.CONTEXT_ROUTE}/${
                    props.match.params.employer
                  }/questionnaire/1`}
                />
              )}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/login`}
              component={LoginContainer}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
              }/:employer/questionnaire/:activeQuestion`}
              component={QuestionsContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/register`}
              component={RegisterContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/priorities`}
              component={PrioritiesContainer}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
              }/:employer/generate-priorities`}
              component={GenerateContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/tips`}
              component={TipsContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/:employer/dashboard`}
              component={DashboardNewContainer}
            />
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

const mapStateToProps = state => ({
  state,
  browser: state.browser,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      push,
      setBreakpoint: changeBreakpoint,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
