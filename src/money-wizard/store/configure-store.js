import { call } from 'redux-saga/effects';
import loginReducer, { loginSaga, registerSaga, getEmployerSaga } from '../modules/login';
import generateReducer, { generateSaga, generateMoneyScoreSaga } from '../modules/generate';
import browserReducer from '../../../modules/browser';
import questionnaireReducer, { questionnaireSaga } from '../modules/questionnaire';
import tipsReducer, { tipsSaga, optInSaga } from '../modules/tips';
import prioritiesReducer from '../modules/priorities';
import patReducer, { sendPatMailSaga } from '../modules/pat';
import configureStoreBoilerplate from '../../../modules/boilerplate/configure-store';

export const configureStore = (history, initialState = {}) =>
  configureStoreBoilerplate(
    {
      browser: browserReducer,
      login: loginReducer,
      questionnaire: questionnaireReducer,
      generate: generateReducer,
      tips: tipsReducer,
      priorities: prioritiesReducer,
      pat: patReducer,
    },
    [
      call(loginSaga),
      call(questionnaireSaga),
      call(generateSaga),
      call(generateMoneyScoreSaga),
      call(tipsSaga),
      call(optInSaga),
      call(registerSaga),
      call(getEmployerSaga),
      call(sendPatMailSaga),
    ],
    history,
    initialState
  );
