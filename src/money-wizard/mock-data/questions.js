export const questions = [
  {
    id: 0,
    hasImages: false,
    question: 'What age are you?',
    answers: [
      {
        id: 0,
        answer: 'Under 26 years old',
      },
      {
        id: 1,
        answer: '26 - 35 years old',
      },
      {
        id: 2,
        answer: '36-50 years old',
      },
      {
        id: 3,
        answer: 'Over 50 years old',
      },
    ],
  },
  {
    id: 1,
    hasImages: true,
    question: "What's your family situation?",
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: "I'm single",
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: "I'm single with children",
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: 'I have a partner',
      },
      {
        id: 3,
        image: 'http://www.christianitytoday.com/images/71940.jpg',
        answer: 'I have got a partner and children',
      },
    ],
  },
  {
    id: 2,
    hasImages: true,
    question: 'What is your employment status?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: 'I work part time on a contract basis',
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: 'I work full time on a contract basis',
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: 'I work part time in a permanent role',
      },
      {
        id: 3,
        image: 'http://www.christianitytoday.com/images/71940.jpg',
        answer: 'I work full time in a permanent role',
      },
    ],
  },
  {
    id: 3,
    hasImages: true,
    question: 'How would you describe your spending habits each month?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: 'I spend more than I earn each month',
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: 'I spend everything I earn, but nothing more',
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: 'I always have some money left over each month',
      },
    ],
  },
  {
    id: 4,
    hasImages: true,
    question: 'How would you handle a large unexpected bill?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: "I'd panic, I have no savings whatsoever",
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: "I might struggle, I have less than 3 months' wages saved up",
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: "I'd probably cope, I have a good emergency fund",
      },
    ],
  },
  {
    id: 5,
    hasImages: true,
    question: 'What is your home ownership situation?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: 'I own my own home',
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: "I'd like to buy my own home",
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: "I'm not interested in buying a home",
      },
    ],
  },
  {
    id: 6,
    hasImages: true,
    question: "What's your debt situation?",
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: "I don't have any debts",
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: 'I repay my debts on time',
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: 'I sometimes have difficulty paying off debts',
      },
      {
        id: 3,
        image: 'http://www.christianitytoday.com/images/71940.jpg',
        answer: "I'm in significant debt",
      },
    ],
  },
  {
    id: 7,
    hasImages: true,
    question: 'How would you describe your pension planning?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: "I've always paid into a company pension",
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer:
          "I've had some periods where I haven't contributed to my pension",
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: "I'm not interested in saving into a pension",
      },
    ],
  },
  {
    id: 8,
    hasImages: true,
    question:
      'Which of these best describes your position on saving for the future?',
    answers: [
      {
        id: 0,
        image: 'https://www.w3schools.com/css/img_fjords.jpg',
        answer: 'I save regularly and am focused on building my savings up',
      },
      {
        id: 1,
        image:
          'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
        answer: "I want to save but I haven't done so recently",
      },
      {
        id: 2,
        image: 'https://www.w3schools.com/w3css/img_lights.jpg',
        answer: "I haven't been able to save",
      },
    ],
  },
];
