/* eslint-disable */
export default [
  {
    id: 0,
    name: 'Building up savings',
    image: 'q8a0',
    short_description:
      'Putting aside some money every month to help you pay for unexpected costs or treats.',
    long_description:
      'Saving is about putting money aside either for a specific purpose like a holiday or to have money available for unexpected costs such as your car breaking down.  Savings are usually held in cash based products such as an easy access savings account with a bank or building society. This means you can access your money quickly if needed.',
    list: [],
    link_header: 'Help to build up savings',
    links: [
      {
        label: 'Save or invest?',
        url:
          'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
      },
      {
        label: 'Savings calculator',
        url:
          'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
      },
    ],
  },
  {
    id: 1,
    name: 'Investing money for the long term',
    image: 'q4a2',
    short_description:
      'Once you’ve built up some savings, you can look for better returns on your money to help it grow over time.',
    long_description:
      'Investing is all about looking for ways to make your savings grow more over a long period of time – usually 5 years or more. This might include investing in stocks and shares, property or investment funds. There are risks involved with investing so getting advice is recommended.',
    list: [],
    link_header: 'Help to invest money for the long term',
    links: [
      {
        label: 'Guide to investing',
        url:
          'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
      },
    ],
  },
  {
    id: 2,
    name: 'Protecting from the unexpected',
    image: 'q4a0',
    short_description:
      'Making sure you and your family are financially protected in the event of illness, unemployment or death.',
    long_description:
      'Protecting yourself from the unexpected is important. Even though it’s not pleasant to think about, making the right decisions now could make events such as redundancy, illness or death less traumatic for you and your family. First, you should understand the level of cover offered by your employer, you may already be entitled to some form of protection such as sick pay, healthcare or life cover.\n\nHave you written a will? It’s easy to do and it ensures that any inheritance you want to leave goes to the people you’d like to benefit.',
    list: [],
    link_header: 'Help to protect from the unexpected',
    links: [
      {
        label: 'Making a will',
        url:
          'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
      },
      {
        label: 'Guide to financial protection',
        url:
          'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
      },
    ],
  },
  {
    id: 3,
    name: 'Saving into pensions',
    image: 'q7a0',
    short_description:
      'Regularly putting aside some money into a pension to give you an income when you retire.',
    long_description:
      'Saving into a pension is about regularly saving a part of your salary so that you can get an income when you retire.  You will get tax relief on what you pay into your pension, and your employer will normally pay in too. This ‘free money’ is worth making the most of.  Starting early is always a good idea when it comes to saving for retirement.',
    list: [],
    link_header: 'Help to save into pensions',
    links: [
      {
        label: 'Pension Wizard',
        url: 'https://apps.wealthwizards.io/pension-wizard/',
      },
      {
        label: 'Your pension. Your future.',
        url: 'https://apps.wealthwizards.io/pension-education',
      },
    ],
  },
  {
    id: 4,
    name: 'Preparing for retirement',
    image: 'q7a0',
    short_description:
      'When you’re close to retirement, working out the best way of providing an income when you retire.',
    long_description:
      'As you approach retirement, there are many ways of getting your finances sorted ready for your new way of life. Pensions are now more flexible than you might think so understanding your options is vital. Financial advice can help you make the most suitable decision for you and your finances. Common questions are:',
    list: [
      'Do I have enough saved?',
      'What options do I have?',
      'How long will my money last?',
      'Can I continue to work and take my pension?',
    ],
    link_header: 'Help to prepare for retirement',
    links: [
      {
        label: 'Pension Wizard',
        url: 'https://apps.wealthwizards.io/pension-wizard/',
      },
      {
        label: 'Your pension. Your future.',
        url: 'https://apps.wealthwizards.io/pension-education',
      },
    ],
  },
  {
    id: 5,
    name: 'Managing regular spending',
    image: 'q3a2',
    short_description:
      'Keeping a track of your spending and making sure this is less than you earn.',
    long_description:
      'Some people have difficulty managing their day to day spending. Feeling in control of what you spend might free up some money to spend on other opportunities. The best place to start is by using a budgeting tool to break down your spending to look at exactly where your money is going and where you might be able to make savings.',
    list: [],
    link_header: 'Help to budget',
    links: [
      {
        label: 'Budgeting tool',
        url:
          'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
      },
    ],
  },
  {
    id: 6,
    name: 'Reducing debt',
    image: 'q6a3',
    short_description:
      'Recognising your debts so that you can pay them off over time.',
    long_description:
      'Most people have some form of debt. Having lots of high interest or unaffordable debt can be worrying. Debts can get out of control if they’re not managed properly and getting behind on your repayments could damage your credit rating. This could affect your ability to borrow in the future. If you’re in this situation, you should seek help.',
    list: [],
    link_header: 'Help to reduce debt',
    links: [
      {
        label: 'Debts and borrowing',
        url:
          'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
      },
      {
        label: 'Debt advice',
        url: 'https://www.stepchange.org/Debtremedy.aspx',
      },
    ],
  },
  {
    id: 7,
    name: 'Buying a home',
    image: 'q5a1',
    short_description:
      'Getting your mind and money ready for buying a new home.',
    long_description:
      "Buying a home can be one of the most exciting times in a person's life, but getting onto the property ladder is not always easy.  Equally, selling a home and finding a new property has its own challenges.  There are many steps involved, so careful research and planning is needed.  Common questions are:",
    list: [
      'How much deposit do I need?',
      'Which mortgage is best?',
      'What’s the process?',
      'How much will it all cost?',
      'How long will it take?',
      'What could go wrong?',
    ],
    link_header: 'Help to buy a home',
    links: [
      {
        label: 'Buying a home',
        url:
          'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
      },
    ],
  },
];
