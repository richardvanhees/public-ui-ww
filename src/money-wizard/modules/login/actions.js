export const LOGIN_SET_LOGGED_IN = 'login/LOGIN_SET_LOGGED_IN';
export const LOGIN_REQUEST = 'login/LOGIN_REQUEST';
export const LOGIN_PRIVACY_TOGGLE = 'login/LOGIN_PRIVACY_TOGGLE';
export const LOGIN_SET_EMAIL = 'login/LOGIN_SET_EMAIL';
export const LOGIN_RESET_ERROR = 'login/LOGIN_RESET_ERROR';
export const LOGIN_SET_ERROR = 'login/LOGIN_SET_ERROR';
export const LOGIN_SET_EMPLOYER_HASH = 'login/LOGIN_SET_EMPLOYER_HASH';
export const LOGIN_SET_DEMO_USER = 'login/LOGIN_SET_DEMO_USER';
export const LOGIN_SET_FIRST_USE = 'login/LOGIN_SET_FIRST_USE';
export const LOGIN_SET_USER_RETURNED = 'login/LOGIN_SET_USER_RETURNED';
export const LOGIN_SET_USER_ANSWERS = 'login/LOGIN_SET_USER_ANSWERS';

export const REGISTER_USER = 'login/REGISTER_USER';
export const REGISTER_USER_START = 'login/REGISTER_USER_START';
export const REGISTER_USER_SUCCESS = 'login/REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAIL = 'login/REGISTER_USER_FAIL';

export const GET_EMPLOYER_START = 'login/GET_EMPLOYER_START';
export const GET_EMPLOYER = 'login/GET_EMPLOYER';
export const GET_EMPLOYER_SUCCESS = 'login/GET_EMPLOYER_SUCCESS';
export const GET_EMPLOYER_FAIL = 'login/GET_EMPLOYER_FAIL';

export const SET_IS_LOADING = 'login/SET_IS_LOADING';

export const setLoggedIn = () => ({
  type: LOGIN_SET_LOGGED_IN,
});
export const setIsLoading = isLoading => ({
  type: SET_IS_LOADING,
  isLoading,
});

export const loginRequest = password => ({
  type: LOGIN_REQUEST,
  password,
});

export const togglePrivacyPolicy = () => ({
  type: LOGIN_PRIVACY_TOGGLE,
});

export const setEmail = email => ({
  type: LOGIN_SET_EMAIL,
  email,
});

export const setFirstUse = firstUse => ({
  type: LOGIN_SET_FIRST_USE,
  firstUse,
});

export const setEmployerHash = employerHash => ({
  type: LOGIN_SET_EMPLOYER_HASH,
  employerHash,
});

export const resetError = () => ({
  type: LOGIN_RESET_ERROR,
  showError: false,
  errorMessage: '',
});

export const setError = data => ({
  type: LOGIN_SET_ERROR,
  showError: true,
  errorMessage: data,
});

export const setDemoUser = () => ({
  type: LOGIN_SET_DEMO_USER,
});

export const setUserReturned = () => ({
  type: LOGIN_SET_USER_RETURNED,
});

export const registerUser = () => ({
  type: REGISTER_USER,
});

export const registerUserStart = () => ({
  type: REGISTER_USER_START,
});

export const registerUserSuccess = () => ({
  type: REGISTER_USER_SUCCESS,
});

export const registerUserFail = () => ({
  type: REGISTER_USER_FAIL,
});

export const getEmployerStart = () => ({
  type: GET_EMPLOYER_START,
});

export const getEmployer = employerHash => ({
  type: GET_EMPLOYER,
  employerHash,
});

export const getEmployerSuccess = employerData => ({
  type: GET_EMPLOYER_SUCCESS,
  employerData,
});

export const getEmployerFail = () => ({
  type: GET_EMPLOYER_FAIL,
});
