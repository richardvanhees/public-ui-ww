import test from 'tape';
import reducer from './reducer.js';
import * as loginTypes from './actions';

test('LoginReducer', t => {
  let result;

  result = reducer(
    {},
    { type: loginTypes.LOGIN_REQUEST, password: 'fakePassword' }
  );
  t.deepEqual(
    result,
    { isLoading: true, password: 'fakePassword' },
    'LOGIN_REQUEST'
  );

  result = reducer({}, { type: loginTypes.LOGIN_PRIVACY_TOGGLE });
  t.deepEqual(result, { privacyPolicyAgreed: true }, 'LOGIN_PRIVACY_TOGGLE');

  result = reducer(
    {},
    { type: loginTypes.LOGIN_SET_EMAIL, email: 'test@test.com' }
  );
  t.deepEqual(result, { email: 'test@test.com' }, 'LOGIN_SET_EMAIL');

  result = reducer(
    {},
    { type: loginTypes.LOGIN_SET_EMPLOYER_HASH, employerHash: 'testHash' }
  );
  t.deepEqual(result, { employerHash: 'testHash' }, 'LOGIN_SET_EMPLOYER_HASH');

  result = reducer(
    {},
    { type: loginTypes.LOGIN_SET_ERROR, errorMessage: 'testError' }
  );
  t.deepEqual(
    result,
    { showError: true, errorMessage: 'testError' },
    'LOGIN_SET_ERROR'
  );

  result = reducer({}, { type: loginTypes.LOGIN_RESET_ERROR });
  t.deepEqual(
    result,
    { showError: false, errorMessage: '' },
    'LOGIN_RESET_ERROR'
  );

  result = reducer(
    { email: 'demo@wealthwizards.com' },
    { type: loginTypes.LOGIN_SET_DEMO_USER }
  );
  t.deepEqual(
    result,
    { demoUser: true, email: 'demo@wealthwizards.com' },
    'LOGIN_SET_DEMO_USER'
  );

  result = reducer({}, { type: loginTypes.LOGIN_SET_USER_RETURNED });
  t.deepEqual(result, { firstUse: false }, 'LOGIN_SET_USER_RETURNED ');

  result = reducer({}, { type: loginTypes.REGISTER_USER_START });
  t.deepEqual(result, { isLoading: true }, 'REGISTER_USER_START ');

  result = reducer({}, { type: loginTypes.REGISTER_USER_SUCCESS });
  t.deepEqual(result, { isLoading: false }, 'REGISTER_USER_SUCCESS ');

  result = reducer({}, { type: loginTypes.REGISTER_USER_FAIL });
  t.deepEqual(result, { isLoading: false }, 'REGISTER_USER_FAIL ');

  result = reducer({}, { type: loginTypes.GET_EMPLOYER_START });
  t.deepEqual(result, { isLoading: true }, 'GET_EMPLOYER_START ');

  result = reducer(
    {},
    {
      type: loginTypes.GET_EMPLOYER_SUCCESS,
      employerData: {
        employerHash: 'testEmployerHash',
        employer: 'testName',
        mailTemplate: 'testTemplate',
        bespokeLinks: 'testBespokeLinks',
        questionsVersion: 2,
        prioritiesVersion: 3,
        popupType: 1,
        canLogin: true,
        canContactPAT: true,
        disablePopup: true,
      },
    }
  );
  t.deepEqual(
    result,
    {
      bespokeLinks: 'testBespokeLinks',
      canContactPAT: true,
      canLogin: true,
      disableEmail: false,
      disablePopup: true,
      employerHash: 'testEmployerHash',
      employerName: 'testName',
      isLoading: false,
      mailTemplate: 'testTemplate',
      popupType: 1,
      prioritiesVersion: 3,
      questionsVersion: 2,
      showMW2BetaLink: false,
    },
    'GET_EMPLOYER_SUCCESS '
  );

  result = reducer({}, { type: loginTypes.GET_EMPLOYER_FAIL });
  t.deepEqual(result, { isLoading: false }, 'GET_EMPLOYER_FAIL ');

  t.end();
});
