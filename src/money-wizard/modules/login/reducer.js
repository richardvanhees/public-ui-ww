import {
  LOGIN_REQUEST,
  LOGIN_PRIVACY_TOGGLE,
  LOGIN_SET_EMAIL,
  LOGIN_RESET_ERROR,
  LOGIN_SET_ERROR,
  LOGIN_SET_EMPLOYER_HASH,
  LOGIN_SET_LOGGED_IN,
  LOGIN_SET_DEMO_USER,
  LOGIN_SET_USER_RETURNED,
  REGISTER_USER_START,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL,
  LOGIN_SET_FIRST_USE,
  GET_EMPLOYER_START,
  GET_EMPLOYER_SUCCESS,
  GET_EMPLOYER_FAIL,
  SET_IS_LOADING,
} from './actions';

const setLoggedIn = state => ({
  ...state,
  loggedIn: true,
});

const loginRequest = (state, { password }) => ({
  ...state,
  isLoading: true,
  password,
});

const togglePrivacy = state => ({
  ...state,
  privacyPolicyAgreed: !state.privacyPolicyAgreed,
});

const setEmail = (state, { email }) => ({
  ...state,
  email,
});

const setFirstUse = (state, { firstUse }) => ({
  ...state,
  firstUse,
});

const setDemoUser = state => ({
  ...state,
  demoUser: state.email === 'demo@wealthwizards.com',
});

const setEmployerHash = (state, { employerHash }) => ({
  ...state,
  employerHash,
});

const setUserReturned = state => ({
  ...state,
  firstUse: false,
});

const setError = (state, { errorMessage }) => ({
  ...state,
  showError: true,
  errorMessage,
});

const resetError = state => ({
  ...state,
  showError: false,
  errorMessage: '',
});

const registerUserStart = state => ({
  ...state,
  isLoading: true,
});

const registerUserSuccess = state => ({
  ...state,
  isLoading: false,
});

const registerUserFail = state => ({
  ...state,
  isLoading: false,
});

const getEmployerStart = state => ({
  ...state,
  isLoading: true,
});

const setIsLoading = (state, { isLoading }) => ({
  ...state,
  isLoading,
});

const getEmployerSuccess = (state, { employerData }) => ({
  ...state,
  isLoading: false,
  employerName: employerData.employer,
  bespokeLinks: employerData.bespokeLinks || [],
  mailTemplate: employerData.mailTemplate,
  questionsVersion: employerData.questionsVersion || 1,
  prioritiesVersion: employerData.prioritiesVersion || 2,
  popupType: employerData.popupType || 1,
  employerHash: employerData.employerHash,
  canContactPAT: employerData.canContactPAT,
  canLogin: employerData.canLogin,
  disablePopup: employerData.disablePopup || false,
  disableEmail: employerData.disableEmail || false,
  showMW2BetaLink: employerData.showMW2BetaLink || false,
});

const getEmployerFail = state => ({
  ...state,
  isLoading: false,
});

const initialState = {
  loggedIn: false,
  isLoading: false,
  privacyPolicyAgreed: false,
  showError: false,
  errorMessage: '',
  email: '',
  employerHash: '',
  employerName: '',
  bespokeLinks: [],
  mailTemplate: '',
  questionsVersion: 1,
  prioritiesVersion: 1,
  popupType: 1,
  demoUser: false,
  firstUse: true,
  canContactPAT: true,
  canLogin: true,
  showMW2BetaLink: false,
};

const reducer = {
  [LOGIN_SET_LOGGED_IN]: setLoggedIn,
  [LOGIN_REQUEST]: loginRequest,
  [LOGIN_PRIVACY_TOGGLE]: togglePrivacy,
  [LOGIN_SET_EMAIL]: setEmail,
  [LOGIN_SET_FIRST_USE]: setFirstUse,
  [LOGIN_RESET_ERROR]: resetError,
  [LOGIN_SET_ERROR]: setError,
  [LOGIN_SET_EMPLOYER_HASH]: setEmployerHash,
  [LOGIN_SET_DEMO_USER]: setDemoUser,
  [LOGIN_SET_USER_RETURNED]: setUserReturned,
  [REGISTER_USER_START]: registerUserStart,
  [REGISTER_USER_SUCCESS]: registerUserSuccess,
  [REGISTER_USER_FAIL]: registerUserFail,
  [GET_EMPLOYER_START]: getEmployerStart,
  [GET_EMPLOYER_SUCCESS]: getEmployerSuccess,
  [GET_EMPLOYER_FAIL]: getEmployerFail,
  [SET_IS_LOADING]: setIsLoading,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
