import { call, put, select, takeLatest } from 'redux-saga/effects';
import R from 'ramda';
import { get, post } from '../../../../../modules/utils/AxiosWrapper';
import {
  LOGIN_REQUEST,
  setError,
  setLoggedIn,
  setDemoUser,
  setFirstUse,
  setIsLoading,
} from '../actions';
import {
  getQuestions,
  mapUserAnswers,
} from '../../../modules/questionnaire/actions';
import {
  mapUserOrder,
  generateScoreSuggestion,
} from '../../../modules/generate/actions';
import { getPrioritiesSuccess } from '../../priorities/actions';
import prioritiesSelector from '../../priorities/selector';
import loginSelector from '../selector';

export const getFirstUse = state => loginSelector(state).firstUse;
export const getTheUserEmail = state => loginSelector(state).email;
export const getTheEmployerHash = state => loginSelector(state).employerHash;
export const getTheQuestionsVersion = state =>
  loginSelector(state).questionsVersion;
export const checkEmployerHash = theHash =>
  get(`/check-employer-hash/${theHash}`);
export const callToLoginUser = data => post({ route: '/login', data });

export const getEmployerPrioritiesVersion = state =>
  prioritiesSelector(state).employerPrioritiesVersion;
export const callToGetPriorities = version => get(`/priorities/${version}`);

export function* loginUser({ password }) {
  try {
    const questionsVersion = yield select(getTheQuestionsVersion);
    const userEmail = yield select(getTheUserEmail);

    const isALoginAttempt = password !== undefined;

    if (isALoginAttempt) {
      try {
        const { data: userData } = yield call(callToLoginUser, {
          email: userEmail,
          password,
        });
        yield put(
          mapUserAnswers({ answers: R.last(userData.questions).questions })
        );
        yield put(
          mapUserOrder({
            userOrder: R.last(userData.userFinancialPriorities).priorities,
          })
        );
        yield put(setLoggedIn());
        yield put(setFirstUse(false));
      } catch (e) {
        yield put(
          setError(
            'We were unable to login you back in, please try again later.'
          )
        );
        return false;
      }
    }

    yield put(getQuestions(questionsVersion || 1)); // Fallback to 1 for older, not updated employer data
    yield put(setDemoUser());

    const version = yield select(getEmployerPrioritiesVersion);
    const { data } = yield call(callToGetPriorities, version);
    yield put(getPrioritiesSuccess(data[0]));

    if (!isALoginAttempt) {
      yield put(setIsLoading(false));
    } else {
      yield put(generateScoreSuggestion());
    }

    return true;
  } catch (error) {
    yield put(
      setError(
        'We were unable to process your request, please try again later.'
      )
    );
    return false;
  }
}

export default function* loginSaga() {
  yield takeLatest(LOGIN_REQUEST, loginUser);
}
