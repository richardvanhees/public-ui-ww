import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import getEmployerSaga, {
  getEmployer,
  checkEmployerHash,
  getEmployerName,
} from './get-employer';
import * as actions from '../actions';
import { replace } from 'react-router-redux';

test('getEmployerSaga', t => {
  let target;
  let testData = {
    email: 'foo@bar.com',
    employerHash: '1a2b3c4d',
    employer: {
      questionsVersion: 1,
    },
  };

  // all good
  const testPriorityData = { data: [{ priorities: 'testData', version: 1 }] };
  target = testSaga(getEmployer, { employerHash: testData.employerHash })
    .next()
    .select(getEmployerName)
    .next(null)
    .put(actions.getEmployerStart())
    .next()
    .call(checkEmployerHash, testData.employerHash)
    .next({ data: [testData.employer] })
    .put(actions.getEmployerSuccess(testData.employer))
    .next()
    .put(actions.loginRequest())
    .next();
  t.equals(!!target.isDone(), true, 'Get employer success');

  // email already used
  target = testSaga(getEmployer, { employerHash: testData.employerHash })
    .next()
    .select(getEmployerName)
    .next(null)
    .put(actions.getEmployerStart())
    .next()
    .call(checkEmployerHash, testData.employerHash)
    .next({ data: [] })
    .put(actions.getEmployerFail())
    .next()
    .put(replace('undefined/'))
    .next();
  t.equals(!!target.isDone(), true, 'Get employer fail');

  // already loaded employer data
  target = testSaga(getEmployer, { employerHash: testData.employerHash })
    .next()
    .select(getEmployerName)
    .next('dummy employer')
    .put(actions.loginRequest())
    .next();
  t.equals(!!target.isDone(), true, 'dont get employer when already got it');

  t.end();
});
