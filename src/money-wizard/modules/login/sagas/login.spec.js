import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import loginSaga, {
  loginUser,
  getTheUserEmail,
  getTheEmployerHash,
  getFirstUse,
  callToLoginUser,
  getEmployerPrioritiesVersion,
  callToGetPriorities,
  getTheQuestionsVersion,
} from './login';
import * as actions from '../actions';
import { questionnaireTypes } from '../../questionnaire/types';
import { mapUserAnswers } from '../../questionnaire/index';
import { mapUserOrder, generateScoreSuggestion } from '../../generate/actions';
import { setLoggedIn } from '../actions';
import { prioritiesTypes } from '../../priorities/types';
import { push } from 'react-router-redux';

test('LoginSaga', t => {
  let target;
  let testData = {
    email: 'foo@bar.com',
    employerHash: '1a2b3c4d',
    employer: {
      questionsVersion: 2,
    },
    password: 'test1234',
  };

  // all good
  const testPriorityData = { data: [{ priorities: 'testData', version: 1 }] };
  target = testSaga(loginUser, {})
    .next()
    .select(getTheQuestionsVersion)
    .next(testData.employer.questionsVersion)
    .select(getTheUserEmail)
    .next(testData.email)
    .put({
      type: questionnaireTypes.GET_QUESTIONS,
      version: testData.employer.questionsVersion,
    })
    .next()
    .put({ type: actions.LOGIN_SET_DEMO_USER })
    .next()
    .select(getEmployerPrioritiesVersion)
    .next(2)
    .call(callToGetPriorities, 2)
    .next(testPriorityData)
    .put({
      type: prioritiesTypes.GET_PRIORITIES_SUCCESS,
      ...testPriorityData.data[0],
    })
    .next()
    .put(actions.setIsLoading(false))
    .next();
  t.equals(!!target.isDone(), true, 'Start without login success');

  // Login existing user
  const returnData = {
    data: {
      questions: [{ questions: {} }],
      userFinancialPriorities: [{ priorities: [] }],
    },
  };

  target = testSaga(loginUser, { password: testData.password })
    .next()
    .select(getTheQuestionsVersion)
    .next(testData.employer.questionsVersion)
    .select(getTheUserEmail)
    .next(testData.email)
    .call(callToLoginUser, {
      email: testData.email,
      password: testData.password,
    })
    .next(returnData)
    .put(mapUserAnswers({ answers: returnData.data.questions[0].questions }))
    .next()
    .put(
      mapUserOrder({
        userOrder: returnData.data.userFinancialPriorities[0].priorities,
      })
    )
    .next()
    .put(setLoggedIn())
    .next()
    .put(actions.setFirstUse(false))
    .next()
    .put({
      type: questionnaireTypes.GET_QUESTIONS,
      version: testData.employer.questionsVersion,
    })
    .next()
    .put({ type: actions.LOGIN_SET_DEMO_USER })
    .next()
    .select(getEmployerPrioritiesVersion)
    .next(2)
    .call(callToGetPriorities, 2)
    .next(testPriorityData)
    .put({
      type: prioritiesTypes.GET_PRIORITIES_SUCCESS,
      ...testPriorityData.data[0],
    })
    .next()
    .put(generateScoreSuggestion())
    .next();
  t.equals(!!target.isDone(), true, 'loginUser success');

  t.end();
});
