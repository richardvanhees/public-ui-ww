import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import registerSaga, {
  registerUser,
  getTheUserEmail,
  getTheEmployerHash,
  checkEmployerHash,
  checkEmailExists,
  getDemoUser,
} from './register-user';
import * as actions from '../actions';
import { replace } from 'react-router-redux';

test('RegisterSaga', t => {
  let target;
  let testData = {
    email: 'foo@bar.com',
    employerHash: '1a2b3c4d',
    employer: {
      questionsVersion: 1,
    },
  };

  // all good
  const testPriorityData = { data: [{ priorities: 'testData', version: 1 }] };
  target = testSaga(registerUser, {})
    .next()
    .put({ type: actions.LOGIN_SET_DEMO_USER })
    .next()
    .select(getDemoUser)
    .next(false)
    .select(getTheEmployerHash)
    .next(testData.employerHash)
    .select(getTheUserEmail)
    .next(testData.email)
    .call(checkEmployerHash, testData.employerHash)
    .next({ data: [testData.employer] })
    .call(checkEmailExists, testData.email)
    .next({ data: [] })
    .put({ type: actions.LOGIN_SET_DEMO_USER })
    .next()
    .put(replace('undefined/1a2b3c4d/tips'))
    .next();
  t.equals(!!target.isDone(), true, 'Start without login success');

  // email already used
  target = testSaga(registerUser, {})
    .next()
    .put({ type: actions.LOGIN_SET_DEMO_USER })
    .next()
    .select(getDemoUser)
    .next(false)
    .select(getTheEmployerHash)
    .next(testData.employerHash)
    .select(getTheUserEmail)
    .next(testData.email)
    .call(checkEmployerHash, testData.employerHash)
    .next({ data: [testData.employer] })
    .call(checkEmailExists, testData.email)
    .next({ data: [testData.email] })
    .put({
      type: actions.LOGIN_SET_ERROR,
      showError: true,
      errorMessage:
        'This email was already used, you should have received an email with tips on your financial priorities',
    })
    .next()
    .next();
  t.equals(!!target.isDone(), true, 'loginUser success');

  t.end();
});
