import { call, put, select, takeLatest } from 'redux-saga/effects';
import { get, post } from '../../../../../modules/utils/AxiosWrapper';
import { REGISTER_USER, setDemoUser, setError } from '../actions';
import loginSelector from '../selector';
import WealthWizards from '../../../../../modules/wealthwizards';
import { replace } from 'react-router-redux';

export const getDemoUser = state => loginSelector(state).demoUser;
export const getTheUserEmail = state => loginSelector(state).email;
export const getTheEmployerHash = state => loginSelector(state).employerHash;
export const checkEmployerHash = theHash =>
  get(`/check-employer-hash/${theHash}`);
export const checkEmailExists = theEmail =>
  post({
    route: '/check-email',
    data: { email: theEmail.toLowerCase() },
  });

export function* registerUser() {
  try {
    yield put(setDemoUser());
    const demoUser = yield select(getDemoUser);
    const employerHash = yield select(getTheEmployerHash);
    const userEmail = yield select(getTheUserEmail);
    const { data: employer } = yield call(checkEmployerHash, employerHash);
    const { data: email } = yield call(checkEmailExists, userEmail);

    if (email.length && !demoUser) {
      yield put(
        setError(
          'This email was already used, you should have received an email with tips on your financial priorities'
        )
      );
      return false;
    }

    if (employer.length < 1) {
      yield put(
        setError(
          'You need to ask your employer for a valid web address before you can start'
        )
      );
      return false;
    }

    yield put(setDemoUser());
    yield put(replace(`${WealthWizards.CONTEXT_ROUTE}/${employerHash}/tips`));

    return true;
  } catch (error) {
    yield put(
      setError(
        'We were unable to process your request, please try again later.'
      )
    );
    return false;
  }
}

export default function* registerSaga() {
  yield takeLatest(REGISTER_USER, registerUser);
}
