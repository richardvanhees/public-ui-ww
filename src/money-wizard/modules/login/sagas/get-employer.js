import { call, put, takeLatest, select } from 'redux-saga/effects';
import { get } from '../../../../../modules/utils/AxiosWrapper';
import {
  GET_EMPLOYER,
  getEmployerStart,
  getEmployerSuccess,
  getEmployerFail,
  loginRequest,
} from '../actions';
import WealthWizards from '../../../../../modules/wealthwizards';
import { replace } from 'react-router-redux';
import loginSelector from '../selector';

export const checkEmployerHash = theHash =>
  get(`/check-employer-hash/${theHash}`);

export const getEmployerName = state => loginSelector(state).employerName;

/* eslint-disable consistent-return */
export function* getEmployer({ employerHash }) {
  try {
    // if this is set we don't need to do all again, we are good
    const employerName = yield select(getEmployerName);
    if (!employerName) {
      yield put(getEmployerStart());
      const { data: employerData } = yield call(
        checkEmployerHash,
        employerHash
      );

      if (employerData.length < 1) {
        yield put(getEmployerFail());
        yield put(replace(`${WealthWizards.CONTEXT_ROUTE}/`));
        return false;
      }

      yield put(getEmployerSuccess(employerData[0]));
    }

    yield put(loginRequest());
  } catch (error) {
    yield put(getEmployerFail());
    yield put(replace(`${WealthWizards.CONTEXT_ROUTE}/`));
  }
}

export default function* getEmployerSaga() {
  yield takeLatest(GET_EMPLOYER, getEmployer);
}
