import test from 'tape';
import * as actions from './actions.js';

test('LoginActions', t => {
  let result;

  result = actions.loginRequest('fakePassword');
  t.deepEqual(result, { type: actions.LOGIN_REQUEST, password: 'fakePassword' }, 'loginRequest');

  result = actions.setLoggedIn();
  t.deepEqual(result, { type: actions.LOGIN_SET_LOGGED_IN }, 'setLoggedIn');

  result = actions.togglePrivacyPolicy();
  t.deepEqual(result, { type: actions.LOGIN_PRIVACY_TOGGLE }, 'togglePrivacyPolicy');

  result = actions.setEmail('fakeEmail');
  t.deepEqual(result, { type: actions.LOGIN_SET_EMAIL, email: 'fakeEmail' }, 'setEmail');

  result = actions.setFirstUse(true);
  t.deepEqual(result, { type: actions.LOGIN_SET_FIRST_USE, firstUse: true }, 'setFirstUse');

  result = actions.setEmployerHash('fakeHash');
  t.deepEqual(result, { type: actions.LOGIN_SET_EMPLOYER_HASH, employerHash: 'fakeHash' }, 'setEmployerHash');

  result = actions.resetError();
  t.deepEqual(result, { type: actions.LOGIN_RESET_ERROR, showError: false, errorMessage: '' }, 'resetError');

  result = actions.setError('fakeError');
  t.deepEqual(result, { type: actions.LOGIN_SET_ERROR, showError: true, errorMessage: 'fakeError' }, 'setError');

  result = actions.setDemoUser();
  t.deepEqual(result, { type: actions.LOGIN_SET_DEMO_USER }, 'setDemoUser');

  result = actions.setUserReturned();
  t.deepEqual(result, { type: actions.LOGIN_SET_USER_RETURNED }, 'setUserReturned');

  result = actions.registerUser();
  t.deepEqual(result, { type: actions.REGISTER_USER }, 'registerUser');

  result = actions.registerUserStart();
  t.deepEqual(result, { type: actions.REGISTER_USER_START }, 'registerUserStart');

  result = actions.registerUserSuccess();
  t.deepEqual(result, { type: actions.REGISTER_USER_SUCCESS }, 'registerUserSuccess');

  result = actions.registerUserFail();
  t.deepEqual(result, { type: actions.REGISTER_USER_FAIL }, 'registerUserFail');

  result = actions.getEmployerStart();
  t.deepEqual(result, { type: actions.GET_EMPLOYER_START }, 'getEmployerStart');

  result = actions.getEmployer('fakeHash');
  t.deepEqual(result, { type: actions.GET_EMPLOYER, employerHash: 'fakeHash' }, 'getEmployer');

  result = actions.getEmployerSuccess('fakeEmployerData');
  t.deepEqual(result, { type: actions.GET_EMPLOYER_SUCCESS, employerData: 'fakeEmployerData' }, 'getEmployerSuccess');

  result = actions.getEmployerFail();
  t.deepEqual(result, { type: actions.GET_EMPLOYER_FAIL }, 'getEmployerFail');

  t.end();
});

