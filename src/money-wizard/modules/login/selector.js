import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const isLoading = state => R.path(['login', 'isLoading'], state);
const privacyPolicyAgreed = state =>
  R.path(['login', 'privacyPolicyAgreed'], state);
const error = state => R.path(['login', 'error'], state);
const showError = state => R.path(['login', 'showError'], state);
const errorMessage = state => R.path(['login', 'errorMessage'], state);
const email = state => R.path(['login', 'email'], state);
const employerHash = state => R.path(['login', 'employerHash'], state);
const employerName = state => R.path(['login', 'employerName'], state);
const mailTemplate = state => R.path(['login', 'mailTemplate'], state);
const popupType = state => R.path(['login', 'popupType'], state);
const demoUser = state => R.path(['login', 'demoUser'], state);
const firstUse = state => R.path(['login', 'firstUse'], state);
const bespokeLinks = state => R.path(['login', 'bespokeLinks'], state);
const questionsVersion = state => R.path(['login', 'questionsVersion'], state);
const canLogin = state => R.path(['login', 'canLogin'], state);
const disableEmail = state => R.path(['login', 'disableEmail'], state);

export default createStructuredSelector({
  isLoading,
  privacyPolicyAgreed,
  error,
  showError,
  errorMessage,
  email,
  employerHash,
  employerName,
  mailTemplate,
  popupType,
  demoUser,
  firstUse,
  bespokeLinks,
  questionsVersion,
  canLogin,
  disableEmail,
});
