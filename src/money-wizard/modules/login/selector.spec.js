import test from 'tape';
import selector from './selector.js';

test('LoginSelector', t => {
  const result = selector({
    login: {
      email: '',
      employerHash: '',
      employerName: '',
      error: '',
      errorMessage: '',
      isLoading: false,
      privacyPolicyAgreed: '',
      showError: '',
      mailTemplate: '',
      popupType: 0,
      demoUser: false,
      disableEmail: false,
      firstUse: false,
      bespokeLinks: [],
      canLogin: true,
      questionsVersion: 3,
      disableEmail: false,
    },
  });

  t.deepEqual(result, {
    email: '',
    employerHash: '',
    employerName: '',
    error: '',
    errorMessage: '',
    isLoading: false,
    privacyPolicyAgreed: '',
    showError: '',
    mailTemplate: '',
    popupType: 0,
    demoUser: false,
    disableEmail: false,
    firstUse: false,
    bespokeLinks: [],
    questionsVersion: 3,
    canLogin: true,
    disableEmail: false,
  });

  t.end();
});
