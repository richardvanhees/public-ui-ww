import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generatePrioritySuggestionSaga, {
  generatePrioritySuggestion,
  getTheUserInput,
  callToGetPrioritySuggestion,
  getVersionData,
} from './sagas';
import {
  GENERATE_PRIORITY_SUGGESTION,
  GENERATE_PRIORITY_SUGGESTION_START,
  GENERATE_PRIORITY_SUGGESTION_FAILED,
  GENERATE_PRIORITY_SUGGESTION_SUCCESS,
  generatePrioritySuggestionStart,
  generatePrioritySuggestionSuccess,
  generatePrioritySuggestionFailed,
} from './actions';

test('GenerateSagas', t => {
  let target;

  target = testSaga(generatePrioritySuggestion)
    .next()
    .put({ type: GENERATE_PRIORITY_SUGGESTION_START })
    .next()
    .select(getTheUserInput)
    .next()
    .select(getVersionData)
    .next({ prioritiesVersion: 2, questionsVersion: 1 })
    .put({ type: GENERATE_PRIORITY_SUGGESTION_FAILED })
    .next();
  t.equals(!!target.isDone(), true, 'generatePrioritySuggestion failed');

  const userInput = { data: 'testData' };
  target = testSaga(generatePrioritySuggestion)
    .next()
    .put({ type: GENERATE_PRIORITY_SUGGESTION_START })
    .next()
    .select(getTheUserInput)
    .next(userInput)
    .select(getVersionData)
    .next({ prioritiesVersion: 2, questionsVersion: 1 })
    .call(callToGetPrioritySuggestion, {
      questionsVersion: 1,
      prioritiesVersion: 2,
      userInput,
    })
    .next(userInput)
    .put({ type: GENERATE_PRIORITY_SUGGESTION_SUCCESS, priorities: 'testData' })
    .next();
  t.equals(!!target.isDone(), true, 'generatePrioritySuggestion success');

  t.end();
});
