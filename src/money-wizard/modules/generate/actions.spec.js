import test from 'tape';
import * as actions from './actions.js';

test('GenerateActions', t => {
  let result;

  result = actions.generatePrioritySuggestionStart();
  t.deepEqual(
    result,
    { type: actions.GENERATE_PRIORITY_SUGGESTION_START },
    'generatePrioritySuggestionStart'
  );

  result = actions.generatePrioritySuggestion();
  t.deepEqual(
    result,
    { type: actions.GENERATE_PRIORITY_SUGGESTION },
    'generatePrioritySuggestion'
  );

  result = actions.generatePrioritySuggestionSuccess('testData');
  t.deepEqual(
    result,
    {
      type: actions.GENERATE_PRIORITY_SUGGESTION_SUCCESS,
      priorities: 'testData',
    },
    'generatePrioritySuggestionSuccess'
  );

  result = actions.setUserOrder('testData');
  t.deepEqual(
    result,
    { type: actions.SET_USER_ORDER, userOrder: 'testData' },
    'setUserOrder'
  );

  result = actions.setResetOrder();
  t.deepEqual(result, { type: actions.SET_RESET_ORDER }, 'setResetOrder');

  result = actions.mapUserOrder({userOrder: 'testData'});
  t.deepEqual(result, { type: actions.MAP_USER_ORDER, userOrder: 'testData' }, 'mapUserOrder');

  t.end();
});
