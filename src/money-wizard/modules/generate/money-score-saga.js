import { call, put, select, takeLatest } from 'redux-saga/effects';
import { post } from '../../../../modules/utils/AxiosWrapper';

import {
  generateScoreSuggestionStart,
  generateScoreSuggestionSuccess,
  generateScoreSuggestionFailed,
  GENERATE_SCORE_SUGGESTION,
} from './actions';
import generateSelector from './selector';
import WealthWizards from '../../../../modules/wealthwizards';
import { push } from 'react-router-redux';
import loginSelector from '../login/selector';

export const getTheEmployerHash = state => loginSelector(state).employerHash;

export const getTheUserInput = state => generateSelector(state).userInput;

export const getVersionData = state => generateSelector(state);

export const callToGetScoreSuggestion = data =>
  post({
    route: '/generate-score',
    data,
  });

export function* generateScoreSuggestion() {
  try {
    yield put(generateScoreSuggestionStart());
    const userInput = yield select(getTheUserInput);
    const { questionsVersion } = yield select(getVersionData);

    if (!userInput) {
      yield put(generateScoreSuggestionFailed());
      return;
    }

    const { data: { score: moneyScore } } = yield call(
      callToGetScoreSuggestion,
      {
        questionsVersion,
        userInput,
      }
    );
    yield put(generateScoreSuggestionSuccess(moneyScore));

    const employerHash = yield select(getTheEmployerHash);
    yield put(push(`${WealthWizards.CONTEXT_ROUTE}/${employerHash}/dashboard`));
  } catch (error) {}
}

export default function* generateScoreSuggestionSaga() {
  yield takeLatest(GENERATE_SCORE_SUGGESTION, generateScoreSuggestion);
}
