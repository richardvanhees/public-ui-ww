export const GENERATE_PRIORITY_SUGGESTION_START =
  'generate/GENERATE_PRIORITY_SUGGESTION_START';
export const GENERATE_PRIORITY_SUGGESTION =
  'generate/GENERATE_PRIORITY_SUGGESTION';
export const GENERATE_PRIORITY_SUGGESTION_SUCCESS =
  'generate/GENERATE_PRIORITY_SUGGESTION_SUCCESS';
export const GENERATE_PRIORITY_SUGGESTION_FAILED =
  'generate/GENERATE_PRIORITY_SUGGESTION_FAILED';
export const SET_USER_ORDER = 'generate/SET_USER_ORDER';
export const SET_RESET_ORDER = 'generate/SET_RESET_ORDER';
export const GENERATE_SCORE_SUGGESTION_START =
  'generate/GENERATE_SCORE_SUGGESTION_START';
export const GENERATE_SCORE_SUGGESTION = 'generate/GENERATE_SCORE_SUGGESTION';
export const GENERATE_SCORE_SUGGESTION_SUCCESS =
  'generate/GENERATE_SCORE_SUGGESTION_SUCCESS';
export const GENERATE_SCORE_SUGGESTION_FAILED =
  'generate/GENERATE_SCORE_SUGGESTION_FAILED';
export const MAP_USER_ORDER = 'generate/MAP_USER_ORDER';

export const generatePrioritySuggestionStart = () => ({
  type: GENERATE_PRIORITY_SUGGESTION_START,
});

export const generatePrioritySuggestion = () => ({
  type: GENERATE_PRIORITY_SUGGESTION,
});

export const generatePrioritySuggestionSuccess = data => ({
  type: GENERATE_PRIORITY_SUGGESTION_SUCCESS,
  priorities: data,
});

export const generatePrioritySuggestionFailed = () => ({
  type: GENERATE_PRIORITY_SUGGESTION_FAILED,
});

export const setUserOrder = data => ({
  type: SET_USER_ORDER,
  userOrder: data,
});

export const setResetOrder = () => ({
  type: SET_RESET_ORDER,
});

export const generateScoreSuggestionStart = () => ({
  type: GENERATE_SCORE_SUGGESTION_START,
});

export const generateScoreSuggestion = () => ({
  type: GENERATE_SCORE_SUGGESTION,
});

export const generateScoreSuggestionSuccess = data => ({
  type: GENERATE_SCORE_SUGGESTION_SUCCESS,
  score: data,
});

export const generateScoreSuggestionFailed = () => ({
  type: GENERATE_SCORE_SUGGESTION_FAILED,
});

export const mapUserOrder = ({ userOrder }) => ({
  type: MAP_USER_ORDER,
  userOrder,
});
