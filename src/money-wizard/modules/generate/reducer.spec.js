import test from 'tape';
import reducer from './reducer.js';
import * as generateTypes from './actions';

test('GenerateReducer', t => {
  let result;

  result = reducer(
    {},
    { type: generateTypes.GENERATE_PRIORITY_SUGGESTION_START }
  );
  t.deepEqual(
    result,
    { isLoading: true },
    'GENERATE_PRIORITY_SUGGESTION_START'
  );

  result = reducer(
    {},
    {
      type: generateTypes.GENERATE_PRIORITY_SUGGESTION_SUCCESS,
      priorities: 'testData',
    }
  );
  t.deepEqual(
    result,
    { isLoading: false, priorities: 'testData', resetOrder: true },
    'GENERATE_PRIORITY_SUGGESTION_SUCCESS'
  );

  result = reducer(
    {},
    { type: generateTypes.SET_USER_ORDER, userOrder: 'testData' }
  );
  t.deepEqual(result, { userOrder: 'testData' }, 'SET_USER_ORDER');

  result = reducer({}, { type: generateTypes.SET_RESET_ORDER });
  t.deepEqual(result, { resetOrder: false }, 'SET_RESET_ORDER');

  result = reducer({}, { type: generateTypes.MAP_USER_ORDER, userOrder: 'fakeData' });
  t.deepEqual(result, { userOrder: 'fakeData' }, 'MAP_USER_ORDER');

  t.end();
});
