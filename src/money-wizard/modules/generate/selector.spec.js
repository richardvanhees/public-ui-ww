import test from 'tape';
import selector from './selector.js';

test('GenerateSelector', t => {
  const result = selector({
    generate: {
      isLoading: false,
      priorities: [],
      userOrder: [],
      error: '',
    },
    questionnaire: {
      data: {},
    },
    login: {
      prioritiesVersion: 2,
      questionsVersion: 1,
    },
  });

  t.deepEqual(result, {
    isLoading: false,
    priorities: [],

    userOrder: [],
    error: '',
    userInput: {},
    prioritiesVersion: 2,
    questionsVersion: 1,
  });

  t.end();
});
