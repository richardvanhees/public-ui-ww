import { call, put, select, takeLatest } from 'redux-saga/effects';
import { post } from '../../../../modules/utils/AxiosWrapper';

import {
  generatePrioritySuggestionStart,
  generatePrioritySuggestionSuccess,
  generatePrioritySuggestionFailed,
  GENERATE_PRIORITY_SUGGESTION,
} from './actions';
import generateSelector from './selector';

export const getTheUserInput = state => generateSelector(state).userInput;

export const getVersionData = state => generateSelector(state);

export const callToGetPrioritySuggestion = data =>
  post({
    route: '/generate-priorities',
    data,
  });

export function* generatePrioritySuggestion() {
  try {
    yield put(generatePrioritySuggestionStart());
    const userInput = yield select(getTheUserInput);
    const { prioritiesVersion, questionsVersion } = yield select(
      getVersionData
    );

    if (!userInput) {
      yield put(generatePrioritySuggestionFailed());
      return;
    }

    const { data } = yield call(callToGetPrioritySuggestion, {
      prioritiesVersion,
      questionsVersion,
      userInput,
    });
    yield put(generatePrioritySuggestionSuccess(data));
  } catch (error) {}
}

export default function* generatePrioritySuggestionSaga() {
  yield takeLatest(GENERATE_PRIORITY_SUGGESTION, generatePrioritySuggestion);
}
