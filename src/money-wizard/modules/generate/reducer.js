import {
  GENERATE_PRIORITY_SUGGESTION_START,
  GENERATE_PRIORITY_SUGGESTION_SUCCESS,
  GENERATE_PRIORITY_SUGGESTION_FAILED,
  GENERATE_SCORE_SUGGESTION_START,
  GENERATE_SCORE_SUGGESTION_SUCCESS,
  GENERATE_SCORE_SUGGESTION_FAILED,
  SET_USER_ORDER,
  SET_RESET_ORDER,
  MAP_USER_ORDER,
} from './actions';

const getPrioritiesStart = state => ({
  ...state,
  isLoading: true,
});

const getPriorities = (state, action) => ({
  ...state,
  isLoading: false,
  resetOrder: true,
  priorities: action.priorities,
});

const getPrioritiesFailed = state => ({
  ...state,
  isLoading: false,
});

const setUserOrder = (state, action) => ({
  ...state,
  userOrder: action.userOrder,
});

const setResetOrder = state => ({
  ...state,
  resetOrder: false,
});

const getScoreStart = state => ({
  ...state,
  isLoading: true,
});

const getScore = (state, action) => ({
  ...state,
  isLoading: false,
  score: action.score,
});

const getScoreFailed = state => ({
  ...state,
  isLoading: false,
});

const mapUserOrder = (state, { userOrder }) => ({
  ...state,
  userOrder,
});

const initialState = {
  isLoading: true,
  priorities: [],
  userOrder: [],
  resetOrder: false,
  score: 0,
};

const reducer = {
  [GENERATE_PRIORITY_SUGGESTION_START]: getPrioritiesStart,
  [GENERATE_PRIORITY_SUGGESTION_SUCCESS]: getPriorities,
  [GENERATE_PRIORITY_SUGGESTION_FAILED]: getPrioritiesFailed,
  [GENERATE_SCORE_SUGGESTION_START]: getScoreStart,
  [GENERATE_SCORE_SUGGESTION_SUCCESS]: getScore,
  [GENERATE_SCORE_SUGGESTION_FAILED]: getScoreFailed,
  [SET_USER_ORDER]: setUserOrder,
  [SET_RESET_ORDER]: setResetOrder,
  [MAP_USER_ORDER]: mapUserOrder,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
