import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const isLoading = state => R.path(['generate', 'isLoading'], state);
const userInput = state => R.path(['questionnaire', 'data'], state);
const priorities = state => R.path(['generate', 'priorities'], state);
const userOrder = state => R.path(['generate', 'userOrder'], state);
const error = state => R.path(['generate', 'error'], state);
const prioritiesVersion = state =>
  R.path(['login', 'prioritiesVersion'], state);
const questionsVersion = state => R.path(['login', 'questionsVersion'], state);

export default createStructuredSelector({
  isLoading,
  userInput,
  priorities,
  userOrder,
  error,
  prioritiesVersion,
  questionsVersion,
});
