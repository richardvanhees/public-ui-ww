import { call, put, takeLatest } from 'redux-saga/effects';
import { get } from '../../../../modules/utils/AxiosWrapper';
import { questionnaireTypes } from './types';
import { getQuestionsSuccess, getQuestionsFailure } from './actions';

export const callToGetQuestions = version => get(`/questions/${version}`);

export function* getQuestions(sagaAction) {
  try {
    const { data } = yield call(callToGetQuestions, sagaAction.version);
    yield put(getQuestionsSuccess(data[0]));
  } catch (error) {
    yield put(getQuestionsFailure('Failed to retrieve questions'));
  }
}

export default function* questionnaireSaga() {
  yield takeLatest(questionnaireTypes.GET_QUESTIONS, getQuestions);
}
