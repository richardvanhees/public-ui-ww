import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const questions = state => R.path(['questionnaire', 'questions'], state);
const version = state => R.path(['questionnaire', 'version'], state);
const userInput = state => R.path(['questionnaire', 'userInput'], state);
const employerQuestionsVersion = state =>
  R.path(['login', 'questionsVersion'], state);

export default createStructuredSelector({
  questions,
  version,
  userInput,
  employerQuestionsVersion,
});
