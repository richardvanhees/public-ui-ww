import { questionnaireTypes } from './types';

export const setActiveQuestion = questionId => ({
  type: questionnaireTypes.SET_ACTIVE_QUESTION,
  payload: { questionId },
});

export const getQuestions = version => ({
  type: questionnaireTypes.GET_QUESTIONS,
  version,
});

export const getQuestionsSuccess = data => ({
  type: questionnaireTypes.GET_QUESTIONS_SUCCESS,
  questions: data.questions,
  version: data.version,
});

export const getQuestionsFailure = error => ({
  type: questionnaireTypes.GET_QUESTIONS_FAILURE,
  error,
});

export const setAnswer = (questionId, answerData) => ({
  type: questionnaireTypes.SET_ANSWER,
  payload: { questionId, answerData },
});

export const mapUserAnswers = ({ answers }) => ({
  type: questionnaireTypes.MAP_USER_ANSWERS,
  answers,
});
