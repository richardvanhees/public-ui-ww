export const questionnaireTypes = {
  SET_ACTIVE_QUESTION: 'questionnaire/SET_ACTIVE_QUESTION',
  GET_QUESTIONS: 'questionnaire/GET_QUESTIONS',
  GET_QUESTIONS_SUCCESS: 'questionnaire/GET_QUESTIONS_SUCCESS',
  GET_QUESTIONS_FAILURE: 'questionnaire/GET_QUESTIONS_FAILURE',
  SET_ANSWER: 'questionnaire/SET_ANSWER',
  MAP_USER_ANSWERS: 'questionnaire/MAP_USER_ANSWERS',
};
