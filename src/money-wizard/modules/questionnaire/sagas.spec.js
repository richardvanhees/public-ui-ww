import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import questionnaireSaga, { getQuestions, callToGetQuestions } from './sagas';
import { questionnaireTypes } from './types';

test('QuestionnaireSagas', t => {
  let target;

  const testData = { data: [{ questions: 'testData', version: 2 }] };
  target = testSaga(getQuestions, { version: 2 })
    .next()
    .call(callToGetQuestions, 2)
    .next(testData)
    .put({
      type: questionnaireTypes.GET_QUESTIONS_SUCCESS,
      ...testData.data[0],
    })
    .next();

  t.equals(!!target.isDone(), true, 'getQuestions success');

  t.end();
});
