import test from 'tape';
import * as actions from './actions.js';
import { questionnaireTypes } from './types';

test('QuestionnaireActions', t => {
  let result;

  result = actions.getQuestions(2);
  t.deepEqual(
    result,
    { type: questionnaireTypes.GET_QUESTIONS, version: 2 },
    'getQuestions'
  );

  result = actions.getQuestionsSuccess({ questions: 'testData', version: 2 });
  t.deepEqual(
    result,
    {
      type: questionnaireTypes.GET_QUESTIONS_SUCCESS,
      questions: 'testData',
      version: 2,
    },
    'getQuestionsSuccess'
  );

  result = actions.getQuestionsFailure('error');
  t.deepEqual(
    result,
    { type: questionnaireTypes.GET_QUESTIONS_FAILURE, error: 'error' },
    'getQuestionsFailure'
  );

  result = actions.setActiveQuestion(1);
  t.deepEqual(
    result,
    {
      type: questionnaireTypes.SET_ACTIVE_QUESTION,
      payload: { questionId: 1 },
    },
    'setActiveQuestion'
  );

  result = actions.setAnswer(1, 'testData');
  t.deepEqual(
    result,
    {
      type: questionnaireTypes.SET_ANSWER,
      payload: { questionId: 1, answerData: 'testData' },
    },
    'setAnswer'
  );

  t.end();
});
