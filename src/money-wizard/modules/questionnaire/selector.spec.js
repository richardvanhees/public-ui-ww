import test from 'tape';
import selector from './selector.js';

test('QuestionnaireSelector', t => {
  const result = selector({
    questionnaire: {
      questions: [],
      version: 2,
      userInput: {},
    },
    login: {
      questionsVersion: 2,
    },
  });

  t.deepEqual(result, {
    questions: [],
    version: 2,
    userInput: {},
    employerQuestionsVersion: 2,
  });

  t.end();
});
