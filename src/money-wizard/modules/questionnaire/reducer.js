import { questionnaireTypes } from './types';

const initialState = {
  activeQuestion: 0,
  questions: [],
  userInput: {},
  error: '',
  version: 0,
  data: {},
};

const reducer = {
  [questionnaireTypes.SET_ACTIVE_QUESTION]: (state, action) => ({
    ...state,
    activeQuestion: action.payload.questionId,
  }),
  [questionnaireTypes.GET_QUESTIONS_SUCCESS]: (state, action) => ({
    ...state,
    questions: action.questions,
    version: action.version,
  }),
  [questionnaireTypes.GET_QUESTIONS_FAILURE]: (state, action) => ({
    ...state,
    error: action.error,
  }),
  [questionnaireTypes.SET_ANSWER]: (state, action) => ({
    ...state,
    data: {
      ...state.data,
      [action.payload.questionId]: action.payload.answerData,
    },
  }),
  [questionnaireTypes.MAP_USER_ANSWERS]: (state, { answers }) => ({
    ...state,
    data: answers,
  }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
