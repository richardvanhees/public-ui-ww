import test from 'tape';
import reducer from './reducer.js';
import { questionnaireTypes } from './types';

test('QuestionnaireReducer', t => {
  let result;

  result = reducer(
    {},
    { type: questionnaireTypes.SET_ACTIVE_QUESTION, payload: { questionId: 1 } }
  );
  t.deepEqual(result, { activeQuestion: 1 }, 'SET_ACTIVE_QUESTION');

  result = reducer(
    {},
    {
      type: questionnaireTypes.GET_QUESTIONS_SUCCESS,
      questions: 'testData',
      version: 1,
    }
  );
  t.deepEqual(
    result,
    { questions: 'testData', version: 1 },
    'GET_QUESTIONS_SUCCESS'
  );

  result = reducer(
    {},
    { type: questionnaireTypes.GET_QUESTIONS_FAILURE, error: 'error' }
  );
  t.deepEqual(result, { error: 'error' }, 'GET_QUESTIONS_FAILURE');

  result = reducer(
    {},
    {
      type: questionnaireTypes.SET_ANSWER,
      payload: { questionId: 1, answerData: 'testData' },
    }
  );
  t.deepEqual(result, { data: { 1: 'testData' } }, 'SET_ANSWER');

  t.end();
});
