import test from 'tape';
import reducer from './reducer.js';
import patTypes from './types';

test('PatReducer', t => {
  let result;

  result = reducer({}, { type: patTypes.HANDLE_FIELD_CHANGE, data: { test: 'test' }, });
  t.deepEqual(result, { test: 'test' }, 'HANDLE_FIELD_CHANGE');

  result = reducer({}, { type: patTypes.SET_PAT_MAIL_SENT });
  t.deepEqual(result, { mailSent: true }, 'SET_PAT_MAIL_SENT');

  t.end();
});
