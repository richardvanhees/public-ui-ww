import test from 'tape';
import selector from './selector.js';

test('PatSelector', t => {
  const result = selector({
    pat: {
      name: 'Fake Name',
      phone: 'Fake phone number',
      contactTime: 'Fake contact time',
      adviceArea: 'Fake advice area',
      mailSent: true,
    },
  });

  t.deepEqual(result, {
    name: 'Fake Name',
    phone: 'Fake phone number',
    contactTime: 'Fake contact time',
    adviceArea: 'Fake advice area',
    mailSent: true,
  });

  t.end();
});
