import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const name = state => R.path(['pat', 'name'], state);
const phone = state => R.path(['pat', 'phone'], state);
const contactTime = state => R.path(['pat', 'contactTime'], state);
const adviceArea = state => R.path(['pat', 'adviceArea'], state);
const mailSent = state => R.path(['pat', 'mailSent'], state);

export default createStructuredSelector({
  name,
  phone,
  contactTime,
  adviceArea,
  mailSent,
});
