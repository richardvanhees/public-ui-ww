import patTypes from './types';

export const handleFieldChange = data => ({
  type: patTypes.HANDLE_FIELD_CHANGE,
  data,
});

export const sendPatMail = () => ({
  type: patTypes.SEND_PAT_MAIL,
});

export const setPatMailSent = () => ({
  type: patTypes.SET_PAT_MAIL_SENT,
});
