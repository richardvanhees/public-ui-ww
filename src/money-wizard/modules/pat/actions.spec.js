import test from 'tape';
import * as actions from './actions.js';
import patTypes from './types';

test('PatActions', t => {
  let result;

  result = actions.handleFieldChange({ test: 'test' });
  t.deepEqual(result, { type: patTypes.HANDLE_FIELD_CHANGE, data: { test: 'test' } }, 'handleFieldChange');

  result = actions.sendPatMail();
  t.deepEqual(result, { type: patTypes.SEND_PAT_MAIL }, 'sendPatMail');

  result = actions.setPatMailSent();
  t.deepEqual(result, { type: patTypes.SET_PAT_MAIL_SENT }, 'setPatMailSent');



  t.end();
});
