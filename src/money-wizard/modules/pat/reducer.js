import patTypes from './types';

const initialState = {
  name: null,
  phone: null,
  contactTime: null,
  adviceArea: null,
  mailSent: false,
};

export const handleFieldChange = (state, { data }) => ({
  ...state,
  ...data,
});

export const setPatMailSent = (state) => ({
  ...state,
  mailSent: true,
});

const reducer = {
  [patTypes.HANDLE_FIELD_CHANGE]: handleFieldChange,
  [patTypes.SET_PAT_MAIL_SENT]: setPatMailSent,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
