import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import sendPatMailSaga, { sendPatMail, callToSendEmail, getUserData } from './send-pat-mail';
import { setPatMailSent } from '../actions';
import { setError } from '../../login/actions';
import tk from 'timekeeper';
import randomPassword from '../../../../../modules/utils/random-password';
import { setLoggedIn } from '../../login';

const time = new Date(1330688329321);

// Setup
const testData = {
  name: 'fakeName',
  phone: 'fakePhone',
  contactTime: 'fakeContactTime',
  adviceArea: 'fakeAdviceArea',
};

// Tests

test('PatSaga', t => {
  let target;

  target = testSaga(sendPatMail)
    .next()
    .select(getUserData)
    .next(testData)
    .call(callToSendEmail, testData)
    .next()
    .put(setPatMailSent())
    .next();
  t.equals(!!target.isDone(), true, 'sendPatMail success');

  t.end();
});

test('PatSaga', t => {
  let target;

  target = testSaga(sendPatMail)
    .next()
    .select(getUserData)
    .next(null)
    .put(setError('We were unable to send your comtact request.'))
    .next();
  t.equals(!!target.isDone(), true, 'sendPatMail fail');

  t.end();
});