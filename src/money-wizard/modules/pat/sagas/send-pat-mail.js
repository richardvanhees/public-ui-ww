import { call, put, takeLatest, select } from 'redux-saga/effects';
import { post } from '../../../../../modules/utils/AxiosWrapper';
import patTypes from '../types';
import { setPatMailSent } from '../actions';
import { setError } from '../../login/actions';
import patSelector from '../selector';

export const callToSendEmail = tokens => post({ route: '/send-pat-mail', data: { tokens } });
export const getUserData = state => patSelector(state);

export function* sendPatMail() {
  try {
    const { name, phone, contactTime, adviceArea } = yield select(getUserData);
    yield call(callToSendEmail, { name, phone, contactTime, adviceArea });
    yield put(setPatMailSent());
  } catch (error) {
    yield put(setError('We were unable to send your comtact request.'));
  }
}

export default function* sendPatMailSaga() {
  yield takeLatest(patTypes.SEND_PAT_MAIL, sendPatMail);
}
