import test from 'tape';
import reducer from './reducer.js';
import * as tipsTypes from './actions';

test('TipsReducer', t => {
  let result;

  result = reducer({}, { type: tipsTypes.EMAIL_SENT });
  t.deepEqual(result, { emailSent: true }, 'EMAIL_SENT');

  result = reducer(
    {},
    { type: tipsTypes.POPUP_DATA_RETRIEVED, data: 'testPopup' }
  );
  t.deepEqual(result, { popup: 'testPopup' }, 'POPUP_DATA_RETRIEVED');

  result = reducer(
    {},
    { type: tipsTypes.SET_OPT_IN_EMAIL, email: 'testEmail' }
  );
  t.deepEqual(result, { optInEmail: 'testEmail' }, 'SET_OPT_IN_EMAIL');

  result = reducer({}, { type: tipsTypes.SET_OPT_IN_SUCCESS });
  t.deepEqual(result, { optInSet: true }, 'SET_OPT_IN_SUCCESS');

  result = reducer({}, { type: tipsTypes.SET_ERROR, error: 'error' });
  t.deepEqual(result, { error: 'error' }, 'SET_ERROR');

  t.end();
});

test('TipsReducer', assert => {
  assert.plan(1);
  const result = reducer(
    {},
    { type: tipsTypes.SET_PRIZE_DRAW_OPT_IN, optIn: true }
  );
  assert.deepEqual(result, { prizeDrawOptIn: true }, 'SET_PRIZE_DRAW_OPT_IN');
});
