import {
  EMAIL_SENT,
  POPUP_DATA_RETRIEVED,
  SET_OPT_IN_SUCCESS,
  SET_ERROR,
  SET_OPT_IN_EMAIL,
  SET_PRIZE_DRAW_OPT_IN,
  SET_OPT_IN,
} from './actions';

const setEmailSent = state => ({
  ...state,
  emailSent: true,
});

const setPopupData = (state, { data }) => ({
  ...state,
  popup: data,
});

const setOptInEmail = (state, { email }) => ({
  ...state,
  optInEmail: email,
});

const setOptInSuccess = state => ({
  ...state,
  optInSet: true,
});

const setOptIn = (state, { optIn }) => ({
  ...state,
  optIn,
});

const setPrizeDrawOptIn = (state, { optIn }) => ({
  ...state,
  prizeDrawOptIn: optIn,
});

const setError = (state, { error }) => ({
  ...state,
  error,
});

const initialState = {
  emailSent: false,
  popup: null,
  optInEmail: '',
  error: null,
  optInSet: null,
  optIn: false,
  prizeDrawOptIn: false,
};

const reducer = {
  [EMAIL_SENT]: setEmailSent,
  [POPUP_DATA_RETRIEVED]: setPopupData,
  [SET_OPT_IN_EMAIL]: setOptInEmail,
  [SET_OPT_IN_SUCCESS]: setOptInSuccess,
  [SET_OPT_IN]: setOptIn,
  [SET_PRIZE_DRAW_OPT_IN]: setPrizeDrawOptIn,
  [SET_ERROR]: setError,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
