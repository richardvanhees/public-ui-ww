import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generateSaga, {
  saveAnswers,
  callToSaveAnswers,
  getTheUserAnswers,
  callToSendEmail,
  checkEmailExists,
  callToGetPopupData,
  callToUpdateAnswers,
  getEmployerName,
  getDemoUser,
  getPriorityData,
  getBespokeLinks,
  checkIfUserCanLogin,
  checkIfEmployerDisabledEmail
} from './save-answers-saga';
import * as actions from '../actions';
import tk from 'timekeeper';
import randomPassword from '../../../../../modules/utils/random-password';
import { setLoggedIn } from '../../login';

const time = new Date(1330688329321);

// Setup
const testData = {
  consentedPrivacyPolicy: true,
  email: 'unittest@test.com',
  template: 'Money Wizard (Pension Wizard user)',
  employer: 'ww',
  questions: {},
  wwFinancialPriorities: [],
  userFinancialPriorities: [],
  questionVersion: 1,
  emailSent: false,
  popupType: 1,
  password: 'fakeRandomPassword',
};

const testPopupData = {
  _id: '5a8c01865cdf6be2a3b8134a',
  updated_at: '2018-02-21T13:49:26.648Z',
  created_at: '2018-02-20T11:07:50.224Z',
  type: 1,
  image: 'donkey',
  title: 'Hey, you!',
  description:
    'Sign up to the Money Wizard newsletters for tips, tools and resources to help you make the most out of your money.',
  callToAction: 'Sign up to the newsletter',
  cancel: 'no thanks',
  __v: 0,
};

const testPriorities = [
  {
    top_tip: 'top top 1',
    id: 0,
    name: 'name 1',
    image: 'image 1',
    tipsLinks: [
      {
        emailDescriptionEnd: ' email description end 1',
        emailCaption: 'email caption 1',
        emailDescriptionStart: 'email description start 1 ',
        caption: 'caption 1',
        url: 'url 1',
      },
    ],
    alternative_descriptions: [],
    short_description: 'short description 1',
    long_description: 'long description 1',
    list: [],
    link_header: 'link header 1',
  },
  {
    top_tip: 'top top 2',
    id: 1,
    name: 'name 2',
    image: 'image 2',
    tipsLinks: [
      {
        emailDescriptionEnd: ' email description end 2',
        emailCaption: 'email caption 2',
        emailDescriptionStart: 'email description start 2 ',
        caption: 'caption 2',
        url: 'url 2',
      },
    ],
    alternative_descriptions: [],
    short_description: 'short description 2',
    long_description: 'long description 2',
    list: [],
    link_header: 'link header 2',
  },
];

// Tests

test('TipsSagas', t => {
  let target;

  tk.freeze(time);

  target = testSaga(saveAnswers)
    .next()
    .select(getDemoUser)
    .next(false)
    .select(getTheUserAnswers)
    .next(testData)
    .select(getEmployerName)
    .next('csc')
    .call(callToGetPopupData, testData.popupType, 'csc')
    .next({ data: { ...testPopupData } })
    .select(getPriorityData)
    .next(testPriorities)
    .select(getBespokeLinks)
    .next([])
    .put({ type: 'tips/POPUP_DATA_RETRIEVED', data: testPopupData })
    .next()
    .call(checkEmailExists, testData.email)
    .next({ data: [] })
    .call(randomPassword)
    .next(testData.password)
    .select(checkIfEmployerDisabledEmail)
    .next(false)
    .call(callToSaveAnswers, testData, 'csc')
    .next()
    .select(checkIfUserCanLogin)
    .next(true)
    .fork(callToSendEmail, {
      email: testData.email,
      template: testData.template,
      tokens: { password: testData.password, priorities: [] },
    })
    .next()
    .call(
      callToUpdateAnswers,
      { email: testData.email, emailSent: true },
      'csc'
    )
    .next()
    .put({ type: actions.EMAIL_SENT })
    .next()
    .put(setLoggedIn())
    .next();
  t.equals(!!target.isDone(), true, 'saveAnswers success');

  t.end();
});

test('TipsSagas - dont resend email', t => {
  let target;

  tk.freeze(time);

  target = testSaga(saveAnswers)
    .next()
    .select(getDemoUser)
    .next(false)
    .select(getTheUserAnswers)
    .next(testData)
    .select(getEmployerName)
    .next('csc')
    .call(callToGetPopupData, testData.popupType, 'csc')
    .next({ data: { ...testPopupData } })
    .select(getPriorityData)
    .next(testPriorities)
    .select(getBespokeLinks)
    .next([])
    .put({ type: 'tips/POPUP_DATA_RETRIEVED', data: testPopupData })
    .next()
    .call(checkEmailExists, testData.email)
    .next({ data: [{ foo: 1 }] })
    .call(randomPassword)
    .next(testData.password)
    .select(checkIfEmployerDisabledEmail)
    .next(false)
    .call(callToUpdateAnswers, testData, 'csc')
    .next()
    .fork(callToSendEmail, {
      email: testData.email,
      template: testData.template,
      tokens: { priorities: [] },
    })
    .next()
    .put({ type: actions.EMAIL_SENT })
    .next()
    .put(setLoggedIn())
    .next();
  t.equals(!!target.isDone(), true, 'saveAnswers success');

  t.end();
});

test('TipsSagas - send email', t => {
  let target;

  tk.freeze(time);

  target = testSaga(saveAnswers)
    .next()
    .select(getDemoUser)
    .next(false)
    .select(getTheUserAnswers)
    .next(testData)
    .select(getEmployerName)
    .next('csc')
    .call(callToGetPopupData, testData.popupType, 'csc')
    .next({ data: { ...testPopupData } })
    .select(getPriorityData)
    .next(testPriorities)
    .select(getBespokeLinks)
    .next([])
    .put({ type: 'tips/POPUP_DATA_RETRIEVED', data: testPopupData })
    .next()
    .call(checkEmailExists, testData.email)
    .next({ data: [{ foo: 1 }] })
    .call(randomPassword)
    .next(testData.password)
    .select(checkIfEmployerDisabledEmail)
    .next(true)
    .call(callToUpdateAnswers, testData, 'csc')
    .next()
    .put(setLoggedIn())
    .next();
  t.equals(!!target.isDone(), true, 'saveAnswers success');

  t.end();
});
