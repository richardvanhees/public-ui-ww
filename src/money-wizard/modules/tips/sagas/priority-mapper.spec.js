import test from 'tape';
import priorityMapper from './priority-mapper';

test('Priority mapper', t => {
  t.test('Map priorities for email', t => {
    t.plan(1);
    const testData = [
      {
        id: 5,
        name: "Managing regular spending",
        tipsLinks: [{
          caption: "Test caption A",
          emailCaption: "Click here",
          priorityId: 5,
          url: "https://www.partnerchoice.co.uk/listing/2899/retailcure"
        }, {
          caption: "Test caption B",
          emailDescriptionStart: 'Fake start B ',
          emailCaption: "Click here B",
          priorityId: 5,
          url: "https://www.partnerchoice.co.uk/listing/2899/retailcure"
        }],
      },
      {
        id: 3,
        name: "Saving into pensions",
        tipsLinks: [{
          caption: "Test caption 2",
          emailDescriptionStart: 'Fake start',
          emailCaption: "Click here too",
          emailDescriptionEnd: 'Fake end',
          priorityId: 3,
          url: "https://www.partnerchoice.co.uk/listing/2899/retailcure"
        }]
      },
    ];

    const result = priorityMapper(testData);

    const expectedNewData = [{
      intro: false,
      priorityTitle: "Managing regular spending",
      tools: [{
        toolUrl: "https://www.partnerchoice.co.uk/listing/2899/retailcure",
        descriptionStart: false,
        caption: "Click here",
        descriptionEnd: false,
      }, {
        toolUrl: "https://www.partnerchoice.co.uk/listing/2899/retailcure",
        descriptionStart: "Fake start B ",
        caption: "Click here B",
        descriptionEnd: false,
      }]
    }, {
      intro: false,
      priorityTitle: "Saving into pensions",
      tools: [{
        toolUrl: "https://www.partnerchoice.co.uk/listing/2899/retailcure",
        descriptionStart: "Fake start",
        caption: "Click here too",
        descriptionEnd: "Fake end"
      }]
    }];

    t.deepEqual(result, expectedNewData, 'Priorities have succesfully been mapped');
    t.end();
  });
});
