export default priorities =>
  priorities.map(priority => ({
    priorityTitle: priority.name,
    intro: priority.tipsLinks.find(({ type }) => type === 'intro') && priority.tipsLinks.find(({ type }) => type === 'intro').content || false,
    tools: priority.tipsLinks
      .filter(link => link.type !== 'intro')
      .map(({ url, emailCaption, emailDescriptionStart, emailDescriptionEnd }) => ({
        toolUrl: url,
        descriptionStart: emailDescriptionStart || false,
        caption: emailCaption,
        descriptionEnd: emailDescriptionEnd || false,
      })),
  }));
