import { call, select, put, takeLatest } from 'redux-saga/effects';
import { SUBMIT, setError, setOptInSuccess } from '../actions';
import { putRequest, post } from '../../../../../modules/utils/AxiosWrapper';
import loginSelector from '../../login/selector';

export const getSubmissionData = state => ({
  email: loginSelector(state).email,
  optInEmail: state.tips.optInEmail,
  prizeDrawOptIn: state.tips.prizeDrawOptIn,
  optIn: state.tips.optIn,
});

export const checkIfOptInExists = email =>
  post({
    route: '/check-opt-in',
    data: { email },
  });

export const callToSetOptIn = (data, employer) =>
  putRequest({
    route: `/employers/${employer}/answers`,
    data,
  });

export const getDemoUser = state => loginSelector(state).demoUser;

export const getEmployerName = state => loginSelector(state).employerName;

export function* setOptIn() {
  try {
    const demoUser = yield select(getDemoUser);
    const { optIn, optInEmail, email, prizeDrawOptIn } = yield select(
      getSubmissionData
    );
    const optInData =
      optIn || prizeDrawOptIn
        ? { email, optIn, optInEmail, prizeDrawOptIn }
        : { email, optIn, prizeDrawOptIn };

    const employerName = yield select(getEmployerName);

    if (demoUser) {
      yield put(setOptInSuccess());
      return;
    }

    if (optIn) {
      const { data } = yield call(checkIfOptInExists, optInEmail.trim());

      if (data.code === 'RESOURCE_EXISTS') {
        yield put(setError({ error: data.message }));
      } else {
        yield call(callToSetOptIn, optInData, employerName);
        yield put(setOptInSuccess());
      }
    } else {
      yield call(callToSetOptIn, optInData, employerName);
      yield put(setOptInSuccess());
    }
  } catch (error) {}
}

export default function* optInSaga() {
  yield takeLatest(SUBMIT, setOptIn);
}
