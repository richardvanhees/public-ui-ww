import { call, select, put, takeLatest, fork } from 'redux-saga/effects';
import randomPassword from '../../../../../modules/utils/random-password';
import priorityMapper from './priority-mapper';
import { SAVE_ANSWERS, emailSentSuccess, setPopupData } from '../actions';
import { setError } from '../../login/actions';
import {
  post,
  putRequest,
  get,
} from '../../../../../modules/utils/AxiosWrapper';
import generateSelector from '../../../modules/generate/selector';
import loginSelector from '../../login/selector';
import * as selectors from '../selector';
import questionnaireSelector from '../../../modules/questionnaire/selector';
import prioritiesSelector from '../../../modules/priorities/selector';
import { setLoggedIn } from '../../login';
import WealthWizards from '../../../../../modules/wealthwizards';

export const checkEmailExists = theEmail =>
  post({
    route: '/check-email',
    data: { email: theEmail.toLowerCase() },
  });

export const getTheUserAnswers = state => ({
  email: loginSelector(state).email,
  employer: loginSelector(state).employerName,
  template: loginSelector(state).mailTemplate,
  popupType: loginSelector(state).popupType,
  consentedPrivacyPolicy: true,
  questions: generateSelector(state).userInput,
  wwFinancialPriorities: generateSelector(state).priorities,
  userFinancialPriorities: generateSelector(state).userOrder,
  questionVersion: questionnaireSelector(state).version,
  emailSent: selectors.emailSent(state),
});

export const getPriorityData = state => prioritiesSelector(state).priorities;

export const getBespokeLinks = state => loginSelector(state).bespokeLinks;

export const getDemoUser = state => loginSelector(state).demoUser;

export const getEmployerName = state => loginSelector(state).employerName;

export const checkIfUserCanLogin = state => !!loginSelector(state).canLogin;

export const checkIfEmployerDisabledEmail = state => loginSelector(state).disableEmail;

export const callToGetPopupData = (popupType, employer) =>
  get(`/employers/${employer}/popups/${popupType}`);

export const callToSaveAnswers = (data, employer) =>
  post({
    route: `/employers/${employer}/answers`,
    data,
  });

export const callToUpdateAnswers = (data, employer) =>
  putRequest({
    route: `/employers/${employer}/answers`,
    data,
  });

export const callToSendEmail = ({ email, template, tokens }) =>
  post({
    route: '/send-email',
    data: { email, template, tokens },
  });

export function* saveAnswers() {
  try {
    const demoUser = yield select(getDemoUser);
    const usersAnswers = yield select(getTheUserAnswers);
    const { email, template, popupType } = usersAnswers;
    const employerName = yield select(getEmployerName);
    const popupData = yield call(callToGetPopupData, popupType, employerName);
    const priorities = yield select(getPriorityData);
    const bespokeLinks = yield select(getBespokeLinks);

    const prioritiesWithBespokeLinks = priorities.map(priority => {
      const bespokeLinksForCurrentPriority = bespokeLinks.filter(
        bespokeLink => bespokeLink.priorityId === priority.id
      );
      return bespokeLinksForCurrentPriority.length &&
        !(
          WealthWizards.FORCE_WW_TEMPLATE &&
          WealthWizards.FORCE_WW_TEMPLATE.includes(employerName)
        )
        ? {
            ...priority,
            tipsLinks: bespokeLinksForCurrentPriority,
          }
        : priority;
    });

    yield put(setPopupData(popupData.data));

    const { data: exists } = yield call(checkEmailExists, email);
    const password = demoUser ? 'demo1234' : yield call(randomPassword);

    const existingAnswers = exists[0];
    const emailPriorities = priorityMapper(
      usersAnswers.userFinancialPriorities.map(userPriority =>
        prioritiesWithBespokeLinks.find(prio => prio.id === userPriority.id)
      )
    );

    if (!demoUser) {
      const employerDisabledEmail = yield select(checkIfEmployerDisabledEmail);

      if (existingAnswers) {
        yield call(callToUpdateAnswers, usersAnswers, employerName);
        if (!employerDisabledEmail) {
          yield fork(callToSendEmail, {
            email,
            template,
            tokens: { priorities: emailPriorities },
          });
          yield put(emailSentSuccess());
        }
      } else {
        yield call(
          callToSaveAnswers,
          {
            ...usersAnswers,
            password,
            email,
          },
          employerName
        );

        const canLogin = yield select(checkIfUserCanLogin);
        if (!employerDisabledEmail) {
          yield fork(callToSendEmail, {
            email,
            template,
            tokens: canLogin
              ? { password, priorities: emailPriorities }
              : { priorities: emailPriorities },
          });
          yield call(
            callToUpdateAnswers,
            { email, emailSent: true },
            employerName
          );
          yield put(emailSentSuccess());
        }
      }
    } else {
      yield call(
        existingAnswers ? callToUpdateAnswers : callToSaveAnswers,
        {
          ...usersAnswers,
          password,
        },
        employerName
      );
    }
    yield put(setLoggedIn());
  } catch (error) {
    yield put(setError('We were unable to save your answers.'));
  }
}

export default function* tipsSaga() {
  yield takeLatest(SAVE_ANSWERS, saveAnswers);
}
