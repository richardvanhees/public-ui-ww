import test from 'tape';
import * as actions from './actions.js';

test('TipsActions', t => {
  let result;

  result = actions.saveAnswers();
  t.deepEqual(result, { type: actions.SAVE_ANSWERS }, 'saveAnswers');

  result = actions.emailSentSuccess();
  t.deepEqual(result, { type: actions.EMAIL_SENT }, 'emailSentSuccess');

  result = actions.setPopupData('popupData');
  t.deepEqual(
    result,
    { type: actions.POPUP_DATA_RETRIEVED, data: 'popupData' },
    'setPopupData'
  );

  result = actions.setOptInEmail('testEmail');
  t.deepEqual(
    result,
    { type: actions.SET_OPT_IN_EMAIL, email: 'testEmail' },
    'setOptInEmail'
  );

  result = actions.setOptIn(true);
  t.deepEqual(result, { type: actions.SET_OPT_IN, optIn: true }, 'setOptIn');

  result = actions.setOptInSuccess();
  t.deepEqual(result, { type: actions.SET_OPT_IN_SUCCESS }, 'setOptInSuccess');

  result = actions.setError({ error: 'testError' });
  t.deepEqual(
    result,
    { type: actions.SET_ERROR, error: 'testError' },
    'setError'
  );

  t.end();
});
