import R from 'ramda';

export const emailSent = R.path(['tips', 'emailSent']);
export const optInEmail = R.path(['tips', 'optInEmail']);

