export const SAVE_ANSWERS = 'tips/SAVE_ANSWERS';
export const EMAIL_SENT = 'tips/EMAIL_SENT';
export const POPUP_DATA_RETRIEVED = 'tips/POPUP_DATA_RETRIEVED';
export const SET_OPT_IN = 'tips/SET_OPT_IN';
export const SET_PRIZE_DRAW_OPT_IN = 'tips/SET_PRIZE_DRAW_OPT_IN';
export const SET_OPT_IN_EMAIL = 'tips/SET_OPT_IN_EMAIL';
export const SET_OPT_IN_SUCCESS = 'tips/SET_OPT_IN_SUCCESS';
export const SET_ERROR = 'tips/SET_ERROR';
export const SUBMIT = 'tips/SUBMIT';

export const saveAnswers = () => ({
  type: SAVE_ANSWERS,
});

export const submit = () => ({
  type: SUBMIT,
});

export const emailSentSuccess = () => ({
  type: EMAIL_SENT,
});

export const setPopupData = data => ({
  type: POPUP_DATA_RETRIEVED,
  data,
});

export const setOptInEmail = email => ({
  type: SET_OPT_IN_EMAIL,
  email,
});

export const setOptIn = optIn => ({
  type: SET_OPT_IN,
  optIn,
});

export const prizeDrawOptIn = optIn => ({
  type: SET_PRIZE_DRAW_OPT_IN,
  optIn,
});

export const setOptInSuccess = () => ({
  type: SET_OPT_IN_SUCCESS,
});

export const setError = ({ error }) => ({
  type: SET_ERROR,
  error,
});
