import test from 'tape';
import { emailSent, optInEmail } from './selector.js';

test('TipsSelector', assert => {
  assert.plan(1);

  const result = emailSent({
    tips: {
      emailSent: false,
      optInEmail: '',
    },
  });

  assert.false(result, 'emailSent');
});

test('TipsSelector', assert => {
  assert.plan(1);

  const result = optInEmail({
    tips: {
      emailSent: false,
      optInEmail: '',
    },
  });

  assert.false(result, '', 'optInEmail');
});
