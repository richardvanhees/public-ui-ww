import test from 'tape';
import * as actions from './actions.js';
import { prioritiesTypes } from './types';

test('PrioritiesActions', t => {
  let result;

  result = actions.getPriorities();
  t.deepEqual(
    result,
    { type: prioritiesTypes.GET_PRIORITIES },
    'getPriorities'
  );

  result = actions.getPrioritiesSuccess({ priorities: 'testData', version: 1 });
  t.deepEqual(
    result,
    {
      type: prioritiesTypes.GET_PRIORITIES_SUCCESS,
      priorities: 'testData',
      version: 1,
    },
    'getPrioritiesSuccess'
  );

  result = actions.getPrioritiesFailure('error');
  t.deepEqual(
    result,
    { type: prioritiesTypes.GET_PRIORITIES_FAILURE, error: 'error' },
    'getPrioritiesFailure'
  );

  t.end();
});
