import test from 'tape';
import reducer from './reducer.js';
import { prioritiesTypes } from './types';

test('PrioritiesReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: prioritiesTypes.GET_PRIORITIES_SUCCESS,
      priorities: 'testData',
      version: 1,
    }
  );
  t.deepEqual(
    result,
    { priorities: 'testData', version: 1 },
    'GET_PRIORITIES_SUCCESS'
  );

  result = reducer(
    {},
    { type: prioritiesTypes.GET_PRIORITIES_FAILURE, error: 'error' }
  );
  t.deepEqual(result, { error: 'error' }, 'GET_PRIORITIES_FAILURE');

  t.end();
});
