import { prioritiesTypes } from './types';

const initialState = {
  priorities: [],
  version: 0,
};

const reducer = {
  [prioritiesTypes.GET_PRIORITIES_SUCCESS]: (state, action) => ({
    ...state,
    priorities: action.priorities,
    version: action.version,
  }),
  [prioritiesTypes.GET_PRIORITIES_FAILURE]: (state, action) => ({
    ...state,
    error: action.error,
  }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
