import test from 'tape';
import selector from './selector.js';

test('PrioritiesSelector', t => {
  const result = selector({
    priorities: {
      priorities: [],
      version: 2,
    },
    login: {
      prioritiesVersion: 2,
    },
  });

  t.deepEqual(result, {
    priorities: [],
    version: 2,
    employerPrioritiesVersion: 2,
  });

  t.end();
});
