import { prioritiesTypes } from './types';

export const getPriorities = () => ({
  type: prioritiesTypes.GET_PRIORITIES,
});

export const getPrioritiesSuccess = data => ({
  type: prioritiesTypes.GET_PRIORITIES_SUCCESS,
  priorities: data.priorities,
  version: data.version,
});

export const getPrioritiesFailure = error => ({
  type: prioritiesTypes.GET_PRIORITIES_FAILURE,
  error,
});
