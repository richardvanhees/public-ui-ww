import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const priorities = state => R.path(['priorities', 'priorities'], state);
const version = state => R.path(['priorities', 'version'], state);
const employerPrioritiesVersion = state =>
  R.path(['login', 'prioritiesVersion'], state);

export default createStructuredSelector({
  priorities,
  version,
  employerPrioritiesVersion,
});
