import '../../assets/styles/common/index.scss';
import '../../assets/styles/money-wizard/index.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import ReactGA from 'react-ga';
import { configureStore } from './store/configure-store';
import WealthWizards from '../../modules/wealthwizards';
import App from './app';
import { push } from 'react-router-redux';

const history = createHistory();

ReactGA.initialize(WealthWizards.GA_TRACKING_ID);
ReactGA.pageview(window.location.pathname + window.location.search);

const rootEl = document.getElementById('app');

history.listen(location => {
  rootEl.scrollIntoView();
  ReactGA.pageview(location.pathname + location.search);
});

const store = configureStore(history);

ReactDOM.render(<App store={store} history={history} />, rootEl);

const pathChunks = window.location.pathname.split('/');

const employerHash = pathChunks.length >= 3 ? pathChunks[2] : '';
const page = pathChunks.length >= 4 ? pathChunks[3] : '';

if (page !== 'login') {
  store.dispatch(
    push(`${WealthWizards.CONTEXT_ROUTE}/${employerHash}/questionnaire/1`)
  );
}
