import React from 'react';
import { SortableElement } from 'react-sortable-hoc';

export default SortableElement(({ position, label }) => (
  <li className="priorities__item" data-test={`priority-item-${position}`}>
    <span
      className={`priorities__item-number ${
        position > 3 ? 'priorities__item-number--faded' : ''
      }`}
    >
      {position}
    </span>
    <span className="priorities__item-label">{label}</span>
  </li>
));
