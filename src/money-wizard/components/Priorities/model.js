import PropTypes from 'prop-types';

export default {
  priorities: PropTypes.array.isRequired,
  prioritySuggestion: PropTypes.object.isRequired,
  questionnaire: PropTypes.object.isRequired,
  breakpoint: PropTypes.string,
  isLoading: PropTypes.bool,
  setUserOrder: PropTypes.func.isRequired,
};
