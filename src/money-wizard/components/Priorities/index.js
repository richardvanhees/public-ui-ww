import { connect } from 'react-redux';
import PrioritiesComponent from './component';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import {
  setUserOrder,
  setResetOrder,
  generatePrioritySuggestion,
} from '../../modules/generate/index';
import WealthWizards from '../../../../modules/wealthwizards';

const mapStateToProps = ({ generate, priorities, questionnaire, browser, login }) => ({
  prioritySuggestion: generate,
  contextRoute: WealthWizards.CONTEXT_ROUTE,
  isLoading: generate.isLoading,
  resetOrder: generate.resetOrder,
  priorities: priorities.priorities || [],
  questionnaire,
  breakpoint: browser.breakpoint,
  loggedIn: login.loggedIn,
  firstUse: login.firstUse,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      generatePrioritySuggestion,
      setUserOrder,
      setResetOrder,
      push,
    },
    dispatch
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PrioritiesComponent)
);
