import React, { Component } from 'react';
import { arrayMove } from 'react-sortable-hoc';
import WealthWizards from '../../../../modules/wealthwizards';
import model from './model';
import Header from '../../../../modules/elements/Header/index';
import Spinner from '../../../../modules/elements/Spinner/index';
import Popup from '../../../../modules/elements/Popup/index';
import SortableList from './SortableList';
import PageNavigation from '../../../../modules/elements/PageNavigation/index';

export default class PrioritiesComponent extends Component {
  static propTypes = model;
  static getDerivedStateFromProps = ({ prioritySuggestion }) => {
    if (prioritySuggestion) {
      return ({ priorities: prioritySuggestion.priorities });
    }
    return null;
  };

  constructor(props) {
    super(props);
    const { userOrder } = props.prioritySuggestion;

    this.state = {
      priorities: props.priorities,
      prioritySuggestion: userOrder,
    };
  }


  componentDidMount() {
    this.props.generatePrioritySuggestion();
  }

  componentDidUpdate = () => {
    const { priorities } = this.props.prioritySuggestion;

    if (!this.state.prioritySuggestion.length && priorities.length) {
      this.setState({
        prioritySuggestion: priorities,
      });
    }

    if (this.props.resetOrder) {
      this.props.setUserOrder(priorities);
      this.props.setResetOrder();
      this.setState({
        prioritySuggestion: priorities,
      });
    }
  };

  onSortEnd = ({ oldIndex, newIndex }) => {
    const newPrios = arrayMove(
      this.state.prioritySuggestion,
      oldIndex,
      newIndex
    );
    this.props.setUserOrder(newPrios);

    this.setState({
      prioritySuggestion: newPrios,
    });
  };

  determineNextPage = () => (this.props.loggedIn ? 'tips' : 'register');

  goToNextPage = () => {
    this.props.push(
      `${WealthWizards.CONTEXT_ROUTE}/${
        this.props.match.params.employer
        }/${this.determineNextPage()}`
    );
  };

  goBackToQuestions = () => {
    this.props.history.goBack();
  };

  togglePopup = () => {
    this.popup.toggle();
  };

  render() {
    const content = this.props.isLoading ? (
      <Spinner />
    ) : (
      <div className="priorities-container__content">
        <div
          className="priorities-container__intro"
          data-test="priorities-description"
        >
          Based on what you've told us, here are the financial priorities which
          could be important to you
        </div>
        <div className="priorities-container__intro priorities-container__intro--emphasized">
          You can drag and drop to re-order these
        </div>

        <div className="priorities-container__drag-container">
          <div className="priorities-container__tooltip">
            <button
              className="priorities-container__tooltip-link"
              data-test="priorities-help-trigger"
              onClick={this.togglePopup}
            >
              What do these priorities mean?
            </button>
          </div>

          <div className="priorities">
            <SortableList
              items={this.state.prioritySuggestion}
              onSortEnd={this.onSortEnd}
              pressDelay={
                this.props.breakpoint === 'phablet' ||
                this.props.breakpoint === 'stablet' ||
                this.props.breakpoint === 'ltablet'
                  ? 100
                  : 0
              }
              helperClass="priorities__item--dragged"
            />
          </div>

          <PageNavigation
            next={this.goToNextPage}
            back={this.goBackToQuestions}
            nextLabel={'Get tips'}
            noSideMargins
            testNext={'priorities'}
            testBack={'priorities'}
          />
        </div>

        <Popup
          ref={c => {
            this.popup = c;
          }}
          title={'What do these priorities mean?'}
          closeButtonTop
          closeClickOutside
          layout={'column'}
          buttons={[
            {
              label: 'Close',
              onClick: this.togglePopup,
              className: 'priorities-container__btn-popup-close',
            },
          ]}
        >
          {this.props.priorities
            .filter(prio =>
              this.state.prioritySuggestion.find(sug => sug.id === prio.id)
            )
            .sort((a, b) => {
              const orderId1 = this.state.prioritySuggestion.find(
                sug => sug.id === a.id
              ).orderId;
              const orderId2 = this.state.prioritySuggestion.find(
                sug => sug.id === b.id
              ).orderId;
              return orderId1 < orderId2 ? -1 : 1;
            })
            .map((priority, i) => (
              <div
                key={`popup-priority-${i}`}
                className={'priorities__popup-item'}
              >
                <div className={'priorities__popup-item-header'}>
                  {priority.name}
                </div>
                <div className={'priorities__popup-item-description'}>
                  {priority.short_description}
                </div>
              </div>
            ))}
        </Popup>
      </div>
    );

    return (
      <div className="priorities-container">
        <Header />
        {content}
      </div>
    );
  }
}
