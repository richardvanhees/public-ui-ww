import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PrioritiesComponent from './component';
import Spinner from '../../../../modules/elements/Spinner/index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  priorities: [],
  loggedIn: true,
  match: {
    params: {
      employer: 'test',
    },
  },
  prioritySuggestion: {
    userOrder: [{ a: 1 }, { b: 2 }, { c: 3 }],
    priorities: [{ a: 1 }, { b: 2 }, { c: 3 }],
  },
  questionnaire: {
    userInput: {},
  },
  setUserInput: () => {},
  generatePrioritySuggestion: () => {},
  setUserOrder: () => {},
};

test('<PrioritiesComponent>', t => {
  t.equals(
    typeof PrioritiesComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.equals(
    shallow(
      <PrioritiesComponent {...fakeRequiredProps} isLoading />
    ).find(Spinner).length,
    1,
    'Spinner shown at load'
  );

  t.equals(
    shallow(
      <PrioritiesComponent {...fakeRequiredProps} isLoading={false} />
    ).find(Spinner).length,
    0,
    'Spinner hidden after load'
  );

  const wrapper = shallow(<PrioritiesComponent {...fakeRequiredProps} />);
  wrapper.instance().onSortEnd({ oldIndex: 1, newIndex: 0 });
  t.deepEqual(
    wrapper.state().prioritySuggestion,
    [{ b: 2 }, { a: 1 }, { c: 3 }],
    "OnSortEnd method saves the user's input in the state"
  );

  t.end();
});
