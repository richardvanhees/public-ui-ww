import React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
import SortableItem from './SortableItem';

export default SortableContainer(({ items }) => (
  <ul className="priorities__list">
    {items.map((prio, index) => (
      <SortableItem
        key={`item-${index}`}
        index={index}
        position={index + 1}
        label={prio.name}
      />
    ))}
  </ul>
));
