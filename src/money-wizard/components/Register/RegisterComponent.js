import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import HeaderIcon from '../../../../assets/images/answer-images/5964_Moreish_WW_Illust_Q3_Spending-10.png';
import Button from '../../../../modules/elements/Button/index';
import Error from '../../../../modules/elements/Error/index';
import validator from 'validator';
import Input from '../../../../modules/elements/Input/index';
import Header from '../../../../modules/elements/Header/index';
import Checkbox from '../../../../modules/elements/Checkbox/index';
import Footer from '../../../../modules/elements/Footer/index';

export default class RegisterComponent extends PureComponent {
  static propTypes = {
    push: PropTypes.func,
    history: PropTypes.object,
    privacyPolicyAgreed: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      password: '',
    };
  }

  goToPriorities = () => {
    const {
      setError,
      email,
      privacyPolicyAgreed,
      resetError,
      registerUser,
    } = this.props;

    if (!validator.isEmail(email)) {
      setError('Enter a valid email address');
    } else if (!privacyPolicyAgreed) {
      setError(
        'You need to state that you have read and agree before you can start'
      );
    } else {
      resetError();
      registerUser();
    }
  };

  handleChange = event => {
    this.props.setEmail(event.target.value.toLowerCase());
  };

  showEmployerMessage = () =>
    this.props.match.params.employer !== '53bce4f1dfa0fe8e7ca126f91b35d3a6';

  render() {
    return (
      <div className="register">
        <div className="register__logo">
          <Header />
          <img
            className="register__logo-src"
            src={HeaderIcon}
            data-test="logo"
            alt="Money Wizard"
          />
        </div>

        <div
          className="register__container"
          style={{ height: this.props.showError ? 365 : '' }}
        >
          <div className="register__form">
            <h1 className="register__header register__header--strong">
              Enter your email to continue
            </h1>
            <p className="register__sub-header">
              You'll unlock your money score and get tools to help you feel in
              control of your finances!
            </p>
            <Input
              type={'email'}
              dataTest={'email-input'}
              wrapperClassName={'register__input-wrapper'}
              inputClassName={'register__input'}
              hintText={'Work or personal email'}
              onChange={this.handleChange}
              autoFocus
            />

            {this.showEmployerMessage() && (
              <p className="register__policy">
                We'll store your email and answers to the questions,<br />
                but{' '}
                <span className="register__policy--strong">
                  your employer won't see these
                </span>
              </p>
            )}
            <Checkbox
              label={
                <span className="checkbox__label">
                  I agree and have read the{' '}
                  <a
                    className="checkbox__label-url"
                    href="https://www.wealthwizards.com/privacy-policy"
                    target="blank"
                  >
                    privacy policy
                  </a>
                </span>
              }
              checked={this.props.privacyPolicyAgreed}
              onChange={() => this.props.togglePrivacyPolicy()}
              name={'privacy-checkbox'}
              dataTest={'privacy-checkbox'}
              type={'custom'}
              className="register__checkbox"
            />

            <Error
              description={this.props.errorMessage}
              active={this.props.showError}
            />

            <Button
              label={<p className="btn__label btn__label--primary">Continue</p>}
              className="register__btn btn--primary"
              dataTest="start-btn"
              onClick={this.goToPriorities}
            />
          </div>
        </div>
        <Footer showWWLogo={false} />
      </div>
    );
  }
}
