import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RegisterComponent from './RegisterComponent';
import sinon from 'sinon';
import Button from '../../../../modules/elements/Button/index';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const pushStub = sandbox.stub();

const fakeRequiredProps = {
  push: pushStub,
  match: {
    params: {
      employer: 'fakeEmployer',
    },
  },
};

test('<RegisterComponent>', t => {
  t.equals(
    typeof RegisterComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.equals(
    mount(
      <RegisterComponent {...fakeRequiredProps} loginMethod={'register'} />
    ).find('.register__input').length,
    1,
    'An e-mail input is rendered'
  );

  t.equals(
    mount(
      <RegisterComponent {...fakeRequiredProps} loginMethod={'register'} />
    ).find('Checkbox').length,
    1,
    'A confirmation checkbox is rendered'
  );

  t.end();
});
