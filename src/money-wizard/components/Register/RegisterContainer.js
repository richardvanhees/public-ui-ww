import { connect } from 'react-redux';
import RegisterComponent from './RegisterComponent';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  togglePrivacyPolicy,
  setError,
  setEmail,
  resetError,
  registerUser,
  setEmployerHash,
} from '../../modules/login/actions';
import WealthWizards from '../../../../modules/wealthwizards';

const mapStateToProps = ({ login }) => ({
  privacyPolicyAgreed: login.privacyPolicyAgreed,
  showError: login.showError,
  errorMessage: login.errorMessage,
  email: login.email,
  loggedIn: login.loggedIn,
  employerHash: login.employerHash,
  firstUse: login.firstUse,
  contextRoute: WealthWizards.CONTEXT_ROUTE,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      togglePrivacyPolicy,
      setError,
      resetError,
      setEmail,
      registerUser,
      push,
      setEmployerHash,
    },
    dispatch
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RegisterComponent)
);
