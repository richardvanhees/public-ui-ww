import React, { PureComponent } from 'react';
import { event } from 'react-ga';
import PropTypes from 'prop-types';
import WealthWizards from '../../../../modules/wealthwizards';

import Header from '../../../../modules/elements/Header/index';
import Tip from './Tip';

import PageNavigation from '../../../../modules/elements/PageNavigation/index';
import OptInPopup from '../../../../modules/elements/OptInPopup/index';
import Popup from '../../../../modules/elements/Popup/index';
import reactHTMLParser from 'react-html-parser';
import R from 'ramda';
import Button from '../../../../modules/elements/Button';
import Input from '../../../../modules/elements/Input';
import Dropdown from '../../../../modules/elements/Dropdown';
import Error from '../../../../modules/elements/Error';

const tipsCount = 3;

export default class TipsComponent extends PureComponent {
  static propTypes = {
    saveAnswers: PropTypes.func.isRequired,
    userOrder: PropTypes.array.isRequired,
    priorities: PropTypes.array.isRequired,
    bespokeLinks: PropTypes.array.isRequired,
    handleFieldChange: PropTypes.func,
    sendPatMail: PropTypes.func,

    setOptInEmail: PropTypes.func,
    optIn: PropTypes.bool,
    optInChecked: PropTypes.bool,
    prizeDrawChecked: PropTypes.bool,
    disablePopup: PropTypes.bool,
    error: PropTypes.string,
    over55: PropTypes.bool,
  };

  static getDerivedStateFromProps = (props, state) => {
    if (props.error !== state.error && !props.optInSet) {
      return { error: props.error };
    }
    if (props.optInSet) {
      return { optInSet: props.optInSet, error: null };
    }
    return null;
  };

  constructor(props) {
    super(props);

    this.state = {
      error: props.error,
      optIn: props.optIn,
      optInSet: props.optInSet,
      prizeDrawOptIn: props.prizeDrawOptIn,
      optInEmail: props.email,
      submit: false,
      disablePopup: props.disablePopup,
      errors: {
        name: false,
        phone: false,
        contactTime: false,
        adviceArea: false,
      },
    };
  }

  componentDidMount() {
    this.props.saveAnswers();
    this.props.setOptInEmail(this.props.email);
    if (this.props.optInSet === null && !this.props.disablePopup) {
      setTimeout(this.popup.toggle, WealthWizards.TIPS_POPUP_TIME_TO_APPEAR);
    }
  }

  selectPriorities = () =>
    this.props.userOrder
      .slice(0, tipsCount)
      .map(item => this.props.priorities.find(prio => item.id === prio.id))
      .map(prio => {
        const bespokeLinks = this.props.bespokeLinks.filter(
          bespokeLink => bespokeLink.priorityId === prio.id
        );

        return bespokeLinks.length
          ? {
            ...prio,
            tipsLinks: bespokeLinks,
          }
          : prio;
      });

  componentDidUpdate = prevProps => {
    if (prevProps.optInSet !== this.state.optInSet) {
      this.popup.toggle();
    }
  };

  optIn = val => {
    if (
      this.state.optInEmail.match(
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
      )
    ) {
      this.props.optIn(val);
      event({
        category: 'MW e-mail opt-in',
        action: val ? 'User opted in' : 'User declined to opt in',
      });
    } else {
      this.setState({ error: 'Enter a valid email' });
    }
  };

  prizeDrawOptIn = val => {
    if (
      this.state.optInEmail.match(
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
      )
    ) {
      this.props.prizeDrawOptIn(val);
      event({
        category: 'MW secondary opt-in',
        action: val ? 'User opted in' : 'User declined to opt in',
      });
    } else {
      this.setState({ error: 'Enter a valid email' });
    }
  };

  setOptInEmail = email => {
    this.setState({ optInEmail: email });
    this.props.setOptInEmail(email);
  };

  validatePatDetails = () => {
    const { name, phone, contactTime, adviceArea } = this.props.pat;
    const isValidNumber = /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/;

    const validationResults = {
      name: !(name && !!name.trim()) ? 'Name is required' : false,
      phone: !(isValidNumber.test(phone) && phone.length >= 11 && phone.length <= 20) ? 'Enter a valid phone number' : false,
      contactTime: !(contactTime && !!contactTime.trim()) ? 'Contact time is required' : false,
      adviceArea: !adviceArea ? 'Advice area is required' : false,
    };

    this.setState({
      errors: {
        ...this.state.errors,
        ...validationResults,
      },
    });

    return Object.values(validationResults).filter(result => result === false).length === Object.keys(validationResults).length;
  };

  patCheck = async () => {
    const canContinue = this.validatePatDetails();

    if (canContinue) {
      await this.props.sendPatMail();
      this.patboostPopup.toggle();
      this.confirmPatPopup.toggle();
      await event({
        category: 'PAT Boost',
        action: 'User gave personal details for PAT',
      });
    }
  };

  render = () => {
    const {
      userOrder,
      priorities,
      history,
      popupData,
      email,
      submit,
      questionnaire,
      optInChecked,
      prizeDrawChecked,
      handleFieldChange,
      patMailSent,
      canContactPAT,
      over55,
    } = this.props;
    const { errors } = this.state;

    return (
      <div className="tips">
        <Header />

        <div
          className="tips__content-container"
          style={{ width: 400 * tipsCount }}
        >
          <div
            className="tips__header tips__header--strong"
            data-test="tips-header-description"
          >
            Based on what you told us, here are handy tips and tools we've found
            that could help you feel more in control of your finances
          </div>

          <div className="tips__subheader">
            You're one step away from your Money Score
          </div>

          {canContactPAT && over55 && <div className="tips__pat">
            <div className="tips__pat-text">
              <div className="tips__pat-title">Did you know £100k in your pension pot today may only give you £3k a year to live on in the future?</div>
              <div className="tips__pat-subtitle">Get financial advice now, your future self will thank you.</div>
            </div>
            <div className="tips__pat-cta">
              <Button
                onClick={() => patMailSent ? this.confirmPatPopup.toggle() : this.patboostPopup.toggle()}
                label={'Book a call'}
                type={'secondary'}
              />
            </div>
          </div>}

          <div className="tips__content">
            <div className="tips__items-container">
              {userOrder &&
              priorities &&
              this.selectPriorities().map((item, i) => (
                <Tip
                  key={`tip-${item.id}`}
                  tipNumber={i + 1}
                  {...item}
                  userAnswers={questionnaire.data}
                  {...this.props}
                />
              ))}
            </div>

            <div className="tips__button-bar">
              <PageNavigation
                back={history.goBack}
                testBack={'tips'}
                next={this.props.generateScoreSuggestion}
                testNext={'dashboard'}
              />
            </div>
          </div>
        </div>

        <OptInPopup
          ref={c => {
            this.popup = c;
          }}
          {...popupData}
          error={this.state.error}
          email={email}
          action={this.optIn}
          optInChecked={optInChecked}
          prizeDrawChecked={prizeDrawChecked}
          prizeDrawOptInAction={this.prizeDrawOptIn}
          onChange={this.setOptInEmail}
          reverse={popupData && popupData.type === 2}
          closeAndSubmit={submit}
          termsAndConditionsLinkText={
            popupData && popupData.termsAndConditionsLinkText
          }
          openTermsAndConditions={() =>
            this.termsAndConditionsPopup &&
            this.termsAndConditionsPopup.toggle()
          }
        />

        <Popup
          ref={c => {
            this.termsAndConditionsPopup = c;
          }}
          closeButtonTop
          closeClickOutside
          transparentBackground
          title={popupData && popupData.termsAndConditionsTitle}
          buttons={[
            {
              label: 'Close',
              onClick: () => {
                this.termsAndConditionsPopup.toggle();
              },
            },
          ]}
        >
          {R.compose(
            text => reactHTMLParser(text),
            R.pathOr('', ['props', 'popupData', 'termsAndConditionsText'])
          )(this)}
        </Popup>

        <Popup
          ref={c => {
            this.confirmPatPopup = c;
          }}
          isNarrow
          closeButtonTop
          closeClickOutside
          title={'Thank you'}
          buttons={[
            {
              label: 'Close',
              onClick: () => this.confirmPatPopup.toggle(),
            },
          ]}
        >
          We will be in touch.
        </Popup>

        <Popup
          ref={c => {
            this.patboostPopup = c;
          }}
          closeButtonTop
          className={'pat-popup'}
          centeredButtons
          layout={'column'}
          title={'Let\'s get a few more details from you...'}
          buttons={[{
            label: 'Book a call',
            type: 'secondary',
            onClick: this.patCheck,
          }]}
        >
          <div className="tips__pat-popup-row">
            <div className="tips__pat-popup-label">Your name</div>
            <Input
              wrapperClassName="tips__pat-popup-input"
              hintText={'Name'}
              onChange={e => handleFieldChange({ name: e.target.value })}
              error={errors.name}
            />
          </div>
          <Error
            active={errors.name}
            title={errors.name}
            compact
            className="tips__pat-popup-error"
          />

          <div className="tips__pat-popup-row">
            <div className="tips__pat-popup-label">Telephone number</div>
            <Input
              wrapperClassName="tips__pat-popup-input"
              hintText={'Telephone number'}
              onChange={e => handleFieldChange({ phone: e.target.value })}
              error={errors.phone}
            />
          </div>
          <Error
            active={errors.phone}
            title={errors.phone}
            compact
            className="tips__pat-popup-error"
          />

          <div className="tips__pat-popup-row">
            <div className="tips__pat-popup-label">What is the best time to call?</div>
            <Input
              wrapperClassName="tips__pat-popup-input"
              hintText={'When are you free?'}
              onChange={e => handleFieldChange({ contactTime: e.target.value })}
              error={errors.contactTime}
            />
          </div>
          <Error
            active={errors.contactTime}
            title={errors.contactTime}
            compact
            className="tips__pat-popup-error"
          />

          <div className="tips__pat-popup-row">
            <div className="tips__pat-popup-label">What is the main area you'd like advice on?</div>
            <Dropdown
              className="tips__pat-popup-dropdown"
              options={['Retirement planning', 'Pensions', 'Investments', 'Protection', 'Estate planning', 'Other']}
              dataTest={'pat-advice-area'}
              onChange={val => handleFieldChange({ adviceArea: val })}
            />
          </div>
          <Error
            active={errors.adviceArea}
            title={errors.adviceArea}
            compact
            className="tips__pat-popup-error"
          />

        </Popup>
      </div>
    );
  }
}
