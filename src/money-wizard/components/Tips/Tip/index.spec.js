import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Tip, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

import { expectedPriorities } from '../TipsComponent.spec';

const fakeRequiredProps = {
  index: 0,
  tipNumber: 1,
  userAnswers: {
    0: { answerId: 4 },
  },
  longDescription: expectedPriorities[0].long_description,
  linkHeader: expectedPriorities[0].link_header,
  links: [
    {
      id: 0,
      links: [
        {
          label: 'Save or invest?',
          url:
            'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
        },
        {
          label: 'Savings calculator',
          url:
            'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
        },
      ],
    },

  ],
  ...expectedPriorities[0],
  alternative_descriptions: [
    {
      triggered_by: ["q0a4"],
      excludedFeatureFlags: ["canContactPAT"],
      short_description: "alternative short description",
      long_description: "alternative long description"
    }
  ],
};

test('<Tip>', t => {
  t.plan(5);
  __RewireAPI__.__Rewire__('WealthWizards', { ALTERNATIVE_PRIORITY_DESCRIPTIONS: true });

  t.equals(typeof Tip.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(<Tip {...fakeRequiredProps} list={['item', 'item2']} />).find(
      '.tip__list'
    ).length,
    1,
    'List is shown if list has entries'
  );

  t.equals(
    shallow(<Tip {...fakeRequiredProps} list={[]} />).find('.tip__list').length,
    0,
    'List is hidden if list has no entries'
  );

  t.equals(
    shallow(<Tip {...fakeRequiredProps} />).find('.tip__description').text(),
    'alternative long description',
    'Alternative description rendered'
  );

  t.equals(
    shallow(<Tip {...fakeRequiredProps} canContactPAT={false} />).find('.tip__description').text(),
    'long description 2',
    'Alternative description is blocked when feature flag is excluded'
  );

  t.end();
});
