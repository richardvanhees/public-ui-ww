import PropTypes from 'prop-types';

export const propTypes = {
  tipNumber: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  long_description: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
    .isRequired,
  list: PropTypes.array,
  link_header: PropTypes.string.isRequired,
  top_tip: PropTypes.string,
  alternative_descriptions: PropTypes.array,
  userAnswers: PropTypes.object.isRequired,
  canContactPAT: PropTypes.bool,
};

export const defaultProps = {
  canContactPAT: true,
};
