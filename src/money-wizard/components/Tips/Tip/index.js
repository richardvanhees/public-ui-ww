import React, { PureComponent } from 'react';
import WealthWizards from '../../../../../modules/wealthwizards';

import * as images from '../../../../../assets/images/answer-images/index';
import { propTypes, defaultProps } from './model';
import interpolate from '../../../../../modules/utils/interpolate';

export default class Tip extends PureComponent {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.state = {
      description: props.long_description,
    };
  }

  componentDidMount() {
    const { alternative_descriptions: alternativeDescriptions, userAnswers } = this.props;

    if (WealthWizards.ALTERNATIVE_PRIORITY_DESCRIPTIONS && alternativeDescriptions && alternativeDescriptions.length) {
      alternativeDescriptions.forEach(({ triggered_by: triggeredBy, long_description: longDescription, excludedFeatureFlags }) => {
        const disabled = excludedFeatureFlags.every(flag => this.props[flag] === false);

        !disabled && triggeredBy.forEach(trigger => {
          const q = parseInt(trigger.split(/q|a/g)[1], 10);
          const a = parseInt(trigger.split(/q|a/g)[2], 10);

          if (userAnswers[q].answerId === a) {
            this.setState({
              description: interpolate(longDescription),
            });
          }
        });
      });
    }
  }

  render() {
    const { image, tipNumber, name, list, tipsLinks: links } = this.props;
    const topTip = this.props.top_tip;
    const { description } = this.state;

    return (
      <div className="tip">
        <div className="tip__image">
          <img className="tip__image-src" src={images[image]} alt={name} />
        </div>

        <div className="tip__content">
          <div className="tip__header">
            <div className="tip__index">{tipNumber}</div>
            <div className="tip__header-label" data-test={`tip-${tipNumber}`}>
              {name}
            </div>
          </div>

          <div className="tip__top-tip">
            <span className="tip__top-tip-label">Top tip: </span>
            <span className="tip__top-tip-value">{topTip}</span>
          </div>

          <div className="tip__description">
            {description}
            {list &&
            !!list.length && (
              <ul className={'tip__list'}>
                {list.map((item, i) => (
                  <li key={`tip-list-item-${i}`} className={'tip__list-item'}>
                    {item}
                  </li>
                ))}
              </ul>
            )}
          </div>

          <div className="tip__links">
            <div className="tip__links-header">Top tool{links.length > 1 ? 's' : ''}</div>
            <ul className="tip__links-container">
              {links.map((link, i) => (
                <li className="tip__link-item" key={`link-${i}`}>
                  <a href={link.url} target="_blank">
                    {link.caption}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
