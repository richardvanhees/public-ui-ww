import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TipsComponent, { __RewireAPI__ } from './TipsComponent';
import Tip from './Tip/index';
import OptInPopup from '../../../../modules/elements/OptInPopup/index';
import Button from '../../../../modules/elements/Button/index';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const setOptInEmailStub = sandbox.stub();
const optInStub = sandbox.stub();
const goBackStub = sandbox.stub();
const saveAnswersStub = sandbox.stub();
const submitStub = sandbox.stub();

configure({ adapter: new Adapter() });

export const priorities = [
  {
    id: 0,
    image: 'image 1',
    link_header: 'link header 1',
    tipsLinks: [{ label: 'link label 1', url: 'link url 1' }],
    list: [],
    long_description: 'long description 1',
    name: 'name 1',
    short_description: 'short description 1',
    alternative_descriptions: [],
  },
  {
    id: 1,
    image: 'image 2',
    link_header: 'link header 2',
    tipsLinks: [{ label: 'link label 2', url: 'link url 2' }],
    list: [],
    long_description: 'long description 2',
    name: 'name 2',
    short_description: 'short description 2',
    alternative_descriptions: [],
  },
  {
    id: 2,
    image: 'image 3',
    link_header: 'link header 3',
    tipsLinks: [{ label: 'link label 3', url: 'link url 3' }],
    list: [],
    long_description: 'long description 3',
    name: 'name 3',
    short_description: 'short description 3',
    alternative_descriptions: [],
  },
];

export const expectedPriorities = [
  {
    id: 1,
    image: 'image 2',
    link_header: 'link header 2',
    tipsLinks: [{ label: 'link label 2', url: 'link url 2' }],
    list: [],
    long_description: 'long description 2',
    name: 'name 2',
    short_description: 'short description 2',
    alternative_descriptions: [],
  },
  {
    id: 2,
    image: 'image 3',
    link_header: 'link header 3',
    tipsLinks: [{ label: 'link label 3', url: 'link url 3' }],
    list: [],
    long_description: 'long description 3',
    name: 'name 3',
    short_description: 'short description 3',
    alternative_descriptions: [],
  },
  {
    id: 0,
    image: 'image 1',
    link_header: 'link header 1',
    tipsLinks: [{ label: 'link label 1', url: 'link url 1' }],
    list: [],
    long_description: 'long description 1',
    name: 'name 1',
    short_description: 'short description 1',
    alternative_descriptions: [],
  },
];
export const expectedPrioritiesWithBespokeLinks = [
  {
    id: 1,
    image: 'image 2',
    link_header: 'link header 2',
    tipsLinks: [
      { caption: 'bespoke caption 1', priorityId: 1, url: 'bespoke url 1' },
    ],
    list: [],
    long_description: 'long description 2',
    name: 'name 2',
    short_description: 'short description 2',
    alternative_descriptions: [],
  },
  {
    id: 2,
    image: 'image 3',
    link_header: 'link header 3',
    tipsLinks: [{ label: 'link label 3', url: 'link url 3' }],
    list: [],
    long_description: 'long description 3',
    name: 'name 3',
    short_description: 'short description 3',
    alternative_descriptions: [],
  },
  {
    id: 0,
    image: 'image 1',
    link_header: 'link header 1',
    tipsLinks: [{ label: 'link label 1', url: 'link url 1' }],
    list: [],
    long_description: 'long description 1',
    name: 'name 1',
    short_description: 'short description 1',
    alternative_descriptions: [],
  },
];

const fakeRequiredProps = {
  questionnaire: {
    data: [],
  },
  priorities: priorities,
  loggedIn: true,
  setOptInEmail: setOptInEmailStub,
  optIn: optInStub,
  submit: submitStub,
  userOrder: [{ id: 1 }, { id: 2 }, { id: 0 }, { id: 4 }, { id: 3 }],
  bespokeLinks: [],
  history: { goBack: goBackStub },
  saveAnswers: saveAnswersStub,
  popupTimeout: 0,
  email: 'test@test.com',
};

test('<TipsComponent>', t => {
  const component = mount(<TipsComponent {...fakeRequiredProps} />);

  t.equals(typeof TipsComponent.propTypes, 'object', 'PropTypes are defined');

  t.deepEqual(
    component.instance().selectPriorities(),
    expectedPriorities,
    'SelectPriorities is correctly slicing and merging'
  );

  t.equals(component.find(Tip).length, 3, 'Component shows 3 tips');

  component
    .find(OptInPopup)
    .find(Button)
    .first()
    .simulate('click');

  t.equals(
    submitStub.called,
    true,
    'Opt-in is set when button in popup is clicked'
  );

  t.end();
});

test('<TipsComponent>', assert => {
  const fakeProps = {
    ...fakeRequiredProps,
    popupData: {
      termsAndConditionsTitle: 'termsAndConditionsTitle',
      termsAndConditionsText: '<p>main terms and conditions</p>',
    },
  };

  const component = mount(<TipsComponent {...fakeProps} />);

  const popup = component.find('Popup').at(0);

  assert.equals(
    popup.prop('title'),
    'termsAndConditionsTitle',
    'tandc title set'
  );
  assert.true(
    popup
      .children()
      .html()
      .indexOf('main terms and conditions') > -1,
    'conditions set'
  );

  assert.end();
});

test('<TipsComponent>', assert => {
  const fakeProps = {
    ...fakeRequiredProps,
    bespokeLinks: [
      {
        priorityId: 1,
        url: 'bespoke url 1',
        caption: 'bespoke caption 1',
      },
    ],
  };

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    component.instance().selectPriorities(),
    expectedPrioritiesWithBespokeLinks,
    'SelectPriorities is correctly replacing links with bespoke links'
  );

  assert.end();
});

test('<TipsComponent>', assert => {
  assert.plan(2);

  const sandbox = sinon.sandbox.create();

  const toggleStub = sandbox.stub();

  __RewireAPI__.__Rewire__('WealthWizards', {
    TIPS_POPUP_TIME_TO_APPEAR: 3000,
  });

  const fakeProps = {
    ...fakeRequiredProps,
    optInSet: null,
    toggle: toggleStub,
    setOptInEmail: () => ({}),
    saveAnswers: () => ({}),
  };

  const setTimeoutBak = global.setTimeout;

  global.setTimeout = sandbox.stub();

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    global.setTimeout.callCount,
    1,
    'set timeout triggered to show popup'
  );
  assert.deepEqual(
    global.setTimeout.args[0][1],
    3000,
    'set timeout triggered after 3000ms to show popup'
  );

  global.setTimeout = setTimeoutBak;
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<TipsComponent> - pop up does not show when flag set', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const toggleStub = sandbox.stub();

  const fakeProps = {
    ...fakeRequiredProps,
    optInSet: null,
    disablePopup: true,
    toggle: toggleStub,
    setOptInEmail: () => ({}),
    saveAnswers: () => ({}),
  };

  const setTimeoutBak = global.setTimeout;

  global.setTimeout = sandbox.stub();

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    global.setTimeout.callCount,
    0,
    'set timeout NOT triggered due to disablePopup flag being true'
  );

  global.setTimeout = setTimeoutBak;
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<TipsComponent> - show banner when canContactPAT is true', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const toggleStub = sandbox.stub();

  const fakeProps = {
    ...fakeRequiredProps,
    optInSet: null,
    disablePopup: true,
    toggle: toggleStub,
    setOptInEmail: () => ({}),
    saveAnswers: () => ({}),
    canContactPAT: true,
    over55: true,
  };

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    component.find('.tips__pat').length,
    1,
    'Banner is loaded'
  );
});

test('<TipsComponent> - no banner when user is not over 55', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const toggleStub = sandbox.stub();

  const fakeProps = {
    ...fakeRequiredProps,
    optInSet: null,
    disablePopup: true,
    toggle: toggleStub,
    setOptInEmail: () => ({}),
    saveAnswers: () => ({}),
    canContactPAT: true,
    over55: false,
  };

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    component.find('.tips__pat').length,
    0,
    'Banner is not loaded'
  );
});

test('<TipsComponent> - no banner when canContactPAT is false', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const toggleStub = sandbox.stub();

  const fakeProps = {
    ...fakeRequiredProps,
    optInSet: null,
    disablePopup: true,
    toggle: toggleStub,
    setOptInEmail: () => ({}),
    saveAnswers: () => ({}),
    canContactPAT: false,
    over55: true,
  };

  const component = mount(<TipsComponent {...fakeProps} />);

  assert.deepEqual(
    component.find('.tips__pat').length,
    0,
    'Banner is not loaded'
  );
});
