import { connect } from 'react-redux';
import R from 'ramda';
import TipsComponent from './TipsComponent';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { saveAnswers, setOptIn, setOptInEmail, prizeDrawOptIn, submit } from '../../modules/tips/actions';
import { handleFieldChange, sendPatMail } from '../../modules/pat/actions';
import WealthWizards from '../../../../modules/wealthwizards';
import { generateScoreSuggestion } from '../../modules/generate/index';

const mapStateToProps = ({ generate, priorities, login, tips, questionnaire, pat }) => ({
  userOrder: generate.userOrder,
  contextRoute: WealthWizards.CONTEXT_ROUTE,
  priorities: priorities.priorities,
  email: login.email,
  bespokeLinks: login.bespokeLinks,
  loggedIn: login.loggedIn,
  firstUse: login.firstUse,
  popupData: tips.popup,
  error: tips.error,
  optIn: tips.optIn,
  optInChecked: tips.optIn,
  prizeDrawChecked: tips.prizeDrawOptIn,
  optInSet: tips.optInSet,
  prizeDrawOptIn: tips.prizeDrawOptIn,
  disablePopup: login.disablePopup,
  questionnaire,
  canContactPAT: login.canContactPAT,
  pat,
  patMailSent: pat.mailSent,
  over55: R.path(['data', '0', 'answerId'], questionnaire) === 4,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      saveAnswers,
      setOptInEmail,
      optIn: setOptIn,
      prizeDrawOptIn,
      push,
      generateScoreSuggestion,
      submit,
      handleFieldChange,
      sendPatMail,
    },
    dispatch
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(TipsComponent)
);
