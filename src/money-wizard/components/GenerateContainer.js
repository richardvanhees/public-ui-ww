import { connect } from 'react-redux';
import GenerateComponent from './GenerateComponent';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import {
  generatePrioritySuggestion,
  setUserInput,
} from '../modules/generate/index';

const mapStateToProps = state => ({
  priorities: state.generate.priorities,
  userInput: state.generate.userInput,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      push,
      generatePrioritySuggestion,
      setUserInput,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(GenerateComponent);
