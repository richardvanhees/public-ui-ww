import React, { PureComponent } from 'react';
import Logo from '../../../assets/images/logos/logo.png';
import PropTypes from 'prop-types';

const returnPriorityList = data =>
  Object.keys(data).map((arr, i) => (
    <li ref={i}>
      {data[arr].name} ({data[arr].score})
    </li>
  ));

export default class GenerateComponent extends PureComponent {
  static propTypes = {
    priorities: PropTypes.array.isRequired,
    userInput: PropTypes.object.isRequired,
    generatePrioritySuggestion: PropTypes.func.isRequired,
    setUserInput: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div className="generate">
        <div className="generate__logo">
          <img
            className="generate__logo-src"
            src={Logo}
            data-test="logo"
            alt="Money Wizard"
          />
        </div>

        <textarea
          className="generate__textarea"
          onChange={e => this.props.setUserInput(JSON.parse(e.target.value))}
        />

        <div className="generate__textarea">
          <ol>{returnPriorityList(this.props.priorities)}</ol>
        </div>

        <div
          className="generate__btn btn btn--primary"
          onClick={() => this.props.generatePrioritySuggestion()}
        >
          <p className="btn__label btn__label--primary">Submit</p>
        </div>
      </div>
    );
  }
}
