import PropTypes from 'prop-types';

export default {
  questionnaire: PropTypes.object.isRequired,
  getQuestions: PropTypes.func.isRequired,
  goToPage: PropTypes.func.isRequired,
  loggedIn: PropTypes.bool.isRequired,
  canLogin: PropTypes.bool.isRequired,
  showMW2BetaLink: PropTypes.bool.isRequired,
};
