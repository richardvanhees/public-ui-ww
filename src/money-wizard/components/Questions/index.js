import { connect } from 'react-redux';
import QuestionsComponent from './QuestionsComponent';
import WealthWizards from '../../../../modules/wealthwizards';
import { push } from 'react-router-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { getQuestions, setAnswer } from '../../modules/questionnaire/index';
import { getEmployer, setFirstUse } from '../../modules/login';

const mapStateToProps = ({ questionnaire, login }) => ({
  questionnaire,
  loggedIn: login.loggedIn,
  employerHash: login.employerHash,
  contextRoute: WealthWizards.CONTEXT_ROUTE,
  firstUse: login.firstUse,
  isLoading: login.isLoading,
  canLogin: login.canLogin,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getQuestions,
      setAnswer,
      push,
      getEmployerData: getEmployer,
      setFirstUse,
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(QuestionsComponent)
);
