import React, { Component } from 'react';
import model from './model';
import Header from '../../../../modules/elements/Header/index';
import QuestionRange from '../../../../modules/elements/QuestionRange/index';
import Spinner from '../../../../modules/elements/Spinner';

// eslint-disable-next-line react/prefer-stateless-function
export default class QuestionComponent extends Component {
  static propTypes = model;

  componentDidMount() {
    this.props.getEmployerData(this.props.match.params.employer);
  }

  render() {
    const {
      questionnaire,
      setAnswer,
      contextRoute,
      isLoading,
      match,
      push,
      canLogin,
      loggedIn,
    } = this.props;

    if (isLoading) {
      return <Spinner />;
    }

    let navigation = null;
    if (canLogin && !loggedIn) {
      navigation = {
        buttons: [
          {
            label: 'Sign in',
            type: 'outline',
            onClick: () => {
              push(`${contextRoute}/${match.params.employer}/login`);
            },
          },
        ],
      };
    }

    return (
      <div className="wrapper">
        <Header navigation={navigation} />
        <div className="questions-component__question-container">
          <QuestionRange
            {...this.props}
            section={'questionnaire'}
            questions={questionnaire.questions}
            nextPage={`${contextRoute}/${match.params.employer}/priorities`}
            onChange={setAnswer}
            noImageStacked
            answerPairCount={2}
          />
        </div>
      </div>
    );
  }
}
