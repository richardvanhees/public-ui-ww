import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import QuestionsComponent from './QuestionsComponent';

configure({ adapter: new Adapter() });

import { questions } from '../../../../server/money-wizard/db/setup/questions_v1';

const fakeRequiredProps = {
  loggedIn: true,
  questionnaire: {
    questions: questions.questions,
    userInput: {},
  },
  contextRoute: 'test',
  getQuestions: () => {},
};

test('<QuestionsComponent>', t => {
  t.equals(
    typeof QuestionsComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.end();
});
