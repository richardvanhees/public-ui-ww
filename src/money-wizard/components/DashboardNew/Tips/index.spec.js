import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import HeaderComponent from './index';
import Tips from '../Tips';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const fakeButtonStub = sandbox.stub();
const homeStub = sandbox.stub();

const fakeData = {
  priorities: [
    {
      link_header: 'link header 1',
      list: [],
      long_description: 'long description 1',
      short_description: 'short description 1',
      image: 'image',
      name: 'name 1',
      id: 0,
      top_tip: 'top tip 1',
    },
    {
      link_header: 'link header 2',
      list: [],
      long_description: 'long description 2',
      short_description: 'short description 2',
      image: 'image',
      name: 'name 2',
      id: 1,
      top_tip: 'top tip 2',
    }, {
      link_header: 'link header 3',
      list: [],
      long_description: 'long description 3',
      short_description: 'short description 3',
      image: 'image',
      name: 'name 3',
      id: 2,
      top_tip: 'top tip 3',
    },
  ],
  tips: [
    {
      id: 0,
      name: 'tip 1',
      score: 16,
      orderId: 0,
    },
    {
      id: 1,
      name: 'tip 2',
      score: 7,
      orderId: 2,
    },
    {
      id: 2,
      name: 'tip 3',
      score: 10,
      orderId: 1,
    }
  ]
};

test('<Tips>', t => {
  t.equals(typeof Tips.propTypes, 'object', 'PropTypes are defined');
  t.end();
});

test('<Tips>', t => {
  const component = mount(<Tips {...fakeData} />);

  t.deepEquals(component.state().tips, [ { id: 1, name: 'tip 2', orderId: 2, priorityData: { id: 1, image: 'image', link_header: 'link header 2', list: [], long_description: 'long description 2', name: 'name 2', short_description: 'short description 2', top_tip: 'top tip 2' }, score: 7, userOrder: 2 }, { id: 0, name: 'tip 1', orderId: 0, priorityData: { id: 0, image: 'image', link_header: 'link header 1', list: [], long_description: 'long description 1', name: 'name 1', short_description: 'short description 1', top_tip: 'top tip 1' }, score: 16, userOrder: 1 }, { id: 2, name: 'tip 3', orderId: 1, priorityData: { id: 2, image: 'image', link_header: 'link header 3', list: [], long_description: 'long description 3', name: 'name 3', short_description: 'short description 3', top_tip: 'top tip 3' }, score: 10, userOrder: 3 } ], 'Values are swapped correctly');

  t.end();
});
