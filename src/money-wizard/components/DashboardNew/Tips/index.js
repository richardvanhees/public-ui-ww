import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as images from '../../../../../assets/images/answer-images';

/* eslint-disable react/no-multi-comp */
class TipCard extends Component {
  static propTypes = {
    tip: PropTypes.object.isRequired,
    blockName: PropTypes.string.isRequired,
  };

  constructor() {
    super();

    this.state = {
      hovered: false,
    };
  }

  render() {
    const { tip, blockName } = this.props;
    const { hovered } = this.state;

    return (
      <div
        data-test={`tip-${tip.userOrder}`}
        className={`${blockName}__tip ${tip.userOrder === 1 ? `${blockName}__tip--large` : ''} ${hovered ? `${blockName}__tip--hover` : ''}`}
        onMouseEnter={() => {
          this.setState({ hovered: true });
        }}
        onMouseLeave={() => {
          this.setState({ hovered: false });
        }}
      >
        <div className={`${blockName}__tip-number`}>{tip.userOrder}</div>
        <div className={`${blockName}__tip-title ${tip.userOrder === 1 ? `${blockName}__tip-title--large` : ''} ${hovered ? `${blockName}__tip-title--hover` : ''}`}>{tip.name}</div>
        <div className={`${blockName}__tip-content ${hovered ? `${blockName}__tip-content--hover` : ''}`}>
          <div className={`${blockName}__tip-image ${hovered ? `${blockName}__tip-image--hidden` : ''}`}><img alt={tip.name} src={images ? images[tip.priorityData.image] : ''} /></div>
          <div className={`${blockName}__tip-description ${hovered ? `${blockName}__tip-description--visible` : ''}`}>{tip.priorityData.short_description}</div>
        </div>
      </div>
    );
  }
}

export default class MoneyScore extends Component {
  static propTypes = {
    tips: PropTypes.array.isRequired,
    priorities: PropTypes.array.isRequired,
    isNarrow: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const values = props.isNarrow ? this.setMobileValues(props.tips.slice(0, 3)) : this.swapArrayValues(props.tips.slice(0, 3));

    this.state = {
      isNarrow: props.isNarrow,
      tips: values.map(tip => ({ ...tip, priorityData: props.priorities.find(prio => prio.id === tip.id) })),
    };
  }

  componentDidUpdate = () => {
    if (this.props.isNarrow !== this.state.isNarrow) {
      const values = this.props.isNarrow ? this.setMobileValues(this.props.tips.slice(0, 3)) : this.swapArrayValues(this.props.tips.slice(0, 3));

      this.setState({
        isNarrow: this.props.isNarrow,
        tips: values.map(tip => ({ ...tip, priorityData: this.props.priorities.find(prio => prio.id === tip.id) })),
      });
    }
  };

  setMobileValues = ([a, b, c]) => [{ userOrder: 1, ...a }, { userOrder: 2, ...b }, { userOrder: 3, ...c }];
  swapArrayValues = ([a, b, c]) => [{ userOrder: 2, ...b }, { userOrder: 1, ...a }, { userOrder: 3, ...c }];

  render() {
    const blockName = 'money-score-tips';
    const { tips } = this.state;

    return (
      <div className={blockName}>
        <div className={`${blockName}__title`}>This is the stuff you need to work on</div>

        <div className={`${blockName}__tips-container`}>
          {tips.map(tip => <TipCard key={`tip-${tip.id}`} blockName={blockName} tip={tip} />)}
        </div>
      </div>
    );
  }
}
