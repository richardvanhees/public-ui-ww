import React, { Component } from 'react';
import Header from '../../../../modules/elements/Header/index';
import PropTypes from 'prop-types';
import WealthWizards from '../../../../modules/wealthwizards';
import Notification from '../../../../modules/elements/Notification';
import MoneyScore from './MoneyScore';
import Carousel from './Carousel';
import Tips from './Tips';
import ReactGA from 'react-ga';

const content = {
  customText:
    'Try our new version 🚀 <span class="notification__link">Switch to Money Wizard 2.0</span> (beta)',
  action: () => {
    ReactGA.event({
      category: 'Money Wizard Beta',
      action: 'User clicked Money Wizard 2.0 beta link',
    });
    window.location = WealthWizards.MONEY_WIZARD_BETA_URL;
  },
};

export default class DashboardComonent extends Component {
  static propTypes = {
    tips: PropTypes.object,
    userOrder: PropTypes.array,
    priorities: PropTypes.array,
    push: PropTypes.func,
    match: PropTypes.object,
    moneyScore: PropTypes.number,
    loggedIn: PropTypes.bool,
    firstUse: PropTypes.bool,
    isNarrow: PropTypes.bool,
    showMW2BetaLink: PropTypes.bool.isRequired,
    contextRoute: PropTypes.string,
    employerName: PropTypes.string,
    generateScoreSuggestion: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    notification: PropTypes.string.isRequired,
    closeNotification: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    props.showMW2BetaLink && props.showNotification('', 'warning', 20000);
  }

  retake = () => {
    this.props.push(
      `${WealthWizards.CONTEXT_ROUTE}/${
        this.props.match.params.employer
      }/questionnaire/1`
    );
  };

  render() {
    const {
      loggedIn,
      moneyScore,
      userOrder,
      priorities,
      isNarrow,
      notification,
      closeNotification,
    } = this.props;

    return loggedIn ? (
      <div className="wrapper">
        <Notification
          {...notification}
          close={closeNotification}
          customContent={content}
          flash={false}
        />
        <Header shareButtons />
        <div className="dashboard-new">
          <MoneyScore moneyScore={moneyScore} retake={this.retake} />
          <Tips tips={userOrder} priorities={priorities} isNarrow={isNarrow} />
          <Carousel {...this.props} isNarrow={isNarrow} />
        </div>
      </div>
    ) : (
      <div />
    );
  }
}
