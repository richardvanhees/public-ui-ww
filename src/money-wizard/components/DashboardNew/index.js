import { connect } from 'react-redux';
import DashboardComponent from './DashboardComponent';
import WealthWizards from '../../../../modules/wealthwizards';
import { push } from 'react-router-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { generateScoreSuggestion } from '../../modules/generate/index';
import {
  resetNotification,
  setNotification,
} from '../../../../modules/browser';

const mapStateToProps = ({
  questionnaire,
  generate,
  login,
  browser,
  priorities,
}) => ({
  tips: questionnaire,
  userOrder: generate.userOrder,
  priorities: priorities.priorities,
  isNarrow:
    browser.breakpoint === 'phablet' || browser.breakpoint === 'stablet',
  moneyScore: generate.score,
  contextRoute: WealthWizards.CONTEXT_ROUTE,
  loggedIn: login.loggedIn,
  firstUse: login.firstUse,
  employerName: login.employerName,
  canContactPAT: login.canContactPAT,
  notification: browser.notification,
  showMW2BetaLink: login.showMW2BetaLink,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      generateScoreSuggestion,
      push,
      closeNotification: resetNotification,
      showNotification: (msg, type, flash) => setNotification(msg, type, flash),
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DashboardComponent)
);
