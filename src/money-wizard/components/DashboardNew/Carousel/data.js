/* eslint-disable */
import YT from '../../../../../assets/images/mw-images/youtube.png';
import Pat from '../../../../../assets/images/mw-images/pat.png';
import Acorn from '../../../../../assets/images/mw-images/acorn.png';

export const data = ({ canContactPAT }) => [
  {
    title: 'Personal Advice Service',
    icon: null,
    image: Pat,
    disabled: !canContactPAT,
    text: 'Speak directly to a financial planner',
    action: 'https://mailchi.mp/wealthwizards/personal-advice',
  },
  {
    title: 'Savings Calculator',
    icon: 'chart-line',
    image: '',
    text: 'Are you saving for a future bill Or just to have a bit in the bank?',
    action: 'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
  },
  {
    title: 'Rainy Day Fund',
    icon: 'umbrella',
    image: '',
    text: 'Are you saving for a rainy day?',
    action:
      'https://www.moneyadviceservice.org.uk/en/articles/emergency-savings-how-much-is-enough',
  },
  {
    title: 'Investment Wizard - coming soon',
    icon: null,
    image: Acorn,
    text: 'Online advice about the best way to invest your money.',
  },
  {
    title: 'Money Vlogs - coming soon',
    icon: null,
    image: YT,
    text: 'Hear money tips from colleagues with the top money scores.',
  },
];
