import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Carousel from 'nuka-carousel';
import { data } from './data';
import Icon from '../../../../../modules/elements/Icon/index';
import ReactGA from 'react-ga';

export default class CarouselContainer extends Component {
  static propTypes = {
    isNarrow: PropTypes.bool,
    employerName: PropTypes.bool,
    canContactPAT: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      slideIndex: 0,
    };

    this.processedData = data({ canContactPAT: this.props.canContactPAT }).filter(item => !item.disabled);
  }

  render() {
    const slides = this.props.isNarrow ? 1 : 3;
    return (
      <div className="carousel">
        <div className="carousel__header">
          Here are some tools you may find helpful
        </div>
        <div className="carousel__wrapper">
          <Carousel
            slideIndex={this.state.slideIndex}
            afterSlide={slideIndex => this.setState({ slideIndex })}
            slidesToShow={slides}
            cellAlign="left"
            dragging
            renderCenterLeftControls={({ previousSlide }) => (
              <div
                className={`carousel__btn carousel__btn--left ${
                  this.state.slideIndex === 0 ? 'carousel__btn--disabled' : ''
                  }`}
                onClick={previousSlide}
              >
                <Icon
                  name="angle-left"
                  className={`carousel__btn--with-link ${
                    this.state.slideIndex === 0 ? 'carousel__btn--disabled' : ''
                    }`}
                />
              </div>
            )}
            renderCenterRightControls={({ nextSlide }) => (
              <div
                className={`carousel__btn carousel__btn--right ${
                  this.state.slideIndex >= this.processedData.length - slides
                    ? 'carousel__btn--disabled'
                    : ''
                  }`}
                onClick={nextSlide}
              >
                <Icon
                  name="angle-right"
                  className={`carousel__btn--with-link ${
                    this.state.slideIndex >= this.processedData.length - slides
                      ? 'carousel__btn--disabled'
                      : ''
                    }`}
                />
              </div>
            )}
          >
            {this.processedData.map((item, i) => (
              <div
                className="carousel__item"
                key={i}
                data-test={`carousel-item-${i}`}
                onClick={() => {
                  if (item.action) {
                    ReactGA.event({
                      category: 'MW dashboard new',
                      action: `user clicked ${item.title}`,
                    });
                    window.open(`${item.action}`);
                  }
                }}
              >
                <p className="carousel__title">{item.title}</p>
                <div className="carousel__row">
                  {item.icon && (
                    <div className="carousel__icon">
                      <Icon
                        name={item.icon}
                        className="carousel__icon--with-link"
                      />
                    </div>
                  )}
                  {item.image && (
                    <div className="carousel__image">
                      <img
                        className="carousel__image--src"
                        src={item.image}
                        data-test={item.title}
                        alt={item.title}
                      />
                    </div>
                  )}
                  <p className="carousel__text carousel__text--side">
                    {item.text}
                  </p>
                </div>
              </div>
            ))}
          </Carousel>
        </div>
      </div>
    );
  }
}
