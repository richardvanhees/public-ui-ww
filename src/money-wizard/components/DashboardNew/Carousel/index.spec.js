import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Carousel, { __RewireAPI__ } from './index';

import { Provider } from 'react-redux';

configure({ adapter: new Adapter() });

test('<Carousel>', t => {
  t.plan(2);

  __RewireAPI__.__Rewire__('WealthWizards', { PAT_DISABLED: 'impellam' });

  const target = shallow(<Carousel isNarrow />);

  t.equals(typeof Carousel.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    target.find('.carousel__item').length,
    4,
    'correct amount of items rendered'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<Carousel>', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('WealthWizards', { PAT_DISABLED: 'impellam' });

  const target = shallow(<Carousel employerName="impellam" />);

  assert.equals(
    target.find('Carousel').children().length,
    4,
    '4 in carousel when employer = impellum'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<Carousel>', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('WealthWizards', { PAT_DISABLED: 'impellam' });

  const target = shallow(<Carousel employerName="demo" />);

  assert.equals(
    target.find('Carousel').children().length,
    4,
    '5 in carousel when employer = demo'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});
