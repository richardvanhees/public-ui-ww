import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PieChart from '../../../../../modules/elements/PieChart';
import Button from '../../../../../modules/elements/Button';

export const getLevelDefinitions = score => {
  if (score < 35) return { color: '#7C0A02', label: 'needs work', pronoun: '' };
  if (score < 90) return { color: '#FADA5E', label: 'fair', pronoun: 'is ' };
  return { color: '#006d12', label: 'excellent', pronoun: 'is ' };
};

export default class MoneyScore extends PureComponent {
  static propTypes = {
    moneyScore: PropTypes.number.isRequired,
    retake: PropTypes.func.isRequired,
  };

  render() {
    const blockName = 'money-score-new';
    const { moneyScore, retake } = this.props;
    const {
      color: moneyColor,
      label: moneyLabel,
      pronoun: moneyPronoun,
    } = getLevelDefinitions(moneyScore);

    return (
      <div className={blockName}>
        <div className={`${blockName}__chart-container`}>
          <PieChart
            className={`${blockName}__chart`}
            data={[
              { color: '#F0F0F0', label: 'path', value: 100 - moneyScore },
              { color: moneyColor, label: 'Money Score', value: moneyScore },
            ]}
            showTooltipOnHover={false}
            size={300}
          />
          <div className={`${blockName}__content`}>
            <div
              className={`${blockName}__score`}
              style={{ color: moneyColor }}
              data-test="money-score"
            >
              {moneyScore}
            </div>
            <div className={`${blockName}__score-text-wrapper`}>
              <span
                data-test="money-score-pre-desc"
                className={`${blockName}__score-text`}
              >
                Your money score {`${moneyPronoun}`}
              </span>
              <span
                className={`${blockName}__score-level`}
                style={{ color: moneyColor }}
              >
                {moneyLabel}
              </span>
            </div>
            <div className={`${blockName}__retake`}>
              <Button
                onClick={retake}
                label={'Retake'}
                dataTest={'retake-btn'}
              />
            </div>
          </div>
        </div>
        <div className={`${blockName}__description`}>
          <p>
            Your Money Score is a simple indicator based on what you have told
            us. Increase the score by using the tips and tools, and by retaking
            the Money Wizard survey if your financial position has improved.
          </p>
          <p>
            You can find several tools below that can help you increase your
            money score.
          </p>
        </div>
      </div>
    );
  }
}
