import MoneyScore, { __RewireAPI__, getLevelDefinitions } from './index';
import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';

const testConfig = [
  { score: 34, expected: 'needs work' },
  { score: 35, expected: 'fair' },
  { score: 89, expected: 'fair' },
  { score: 90, expected: 'excellent' },
];

testConfig.forEach(testCase => {
  test(`getLevelDefinitions - ${JSON.stringify(testCase)}`, assert => {
    assert.plan(1);
    assert.equal(getLevelDefinitions(testCase.score).label, testCase.expected);
  });
});

test('money score', assert => {
  assert.plan(1);

  const FakePieChart = props => <div>{props.children}</div>;

  __RewireAPI__.__Rewire__('PieChart', FakePieChart);

  const sandbox = sinon.sandbox.create();

  const retakeStub = sandbox.stub();

  const component = mount(<MoneyScore moneyScore={90} retake={retakeStub} />);

  assert.equal(
    component.find('span[data-test="money-score-pre-desc"]').text(),
    'Your money score is ',
    'the text prior to showing the numeric score value is correct for label = excellent'
  );

  __RewireAPI__.__ResetDependency__('PieChart');
});

test('money score', assert => {
  assert.plan(1);

  const FakePieChart = props => <div>{props.children}</div>;

  __RewireAPI__.__Rewire__('PieChart', FakePieChart);

  const sandbox = sinon.sandbox.create();

  const retakeStub = sandbox.stub();

  const component = mount(<MoneyScore moneyScore={89} retake={retakeStub} />);

  assert.equal(
    component.find('span[data-test="money-score-pre-desc"]').text(),
    'Your money score is ',
    'the text prior to showing the numeric score value is correct for label = fair'
  );

  __RewireAPI__.__ResetDependency__('PieChart');
});

test('money score', assert => {
  assert.plan(1);

  const FakePieChart = props => <div>{props.children}</div>;

  __RewireAPI__.__Rewire__('PieChart', FakePieChart);

  const sandbox = sinon.sandbox.create();

  const retakeStub = sandbox.stub();

  const component = mount(<MoneyScore moneyScore={34} retake={retakeStub} />);

  assert.equal(
    component.find('span[data-test="money-score-pre-desc"]').text(),
    'Your money score ',
    'the text prior to showing the numeric score value is correct for label = needs-work'
  );

  __RewireAPI__.__ResetDependency__('PieChart');
});
