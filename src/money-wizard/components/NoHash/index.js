import React, { PureComponent } from 'react';
import Header from '../../../../modules/elements/Header/index';

export default class NoHashContainer extends PureComponent {
  render() {
    return (
      <div>
        <Header />
        <div
          style={{
            display: 'flex',
            alignContent: 'center',
            justifyContent: 'center',
            height: '50%',
          }}
        >
          <div>
            This is an invalid url. Please use the url provided by your
            employer.
          </div>
        </div>
      </div>
    );
  }
}
