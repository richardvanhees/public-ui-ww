import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Logo from '../../../../assets/images/logos/logo.png';
import Footer from '../../../../modules/elements/Footer';
import Button from '../../../../modules/elements/Button';
import Error from '../../../../modules/elements/Error';
import validator from 'validator';
import Input from '../../../../modules/elements/Input';
import WealthWizards from '../../../../modules/wealthwizards';

export default class LoginComponent extends PureComponent {
  static propTypes = {
    push: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      password: '',
    };
  }

  goToQuestions = () =>
    this.props.push(
      `${WealthWizards.CONTEXT_ROUTE}/${
        this.props.match.params.employer
      }/questionnaire/1`
    );

  login = () => {
    const {
      setError,
      email,
      setEmployerHash,
      resetError,
      loginRequest,
      match,
    } = this.props;

    if (!validator.isEmail(email)) {
      setError('Enter a valid email address');
    } else if (!this.state.password.trim()) {
      setError('Enter a password');
    } else {
      setEmployerHash(match.params.employer);
      resetError();
      loginRequest(this.state.password);
    }
  };

  handleChange = event => this.props.setEmail(event.target.value.toLowerCase());

  setFirstUse = firstUse => {
    this.props.resetError();
    this.props.setFirstUse(firstUse);
  };

  render = () => (
    <div className="login">
      <div className="login__logo">
        <img
          className="login__logo-src"
          src={Logo}
          data-test="logo"
          alt="Money Wizard"
        />
      </div>

      <div className="login__usp-container usp">
        <div className="usp__item">
          <p className="usp__label">
            Feel in control of your finances in less than five minutes
          </p>
        </div>
      </div>

      <div
        className="login__container"
        style={{ height: this.props.showError ? 365 : '' }}
      >
        <div className="login__form">
          <h1 className="login__header">Enter your details to continue</h1>

          <Input
            type={'email'}
            dataTest={'email-input'}
            wrapperClassName={'login__input-wrapper login__email-input-wrapper'}
            inputClassName={'login__input'}
            hintText={'Work or personal email'}
            onChange={this.handleChange}
          />

          <Input
            type={'password'}
            dataTest={'password-input'}
            wrapperClassName={'login__input-wrapper'}
            inputClassName={'login__input'}
            hintText={'Password'}
            onChange={e => {
              this.setState({ password: e.target.value });
            }}
          />

          <Error
            description={this.props.errorMessage}
            active={this.props.showError}
          />

          <Button
            label={<p className="btn__label btn__label--primary">Sign in</p>}
            className="login__btn btn--primary"
            dataTest="start-btn"
            onClick={this.login}
          />

          <div
            className="login__switch"
            onClick={() => {
              this.setFirstUse(true);
              this.goToQuestions();
            }}
          >
            <span className={'login__switch-text'}>
              I don't have an account
            </span>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
