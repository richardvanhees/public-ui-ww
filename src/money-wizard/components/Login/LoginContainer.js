import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import {
  togglePrivacyPolicy,
  setError,
  setEmail,
  setFirstUse,
  resetError,
  loginRequest,
  setEmployerHash,
  getEmployer,
} from '../../modules/login';

const mapStateToProps = ({ login }) => ({
  privacyPolicyAgreed: login.privacyPolicyAgreed,
  showError: login.showError,
  errorMessage: login.errorMessage,
  email: login.email,
  firstUse: login.firstUse,
  isLoading: login.isLoading,
  canLogin: login.canLogin,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      togglePrivacyPolicy,
      setError,
      resetError,
      setEmail,
      setFirstUse,
      loginRequest,
      push,
      setEmployerHash,
      getEmployerData: getEmployer,
    },
    dispatch
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
);
