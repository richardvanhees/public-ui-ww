import { avatarMan } from '../../../assets/images/money-wizard-beta';

export default {
  john: {
    imgPath: avatarMan,
    name: 'John',
  },
};
