import { call } from 'redux-saga/effects';
import configureStoreBoilerplate from '../../../modules/boilerplate/configure-store';

import browserReducer from '../../../modules/browser';

import contentReducer, {
  getContentRequestSaga,
} from '../../../modules/content';
import todoReducer, {
  getTodosSaga,
  openExternalLinkSaga,
} from '../modules/todos';
import sidebarReducer from '../modules/sidebar';
import logoutSaga from '../modules/login/logout-saga';
import getChatSaga from '../modules/chat/get-chat-saga';
import chatReducer from '../modules/chat/reducer';
import factFindReducer from '../modules/fact-find/reducer';
import registerUserSaga from '../modules/chat/register-user-saga';
import saveChatSaga from '../modules/chat/save-chat-saga';
import saveChatAnswerSaga from '../modules/chat/save-chat-answer-saga';
import triggerApiCallSaga from '../modules/chat/trigger-api-call-saga';
import startIntercomSaga from '../modules/sidebar/start-intercom-saga';
import factFindSaga from '../modules/fact-find/get-fact-find-saga';
import documentsReducer from '../modules/documents/reducer';
import downloadReportSaga from '../modules/documents/download-document-saga';

export const configureStore = (history, initialState = {}) =>
  configureStoreBoilerplate(
    {
      browser: browserReducer,
      content: contentReducer,
      todos: todoReducer,
      chat: chatReducer,
      sidebar: sidebarReducer,
      factFind: factFindReducer,
      documents: documentsReducer,
    },
    [
      call(getContentRequestSaga),
      call(getTodosSaga),
      call(logoutSaga),
      call(getChatSaga),
      call(registerUserSaga),
      call(saveChatSaga),
      call(saveChatAnswerSaga),
      call(triggerApiCallSaga),
      call(startIntercomSaga),
      call(openExternalLinkSaga),
      call(factFindSaga),
      call(downloadReportSaga),
    ],
    history,
    initialState,
    []
  );
