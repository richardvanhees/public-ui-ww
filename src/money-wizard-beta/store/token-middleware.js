import WealthWizards from '../../../modules/wealthwizards';
import { getItem } from '../../../modules/utils/local-storage';

const middleware = () => next => action => {
  const jwt = getItem(WealthWizards.TOKEN_KEY_NAME);
  if (!jwt) {
    return window.location.assign(WealthWizards.LOGIN_REDIRECT);
  }

  return next(action);
};

export default middleware;
