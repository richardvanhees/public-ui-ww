import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { call, takeLatest } from 'redux-saga/effects';
import { StartIntercom } from './start-intercom-saga';
import { START_INTERCOM } from './actions';
import { boot, show } from '../../../../modules/utils/intercom';
import { push } from 'react-router-redux';

const userData = {
  email_address: 'test@wealthwizards.com',
  name: 'Jeffrey Horner',
  employer: 'Butchers',
  user_hash: 'bb62aa467dd2179b468e798a7d8d6f0062923ada4d1a8b8196a69fe1151336f4',
  user_id: '324rdt45dcfg567',
};

test('start intercom saga', t => {
  const target = testSaga(StartIntercom, {})
    .next()
    .call(boot, userData)
    .next()
    .call(show)
    .next();

  t.true(target.isDone(), 'intercom started');

  t.end();
});

test('start intercom saga', t => {
  const target = testSaga(StartIntercom, {})
    .next()
    .call(boot, userData)
    .throw('aaaa')
    .put({
      type: '@@router/CALL_HISTORY_METHOD',
      payload: { method: 'push', args: ['undefined/error'] },
    })
    .next();

  t.true(target.isDone(), 'intercom error when launching so redirect to error');

  t.end();
});
