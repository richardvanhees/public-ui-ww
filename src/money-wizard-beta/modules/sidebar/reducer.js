import { TOGGLE_SIDEBAR } from './actions';

const initialState = {
  showMenu: false,
};

const toggleSidebar = state => ({
  ...state,
  showMenu: !state.showMenu,
});

const reducer = {
  [TOGGLE_SIDEBAR]: toggleSidebar,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
