import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { START_INTERCOM } from './actions';
import { boot, show } from '../../../../modules/utils/intercom';

const userData = {
  email_address: 'test@wealthwizards.com',
  name: 'Jeffrey Horner',
  employer: 'Butchers',
  user_hash: 'bb62aa467dd2179b468e798a7d8d6f0062923ada4d1a8b8196a69fe1151336f4',
  user_id: '324rdt45dcfg567',
};

export function* StartIntercom() {
  try {
    yield call(boot, userData);
    return yield call(show);
  } catch (error) {
    return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/error`));
  }
}

export default function* StartIntercomSaga() {
  yield takeLatest(START_INTERCOM, StartIntercom);
}
