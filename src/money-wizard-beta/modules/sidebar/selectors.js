import R from 'ramda';

export const getShowmenu = R.pathOr([], ['showMenu']);
