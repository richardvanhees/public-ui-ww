export const TOGGLE_SIDEBAR = 'sidebar/TOGGLE_SIDEBAR';
export const START_INTERCOM = 'sidebar/START_INTERCOM';

export const toggleSidebar = showMenu => ({
  type: TOGGLE_SIDEBAR,
  showMenu,
});

export const startIntercom = () => ({
  type: START_INTERCOM,
});
