import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import {
  saveChat,
  callSaveChat,
  getState,
  __RewireAPI__,
} from './save-chat-saga';
import { updateCurrentQuestionIndex } from './actions';
import { setError, setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';

test('save chat saga', t => {
  const registrationChat = {
    status: 'IN_PROGRESS',
    chat: {
      questions: [
        {
          id: '1',
          content: 'Hi',
          pause: 1000,
          answerType: 'email',
          answerValue: 'test@support.com',
        },
        {
          id: '1',
          content: 'Hi',
          pause: 1000,
          answerType: 'recaptcha',
          answerValue: 'aaaaa',
        },
      ],
    },
  };

  const target = testSaga(saveChat, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: 1,
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: registrationChat,
        },
      },
    })
    .call(
      callSaveChat,
      { ...registrationChat, finalQuestionId: '1' },
      'registration'
    )
    .next()
    .put(setIsLoading(false))
    .next()
    .put(updateCurrentQuestionIndex(2))
    .next();

  t.true(target.isDone(), 'chat saved');

  t.end();
});

test('save chat saga', t => {
  const registrationChat = {
    status: 'IN_PROGRESS',
    chat: {
      questions: [
        {
          id: '1',
          content: 'Hi',
          pause: 1000,
          answerType: 'email',
          answerValue: 'test@support.com',
        },
        {
          id: '1',
          content: 'Hi',
          pause: 1000,
          answerType: 'recaptcha',
          answerValue: 'aaaaa',
        },
      ],
    },
  };

  const target = testSaga(saveChat, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: 1,
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: registrationChat,
        },
      },
    })
    .call(
      callSaveChat,
      { ...registrationChat, finalQuestionId: '1' },
      'registration'
    )
    .throw('aaaa')
    .put(setIsLoading(false))
    .next()
    .put(setError('We are currenly save the chat, please try again later.'))
    .next();

  t.true(target.isDone(), 'error dialog triggered');

  t.end();
});
