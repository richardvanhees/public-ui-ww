import { fork, takeLatest, select } from 'redux-saga/effects';
import { SAVE_CHAT_ANSWER } from './actions';
import { activeTodoId } from './selectors';
import WealthWizards from '../../../../modules/wealthwizards';
import { getItem } from '../../../../modules/utils/local-storage';
import { patch } from '../../../../modules/utils/refresh-request';

export const callSaveAnswer = (todoId, answerPayload) =>
  patch(
    `${WealthWizards.CHAT_STORE_BASE_URL}/v1/chat?todoId=${todoId}`,
    `Bearer ${getItem('user_token')}`,
    {
      data: answerPayload,
    }
  );

export const getState = state => state;

export function* saveChat({ questionId, answerValue }) {
  try {
    if (!getItem('user_token')) {
      // we don't patch when they are logged out
      return null;
    }
    const state = yield select(getState);
    const todoId = activeTodoId(state.chat);

    return yield fork(callSaveAnswer, todoId, {
      questionId,
      answerValue,
    });
  } catch (error) {
    // TODO what do we do if a patch fails
    return null;
  }
}

export default function* saveChatAnswerSaga() {
  yield takeLatest(SAVE_CHAT_ANSWER, saveChat);
}
