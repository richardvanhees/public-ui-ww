import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_CHAT, getChatSuccess } from './actions';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';
import { get } from '../../../../modules/utils/refresh-request';
import { getItem } from '../../../../modules/utils/local-storage';
import R from 'ramda';
import { push } from 'react-router-redux';
import ReactGA from 'react-ga';

export const callGetChat = path =>
  get(
    `${WealthWizards.CHAT_STORE_BASE_URL}${path}`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

export function* getChat({ todoId }) {
  if (!todoId) {
    return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/todos`));
  }

  // once logged in you should not be able to do registration again
  if (getItem('user_token') && todoId === 'registration') {
    return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/todos`));
  }

  // if not logged in you should not be able to do anything but registration
  if (!getItem('user_token') && todoId !== 'registration') {
    return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/todos`));
  }

  try {
    yield put(setIsLoading(true));

    const { data } = yield call(
      callGetChat,
      getItem('user_token')
        ? `/v1/chat?todoId=${todoId}`
        : // the first time they load the initial chat
          // they won't be registered, therefore we use this endpoint to
          // pull it back, once they give us their email, we register them
          // and then record things with the token granted to us
          `/anonymous/chat?todoId=${todoId}`
    );

    if (data.status === 'COMPLETE') {
      return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/todos`));
    }

    ReactGA.event({
      category: 'Chat loaded',
      action: `${todoId}/${data.abVersion}`,
    });

    yield put(getChatSuccess(data));
    return yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));
    const statusCode = R.path(['response', 'status'])(error);
    if (statusCode !== 401) {
      yield put(
        setError(
          'We are currenly unable to load this chat, please try again later.'
        )
      );
    }
    return null;
  }
}

export default function* getChatSaga() {
  yield takeLatest(GET_CHAT, getChat);
}
