import R from 'ramda';
import {
  GET_CHAT_SUCCESS,
  SET_ACTIVE_TODO,
  UPDATE_CURRENT_QUESTION_INDEX,
  SHOW_QUESTION_ANSWER,
  UPDATE_CHAT_STATE_WITH_ANSWER,
  SET_CURRENT_QUESTION_INDEX,
  SHOW_MESSAGE_AFTER_USER_ANSWER,
  RECAPTCHA_FAILED,
  UNDO_QUESTION,
  API_RESPONSE,
} from './actions';
import { GET_TODOS } from '../todos/actions';
import { LOGOUT } from '../login/actions';

const initialState = {
  chatsByTodoId: {},
  activeTodoId: null,
  currentQuestionIndex: 0,
  // if true this trigger the multiple choice or input to show for current Q
  showQuestionAnswer: false,
  // shows our little response to an answer if true
  showMessageAfterUserAnswer: false,
  chatPaused: false,
  replayRecaptcha: false,
  undoing: false,
};

const getChatSuccess = (state, action) => {
  let lastQuestionedAnswered = null;

  // find the last question answered
  action.chatRecord.chat.questions.forEach((question, index) => {
    if (question.answerValue !== null && question.answerValue !== undefined) {
      lastQuestionedAnswered = index;
    }
  });

  return {
    ...state,
    chatsByTodoId: {
      ...state.chatsByTodoId,
      [action.chatRecord.todoId]: action.chatRecord,
    },
    activeTodoId: action.chatRecord.todoId,
    currentQuestionIndex:
      lastQuestionedAnswered !== null ? lastQuestionedAnswered + 1 : 0,
  };
};

const resetChat = state => ({
  ...state,
  activeTodoId: null,
  currentQuestionIndex: 0,
  showQuestionAnswer: false,
  chatPaused: false,
  showMessageAfterUserAnswer: false,
  replayRecaptcha: false,
  undoing: false,
});

const setActiveTodo = (state, action) => ({
  ...state,
  activeTodoId: action.todoId,
});

const showMessageAfterUserAnswer = state => ({
  ...state,
  showMessageAfterUserAnswer: true,
  chatPaused: false,
});

const updateCurrentQuestionIndex = (state, action) => ({
  ...state,
  currentQuestionIndex: action.nextIndex,
  showQuestionAnswer: false,
  chatPaused: false,
  showMessageAfterUserAnswer: false,
  replayRecaptcha: false,
  undoing: false,
});

const setCurrentQuestionIndex = (state, action) => {
  const currentChat = state.chatsByTodoId[state.activeTodoId];

  const newIndex = R.findIndex(R.propEq('id', action.questionId))(
    currentChat.chat.questions
  );

  if (newIndex === -1) {
    console.error('Not found next question'); // eslint-disable-line no-console
  }

  return {
    ...state,
    currentQuestionIndex: newIndex,
    showQuestionAnswer: false,
    chatPaused: false,
    replayRecaptcha: false,
    undoing: false,
  };
};

const showQuestionAnswer = state => ({
  ...state,
  showQuestionAnswer: true,
  chatPaused: true,
});

const recaptchaFailed = state => {
  const newState = {
    ...state,
    replayRecaptcha: true,
    chatPaused: true,
  };

  const currentChat = newState.chatsByTodoId[state.activeTodoId];

  if (!currentChat) {
    return newState;
  }

  const indexOfRecaptchaConversation = R.findIndex(
    R.propEq('answerType', 'recaptcha')
  )(currentChat.chat.questions);

  currentChat.chat.questions = currentChat.chat.questions.map(
    (question, index) => {
      if (index >= indexOfRecaptchaConversation) {
        return {
          ...question,
          answerValue: null,
        };
      }
      return question;
    }
  );

  return {
    ...newState,
    currentQuestionIndex: indexOfRecaptchaConversation,
  };
};

const undoQuestion = (state, action) => {
  const newState = {
    ...state,
    chatPaused: true,
    undoing: true,
    showMessageAfterUserAnswer: false,
    showQuestionAnswer: false,
  };

  const currentChat = newState.chatsByTodoId[state.activeTodoId];

  if (!currentChat) {
    return newState;
  }

  const indexOfQuestionToUndo = R.findIndex(R.propEq('id', action.questionId))(
    currentChat.chat.questions
  );

  currentChat.chat.questions = currentChat.chat.questions.map(
    (question, index) => {
      if (index > indexOfQuestionToUndo) {
        return {
          ...question,
          answerValue: null,
        };
      }
      return question;
    }
  );

  return {
    ...newState,
    currentQuestionIndex: indexOfQuestionToUndo,
  };
};

const apiResponse = (state, action) => {
  const newState = {
    ...state,
    chatPaused: false,
    showQuestionAnswer: true,
    showMessageAfterUserAnswer: true,
  };

  const currentChat = newState.chatsByTodoId[state.activeTodoId];

  if (!currentChat) {
    return newState;
  }

  currentChat.chat.questions = currentChat.chat.questions.map(question => {
    if (question.id === action.questionId) {
      return {
        ...question,
        apiResponse: action.apiResponse,
      };
    }
    return question;
  });

  return {
    ...newState,
  };
};

const updateStateWithAnswer = (state, { questionId, value }) => {
  const newState = { ...state };

  const currentChat = newState.chatsByTodoId[state.activeTodoId];

  if (!currentChat) {
    return newState;
  }

  currentChat.chat.questions = currentChat.chat.questions.map(question => {
    if (question.id === questionId) {
      return {
        ...question,
        answerValue: value,
      };
    }
    return question;
  });

  return newState;
};

const reducer = {
  [GET_CHAT_SUCCESS]: getChatSuccess,
  [SET_ACTIVE_TODO]: setActiveTodo,
  [UPDATE_CURRENT_QUESTION_INDEX]: updateCurrentQuestionIndex,
  [SHOW_QUESTION_ANSWER]: showQuestionAnswer,
  [UPDATE_CHAT_STATE_WITH_ANSWER]: updateStateWithAnswer,
  [SET_CURRENT_QUESTION_INDEX]: setCurrentQuestionIndex,
  [SHOW_MESSAGE_AFTER_USER_ANSWER]: showMessageAfterUserAnswer,
  [GET_TODOS]: resetChat,
  [RECAPTCHA_FAILED]: recaptchaFailed,
  [UNDO_QUESTION]: undoQuestion,
  [API_RESPONSE]: apiResponse,
  [LOGOUT]: () => ({ ...initialState }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
