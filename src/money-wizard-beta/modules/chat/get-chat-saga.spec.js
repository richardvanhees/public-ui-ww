import test from 'tape';
import sinon from 'sinon';
import { testSaga } from 'redux-saga-test-plan';
import { callGetChat, getChat, __RewireAPI__ } from './get-chat-saga';
import { GET_CHAT, getChatSuccess } from './actions';
import { setError, setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';

test('get chat saga', t => {
  localStorage.setItem('user_token', 'user_token');
  localStorage.setItem('refresh_token', 'refresh_token');

  const fakeChatResponse = {
    data: 'fake response',
  };

  const target = testSaga(getChat, { todoId: '123' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callGetChat, '/v1/chat?todoId=123')
    .next(fakeChatResponse)
    .put(getChatSuccess(fakeChatResponse.data))
    .next()
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'chat fetched for user');

  t.end();

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});

test('get chat saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const fakeChatResponse = {
    data: {
      abVersion: 'a',
    },
  };

  const sandbox = sinon.sandbox.create();

  const eventStub = sandbox.stub();

  __RewireAPI__.__Rewire__('ReactGA', {
    event: eventStub,
  });

  const target = testSaga(getChat, { todoId: 'registration' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callGetChat, '/anonymous/chat?todoId=registration')
    .next(fakeChatResponse)
    .put(getChatSuccess(fakeChatResponse.data))
    .next()
    .put(setIsLoading(false))
    .next();

  t.deepEqual(eventStub.args, [
    [{ action: 'registration/a', category: 'Chat loaded' }],
  ]);

  t.true(target.isDone(), 'chat fetched for anonymous user');

  t.end();

  __RewireAPI__.__ResetDependency__('ReactGA');
});

test('get chat saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const fakeChatResponse = {
    data: {
      abVersion: 'a',
      status: 'COMPLETE',
    },
  };

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: 'CONTEXT_ROUTE',
  });

  const sandbox = sinon.sandbox.create();

  const eventStub = sandbox.stub();

  __RewireAPI__.__Rewire__('ReactGA', {
    event: eventStub,
  });

  const target = testSaga(getChat, { todoId: 'registration' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callGetChat, '/anonymous/chat?todoId=registration')
    .next(fakeChatResponse)
    .put(push(`CONTEXT_ROUTE/todos`))
    .next();

  t.true(target.isDone(), 'redirected to todos when done todo already');

  t.end();

  __RewireAPI__.__ResetDependency__('ReactGA');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('get chat saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const fakeChatResponse = {
    data: 'fake response',
  };

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: 'CONTEXT_ROUTE',
  });

  const target = testSaga(getChat, {})
    .next()
    .put(push(`CONTEXT_ROUTE/todos`))
    .next();

  t.true(target.isDone(), 'redirected to todos when todoid not passed in saga');

  t.end();

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('get chat saga', t => {
  localStorage.setItem('user_token', 'fake token');
  localStorage.removeItem('refresh_token');

  const fakeChatResponse = {
    data: 'fake response',
  };

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: 'CONTEXT_ROUTE',
  });

  const target = testSaga(getChat, { todoId: 'registration' })
    .next()
    .put(push(`CONTEXT_ROUTE/todos`))
    .next();

  t.true(
    target.isDone(),
    'redirected to todos when logged in and todoId = registration'
  );

  t.end();

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('get chat saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const fakeChatResponse = {
    data: 'fake response',
  };

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: 'CONTEXT_ROUTE',
  });

  const target = testSaga(getChat, { todoId: 'somethingNotRegistration' })
    .next()
    .put(push(`CONTEXT_ROUTE/todos`))
    .next();

  t.true(
    target.isDone(),
    'redirected to todos when not logged in and todoId !== registration'
  );

  t.end();

  __RewireAPI__.__ResetDependency__('WealthWizards');
});
