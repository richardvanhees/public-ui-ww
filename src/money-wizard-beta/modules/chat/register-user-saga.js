import { call, put, select, takeLatest } from 'redux-saga/effects';
import { REGISTER_USER, saveChat, recaptchaFailed } from './actions';
import { lookupActiveChat } from './selectors';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import { setItem, getItem } from '../../../../modules/utils/local-storage';
import R from 'ramda';
import { post } from '../../../../modules/utils/AxiosWrapper';

export const callRegisterUser = (emailAddress, recaptcha) =>
  post({
    route: '/v1/magic-link/register',
    data: {
      emailAddress,
    },
    headers: { recaptcha },
  });

export const getState = state => state;

const getAnswer = answerValue =>
  R.compose(
    R.path(['answerValue']),
    R.find(R.propEq('answerType', answerValue)),
    R.path(['chat', 'questions'])
  );

/* eslint-disable consistent-return */
export function* registerUser() {
  try {
    if (getItem('user_token')) {
      return;
    }
    yield put(setIsLoading(true));

    const state = yield select(getState);

    const currentChatFromState = lookupActiveChat(state.chat);

    const emailAddress = getAnswer('email')(currentChatFromState);

    const recaptcha = getAnswer('recaptcha')(currentChatFromState);

    const { data } = yield call(callRegisterUser, emailAddress, recaptcha);

    setItem('user_token', data.jwt);
    setItem('refresh_token', data.refresh_token);

    // now time to flush the current chat to the DB
    return yield put(saveChat());
  } catch (error) {
    yield put(setIsLoading(false));

    const errorCode = R.path(['response', 'data', 'code'])(error);

    if (errorCode === 'RECAPTCHA_FAILED') {
      return yield put(recaptchaFailed());
    }

    if (errorCode === 'EMAIL_ALREADY_EXISTS') {
      return yield put(
        setError('This email address appears to already be in use.')
      );
    }

    const statusCode = R.path(['response', 'status'])(error);
    if (statusCode !== 401) {
      return yield put(
        setError(
          'We are currenly unable to register you, please try again later.'
        )
      );
    }
    return null;
  }
}

export default function* registerUserSaga() {
  yield takeLatest(REGISTER_USER, registerUser);
}
