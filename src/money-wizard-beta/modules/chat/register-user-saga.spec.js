import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import {
  registerUser,
  callRegisterUser,
  getState,
  __RewireAPI__,
} from './register-user-saga';
import { REGISTER_USER, saveChat, recaptchaFailed } from './actions';
import { setError, setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';
import { removeItem } from '../../../../modules/utils/local-storage';

test('register user saga', t => {
  localStorage.removeItem('user_token');

  const target = testSaga(registerUser)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: '1',
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: {
            status: 'IN_PROGRESS',
            chat: {
              questions: [
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'email',
                  answerValue: 'test@support.com',
                },
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'recaptcha',
                  answerValue: 'aaaaa',
                },
              ],
            },
          },
        },
      },
    })
    .call(callRegisterUser, 'test@support.com', 'aaaaa')
    .next({
      data: {
        jwt: 'jwt',
        refresh_token: 'refresh_token',
      },
    })
    .put(saveChat())
    .next();

  t.true(target.isDone(), 'user registered');

  t.end();
});

test('register user saga', t => {
  localStorage.removeItem('user_token');
  const target = testSaga(registerUser)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: '1',
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: {
            status: 'IN_PROGRESS',
            chat: {
              questions: [
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'email',
                  answerValue: 'test@support.com',
                },
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'recaptcha',
                  answerValue: 'aaaaa',
                },
              ],
            },
          },
        },
      },
    })
    .call(callRegisterUser, 'test@support.com', 'aaaaa')
    .throw('aaaaa')
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'We are currenly unable to register you, please try again later.'
      )
    )
    .next();

  t.true(target.isDone(), 'user registration error handled');

  t.end();
});

test('register user saga', t => {
  localStorage.removeItem('user_token');
  const target = testSaga(registerUser)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: '1',
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: {
            status: 'IN_PROGRESS',
            chat: {
              questions: [
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'email',
                  answerValue: 'test@support.com',
                },
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'recaptcha',
                  answerValue: 'aaaaa',
                },
              ],
            },
          },
        },
      },
    })
    .call(callRegisterUser, 'test@support.com', 'aaaaa')
    .throw({
      response: {
        data: {
          code: 'EMAIL_ALREADY_EXISTS',
        },
      },
    })
    .put(setIsLoading(false))
    .next()
    .put(setError('This email address appears to already be in use.'))
    .next();

  t.true(target.isDone(), 'duplicate email error handled');

  t.end();
});

test('register user saga', t => {
  localStorage.removeItem('user_token');
  const target = testSaga(registerUser)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: '1',
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: {
            status: 'IN_PROGRESS',
            chat: {
              questions: [
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'email',
                  answerValue: 'test@support.com',
                },
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'recaptcha',
                  answerValue: 'aaaaa',
                },
              ],
            },
          },
        },
      },
    })
    .call(callRegisterUser, 'test@support.com', 'aaaaa')
    .throw({
      response: {
        data: {
          code: 'RECAPTCHA_FAILED',
        },
      },
    })
    .put(setIsLoading(false))
    .next()
    .put(recaptchaFailed())
    .next();

  t.true(target.isDone(), 'recaptcha failed fires recaptchaFailed event');

  t.end();
});

test('register user saga', t => {
  localStorage.removeItem('user_token');
  const target = testSaga(registerUser)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getState)
    .next({
      chat: {
        activeTodoId: 'registration',
        currentQuestionIndex: '1',
        showQuestionAnswer: false,
        chatPaused: false,
        chatsByTodoId: {
          registration: {
            status: 'IN_PROGRESS',
            chat: {
              questions: [
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'email',
                  answerValue: 'test@support.com',
                },
                {
                  id: '1',
                  content: 'Hi',
                  pause: 1000,
                  answerType: 'recaptcha',
                  answerValue: 'aaaaa',
                },
              ],
            },
          },
        },
      },
    })
    .call(callRegisterUser, 'test@support.com', 'aaaaa')
    .throw({
      response: {
        status: 401,
      },
    })
    .put(setIsLoading(false))
    .next();

  t.true(
    target.isDone(),
    '401 error handled, no alert triggered from this saga'
  );

  t.end();
  localStorage.removeItem('user_token');
});
