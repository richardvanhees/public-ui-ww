export const REGISTER_USER = 'chat/REGISTER_USER';
export const SAVE_CHAT = 'chat/SAVE_CHAT';
export const SAVE_CHAT_ANSWER = 'chat/SAVE_CHAT_ANSWER';
export const GET_CHAT_SUCCESS = 'chat/GET_CHAT_SUCCESS';
export const GET_CHAT = 'chat/GET_CHAT';
export const SET_ACTIVE_TODO = 'chat/SET_ACTIVE_TODO';
export const UPDATE_CHAT_STATE_WITH_ANSWER =
  'chat/UPDATE_CHAT_STATE_WITH_ANSWER';
export const SHOW_QUESTION_ANSWER = 'chat/SHOW_QUESTION_ANSWER';
export const UPDATE_CURRENT_QUESTION_INDEX =
  'chat/UPDATE_CURRENT_QUESTION_INDEX';
export const SET_CURRENT_QUESTION_INDEX = 'chat/SET_CURRENT_QUESTION_INDEX';
export const SHOW_MESSAGE_AFTER_USER_ANSWER =
  'chat/SHOW_MESSAGE_AFTER_USER_ANSWER';
export const RECAPTCHA_FAILED = 'chat/RECAPTCHA_FAILED';
export const UNDO_QUESTION = 'chat/UNDO_QUESTION';
export const TRIGGER_API_CALL = 'chat/TRIGGER_API_CALL';
export const API_RESPONSE = 'chat/API_RESPONSE';

export const saveChat = (redirectUrl, reloadFactFind) => ({
  type: SAVE_CHAT,
  redirectUrl,
  reloadFactFind,
});

export const triggerApiCall = questionId => ({
  type: TRIGGER_API_CALL,
  questionId,
});

export const apiResponse = (questionId, response) => ({
  type: API_RESPONSE,
  questionId,
  apiResponse: response,
});

export const showMessageAfterUserAnswer = () => ({
  type: SHOW_MESSAGE_AFTER_USER_ANSWER,
});

export const saveChatAnswer = (questionId, answerValue) => ({
  type: SAVE_CHAT_ANSWER,
  questionId,
  answerValue,
});

export const getChatSuccess = chatRecord => ({
  type: GET_CHAT_SUCCESS,
  chatRecord,
});

export const getChatFromServer = todoId => ({
  type: GET_CHAT,
  todoId,
});

export const undoQuestion = questionId => ({
  type: UNDO_QUESTION,
  questionId,
});

export const updateStateWithAnswer = (questionId, value) => ({
  type: UPDATE_CHAT_STATE_WITH_ANSWER,
  questionId,
  value,
});

export const updateCurrentQuestionIndex = nextIndex => ({
  type: UPDATE_CURRENT_QUESTION_INDEX,
  nextIndex,
});

export const triggerQuestionWithId = questionId => ({
  type: SET_CURRENT_QUESTION_INDEX,
  questionId,
});

export const triggerShowOfQuestionAnswer = () => ({
  type: SHOW_QUESTION_ANSWER,
});

export const registerUser = () => ({
  type: REGISTER_USER,
});

export const recaptchaFailed = () => ({
  type: RECAPTCHA_FAILED,
});
