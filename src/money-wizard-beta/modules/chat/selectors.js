export const lookupActiveChat = chat =>
  chat.chatsByTodoId && chat.chatsByTodoId[chat.activeTodoId];

export const lookupCurrentQuestionIndex = chat => chat.currentQuestionIndex;

export const showQuestionAnswer = chat => chat.showQuestionAnswer;

export const chatPaused = chat => chat.chatPaused;

export const activeTodoId = chat => chat.activeTodoId;
