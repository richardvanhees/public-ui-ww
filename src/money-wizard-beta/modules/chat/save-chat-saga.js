import { call, put, takeLatest, select } from 'redux-saga/effects';
import { getFactFindRequest } from '../fact-find/action';
import { push } from 'react-router-redux';
import { SAVE_CHAT, updateCurrentQuestionIndex } from './actions';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';
import { getItem } from '../../../../modules/utils/local-storage';
import R from 'ramda';
import { putRequest } from '../../../../modules/utils/refresh-request';
import { lookupActiveChat, activeTodoId } from './selectors';

export const callSaveChat = (chat, todoId) =>
  putRequest(
    `${WealthWizards.CHAT_STORE_BASE_URL}/v1/chat?todoId=${todoId}`,
    `Bearer ${getItem('user_token')}`,
    { data: chat }
  );

export const getState = state => state;

export function* saveChat({ redirectUrl, reloadFactFind }) {
  try {
    yield put(setIsLoading(true));

    const state = yield select(getState);
    const currentChatFromState = lookupActiveChat(state.chat);

    const currentQuestionIndex = state.chat.currentQuestionIndex;

    const todoId = activeTodoId(state.chat);

    yield call(
      callSaveChat,
      {
        ...currentChatFromState,
        // enables server to find final node in tree and therefore which todos to generate
        finalQuestionId:
          currentChatFromState.chat.questions[currentQuestionIndex].id,
      },
      todoId
    );

    yield put(setIsLoading(false));

    if (reloadFactFind) {
      return yield put(getFactFindRequest(redirectUrl));
    }

    if (redirectUrl) {
      return yield put(push(redirectUrl));
    }

    return yield put(updateCurrentQuestionIndex(currentQuestionIndex + 1));
  } catch (error) {
    yield put(setIsLoading(false));
    const statusCode = R.path(['response', 'status'])(error);
    if (statusCode !== 401) {
      yield put(
        setError('We are currenly save the chat, please try again later.')
      );
    }
    return null;
  }
}

export default function* saveChatSaga() {
  yield takeLatest(SAVE_CHAT, saveChat);
}
