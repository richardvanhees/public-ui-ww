import { call, put, select, takeLatest } from 'redux-saga/effects';
import { TRIGGER_API_CALL, apiResponse } from './actions';
import { lookupActiveChat } from './selectors';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import R from 'ramda';
import externalApis from '../../external-apis/index';

export const getState = state => state;

const getQuestionDefinition = questionId =>
  R.compose(
    R.find(R.propEq('id', questionId)),
    R.path(['chat', 'questions'])
  );

export function* triggerApiCall({ questionId }) {
  try {
    yield put(setIsLoading(true));

    const state = yield select(getState);

    const currentChatFromState = lookupActiveChat(state.chat);

    const question = getQuestionDefinition(questionId)(currentChatFromState);

    const api = externalApis[question.api];

    if (!api) {
      console.error(
        // eslint-disable-line no-console
        `Something has not been defined correctly in the DSL, ${question.api}`
      );
    }

    const inputs = {};

    const questionIds = R.keys(question.apiMapping);

    for (let i = 0; i < questionIds.length; i++) {
      const id = questionIds[i];
      const questionDef = getQuestionDefinition(id)(currentChatFromState);
      inputs[question.apiMapping[id]] = questionDef.answerValue;
    }

    const response = yield call(api, inputs);

    yield put(apiResponse(questionId, response));

    return yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));

    const statusCode = R.path(['response', 'status'])(error);
    if (statusCode !== 401) {
      return yield put(
        setError(
          'We are currenly unable to process that, please try again later.'
        )
      );
    }
    return null;
  }
}

export default function* triggerApiCallSaga() {
  yield takeLatest(TRIGGER_API_CALL, triggerApiCall);
}
