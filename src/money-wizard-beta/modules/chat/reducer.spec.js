import reducer from './reducer';
import test from 'tape';
import {
  GET_CHAT_SUCCESS,
  UPDATE_CHAT_STATE_WITH_ANSWER,
  SHOW_MESSAGE_AFTER_USER_ANSWER,
  RECAPTCHA_FAILED,
  UNDO_QUESTION,
} from './actions';

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      showMessageAfterUserAnswer: false,
      chatPaused: true,
    },
    {
      type: SHOW_MESSAGE_AFTER_USER_ANSWER,
    }
  );

  assert.deepEqual(newState, {
    showMessageAfterUserAnswer: true,
    chatPaused: false,
  });
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      chatPaused: false,
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
                answerType: 'number',
                answerValue: 123,
              },
              {
                id: '2',
                answerType: 'number',
                answerValue: 123,
              },
              {
                id: '3',
                answerType: 'number',
                answerValue: 123,
              },
            ],
          },
        },
      },
    },
    {
      type: UNDO_QUESTION,
      questionId: '2',
    }
  );

  assert.deepEqual(newState, {
    activeTodoId: '123',
    chatPaused: true,
    chatsByTodoId: {
      123: {
        chat: {
          questions: [
            {
              id: '1',
              answerType: 'number',
              answerValue: 123,
            },
            {
              id: '2',
              answerType: 'number',
              answerValue: 123,
            },
            {
              id: '3',
              answerType: 'number',
              answerValue: null,
            },
          ],
        },
      },
    },
    showMessageAfterUserAnswer: false,
    showQuestionAnswer: false,
    undoing: true,
    currentQuestionIndex: 1,
  });
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      chatPaused: false,
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
                answerType: 'recaptcha',
                answerValue: 'recaptcha stuff',
              },
              {
                id: '2',
                answerType: 'number',
                answerValue: 'stuff',
              },
            ],
          },
        },
      },
    },
    {
      type: RECAPTCHA_FAILED,
    }
  );

  assert.deepEqual(newState, {
    activeTodoId: '123',
    chatPaused: true,
    chatsByTodoId: {
      123: {
        chat: {
          questions: [
            {
              id: '1',
              answerType: 'recaptcha',
              answerValue: null,
            },
            {
              id: '2',
              answerType: 'number',
              answerValue: null,
            },
          ],
        },
      },
    },
    currentQuestionIndex: 0,
    replayRecaptcha: true,
  });
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {},
    {
      type: GET_CHAT_SUCCESS,
      chatRecord: {
        todoId: '123',
        chat: {
          questions: [],
        },
      },
      activeTodoId: '123',
    }
  );

  assert.deepEqual(newState, {
    activeTodoId: '123',
    chatsByTodoId: {
      123: {
        chat: {
          questions: [],
        },
        todoId: '123',
      },
    },
    currentQuestionIndex: 0,
  });
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {},
    {
      type: GET_CHAT_SUCCESS,
      chatRecord: {
        todoId: '123',
        chat: {
          questions: [
            {
              id: '123',
              answerValue: 33,
            },
          ],
        },
      },
      activeTodoId: '123',
    }
  );

  assert.deepEqual(newState, {
    activeTodoId: '123',
    chatsByTodoId: {
      123: {
        chat: {
          questions: [
            {
              id: '123',
              answerValue: 33,
            },
          ],
        },
        todoId: '123',
      },
    },
    currentQuestionIndex: 1,
  });
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
              },
            ],
          },
        },
      },
    },
    {
      type: UPDATE_CHAT_STATE_WITH_ANSWER,
      questionId: '1',
      value: 'shabba',
    }
  );

  assert.deepEqual(
    newState,
    {
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
                answerValue: 'shabba',
              },
            ],
          },
        },
      },
    },
    'answer saved to correct location in state'
  );
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      activeTodoId: 'not defined for some reason',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
              },
            ],
          },
        },
      },
    },
    {
      type: UPDATE_CHAT_STATE_WITH_ANSWER,
      questionId: '1',
      value: 'shabba',
    }
  );

  assert.deepEqual(
    newState,
    {
      activeTodoId: 'not defined for some reason',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
              },
            ],
          },
        },
      },
    },
    'handles activeTodoId not being defined'
  );
});

test('chat/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
              },
            ],
          },
        },
      },
    },
    {
      type: UPDATE_CHAT_STATE_WITH_ANSWER,
      questionId: '2',
      value: 'shabba',
    }
  );

  assert.deepEqual(
    newState,
    {
      activeTodoId: '123',
      chatsByTodoId: {
        123: {
          chat: {
            questions: [
              {
                id: '1',
              },
            ],
          },
        },
      },
    },
    'handles question id not finding match in list of questions'
  );
});
