import { getCountdownDocument } from './selectors';
import test from 'tape';

test('documents/selectors/getCountdownDocument', assert => {
  assert.plan(1);

  assert.deepEqual(
    getCountdownDocument({
      data: {
        documents: [],
      },
    }),
    undefined,
    'returns undefined when countdown document does not exist'
  );
});

test('documents/selectors/getCountdownDocument', assert => {
  assert.plan(1);

  assert.deepEqual(
    getCountdownDocument({
      data: {
        documents: [
          {
            capability: 'investment',
          },
        ],
      },
    }),
    undefined,
    'returns undefined when there is only an investment doc'
  );
});

test('documents/selectors/getCountdownDocument', assert => {
  assert.plan(1);

  assert.deepEqual(
    getCountdownDocument({
      data: {
        documents: [
          {
            capability: 'countdown',
          },
          {
            capability: 'investment',
          },
        ],
      },
    }),
    {
      capability: 'countdown',
    },
    'returns countdown doc'
  );
});
