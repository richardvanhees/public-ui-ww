import test from 'tape';
import sinon from 'sinon';
import sinonP from 'sinon-as-promised';
import { testSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects';
import { get } from '../../../../modules/utils/refresh-request';
import WealthWizards from '../../../../modules/wealthwizards';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import { DOWNLOAD_REPORT } from './actions';
import downloadReportSaga, {
  downloadReport,
  callDownloadReport,
  __RewireAPI__,
} from './download-document-saga';
import { getItem } from '../../../../modules/utils/local-storage';
import { push } from 'react-router-redux';

const co = require('co');

test('downloadDocumentSaga', assert => {
  assert.plan(2);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const filesaverStub = sandbox.stub();

  __RewireAPI__.__Rewire__('filesaver', filesaverStub);

  const target = testSaga(downloadReport, {
    href: 'downloadUrl',
  })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callDownloadReport, 'downloadUrl')
    .next({ data: 'a blob' })
    .put(setIsLoading(false))
    .next();

  assert.deepEqual(
    filesaverStub.args,
    [['a blob', 'CountdownReport.pdf']],
    'file saver invoked with blob and file name'
  );

  assert.true(target.isDone(), 'report downloaded');

  localStorage.removeItem('user_token');

  __RewireAPI__.__ResetDependency__('filesaver');
});

test('downloadDocumentSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const filesaverStub = sandbox.stub();

  __RewireAPI__.__Rewire__('filesaver', filesaverStub);

  const target = testSaga(downloadReport, {
    href: 'downloadUrl',
  })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callDownloadReport, 'downloadUrl')
    .throw({
      response: {
        status: 500,
      },
    })
    .put(setIsLoading(false))
    .next()
    .put(setError('We are currently unable to download your report'))
    .next();

  assert.true(target.isDone(), 'report downloaded');

  localStorage.removeItem('user_token');

  __RewireAPI__.__ResetDependency__('filesaver');
});

test('downloadDocumentSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const filesaverStub = sandbox.stub();

  __RewireAPI__.__Rewire__('filesaver', filesaverStub);

  const target = testSaga(downloadReport, {
    href: 'downloadUrl',
  })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(callDownloadReport, 'downloadUrl')
    .throw({
      response: {
        status: 401,
      },
    })
    .put(setIsLoading(false))
    .next();

  assert.true(target.isDone(), 'report downloaded');

  localStorage.removeItem('user_token');

  __RewireAPI__.__ResetDependency__('filesaver');
});
