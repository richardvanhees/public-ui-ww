import reducer from './reducer';
import test from 'tape';
import { SET_DOCUMENTS } from './actions';

test('documents/reducer', assert => {
  assert.plan(1);

  const newState = reducer(
    {
      data: {},
    },
    {
      type: SET_DOCUMENTS,
      data: {
        documents: [],
      },
    }
  );

  assert.deepEqual(newState, {
    data: {
      documents: [],
    },
  });
});
