import R from 'ramda';

export const getCountdownDocument = R.compose(
  R.find(R.propEq('capability', 'countdown')),
  R.pathOr([], ['data', 'documents'])
);
