import { SET_DOCUMENTS } from './actions';
import { LOGOUT } from '../login/actions';

const setDocuments = (state, { data }) => ({
  ...state,
  data,
});

const initialState = {
  data: {},
};

const reducer = {
  [SET_DOCUMENTS]: setDocuments,
  [LOGOUT]: () => ({ ...initialState }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
