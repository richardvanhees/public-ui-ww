import { put, takeLatest, call } from 'redux-saga/effects';
import R from 'ramda';
import { get } from '../../../../modules/utils/refresh-request';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import { DOWNLOAD_REPORT } from './actions';
import { getItem } from '../../../../modules/utils/local-storage';
import filesaver from 'file-saver';

const getResponseCode = R.path(['response', 'status']);

export function* callDownloadReport(href) {
  return yield get(href, `Bearer ${getItem('user_token')}`, {}, 'blob');
}

/* eslint-disable consistent-return */
export function* downloadReport({ href }) {
  try {
    yield put(setIsLoading(true));

    const { data } = yield call(callDownloadReport, href);

    yield put(setIsLoading(false));

    return filesaver(data, 'CountdownReport.pdf');
  } catch (error) {
    yield put(setIsLoading(false));

    const responseStatus = getResponseCode(error);

    if (responseStatus !== 404 && responseStatus !== 401) {
      yield put(setError('We are currently unable to download your report'));
    }
  }
}

export default function* downloadReportSaga() {
  yield takeLatest(DOWNLOAD_REPORT, downloadReport);
}
