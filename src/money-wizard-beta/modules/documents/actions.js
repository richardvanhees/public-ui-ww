export const SET_DOCUMENTS = 'modules/DOCUMENTS/SET_DOCUMENTS';
export const DOWNLOAD_REPORT = 'modules/DOCUMENTS/DOWNLOAD_REPORT';

export const setDocuments = data => ({
  type: SET_DOCUMENTS,
  data,
});

export const downloadCountdownReport = href => ({
  type: DOWNLOAD_REPORT,
  href,
});
