import { fork, put, call, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../../../modules/wealthwizards';
import { LOGOUT } from './actions';
import { setIsLoading } from '../../../../modules/browser/actions';
import axios from 'axios';
import { push } from 'react-router-redux';
import { getItem, removeItem } from '../../../../modules/utils/local-storage';
import { shutdown } from '../../../../modules/utils/intercom';

export const logoutCall = jwt =>
  axios({
    method: 'POST',
    url: `${WealthWizards.AUTH_SVC_URL}/v1/oauth/revoke-token`,
    data: { jwt },
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  });

export const getRouting = state => state.routing;

export function* logout() {
  if (getItem('user_token')) {
    yield put(setIsLoading(true));

    try {
      yield fork(logoutCall, getItem('user_token'));
    } catch (e) {
      // deliberately swallow, we try and revoke if possible
    }

    removeItem('user_token');
    removeItem('refresh_token');
    yield call(shutdown);
    yield put(setIsLoading(false));
  }

  // see magic in modules/boilerplate/configure-store.js
  // TODO
  // yield put(resetState());

  return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/`));
}

export default function* logoutSaga() {
  yield takeLatest(LOGOUT, logout);
}
