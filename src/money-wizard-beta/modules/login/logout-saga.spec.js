import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { logout, logoutCall, getRouting } from './logout-saga';
import { LOGOUT } from './actions';
import { setError, setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';
import { shutdown } from '../../../../modules/utils/intercom';

test('logout saga', t => {
  localStorage.setItem('user_token', 'user_token');
  localStorage.setItem('refresh_token', 'refresh_token');

  const target = testSaga(logout, { redirectAfterLoginIfPossible: true })
    .next()
    .put(setIsLoading(true))
    .next()
    .fork(logoutCall, 'user_token')
    .next()
    .call(shutdown)
    .next()
    .put(setIsLoading(false))
    .next()
    .put(push(`${WealthWizards.CONTEXT_ROUTE}/`))
    .next();

  t.true(target.isDone(), 'user logged out');

  t.equals(localStorage.getItem('user_token'), null);
  t.equals(localStorage.getItem('refresh_token'), null);

  t.end();

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});

test('logout saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const target = testSaga(logout, {})
    .next()
    .put(push(`${WealthWizards.CONTEXT_ROUTE}/`))
    .next();

  t.true(
    target.isDone(),
    'user logged out when no token defined in local storage'
  );

  t.equals(localStorage.getItem('user_token'), null);
  t.equals(localStorage.getItem('refresh_token'), null);

  t.end();

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});
