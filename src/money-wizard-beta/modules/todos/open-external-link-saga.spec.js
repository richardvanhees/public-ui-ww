import test from 'tape';
import sinon from 'sinon';
import { testSaga } from 'redux-saga-test-plan';
import {
  patchServerToMarkTodoAsDone,
  openExternalLink,
  __RewireAPI__,
} from './open-external-link-saga';
import { OPEN_EXTERNAL_TODO_LINK, markTodoAsDone } from './actions';
import { setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';

test('open external link saga', t => {
  localStorage.setItem('user_token', 'user_token');
  localStorage.setItem('refresh_token', 'refresh_token');

  const windowLocationAssignBackup = window.location.assign;

  const sandbox = sinon.sandbox.create();
  window.location.assign = sandbox.stub();

  const target = testSaga(openExternalLink, {
    href: 'http://www.google.com',
    todoId: '123',
  })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(patchServerToMarkTodoAsDone, `/v1/todo?todoId=123`)
    .next()
    .put(markTodoAsDone('123'))
    .next();

  t.true(target.isDone(), 'external link opened');

  t.deepEqual(
    window.location.assign.args,
    [['http://www.google.com']],
    'link opened'
  );

  t.end();

  window.location.assign = windowLocationAssignBackup;

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});

test('open external link saga', t => {
  localStorage.setItem('user_token', 'user_token');
  localStorage.setItem('refresh_token', 'refresh_token');

  const windowLocationAssignBackup = window.location.assign;

  const sandbox = sinon.sandbox.create();
  window.location.assign = sandbox.stub();

  const target = testSaga(openExternalLink, {
    href: 'http://www.google.com',
    todoId: '123',
  })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(patchServerToMarkTodoAsDone, `/v1/todo?todoId=123`)
    .throw(new Error('aaaa'));

  t.true(target.isDone(), 'external link opened regardless of server error');

  t.deepEqual(
    window.location.assign.args,
    [['http://www.google.com']],
    'link opened'
  );

  t.end();

  window.location.assign = windowLocationAssignBackup;

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});
