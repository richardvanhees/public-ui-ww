import R from 'ramda';

export const getTodos = R.pathOr([], ['todos']);

export const incompleteTodos = R.pathOr([], ['incompleteTodos']);

export const lookupNextTodo = R.find(todo => todo.status !== 'COMPLETE');
