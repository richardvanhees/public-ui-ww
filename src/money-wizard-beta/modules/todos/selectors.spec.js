import { lookupNextTodo } from './selectors';
import test from 'tape';

test('selectors/lookupNextTodo', assert => {
  assert.plan(1);

  assert.deepEqual(
    lookupNextTodo([
      {
        id: 'shabba',
        status: 'COMPLETE',
      },
      {
        id: 'shabba1',
        status: 'AVAILABLE',
      },
    ]),
    {
      id: 'shabba1',
      status: 'AVAILABLE',
    }
  );
});

test('selectors/lookupNextTodo', assert => {
  assert.plan(1);

  assert.deepEqual(
    lookupNextTodo([
      {
        id: 'shabba',
        status: 'COMPLETE',
      },
      {
        id: 'shabba1',
        status: 'COMPLETE',
      },
    ]),
    undefined
  );
});
