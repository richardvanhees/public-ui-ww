import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { getTodosCall, getTodos, __RewireAPI__ } from './get-todos-saga';
import { GET_TODOS, getTodosSuccess, updateIncompleteTodos } from './actions';
import { setError, setIsLoading } from '../../../../modules/browser/actions';
import { push } from 'react-router-redux';

test('get todos saga', t => {
  localStorage.setItem('user_token', 'user_token');
  localStorage.setItem('refresh_token', 'refresh_token');

  const fakeTodosResponse = {
    data: { todos: ['fake response'] },
  };

  const target = testSaga(getTodos)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(getTodosCall, '/v1/todos')
    .next(fakeTodosResponse)
    .put(updateIncompleteTodos(1))
    .next()
    .put(getTodosSuccess(fakeTodosResponse.data))
    .next()
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'todos fetched for user');

  t.end();

  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');
});

test('get todos saga', t => {
  localStorage.removeItem('user_token');
  localStorage.removeItem('refresh_token');

  const fakeTodosResponse = {
    data: { todos: ['fake response'] },
  };

  const target = testSaga(getTodos)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(getTodosCall, '/v1/defaults')
    .next(fakeTodosResponse)
    .put(updateIncompleteTodos(1))
    .next()
    .put(getTodosSuccess(fakeTodosResponse.data))
    .next()
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'default todos fetched');

  t.end();
});
