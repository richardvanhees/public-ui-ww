import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_TODOS, getTodosSuccess, updateIncompleteTodos } from './actions';
import { setIsLoading, setError } from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';
import { get } from '../../../../modules/utils/refresh-request';
import { getItem } from '../../../../modules/utils/local-storage';
import R from 'ramda';

export const getTodosCall = path =>
  get(
    `${WealthWizards.TODO_STORE_BASE_URL}${path}`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

export function* getTodos() {
  try {
    yield put(setIsLoading(true));

    const { data } = yield call(
      getTodosCall,
      getItem('user_token') ? '/v1/todos' : '/v1/defaults'
    );

    const incompleteTodos = data.todos.filter(
      item => item.status !== 'COMPLETE'
    ).length;

    yield put(updateIncompleteTodos(incompleteTodos));
    yield put(getTodosSuccess(data));
    yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));
    const statusCode = R.path(['response', 'status'])(error);
    if (statusCode !== 401) {
      yield put(
        setError(
          'We are currenly unable to load your todos, please try again later.'
        )
      );
    }
  }
}

export default function* getTodosSaga() {
  yield takeLatest(GET_TODOS, getTodos);
}
