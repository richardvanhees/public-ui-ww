export const GET_DEFAULT_TODOS = 'todos/GET_DEFAULT_TODOS';
export const GET_TODOS_SUCCESS = 'todos/GET_TODOS_SUCCESS';
export const GET_TODOS_FAILED = 'todos/GET_TODOS_FAILED';
export const GET_TODOS = 'todos/GET_TODOS';
export const UPDATE_INCOMPLETE_TODOS = 'todos/UPDATE_INCOMPLETE_TODOS';
export const OPEN_EXTERNAL_TODO_LINK = 'todos/OPEN_EXTERNAL_TODO_LINK';
export const MARK_TODO_AS_DONE = 'todos/MARK_TODO_AS_DONE';

export const getTodosSuccess = todos => ({
  type: GET_TODOS_SUCCESS,
  todos,
});

export const updateIncompleteTodos = incompleteTodos => ({
  type: UPDATE_INCOMPLETE_TODOS,
  incompleteTodos,
});

export const markTodoAsDone = todoId => ({
  type: MARK_TODO_AS_DONE,
  todoId,
});

export const getTodosFromServer = () => ({
  type: GET_TODOS,
});

export const openExternalTodoLink = (todoId, href) => ({
  type: OPEN_EXTERNAL_TODO_LINK,
  todoId,
  href,
});
