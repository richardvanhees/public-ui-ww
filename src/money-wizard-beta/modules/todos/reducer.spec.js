import reducer from './reducer';
import test from 'tape';
import {
  GET_TODOS_FAILED,
  GET_TODOS_SUCCESS,
  UPDATE_INCOMPLETE_TODOS,
  MARK_TODO_AS_DONE,
} from './actions';

test('todos/reducer', assert => {
  assert.plan(1);

  assert.deepEqual(
    reducer(
      {
        todos: [
          {
            id: 1,
            status: 'AVAILABLE',
          },
        ],
      },
      { type: MARK_TODO_AS_DONE, todoId: 1 }
    ),
    { todos: [{ id: 1, status: 'COMPLETE' }] },
    'MARK_TODO_AS_DONE'
  );
});

test('todos/reducer', assert => {
  assert.plan(1);

  assert.deepEqual(
    reducer(
      {
        todos: [
          {
            id: 1,
            status: 'AVAILABLE',
          },
        ],
      },
      { type: MARK_TODO_AS_DONE, todoId: 2 }
    ),
    { todos: [{ id: 1, status: 'AVAILABLE' }] },
    'MARK_TODO_AS_DONE handles todo id not existing in state'
  );
});
