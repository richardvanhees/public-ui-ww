import R from 'ramda';
import {
  GET_TODOS_FAILED,
  GET_TODOS_SUCCESS,
  UPDATE_INCOMPLETE_TODOS,
  MARK_TODO_AS_DONE,
} from './actions';
import { LOGOUT } from '../login/actions';

const initialState = {
  todos: [],
  incompleteTodos: 0,
};

const getTodosSuccess = (state, action) => ({
  ...state,
  todos: action.todos.todos,
});

const markTodoAsDone = (state, { todoId }) => ({
  ...state,
  todos: R.map(
    R.ifElse(R.propEq('id', todoId), R.assoc('status', 'COMPLETE'), R.identity)
  )(state.todos),
});

const updateIncompleteTodos = (state, action) => ({
  ...state,
  incompleteTodos: action.incompleteTodos,
});

const getTodosFailed = state => ({
  ...state,
});

const reducer = {
  [GET_TODOS_SUCCESS]: getTodosSuccess,
  [UPDATE_INCOMPLETE_TODOS]: updateIncompleteTodos,
  [GET_TODOS_FAILED]: getTodosFailed,
  [MARK_TODO_AS_DONE]: markTodoAsDone,
  [LOGOUT]: () => ({ ...initialState }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
