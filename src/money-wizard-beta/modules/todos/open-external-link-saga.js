import { call, put, takeLatest } from 'redux-saga/effects';
import { OPEN_EXTERNAL_TODO_LINK, markTodoAsDone } from './actions';
import { getItem } from '../../../../modules/utils/local-storage';
import WealthWizards from '../../../../modules/wealthwizards';
import { patch } from '../../../../modules/utils/refresh-request';
import { setIsLoading } from '../../../../modules/browser/actions';

export const patchServerToMarkTodoAsDone = path =>
  patch(
    `${WealthWizards.TODO_STORE_BASE_URL}${path}`,
    `Bearer ${getItem('user_token')}`,
    {
      data: {
        status: 'COMPLETE',
      },
    }
  );

export function* openExternalLink({ todoId, href }) {
  yield put(setIsLoading(true));
  try {
    yield call(patchServerToMarkTodoAsDone, `/v1/todo?todoId=${todoId}`);
    yield put(markTodoAsDone(todoId));
  } catch (e) {
    // deliberately swallow, open link anyway
  }

  window.location.assign(href);
}

export default function* openExternalLinkSaga() {
  yield takeLatest(OPEN_EXTERNAL_TODO_LINK, openExternalLink);
}
