import { put, takeLatest, call, all } from 'redux-saga/effects';
import R from 'ramda';

import { get } from '../../../../modules/utils/refresh-request';
import {
  setIsFullLoading,
  setError,
} from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';

import {
  GET_FACT_FIND_REQUEST,
  getFactFindSuccess,
  getFactFindFailure,
  setFactFindData,
} from './action';
import { getItem } from '../../../../modules/utils/local-storage';
import { push } from 'react-router-redux';
import { setDocuments } from '../documents/actions';

const getResponseCode = R.path(['response', 'status']);

export function* getFactFindCall() {
  return yield get(
    `${WealthWizards.FACT_FIND_SVC_URL}/v1/user/fact-find`,
    `Bearer ${getItem('user_token')}`,
    {}
  );
}

export function* getUserDocuments() {
  return yield get(
    `${WealthWizards.SOLUTION_STORE_URL}/v1/user/documents`,
    `Bearer ${getItem('user_token')}`,
    {}
  );
}

/* eslint-disable consistent-return */
export function* getFactFind({ redirectPath }) {
  try {
    if (!getItem('user_token')) {
      if (redirectPath) {
        yield put(push(redirectPath));
      }
      return;
    }

    yield put(setIsFullLoading(true));

    const [factFindResponse, userDocumentsResponse] = yield all([
      call(getFactFindCall),
      call(getUserDocuments),
    ]);

    yield put(setDocuments(userDocumentsResponse.data));

    yield put(setFactFindData(factFindResponse.data));

    yield put(setIsFullLoading(false));
    yield put(getFactFindSuccess());

    if (redirectPath) {
      yield put(push(redirectPath));
    }
  } catch (error) {
    yield put(setIsFullLoading(false));

    const responseStatus = getResponseCode(error);
    yield put(getFactFindFailure());
    if (responseStatus !== 404 && responseStatus !== 401) {
      yield put(setError('We are currently unable to fetch your data.'));
    }
  }
}

export default function* getFactFindSaga() {
  yield takeLatest(GET_FACT_FIND_REQUEST, getFactFind);
}
