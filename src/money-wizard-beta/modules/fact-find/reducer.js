import {
  GET_FACT_FIND_REQUEST,
  GET_FACT_FIND_SUCCESS,
  GET_FACT_FIND_FAILURE,
  SET_FACT_FIND_DATA,
} from './action';
import { LOGOUT } from '../login/actions';

const getFactFindRequest = state => ({
  ...state,
  isLoading: true,
});

const getFactFindSuccess = state => ({
  ...state,
  isLoading: false,
});

const getFactFindFailure = state => ({
  ...state,
  isLoading: false,
});

const setFactFindData = (state, { factFindData }) => ({
  ...state,
  data: {
    ...state.data,
    ...factFindData,
  },
  isLoading: false,
});

const initialState = {
  isLoading: false,
  data: {},
};

const reducer = {
  [GET_FACT_FIND_REQUEST]: getFactFindRequest,
  [GET_FACT_FIND_SUCCESS]: getFactFindSuccess,
  [GET_FACT_FIND_FAILURE]: getFactFindFailure,
  [SET_FACT_FIND_DATA]: setFactFindData,
  [LOGOUT]: () => ({ ...initialState }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
