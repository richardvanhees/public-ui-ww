export const GET_FACT_FIND_REQUEST = 'modules/FACT_FIND/GET_FACT_FIND_REQUEST';
export const GET_FACT_FIND_SUCCESS = 'modules/FACT_FIND/GET_FACT_FIND_SUCCESS';
export const GET_FACT_FIND_FAILURE = 'modules/FACT_FIND/GET_FACT_FIND_FAILURE';
export const SET_FACT_FIND_DATA = 'modules/FACT_FIND/SET_FACT_FIND_DATA';

export const getFactFindRequest = redirectPath => ({
  type: GET_FACT_FIND_REQUEST,
  redirectPath,
});

export const getFactFindSuccess = () => ({
  type: GET_FACT_FIND_SUCCESS,
});

export const getFactFindFailure = () => ({
  type: GET_FACT_FIND_FAILURE,
});

export const setFactFindData = factFindData => ({
  type: SET_FACT_FIND_DATA,
  factFindData,
});
