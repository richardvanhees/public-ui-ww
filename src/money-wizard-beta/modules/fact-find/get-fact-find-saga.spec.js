import test from 'tape';
import sinon from 'sinon';
import sinonP from 'sinon-as-promised';
import { testSaga } from 'redux-saga-test-plan';
import { call } from 'redux-saga/effects';
import { get } from '../../../../modules/utils/refresh-request';
import {
  setIsFullLoading,
  setError,
} from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';

import {
  GET_FACT_FIND_REQUEST,
  getFactFindSuccess,
  getFactFindFailure,
  setFactFindData,
} from './action';
import getFactFindSaga, {
  getFactFind,
  getFactFindCall,
  getUserDocuments,
  __RewireAPI__,
} from './get-fact-find-saga';
import { getItem } from '../../../../modules/utils/local-storage';
import { push } from 'react-router-redux';
import { setDocuments } from '../documents/actions';

const co = require('co');

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('push', pushStub);

  const target = testSaga(getFactFind, {})
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([call(getFactFindCall), call(getUserDocuments)])
    .next([
      {
        data: { personal__email_address: 'baa' },
      },
      {
        data: {
          documents: [],
        },
      },
    ])
    .put(
      setDocuments({
        documents: [],
      })
    )
    .next()
    .put(setFactFindData({ personal__email_address: 'baa' }))
    .next()
    .put(setIsFullLoading(false))
    .next()
    .put(getFactFindSuccess())
    .next();

  assert.true(target.isDone(), 'gets fact find data successfully');

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('push', pushStub);

  const target = testSaga(getFactFind, { redirectPath: '/somewhere' })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([call(getFactFindCall), call(getUserDocuments)])
    .next([
      {
        data: { personal__email_address: 'baa' },
      },
      {
        data: {
          documents: [],
        },
      },
    ])
    .put(
      setDocuments({
        documents: [],
      })
    )
    .next()
    .put(setFactFindData({ personal__email_address: 'baa' }))
    .next()

    .put(setIsFullLoading(false))
    .next()
    .put(getFactFindSuccess())
    .next()
    .put('redirection')
    .next();

  assert.true(
    target.isDone(),
    'gets fact find data successfully and redirects'
  );

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');

  localStorage.removeItem('user_token');
});
