import R from 'ramda';

export const getEmail = R.pathOr([], ['data', 'personal__email_address']);
