import {
  get,
  manualTokenRefresh,
} from '../../../modules/utils/refresh-request';
import filesaver from 'file-saver';
import co from 'co';
import WealthWizards from '../../../modules/wealthwizards';
import { getItem } from '../../../modules/utils/local-storage';

export default async () => {
  const { data } = await co(function* downloadReport() {
    try {
      yield manualTokenRefresh();
    } catch (e) {
      // nom nom nom
    }
    return yield get(
      `${
        WealthWizards.SOLUTION_STORE_URL
      }/v1/user/solution/suitability-report?capability=countdown`,
      `Bearer ${getItem('user_token')}`,
      {},
      'blob'
    );
  });
  return filesaver(data, 'CountdownReport.pdf');
};
