import incomeTax from './income-tax';
import statePension from './state-pension';
import riskProfile from './risk-profile';
import countdownReportDownload from './countdown-report-download';

export default {
  incomeTax,
  statePension,
  atr: riskProfile('attitude-to-risk'),
  investmentExperience: riskProfile('investment-experience'),
  investmentResilience: riskProfile('financial-resilience'),
  countdownReportDownload,
};
