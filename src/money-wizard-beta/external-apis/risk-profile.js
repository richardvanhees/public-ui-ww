import axios from 'axios';
import WealthWizards from '../../../modules/wealthwizards';

const getDiscretionaryIncome = (grossIncome, monthlyExpenses) =>
  Math.max(grossIncome - monthlyExpenses * 12, 0);

export default profileType => async inputs => {
  const data = {
    profileType,
    ...inputs,
  };

  if (profileType === 'financial-resilience') {
    data.discretionary_income = getDiscretionaryIncome(
      inputs.gross_income,
      inputs.monthly_expenses
    );
  }

  const res = await axios({
    method: 'post',
    url: `${WealthWizards.CONTEXT_ROUTE}/v1/risk-profile-proxy`,
    data,
    headers: {
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
  });
  return {
    profile: res.data.profile,
  };
};
