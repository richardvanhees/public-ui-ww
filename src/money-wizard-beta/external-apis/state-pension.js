import axios from 'axios';
import moment from 'moment';
import WealthWizards from '../../../modules/wealthwizards';

export default async inputs => {
  const res = await axios({
    method: 'post',
    url: `${WealthWizards.STATE_PENSION_DATE_CALC_BASE_URL}/calculate`,
    data: {
      ...inputs,
      dob: moment(inputs.dob, 'DD-MM-YYYY').format('YYYY-MM-DD'),
    },
  });
  return {
    statePensionDate: res.data.state_pension_date,
    statePensionDateFormatted: moment(
      res.data.state_pension_date,
      'YYYY-MM-DD'
    ).format('Do MMMM YYYY'),
  };
};
