import axios from 'axios';
import WealthWizards from '../../../modules/wealthwizards';

export default async inputs => {
  const res = await axios({
    method: 'post',
    url: `${WealthWizards.INCOME_TAX_SVC_BASE_URL}/v1/calculate`,
    data: {
      income_period: 'ANNUALLY',
      apply_tax_reduction_thresholds: true,
      ...inputs,
    },
  });
  return {
    netSalaryPerMonth: Number(
      (
        (inputs.gross_income -
          res.data.total_tax -
          res.data.class_a_national_insurance) /
        12
      ).toFixed(2)
    ),
  };
};
