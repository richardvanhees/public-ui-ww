import { connect } from 'react-redux';
import AppWrapperComponent from './AppWrapperComponent';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { login, logout } from '../modules/login/actions';
import { lookContentByKey } from '../../../modules/content';
import { resetNotification, resetError } from '../../../modules/browser';
import { getShowmenu } from '../modules/sidebar/selectors';
import { getCountdownDocument } from '../modules/documents/selectors';
import { downloadCountdownReport } from '../modules/documents/actions';
import { getEmail } from '../modules/fact-find/selectors';
import { toggleSidebar, startIntercom } from '../modules/sidebar/actions';
import { push } from 'react-router-redux';

const mapStateToProps = ({
  sidebar,
  browser,
  content,
  factFind,
  documents,
}) => ({
  isMobile: browser.breakpoint === 'phablet',
  content: lookContentByKey('money-wizard-beta', content),
  errorMsg: browser.errorMsg,
  loading: browser.loading,
  showMenu: getShowmenu(sidebar),
  email: getEmail(factFind),
  notification: browser.notification,
  countdownDocument: getCountdownDocument(documents),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      login: () => login(),
      logout: () => logout(),
      navigate: page => push(page),
      closeNotification: resetNotification,
      toggleSideBar: () => toggleSidebar(),
      startIntercom: () => startIntercom(),
      downloadCountdownReport: href => downloadCountdownReport(href),
      resetError,
    },
    dispatch
  );

const AppWrapperContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default AppWrapperContainer(AppWrapperComponent);
