import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';

import SplashComponent from './component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigate: page => push(page),
      setError: msg => setError(msg),
      setIsLoading: loading => setIsLoading(loading),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashComponent);
