import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import WealthWizards from '../../../../../modules/wealthwizards';
import Title from '../../elements/Title';
import Subtitle from '../../elements/Subtitle';
import { getItem } from '../../../../../modules/utils/local-storage';
import {
  splash,
  signInIcon,
} from '../../../../../assets/images/money-wizard-beta';

export default class SplashComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
    setError: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  componentDidMount() {
    if (getItem('user_token')) {
      this.props.navigate(`${WealthWizards.CONTEXT_ROUTE}/todos`);
    }
  }

  render() {
    const { navigate } = this.props;
    return (
      <div className="wrapper splash">
        <div className="splash__get-started">
          <Title className="splash__heading" title="Welcome to Money Wizard" />

          <Subtitle
            className="splash__sub-heading"
            title="Your personal financial coach."
          />

          <div className="splash__image-mobile">
            <img
              className="splash__image-mobile-img"
              src={splash}
              alt="money-wizard-beta"
            />
          </div>

          <div className="splash__start-container">
            <Button
              className="btn--primary splash__get-started-btn"
              label="Get started"
              onClick={() =>
                navigate(`${WealthWizards.CONTEXT_ROUTE}/chat/registration`)
              }
              dataTest={'get-started-btn'}
            />
            <p className="splash__text splash__start-container-text">
              Already have an account?{' '}
              <span
                className="splash__start-container-text--link"
                onClick={() => navigate(`${WealthWizards.CONTEXT_ROUTE}/login`)}
              >
                Sign In
              </span>
            </p>
          </div>
        </div>

        <div className="splash__image">
          <div className="splash__sign-in-container">
            <div className="splash__sign-in-icon">
              <img
                className="splash__sign-in-icon-img"
                src={signInIcon}
                alt="money-wizard-beta"
              />
            </div>
            <p
              className="splash__sign-in-text"
              onClick={() => navigate(`${WealthWizards.CONTEXT_ROUTE}/login`)}
            >
              Sign in
            </p>
          </div>

          <img
            className="splash__image-img"
            src={splash}
            alt="money-wizard-beta"
          />
        </div>
      </div>
    );
  }
}
