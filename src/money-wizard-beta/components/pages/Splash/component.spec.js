import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Splash, { __RewireAPI__ } from './component';
import {
  getItem,
  removeItem,
} from '../../../../../modules/utils/local-storage';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Splash>', t => {
  t.equals(typeof Splash.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Splash>', t => {
  t.equals(
    shallow(<Splash {...fakeRequiredProps} />).find('.splash').length,
    1,
    'Splash is rendered'
  );

  t.end();
});

test('<Splash>', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/money-wizard-beta',
  });

  localStorage.setItem('user_token', 'fake token');

  const sandbox = sinon.sandbox.create();
  const navigateStub = sandbox.stub();

  const fakeRequiredProps = {
    navigate: navigateStub,
  };

  const wrapper = mount(<Splash {...fakeRequiredProps} />);

  assert.deepEqual(
    navigateStub.args,
    [['/money-wizard-beta/todos']],
    'redirected to todos'
  );

  localStorage.removeItem('user_token');

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<Splash>', t => {
  const sandbox = sinon.sandbox.create();
  const navigateStub = sandbox.stub();

  const fakeRequiredProps = {
    navigate: navigateStub,
  };

  const wrapper = mount(<Splash {...fakeRequiredProps} />);

  wrapper.find('button[data-test="get-started-btn"]').simulate('click');

  t.equals(
    navigateStub.callCount,
    1,
    'navigate stub called when clicked on register button'
  );

  t.end();
});
