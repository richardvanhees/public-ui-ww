import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import {
  getChatFromServer,
  updateCurrentQuestionIndex,
  triggerShowOfQuestionAnswer,
  updateStateWithAnswer,
  registerUser,
  triggerQuestionWithId,
  saveChat,
  saveChatAnswer,
  showMessageAfterUserAnswer,
  undoQuestion,
  triggerApiCall,
} from '../../../modules/chat/actions';
import {
  lookupActiveChat,
  lookupCurrentQuestionIndex,
  showQuestionAnswer,
  chatPaused,
} from '../../../modules/chat/selectors';
import { getShowmenu } from '../../../modules/sidebar/selectors';
import { toggleSidebar } from '../../../modules/sidebar/actions';
import { incompleteTodos } from '../../../modules/todos/selectors';
import WealthWizards from '../../../../../modules/wealthwizards';
import ChatComponent from './component';

const mapStateToProps = ({ browser, chat, todos, sidebar }) => ({
  loading: browser.loading,
  activeChat: lookupActiveChat(chat),
  showMenu: getShowmenu(sidebar),
  currentQuestionIndex: lookupCurrentQuestionIndex(chat),
  showQuestionAnswer: showQuestionAnswer(chat),
  incompleteTodos: incompleteTodos(todos),
  chatPaused: chatPaused(chat),
  shouldWeShowMessageAfterUserAnswer: chat.showMessageAfterUserAnswer,
  replayRecaptcha: chat.replayRecaptcha,
  undoing: chat.undoing,
  isMobile: browser.breakpoint === 'phablet',
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigate: page => push(page),
      loadChatFromServer: todoId => getChatFromServer(todoId),
      triggerShowOfQuestionAnswer: () => triggerShowOfQuestionAnswer(),
      updateStateWithAnswer: (questionId, value) =>
        updateStateWithAnswer(questionId, value),
      updateCurrentQuestionIndex: nextIndex =>
        updateCurrentQuestionIndex(nextIndex),
      registerUser: () => registerUser(),
      showMessageAfterUserAnswer,
      saveChatAnswer: (questionId, value) => saveChatAnswer(questionId, value),
      triggerQuestionWithId: questionId => triggerQuestionWithId(questionId),
      flushChatToServer: () =>
        saveChat(`${WealthWizards.CONTEXT_ROUTE}/todos`, true),
      undoQuestion,
      toggleSideBar: () => toggleSidebar(),
      triggerApiCall,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatComponent);
