import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../../elements/Header';
import ChatQuestion from '../../elements/ChatQuestion';
import WealthWizards from '../../../../../modules/wealthwizards';
import R from 'ramda';
import AnswerComponentFactory from './answer-component-factory';
import interpolate from '../../../../../modules/utils/interpolate';
import { red, blue } from '../../../../../assets/images/money-wizard-beta';
import getAvatar from '../../../utils/avatar';

export default class ChatComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
    loadChatFromServer: PropTypes.func.isRequired,
    triggerQuestionWithId: PropTypes.func.isRequired,
    updateCurrentQuestionIndex: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    showQuestionAnswer: PropTypes.bool.isRequired,
    chatPaused: PropTypes.bool.isRequired,
    triggerShowOfQuestionAnswer: PropTypes.func.isRequired,
    updateStateWithAnswer: PropTypes.func.isRequired,
    registerUser: PropTypes.func.isRequired,
    flushChatToServer: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    currentQuestionIndex: PropTypes.number.isRequired,
    incompleteTodos: PropTypes.number.isRequired,
    activeChat: PropTypes.object,
    saveChatAnswer: PropTypes.func.isRequired,
    showMessageAfterUserAnswer: PropTypes.func.isRequired,
    undoQuestion: PropTypes.func.isRequired,
    triggerApiCall: PropTypes.func.isRequired,
    toggleSideBar: PropTypes.func.isRequired,
    shouldWeShowMessageAfterUserAnswer: PropTypes.bool.isRequired,
    replayRecaptcha: PropTypes.bool.isRequired,
    undoing: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool.isRequired,
  };

  constructor() {
    super();
    this.triggerNextQuestion = this.triggerNextQuestion.bind(this);
  }

  componentDidMount() {
    const { todoId } = this.props.match.params;
    return this.props.loadChatFromServer(todoId);
  }

  componentDidUpdate() {
    this.timer && clearTimeout(this.timer);

    const currentQuestion = this.getCurrentQuestion();

    if (!currentQuestion) {
      return;
    }

    // trigger the answer selection for the question
    if (currentQuestion.answerType && !this.props.showQuestionAnswer) {
      // if we're undoing we don't want any delay, just show input
      this.props.undoing
        ? this.props.triggerShowOfQuestionAnswer()
        : (this.timer = setTimeout(() => {
            this.props.triggerShowOfQuestionAnswer();
          }, currentQuestion.pause || WealthWizards.DEFAULT_CHAT_PAUSE_IF_NOT_SPECIFIED));
      // show the next question
    } else if (!this.props.chatPaused) {
      this.timer = setTimeout(() => {
        this.triggerNextQuestion(currentQuestion);
      }, currentQuestion.pause || WealthWizards.DEFAULT_CHAT_PAUSE_IF_NOT_SPECIFIED);
    }

    if (
      this.dummyElToScrollTo &&
      R.is(Function, this.dummyElToScrollTo.scrollIntoView)
    ) {
      this.dummyElToScrollTo.scrollIntoView();
    }
  }

  getCurrentQuestion() {
    return R.path([
      'activeChat',
      'chat',
      'questions',
      this.props.currentQuestionIndex,
    ])(this.props);
  }

  triggerNextQuestion(question, endState) {
    if (question.triggerRegistration) {
      this.props.registerUser();
    } else if (endState) {
      this.props.flushChatToServer();
    } else if (
      question.answerType === 'button' &&
      question.onClickRedirectPath
    ) {
      this.props.navigate(
        `${WealthWizards.CONTEXT_ROUTE}${question.onClickRedirectPath}`
      );
    } else if (question.nextQuestionId) {
      this.props.triggerQuestionWithId(nextQuestionId);
    } else {
      this.props.updateCurrentQuestionIndex(
        this.props.currentQuestionIndex + 1
      );
    }
  }

  userWillNotBeAskedThisQuestion(isLatestQuestion, question) {
    return (
      !isLatestQuestion &&
      question.answerType &&
      question.answerType !== 'button' &&
      (question.answerValue === null || question.answerValue === undefined)
    );
  }

  render() {
    if (
      (this.props.loading && !this.props.activeChat) ||
      !this.props.activeChat
    ) {
      return null;
    }

    const todoId = R.path(['props', 'match', 'params', 'todoId'])(this);

    const elements = [];

    for (let i = 0; i <= this.props.currentQuestionIndex; i++) {
      const question = this.props.activeChat.chat.questions[i];

      if (!question) break;

      const isLatestQuestion = i === this.props.currentQuestionIndex;

      if (this.userWillNotBeAskedThisQuestion(isLatestQuestion, question)) {
        // this Q was not one they will be asked due to their answers
        continue;
      }

      const avatar = question.avatar ? getAvatar[question.avatar] : false;

      if (!R.isNil(question.content)) {
        elements.push(
          <ChatQuestion
            dataTest={`chat-element-${i}`}
            key={`chat-element-${i}`}
            content={question.content}
            showLoadingDot={isLatestQuestion && !this.props.showQuestionAnswer}
            replayRecaptcha={this.props.replayRecaptcha && isLatestQuestion}
            avatar={avatar}
          />
        );
      }

      if (
        this.props.showQuestionAnswer ||
        (!isLatestQuestion && question.answerType)
      ) {
        elements.push(
          <AnswerComponentFactory
            isMobile={this.props.isMobile}
            question={question}
            recaptchaKey={WealthWizards.RECAPTCHA_KEY}
            isLatestQuestion={isLatestQuestion}
            todoId={todoId}
            key={`chat-item-${i}`}
            dataTest={`chat-item-${i}`}
            // eslint-disable-next-line
            triggerNextQuestion={(nextQuestionId, endState) => {
              if (this.props.loading) {
                return;
              }
              clearTimeout(this.triggerNextQuestionTimeout);
              this.triggerNextQuestionTimeout = setTimeout(() => {
                if (
                  question.contentToShowAfterAnswer &&
                  !this.props.shouldWeShowMessageAfterUserAnswer
                ) {
                  this.props.showMessageAfterUserAnswer();
                } else if (nextQuestionId) {
                  this.props.triggerQuestionWithId(nextQuestionId);
                } else {
                  this.triggerNextQuestion(question, endState);
                }
              }, 500);
            }}
            triggerApiCall={this.props.triggerApiCall}
            undo={() => this.props.undoQuestion(question.id)}
            updateStateWithAnswer={(questionId, value) =>
              this.props.updateStateWithAnswer(questionId, value)
            }
            saveChatAnswer={this.props.saveChatAnswer}
          />
        );
      }

      if (
        this.props.shouldWeShowMessageAfterUserAnswer ||
        (!isLatestQuestion && question.contentToShowAfterAnswer)
      ) {
        elements.push(
          <ChatQuestion
            dataTest={`chat-element-after-answer-${i}`}
            key={`chat-element-after-answer-${i}`}
            content={
              question.apiResponse
                ? interpolate(question.contentToShowAfterAnswer, {
                    apiResponseValue:
                      question.apiResponse[question.apiResponseValueField],
                  })
                : question.contentToShowAfterAnswer
            }
          />
        );
      }
    }

    elements.push(
      <div
        ref={el => {
          this.dummyElToScrollTo = el;
        }}
        key="dummy-scroll-div"
        className="chat__scroll-div"
      />
    );

    return (
      <div className="chat">
        <div className="chat__shape-left">
          <img className="chat__shape-left-img" src={blue} alt="blue shape" />
        </div>
        <div className="chat__shape-right">
          <img className="chat__shape-right-img" src={red} alt="red shape" />
        </div>
        <Header
          title={R.path(['chat', 'title'])(this.props.activeChat)}
          incompleteTodos={this.props.incompleteTodos}
          toggleSidebar={this.props.toggleSideBar}
        />
        <div className="chat__wrapper">{elements}</div>
      </div>
    );
  }
}
