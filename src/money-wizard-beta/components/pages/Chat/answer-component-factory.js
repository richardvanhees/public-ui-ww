import React from 'react';
import PropTypes from 'prop-types';
import ChatRecaptcha from '../../elements/ChatRecaptcha';
import ChatInputField from '../../elements/ChatInputField';
import ChatButton from '../../elements/ChatButton';
import ChatMultipleChoice from '../../elements/ChatMultipleChoice';
import ChatDatePicker from '../../elements/ChatDatePicker';

const AnswerComponentFactory = ({
  question,
  recaptchaKey,
  isLatestQuestion,
  dataTest,
  triggerNextQuestion,
  updateStateWithAnswer,
  saveChatAnswer,
  undo,
  todoId,
  triggerApiCall,
  isMobile,
}) => {
  const undoEnabled = todoId !== 'registration';

  switch (question.answerType) {
    case 'recaptcha':
      return (
        <ChatRecaptcha
          dataTest={dataTest}
          onVerify={res => {
            if (isLatestQuestion) {
              updateStateWithAnswer(question.id, res);
              triggerNextQuestion();
            }
          }}
          enabled={isLatestQuestion}
          recaptchaKey={recaptchaKey}
          showFakeRecaptcha={WealthWizards.SHOW_FAKE_RECAPTCHA}
        />
      );

    case 'date':
      return (
        <ChatDatePicker
          dataTest={dataTest}
          onChange={value =>
            isLatestQuestion && updateStateWithAnswer(question.id, value)
          }
          enabled={isLatestQuestion}
          answerValue={question.answerValue || ''}
          done={() => {
            saveChatAnswer(question.id, question.answerValue);
            if (question.api) {
              triggerApiCall(question.id);
            } else {
              triggerNextQuestion(question.nextQuestionId, question.endState);
            }
          }}
          undo={undo}
          undoEnabled={undoEnabled}
          {...question}
        />
      );
    case 'email':
    case 'text':
    case 'number':
      return (
        <ChatInputField
          dataTest={dataTest}
          type={question.answerType}
          onChange={value =>
            isLatestQuestion && updateStateWithAnswer(question.id, value)
          }
          enabled={isLatestQuestion}
          answerValue={question.answerValue || ''}
          done={() => {
            saveChatAnswer(question.id, question.answerValue);
            if (question.api) {
              triggerApiCall(question.id);
            } else {
              triggerNextQuestion(question.nextQuestionId, question.endState);
            }
          }}
          undo={undo}
          undoEnabled={undoEnabled}
          {...question}
        />
      );
    case 'button':
      return (
        <ChatButton
          dataTest={dataTest}
          enabled={isLatestQuestion}
          onClick={() => {
            if (isLatestQuestion) {
              if (question.api) {
                triggerApiCall(question.id);
              } else {
                triggerNextQuestion(question.nextQuestionId, question.endState);
              }
            }
          }}
          label={question.buttonLabel}
        />
      );
    case 'multipleChoice':
      return (
        <ChatMultipleChoice
          isMobile={isMobile}
          dataTest={dataTest}
          enabled={isLatestQuestion}
          onSelect={choice => {
            if (isLatestQuestion) {
              updateStateWithAnswer(question.id, choice.id);
              saveChatAnswer(question.id, choice.id);
              if (question.api) {
                triggerApiCall(question.id);
              } else {
                triggerNextQuestion(choice.nextQuestionId, choice.endState);
              }
            }
          }}
          undo={undo}
          undoEnabled={undoEnabled}
          {...question}
        />
      );

    default:
      return null;
  }
};

AnswerComponentFactory.propTypes = {
  question: PropTypes.object.isRequired,
  recaptchaKey: PropTypes.string.isRequired,
  isLatestQuestion: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
  dataTest: PropTypes.string.isRequired,
  triggerNextQuestion: PropTypes.func.isRequired,
  updateStateWithAnswer: PropTypes.func.isRequired,
  triggerApiCall: PropTypes.func.isRequired,
  undo: PropTypes.func.isRequired,
  saveChatAnswer: PropTypes.func.isRequired,
  todoId: PropTypes.string.isRequired,
};

export default AnswerComponentFactory;
