import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Chat, { __RewireAPI__ } from './component';

configure({ adapter: new Adapter() });

const fakeRequiredProps = sandbox => ({
  navigate: sandbox.stub(),
  getChatTitle: sandbox.stub(),
  loadChatFromServer: sandbox.stub(),
  loading: false,
  match: {
    params: {
      todoId: '123',
    },
  },
  updateCurrentQuestionIndex: sandbox.stub(),
  showQuestionAnswer: false,
  chatPaused: false,
  triggerShowOfQuestionAnswer: sandbox.stub(),
  updateStateWithAnswer: sandbox.stub(),
  registerUser: sandbox.stub(),
  currentQuestionIndex: 0,
  activeChat: {
    chat: {
      questions: [
        {
          id: '1',
          content:
            'Welcome to Money Wizard, your personal financial coach.',
          pause: 0,
        },
      ],
    },
  },
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = { ...fakeRequiredProps(sandbox) };

  const target = mount(<Chat {...fakeProps} />);

  assert.equal(
    fakeProps.loadChatFromServer.callCount,
    1,
    'loadChatFromServer invoked'
  );

  assert.deepEqual(
    fakeProps.loadChatFromServer.args,
    [['123']],
    'loadChatFromServer invoked with todo id'
  );

  assert.end();
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
  };

  const target = mount(<Chat {...fakeProps} />);

  assert.equal(
    fakeProps.loadChatFromServer.callCount,
    1,
    'loadChatFromServer invoked'
  );

  assert.deepEqual(
    fakeProps.loadChatFromServer.args,
    [['123']],
    'loadChatFromServer invoked with todo id'
  );

  assert.end();
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const setTimeoutBackup = setTimeout;

  global.setTimeout = func => func();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: false,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'email',
          },
        ],
      },
    },
  };

  const target = mount(<Chat {...fakeProps} />);

  target.instance().componentDidUpdate();

  assert.equal(
    fakeProps.triggerShowOfQuestionAnswer.callCount,
    1,
    'triggerShowOfQuestionAnswer invoked when Q has answer type and showQuestionAnswer is false'
  );

  assert.end();

  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    chatPaused: false,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'email',
          },
        ],
      },
    },
  };

  const setTimeoutBackup = setTimeout;

  global.setTimeout = sandbox.stub();

  const target = mount(<Chat {...fakeProps} />);

  target.instance().componentDidUpdate();

  assert.equal(
    global.setTimeout.callCount,
    1,
    'setTimeout invoked when chat not paused and it is showing Q answer, i.e. its time to show the next Q'
  );

  global.setTimeout.args[0][0]();

  assert.equal(
    fakeProps.updateCurrentQuestionIndex.callCount,
    1,
    'updateCurrentQuestionIndex invoked inside set timeout'
  );
  assert.deepEqual(
    fakeProps.updateCurrentQuestionIndex.args,
    [[1]],
    'updateCurrentQuestionIndex invoked inside set timeout with index incremented'
  );
  assert.deepEqual(
    global.setTimeout.args[0][1],
    2500,
    'setTimeout pause set correctly'
  );

  assert.end();

  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'email',
            triggerRegistration: true,
          },
        ],
      },
    },
  };

  const setTimeoutBackup = setTimeout;

  global.setTimeout = func => func();

  const target = mount(<Chat {...fakeProps} />);

  target.find('AnswerComponentFactory').prop('triggerNextQuestion')();

  assert.equal(
    fakeProps.registerUser.callCount,
    1,
    'registerUser invoked trigger next question called and Q is set to trigger reg'
  );

  assert.end();

  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/money-wizard-beta',
  });

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'button',
            triggerRegistration: false,
            onClickRedirectPath: '/somewhere',
          },
        ],
      },
    },
  };

  const setTimeoutBackup = setTimeout;
  global.setTimeout = func => func();

  const target = mount(<Chat {...fakeProps} />);

  target.find('AnswerComponentFactory').prop('triggerNextQuestion')();

  assert.deepEqual(
    fakeProps.navigate.args,
    [['/money-wizard-beta/somewhere']],
    'navigate invoked to somewhere'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('WealthWizards');
  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const setTimeoutBackup = setTimeout;
  global.setTimeout = func => func();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'button',
          },
        ],
      },
    },
  };

  const target = mount(<Chat {...fakeProps} />);

  target.find('AnswerComponentFactory').prop('triggerNextQuestion')();

  assert.deepEqual(
    fakeProps.updateCurrentQuestionIndex.args,
    [[1]],
    'updateCurrentQuestionIndex invoked with incremented index when answer type is recaptcha and trigger next Q called'
  );

  assert.end();
  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const setTimeoutBackup = setTimeout;
  global.setTimeout = func => func();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    shouldWeShowMessageAfterUserAnswer: true,
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'button',
            contentToShowAfterAnswer: 'whos your daddy',
          },
        ],
      },
    },
  };

  const target = mount(<Chat {...fakeProps} />);

  assert.deepEqual(
    target.find('ChatQuestion').get(1).props,
    { content: 'whos your daddy', dataTest: 'chat-element-after-answer-0' },
    'shows data defined in contentToShowAfterAnswer'
  );

  assert.end();
  global.setTimeout = setTimeoutBackup;
});

test('<Chat>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    ...fakeRequiredProps(sandbox),
    showQuestionAnswer: true,
    shouldWeShowMessageAfterUserAnswer: false,
    showMessageAfterUserAnswer: sandbox.stub(),
    activeChat: {
      chat: {
        questions: [
          {
            id: '1',
            content: 'What is your email address?',
            pause: 2500,
            answerType: 'email',
            triggerRegistration: true,
            contentToShowAfterAnswer: 'big dog',
          },
        ],
      },
    },
  };

  const setTimeoutBackup = setTimeout;

  global.setTimeout = func => func();

  const target = mount(<Chat {...fakeProps} />);

  target.find('AnswerComponentFactory').prop('triggerNextQuestion')();

  assert.equal(
    fakeProps.showMessageAfterUserAnswer.callCount,
    1,
    'showMessageAfterUserAnswer invoked when showQuestionAnswer and shouldWeShowMessageAfterUserAnswer = false'
  );

  assert.end();

  global.setTimeout = setTimeoutBackup;
});
