import React, { Component } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import WealthWizards from '../../../../../modules/wealthwizards';
import Header from '../../elements/Header';
import Title from '../../elements/Title';
import Subtitle from '../../elements/Subtitle';
import Todo from '../../../../../modules/elements/Todo';

export default class ToDosComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
    getTodosFromServer: PropTypes.func.isRequired,
    toggleSideBar: PropTypes.func.isRequired,
    openExternalTodoLink: PropTypes.func.isRequired,
    todos: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    nextTodo: PropTypes.object,
    incompleteTodos: PropTypes.number.isRequired,
  };
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getTodosFromServer();
  }

  render() {
    if (this.props.loading) return null;
    const {
      incompleteTodos,
      todos,
      toggleSideBar,
      nextTodo,
      navigate,
      openExternalTodoLink,
    } = this.props;

    return (
      <div className="wrapper todo-wrapper">
        <Header
          title="To-Dos"
          incompleteTodos={incompleteTodos}
          toggleSidebar={toggleSideBar}
          title="Your To-Do items"
          incompleteTodos={incompleteTodos}
          onClickInbox={() => {
            if (nextTodo) {
              nextTodo.href
                ? openExternalTodoLink(nextTodo.id, nextTodo.href)
                : navigate(
                    `${WealthWizards.CONTEXT_ROUTE}/chat/${nextTodo.id}`
                  );
            }
          }}
        />

        <div className="todo-wrapper__title-section">
          <div className="todo-wrapper__desc-container">
            <Title
              className="todo-wrapper__title"
              title="How long have you got?"
            />
            <Subtitle
              className="todo-wrapper__description"
              title="A financial health check will take you about 10 minutes to
              complete"
            />
          </div>
        </div>

        {todos.map((todo, i) => (
          <Todo
            title={todo.title}
            hideCompletionDate
            hideduration={!R.is(Number, todo.duration)}
            duration={todo.duration}
            key={`todo-${i}`}
            buttonLabel={todo.status === 'COMPLETE' ? '' : 'Start'}
            description={todo.description}
            progress={todo.status === 'COMPLETE' ? 100 : 0}
            color={'#0F6b92'}
            action={() => {
              if (todo.status !== 'COMPLETE') {
                if (todo.href) {
                  openExternalTodoLink(todo.id, todo.href);
                } else {
                  this.props.navigate(
                    `${WealthWizards.CONTEXT_ROUTE}/chat/${todo.id}`
                  );
                }
              }
            }}
            dataTest={`todo-item-${i}`}
          />
        ))}
      </div>
    );
  }
}
