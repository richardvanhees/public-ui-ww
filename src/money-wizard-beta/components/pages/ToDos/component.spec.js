import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ToDos from './component';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const navigateStub = sandbox.stub();
const openExternalTodoLink = sandbox.stub();

const fakeRequiredProps = {
  todos: [
    {
      id: 'registration',
      title: 'Registration',
      description: "Let's get you registered",
      status: 'COMPLETE',
    },
    {
      id: 'getToKnowYou',
      title: 'Get to know you',
      description: "Let's get to know a bit so we can be friends",
      status: 'AVAILABLE',
    },
    {
      id: 'openExternalType',
      title: 'Open external app',
      description: "Let's open an external application",
      status: 'AVAILABLE',
      href: 'http://google.com',
    },
  ],
  navigate: () => navigateStub(),
  getTodosFromServer: () => ({}),
  openExternalTodoLink,
};

test('<ToDos>', t => {
  t.equals(typeof ToDos.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ToDos>', t => {
  const wrapper = mount(<ToDos {...fakeRequiredProps} />);

  t.equals(
    wrapper.html(),
    '<div class="wrapper todo-wrapper"><div class="header"><div class="header__settings"><img class="header__settings-img" src="[object Object]" alt="open menu"></div><div class="header__title">Your To-Do items</div><div class="header__inbox"><img class="header__inbox-img" src="[object Object]" alt="open inbox"></div></div><div class="todo-wrapper__title-section"><div class="todo-wrapper__desc-container"><h3 class="todo-wrapper__title">How long have you got?</h3><p class="todo-wrapper__description">A financial health check will take you about 10 minutes to complete</p></div></div><div class="todo"><div class="todo__icon-container"><div class="todo__circle"></div><img class="todo__check" src="[object Object]" alt="checkmark"></div><div class="todo__description-container"><div class="todo__title">Registration</div><div class="todo__description" data-test="todo-item-0-description" style="color: rgb(15, 107, 146);">Let\'s get you registered</div></div><div class="todo__progress-container"></div></div><div class="todo"><div class="todo__icon-container"><div class="todo__circle"></div></div><div class="todo__description-container"><div class="todo__title">Get to know you</div><div class="todo__description" data-test="todo-item-1-description" style="color: rgb(15, 107, 146);">Let\'s get to know a bit so we can be friends</div></div><div class="todo__progress-container"><button class="btn todo__button btn--outline " data-test="todo-item-1" style="color: rgb(15, 107, 146); border: 1px solid #0f6b92;">Start</button></div></div><div class="todo"><div class="todo__icon-container"><div class="todo__circle"></div></div><div class="todo__description-container"><div class="todo__title">Open external app</div><div class="todo__description" data-test="todo-item-2-description" style="color: rgb(15, 107, 146);">Let\'s open an external application</div></div><div class="todo__progress-container"><button class="btn todo__button btn--outline " data-test="todo-item-2" style="color: rgb(15, 107, 146); border: 1px solid #0f6b92;">Start</button></div></div></div>',
    'html correctly rendered'
  );

  const todoItemIncomplete = wrapper
    .find('button[data-test="todo-item-1"]')
    .simulate('click');

  const todoItemWithHref = wrapper
    .find('button[data-test="todo-item-2"]')
    .simulate('click');

  t.equals(
    navigateStub.callCount,
    1,
    'navigate stub only called when an incomplete item without href prop is clicked'
  );

  t.end();
});
