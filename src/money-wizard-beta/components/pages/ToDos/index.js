import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import {
  getTodos,
  incompleteTodos,
  lookupNextTodo,
} from '../../../modules/todos/selectors';
import {
  getTodosFromServer,
  openExternalTodoLink,
} from '../../../modules/todos/actions';
import { getShowmenu } from '../../../modules/sidebar/selectors';
import { toggleSidebar } from '../../../modules/sidebar/actions';
import ToDosComponent from './component';

const mapStateToProps = ({ todos, browser, sidebar }) => ({
  todos: getTodos(todos),
  incompleteTodos: incompleteTodos(todos),
  showMenu: getShowmenu(sidebar),
  loading: browser.loading,
  nextTodo: lookupNextTodo(todos.todos),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigate: page => push(page),
      getTodosFromServer: () => getTodosFromServer(),
      toggleSideBar: () => toggleSidebar(),
      openExternalTodoLink: (todoId, href) =>
        openExternalTodoLink(todoId, href),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToDosComponent);
