import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Login, { __RewireAPI__ } from './component';
import {
  getItem,
  removeItem,
} from '../../../../../modules/utils/local-storage';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Login>', t => {
  t.equals(typeof Login.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Login>', t => {
  const sandbox = sinon.sandbox.create();
  const navigateStub = sandbox.stub();
  const setIsLoadingStub = sandbox.stub();
  const setErrorStub = sandbox.stub();
  const setNotificationStub = sandbox.stub();

  const axiosStub = sandbox.stub();
  axiosStub.resolves();

  __RewireAPI__.__Rewire__('axios', axiosStub);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/money-wizard-beta',
  });

  const fakeRequiredProps = {
    navigate: navigateStub,
    setIsLoading: setIsLoadingStub,
    setError: setErrorStub,
    setNotification: setNotificationStub,
  };

  const wrapper = mount(<Login {...fakeRequiredProps} />);

  wrapper.setState({ emailAddress: 'test@support.com' });

  const signInButton = wrapper.find('button[data-test="sign-in-btn"]');

  signInButton.simulate('click');

  t.deepEqual(axiosStub.args, [
    [
      {
        data: { emailAddress: 'test@support.com' },
        headers: {
          'Cache-Control':
            'no-cache, no-store, must-revalidate, private, max-age=0',
        },
        json: true,
        method: 'POST',
        url: '/money-wizard-beta/v1/magic-link/request',
      },
    ],
  ]);

  setImmediate(() => {
    t.deepEqual(
      setNotificationStub.args,
      [
        [
          'Thanks, check your email for a magic link that will sign you in',
          'success',
          5000,
        ],
      ],
      'notification displayed'
    );
    t.deepEqual(
      setIsLoadingStub.args,
      [[true], [false]],
      'loading flag turned on and off'
    );

    t.end();

    __RewireAPI__.__ResetDependency__('axios');
    __RewireAPI__.__ResetDependency__('WealthWizards');
  });
});
