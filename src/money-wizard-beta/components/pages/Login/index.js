import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import {
  setError,
  setIsLoading,
  setNotification,
} from '../../../../../modules/browser/actions';

import SplashComponent from './component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigate: page => push(page),
      setError: msg => setError(msg),
      setIsLoading: loading => setIsLoading(loading),
      setNotification: (message, notificationType, flash = true) =>
        setNotification(message, notificationType, flash),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashComponent);
