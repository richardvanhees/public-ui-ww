import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Button from '../../../../../modules/elements/Button';
import Input from '../../../../../modules/elements/Input';
import Error from '../../../../../modules/elements/Error';
import WealthWizards from '../../../../../modules/wealthwizards';
import { getItem } from '../../../../../modules/utils/local-storage';

export default class SplashComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
    setError: PropTypes.func.isRequired,
    setIsLoading: PropTypes.func.isRequired,
    setNotification: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {
      emailAddress: '',
      magicLinkRequestError: false,
    };

    this.signIn = this.signIn.bind(this);
  }

  componentDidMount() {
    if (getItem('user_token')) {
      this.props.navigate(`${WealthWizards.CONTEXT_ROUTE}/todos`);
    }
  }

  signIn() {
    if (!this.state.emailAddress) return;

    this.props.setIsLoading(true);

    axios({
      url: `${WealthWizards.CONTEXT_ROUTE}/v1/magic-link/request`,
      method: 'POST',
      json: true,
      data: {
        emailAddress: this.state.emailAddress,
      },
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
      },
    })
      .then(() => {
        this.setState({
          magicLinkRequestError: false,
          emailAddress: '',
        });
        this.props.setIsLoading(false);
        this.props.setNotification(
          'Thanks, check your email for a magic link that will sign you in',
          'success',
          5000
        );
      })
      .catch(() => {
        this.props.setIsLoading(false);
        this.setState({ magicLinkRequestError: true });
      });
  }

  render() {
    return (
      <div className="wrapper login">
        <div className="login__get-started">
          <div className="login__form-container">
            <p className="login__text">Login</p>
            <Input
              wrapperClassName="login__input-wrapper"
              hintText="Please enter your email address"
              type="email"
              autoComplete="email"
              inputClassName="login__input-wrapper-input"
              value={this.state.emailAddress}
              onChange={e => this.setState({ emailAddress: e.target.value })}
              onKeyPress={event => {
                if (event.key === 'Enter') {
                  this.signIn();
                }
              }}
            />
            {this.state.magicLinkRequestError && (
              <Error
                compact
                title={'We are currently unable to send you a magic link'}
                active
              />
            )}
            <Button
              className="btn--primary login__sign-in-btn"
              label="Sign in"
              onClick={this.signIn}
              dataTest={'sign-in-btn'}
            />
          </div>
        </div>
      </div>
    );
  }
}
