import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Profile from './component';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Profile>', t => {

  t.equals(typeof Profile.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Profile>', t => {

  t.equals(shallow(<Profile {...fakeRequiredProps} />).find('.profile').length, 1, 'Profile is rendered');

  t.end();
});
