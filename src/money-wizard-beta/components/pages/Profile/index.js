import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';

import ProfileComponent from './component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => (bindActionCreators({
  navigate: page => push(page),
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(ProfileComponent);
