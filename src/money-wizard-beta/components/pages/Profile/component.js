import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ProfileComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    // const { navigate } = this.props;

    return (
      <div className="profile">
        Profile placeholder
      </div>
    );
  }
}
