import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Error from './component';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Error>', t => {
  t.equals(typeof Error.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Error>', t => {
  t.equals(
    shallow(<Error {...fakeRequiredProps} />).find('.error-view').length,
    1,
    'Error is rendered'
  );

  t.end();
});
