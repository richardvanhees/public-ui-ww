import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { goBack } from 'react-router-redux';
import ErrorComponent from './component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: goBack,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorComponent);
