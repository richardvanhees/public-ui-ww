import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import { error } from '../../../../../assets/images/money-wizard-beta';

export default class ErrorComponent extends Component {
  static propTypes = {
    back: PropTypes.func.isRequired,
  };
  static defaultProps = {};

  render() {
    const { back } = this.props;
    return (
      <div className="wrapper error-view">
        <div className="error-view__icon">
          <img className="error-view__icon-img" src={error} alt="error" />
        </div>
        <p>Something has gone wrong</p>
        <Button
          className="btn--primary error-view__btn"
          label="Try again"
          onClick={() => back()}
          dataTest={'try-again-btn'}
        />
      </div>
    );
  }
}
