import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { setError } from '../../../../../modules/browser/actions';

import ClaimLinkAndLoginComponent from './ClaimLinkAndLoginComponent';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigate: page => push(page),
      setError: msg => setError(msg),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClaimLinkAndLoginComponent);
