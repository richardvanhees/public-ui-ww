import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Spinner from '../../../../../modules/elements/Spinner';
import WealthWizards from '../../../../../modules/wealthwizards';
import {
  setItem,
  removeItem,
} from '../../../../../modules/utils/local-storage';
import { parse } from 'querystring-es3';

export default class ChatComponent extends Component {
  static propTypes = {
    navigate: PropTypes.func.isRequired,
    setError: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
  };
  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  componentDidMount() {
    const uuid = parse(this.props.location.search)['?uuid'];

    if (!uuid) {
      return this.props.navigate(`${WealthWizards.CONTEXT_ROUTE}/`);
    }

    removeItem('user_token');
    removeItem('refresh_token');

    return axios({
      url: `${WealthWizards.CONTEXT_ROUTE}/v1/magic-link/claim`,
      method: 'POST',
      json: true,
      data: {
        uuid,
      },
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
      },
    })
      .then(({ data }) => {
        setItem('user_token', data.jwt);
        setItem('refresh_token', data.refresh_token);
        window.location = `${WealthWizards.CONTEXT_ROUTE}/todos`;
      })
      .catch(() => {
        this.props.navigate(`${WealthWizards.CONTEXT_ROUTE}/`);
        this.props.setError(
          'We are currenly unable to sign you in, please try again.'
        );
      });
  }

  render() {
    return this.state.loading ? <Spinner visible /> : null;
  }
}
