import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatInputField, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    done: sandbox.stub(),
    answerValue: 2,
    type: 'number',
    enabled: false,
    undoEnabled: true,
    undo: () => ({}),
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  assert.true(
    target.find('ChatAnswerPlayback').exists(),
    'ChatAnswerPlayback exists'
  );

  assert.end();
});

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    done: sandbox.stub(),
    answerValue: 2,
    type: 'number',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  assert.true(target.instance().validateInput(), 'valid number');

  assert.end();
});

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    done: sandbox.stub(),
    answerValue: '2345',
    type: 'number',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  assert.true(target.instance().validateInput(), 'valid number');

  assert.end();
});

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    done: sandbox.stub(),
    answerValue: 'asdasd',
    type: 'number',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  assert.false(target.instance().validateInput(), 'invalid number');

  assert.end();
});

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake-data-test',
    done: sandbox.stub(),
    answerValue: '123',
    type: 'number',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  target.find('Button').prop('onClick')();

  assert.deepEqual(
    fakeProps.done.args,
    [[123]],
    'parses string to number when valid'
  );

  assert.end();
});

test('<ChatInputField>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake-data-test',
    done: sandbox.stub(),
    answerValue: '123',
    type: 'email',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatInputField {...fakeProps} />);

  assert.false(target.instance().validateInput(), 'catches invalid email');

  target.find('Button').prop('onClick')();

  assert.deepEqual(
    target.instance().state,
    {
      errors: { email: true },
      value: '123',
    },
    'email error set'
  );

  assert.end();
});
