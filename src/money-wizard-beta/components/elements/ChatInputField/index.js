import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import Error from '../../../../../modules/elements/Error';
import ChatAnswerPlayback from '../ChatAnswerPlayback';
import { emailIsValid } from '../../../../../modules/utils/regexes';

export default class ChatInputField extends React.PureComponent {
  static propTypes = {
    dataTest: PropTypes.string.isRequired,
    done: PropTypes.func.isRequired,
    undo: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    enabled: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    answerValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    answerTemplate: PropTypes.string,
    undoEnabled: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { value: props.answerValue, errors: {} };
    this.validateInput = this.validateInput.bind(this);
  }

  validateInput() {
    if (this.props.type === 'email' && !emailIsValid(this.state.value)) {
      this.setState({ errors: { email: true } });
      return false;
    }

    if (
      this.props.type === 'number' &&
      (this.state.value === undefined ||
        this.state.value === '' ||
        isNaN(this.state.value))
    ) {
      this.setState({ errors: { number: true } });
      return false;
    }

    if (
      this.props.type === 'text' &&
      (!this.state.value || this.state.value.trim() === '')
    ) {
      this.setState({ errors: { text: true } });
      return false;
    }

    this.setState({ errors: { email: false } });
    return true;
  }

  render() {
    const {
      dataTest,
      enabled,
      done,
      placeholder,
      answerTemplate,
      undo,
      undoEnabled,
      answerValue,
    } = this.props;

    return (
      <div className="chat-input">
        <div className="chat-input__wrapper">
          <input
            ref={el => {
              if (el && enabled) {
                el.focus();
              }
            }}
            data-test={dataTest}
            placeholder={placeholder}
            value={answerValue}
            onChange={e => {
              this.setState({ value: e.target.value });
              this.props.onChange(
                this.props.type === 'number'
                  ? parseFloat(e.target.value)
                  : e.target.value
              );
            }}
            disabled={!enabled}
            type={'text'}
            className="chat-input__input"
            onKeyPress={event => {
              if (event.key === 'Enter' && enabled && this.validateInput()) {
                done();
              }
            }}
          />
          <Button
            className="chat-button--primary chat-input__button"
            label="Ok"
            onClick={() =>
              enabled &&
              this.validateInput() &&
              done(
                this.props.type === 'number'
                  ? parseFloat(this.state.value)
                  : this.state.value
              )
            }
            dataTest={`${dataTest}-submit-button`}
          />
        </div>
        <Error
          compact
          title="Please enter a valid email"
          active={this.state.errors.email}
          className="chat-input__error"
        />
        <Error
          compact
          title="Please enter a number"
          active={this.state.errors.number}
          className="chat-input__error"
        />
        <Error
          compact
          title="Please answer the question"
          active={this.state.errors.text}
          className="chat-input__error"
        />
        {!enabled && (
          <ChatAnswerPlayback
            answerTemplate={answerTemplate}
            answer={this.state.value}
            undo={undo}
            undoEnabled={undoEnabled}
          />
        )}
      </div>
    );
  }
}
