import React from 'react';
import { logo } from '../../../../../assets/images/money-wizard-beta';

const Footer = () => (
  <div className="sidebar__footer">
    <div className="sidebar__footer-logo">
      <p className="sidebar__footer-logo-text">Powered by</p>
      <img className="sidebar__footer-logo-img" src={logo} alt="logo" />
    </div>
    <p className="sidebar__footer-text">
      <a
        className={'link footer__link'}
        target="_blank"
        href="https://www.wealthwizards.com/cookie-policy/"
      >
        View cookie policy
      </a>
    </p>
    <p className="sidebar__footer-text">
      <a
        className={'link footer__link'}
        target="_blank"
        href="https://www.wealthwizards.com/privacy-policy/"
      >
        View privacy policy
      </a>
    </p>

    <p className="sidebar__footer-text">Copyright @ 2018 Wealth Wizards</p>
    <p className="sidebar__footer-text">
      Wealth Wizards Advisers Limited is authorised and regulated by the
      Financial Conduct Authority and is entered on the Financial Services
      Register under reference 596436.
    </p>
    <p className="sidebar__footer-text">
      Registered Address: Wizards House, 8 Athena Court, Tachbrook Park,
      Leamington Spa, CV34 6RT.
    </p>
  </div>
);

export default Footer;
