import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Sidebar, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

test('<Sidebar>', assert => {
  const sandbox = sinon.sandbox.create();
  const fakeProps = {
    logout: sandbox.stub(),
    toggleSideBar: sandbox.stub(),
    navigate: sandbox.stub(),
  };

  const target = mount(<Sidebar {...fakeProps} />);

  assert.equals(
    target.html(),
    '<div class="sidebar sidebar-hide"><div class="sidebar__header"><div class="sidebar__header-back"><img src="[object Object]" alt="close menu"></div><div class="sidebar__header-title">Profile</div><div class="sidebar__header-padding"></div></div><div class="sidebar__profile"><div class="sidebar__profile-image"><img class="sidebar__profile-image-img" src="[object Object]" alt="profile"></div></div><div class="sidebar__buttons"><button class="btn sidebar__buttons-btn btn--outline " data-test="home-btn">Home</button><button class="btn sidebar__buttons-btn sidebar__buttons-btn--margin-top btn--outline " data-test="help-and-support-btn">Help &amp; support</button><button class="btn sidebar__buttons-btn sidebar__buttons-btn--margin-top btn--outline " data-test="sign-out-btn">Sign out</button></div><div class="sidebar__footer"><div class="sidebar__footer-logo"><p class="sidebar__footer-logo-text">Powered by</p><img class="sidebar__footer-logo-img" src="[object Object]" alt="logo"></div><p class="sidebar__footer-text"><a class="link footer__link" target="_blank" href="https://www.wealthwizards.com/cookie-policy/">View cookie policy</a></p><p class="sidebar__footer-text"><a class="link footer__link" target="_blank" href="https://www.wealthwizards.com/privacy-policy/">View privacy policy</a></p><p class="sidebar__footer-text">Copyright @ 2018 Wealth Wizards</p><p class="sidebar__footer-text">Wealth Wizards Advisers Limited is authorised and regulated by the Financial Conduct Authority and is entered on the Financial Services Register under reference 596436.</p><p class="sidebar__footer-text">Registered Address: Wizards House, 8 Athena Court, Tachbrook Park, Leamington Spa, CV34 6RT.</p></div></div>',
    'sidebar html correctly rendered'
  );

  const homeBtn = target.find('button[data-test="home-btn"]');
  const logoutBtn = target.find('button[data-test="sign-out-btn"]');

  homeBtn.simulate('click');

  assert.equals(
    fakeProps.toggleSideBar.callCount,
    1,
    'sidebar toggled when home button clicked'
  );
  assert.equals(
    fakeProps.navigate.callCount,
    1,
    'navigate stub called when clicked on home button'
  );

  logoutBtn.simulate('click');

  assert.equals(
    fakeProps.toggleSideBar.callCount,
    2,
    'sidebar toggled when logout button clicked'
  );
  assert.equals(
    fakeProps.logout.callCount,
    1,
    'navigate stub called when clicked on home button'
  );

  assert.end();
});
