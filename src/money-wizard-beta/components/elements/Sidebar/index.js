import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { back, profile } from '../../../../../assets/images/money-wizard-beta';
import Button from '../../../../../modules/elements/Button';
import Footer from '../Footer';
import WealthWizards from '../../../../../modules/wealthwizards';

export default class SidebarComponent extends PureComponent {
  static propTypes = {
    logout: PropTypes.func.isRequired,
    toggleSideBar: PropTypes.func.isRequired,
    startIntercom: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
    downloadCountdownReport: PropTypes.func.isRequired,
    email: PropTypes.string.isRequired,
    countdownDocument: PropTypes.object,
  };

  static defaultProps = {};

  render() {
    const {
      logout,
      toggleSideBar,
      startIntercom,
      navigate,
      email,
      countdownDocument,
      downloadCountdownReport,
    } = this.props;

    return (
      <div
        className={`sidebar ${
          this.props.showMenu ? 'sidebar-show' : 'sidebar-hide'
        }`}
      >
        <div className="sidebar__header">
          <div
            className="sidebar__header-back"
            onClick={() => {
              toggleSideBar();
            }}
          >
            <img src={back} alt="close menu" />
          </div>
          <div className="sidebar__header-title">Profile</div>
          <div className="sidebar__header-padding" />
        </div>

        <div className="sidebar__profile">
          <div className="sidebar__profile-image">
            <img
              className="sidebar__profile-image-img"
              src={profile}
              alt="profile"
            />
          </div>
          {email && <p className="sidebar__profile-email">{email}</p>}
        </div>

        {countdownDocument && (
          <div className="sidebar__countdown-download-container">
            <a
              onClick={() => downloadCountdownReport(countdownDocument.href)}
              className="sidebar__countdown-download-container-link"
            >
              Your Pension Income Options (PDF)
            </a>
          </div>
        )}

        <div className="sidebar__buttons">
          <Button
            label="Home"
            className="sidebar__buttons-btn"
            onClick={() => {
              navigate(`${WealthWizards.CONTEXT_ROUTE}/todos`);
              toggleSideBar();
            }}
            invertOnHover
            dataTest="home-btn"
          />
          <Button
            label="Help & support"
            className="sidebar__buttons-btn sidebar__buttons-btn--margin-top"
            onClick={() => {
              startIntercom();
              toggleSideBar();
            }}
            invertOnHover
            dataTest="help-and-support-btn"
          />
          <Button
            label="Sign out"
            className="sidebar__buttons-btn sidebar__buttons-btn--margin-top"
            onClick={() => {
              logout();
              toggleSideBar();
            }}
            invertOnHover
            dataTest="sign-out-btn"
          />
        </div>

        <Footer />
      </div>
    );
  }
}
