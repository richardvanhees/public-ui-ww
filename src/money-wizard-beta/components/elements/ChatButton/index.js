import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';

const ChatButton = ({ dataTest, onClick, enabled, label }) => (
  <div className="chat-button">
    <Button
      className="chat-button--tertiary"
      label={label}
      onClick={() => enabled && onClick()}
      dataTest={dataTest}
      enabled={enabled}
    />
  </div>
);

ChatButton.propTypes = {
  dataTest: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  enabled: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
};

export default ChatButton;
