import React from 'react';
import PropTypes from 'prop-types';
import ReactHTMLParser from 'react-html-parser';
import interpolate from '../../../../../modules/utils/interpolate';

const ChatAnswerPlayback = ({ answerTemplate, answer, undo, undoEnabled }) => (
  <div className="chat-answer-playback">
    {undoEnabled && (
      <a
        data-test="a-undo"
        onClick={undo}
        className="chat-answer-playback-container__undo"
      >
        Undo
      </a>
    )}
    <div className="chat-answer-playback-container">
      <p className="chat-answer-playback-container__content">
        {ReactHTMLParser(interpolate(answerTemplate, { answer }))}
      </p>
    </div>
  </div>
);

ChatAnswerPlayback.propTypes = {
  answerTemplate: PropTypes.string,
  answer: PropTypes.any.isRequired,
  undo: PropTypes.func.isRequired,
  undoEnabled: PropTypes.bool.isRequired,
};

ChatAnswerPlayback.defaultProps = {
  answerTemplate: '{{answer}}',
};

export default ChatAnswerPlayback;
