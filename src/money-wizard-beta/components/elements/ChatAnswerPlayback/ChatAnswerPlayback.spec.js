import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatAnswerPlayback, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

test('<ChatAnswerPlayback>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    answerTemplate: 'You answered: {{answer}}',
    answer: 3,
    undo: sandbox.stub(),
    undoEnabled: true,
  };

  const target = mount(<ChatAnswerPlayback {...fakeProps} />);

  assert.true(
    target.find('a[data-test="a-undo"]').exists(),
    'undo link exists'
  );

  target.find('a[data-test="a-undo"]').prop('onClick')();

  assert.equal(fakeProps.undo.callCount, 1, 'undo invoked');

  assert.end();
});

test('<ChatAnswerPlayback>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    answerTemplate: 'You answered: {{answer}}',
    answer: 3,
    undo: sandbox.stub(),
    undoEnabled: false,
  };

  const target = mount(<ChatAnswerPlayback {...fakeProps} />);

  assert.false(
    target.find('a[data-test="a-undo"]').exists(),
    'undo link doesnt exist'
  );

  assert.end();
});
