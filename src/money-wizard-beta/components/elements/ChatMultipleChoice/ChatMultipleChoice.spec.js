import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatMultipleChoice, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

test('<ChatMultipleChoice>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    onSelect: sandbox.stub(),
    choices: [
      {
        id: '1',
        label: 'label 1',
        nextQuestionId: 'nextQuestionId',
      },
      {
        id: '2',
        label: 'label 2',
      },
    ],
    answerValue: '1',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatMultipleChoice {...fakeProps} />);

  assert.equals(target.find('Button').length, 2, 'two buttons rendered');

  target
    .find('Button')
    .first()
    .prop('onClick')();

  assert.deepEqual(
    fakeProps.onSelect.args,
    [
      [
        {
          id: '1',
          label: 'label 1',
          nextQuestionId: 'nextQuestionId',
        },
      ],
    ],
    'selected choice passed to onSelect function'
  );

  assert.end();
});

test('<ChatMultipleChoice>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    onSelect: sandbox.stub(),
    choices: [
      {
        id: '1',
        label: 'label 1',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '2',
        label: 'label 2',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '3',
        label: 'label 3',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '4',
        label: 'label 4',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
    ],
    answerValue: '1',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatMultipleChoice {...fakeProps} />);

  assert.equals(
    target.find('Button').length,
    0,
    'zero buttons rendered, as and imgPath is present'
  );

  assert.equals(
    target.find('.chat-button__container').length,
    4,
    '4 answer containers rendered, as and imgPath is present'
  );

  assert.equals(
    target.find('.chat-button__img').length,
    4,
    '4 answer images rendered, as and imgPath is present'
  );

  assert.end();
});

test('<ChatMultipleChoice>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    onSelect: sandbox.stub(),
    choices: [
      {
        id: '1',
        label: 'label 1',
        nextQuestionId: 'nextQuestionId',
      },
      {
        id: '2',
        label: 'label 2',
      },
    ],
    answerValue: '1',
    enabled: false,
    answerTemplate: 'You answered: {{answer}}',
    selected: { id: '1' },
  };

  const target = mount(<ChatMultipleChoice {...fakeProps} />);

  assert.equals(
    target.find('Button').length,
    2,
    'buttons are still rendered when not enabled'
  );

  assert.end();
});

test('<ChatMultipleChoice>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    onSelect: sandbox.stub(),
    choices: [
      {
        id: '1',
        label: 'label 1',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '2',
        label: 'label 2',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '3',
        label: 'label 3',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
      {
        id: '4',
        label: 'label 4',
        nextQuestionId: 'nextQuestionId',
        imgPath: '../fakepath.png',
      },
    ],
    isMobile: true,
    answerValue: '1',
    enabled: true,
    answerTemplate: 'You answered: {{answer}}',
  };

  const target = mount(<ChatMultipleChoice {...fakeProps} />);

  assert.equals(
    target.find('Button').length,
    4,
    '4 buttons rendered, as and imgPath is present but isMobile is true'
  );

  assert.equals(
    target.find('.chat-button__img').length,
    0,
    '0 answer images rendered, as and imgPath is present but isMobile is true'
  );

  assert.end();
});
