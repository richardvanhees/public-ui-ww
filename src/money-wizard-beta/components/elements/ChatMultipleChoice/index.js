import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import ChatAnswerPlayback from '../ChatAnswerPlayback';
import R from 'ramda';

const createAnswer = (
  choice,
  index,
  dataTest,
  enabled,
  onSelect,
  selected,
  isMobile
) => {
  let answer;
  const selectedId = R.pathOr(null, ['id'])(selected);
  const choiceId = R.pathOr(null, ['id'])(choice);
  if (choice.imgPath && !isMobile) {
    answer = (
      <div
        className={`chat-button__container ${
          selectedId === choiceId ? 'chat-button__container--selected' : ''
        }
        ${!enabled ? 'chat-button__container--disabled' : ''}`}
        onClick={() => {
          enabled && onSelect(choice);
        }}
        key={`choice-${index}`}
      >
        <p className={'chat-button__label'} data-test={dataTest}>
          {choice.label}
        </p>
        <img
          className={'chat-button__img'}
          src={choice.imgPath}
          alt={choice.label}
        />
      </div>
    );
  } else {
    answer = (
      <Button
        className={`chat-button--secondary chat-multiple-choice__button ${
          selectedId === choiceId
            ? 'chat-multiple-choice__button--selected'
            : ''
        }`}
        label={choice.label}
        onClick={() => {
          enabled && onSelect(choice);
        }}
        dataTest={dataTest}
        enabled={enabled}
        key={`choice-${index}`}
      />
    );
  }
  return answer;
};

const ChatMultipleChoice = ({
  dataTest,
  onSelect,
  choices,
  answerValue,
  enabled,
  answerTemplate,
  undo,
  undoEnabled,
  isMobile,
}) => {
  if (!choices) {
    console.error(
      // eslint-disable-line no-console
      'No choices defined for multiple choice Q, something is wrong here'
    );
  }
  const selectedChoice = choices.find(choice => choice.id === answerValue);

  return (
    <div className="chat-multiple-choice">
      <div className="chat-multiple-choice__wrapper">
        {choices.map((choice, index) =>
          createAnswer(
            choice,
            index,
            dataTest,
            enabled,
            onSelect,
            selectedChoice,
            isMobile
          )
        )}
      </div>
      {!enabled &&
        selectedChoice && (
          <ChatAnswerPlayback
            answerTemplate={answerTemplate}
            answer={selectedChoice.label}
            undo={undo}
            undoEnabled={undoEnabled}
          />
        )}
    </div>
  );
};

ChatMultipleChoice.propTypes = {
  dataTest: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  undo: PropTypes.func.isRequired,
  choices: PropTypes.array.isRequired,
  answerValue: PropTypes.string,
  enabled: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
  answerTemplate: PropTypes.string,
  undoEnabled: PropTypes.bool.isRequired,
};

export default ChatMultipleChoice;
