import React from 'react';
import PropTypes from 'prop-types';
import { face, inbox } from '../../../../../assets/images/money-wizard-beta';

const Header = ({ title, incompleteTodos, onClickInbox, toggleSidebar }) => (
  <div className="header">
    <div className="header__settings" onClick={() => toggleSidebar()}>
      <img className="header__settings-img" src={face} alt="open menu" />
    </div>
    <div className="header__title">{title}</div>
    <div
      className="header__inbox"
      onClick={() => incompleteTodos > 0 && onClickInbox && onClickInbox()}
    >
      {incompleteTodos > 0 && (
        <div className="header__inbox-notification">{incompleteTodos}</div>
      )}
      <img className="header__inbox-img" src={inbox} alt="open inbox" />
    </div>
  </div>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  incompleteTodos: PropTypes.number.isRequired,
  onClickInbox: PropTypes.func,
  toggleSidebar: PropTypes.func,
};

export default Header;
