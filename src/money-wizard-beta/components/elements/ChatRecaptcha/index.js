import React from 'react';
import PropTypes from 'prop-types';
import ReCaptcha from '../../../../../modules/elements/ReCaptcha';
import ChatAnswerPlayback from '../ChatAnswerPlayback';

const ChatRecaptcha = ({
  onExpire,
  onVerify,
  recaptchaKey,
  enabled,
  showFakeRecaptcha,
}) => {
  if (!enabled) {
    return (
      <ChatAnswerPlayback answerTemplate={'Looks like you\'re a real human 😀'} answer={''} />
    );
  }

  return (
    <div className="chat-recaptcha">
      <div className="chat-recaptcha__wrapper">
        {// eslint-disable-next-line
        showFakeRecaptcha ? (
          <button onClick={() => onVerify('fake recaptcha')}>
            Continue (this is fake recaptcha)
          </button>
        ) : enabled ? (
          <ReCaptcha
            siteKey={recaptchaKey}
            onExpire={onExpire}
            onVerify={onVerify}
          />
        ) : (
          <p>Recaptcha complete</p>
        )}
      </div>
    </div>
  );
};

ChatRecaptcha.propTypes = {
  onExpire: PropTypes.func.isRequired,
  onVerify: PropTypes.func.isRequired,
  recaptchaKey: PropTypes.string.isRequired,
  enabled: PropTypes.bool.isRequired,
  showFakeRecaptcha: PropTypes.bool.isRequired,
};

export default ChatRecaptcha;
