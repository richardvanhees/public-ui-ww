import React from 'react';
import PropTypes from 'prop-types';
import ReactHTMLParser from 'react-html-parser';

const replayRecaptchaQuestion =
  'It looks as if the recaptcha has expired, please try again';

const ChatQuestion = ({ content, showLoadingDot, replayRecaptcha, avatar }) => {
  // prevent odd state with timeouts occuring
  if (!content && !replayRecaptcha) return null;
  return (
    <div>
      <div className="chat-question__outer-container">
        <div className="chat-question__avatar-container">
          {avatar && (
            <div className="chat-question__avatar">
              <img
                className="chat-question__avatar-img"
                src={avatar.imgPath}
                alt="avatar"
              />
              <p className="chat-question__avatar-name">{avatar.name}</p>
            </div>
          )}
        </div>
        <div className="chat-question">
          <p className="chat-question__content">
            {replayRecaptcha
              ? replayRecaptchaQuestion
              : ReactHTMLParser(content)}
          </p>
        </div>
      </div>
      {showLoadingDot && (
        <div className="chat-question__loading-dot-container">
          <span className="chat-question__loading-dot one">.</span>
          <span className="chat-question__loading-dot two">.</span>
          <span className="chat-question__loading-dot three">.</span>
        </div>
      )}
    </div>
  );
};

ChatQuestion.propTypes = {
  content: PropTypes.string.isRequired,
  showLoadingDot: PropTypes.bool,
  replayRecaptcha: PropTypes.bool,
  avatar: PropTypes.object,
};

export default ChatQuestion;
