import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatQuestion from './index';

configure({ adapter: new Adapter() });

test('<ChatQuestion>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    content: 'fake question content',
    showLoadingDot: false,
    replayRecaptcha: false,
    avatar: {
      imgPath: 'fake path',
      name: 'bruno',
    },
  };

  const target = mount(<ChatQuestion {...fakeProps} />);

  assert.equals(
    target.find('.chat-question__outer-container').length,
    1,
    'question container found'
  );

  assert.equals(
    target.find('.chat-question__avatar').length,
    1,
    'avatar container found when avatar object passed in props'
  );

  assert.end();
});

test('<ChatQuestion>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    content: 'fake question content',
    showLoadingDot: false,
    replayRecaptcha: false,
    avatar: false,
  };

  const target = mount(<ChatQuestion {...fakeProps} />);

  assert.equals(
    target.find('.chat-question__avatar').length,
    0,
    'avatar container NOT found when avatar object NOT passed in props'
  );

  assert.end();
});
