import React from 'react';
import PropTypes from 'prop-types';

const Subtitle = ({ title, className }) => (
  <p className={`${className}`}>{title}</p>
);

Subtitle.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Subtitle;
