import React from 'react';
import PropTypes from 'prop-types';

const Title = ({ title, className }) => (
  <h3 className={`${className}`}>{title}</h3>
);

Title.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
};

export default Title;
