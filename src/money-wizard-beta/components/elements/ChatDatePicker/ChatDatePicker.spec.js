import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ChatDatePicker, { __RewireAPI__ } from './index';

configure({ adapter: new Adapter() });

test('<ChatDatePicker>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake data test',
    done: sandbox.stub(),
    answerValue: '',
    enabled: false,
    undoEnabled: true,
    undo: () => ({}),
    answerTemplate: 'You answered: {{answer}}',
    onChange: () => ({}),
  };

  const target = mount(<ChatDatePicker {...fakeProps} />);

  assert.true(
    target.find('ChatAnswerPlayback').exists(),
    'ChatAnswerPlayback exists'
  );

  assert.end();
});

test('<ChatDatePicker>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeProps = {
    dataTest: 'fake-data-test',
    done: sandbox.stub(),
    answerValue: '',
    enabled: true,
    undoEnabled: true,
    undo: () => ({}),
    answerTemplate: 'You answered: {{answer}}',
    onChange: () => ({}),
  };

  const target = mount(<ChatDatePicker {...fakeProps} />);

  assert.false(target.instance().isValid(), 'catches empty input');

  target.find('Button').prop('onClick')();

  assert.deepEqual(
    target.instance().state,
    {
      displayValue: 'Invalid date',
      error: true,
      value: '',
    },
    'input missing'
  );

  assert.end();
});
