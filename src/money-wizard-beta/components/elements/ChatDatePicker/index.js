import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import Error from '../../../../../modules/elements/Error';
import ChatAnswerPlayback from '../ChatAnswerPlayback';
import DatePicker from '../../../../../modules/elements/DatePicker';
import R from 'ramda';

export default class ChatDatePicker extends React.PureComponent {
  static propTypes = {
    dataTest: PropTypes.string.isRequired,
    done: PropTypes.func.isRequired,
    undo: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    enabled: PropTypes.bool.isRequired,
    answerValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    answerTemplate: PropTypes.string,
    undoEnabled: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.answerValue,
      displayValue: null,
      error: null,
    };
  }

  isValid = () => {
    const value = this.state.value;
    const result = R.not(R.isEmpty(value) || R.isNil(value));
    this.setState({
      error: !result,
    });
    return result;
  };

  render() {
    const {
      dataTest,
      enabled,
      done,
      placeholder,
      answerTemplate,
      undo,
      undoEnabled,
    } = this.props;

    return (
      <div className="chat-date-picker">
        <div className="chat-date-picker__wrapper">
          <DatePicker
            ref={el => {
              if (el && enabled) {
                el.focus();
              }
            }}
            className={'chat-date-picker__input'}
            name={'datePicker'}
            placeholder={placeholder}
            onChange={value => {
              // this value is formatted
              this.setState({ value });
              this.props.onChange(value);
            }}
            updatedDisplayValue={value => {
              this.setState({
                displayValue: value,
              });
            }}
            initialValue={this.state.value}
            dataTest={dataTest}
            pastOnly
            displayFormat={'DD-MM-YYYY'}
          />
          <Button
            className="chat-button--primary chat-date-picker__button"
            label="Ok"
            onClick={() => enabled && this.isValid() && done(this.state.value)}
            dataTest={`${dataTest}-submit-button`}
          />
        </div>

        <Error
          compact
          title="Please answer the question"
          active={this.state.error}
          className="chat-date-picker__error"
        />
        {!enabled && (
          <ChatAnswerPlayback
            answerTemplate={answerTemplate}
            answer={this.state.displayValue || ''}
            undo={undo}
            undoEnabled={undoEnabled}
          />
        )}
      </div>
    );
  }
}
