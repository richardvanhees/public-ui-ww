import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Popup from '../../../modules/elements/Popup';
import Sidebar from './elements/Sidebar';
import Spinner from '../../../modules/elements/Spinner';
import Notification from '../../../modules/elements/Notification';

export default class AppWrapperComponent extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    history: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired,
    toggleSideBar: PropTypes.func.isRequired,
    startIntercom: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool,
    showMenu: PropTypes.bool,
    resetError: PropTypes.func,
    countdownDocument: PropTypes.object,
    downloadCountdownReport: PropTypes.func.isRequired,
  };

  componentDidUpdate() {
    if (this.errorPopup && this.props.errorMsg !== '') {
      this.errorPopup.toggle(true);
    }
  }

  render() {
    const {
      children,
      loading,
      closeNotification,
      notification,
      resetError,
      logout,
      showMenu,
      toggleSideBar,
      startIntercom,
      navigate,
      email,
      countdownDocument,
      downloadCountdownReport,
    } = this.props;
    return (
      <div className={'app-wrapper'}>
        <Notification {...notification} close={closeNotification} />
        {loading && <Spinner visible />}
        <Sidebar
          logout={logout}
          showMenu={showMenu}
          toggleSideBar={toggleSideBar}
          startIntercom={startIntercom}
          navigate={navigate}
          email={email}
          countdownDocument={countdownDocument}
          downloadCountdownReport={downloadCountdownReport}
        />
        <Popup
          ref={c => {
            this.errorPopup = c;
          }}
          isError
          closeButtonTop
          closeClickOutside
          buttons={[
            {
              label: 'Close',
              onClick: () => {
                this.errorPopup.toggle(false);
                resetError();
              },
            },
          ]}
        >
          {this.props.errorMsg}
        </Popup>
        {children}
      </div>
    );
  }
}
