import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Provider, connect } from 'react-redux';
import { Route } from 'react-router';
import { ConnectedRouter, push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { changeBreakpoint, breakpoint } from './../../modules/browser';
import WealthWizards from '../../modules/wealthwizards';

import AppWrapperContainer from './components/AppWrapperContainer';
import LoginContainer from './components/pages/Login';
import SplashContainer from './components/pages/Splash';
import ChatContainer from './components/pages/Chat';
import ToDosContainer from './components/pages/ToDos';
import ProfileContainer from './components/pages/Profile';
import ClaimLinkAndLoginContainer from './components/pages/Auth';
import ErrorContainer from './components/pages/Error';

class App extends PureComponent {
  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      currentBreakpoint: props.browser.breakpoint,
      offline: props.browser.offline,
    };
  }

  checkBreakpoint = () => {
    const currentBreakpoint = breakpoint();

    if (this.state.currentBreakpoint !== currentBreakpoint) {
      this.setState({ currentBreakpoint });
      this.props.setBreakpoint(currentBreakpoint);
    }
  };

  userIsTabbing = e =>
    e.keyCode === 9 && document.body.classList.add('user-is-tabbing');

  userIsNotTabbing = () => document.body.classList.remove('user-is-tabbing');

  componentDidMount() {
    this.checkBreakpoint();
    window.addEventListener('resize', this.checkBreakpoint);
    window.addEventListener('keydown', this.userIsTabbing);
    window.addEventListener('mousedown', this.userIsNotTabbing);
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <AppWrapperContainer history={this.props.history}>
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/`}
              component={SplashContainer}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/login`}
              component={LoginContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/chat/:todoId`}
              component={ChatContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/todos`}
              component={ToDosContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/profile`}
              component={ProfileContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/v1/claim`}
              component={ClaimLinkAndLoginContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/error`}
              component={ErrorContainer}
            />
          </AppWrapperContainer>
        </ConnectedRouter>
      </Provider>
    );
  }
}

const mapStateToProps = state => ({
  state,
  browser: state.browser,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      push,
      setBreakpoint: changeBreakpoint,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
