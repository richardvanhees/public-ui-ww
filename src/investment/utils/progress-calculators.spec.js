import test from 'tape';
import {
  progressQuestionCounts,
  calculateQuestionProgress,
  progressResilienceCounts,
  progressAboutCounts,
  progressFinalChecksCounts,
  progressISACounts,
} from './progress-calculators';

const testData = {
  a2risk__describe_as_cautious: 'no_opinion',
  a2risk__comfortable_investing: 'disagree',
  a2risk__prefer_safer_investments: 'agree',
  a2risk__long_time_to_decide: undefined,
  a2risk__risk_equals_opportunity: undefined,
  a2risk__prefer_bank_deposits: undefined,
  a2risk__investment_easy_to_understand: undefined,
  a2risk__willing_to_risk: undefined,
  a2risk__little_investment_experience: undefined,
  a2risk__anxious_about_decisions: undefined,
  a2risk__prefer_high_risk_investments: undefined,
  a2risk__concerned_about_volatility: undefined,
};

test('progressQuestionCounts', t => {
  t.plan(1);
  const target = progressQuestionCounts(testData, 12);
  t.deepEquals(
    target,
    { answers: 3, questions: 12 },
    'should return correct count of answers and questions'
  );
});

test('progressQuestionCounts', t => {
  t.plan(1);
  const target = progressQuestionCounts({}, 12);
  t.deepEquals(
    target,
    { answers: 0, questions: 12 },
    'when no data - should return correct count of answers and questions'
  );
});

test('calculateQuestionProgress', t => {
  t.plan(1);
  const target = calculateQuestionProgress(testData, 12);
  t.deepEquals(target, 25, 'should return correct percentage');
});

test('calculateQuestionProgress', t => {
  t.plan(1);
  const target = calculateQuestionProgress({}, 12);
  t.deepEquals(target, 0, 'when no data - should return correct percentage');
});

test('progressResilienceCounts', t => {
  t.plan(1);
  const target = progressResilienceCounts({});
  t.deepEquals(
    target,
    { answers: 0, questions: 6 },
    'when empty - should return correct count of answers and questions'
  );
});

test('progressResilienceCounts', t => {
  t.plan(1);
  const target = progressResilienceCounts({ b: 1, c: 2, d: 3 });
  t.deepEquals(
    target,
    { answers: 3, questions: 6 },
    'when first section - should return correct count of answers and questions'
  );
});

test('progressResilienceCounts', t => {
  t.plan(1);
  const target = progressResilienceCounts({
    b: 1,
    c: 2,
    d: 3,
    e: 4,
    f: 5,
  });
  t.deepEquals(
    target,
    { answers: 5, questions: 6 },
    'when second section - should return correct count of answers and questions'
  );
});

test('progressResilienceCounts', t => {
  t.plan(1);
  const target = progressResilienceCounts({
    b: 1,
    c: 2,
    d: 3,
    e: 4,
    f: 5,
    investment_resilience__discretionary_income_both_popup: 'foo',
    investment_resilience__discretionary_income_excess_popup: 'foo',
    investment_resilience__low_monthly_discretionary_income_popup: 'foo',
  });
  t.deepEquals(target, { answers: 5, questions: 6 }, 'excludes popup keys');
});

test('progressResilienceCounts', t => {
  t.plan(1);
  const target = progressResilienceCounts({
    a: undefined,
    b: undefined,
    c: undefined,
  });
  t.deepEquals(
    target,
    { answers: 0, questions: 6 },
    'undefined sections - should return correct count of answers and questions'
  );
});

test('progressAboutCounts', t => {
  t.plan(1);
  const target = progressAboutCounts({}, {});
  t.deepEquals(
    target,
    { answers: 0, questions: 7 },
    'when empty - should return correct count of answers and questions'
  );
});

test('progressAboutCounts', t => {
  t.plan(1);
  const aboutData = {
    niNumber: '',
  };
  const initialAbout = {
    niNumber: '',
  };
  const target = progressAboutCounts(aboutData, initialAbout);
  t.deepEquals(
    target,
    { answers: 0, questions: 7 },
    'when partial filled - should return correct count of answers and questions'
  );
});

test('progressAboutCounts', t => {
  t.plan(1);
  const target = progressAboutCounts(
    { dob: 1, contactNumber: 2, niNumber: 3, email: null },
    {}
  );
  t.deepEquals(
    target,
    { answers: 3, questions: 7 },
    'should return correct count of answers and questions'
  );
});

test('progressISACounts', t => {
  t.plan(1);
  const target = progressISACounts({
    isas: [{ in_use: 'no' }, { in_use: 'no' }],
  });
  t.deepEquals(
    target,
    { answers: 0, questions: 1 },
    'when not set - should return correct count of answers and questions'
  );
});

test('progressFinalChecksCounts', t => {
  t.plan(1);
  const target = progressFinalChecksCounts({});
  t.deepEquals(
    target,
    { answers: 0, questions: 7 },
    'when empty - should return correct count of answers and questions'
  );
});

test('progressFinalChecksCounts', t => {
  t.plan(1);
  const target = progressFinalChecksCounts(
    {
      investment__money_to_invest_readily_available: 'yes',
      investment__min_amount_in_savings_available: 'yes',
      investment__maxing_your_employer_pension_scheme: 'yes',
      investment__completed_at: '2018-08-29T08:43:56+00:00',
      investment__hid_borrowing_above_30: 'no',
      investment__hid_borrowing_below_30: 'no',
      investment__hid_mortgage: 'no',
    },
    {
      amount: 100,
      monthly: 20,
    }
  );
  t.deepEquals(
    target,
    { answers: 7, questions: 7 },
    'when complete not above 30 - should return correct count of answers and questions'
  );
});

test('progressFinalChecksCounts', t => {
  t.plan(1);
  const target = progressFinalChecksCounts(
    {
      investment__money_to_invest_readily_available: 'yes',
      investment__min_amount_in_savings_available: 'yes',
      investment__maxing_your_employer_pension_scheme: 'yes',
      investment__completed_at: '2018-08-29T08:43:56+00:00',
      investment__hid_borrowing_above_30: 'yes',
      investment__hid_borrowing_below_30: 'no',
      investment__hid_mortgage: 'no',
      investment__hid_borrowing_above_30_short_term: 'yes',
      investment__hid_borrowing_above_30_taken_advice: 'no',
      investment__hid_borrowing_above_30_getting_worse: 'no',
      investment__hid_borrowing_above_30_struggling: 'no',
    },
    {
      amount: 100,
      monthly: 20,
    }
  );
  t.deepEquals(
    target,
    { answers: 11, questions: 11 },
    'when complete above 30 - should return correct count of answers and questions'
  );
});

test('progressFinalChecksCounts', t => {
  t.plan(1);
  const target = progressFinalChecksCounts(
    {
      investment__money_to_invest_readily_available: null,
      investment__min_amount_in_savings_available: 'yes',
      investment__maxing_your_employer_pension_scheme: 'yes',
      investment__completed_at: '2018-08-29T08:43:56+00:00',
      investment__hid_borrowing_above_30: 'no',
      investment__hid_borrowing_below_30: 'no',
      investment__hid_mortgage: 'no',
    },
    {
      amount: 0,
      monthly: 20,
    }
  );
  t.deepEquals(
    target,
    { answers: 6, questions: 6 },
    'when monthly investment only, reduce questions by one.'
  );
});

test('progressISACounts', t => {
  t.plan(1);
  const target = progressISACounts({
    isas: [{ in_use: 'no' }, { in_use: 'no' }],
    contributedToIsas: 'no',
  });
  t.deepEquals(
    target,
    { answers: 1, questions: 1 },
    'when inactive - should return correct count of answers and questions'
  );
});

test('progressISACounts', t => {
  t.plan(1);
  const target = progressISACounts({
    isas: [
      { in_use: 'no' },
      { in_use: 'yes', amount: 1 },
      { in_use: 'yes', amount: 0 },
    ],
    contributedToIsas: 'yes',
  });
  t.deepEquals(
    target,
    { answers: 1, questions: 2 },
    'when active - should return correct count of answers and questions'
  );
});
