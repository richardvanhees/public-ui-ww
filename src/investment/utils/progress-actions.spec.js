import test from 'tape';
import { generateRiskAction, generateAboutAction } from './progress-actions';

const complete = 100;

const partial = 50;

const incomplete = 0;

test('generateRiskAction', t => {
  t.plan(1);

  const target = generateRiskAction([
    incomplete,
    incomplete,
    incomplete,
    incomplete,
  ]);
  t.deepEquals(
    target,
    { label: 'Start', path: 'atr/start' },
    'should start at atr'
  );
});

test('generateRiskAction', t => {
  t.plan(1);

  const target = generateRiskAction([
    partial,
    incomplete,
    incomplete,
    incomplete,
  ]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'atr/start' },
    'should continue at atr'
  );
});

test('generateRiskAction', t => {
  t.plan(1);

  const target = generateRiskAction([
    complete,
    incomplete,
    incomplete,
    incomplete,
  ]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'experience/start' },
    'should start at experience'
  );
});

test('generateRiskAction', t => {
  t.plan(1);

  const target = generateRiskAction([complete, complete, incomplete]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'resilience/start' },
    'should start at resilience'
  );
});

test('generateRiskAction', t => {
  t.plan(1);

  const target = generateRiskAction([complete, complete, complete]);
  t.deepEquals(
    target,
    { label: 'Review', path: 'atr/start' },
    'should review at atr'
  );
});

test('generateAboutAction', t => {
  t.plan(1);

  const target = generateAboutAction([incomplete, incomplete, incomplete]);
  t.deepEquals(
    target,
    { label: 'Start', path: 'about' },
    'should start at about'
  );
});

test('generateAboutAction', t => {
  t.plan(1);

  const target = generateAboutAction([partial, incomplete, incomplete]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'about' },
    'should continue at about'
  );
});

test('generateAboutAction', t => {
  t.plan(1);

  const target = generateAboutAction([complete, incomplete, incomplete]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'isa/capture' },
    'should start at isa'
  );
});

test('generateAboutAction', t => {
  t.plan(1);

  const target = generateAboutAction([complete, complete, incomplete]);
  t.deepEquals(
    target,
    { label: 'Continue', path: 'final-checks/1' },
    'should start at final-checks'
  );
});

test('generateAboutAction', t => {
  t.plan(1);

  const target = generateAboutAction([complete, complete, complete]);
  t.deepEquals(
    target,
    { label: 'Review', path: 'about' },
    'should review at about'
  );
});
