export default riskLevel => ({
  3: 0.45,
  4: 0.47,
  5: 0.48,
  6: 0.48,
  7: 0.41,
}[riskLevel]);
