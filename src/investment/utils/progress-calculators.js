import R from 'ramda';
import { onlyMonthlyInvestment } from '../modules/investment-goal/selector';

const percentageCalculation = counts => {
  if (counts.answers === 0) return 0;
  if (counts.answers > counts.questions) return 100;
  return Math.round((counts.answers / counts.questions) * 100);
};

const objectAnswerCount = (data, questions) => {
  const answers = R.compose(
    R.reject(R.isNil),
    R.values
  )(data).length;
  return { answers, questions };
};

const compareInitialState = (data, initialState, questions, trackedKeys) => {
  const trackedPairs = R.compose(
    R.fromPairs,
    R.reject(pair => R.propEq(pair[0], pair[1])(initialState)),
    R.toPairs,
    R.pick(trackedKeys)
  )(data);
  return objectAnswerCount(trackedPairs, questions);
};

export const progressQuestionCounts = (data, questions) => {
  let answers = 0;

  if (!R.isEmpty(data)) {
    answers = R.compose(R.reject(R.isNil))(R.values(data)).length;
  }

  return { answers, questions };
};

export const progressResilienceCounts = data => {
  let answers = 0;

  const excludedKeys = [
    'investment_resilience__discretionary_income_both_popup',
    'investment_resilience__discretionary_income_excess_popup',
    'investment_resilience__low_monthly_discretionary_income_popup',
  ];

  Object.keys(data).forEach(key => {
    if (!R.isNil(data[key]) && excludedKeys.indexOf(key) === -1) {
      answers++;
    }
  });

  return { answers, questions: 6 };
};

export const progressISACounts = data => {
  let answers = 0;
  let questions = 1;

  if (data.contributedToIsas) {
    answers = 1;
    if (data.isas && data.contributedToIsas === 'yes') {
      answers = R.compose(
        R.filter(a => a > 0),
        R.pluck('amount'),
        R.filter(R.propEq('in_use', 'yes'))
      )(data.isas).length;
      questions = R.filter(R.propEq('in_use', 'yes'), data.isas).length;
    }
  }

  return { answers, questions };
};

export const progressAboutCounts = (data, initial) => {
  const trackedKeys = [
    'dob',
    'niNumber',
    'contactNumber',
    'addressLine1',
    'addressLine2',
    'addressPostcode',
    'threeYearsOfAddressesProvided',
  ];
  return compareInitialState(data, initial, trackedKeys.length, trackedKeys);
};

export const progressFinalChecksCounts = (finalChecks, investmentGoal) => {
  let questions =
    finalChecks.investment__hid_borrowing_above_30 === 'yes' ? 11 : 7;
  if (onlyMonthlyInvestment({ investmentGoal })) questions -= 1;

  return objectAnswerCount(finalChecks, questions);
};

export const calculateFinalChecksCounts = data =>
  percentageCalculation(progressFinalChecksCounts(data));
export const calculateISAprogress = data =>
  percentageCalculation(progressISACounts(data));
export const calculateFinalChecksProgress = (finalChecks, investmentGoal) =>
  percentageCalculation(progressFinalChecksCounts(finalChecks, investmentGoal));
export const calculateAboutProgress = (data, initial) =>
  percentageCalculation(progressAboutCounts(data, initial));
export const calculateQuestionProgress = (data, questions) =>
  percentageCalculation(progressQuestionCounts(data, questions));
export const calculateResilienceProgress = data =>
  percentageCalculation(progressResilienceCounts(data));
