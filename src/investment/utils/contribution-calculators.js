import R from 'ramda';
import moment from 'moment';

export const contributionValueForTheTaxYr = goalMonthly => {
  const monthsUntilEndOfTaxYear = moment(`${moment().utc().year() + 1}-05-01`, 'YYYY-MM-DD').utc();
  return R.multiply(monthsUntilEndOfTaxYear.diff(moment().utc(), 'months'), goalMonthly);
};
