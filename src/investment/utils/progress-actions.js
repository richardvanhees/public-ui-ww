import R from 'ramda';

const filterLogic = progress => progress !== 100;

const selectActiveState = stateArray => {
  let index = R.findIndex(filterLogic)(stateArray);
  if (index === -1) {
    index = 0;
  }
  return {
    index,
    activeState: stateArray[index],
  };
};

const generatePath = (stateArray, sectionsArray) =>
  sectionsArray[selectActiveState(stateArray).index];

const generateLabel = stateArray => {
  const state = selectActiveState(stateArray);
  // any element beyond the first should be continue
  if (state.index > 0) return 'Continue';
  if (state.activeState === 0) return 'Start';
  if (state.activeState < 100) return 'Continue';
  return 'Review';
};

const riskSections = ['atr/start', 'experience/start', 'resilience/start'];

const aboutSections = ['about', 'isa/capture', 'final-checks/1'];

export const generateRiskAction = stateArray => ({
  label: generateLabel(stateArray, riskSections),
  path: generatePath(stateArray, riskSections),
});

export const generateAboutAction = stateArray => ({
  label: generateLabel(stateArray, aboutSections),
  path: generatePath(stateArray, aboutSections),
});
