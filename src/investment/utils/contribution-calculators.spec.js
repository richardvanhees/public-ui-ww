import test from 'tape';
import {
  contributionValueForTheTaxYr
} from './contribution-calculators';

import timekeeper from 'timekeeper';
import moment from 'moment';

test('progressQuestionCounts', assert => {
  assert.plan(8);

  timekeeper.freeze(moment(`${moment().utc().year()}-05-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 3300, 'calculation correct when 11 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-06-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 3000, 'calculation correct when 10 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-07-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 2700, 'calculation correct when 9 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-08-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 2400, 'calculation correct when 8 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-09-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 2100, 'calculation correct when 7 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-10-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 1800, 'calculation correct when 6 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-11-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 1500, 'calculation correct when 5 months difference');
  timekeeper.reset();

  timekeeper.freeze(moment(`${moment().utc().year()}-12-05`, 'YYYY-MM-DD').utc().format());
  assert.equal(contributionValueForTheTaxYr(300), 1200, 'calculation correct when 4 months difference');
  timekeeper.reset();
});
