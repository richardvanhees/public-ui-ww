import { getItem } from '../../../modules/utils/local-storage';
import R from 'ramda';
// import { push } from 'react-router-redux';
// import WealthWizards from '../../../modules/wealthwizards';

/*
THIS APPROACH IS TOO COMPLICATED, SO IT WILL SIMPLIFIED. THE USER WILL ALWAYS RETURN TO THE START ON REFRESH, WHEREVER THE USER IS.
THE RIGHT TIME COMPONENT WILL WORK AS EXPECTED, BUT THE TWO FOLLOWING SECTIONS (INVESTMENT AND REGISTER) WILL NOT USE THIS FLOW.
JUST LEAVING THIS HERE, INSTEAD SOMEONE ELSE WOULD LIKE TO FINISH IT.

UNIT TESTS HAVE NOT BEEN WRITTEN YET.
*/

// const sectionsBeforeRegistration = ['right-time', 'investment', 'registration'];

const checkRightTime = async ({ userInput }, { match, setAllData, content, goToQuestion, backToStart }) => {
  const localStorageInput = getItem('right-time') ? JSON.parse(getItem('right-time')) : { userInput: {}, checkboxInput: {} };
  const hasInputInReduxState = !!Object.values(userInput).length;
  const hasAnswersInLocalStorage = !!Object.values(localStorageInput.userInput).length;

  if (!hasInputInReduxState && !hasAnswersInLocalStorage) backToStart();
  if (!hasInputInReduxState && hasAnswersInLocalStorage) {
    await setAllData(localStorageInput);

    if (Object.values(localStorageInput.userInput).length !== content.rightTime.questions.length) {
      // The first question, with index 0, is on page 2 of the pager. We want to show the question
      // the user is at, or the next one after the last answered: +3.
      const nextQuestionToAnswer = parseInt(R.last(Object.keys(localStorageInput.userInput)), 10) + 3;
      const questionToShow = match.params.activePage <= nextQuestionToAnswer ? match.params.activePage : nextQuestionToAnswer;
      goToQuestion(questionToShow);
    }
  }
  // If redux state does have input, data will be processed as before. No need to specify.
};

export const checkWhereToGoOnRefresh = async ({ rightTime }, props) => {
  checkRightTime(rightTime, props);
  // checkInvestment(investment, props);
  // checkRegistration(about, props);
};

