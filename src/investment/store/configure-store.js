import { call } from 'redux-saga/effects';
import loginSaga from '../modules/login/login-saga';
import loginReducer from '../modules/login/login-reducer';
import logoutSaga from '../modules/login/logout-saga';
import resendActivationEmailSaga from '../modules/login/resend-activation-email';
import forgottenPasswordSaga from '../../../modules/password/forgotten-password-saga';
import resetPasswordSaga from '../../../modules/password/reset-password-saga';
import configureStoreBoilerplate from '../../../modules/boilerplate/configure-store';
import browserReducer from '../../../modules/browser';
import contentReducer, {
  getContentRequestSaga,
} from '../../../modules/content';
import factFindReducer, {
  getFactFindSaga,
  saveFactFindSaga,
  resetRiskSaga,
  RESET_STATE,
} from '../modules/fact-find';
import solutionReducer, {
  generateSolutionSaga,
  downloadReportSaga,
  requestPaymentUidSaga,
  setSolutionPaidSaga,
  publishToCrmSaga,
} from '../modules/solution';
import atrReducer, { generateATRProfileSaga } from '../modules/atr';
import experienceReducer, {
  generateExperienceProfileSaga,
} from '../modules/experience';
import flexibilityReducer, {
  generateFlexibilityProfileSaga,
} from '../modules/flexibility';
import resilienceReducer, {
  generateResilienceProfileSaga,
} from '../modules/resilience';
import finalChecksReducer from '../modules/final-checks';
import investmentGoalReducer from '../modules/investment-goal';
import aboutYouReducer, { createUserSaga } from '../modules/about-you';
import postcodeAnywhere, {
  sendPostcodeSearchRequestSaga,
  sendPostcodeSearchSelectSaga,
} from '../../../modules/postcode-anywhere';
import isaReducer from '../modules/isa';
import rightTimeReducer from '../modules/right-time';
import solutionMetadataReducer from '../modules/solution-metadata';

export const configureStore = (history, initialState = {}) =>
  configureStoreBoilerplate(
    {
      aboutYou: aboutYouReducer,
      atr: atrReducer,
      login: loginReducer,
      browser: browserReducer,
      content: contentReducer,
      experience: experienceReducer,
      factFind: factFindReducer,
      flexibility: flexibilityReducer,
      investmentGoal: investmentGoalReducer,
      isa: isaReducer,
      postcodeAnywhere,
      resilience: resilienceReducer,
      solution: solutionReducer,
      finalChecks: finalChecksReducer,
      rightTime: rightTimeReducer,
      solutionMetadata: solutionMetadataReducer,
    },
    [
      call(loginSaga),
      call(logoutSaga),
      call(resendActivationEmailSaga),
      call(forgottenPasswordSaga),
      call(resetPasswordSaga),
      call(getContentRequestSaga),
      call(getFactFindSaga),
      call(saveFactFindSaga),
      call(resetRiskSaga),
      call(generateATRProfileSaga),
      call(generateExperienceProfileSaga),
      call(generateFlexibilityProfileSaga),
      call(sendPostcodeSearchRequestSaga),
      call(sendPostcodeSearchSelectSaga),
      call(generateResilienceProfileSaga),
      call(createUserSaga),
      call(generateSolutionSaga),
      call(setSolutionPaidSaga),
      call(publishToCrmSaga),
      call(downloadReportSaga),
      call(requestPaymentUidSaga),
    ],
    history,
    initialState,
    [],
    RESET_STATE
  );
