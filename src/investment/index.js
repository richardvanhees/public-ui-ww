import '../../assets/styles/common/index.scss';
import '../../assets/styles/investment-wizard/index.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import ReactGA from 'react-ga';
import WealthWizards from '../../modules/wealthwizards';
import App from './app';
import factFindTypes from './modules/fact-find/types';

const history = createHistory();

ReactGA.initialize(WealthWizards.GA_TRACKING_ID);
ReactGA.pageview(window.location.pathname + window.location.search);

import { configureStore } from './store/configure-store';

const store = configureStore(history);

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/investment/service-worker.js', { scope: '/' })
    .then(registration => {
      console.log('Registration successful, scope is:', registration.scope); // eslint-disable-line no-console
    })
    .catch(error => {
      console.log('Service worker registration failed, error:', error); // eslint-disable-line no-console
    });
}

history.listen(location => {
  ReactGA.pageview(location.pathname + location.search);
  window.scrollTo(0, 0);
});

const rootEl = document.getElementById('app');

store.dispatch({ type: factFindTypes.GET_FACT_FIND_REQUEST });

ReactDOM.render(<App store={store} history={history} />, rootEl);
