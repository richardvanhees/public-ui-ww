import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Provider, connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { ConnectedRouter, push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { changeBreakpoint, breakpoint } from './../../modules/browser';
import WealthWizards from '../../modules/wealthwizards';
import Loadable from 'react-loadable';

import RegisterContainer from './components/pages/register/RegisterContainer';
import StartContainer from './components/pages/StartContainer';
import AppWrapperContainer from './components/AppWrapperContainer';
import InvestmentGoalContainer from './components/pages/investment/InvestmentGoalContainer';
import InvestmentAmountContainer from './components/pages/investment/InvestmentAmountContainer';
import AboutContainer from './components/pages/about/AboutContainer';
import AtrStartContainer from './components/pages/atr/AtrStartContainer';
import AtrQuestionContainer from './components/pages/atr/AtrQuestionContainer';
import ExperienceStartContainer from './components/pages/experience/ExperienceStartContainer';
import ExperienceQuestionContainer from './components/pages/experience/ExperienceQuestionContainer';
import FlexibilityStartContainer from './components/pages/flexibility/FlexibilityStartContainer';
import FlexibilityQuestionContainer from './components/pages/flexibility/FlexibilityQuestionContainer';
import ResilienceStartContainer from './components/pages/resilience/ResilienceStartContainer';
import ResilienceQuestionContainer from './components/pages/resilience/ResilienceQuestionContainer';
import Playback from './components/pages/Playback';
import ConfirmObjectiveContainer from './components/pages/confirm-objective/ConfirmObjectiveContainer';
import AdviceAcceptedContainer from './components/pages/advice/AdviceAcceptedContainer';
import ReferralContainer from './components/pages/referral/ReferralContainer';
import AdviceSummaryContainer from './components/pages/advice-summary/AdviceSummaryContainer';
import FinalChecksContainer from './components/pages/final-checks/FinalChecksContainer';
import HIDContainer from './components/pages/high-interest-debt/HIDContainer';
import HIDBorrowingContainer from './components/pages/high-interest-debt/HIDBorrowingAboveContainer';
import IsaCaptureContainer from './components/pages/isa/IsaCaptureContainer';
import IsaAmountContainer from './components/pages/isa/IsaAmountContainer';
import IsaAmendInvestmentContainer from './components/pages/isa/IsaAmendInvestmentContainer';
import LoginContainer from './components/pages/login/LoginContainer';
import checkUserLoggedin from './modules/check-user-logged-in';
import ForgottenPasswordContainer from './components/pages/forgotten-password/ForgottenPasswordContainer';
import ResetPasswordContainer from './components/pages/reset-password/ResetPasswordContainer';
import LandingPageContainer from './components/pages/landing/LandingPageContainer';
import DashboardContainer from './components/pages/dashboard/DashboardContainer';
import ActivateContainer from './components/pages/activate/ActivateContainer';
import RightTimeContainer from './components/pages/right-time/RightTimeContainer';
import NeedMoreTimeContainer from './components/pages/need-more-time/NeedMoreTimeContainer';
import Spinner from '../../modules/elements/Spinner';
import PaymentSuccessContainer from './components/pages/payment/PaymentSuccessContainer';
import ServicesDownContainer from './components/pages/services-down/ServicesDownContainer';
import PieChart from '../../modules/elements/PieChart';

// exporting functions from containers makes it easier to unit test
const FinalChecksContainerInstance = FinalChecksContainer();
const HIDContainerInstance = HIDContainer();
const HIDBorrowingContainerInstance = HIDBorrowingContainer();
const ConfirmObjectiveContainerInstance = ConfirmObjectiveContainer();
const AboutContainerInstance = AboutContainer();
const ExperienceQuestionContainerInstance = ExperienceQuestionContainer();
const AtrQuestionContainerInstance = AtrQuestionContainer();
const ForgottenPasswordContainerInstance = ForgottenPasswordContainer();
const ResetPasswordContainerInstance = ResetPasswordContainer();
const DashboardContainerInstance = DashboardContainer();
const AdviceSummaryContainerInstance = AdviceSummaryContainer();
const RightTimeContainerInstance = RightTimeContainer();
const NeedMoreTimeContainerInstance = NeedMoreTimeContainer();

const Loading = () => <Spinner visible />;

const PaymentContainerInstance = Loadable({
  loader: () => import('./components/pages/payment/PaymentContainer'),
  loading: Loading,
  delay: 2000,
});

const AllowAccessToDashboard = checkUserLoggedin('landing')(
  DashboardContainerInstance
);

const AllowAccessToLogin = checkUserLoggedin('dashboard', true)(LoginContainer);
const AllowAccessToResetPassword = checkUserLoggedin(
  `${WealthWizards.CONTEXT_ROUTE}/dashboard`,
  true
)(ResetPasswordContainerInstance);
const AllowAccessToForgottenPassword = checkUserLoggedin('dashboard', true)(
  ForgottenPasswordContainerInstance
);

class App extends PureComponent {
  static propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.checkBreakpoint = this.checkBreakpoint.bind(this);

    this.state = {
      currentBreakpoint: props.browser.breakpoint,
      offline: props.browser.offline,
    };
  }

  checkBreakpoint = () => {
    const currentBreakpoint = breakpoint();

    if (this.state.currentBreakpoint !== currentBreakpoint) {
      this.setState({ currentBreakpoint });
      this.props.setBreakpoint(currentBreakpoint);
    }
  };

  userIsTabbing = e =>
    e.keyCode === 9 && document.body.classList.add('user-is-tabbing');

  userIsNotTabbing = () => document.body.classList.remove('user-is-tabbing');

  componentDidMount() {
    this.checkBreakpoint();
    window.addEventListener('resize', this.checkBreakpoint);
    window.addEventListener('keydown', this.userIsTabbing);
    window.addEventListener('mousedown', this.userIsNotTabbing);

    // To prevent gray highlighting on tap when using safari on ios
    window.addEventListener('touchstart', () => {
    }, true);
  }

  render() {
    return (
      <Provider store={this.props.store}>
        <ConnectedRouter history={this.props.history}>
          <AppWrapperContainer history={this.props.history}>
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/landing`}
              component={LandingPageContainer}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/`}
              component={LandingPageContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/login`}
              component={AllowAccessToLogin}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/forgotten-password`}
              component={AllowAccessToForgottenPassword}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/need-more-time`}
              component={NeedMoreTimeContainerInstance}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/right-time/:activePage`}
              component={RightTimeContainerInstance}
            />
            <Route
              exact
              path={`${WealthWizards.CONTEXT_ROUTE}/forgotten-password/:token`}
              component={AllowAccessToResetPassword}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/plan`}
              component={StartContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/goal`}
              component={InvestmentGoalContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/amount`}
              component={InvestmentAmountContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/register`}
              component={RegisterContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/dashboard`}
              component={AllowAccessToDashboard}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/isa/capture`}
              component={IsaCaptureContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/isa/amount`}
              component={IsaAmountContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/isa/amend-investment`}
              component={IsaAmendInvestmentContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/atr/start`}
              component={AtrStartContainer}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
                }/atr/question/:activePage`}
              component={AtrQuestionContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/atr/playback`}
              render={routeProps => (
                <Playback {...routeProps} section={'atr'} />
              )}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/experience/start`}
              component={ExperienceStartContainer}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
                }/experience/question/:activePage`}
              component={ExperienceQuestionContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/experience/playback`}
              render={routeProps => (
                <Playback {...routeProps} section={'experience'} />
              )}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/flexibility/start`}
              component={FlexibilityStartContainer}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
                }/flexibility/question/:activeQuestion`}
              component={FlexibilityQuestionContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/flexibility/playback`}
              render={routeProps => (
                <Playback {...routeProps} section={'flexibility'} />
              )}
            />
            <Switch>
              <Route
                exact
                path={`${WealthWizards.CONTEXT_ROUTE}/resilience/start`}
                component={ResilienceStartContainer}
              />
              <Route
                exact
                path={`${WealthWizards.CONTEXT_ROUTE}/resilience/playback`}
                render={routeProps => (
                  <Playback {...routeProps} section={'resilience'} />
                )}
              />
              <Route
                path={`${WealthWizards.CONTEXT_ROUTE}/resilience/:activePage`}
                component={ResilienceQuestionContainer}
              />
            </Switch>
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/about`}
              component={AboutContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/advice-summary/:activePage`}
              component={AdviceSummaryContainerInstance}
            />
            <Route
              path={`${
                WealthWizards.CONTEXT_ROUTE
                }/final-checks/:activeQuestion`}
              component={FinalChecksContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/hid`}
              component={HIDContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/borrowing-above-30`}
              component={HIDBorrowingContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/confirm-objective`}
              component={ConfirmObjectiveContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/advice-accepted`}
              component={AdviceAcceptedContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/referral`}
              component={ReferralContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/alert-referral`}
              component={ReferralContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/activate-account`}
              component={ActivateContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/payment`}
              component={PaymentContainerInstance}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/payment-success`}
              component={PaymentSuccessContainer}
            />
            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/services-down`}
              component={ServicesDownContainer}
            />

            <Route
              path={`${WealthWizards.CONTEXT_ROUTE}/pie-chart`}
              component={PieChart}
            />
          </AppWrapperContainer>
        </ConnectedRouter>
      </Provider>
    );
  }
}

const mapStateToProps = state => ({
  state,
  browser: state.browser,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      push,
      setBreakpoint: changeBreakpoint,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
