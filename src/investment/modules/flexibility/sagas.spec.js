import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generateFlexibilityProfileSaga, {
  getFlexibilityInput,
  callToGenerateFlexibilityProfile,
  generateFlexibilityProfile,
} from './sagas';
import atrTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';


test('generateFlexibilityProfileSaga', t => {
  let target;

  const testData = { 0: { answerKey: 'testKey', answerValue: 'testValue' } };
  const formattedTestData = { testKey: 'testValue' };
  const testResponse = { data: 'atrProfile' };
  target = testSaga(generateFlexibilityProfile)
    .next()
    .put(setIsLoading(true))
    .next()
    .put({ type: atrTypes.GENERATE_FLEXIBILITY_PROFILE_START })
    .next()
    .select(getFlexibilityInput)
    .next(testData)
    .call(callToGenerateFlexibilityProfile, formattedTestData)
    .next(testResponse)
    .put(setIsLoading(false))
    .next()
    .put({
      type: atrTypes.GENERATE_FLEXIBILITY_PROFILE_SUCCESS,
      profile: testResponse.data,
    })
    .next();
  t.equals(!!target.isDone(), true, 'generateFlexibilityProfile success');

  t.end();
});
