import flexibilityTypes from './types';
import {
  MAP_FROM_SERVER,
  RESET_RISK_SECTIONS,
} from '../../modules/fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

const flexibilityFieldUpdated = (state, action) => ({
  ...state,
  data: {
    ...state.data,
    [action.name]: action.value,
  },
});

const resetSection = () => initialState;

const generateFlexibilityProfileStart = state => ({
  ...state,
  isLoading: true,
  override: false,
});

const generateFlexibilityProfileSuccess = (state, action) => ({
  ...state,
  isLoading: false,
  profile: action.profile,
});

const generateFlexibilityProfileFailed = state => ({
  ...state,
  isLoading: false,
});

const setManualOverride = (state, action) => ({
  ...state,
  override: action.override,
});

const mapFromServer = (state, action) => ({
  ...state,
  profile: action.data.flexibility.profile,
  override: action.data.flexibility.override,
  data: {
    ...state.data,
    ...action.data.flexibility.data,
  },
});

const reducer = {
  [flexibilityTypes.FLEXIBILITY_FIELD_UPDATED]: flexibilityFieldUpdated,
  [flexibilityTypes.RESET_FLEXIBILITY]: resetSection,
  [RESET_RISK_SECTIONS]: resetSection,
  [flexibilityTypes.FLEXIBILITY_MANUAL_OVERRIDE]: setManualOverride,
  [flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_START]: generateFlexibilityProfileStart,
  [flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_SUCCESS]: generateFlexibilityProfileSuccess,
  [flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_FAILED]: generateFlexibilityProfileFailed,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
