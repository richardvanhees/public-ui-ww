import test from 'tape';
import reducer from './reducer.js';
import atrTypes from './types';
import { MAP_FROM_SERVER, RESET_RISK_SECTIONS } from '../fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

test('FlexibilityReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: atrTypes.FLEXIBILITY_FIELD_UPDATED,
      name: 'testName',
      value: 'testValue',
    }
  );
  t.deepEqual(
    result,
    {
      data: {
        testName: 'testValue',
      },
    },
    'FLEXIBILITY_FIELD_UPDATED'
  );

  result = reducer(
    {},
    {
      type: atrTypes.RESET_FLEXIBILITY,
    }
  );
  t.deepEqual(result, initialState, 'RESET_FLEXIBILITY');

  result = reducer(
    {},
    {
      type: atrTypes.FLEXIBILITY_MANUAL_OVERRIDE,
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    }
  );
  t.deepEqual(
    result,
    {
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    },
    'FLEXIBILITY_MANUAL_OVERRIDE'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_FLEXIBILITY_PROFILE_START,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: true,
      override: false,
    },
    'GENERATE_FLEXIBILITY_PROFILE_START'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_FLEXIBILITY_PROFILE_SUCCESS,
      profile: 'testProfile',
    }
  );
  t.deepEqual(
    result,
    {
      profile: 'testProfile',
      isLoading: false,
    },
    'GENERATE_FLEXIBILITY_PROFILE_SUCCESS'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_FLEXIBILITY_PROFILE_FAILED,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: false,
    },
    'GENERATE_FLEXIBILITY_PROFILE_FAILED'
  );

  t.end();
});

test('FlexibilityReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: MAP_FROM_SERVER,
      data: {
        flexibility: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(
    res,
    {
      data: {
        foo: 'baa',
      },
      profile: 'fake profile',
      override: 'fake override',
    },
    'maps from server'
  );
});

test('FlexibilityReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: RESET_RISK_SECTIONS,
      data: {
        flexibility: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(res, initialState, 'RESET_RISK_SECTIONS');
});
