import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('FlexibilityActions', t => {
  let result;

  result = actions.generateFlexibilityProfileStart();
  t.deepEqual(
    result,
    { type: types.GENERATE_FLEXIBILITY_PROFILE_START },
    'generateFlexibilityProfileStart'
  );

  result = actions.generateFlexibilityProfile();
  t.deepEqual(
    result,
    { type: types.GENERATE_FLEXIBILITY_PROFILE },
    'generateFlexibilityProfile'
  );

  result = actions.generateFlexibilityProfileSuccess('testData');
  t.deepEqual(
    result,
    { type: types.GENERATE_FLEXIBILITY_PROFILE_SUCCESS, profile: 'testData' },
    'generateFlexibilityProfileSuccess'
  );

  result = actions.generateFlexibilityProfileFailed();
  t.deepEqual(
    result,
    { type: types.GENERATE_FLEXIBILITY_PROFILE_FAILED },
    'generateFlexibilityProfileFailed'
  );

  t.end();
});

test('FlexibilityActions', assert => {
  assert.plan(1);
  const result = actions.updateProgress();
  assert.deepEqual(
    result,
    { type: types.FLEXIBILITY_UPDATE_PROGRESS },
    'updateProgress'
  );
});
