import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const userInput = state => R.path(['flexibility', 'data'], state);
const profile = state => R.path(['flexibility', 'profile'], state);

export const calculateFlexibilityProgress = () => 100;

export default createStructuredSelector({
  userInput,
  profile,
});
