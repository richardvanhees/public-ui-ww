import test from 'tape';
import selector, {
  calculateFlexibilityProgress,
  __RewireAPI__,
} from './selectors.js';

test('FlexibilitySelector', t => {
  const result = selector({
    flexibility: {
      data: 'testUserInput',
      profile: 'testProfile',
    },
  });

  t.deepEqual(result, {
    userInput: 'testUserInput',
    profile: 'testProfile',
  });

  t.end();
});

test('calculateFlexibilityProgress', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => 50);

  const result = calculateFlexibilityProgress(
    {
      length: '10 years',
      target: 0,
    },
    {},
    5
  );

  assert.equal(result, 100, 'defaults to 100 when flexibility is not required');

  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
});
