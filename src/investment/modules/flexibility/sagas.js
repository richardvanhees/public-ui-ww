import { call, put, select, takeLatest } from 'redux-saga/effects';

import { post } from '../../../../modules/utils/AxiosWrapper';
import {
  generateFlexibilityProfileSuccess,
  generateFlexibilityProfileFailed,
  generateFlexibilityProfileStart,
} from './actions';
import flexibilitySelector from './selectors';
import flexibilityTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

export const getFlexibilityInput = state =>
  flexibilitySelector(state).userInput;

export const callToGenerateFlexibilityProfile = data =>
  post({
    route: '/v1/financial-flexibility',
    data,
  });

export function* generateFlexibilityProfile() {
  try {
    yield put(setIsLoading(true));
    yield put(generateFlexibilityProfileStart());
    const userInput = yield select(getFlexibilityInput);

    const formattedInput = {};
    Object.values(userInput).forEach(question => {
      formattedInput[
        question.answerKey.replace(/investment_flexibility__/g, '')
      ] =
        question.answerValue;
    });

    const profile = yield call(
      callToGenerateFlexibilityProfile,
      formattedInput
    );
    yield put(setIsLoading(false));
    yield put(generateFlexibilityProfileSuccess(profile.data));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(generateFlexibilityProfileFailed());
  }
}

export default function* generateFlexibilityProfileSaga() {
  yield takeLatest(
    flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE,
    generateFlexibilityProfile
  );
}
