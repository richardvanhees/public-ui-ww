import flexibilityTypes from './types';

export const generateFlexibilityProfile = () => ({
  type: flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE,
});

export const generateFlexibilityProfileStart = () => ({
  type: flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_START,
});

export const generateFlexibilityProfileSuccess = profile => ({
  type: flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_SUCCESS,
  profile,
});

export const generateFlexibilityProfileFailed = () => ({
  type: flexibilityTypes.GENERATE_FLEXIBILITY_PROFILE_FAILED,
});

export const resetFlexibilitySection = () => ({
  type: flexibilityTypes.RESET_FLEXIBILITY,
});

export const flexibilityManualOverride = override => ({
  type: flexibilityTypes.FLEXIBILITY_MANUAL_OVERRIDE,
  override,
});

export const handleFieldChange = (name, value) => ({
  type: flexibilityTypes.FLEXIBILITY_FIELD_UPDATED,
  name,
  value,
});

export const updateProgress = () => ({
  type: flexibilityTypes.FLEXIBILITY_UPDATE_PROGRESS,
});
