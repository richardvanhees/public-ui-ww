import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const dob = state => R.path(['aboutYou', 'dob'], state);
const niNumber = state => R.path(['aboutYou', 'niNumber'], state);
const contactNumber = state => R.path(['aboutYou', 'contactNumber'], state);
const gender = state => R.path(['aboutYou', 'gender'], state);
const addressLine1 = state => R.path(['aboutYou', 'addressLine1'], state);
const addressLine2 = state => R.path(['aboutYou', 'addressLine2'], state);
const addressLine3 = state => R.path(['aboutYou', 'addressLine3'], state);
const addressPostcode = state => R.path(['aboutYou', 'addressPostcode'], state);

const email = state => R.path(['aboutYou', 'email'], state);
const firstName = state => R.path(['aboutYou', 'firstName'], state);
const lastName = state => R.path(['aboutYou', 'lastName'], state);
const consentedPrivacyPolicy = state =>
  R.path(['aboutYou', 'consentedPrivacyPolicy'], state);

export default createStructuredSelector({
  dob,
  niNumber,
  contactNumber,
  gender,
  addressLine1,
  addressLine2,
  addressLine3,
  addressPostcode,
  email,
  firstName,
  lastName,
  consentedPrivacyPolicy,
});
