export const SET_ABOUT_ADDRESS_LINE_1 = 'about-you/SET_ABOUT_ADDRESS_LINE_1';
export const SET_ABOUT_ADDRESS_LINE_2 = 'about-you/SET_ABOUT_ADDRESS_LINE_2';
export const SET_ABOUT_ADDRESS_LINE_3 = 'about-you/SET_ABOUT_ADDRESS_LINE_3';
export const SET_ABOUT_POST_CODE = 'about-you/SET_ABOUT_POST_CODE';
export const SET_TIME_LIVED_AT_CURRENT_ADDRESS =
  'about-you/SET_TIME_LIVED_AT_CURRENT_ADDRESS';
export const UPDATE_PREVIOUS_ADDRESS_ARRAY =
  'about-you/UPDATE_PREVIOUS_ADDRESS_ARRAY';
export const SET_THREE_YEARS_OF_ADDRESSES_PROVIDED =
  'about-you/SET_THREE_YEARS_OF_ADDRESSES_PROVIDED';
export const TOGGLE_MANUAL_INPUTS = 'about-you/TOGGLE_MANUAL_INPUTS';
export const HANDLE_USER_INPUT = 'about-you/HANDLE_USER_INPUT';
export const CREATE_USER_START = 'about-you/CREATE_USER_START';
export const CREATE_USER = 'about-you/CREATE_USER';
export const CREATE_USER_SUCCESS = 'about-you/CREATE_USER_SUCCESS';
export const CREATE_USER_FAILED = 'about-you/CREATE_USER_FAILED';
export const RESET_ERROR = 'about/you/RESET_ERROR';
export const UPDATE_PROGRESS = 'about-you/UPDATE_PROGRESS';
export const CLEAR_PENDING_RECORDS = 'about-you/CLEAR_PENDING_RECORDS';
export const SET_POSTCODE_TO_SEARCH_FOR =
  'about-you/SET_POSTCODE_TO_SEARCH_FOR';

export const toggleManualInputs = () => ({
  type: TOGGLE_MANUAL_INPUTS,
});

export const setAboutAdressLine1 = data => ({
  type: SET_ABOUT_ADDRESS_LINE_1,
  addressLine1: data.addressLine1,
});

export const setAboutAdressLine2 = data => ({
  type: SET_ABOUT_ADDRESS_LINE_2,
  addressLine2: data.addressLine2,
});

export const setAboutAdressLine3 = data => ({
  type: SET_ABOUT_ADDRESS_LINE_3,
  addressLine3: data.addressLine3,
});

export const setAboutPostCode = data => ({
  type: SET_ABOUT_POST_CODE,
  postCode: data.postCode,
});

export const setPostcodeToSearchFor = data => ({
  type: SET_POSTCODE_TO_SEARCH_FOR,
  postCode: data.postCode,
});

export const setTimeLivedAtCurrentAddress = data => ({
  type: SET_TIME_LIVED_AT_CURRENT_ADDRESS,
  timeLivedAtCurrentAddress: data,
});

export const setThreeYearsOfAddressesProvided = data => ({
  type: SET_THREE_YEARS_OF_ADDRESSES_PROVIDED,
  threeYearsOfAddressesProvided: data,
});

export const updatePreviousAddressArray = data => ({
  type: UPDATE_PREVIOUS_ADDRESS_ARRAY,
  addressPreviousArray: data,
});

export const handleUserInput = data => ({
  type: HANDLE_USER_INPUT,
  data,
});

export const createUserStart = () => ({
  type: CREATE_USER_START,
});

export const createUser = (password, recaptcha) => ({
  type: CREATE_USER,
  password,
  recaptcha,
});

export const createUserSuccess = data => ({
  type: CREATE_USER_SUCCESS,
  data,
});

export const createUserFailed = error => ({
  type: CREATE_USER_FAILED,
  error,
});

export const clearPendingRecords = () => ({
  type: CLEAR_PENDING_RECORDS,
});

export const resetError = () => ({
  type: RESET_ERROR,
});

export const updateProgress = () => ({
  type: UPDATE_PROGRESS,
});
