import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import {
  callToCreateUser,
  getUserInput,
  createUser,
  getState
} from './create-user-saga';
import {
  createUserFailed,
} from '../actions';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { getFactFind } from '../../fact-find/sagas/save-fact-find'

const fakeState = {};

test('createUserSaga SUCCESS', t => {
  let target;
  const testActionPayload = { password: 'password', recaptcha: 'fakeRecaptcha' };

  const testSelector = { email: 'test@test.com' };
  const testData = { username: 'test@test.com', password: 'password', fact_find: {} };

  target = testSaga(createUser, testActionPayload)
    .next()
    .select(getState)
    .next({})
    .select(getFactFind, fakeState)
    .next({})
    .put(setIsLoading(true))
    .next({})
    .select(getUserInput)
    .next(testSelector)
    .call(callToCreateUser, testData, 'fakeRecaptcha')
    .next()
    .put(push(`${WealthWizards.CONTEXT_ROUTE}/activate-account`))
    .next()
    .put(setIsLoading(false))
    .next();
  t.equals(!!target.isDone(), true, 'createUser success');

  t.end();
});

test('createUserSaga ERROR', t => {
  let target;
  const testActionPayload = { password: 'password', recaptcha: 'fakeRecaptcha' };

  const testSelector = { email: 'test@test.com' };

  const testData = { username: 'test@test.com', password: 'password', fact_find: {} };

  const fakeError = { response: { status: 500 } };

  target = testSaga(createUser, testActionPayload)
    .next()
    .select(getState)
    .next({})
    .select(getFactFind, fakeState)
    .next({})
    .put(setIsLoading(true))
    .next()
    .select(getUserInput)
    .next(testSelector)
    .call(callToCreateUser, testData, 'fakeRecaptcha')
    .throw(fakeError)
    .put(setIsLoading(false))
    .next()
    .put(createUserFailed(fakeError))
    .next()
    .put(
      setError(
        'We are currently unable to create your account, please try again later'
      )
    )
    .next();

  t.true(target.isDone(), 'createUser error handled');

  t.end();
});
