import { call, put, select, takeLatest } from 'redux-saga/effects';
import R from 'ramda';
import WealthWizards from '../../../../../modules/wealthwizards';
import { post } from '../../../../../modules/utils/AxiosWrapper';
import aboutYouSelector from '../selector';
import { CREATE_USER, createUserFailed } from '../actions';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import { push } from 'react-router-redux';
import { getFactFind } from '../../fact-find/sagas/save-fact-find';

export const getState = state => state;

export const getUserInput = state => ({
  email: aboutYouSelector(state).email,
});

export const callToCreateUser = (data, recaptcha) =>
  post({
    route: '/v1/user',
    data,
    headers: { recaptcha },
  });

export function* createUser({ password, recaptcha }) {
  try {
    const state = yield select(getState);
    const factFind = yield select(getFactFind, state);
    yield put(setIsLoading(true));
    const userInput = yield select(getUserInput);
    yield call(callToCreateUser, { username: userInput.email, password, fact_find: factFind }, recaptcha);
    yield put(push(`${WealthWizards.CONTEXT_ROUTE}/activate-account`));
    yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(createUserFailed(error));
    if (R.path(['response', 'status'])(error) >= 500) {
      yield put(
        setError(
          'We are currently unable to create your account, please try again later'
        )
      );
    }
  }
}

export default function* createUserSaga() {
  yield takeLatest(CREATE_USER, createUser);
}
