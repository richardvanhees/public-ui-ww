import test from 'tape';
import {
  SET_ABOUT_ADDRESS_LINE_1,
  SET_ABOUT_ADDRESS_LINE_2,
  SET_ABOUT_ADDRESS_LINE_3,
  SET_ABOUT_POST_CODE,
  setAboutAdressLine1,
  setAboutAdressLine2,
  setAboutAdressLine3,
  setAboutPostCode,
  updateProgress,
  createUser,
} from './actions';

test('AboutUsActions', t => {
  let result;

  result = setAboutAdressLine1({ addressLine1: '123 fake street' });
  t.deepEqual(
    result,
    {
      addressLine1: '123 fake street',
      type: 'about-you/SET_ABOUT_ADDRESS_LINE_1',
    },
    'SET_ABOUT_ADDRESS_LINE_1'
  );

  result = setAboutAdressLine2({ addressLine2: '456 fake street' });
  t.deepEqual(
    result,
    {
      addressLine2: '456 fake street',
      type: 'about-you/SET_ABOUT_ADDRESS_LINE_2',
    },
    'SET_ABOUT_ADDRESS_LINE_2'
  );

  result = setAboutAdressLine3({ addressLine3: '789 fake street' });
  t.deepEqual(
    result,
    {
      addressLine3: '789 fake street',
      type: 'about-you/SET_ABOUT_ADDRESS_LINE_3',
    },
    'SET_ABOUT_ADDRESS_LINE_3'
  );

  result = setAboutPostCode({ postCode: 'cv34 6rt' });
  t.deepEqual(
    result,
    { postCode: 'cv34 6rt', type: 'about-you/SET_ABOUT_POST_CODE' },
    'SET_ABOUT_POST_CODE'
  );

  t.end();
});

test('AboutUsActions', t => {
  let result;

  result = updateProgress();
  t.deepEqual(
    result,
    {
      type: 'about-you/UPDATE_PROGRESS',
    },
    'UPDATE_PROGRESS'
  );
  t.end();
});

test('AboutUsActions', t => {
  let result;

  result = createUser('password', 'recaptcha');
  t.deepEqual(
    result,
    {
      type: 'about-you/CREATE_USER',
      password: 'password',
      recaptcha: 'recaptcha'
    },
    'CREATE_USER'
  );
  t.end();
});
