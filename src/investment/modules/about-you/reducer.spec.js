import test from 'tape';
import reducer from './reducer.js';
import {
  SET_ABOUT_DOB,
  SET_ABOUT_NI_NUMBER,
  SET_ABOUT_CONTACT_NUMBER,
  SET_ABOUT_GENDER,
  SET_ABOUT_POST_CODE,
  CLEAR_PENDING_RECORDS,
  SET_POSTCODE_TO_SEARCH_FOR,
} from './actions';
import { MAP_FROM_SERVER } from '../fact-find/actions';
import {
  SEND_POSTCODE_SEARCH_FAILURE,
  SEND_POSTCODE_SEARCH_MAP_FIELDS,
} from '../../../../modules/postcode-anywhere';
import tk from 'timekeeper';
import moment from 'moment';

test('AboutYouReducer', assert => {
  assert.plan(1);

  const result = reducer(
    {},
    {
      type: SEND_POSTCODE_SEARCH_MAP_FIELDS,
      fieldMappings: { addressLine1: 'addressLine1' },
    }
  );
  assert.deepEqual(
    result,
    { addressLine1: 'addressLine1' },
    SEND_POSTCODE_SEARCH_MAP_FIELDS
  );
});

test('AboutYouReducer', t => {
  let result;

  result = reducer({}, { type: SET_ABOUT_POST_CODE, postCode: 'cv34 6rt' });
  t.deepEqual(result, { addressPostcode: 'cv34 6rt' }, SET_ABOUT_POST_CODE);

  t.end();
});

test('AboutYouReducer', assert => {
  assert.plan(1);

  const result = reducer(
    {},
    { type: MAP_FROM_SERVER, data: { personal: { contactNumber: 123451 } } }
  );
  assert.deepEqual(result, { contactNumber: 123451 }, 'maps from server');
});

test('AboutYouReducer', assert => {
  assert.plan(1);

  tk.freeze(moment.utc('2012-01-01').toDate());

  const result = reducer(
    {},
    {
      type: SEND_POSTCODE_SEARCH_MAP_FIELDS,
      fieldMappings: { addressLine1: 'addressLine1' },
      subStateKey: 'addressPreviousArray',
      addressIndex: 0,
    }
  );
  assert.deepEqual(
    result,
    {
      addressPreviousArray: [{ addressLine1: 'addressLine1' }],
      lastPostcodeMappingTimestamp: 1325376000000,
    },
    SEND_POSTCODE_SEARCH_MAP_FIELDS
  );

  tk.reset();
});

test('AboutYouReducer', assert => {
  assert.plan(1);

  tk.freeze(moment.utc('2012-01-01').toDate());

  const result = reducer(
    { addressPreviousArray: [{ foo: 'ba' }] },
    {
      type: SEND_POSTCODE_SEARCH_MAP_FIELDS,
      fieldMappings: { addressLine1: 'addressLine1' },
      subStateKey: 'addressPreviousArray',
      addressIndex: 0,
    }
  );
  assert.deepEqual(
    result,
    {
      addressPreviousArray: [{ foo: 'ba', addressLine1: 'addressLine1' }],
      lastPostcodeMappingTimestamp: 1325376000000,
    },
    SEND_POSTCODE_SEARCH_MAP_FIELDS
  );

  tk.reset();
});

test('AboutYouReducer', assert => {
  assert.plan(1);

  const result = reducer(
    { addressPreviousArray: [{ foo: 'ba', pending: true }] },
    {
      type: CLEAR_PENDING_RECORDS,
    }
  );
  assert.deepEqual(
    result,
    {
      addressPreviousArray: [],
      postcodeToSearchFor: null,
    },
    CLEAR_PENDING_RECORDS
  );
});

test('AboutYouReducer', assert => {
  assert.plan(1);

  const result = reducer(
    {},
    {
      type: SET_POSTCODE_TO_SEARCH_FOR,
      postCode: 'something fishy',
    }
  );
  assert.deepEqual(
    result,
    {
      postcodeToSearchFor: 'something fishy',
    },
    SET_POSTCODE_TO_SEARCH_FOR
  );
});
