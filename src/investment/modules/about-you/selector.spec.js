import test from 'tape';
import selector from './selector.js';

test('AboutYouSelector', t => {
  const result = selector({
    aboutYou: {
      dob: '',
      niNumber: '',
      contactNumber: '',
      gender: '',
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      addressPostcode: '',
      email: '',
      firstName: '',
      lastName: '',
      consentedPrivacyPolicy: false,
    },
  });

  t.deepEqual(result, {
    dob: '',
    niNumber: '',
    contactNumber: '',
    gender: '',
    addressLine1: '',
    addressLine2: '',
    addressLine3: '',
    addressPostcode: '',
    email: '',
    firstName: '',
    lastName: '',
    consentedPrivacyPolicy: false,
  });

  t.end();
});
