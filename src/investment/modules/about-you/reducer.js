import {
  SET_ABOUT_ADDRESS_LINE_1,
  SET_ABOUT_ADDRESS_LINE_2,
  SET_ABOUT_ADDRESS_LINE_3,
  SET_ABOUT_POST_CODE,
  SET_TIME_LIVED_AT_CURRENT_ADDRESS,
  SET_THREE_YEARS_OF_ADDRESSES_PROVIDED,
  UPDATE_PREVIOUS_ADDRESS_ARRAY,
  TOGGLE_MANUAL_INPUTS,
  HANDLE_USER_INPUT,
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_FAILED,
  RESET_ERROR,
  CLEAR_PENDING_RECORDS,
  SET_POSTCODE_TO_SEARCH_FOR,
} from './actions';
import R from 'ramda';
import {
  SEND_POSTCODE_SEARCH_FAILURE,
  SEND_POSTCODE_SEARCH_MAP_FIELDS,
  SEND_POSTCODE_SEARCH_RESET,
} from '../../../../modules/postcode-anywhere';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

export const initialState = {
  dob: '',
  niNumber: '',
  contactNumber: '',
  gender: 'u',
  addressLine1: '',
  addressLine2: '',
  addressLine3: '',
  addressPostcode: '',
  addressTimeLivedAt: null,
  addressPreviousArray: [],
  showManualInputs: false,
  email: null,
  firstName: null,
  lastName: null,
  title: null,
  consentedPrivacyPolicy: false,
  userCreationSuccess: null,
  isLoading: false,
  threeYearsOfAddressesProvided: false,
  lastPostcodeMappingTimestamp: 0,
  postcodeToSearchFor: null,
  lgEmployee: false,
};

const setFieldValue = (state, action, stateKey, actionKey) => {
  const newState = {
    ...state,
    [stateKey]: action[actionKey || stateKey],
  };
  return newState;
};

const mapFromServer = (state, { data }) => ({
  ...state,
  ...data.personal,
});

const clearPreviousAddressPendingRecords = state => ({
  ...state,
  addressPreviousArray: (state.addressPreviousArray || []).filter(
    address => address.pending === false
  ),
  postcodeToSearchFor: null,
});

const reducer = {
  [TOGGLE_MANUAL_INPUTS]: state => ({
    ...state,
    showManualInputs: !state.showManualInputs,
  }),
  [SEND_POSTCODE_SEARCH_FAILURE]: (state, action) => ({
    ...state,
    error: action.error,
  }),
  [SEND_POSTCODE_SEARCH_MAP_FIELDS]: (
    state,
    { fieldMappings, subStateKey, addressIndex }
  ) => {
    if (subStateKey && R.is(Number, addressIndex)) {
      const addressList = state[subStateKey] || [];
      const existingItem = addressList[addressIndex] || { pending: true };

      let newAddressList;
      if (R.isEmpty(addressList)) {
        newAddressList = [fieldMappings];
      } else {
        newAddressList = [...addressList];
        newAddressList[addressIndex] = {
          ...existingItem,
          ...fieldMappings,
        };
      }

      return {
        ...state,
        [subStateKey]: newAddressList,
        // we set this so that the component can know whether update it being done
        // by a postcode anywhere search or not in order to update local state
        lastPostcodeMappingTimestamp: new Date().getTime(),
      };
    }
    const newState = {
      ...state,
      ...fieldMappings,
    };
    return newState;
  },
  [SET_ABOUT_ADDRESS_LINE_1]: (state, action) =>
    setFieldValue(state, action, 'addressLine1'),
  [SET_ABOUT_ADDRESS_LINE_2]: (state, action) =>
    setFieldValue(state, action, 'addressLine2'),
  [SET_ABOUT_ADDRESS_LINE_3]: (state, action) =>
    setFieldValue(state, action, 'addressLine3'),
  [SET_ABOUT_POST_CODE]: (state, action) =>
    setFieldValue(state, action, 'addressPostcode', 'postCode'),
  [SET_POSTCODE_TO_SEARCH_FOR]: (state, action) =>
    setFieldValue(state, action, 'postcodeToSearchFor', 'postCode'),
  [SET_TIME_LIVED_AT_CURRENT_ADDRESS]: (state, action) =>
    setFieldValue(
      state,
      action,
      'addressTimeLivedAt',
      'timeLivedAtCurrentAddress'
    ),
  [UPDATE_PREVIOUS_ADDRESS_ARRAY]: (state, action) => {
    const newState = setFieldValue(
      state,
      action,
      'addressPreviousArray',
      'addressPreviousArray'
    );

    newState.postcodeToSearchFor = null;

    return newState;
  },
  [SEND_POSTCODE_SEARCH_RESET]: state => ({
    ...state,
    postcodeToSearchFor: null,
  }),
  [SET_THREE_YEARS_OF_ADDRESSES_PROVIDED]: (state, action) =>
    setFieldValue(
      state,
      action,
      'threeYearsOfAddressesProvided',
      'threeYearsOfAddressesProvided'
    ),
  [HANDLE_USER_INPUT]: (state, action) => ({
    ...state,
    ...action.data,
  }),
  [CREATE_USER_SUCCESS]: (state, action) => ({
    ...state,
    tokenData: action.data,
    userCreationSuccess: true,
    isLoading: false,
    error: null,
  }),
  [CREATE_USER_FAILED]: (state, action) => ({
    ...state,
    isLoading: false,
    error: action.error,
  }),
  [RESET_ERROR]: state => ({
    ...state,
    error: null,
  }),
  [CREATE_USER]: state => ({
    ...state,
    isLoading: true,
  }),
  [MAP_FROM_SERVER]: mapFromServer,
  [CLEAR_PENDING_RECORDS]: clearPreviousAddressPendingRecords,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
