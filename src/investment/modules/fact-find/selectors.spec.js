import test from 'tape';

import {
  investmentGoalSelector,
  atrSelector,
  experienceSelector,
  resilienceSelector,
  flexibilitySelector,
  personalSelector,
  finalChecksSelector,
  isaSelector,
  atrOverride,
  experienceOverride,
  resilienceOverride,
  flexibilityOverride,
  lookupValueInStore,
  lookupFactFindData,
  isLoggedIn,
} from './selectors';

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    investmentGoalSelector({
      investmentGoal: 'fake goal',
    }),
    'fake goal',
    'investmentGoalSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    atrSelector({
      atr: {
        data: 'atr',
      },
    }),
    'atr',
    'atrSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    experienceSelector({
      experience: {
        data: 'experience',
      },
    }),
    'experience',
    'experienceSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    resilienceSelector({
      resilience: {
        data: 'resilience',
      },
    }),
    'resilience',
    'resilienceSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    flexibilitySelector({
      flexibility: {
        data: 'flexibility',
      },
    }),
    'flexibility',
    'flexibilitySelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    personalSelector({
      aboutYou: 'personal',
    }),
    'personal',
    'personalSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    finalChecksSelector({
      finalChecks: 'finalChecks',
    }),
    'finalChecks',
    'finalChecksSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    isaSelector({
      isa: 'isa',
    }),
    'isa',
    'isaSelector'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    atrOverride({
      atr: {
        override: 'atr override',
      },
    }),
    'atr override',
    'atrOverride'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    experienceOverride({
      experience: {
        override: 'experience override',
      },
    }),
    'experience override',
    'experienceOverride'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    resilienceOverride({
      resilience: {
        override: 'resilience override',
      },
    }),
    'resilience override',
    'resilienceOverride'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    flexibilityOverride({
      flexibility: {
        override: 'flexibility override',
      },
    }),
    'flexibility override',
    'flexibilityOverride'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    lookupValueInStore('key', {
      key: 'value',
    }),
    'value',
    'lookupValueInStore'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.deepEqual(
    lookupFactFindData({
      data: 'fact find data',
    }),
    'fact find data',
    'lookupFactFindData'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.true(
    isLoggedIn({
      isLoggedIn: true,
    }),
    'isLoggedIn true'
  );
});

test('fact find selectors', assert => {
  assert.plan(1);

  assert.false(
    isLoggedIn({
      isLoggedIn: false,
    }),
    'isLoggedIn false'
  );
});
