import test from 'tape';
import sinon from 'sinon';

import getServerTime, { __RewireAPI__ } from './get-server-time';

test('get-server-time', async assert => {
  assert.plan(2);

  const sandbox = sinon.sandbox.create();

  const axiosStub = sandbox.stub();
  axiosStub.resolves({ server_timestamp: '2012-01-01T01:00:00Z' });

  __RewireAPI__.__Rewire__('axios', axiosStub);
  __RewireAPI__.__Rewire__('WealthWizards', { CONTEXT_ROUTE: '/investment' });

  const serverTime = await getServerTime();

  assert.deepEqual(
    serverTime,
    { server_timestamp: '2012-01-01T01:00:00Z' },
    'server time returned'
  );

  assert.deepEqual(
    axiosStub.args,
    [['/investment/server-timestamp']],
    'correct uri for server timestamp'
  );

  __RewireAPI__.__ResetDependency__('axios');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});
