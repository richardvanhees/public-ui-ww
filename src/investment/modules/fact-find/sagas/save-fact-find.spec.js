import test from 'tape';
import sinon from 'sinon';
import sinonAsPromised from 'sinon-as-promised';
import { testSaga } from 'redux-saga-test-plan';
import saveFactFindSaga, {
  __RewireAPI__,
  getATRData,
  getInvestmentGoalData,
  getExperienceData,
  getFlexibilityData,
  getResilienceData,
  getPersonalData,
  saveFactFindCall,
  saveFactFind,
  getFactFind,
  getFinalChecksData,
  getIsaData,
  getState,
} from './save-fact-find';
import factFindTypes from '../types';
import factFindSelectors from '../selectors';
import { saveFactFindSuccess, saveFactFindFailure } from '../actions';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';
import co from 'co';
import getServerTime from './get-server-time';

const baseTestData = () => {
  const atrTestData = {
    a2risk__anxious_about_decisions: {
      answerValue: 'strongly_disagree',
    },
    a2risk__comfortable_investing: {
      answerValue: 'strongly_agree',
    },
    a2risk__concerned_about_volatility: {
      answerValue: 'strongly_agree',
    },
    a2risk__describe_as_cautious: {
      answerValue: 'no_opinion',
    },
    a2risk__investment_easy_to_understand: {
      answerValue: 'strongly_agree',
    },
    a2risk__little_investment_experience: {
      answerValue: 'disagree',
    },
    a2risk__long_time_to_decide: {
      answerValue: 'disagree',
    },
    a2risk__prefer_bank_deposits: {
      answerValue: 'no_opinion',
    },
    a2risk__prefer_high_risk_investments: {
      answerValue: 'no_opinion',
    },
    a2risk__prefer_safer_investments: {
      answerValue: 'agree',
    },
    a2risk__risk_equals_opportunity: {
      answerValue: 'strongly_agree',
    },
    a2risk__willing_to_risk: {
      answerValue: 'agree',
    },
  };
  const experienceTestData = {
    investment_experience__general: {
      answerValue: 'yes',
    },
    investment_experience__bank_or_savings: {
      answerValue: 'no',
    },
    investment_experience__cash_isas: {
      answerValue: 'yes',
    },
    investment_experience__endowment: {
      answerValue: 'yes',
    },
    investment_experience__national_savings: {
      answerValue: 'yes',
    },
    investment_experience__portfolio: {
      answerValue: 'yes',
    },
    investment_experience__shares_isa: {
      answerValue: 'yes',
    },
    investment_experience__trusts: {
      answerValue: 'yes',
    },
  };
  const flexibilityTestData = {
    investment_flexibility__contribute_more: {
      answerValue: 'yes',
    },
    investment_flexibility__defer_income: {
      answerValue: 'no',
    },
    investment_flexibility__family_support: {
      answerValue: 'yes',
    },
    investment_flexibility__other_resources: {
      answerValue: 'no',
    },
    investment_flexibility__reduce_income: {
      answerValue: 'yes',
    },
  };
  const resilienceTestData = {
    investment_resilience__discretionary_income: {
      answerValue: 853,
    },
    investment_resilience__gross_income: {
      answerValue: 62791,
    },
    investment_resilience__health_status: {
      answerValue: 'good',
    },
    investment_resilience__investment_significance: {
      answerValue: 'very_significant',
    },
    investment_resilience__occupation: {
      answerValue: 'manager_director_senior_official',
    },
  };
  const goalTestData = {
    investment_investmentgoal_goal: 'test goal',
    investment_investmentgoal_amount: 4000,
    investment_investmentgoal_length: '10 years',
  };
  const personalTestData = {
    personal__address_line_1: '17 towpath close',
    personal__address_line_2: 'coventry',
    personal__dob: '1984-07-24',
    personal__first_name: 'gary george',
    personal__last_name: 'gary george',
    personal__ni_number: 'NL768765C',
    personal__postcode: 'cv6 6rg',
    personal__telephone: '07967405777',
    personal__title: 'Mr',
  };
  const factFindTestData = {
    a2risk__anxious_about_decisions: 'strongly_disagree',
    a2risk__comfortable_investing: 'strongly_agree',
    a2risk__concerned_about_volatility: 'strongly_agree',
    a2risk__describe_as_cautious: 'no_opinion',
    a2risk__investment_easy_to_understand: 'strongly_agree',
    a2risk__little_investment_experience: 'disagree',
    a2risk__long_time_to_decide: 'disagree',
    a2risk__prefer_bank_deposits: 'no_opinion',
    a2risk__prefer_high_risk_investments: 'no_opinion',
    a2risk__prefer_safer_investments: 'agree',
    a2risk__risk_equals_opportunity: 'strongly_disagree',
    a2risk__willing_to_risk: 'agree',
    investment_experience__general: 'yes',
    investment_experience__bank_or_savings: 'no',
    investment_experience__cash_isas: 'yes',
    investment_experience__endowment: 'yes',
    investment_experience__national_savings: 'yes',
    investment_experience__portfolio: 'yes',
    investment_experience__shares_isa: 'yes',
    investment_experience__trusts: 'yes',
    investment_flexibility__contribute_more: 'yes',
    investment_flexibility__defer_income: 'no',
    investment_flexibility__family_support: 'yes',
    investment_flexibility__other_resources: 'no',
    investment_flexibility__reduce_income: 'yes',
    investment_resilience__discretionary_income: 853,
    investment_resilience__gross_income: 62791,
    investment_resilience__health_status: 'good',
    investment_resilience__investment_significance: 'very_significant',
    investment_resilience__occupation: 'manager_director_senior_official',
    personal__address_line_1: '17 towpath close',
    personal__address_line_2: 'coventry',
    personal__dob: '1984-07-24',
    personal__first_name: 'gary george',
    personal__last_name: 'gary george',
    personal__ni_number: 'NL768765C',
    personal__postcode: 'cv6 6rg',
    personal__telephone: '07967405777',
    personal__title: 'Mr',
    retirement__selected_age: 60,
  };

  const finalChecksTestData = {
    investment__borrowing_above_apr_level: 'no',
    investment__maxing_your_employer_pension_scheme: 'yes',
    investment__min_amount_in_savings_available: 'yes',
    investment__money_to_invest_readily_available: 'yes',
  };

  const isaTestData = {
    investment__has_existing_stocks_and_shares_isa: 'no',
    investment__isas: [
      {
        type: 'foo',
        amount: 0,
        in_use: 'no',
      },
    ],
  };

  const termsTestData = {
    investment__accepted_solutions: {
      foo: {
        accepted_advice: true,
        read_documents: true,
      },
    },
  };

  const compiledTestData = {};
  compiledTestData.atr = atrTestData;
  compiledTestData.experience = experienceTestData;
  compiledTestData.resilience = resilienceTestData;
  compiledTestData.personal = personalTestData;
  compiledTestData.flexibility = flexibilityTestData;
  compiledTestData.investmentGoal = goalTestData;
  compiledTestData.isa = isaTestData;
  compiledTestData.finalChecks = finalChecksTestData;
  compiledTestData.terms = termsTestData;

  return {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
    termsTestData,
  };
};

test('saveFactFindSaga', t => {
  let target;

  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => false);

  target = testSaga(saveFactFind, {
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .next(factFindTestData)
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_REQUEST',
      factFind: factFindTestData,
      redirectPath: undefined,
    })
    .next();
  t.equals(!!target.isDone(), true, 'saveFactFindSaga success');

  t.end();
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});

test('saveFactFindSaga', t => {
  let target;

  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => true);

  target = testSaga(saveFactFind, {
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .next(factFindTestData)
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: factFindTypes.RESET_RISK,
      forceReset: true,
    })
    .next()
    .put(setIsLoading(false))
    .next();

  t.equals(!!target.isDone(), true, 'saveFactFindSaga success');

  t.end();
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});

test('saveFactFindSaga', t => {
  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  pushStub.returns({
    type: 'foo',
  });

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => false);

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  const target = testSaga(saveFactFind, {
    redirectPath: '/referral',
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .next(factFindTestData)
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_REQUEST',
      factFind: factFindTestData,
      redirectPath: '/referral',
    })
    .next();

  t.equals(
    !!target.isDone(),
    true,
    'saveFactFindSaga success when it is a referral'
  );

  t.end();

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});

test('saveFactFindSaga', t => {
  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  pushStub.returns({
    type: 'foo',
  });

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  const target = testSaga(saveFactFind, {
    redirectPath: '/referral',
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .throw({
      response: {
        data: {
          code: 'NI_ALREADY_IN_USE',
        },
      },
    })
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'Your national insurance number appears to already be assigned to another account.'
      )
    )
    .next();

  t.true(target.isDone(), 'should handle ni duplicate error warning');

  t.end();

  __RewireAPI__.__ResetDependency__('push');
});

test('saveFactFindSaga', t => {
  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  pushStub.returns({
    type: 'foo',
  });

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeState = {
    factFind: { isLoggedIn: true },
    doInBackground: false,
    investmentGoal: { length: '10 years', target: 0 },
  };

  const target = testSaga(saveFactFind, {
    redirectPath: '/referral',
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .throw({ response: { status: 500 } })
    .put(setIsLoading(false))
    .next()
    .put(
      setError('We are currently unable to save your data, please try again.')
    )
    .next();

  t.true(target.isDone(), 'should handle unknown error');

  t.end();

  __RewireAPI__.__ResetDependency__('push');
});

test('saveFactFindSaga', t => {
  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  pushStub.returns({
    type: 'foo',
  });

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeState = {
    factFind: { isLoggedIn: true },
    doInBackground: false,
    investmentGoal: { length: '10 years', target: 0 },
  };

  const target = testSaga(saveFactFind, {
    redirectPath: '/referral',
    recordSolution: true,
    doInBackground: false,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(saveFactFindCall, factFindTestData, false, false, undefined)
    .throw({ response: { status: undefined } })
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'You appear to be offline, please check your internet connection.'
      )
    )
    .next();

  t.true(target.isDone(), 'should handle unknown error');

  t.end();

  __RewireAPI__.__ResetDependency__('push');
});

test('saveFactFindSaga', t => {
  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  pushStub.returns({
    type: 'foo',
  });

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  const target = testSaga(saveFactFind, {
    redirectPath: '/referral',
    recordSolution: true,
    doInBackground: true,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .fork(saveFactFindCall, factFindTestData, true, false, undefined)
    .next(factFindTestData)
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_REQUEST',
      factFind: factFindTestData,
      redirectPath: '/referral',
    })
    .next();

  t.true(target.isDone(), 'forks request when in background mode');

  t.end();

  __RewireAPI__.__ResetDependency__('push');
});

test('saveFactFindCall', assert => {
  co(function*() {
    assert.plan(1);

    __RewireAPI__.__Rewire__('WealthWizards', {
      FACT_FIND_SVC_URL: '/investment',
    });

    localStorage.setItem('user_token', 'jwt');

    const sandbox = sinon.sandbox.create();

    const putRequestStub = sandbox.stub();
    putRequestStub.resolves({});

    __RewireAPI__.__Rewire__('putRequest', putRequestStub);

    const res = yield saveFactFindCall({}, false);

    localStorage.removeItem('user_token');

    assert.deepEqual(
      putRequestStub.args,
      [
        [
          '/investment/v1/user/fact-find',
          'Bearer jwt',
          { data: {} },
          'json',
          undefined,
        ],
      ],
      'should pass correct args to request when not in background mode'
    );

    __RewireAPI__.__ResetDependency__('putRequest');
    __RewireAPI__.__ResetDependency__('WealthWizards');
  });
});

test('saveFactFindCall', assert => {
  co(function*() {
    assert.plan(1);

    localStorage.setItem('user_token', 'jwt');

    const sandbox = sinon.sandbox.create();

    const putRequestStub = sandbox.stub();
    putRequestStub.resolves({});

    __RewireAPI__.__Rewire__('putRequest', putRequestStub);
    __RewireAPI__.__Rewire__('WealthWizards', {
      FACT_FIND_SVC_URL: '/investment',
    });

    const res = yield saveFactFindCall({}, true);

    localStorage.removeItem('user_token');

    assert.deepEqual(
      putRequestStub.args,
      [
        [
          '/investment/v1/user/fact-find',
          'Bearer jwt',
          { data: {} },
          'json',
          undefined,
        ],
      ],
      'should pass correct args to request when is in background mode'
    );

    __RewireAPI__.__ResetDependency__('putRequest');
  });
});

test('saveFactFindSaga', t => {
  let target;

  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();
  const setCompletedAtStub = sandbox.stub();
  setCompletedAtStub.returns({
    type: 'FAKE_ACTION',
    date: '2012-01-01T01:01:01Z',
  });

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => false);

  const factFindTestDataWithCompletedAt = {
    ...factFindTestData,
    personal__completed_at: '2012-01-01T01:01:01Z',
    investment__flexibility_required: false,
  };

  target = testSaga(saveFactFind, {
    recordSolution: true,
    doInBackground: false,
    withTimestamp: 'personal__completed_at',
    setCompletedAt: setCompletedAtStub,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestData)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2012-01-01T01:01:01Z',
      },
    })
    .call(
      saveFactFindCall,
      factFindTestDataWithCompletedAt,
      false,
      false,
      undefined
    )
    .next(factFindTestDataWithCompletedAt)
    .put({ type: 'FAKE_ACTION', date: '2012-01-01T01:01:01Z' })
    .next()
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_REQUEST',
      factFind: factFindTestDataWithCompletedAt,
      redirectPath: undefined,
    })
    .next();
  t.equals(!!target.isDone(), true, 'saveFactFindSaga success withTimestamp');

  t.end();
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});

test('saveFactFindSaga', t => {
  let target;

  const {
    compiledTestData,
    isaTestData,
    finalChecksTestData,
    factFindTestData,
    personalTestData,
    goalTestData,
    resilienceTestData,
    flexibilityTestData,
    atrTestData,
    experienceTestData,
  } = baseTestData();

  const sandbox = sinon.sandbox.create();
  const setCompletedAtStub = sandbox.stub();
  setCompletedAtStub.returns({
    type: 'FAKE_ACTION',
    date: '2012-01-01T01:01:01Z',
  });

  const fakeState = {
    factFind: { isLoggedIn: true },
    investmentGoal: { length: '10 years', target: 0 },
  };

  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => false);

  const factFindTestDataWithCompletedAt = {
    ...factFindTestData,
    personal__completed_at: '2012-01-01T01:01:01Z',
  };

  target = testSaga(saveFactFind, {
    recordSolution: true,
    doInBackground: false,
    withTimestamp: 'personal__completed_at',
    setCompletedAt: setCompletedAtStub,
  })
    .next()
    .select(getState)
    .next(fakeState)
    .select(getFactFind, fakeState)
    .next(factFindTestDataWithCompletedAt)
    .call(getServerTime)
    .next({
      data: {
        server_timestamp: '2018-01-01T01:01:01Z',
      },
    })
    .call(
      saveFactFindCall,
      factFindTestDataWithCompletedAt,
      false,
      false,
      undefined
    )
    .next(factFindTestDataWithCompletedAt)
    .put({ type: 'FAKE_ACTION', date: '2012-01-01T01:01:01Z' })
    .next()
    .put({
      type: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
      isLoading: true,
    })
    .next()
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_REQUEST',
      factFind: factFindTestDataWithCompletedAt,
      redirectPath: undefined,
    })
    .next();
  t.equals(
    !!target.isDone(),
    true,
    'saveFactFindSaga withTimestamp dont override existing'
  );

  t.end();
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});
