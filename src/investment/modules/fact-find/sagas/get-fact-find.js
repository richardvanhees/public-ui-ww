import { put, takeLatest, all, select } from 'redux-saga/effects';
import R from 'ramda';

import { get } from '../../../../../modules/utils/refresh-request';
import {
  setIsFullLoading,
  setError,
} from '../../../../../modules/browser/actions';
import WealthWizards from '../../../../../modules/wealthwizards';
import { event } from 'react-ga';
import factFindTypes from '../types';
import mapFromServer from '../mappers/map-from-server';
import { getItem } from '../../../../../modules/utils/local-storage';
import axios from 'axios';
import { mapFromServer as mapFromServerAction } from '../actions';
import { push } from 'react-router-redux';
import { logout } from '../../login/actions';

const userCouldBeStillLoggedIn = () => getItem('user_token') !== null;

const loggedInDataCallShouldNotBeMade = () =>
  !userCouldBeStillLoggedIn() ||
  window.location.href.indexOf('/landing') !== -1;

const getResponseCode = R.path(['response', 'status']);

export function* getFactFindCall() {
  if (loggedInDataCallShouldNotBeMade()) {
    return { data: null };
  }

  try {
    return yield get(
      `${WealthWizards.FACT_FIND_SVC_URL}/v1/user/fact-find`,
      `Bearer ${getItem('user_token')}`,
      {}
    );
  } catch (e) {
    return { data: null, responseCode: getResponseCode(e), error: true };
  }
}

export function* makeGetRequestForInvestmentSummary() {
  if (loggedInDataCallShouldNotBeMade()) {
    return { data: null };
  }
  try {
    return yield get(
      `${WealthWizards.SOLUTION_STORE_URL}/v1/user/solution/investment-summary`,
      `Bearer ${getItem('user_token')}`,
      {}
    );
  } catch (e) {
    return { data: null, responseCode: getResponseCode(e), error: true };
  }
}

export const getContentFromState = R.path(['content', 'data', 'investment']);

export function* getContent() {
  const contentFromState = yield select(getContentFromState);

  if (contentFromState) {
    return { data: contentFromState };
  }

  try {
    return yield axios({
      method: 'get',
      url: `${WealthWizards.CONTENT_SERVICE_BASE_URL}/content-service/${
        WealthWizards.CONTENT_SERVICE_VERSION
      }/content/investment-wizard`,
      headers: {
        tenant: WealthWizards.TENANT_KEY,
      },
    });
  } catch (e) {
    event({
      category: 'GetContentError',
      action: `Response code: ${getResponseCode(e)}`,
    });
    return { data: null, responseCode: getResponseCode(e), error: true };
  }
}

/* eslint-disable consistent-return */
export function* getFactFind({ redirectPath, requestIsBeingMadeAfterALogin }) {
  try {
    yield put(setIsFullLoading(true));

    const [
      contentResponse,
      factFindResponse,
      investmentSummaryResponse,
    ] = yield all([
      getContent(),
      getFactFindCall(),
      makeGetRequestForInvestmentSummary(),
    ]);

    if (contentResponse.responseCode >= 400) {
      yield put(logout(false, `${WealthWizards.CONTEXT_ROUTE}/services-down`));
      return yield put(setIsFullLoading(false));
    }

    // set is referral
    const initialLoadData = {
      content: { investment: contentResponse.data },
      factFind: factFindResponse.data,
      solution: investmentSummaryResponse.data,
      isLoggedIn: factFindResponse.data !== null,
    };

    if (userCouldBeStillLoggedIn() && !factFindResponse.data) {
      return yield put(
        logout(
          false,
          requestIsBeingMadeAfterALogin
            ? `${WealthWizards.CONTEXT_ROUTE}/services-down`
            : `${WealthWizards.CONTEXT_ROUTE}/login`
        )
      );
    }

    if (initialLoadData.factFind) {
      yield put(
        mapFromServerAction(
          mapFromServer(
            initialLoadData.factFind,
            initialLoadData.content.investment
          )
        )
      );
    }

    const isUserReferred = R.pathOr(
      false,
      ['solution', 'referral'],
      initialLoadData
    );
    if (isUserReferred) {
      yield put(push(`${WealthWizards.CONTEXT_ROUTE}/referral`));
    }

    yield put.resolve({
      type: factFindTypes.INITIAL_LOAD,
      ...initialLoadData,
    });

    if (
      factFindResponse.responseCode >= 500 ||
      investmentSummaryResponse.responseCode >= 500
    ) {
      yield put(logout(false, `${WealthWizards.CONTEXT_ROUTE}/services-down`));
      return yield put(setIsFullLoading(false));
    }

    // to avoid inconsistent state after a login if failure are due to slow network
    if (
      (!factFindResponse.responseCode && factFindResponse.error) ||
      (!investmentSummaryResponse.responseCode &&
        investmentSummaryResponse.error)
    ) {
      yield put(
        logout(
          false,
          requestIsBeingMadeAfterALogin
            ? `${WealthWizards.CONTEXT_ROUTE}/services-down`
            : `${WealthWizards.CONTEXT_ROUTE}/login`
        )
      );
      return yield put(setIsFullLoading(false));
    }

    yield put({ type: factFindTypes.RESET_RISK });

    if (redirectPath) {
      yield put(push(redirectPath));
    }

    yield put(setIsFullLoading(false));
  } catch (error) {
    yield put(setIsFullLoading(false));

    const responseStatus = getResponseCode(error);

    if (responseStatus !== 404 && responseStatus !== 401) {
      yield put(setError('We are currently unable to fetch your data.'));
    }
  }
}

export default function* getFactFindSaga() {
  yield takeLatest(factFindTypes.GET_FACT_FIND_REQUEST, getFactFind);
}
