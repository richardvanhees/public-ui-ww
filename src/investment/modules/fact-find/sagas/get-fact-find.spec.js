import test from 'tape';
import sinon from 'sinon';
import sinonP from 'sinon-as-promised';
import { testSaga } from 'redux-saga-test-plan';
import {
  setIsFullLoading,
  setError,
} from '../../../../../modules/browser/actions';
import { referral } from '../../../modules/solution/actions';
import { logout } from '../../login/actions';

import getFactFindSaga, {
  getFactFind,
  getFactFindCall,
  getContent,
  getContentFromState,
  makeGetRequestForInvestmentSummary,
  __RewireAPI__,
} from './get-fact-find';
import {
  getFactFindSuccess,
  getFactFindFailure,
  mapFromServer as mapFromServerAction,
  setIsLoggedIn,
} from '../actions';
import factFindTypes from '../types';
import {
  getItem,
  setItem,
  removeItem,
} from '../../../../../modules/utils/local-storage';
const co = require('co');

test('getFactFindSaga', assert => {
  co(function*() {
    assert.plan(1);

    const sandbox = sinon.sandbox.create();

    const getStub = sandbox.stub();

    __RewireAPI__.__Rewire__('WealthWizards', {
      SOLUTION_STORE_URL: 'SOLUTION_STORE_URL',
    });
    __RewireAPI__.__Rewire__('get', getStub);

    localStorage.setItem('user_token', '12345');

    yield makeGetRequestForInvestmentSummary();

    assert.deepEqual(
      getStub.args,
      [
        [
          'SOLUTION_STORE_URL/v1/user/solution/investment-summary',
          'Bearer 12345',
          {},
        ],
      ],
      'get function invoked with correct args'
    );

    __RewireAPI__.__ResetDependency__('get');
    localStorage.removeItem('user_token');
  });
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const axiosStub = sandbox.stub();
  axiosStub.resolves();

  __RewireAPI__.__Rewire__('axios', axiosStub);

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTENT_SERVICE_BASE_URL: 'CONTENT_SERVICE_BASE_URL',
    CONTENT_SERVICE_VERSION: 'CONTENT_SERVICE_VERSION',
    TENANT_KEY: 'TENANT_KEY',
  });

  const target = testSaga(getContent, {})
    .next()
    .select(getContentFromState)
    .next();

  assert.deepEqual(
    axiosStub.args,
    [
      [
        {
          headers: { tenant: 'TENANT_KEY' },
          method: 'get',
          url:
            'CONTENT_SERVICE_BASE_URL/content-service/CONTENT_SERVICE_VERSION/content/investment-wizard',
        },
      ],
    ],
    'args correct for content svc call'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const axiosStub = sandbox.stub();
  axiosStub.resolves();

  __RewireAPI__.__Rewire__('axios', axiosStub);

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTENT_SERVICE_BASE_URL: 'CONTENT_SERVICE_BASE_URL',
    CONTENT_SERVICE_VERSION: 'CONTENT_SERVICE_VERSION',
    TENANT_KEY: 'TENANT_KEY',
  });

  const target = testSaga(getContent, {})
    .next()
    .select(getContentFromState)
    .next('fake content');

  assert.equal(
    axiosStub.callCount,
    0,
    'axiosStub not called because cached content used'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('getFactFindSaga', assert => {
  assert.plan(2);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.resolve({ data: { foo: 'baa' } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([
      { data: 'fake content' },
      { data: 'fake fact find data' },
      { data: 'fake solution data' },
    ])
    .put(mapFromServerAction({}))
    .next()
    .put.resolve({
      type: 'modules/FACT_FIND/INITIAL_LOAD',
      content: { investment: 'fake content' },
      factFind: 'fake fact find data',
      solution: 'fake solution data',
      isLoggedIn: true,
    })
    .next()
    .put({ type: factFindTypes.RESET_RISK })
    .next()
    .put('redirection')
    .next()
    .put(setIsFullLoading(false))
    .next();

  assert.deepEqual(mapFromServerStub.args, [
    ['fake fact find data', 'fake content'],
  ]);

  assert.true(target.isDone(), 'gets fact find data successfully');

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(2);

  localStorage.removeItem('user_token');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.resolve({ data: { foo: 'baa' } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([{ data: 'fake content' }, { data: null }, { data: null }])
    .put.resolve({
      type: 'modules/FACT_FIND/INITIAL_LOAD',
      content: { investment: 'fake content' },
      factFind: null,
      solution: null,
      isLoggedIn: false,
    })
    .next()
    .put({ type: factFindTypes.RESET_RISK })
    .next()
    .put('redirection')
    .next()
    .put(setIsFullLoading(false))
    .next();

  assert.deepEqual(mapFromServerStub.callCount, 0, 'mapFromServer not called');

  assert.true(target.isDone(), 'gets fact find data successfully');

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.resolve({ data: { foo: 'baa' } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([
      { data: 'fake content' },
      { data: 'fake fact find data' },
      { data: 'fake solution data' },
    ])
    .put(mapFromServerAction({}))
    .throw('aaaa')
    .put(setIsFullLoading(false))
    .next()
    .put(setError('We are currently unable to fetch your data.'))
    .next();

  assert.true(target.isDone(), 'gets fact find data successfully');

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.reject({ response: { status: 500 } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([
      { responseCode: 500 },
      { data: 'fake fact find data' },
      { data: 'fake solution data' },
    ])
    .put(logout(false, `${WealthWizards.CONTEXT_ROUTE}/services-down`))
    .next()
    .put(setIsFullLoading(false))
    .next();

  assert.true(
    target.isDone(),
    'redirects to services-down when content fails to load'
  );

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.reject({ response: { status: 500 } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([
      { responseCode: 500 },
      { data: { } },
      { data: 'fake solution data' },
    ])
    .put(logout(false, `${WealthWizards.CONTEXT_ROUTE}/services-down`))
    .next()
    .put(setIsFullLoading(false))
    .next();

  assert.true(
    target.isDone(),
    'redirects to services-down when content fails to load'
  );

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});

test('getFactFindSaga', assert => {
  assert.plan(1);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirection');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.reject({ response: { status: 500 } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({}));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, {}])
    .next([
      { data: 'fake content data' },
      { data: null },
      { data: 'fake solution data' },
    ])
    .put(logout(false, `${WealthWizards.CONTEXT_ROUTE}/login`))
    .next();

  assert.true(
    target.isDone(),
    'redirects to login when fact find is null'
  );

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});


test('getFactFindSaga -> referred user gets redirected', assert => {
  assert.plan(2);

  localStorage.setItem('user_token', '12345');

  const sandbox = sinon.sandbox.create();

  const mapFromServerStub = sandbox.stub();
  mapFromServerStub.returns({});

  const pushStub = sandbox.stub();
  pushStub.returns('redirect to referrral page');

  __RewireAPI__.__Rewire__('mapFromServer', mapFromServerStub);

  const getStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('get', getStub);
  getStub.returns(Promise.resolve({ data: { foo: 'baa' } }));

  const fakeState = { factFind: { isLoggedIn: true } };

  __RewireAPI__.__Rewire__('getContent', () => ({}));
  __RewireAPI__.__Rewire__('getFactFindCall', () => ({}));
  __RewireAPI__.__Rewire__('makeGetRequestForInvestmentSummary', () => ({
    data: { referred: true, referralReasons: [] },
  }));

  const target = testSaga(getFactFind, {
    redirectPath: '/somewhere',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .all([{}, {}, { data: { referred: true, referralReasons: [] } }])
    .next([
      { data: 'fake content' },
      { data: 'fake fact find data' },
      { data: { referred: true, referralReasons: [] } },
    ])
    .put(mapFromServerAction({}))
    .next()
    .put.resolve({
      type: 'modules/FACT_FIND/INITIAL_LOAD',
      content: { investment: 'fake content' },
      factFind: 'fake fact find data',
      solution: { referred: true, referralReasons: [] },
      isLoggedIn: true,
    })
    .next()
    .put({ type: factFindTypes.RESET_RISK })
    .next()
    .put('redirect to referrral page')
    .next()
    .put(setIsFullLoading(false))
    .next();

  assert.deepEqual(mapFromServerStub.args, [
    ['fake fact find data', 'fake content'],
  ]);

  assert.true(target.isDone(), 'gets redirected correctly');

  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('get');
  __RewireAPI__.__ResetDependency__('mapFromServer');

  localStorage.removeItem('user_token');
});
