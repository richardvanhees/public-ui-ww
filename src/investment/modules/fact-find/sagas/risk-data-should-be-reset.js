import moment from 'moment';
import { dynamicConfig } from '../../../../../modules/wealthwizards';

const UTC_FORMAT = 'YYYY-MM-DDTHH:mm:ssZ';

const shouldExpire = (serverTimestamp, timestamp) =>
  timestamp &&
  moment
    .utc(serverTimestamp, UTC_FORMAT)
    .diff(
      moment.utc(timestamp, UTC_FORMAT),
      dynamicConfig().RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH
    ) > dynamicConfig().RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS;

export default (timestamps, serverTimestamp) =>
  timestamps &&
  serverTimestamp &&
  (shouldExpire(serverTimestamp, timestamps.atr) ||
    shouldExpire(serverTimestamp, timestamps.experience) ||
    shouldExpire(serverTimestamp, timestamps.resilience) ||
    shouldExpire(serverTimestamp, timestamps.flexibility));
