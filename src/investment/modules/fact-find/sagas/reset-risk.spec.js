import test from 'tape';
import sinon from 'sinon';
import { testSaga } from 'redux-saga-test-plan';
import resetRiskSaga, {
  resetRisk,
  getContent,
  saveFactFindCall,
  selectSolutionId,
  __RewireAPI__,
} from './reset-risk';
import { RESET_RISK_SECTIONS } from '../actions';
import getServerTime from './get-server-time';
import { lookupGeneratedAtRiskProfileTimestamps } from '../selectors';
import { getFactFind } from './save-fact-find';

test('resetRiskSaga', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const setAlertStub = sandbox.stub();
  setAlertStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setAlert', setAlertStub);

  const target = testSaga(resetRisk, {
    forceReset: true,
  })
    .next()
    .select(selectSolutionId)
    .next(false)
    .put.resolve({ type: RESET_RISK_SECTIONS })
    .next()
    .select(getFactFind)
    .next('a fake fact find')
    .fork(saveFactFindCall, 'a fake fact find')
    .next()
    .select(getContent)
    .next({})
    .put(setAlertStub())
    .next();

  assert.true(target.isDone(), 'resets risk successfully when reset is forced');

  __RewireAPI__.__ResetDependency__('setAlert');
});

test('resetRiskSaga', assert => {
  assert.plan(2);

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();

  const setAlertStub = sandbox.stub();
  setAlertStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('WealthWizards', { CONTEXT_ROUTE: '/investment' });
  __RewireAPI__.__Rewire__('setAlert', setAlertStub);
  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => true);

  const target = testSaga(resetRisk, {
    forceReset: false,
  })
    .next()
    .select(selectSolutionId)
    .next(false)
    .call(getServerTime)
    .next({ data: { server_timestamp: '2012-01-01T01:01:00Z' } })
    .select(lookupGeneratedAtRiskProfileTimestamps)
    .next({ atr: '2011-01-01T01:01:00Z' })

    .put.resolve({ type: RESET_RISK_SECTIONS })
    .next()
    .select(getFactFind)
    .next('a fake fact find')
    .fork(saveFactFindCall, 'a fake fact find')
    .next()
    .select(getContent)
    .next({})
    .put(setAlertStub())
    .next();

  assert.true(
    target.isDone(),
    'resets risk successfully without forcing reset'
  );

  setAlertStub.args[0][0].onCloseCallToActionFunction();

  assert.deepEqual(
    pushStub.args,
    [['/investment/dashboard']],
    'redirected to dashboard on close'
  );

  __RewireAPI__.__ResetDependency__('setAlert');
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
  __RewireAPI__.__ResetDependency__('push');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('resetRiskSaga', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const setAlertStub = sandbox.stub();
  setAlertStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setAlert', setAlertStub);
  __RewireAPI__.__Rewire__('riskDataShouldBeReset', () => false);

  const target = testSaga(resetRisk, {
    forceReset: false,
  })
    .next()
    .select(selectSolutionId)
    .next(false)
    .call(getServerTime)
    .next({ data: { server_timestamp: '2012-01-01T01:01:00Z' } })
    .select(lookupGeneratedAtRiskProfileTimestamps)
    .next({ atr: '2011-01-01T01:01:00Z' });

  assert.true(
    target.isDone(),
    'does not reset risk when riskDataShouldBeReset returns false'
  );

  __RewireAPI__.__ResetDependency__('setAlert');
  __RewireAPI__.__ResetDependency__('riskDataShouldBeReset');
});
