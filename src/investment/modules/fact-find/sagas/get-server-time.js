import WealthWizards from '../../../../../modules/wealthwizards';
import axios from 'axios';

export default () => axios(`${WealthWizards.CONTEXT_ROUTE}/server-timestamp`);
