import test from 'tape';
import sinon from 'sinon';
import tk from 'timekeeper';

import riskDataShouldBeReset, {
  __RewireAPI__,
} from './risk-data-should-be-reset';

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.false(
    riskDataShouldBeReset(null, null),
    'handles both params being null'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.true(
    riskDataShouldBeReset(
      {
        atr: '2012-01-01T00:00:00Z',
      },
      '2012-02-01T00:00:01Z'
    ),
    'handles atr expiring'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.false(
    riskDataShouldBeReset(
      {
        atr: '2012-01-01T00:00:01Z',
      },
      '2012-01-02T00:00:00Z'
    ),
    'handles atr being just within a day'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.true(
    riskDataShouldBeReset(
      {
        flexibility: '2012-01-01T00:00:00Z',
      },
      '2012-02-01T00:00:01Z'
    ),
    'handles flexibility expiring'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.true(
    riskDataShouldBeReset(
      {
        resilience: '2012-01-01T00:00:00Z',
      },
      '2012-02-01T00:00:01Z'
    ),
    'handles resilience expiring'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});

test('risk-data-should-be-reset', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('dynamicConfig', () => ({
    RISK_QUESTIONS_COMPLETION_TIMEOUT_EPOCH: 'days',
    RISK_QUESTIONS_COMPLETION_TIMEOUT_IN_DAYS: 1,
  }));

  assert.true(
    riskDataShouldBeReset(
      {
        experience: '2012-01-01T00:00:00Z',
      },
      '2012-02-01T00:00:01Z'
    ),
    'handles experience expiring'
  );

  __RewireAPI__.__ResetDependency__('dynamicConfig');
});
