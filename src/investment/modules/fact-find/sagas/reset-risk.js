import { put, select, takeLatest, call, fork } from 'redux-saga/effects';
import R from 'ramda';
import { setAlert } from '../../../../../modules/browser/actions';

import WealthWizards from '../../../../../modules/wealthwizards';
import factFindTypes from '../types';
import { push } from 'react-router-redux';
import riskDataShouldBeReset from './risk-data-should-be-reset';
import { RESET_RISK_SECTIONS } from '../actions';
import { lookupGeneratedAtRiskProfileTimestamps } from '../selectors';
import getServerTime from './get-server-time';
import { getItem } from '../../../../../modules/utils/local-storage';
import { getFactFind } from './save-fact-find';
import { putRequest } from '../../../../../modules/utils/refresh-request';

export const getContent = R.path([
  'content',
  'data',
  'investment',
  'riskResetPopup',
]);

export const selectSolutionId = R.path(['solution', 'solution_id']);

export function* saveFactFindCall(factFind) {
  return yield putRequest(
    `${WealthWizards.FACT_FIND_SVC_URL}/v1/user/fact-find`,
    `Bearer ${getItem('user_token')}`,
    { data: factFind }
  );
}

export function* resetRisk({ forceReset }) {
  let resetOfRiskRequired = false;

  const solutionId = yield select(selectSolutionId);
  if (solutionId) {
    // never do a reset when solution has been generated
    return;
  }

  if (!forceReset) {
    const { data: serverTimeResponse } = yield call(getServerTime);
    const { server_timestamp: serverTimestamp } = serverTimeResponse;

    const timestamps = yield select(lookupGeneratedAtRiskProfileTimestamps);

    resetOfRiskRequired = riskDataShouldBeReset(timestamps, serverTimestamp);
  }

  if (forceReset || resetOfRiskRequired) {
    yield put.resolve({ type: RESET_RISK_SECTIONS });

    const factFind = yield select(getFactFind);

    yield fork(saveFactFindCall, factFind);

    const popupContent = yield select(getContent);

    yield put(
      setAlert({
        ...popupContent,
        onCloseCallToActionFunction: () =>
          push(`${WealthWizards.CONTEXT_ROUTE}/dashboard`),
        callToActionFunction: () =>
          push(`${WealthWizards.CONTEXT_ROUTE}/atr/start`),
      })
    );
  }
}

export default function* resetRiskSaga() {
  yield takeLatest(factFindTypes.RESET_RISK, resetRisk);
}
