import { call, put, select, fork, takeEvery } from 'redux-saga/effects';
import WealthWizards from '../../../../../modules/wealthwizards';
import { saveFactFindSuccess } from '../actions';
import factFindTypes from '../types';
import { putRequest, post } from '../../../../../modules/utils/refresh-request';
import { push } from 'react-router-redux';
import { generateSolutionRequest } from '../../../modules/solution/actions';
import { to as atrToServerMapper } from '../mappers/atr';
import { to as investmentExperienceToServerMapper } from '../mappers/investment-experience';
import { to as personalMapper } from '../mappers/personal';
import { to as resilienceMapper } from '../mappers/resilience';
import { to as flexibilityMapper } from '../mappers/flexibility';
import { to as investmentGoalMapper } from '../mappers/investment-goal';
import { to as existingIsasMapper } from '../mappers/existing-isas';
import { to as solutionMetadataMapper } from '../mappers/solution-metadata';
import { to as rightTimeMapper } from '../mappers/right-time';
import R from 'ramda';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';
import getServerTime from './get-server-time';
import {
  isLoggedIn,
  lookupGeneratedAtRiskProfileTimestamps,
} from '../selectors';
import riskDataShouldBeReset from './risk-data-should-be-reset';
import { getItem } from '../../../../../modules/utils/local-storage';

/**
 * Only ever store one request in queue
 */
let queueOfOneRequest;
let requestInProgress = false;

export function* saveFactFindCall(
  factFind,
  doInBackground,
  createFactFind,
  callback
) {
  const requestFunction = createFactFind ? post : putRequest;
  if (!doInBackground) {
    queueOfOneRequest = null;
    return yield requestFunction(
      `${WealthWizards.FACT_FIND_SVC_URL}/v1/user/fact-find`,
      `Bearer ${getItem('user_token')}`,
      { data: factFind },
      'json',
      callback
    );
  }

  // overrite the existing queue of one
  if (requestInProgress) {
    queueOfOneRequest = { factFind };
    return queueOfOneRequest;
  }

  try {
    requestInProgress = true;

    const res = yield requestFunction(
      `${WealthWizards.FACT_FIND_SVC_URL}/v1/user/fact-find`,
      `Bearer ${getItem('user_token')}`,
      { data: factFind },
      'json',
      callback
    );

    requestInProgress = false;

    if (queueOfOneRequest) {
      const factFindData = R.clone(queueOfOneRequest.factFind);
      queueOfOneRequest = null;
      yield saveFactFindCall(factFindData, true);
    }

    return res;
  } catch (e) {
    requestInProgress = false;

    if (queueOfOneRequest) {
      const factFindData = R.clone(queueOfOneRequest.factFind);
      queueOfOneRequest = null;
      yield saveFactFindCall(factFindData, true);
    }
    return null;
  }
}

export const getFactFind = state => ({
  ...atrToServerMapper(state),
  ...investmentExperienceToServerMapper(state),
  ...personalMapper(state),
  ...resilienceMapper(state),
  ...flexibilityMapper(state),
  ...investmentGoalMapper(state),
  ...existingIsasMapper(state),
  ...solutionMetadataMapper(state),
  ...rightTimeMapper(state),
  ...state.finalChecks,
  active_products: ['investment'],
});

export const getState = state => state;

/**
 * A generic function to save fact finds
 * @param recordSolution {Boolean} if true, it triggers a solution to be recorded in the store
 * @param doInBackground {Boolean} if true, we essentially fire and forget
 * @param redirectPath {String} the route path e.g. /referral which the user should be taken to after these actions have been fired
 * @param setCompletedAt {Function} Set completedAt date in state
 */
export function* saveFactFind({
  recordSolution = false,
  redirectPath,
  doInBackground,
  withTimestamp = false,
  createFactFind = false,
  setCompletedAt,
  callback,
}) {
  try {
    if (withTimestamp && !setCompletedAt) {
      console.error('setCompletedAt is required when using withTimestamp.'); // eslint-disable-line no-console
    }

    let serverTimestamp;

    const state = yield select(getState);
    let factFind = yield select(getFactFind, state);

    factFind.investment__flexibility_required = false;

    const loggedIn = isLoggedIn(state.factFind);

    if ((recordSolution && loggedIn) || !!withTimestamp) {
      const { data: serverTimeResponse } = yield call(getServerTime);
      serverTimestamp = serverTimeResponse.server_timestamp;
    }
    if (!!withTimestamp && !factFind[withTimestamp]) {
      factFind = {
        ...factFind,
        [withTimestamp]: serverTimestamp,
      };
    }

    if (loggedIn) {
      const sagaRequestFunction = doInBackground ? fork : call;
      yield sagaRequestFunction(
        saveFactFindCall,
        factFind,
        doInBackground,
        createFactFind,
        callback
      );
    }

    if (withTimestamp) {
      yield put(setCompletedAt(serverTimestamp));
    }

    if (recordSolution && loggedIn) {
      yield put(saveFactFindSuccess(true));
      const resetRequired = riskDataShouldBeReset(
        lookupGeneratedAtRiskProfileTimestamps(state),
        serverTimestamp
      );

      if (resetRequired) {
        yield put({
          type: factFindTypes.RESET_RISK,
          forceReset: true,
        });
        yield put(setIsLoading(false));
      } else {
        yield put(generateSolutionRequest({ factFind, redirectPath }));
      }
    } else if (redirectPath) {
      yield put(saveFactFindSuccess(false));

      yield put(setIsLoading(false));

      const pushUrl =
        redirectPath.indexOf(WealthWizards.CONTEXT_ROUTE) === 0
          ? redirectPath
          : `${WealthWizards.CONTEXT_ROUTE}${redirectPath}`;
      yield put(push(pushUrl));
    }
  } catch (error) {
    yield put(setIsLoading(false));

    const statusCode = R.path(['response', 'status'])(error);

    if (R.path(['response', 'data', 'code'])(error) === 'NI_ALREADY_IN_USE') {
      yield put(
        setError(
          'Your national insurance number appears to already be assigned to another account.'
        )
      );
    } else {
      yield put(
        setError(
          statusCode
            ? 'We are currently unable to save your data, please try again.'
            : 'You appear to be offline, please check your internet connection.'
        )
      );
    }
  }
}

export default function* saveFactFindSaga() {
  yield takeEvery(factFindTypes.SAVE_FACT_FIND_REQUEST, saveFactFind);
}
