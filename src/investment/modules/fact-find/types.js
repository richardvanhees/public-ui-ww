export default {
  GET_FACT_FIND_REQUEST: 'modules/FACT_FIND/GET_FACT_FIND_REQUEST',
  GET_FACT_FIND_SUCCESS: 'modules/FACT_FIND/GET_FACT_FIND_SUCCESS',
  GET_FACT_FIND_FAILURE: 'modules/FACT_FIND/GET_FACT_FIND_FAILURE',

  SAVE_FACT_FIND_REQUEST: 'modules/FACT_FIND/SAVE_FACT_FIND_REQUEST',
  SAVE_FACT_FIND_SUCCESS: 'modules/FACT_FIND/SAVE_FACT_FIND_SUCCESS',
  SAVE_FACT_FIND_FAILURE: 'modules/FACT_FIND/SAVE_FACT_FIND_FAILURE',
  FACT_FIND_AND_SOLUTION_FINISH:
    'modules/FACT_FIND/FACT_FIND_AND_SOLUTION_FINISH',
  SET_REFERRAL_FLAG: 'modules/FACT_FIND/SET_REFERRAL_FLAG',
  SET_IS_LOGGED_IN: 'modules/FACT_FIND/SET_IS_LOGGED_IN',
  RESET_RISK: 'modules/FACT_FIND/RESET_RISK',
  INITIAL_LOAD: 'modules/FACT_FIND/INITIAL_LOAD',
};
