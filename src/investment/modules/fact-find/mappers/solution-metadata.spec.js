import test from 'tape';
import { to, from } from './solution-metadata';

test('solution-metadata mapper', assert => {
  assert.plan(1);

  const result = to({
    solutionMetadata: {
      data: [
        {
          id: 'foo',
          bar: true,
          baz: false
        }
      ]
    }
  });

  assert.deepEqual(
    result,
    {
      investment__solutions: [
        {
          id: 'foo',
          bar: true,
          baz: false
        }
      ],
    },
    'maps to server'
  );
});

test('solution-metadata mapper', assert => {
  assert.plan(1);

  const result = from({
    investment__solutions: [
      {
        id: 'foo',
        bar: true,
        baz: false
      }
    ],
  });

  assert.deepEqual(
    result,
    {
      data: [
        {
          id: 'foo',
          bar: true,
          baz: false
        }
      ]
    },
    'maps from server'
  );
});

test('solution-metadata mapper', assert => {
  assert.plan(1);

  const result = from({
    investment__solutions: undefined,
  });

  assert.deepEqual(
    result,
    {
      data: []
    },
    'maps from server'
  );
});
