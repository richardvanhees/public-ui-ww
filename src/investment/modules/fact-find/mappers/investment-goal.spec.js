import test from 'tape';
import { to, from } from './investment-goal';

test('Investment goal mapper', assert => {
  assert.plan(1);

  const result = to({
    investmentGoal: {
      goal: 'goal',
      amount: 100,
      length: 5,
      lengthValue: 10,
      monthly: 7,
      target: 1000,
      completedAt: '2018-06-07',
    },
  });

  assert.deepEqual(
    result,
    {
      investment_investmentgoal_goal: 'goal',
      investment_investmentgoal_amount: 100,
      investment_investmentgoal_length: 5,
      investment_investmentgoal_length_value: 10,
      investment_investmentgoal_monthly: 7,
      investment_investmentgoal_target: 1000,
      investment_investmentgoal_completed_at: '2018-06-07',
    },
    'maps to server'
  );
});

test('Existing isas mapper', assert => {
  assert.plan(1);

  const result = from({
    investment_investmentgoal_goal: 'goal',
    investment_investmentgoal_amount: 100,
    investment_investmentgoal_length: 5,
    investment_investmentgoal_length_value: 10,
    investment_investmentgoal_target: 500,
    investment_investmentgoal_monthly: 55,
    investment_investmentgoal_completed_at: '2018-06-07',
  });

  assert.deepEqual(
    result,
    {
      goal: 'goal',
      amount: 100,
      length: 5,
      lengthValue: 10,
      target: 500,
      monthly: 55,
      completedAt: '2018-06-07',
    },
    'maps to ui'
  );
});
