import test from 'tape';
import { to, from } from './resilience';

test('resilience mapper', assert => {
  assert.plan(1);

  const result = to({
    resilience: {
      override: {
        key: 'investment_resilience__manual_override',
        profile: 'confident',
      },
      data: {
        investment_resilience__gross_income: 100,
        investment_resilience__monthly_income: 48,
        investment_resilience__monthly_expenses: 99,
        investment_resilience__health_status: 102,
        investment_resilience__investment_significance: 103,
        investment_resilience__occupation: 104,
        investment_resilience__discretionary_income_excess_popup: 'n/a',
        investment_resilience__low_monthly_discretionary_income_popup: 'n/a',
        investment_resilience__discretionary_income_both_popup: 'n/a',
      },

      profile: 'fake profile',
    },
  });

  assert.deepEqual(
    result,
    {
      investment_resilience__monthly_expenses: 99,
      investment_resilience__discretionary_income: 0,
      investment_resilience__gross_income: 100,
      investment_resilience__health_status: 102,
      investment_resilience__monthly_income: 48,
      investment_resilience__investment_significance: 103,
      investment_resilience__occupation: 104,
      investment_resilience_profile: 'fake profile',
      investment_resilience__manual_override: 'Confident',
      investment_resilience__discretionary_income_excess_popup: 'n/a',
      investment_resilience__low_monthly_discretionary_income_popup: 'n/a',
      investment_resilience__discretionary_income_both_popup: 'n/a',
    },
    'maps to server'
  );
});

test('resilience mapper', assert => {
  assert.plan(1);

  const result = from({
    investment_resilience__gross_income: 100,
    investment_resilience__monthly_income: 48,
    investment_resilience__health_status: 102,
    investment_resilience__investment_significance: 103,
    investment_resilience__discretionary_income: 0,
    investment_resilience__occupation: 104,
    investment_resilience__monthly_expenses: 99,
    investment_resilience__manual_override: 'Confident',
    investment_resilience__discretionary_income_excess_popup: 'yes',
    investment_resilience__low_monthly_discretionary_income_popup: 'n/a',
    investment_resilience__discretionary_income_both_popup: 'n/a',
  });

  assert.deepEqual(
    result,
    {
      override: {
        key: 'investment_resilience__manual_override',
        profile: 'confident',
      },
      data: {
        investment_resilience__monthly_expenses: 99,
        investment_resilience__gross_income: 100,
        investment_resilience__monthly_income: 48,
        investment_resilience__health_status: 102,
        investment_resilience__investment_significance: 103,
        investment_resilience__occupation: 104,
        investment_resilience__discretionary_income_excess_popup: 'yes',
        investment_resilience__low_monthly_discretionary_income_popup: 'n/a',
        investment_resilience__discretionary_income_both_popup: 'n/a',
      },
      profile: {},
    },
    'maps to client'
  );
});
