import R from 'ramda';
import {
  findAnswer,
  mapKeyValuesFromServerToUIComponentsDataStructure,
  upperCaseFirst,
} from './utils';

export const to = R.applySpec({
  investment_flexibility__defer_income: findAnswer(
    'investment_flexibility__defer_income',
    'flexibility'
  ),
  investment_flexibility__other_resources: findAnswer(
    'investment_flexibility__other_resources',
    'flexibility'
  ),
  investment_flexibility__reduce_income: findAnswer(
    'investment_flexibility__reduce_income',
    'flexibility'
  ),
  investment_flexibility__family_support: findAnswer(
    'investment_flexibility__family_support',
    'flexibility'
  ),
  investment_flexibility__contribute_more: findAnswer(
    'investment_flexibility__contribute_more',
    'flexibility'
  ),
  investment_flexibility_profile: R.pathOr(null, ['flexibility', 'profile']),
  investment_flexibility__manual_override: R.compose(
    upperCaseFirst,
    R.path(['flexibility', 'override', 'profile'])
  ),
});

export const from = (dataLoadedFromTheServer, content) => ({
  data: mapKeyValuesFromServerToUIComponentsDataStructure(
    dataLoadedFromTheServer,
    content
  ),
  profile: dataLoadedFromTheServer.investment_flexibility_profile || {},
  override: dataLoadedFromTheServer.investment_flexibility__manual_override
    ? {
        key: 'investment_flexibility__manual_override',
        profile: dataLoadedFromTheServer.investment_flexibility__manual_override.toLowerCase(),
      }
    : false,
});
