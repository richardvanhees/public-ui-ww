import test from 'tape';
import { to, from } from './flexibility';

test('flexibility mapper', assert => {
  assert.plan(1);

  const result = to({
    flexibility: {
      override: {
        key: 'investment_flexibility__manual_override',
        profile: 'Confident',
      },
      data: [
        {
          answerKey: 'investment_flexibility__defer_income',
          answerValue: 'yes',
        },
        {
          answerKey: 'investment_flexibility__other_resources',
          answerValue: 'yes',
        },
        {
          answerKey: 'investment_flexibility__reduce_income',
          answerValue: 'yes',
        },
        {
          answerKey: 'investment_flexibility__family_support',
          answerValue: 'yes',
        },
        {
          answerKey: 'investment_flexibility__contribute_more',
          answerValue: 'yes',
        },
      ],
    },
  });

  assert.deepEqual(
    result,
    {
      investment_flexibility__defer_income: 'yes',
      investment_flexibility__other_resources: 'yes',
      investment_flexibility__reduce_income: 'yes',
      investment_flexibility__family_support: 'yes',
      investment_flexibility__contribute_more: 'yes',
      investment_flexibility_profile: null,
      investment_flexibility__manual_override: 'Confident',
    },
    'maps to server'
  );
});

test('flexibility mapper', assert => {
  assert.plan(1);

  const result = from(
    {
      investment_flexibility__defer_income: 'yes',
      investment_flexibility__other_resources: 'yes',
      investment_flexibility__reduce_income: 'yes',
      investment_flexibility__family_support: 'yes',
      investment_flexibility__contribute_more: 'yes',
      investment_flexibility__manual_override: 'Confident',
    },
    require('../../../../../content/investment/athena/investment.json')
      .flexibilityQuestions
  );

  assert.deepEqual(
    result,
    {
      override: {
        key: 'investment_flexibility__manual_override',
        profile: 'confident',
      },
      data: [
        {
          answerId: 0,
          answerKey: 'investment_flexibility__defer_income',
          answerValue: 'yes',
        },
        {
          answerId: 0,
          answerKey: 'investment_flexibility__contribute_more',
          answerValue: 'yes',
        },
        {
          answerId: 0,
          answerKey: 'investment_flexibility__other_resources',
          answerValue: 'yes',
        },
        {
          answerId: 0,
          answerKey: 'investment_flexibility__reduce_income',
          answerValue: 'yes',
        },
        {
          answerId: 0,
          answerKey: 'investment_flexibility__family_support',
          answerValue: 'yes',
        },
      ],
      profile: {},
    },
    'maps to server'
  );
});
