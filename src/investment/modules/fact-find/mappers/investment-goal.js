import R from 'ramda';

export const to = R.applySpec({
  investment_investmentgoal_goal: R.path(['investmentGoal', 'goal']),
  investment_investmentgoal_amount: R.path(['investmentGoal', 'amount']),
  investment_investmentgoal_monthly: R.path(['investmentGoal', 'monthly']),
  investment_investmentgoal_length: R.path(['investmentGoal', 'length']),
  investment_investmentgoal_length_value: R.path([
    'investmentGoal',
    'lengthValue',
  ]),
  investment_investmentgoal_target: R.path(['investmentGoal', 'target']),
  investment_investmentgoal_completed_at: R.path([
    'investmentGoal',
    'completedAt',
  ]),
});

export const from = R.applySpec({
  goal: R.path(['investment_investmentgoal_goal']),
  amount: R.path(['investment_investmentgoal_amount']),
  monthly: R.path(['investment_investmentgoal_monthly']),
  length: R.path(['investment_investmentgoal_length']),
  lengthValue: R.path(['investment_investmentgoal_length_value']),
  target: R.path(['investment_investmentgoal_target']),
  completedAt: R.path(['investment_investmentgoal_completed_at']),
});
