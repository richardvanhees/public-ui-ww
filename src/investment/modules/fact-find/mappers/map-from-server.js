import { from as personalMapper } from './personal';
import { from as resilienceMapper } from './resilience';
import { from as investmentGoalMapper } from './investment-goal';
import { from as investmentExperienceMapper } from './investment-experience';
import { from as flexibilityMapper } from './flexibility';
import { from as existingIsasMapper } from './existing-isas';
import { from as atrMapper } from './atr';
import { from as finalChecksMapper } from './final-checks';
import { from as solutionMetadataMapper } from './solution-metadata';
import { from as rightTimeMapper } from './right-time';

export default (factFind, content) => ({
  personal: personalMapper(factFind),
  resilience: resilienceMapper(factFind),
  investmentGoal: investmentGoalMapper(factFind),
  experience: investmentExperienceMapper(factFind, content.experienceQuestions),
  flexibility: flexibilityMapper(factFind, content.flexibilityQuestions),
  existingIsas: existingIsasMapper(factFind),
  atr: atrMapper(factFind, content.atrQuestions),
  finalChecks: finalChecksMapper(factFind),
  solutionMetadata: solutionMetadataMapper(factFind),
  rightTime: rightTimeMapper(factFind, content.rightTime),
});
