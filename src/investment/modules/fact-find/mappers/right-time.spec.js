import test from 'tape';
import { to, from } from './right-time';

test('right time mapper', assert => {
  assert.plan(1);

  const result = to({
    rightTime: {
      userInput: {
        '0': {
          answerId: 0,
          answerKey: 'right_time__relationship_break',
          answerValue: 'yes'
        },
        '1': {
          answerId: 0,
          answerKey: 'right_time__bereavement',
          answerValue: 'yes'
        },
        '2': {
          answerId: 1,
          answerKey: 'right_time__employment_change',
          answerValue: 'no'
        },
        '3': {
          answerId: 0,
          answerKey: 'right_time__health_issues',
          answerValue: 'yes'
        },
        '4': {
          answerId: 0,
          answerKey: 'right_time__ISA_help',
          answerValue: 'yes'
        }
      },
      checkboxInput: {
        right_time__more_reassured: true,
        right_time__english_not_first_language: true,
        right_time__struggle_with_reading: false,
      }
    }
  });

  assert.deepEqual(
    result,
    {
      right_time__relationship_break: 'yes',
      right_time__bereavement: 'yes',
      right_time__employment_change: 'no',
      right_time__health_issues: 'yes',
      right_time__ISA_help: 'yes',
      right_time__english_not_first_language: true,
      right_time__more_reassured: true,
      right_time__not_good_with_technology: false,
      right_time__struggle_to_understand: false,
      right_time__struggle_with_reading: false,
    },
    'maps to server'
  );
});

test('right time mapper', assert => {
  assert.plan(1);

  const result = from(
    {
      right_time__relationship_break: 'yes',
      right_time__bereavement: 'yes',
      right_time__employment_change: 'no',
      right_time__health_issues: 'yes',
      right_time__ISA_help: 'yes',
      right_time__english_not_first_language: true,
      right_time__more_reassured: true,
      right_time__not_good_with_technology: false,
      right_time__struggle_to_understand: false,
      right_time__struggle_with_reading: false,
    },
    require('../../../../../content/investment/athena/investment.json')
      .rightTime
  );

  assert.deepEqual(
    result,
    {
      checkboxInput: {
        right_time__english_not_first_language: true,
        right_time__more_reassured: true,
        right_time__not_good_with_technology: false,
        right_time__struggle_to_understand: false,
        right_time__struggle_with_reading: false,
      },
      userInput: {
        '0': {
          answerId: 0,
          answerKey: 'right_time__relationship_break',
          answerValue: 'yes'
        },
        '1': {
          answerId: 0,
          answerKey: 'right_time__bereavement',
          answerValue: 'yes'
        },
        '2': {
          answerId: 1,
          answerKey: 'right_time__employment_change',
          answerValue: 'no'
        },
        '3': {
          answerId: 0,
          answerKey: 'right_time__health_issues',
          answerValue: 'yes'
        },
        '4': {
          answerId: 0,
          answerKey: 'right_time__ISA_help',
          answerValue: 'yes'
        }
      },
    },
    'maps to server'
  );
});
