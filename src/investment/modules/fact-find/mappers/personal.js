import R from 'ramda';
import { capitalize } from '../../../../../modules/utils/formatting';

const resolveDob = factFind => {
  const defaultDob = '1900-01-01';
  const dob = R.pathOr(defaultDob, ['aboutYou', 'dob'])(factFind);
  if (R.isEmpty(dob)) return defaultDob;
  return dob;
};

const toUpperCase = (value) => value ? value.toUpperCase() : value;
const capitalizeLastName = (value) => {
  const [first, second] = R.split(' ', value);
  return second ? R.join(' ', [first, capitalize(second)]) : capitalize(value);
};

export const to = R.applySpec({
  personal__dob: resolveDob,
  personal__postcode: R.path(['aboutYou', 'addressPostcode']),
  personal__address_time_lived_at: R.path(['aboutYou', 'addressTimeLivedAt']),
  personal__previous_addresses: R.path(['aboutYou', 'addressPreviousArray']),
  personal__three_years_of_addresses_provided: R.path([
    'aboutYou',
    'threeYearsOfAddressesProvided',
  ]),
  personal__email_address: R.path(['aboutYou', 'email']),
  personal__address_line_1: R.path(['aboutYou', 'addressLine1']),
  personal__address_line_2: R.path(['aboutYou', 'addressLine2']),
  personal__address_line_3: R.path(['aboutYou', 'addressLine3']),
  personal__telephone: R.path(['aboutYou', 'contactNumber']),
  personal__ni_number: R.compose(
    R.ifElse(
      R.isNil,
      R.identity,
      toUpperCase
    ),
    R.path(['aboutYou', 'niNumber'])
  ),
  personal__first_name: R.compose(
    R.ifElse(
      R.isNil,
      R.identity,
      capitalize
    ),
    R.path(['aboutYou', 'firstName'])
  ),
  personal__last_name: R.compose(
    R.ifElse(
      R.isNil,
      R.identity,
      capitalizeLastName
    ),
    R.path(['aboutYou', 'lastName']),
  ),
  personal__title: R.path(['aboutYou', 'title']),
  personal__gender: () => 'u',
  personal__lg_employee: R.path(['aboutYou', 'lgEmployee']),
});

export const from = R.applySpec({
  dob: factFind => {
    const dob = R.path(['personal__dob'])(factFind);
    if (dob === '1900-01-01') {
      return '';
    }
    return dob;
  },
  addressPostcode: R.path(['personal__postcode']),
  addressTimeLivedAt: R.path(['personal__address_time_lived_at']),
  addressPreviousArray: R.path(['personal__previous_addresses']),
  threeYearsOfAddressesProvided: R.path([
    'personal__three_years_of_addresses_provided',
  ]),
  addressLine1: R.path(['personal__address_line_1']),
  addressLine2: R.path(['personal__address_line_2']),
  addressLine3: R.path(['personal__address_line_3']),
  contactNumber: R.path(['personal__telephone']),
  email: R.path(['personal__email_address']),
  niNumber: R.path(['personal__ni_number']),
  firstName: R.path(['personal__first_name']),
  lastName: R.path(['personal__last_name']),
  title: R.path(['personal__title']),
  gender: () => 'u',
  completedAt: R.pathOr(null, ['personal__completed_at']),
  lgEmployee: R.path(['personal__lg_employee']),
});
