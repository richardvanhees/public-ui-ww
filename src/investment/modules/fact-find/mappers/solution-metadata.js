import R from 'ramda';

export const to = R.applySpec({
  investment__solutions: R.path([
    'solutionMetadata',
    'data',
  ]),
});

export const from = R.applySpec({
  data: R.pathOr([], ['investment__solutions']),
});
