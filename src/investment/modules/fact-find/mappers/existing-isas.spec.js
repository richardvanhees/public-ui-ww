import test from 'tape';
import { to, from } from './existing-isas';

test('Existing isas mapper', assert => {
  assert.plan(1);

  const result = to({
    isa: {
      hasExisitingStocksAndSharesIsa: 'yes',
      isas: [],
      contributedToIsas: 'yes',
    },
  });

  assert.deepEqual(
    result,
    {
      investment__has_existing_stocks_and_shares_isa: 'yes',
      investment__isas: [],
      investment__contributed_to_isas: 'yes',
    },
    'maps to server'
  );
});

test('Existing isas mapper', assert => {
  assert.plan(1);

  const result = from({
    investment__has_existing_stocks_and_shares_isa: 'yes',
    investment__isas: [],
    investment__contributed_to_isas: 'yes',
  });

  assert.deepEqual(
    result,
    {
      hasExisitingStocksAndSharesIsa: 'yes',
      isas: [],
      contributedToIsas: 'yes',
    },
    'maps to ui'
  );
});

test('Existing isas mapper', assert => {
  assert.plan(1);

  const result = to({
    isa: {
      hasExisitingStocksAndSharesIsa: 'yes',
      isas: [
        { type: 'cash', amount: 0, in_use: 'yes' },
        { type: 'lifetime', amount: 0, in_use: 'yes' },
        { type: 'innovation', amount: 0, in_use: 'yes' },
      ],
      contributedToIsas: 'no',
    },
  });

  assert.deepEqual(
    result,
    {
      investment__has_existing_stocks_and_shares_isa: 'no',
      investment__isas: [
        { type: 'cash', amount: 0, in_use: 'no' },
        { type: 'lifetime', amount: 0, in_use: 'no' },
        { type: 'innovation', amount: 0, in_use: 'no' },
      ],
      investment__contributed_to_isas: 'no',
    },
    'maps to server'
  );
});
