import R from 'ramda';

export const to = R.applySpec({
  investment__money_to_invest_readily_available: R.path([
    'finalChecks',
    'investment__money_to_invest_readily_available',
  ]),
  investment__min_amount_in_savings_available: R.path([
    'finalChecks',
    'investment__min_amount_in_savings_available',
  ]),
  investment__borrowing_above_apr_level: R.path([
    'finalChecks',
    'investment__borrowing_above_apr_level',
  ]),
  investment__maxing_your_employer_pension_scheme: R.path([
    'finalChecks',
    'investment__maxing_your_employer_pension_scheme',
  ]),
  investment__reconsider_pay_into_pension: R.path([
    'finalChecks',
    'investment__reconsider_pay_into_pension',
  ]),
  investment__hid_borrowing_above_30: R.path([
    'finalChecks',
    'investment__hid_borrowing_above_30',
  ]),
  investment__hid_borrowing_below_30: R.path([
    'finalChecks',
    'investment__hid_borrowing_below_30',
  ]),
  investment__hid_mortgage: R.path(['finalChecks', 'investment__hid_mortgage']),
  investment__hid_borrowing_above_30_short_term: R.path([
    'finalChecks',
    'investment__hid_borrowing_above_30_short_term',
  ]),
  investment__hid_borrowing_above_30_taken_advice: R.path([
    'finalChecks',
    'investment__hid_borrowing_above_30_taken_advice',
  ]),
  investment__hid_borrowing_above_30_getting_worse: R.path([
    'finalChecks',
    'investment__hid_borrowing_above_30_getting_worse',
  ]),
  investment__hid_borrowing_above_30_struggling: R.path([
    'finalChecks',
    'investment__hid_borrowing_above_30_struggling',
  ]),
  investment__completed_at: R.path(['finalChecks', 'investment__completed_at']),
});

export const from = R.applySpec({
  investment__money_to_invest_readily_available: R.path([
    'investment__money_to_invest_readily_available',
  ]),
  investment__min_amount_in_savings_available: R.path([
    'investment__min_amount_in_savings_available',
  ]),
  investment__borrowing_above_apr_level: R.path([
    'investment__borrowing_above_apr_level',
  ]),
  investment__maxing_your_employer_pension_scheme: R.path([
    'investment__maxing_your_employer_pension_scheme',
  ]),
  investment__reconsider_pay_into_pension: R.path([
    'investment__reconsider_pay_into_pension',
  ]),
  investment__completed_at: R.path(['investment__completed_at']),
  investment__hid_borrowing_above_30: R.path([
    'investment__hid_borrowing_above_30',
  ]),
  investment__hid_borrowing_below_30: R.path([
    'investment__hid_borrowing_below_30',
  ]),
  investment__hid_mortgage: R.path(['investment__hid_mortgage']),
  investment__hid_borrowing_above_30_short_term: R.path([
    'investment__hid_borrowing_above_30_short_term',
  ]),
  investment__hid_borrowing_above_30_taken_advice: R.path([
    'investment__hid_borrowing_above_30_taken_advice',
  ]),
  investment__hid_borrowing_above_30_getting_worse: R.path([
    'investment__hid_borrowing_above_30_getting_worse',
  ]),
  investment__hid_borrowing_above_30_struggling: R.path([
    'investment__hid_borrowing_above_30_struggling',
  ]),
});
