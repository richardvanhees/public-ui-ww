import R from 'ramda';
import { upperCaseFirst } from './utils';
import { getDiscretionaryIncome } from '../../resilience/selectors';

export const to = R.applySpec({
  investment_resilience__health_status: R.path([
    'resilience',
    'data',
    'investment_resilience__health_status',
  ]),
  investment_resilience__investment_significance: R.path([
    'resilience',
    'data',
    'investment_resilience__investment_significance',
  ]),
  investment_resilience__occupation: R.path([
    'resilience',
    'data',
    'investment_resilience__occupation',
  ]),
  investment_resilience__gross_income: R.path([
    'resilience',
    'data',
    'investment_resilience__gross_income',
  ]),
  investment_resilience__monthly_income: R.path([
    'resilience',
    'data',
    'investment_resilience__monthly_income',
  ]),
  investment_resilience__discretionary_income: state =>
    getDiscretionaryIncome(state),
  investment_resilience__monthly_expenses: R.path([
    'resilience',
    'data',
    'investment_resilience__monthly_expenses',
  ]),
  investment_resilience_profile: R.pathOr(null, ['resilience', 'profile']),
  investment_resilience__manual_override: R.compose(
    upperCaseFirst,
    R.path(['resilience', 'override', 'profile'])
  ),
  investment_resilience__discretionary_income_excess_popup: R.path([
    'resilience',
    'data',
    'investment_resilience__discretionary_income_excess_popup',
  ]),
  investment_resilience__low_monthly_discretionary_income_popup: R.path([
    'resilience',
    'data',
    'investment_resilience__low_monthly_discretionary_income_popup',
  ]),
  investment_resilience__discretionary_income_both_popup: R.path([
    'resilience',
    'data',
    'investment_resilience__discretionary_income_both_popup',
  ]),
});

export const from = factFind => ({
  data: {
    investment_resilience__gross_income: R.path([
      'investment_resilience__gross_income',
    ])(factFind),
    investment_resilience__monthly_income: R.path([
      'investment_resilience__monthly_income',
    ])(factFind),
    investment_resilience__monthly_expenses: R.path([
      'investment_resilience__monthly_expenses',
    ])(factFind),
    investment_resilience__occupation: R.path([
      'investment_resilience__occupation',
    ])(factFind),
    investment_resilience__investment_significance: R.path([
      'investment_resilience__investment_significance',
    ])(factFind),
    investment_resilience__health_status: R.path([
      'investment_resilience__health_status',
    ])(factFind),
    investment_resilience__discretionary_income_excess_popup: R.path([
      'investment_resilience__discretionary_income_excess_popup',
    ])(factFind),
    investment_resilience__low_monthly_discretionary_income_popup: R.path([
      'investment_resilience__low_monthly_discretionary_income_popup',
    ])(factFind),
    investment_resilience__discretionary_income_both_popup: R.path([
      'investment_resilience__discretionary_income_both_popup',
    ])(factFind),
  },
  profile: factFind.investment_resilience_profile || {},
  override: factFind.investment_resilience__manual_override
    ? {
        key: 'investment_resilience__manual_override',
        profile: factFind.investment_resilience__manual_override.toLowerCase(),
      }
    : false,
});
