import R from 'ramda';

export const to = state => {
  const result = R.applySpec({
    investment__has_existing_stocks_and_shares_isa: R.path([
      'isa',
      'hasExisitingStocksAndSharesIsa',
    ]),
    investment__isas: R.path(['isa', 'isas']),
    investment__contributed_to_isas: R.path(['isa', 'contributedToIsas']),
  })(state);

  // ensure everything reset if user switches back answer
  if (result.investment__contributed_to_isas === 'no') {
    result.investment__has_existing_stocks_and_shares_isa = 'no';
    result.investment__isas = result.investment__isas.map(isa => ({
      ...isa,
      in_use: 'no',
    }));
  }

  return result;
};

export const from = R.applySpec({
  hasExisitingStocksAndSharesIsa: R.path([
    'investment__has_existing_stocks_and_shares_isa',
  ]),
  isas: R.path(['investment__isas']),
  contributedToIsas: R.path(['investment__contributed_to_isas']),
});
