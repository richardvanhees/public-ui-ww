import R from 'ramda';
import { upperCaseFirst } from './utils';

export const to = state => ({
  ...R.pathOr({}, ['experience', 'data'])(state),
  investment_experience_profile: R.pathOr(null, ['experience', 'profile'])(
    state
  ),
  investment_experience__manual_override: R.compose(
    upperCaseFirst,
    R.path(['experience', 'override', 'profile'])
  )(state),
});

export const from = (factFind, experienceQuestions) => {
  const payloadIds = R.map(R.prop('answerKey'))(experienceQuestions);
  return {
    data: payloadIds.reduce((acc, id) => {
      // eslint-disable-next-line
      acc[id] = R.path([id])(factFind);
      return acc;
    }, {}),
    profile: factFind.investment_experience_profile || {},
    override: factFind.investment_experience__manual_override
      ? {
          key: 'investment_experience__manual_override',
          profile: factFind.investment_experience__manual_override.toLowerCase(),
        }
      : false,
  };
};
