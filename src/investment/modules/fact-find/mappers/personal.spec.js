import test from 'tape';
import { to, from } from './personal';

test('Personal mapper', assert => {
  assert.plan(1);

  const result = to({
    aboutYou: {
      addressLine1: 'address1',
      addressLine2: 'address2',
      addressLine3: 'address3',
      addressPostcode: 'B301JJ',
      addressTimeLivedAt: 'February 2017',
      addressPreviousArray: [{ addressLine1: 'fake line' }],
      threeYearsOfAddressesProvided: true,
      contactNumber: '01214333570',
      dob: '1980-01-01',
      firstName: 'first',
      lastName: 'van hees',
      niNumber: 'jt593597b',
      title: 'Mr',
      gender: 'u',
      email: 'test@support.com',
      lgEmployee: true,
    },
  });

  assert.deepEqual(
    result,
    {
      personal__address_line_1: 'address1',
      personal__address_line_2: 'address2',
      personal__address_line_3: 'address3',
      personal__dob: '1980-01-01',
      personal__first_name: 'First',
      personal__last_name: 'van Hees',
      personal__ni_number: 'JT593597B',
      personal__postcode: 'B301JJ',
      personal__address_time_lived_at: 'February 2017',
      personal__previous_addresses: [{ addressLine1: 'fake line' }],
      personal__three_years_of_addresses_provided: true,
      personal__telephone: '01214333570',
      personal__title: 'Mr',
      personal__gender: 'u',
      personal__email_address: 'test@support.com',
      personal__lg_employee: true,
    },
    'maps to server'
  );
});

test('Personal mapper', assert => {
  assert.plan(1);

  const result = to({
    aboutYou: {
      addressLine1: 'address1',
      addressLine2: 'address2',
      addressLine3: 'address3',
      addressPostcode: 'B301JJ',
      addressTimeLivedAt: 'February 2017',
      addressPreviousArray: [{ addressLine1: 'fake line' }],
      threeYearsOfAddressesProvided: true,
      contactNumber: '01214333570',
      dob: '1980-01-01',
      firstName: '',
      lastName: 'foo',
      niNumber: null,
      title: 'Mr',
      gender: 'u',
      email: 'test@support.com',
      lgEmployee: true,
    },
  });

  assert.deepEqual(
    result,
    {
      personal__address_line_1: 'address1',
      personal__address_line_2: 'address2',
      personal__address_line_3: 'address3',
      personal__dob: '1980-01-01',
      personal__first_name: '',
      personal__last_name: 'Foo',
      personal__ni_number: null,
      personal__postcode: 'B301JJ',
      personal__address_time_lived_at: 'February 2017',
      personal__previous_addresses: [{ addressLine1: 'fake line' }],
      personal__three_years_of_addresses_provided: true,
      personal__telephone: '01214333570',
      personal__title: 'Mr',
      personal__gender: 'u',
      personal__email_address: 'test@support.com',
      personal__lg_employee: true,
    },
    'maps to server'
  );
});

test('Personal mapper', assert => {
  assert.plan(1);

  const result = from({
    personal__dob: '1980-01-01',
    personal__postcode: 'B301JJ',
    personal__address_line_1: 'address1',
    personal__address_line_2: 'address2',
    personal__address_line_3: 'address3',
    personal__address_time_lived_at: 'February 2017',
    personal__previous_addresses: [{ addressLine1: 'fake line' }],
    personal__three_years_of_addresses_provided: true,
    personal__telephone: '01214333570',
    personal__ni_number: 'JT593597B',
    personal__first_name: 'first',
    personal__last_name: 'last',
    personal__title: 'Mr',
    personal__gender: 'u',
    personal__email_address: 'test@support.com',
    personal__lg_employee: true,
  });

  assert.deepEqual(
    result,
    {
      addressLine1: 'address1',
      addressLine2: 'address2',
      addressLine3: 'address3',
      addressPostcode: 'B301JJ',
      addressTimeLivedAt: 'February 2017',
      addressPreviousArray: [{ addressLine1: 'fake line' }],
      threeYearsOfAddressesProvided: true,
      contactNumber: '01214333570',
      dob: '1980-01-01',
      firstName: 'first',
      lastName: 'last',
      niNumber: 'JT593597B',
      title: 'Mr',
      gender: 'u',
      completedAt: null,
      email: 'test@support.com',
      lgEmployee: true,
    },
    'maps back to state'
  );
});

test('Personal mapper', assert => {
  assert.plan(1);

  const result = from({
    personal__dob: '1900-01-01',
    personal__postcode: 'B301JJ',
    personal__address_line_1: 'address1',
    personal__address_line_2: 'address2',
    personal__address_line_3: 'address3',
    personal__address_time_lived_at: 'February 2017',
    personal__previous_addresses: [{ addressLine1: 'fake line' }],
    personal__three_years_of_addresses_provided: true,
    personal__telephone: '01214333570',
    personal__ni_number: 'JT593597B',
    personal__first_name: 'first',
    personal__last_name: 'last',
    personal__title: 'Mr',
    personal__gender: 'u',
    personal__email_address: 'test@support.com',
    personal__lg_employee: true,
  });

  assert.deepEqual(
    result,
    {
      addressLine1: 'address1',
      addressLine2: 'address2',
      addressLine3: 'address3',
      addressPostcode: 'B301JJ',
      addressTimeLivedAt: 'February 2017',
      addressPreviousArray: [{ addressLine1: 'fake line' }],
      threeYearsOfAddressesProvided: true,
      contactNumber: '01214333570',
      dob: '',
      firstName: 'first',
      lastName: 'last',
      niNumber: 'JT593597B',
      title: 'Mr',
      gender: 'u',
      completedAt: null,
      email: 'test@support.com',
      lgEmployee: true,
    },
    'blanks out dob when set to default of 1900'
  );
});

test('Personal mapper', assert => {
  assert.plan(1);

  const result = from({
    personal__dob: '1980-01-01',
    personal__postcode: 'B301JJ',
    personal__address_line_1: 'address1',
    personal__address_line_2: 'address2',
    personal__address_line_3: 'address3',
    personal__address_time_lived_at: 'February 2017',
    personal__previous_addresses: [{ addressLine1: 'fake line' }],
    personal__three_years_of_addresses_provided: true,
    personal__telephone: '01214333570',
    personal__ni_number: 'JT593597B',
    personal__first_name: 'first',
    personal__last_name: 'last',
    personal__title: 'Mr',
    personal__gender: 'u',
    personal__completed_at: '2018-01-01T01:01:01Z',
    personal__email_address: 'test@support.com',
    personal__lg_employee: true,
  });

  assert.deepEqual(
    result,
    {
      addressLine1: 'address1',
      addressLine2: 'address2',
      addressLine3: 'address3',
      addressPostcode: 'B301JJ',
      addressTimeLivedAt: 'February 2017',
      addressPreviousArray: [{ addressLine1: 'fake line' }],
      threeYearsOfAddressesProvided: true,
      contactNumber: '01214333570',
      dob: '1980-01-01',
      firstName: 'first',
      lastName: 'last',
      niNumber: 'JT593597B',
      title: 'Mr',
      gender: 'u',
      completedAt: '2018-01-01T01:01:01Z',
      email: 'test@support.com',
      lgEmployee: true,
    },
    'maps back to state with completed_at'
  );
});
