import test from 'tape';
import { to, from } from './atr';

test('atr mapper', assert => {
  assert.plan(1);

  const result = to({});

  assert.deepEqual(
    result,
    {
      investment__a2risk_profile: null,
      a2risk__manual_override: null,
    },
    'handles nothing in the state, everything set to null so that keys not sent to server'
  );
});

test('atr mapper', assert => {
  assert.plan(1);

  const result = to({
    atr: {
      data: {
        a2risk__anxious_about_decisions: 'strongly_disagree',
        a2risk__comfortable_investing: 'strongly_disagree',
        a2risk__concerned_about_volatility: 'strongly_disagree',
        a2risk__describe_as_cautious: 'strongly_disagree',
        a2risk__investment_easy_to_understand: 'strongly_disagree',
        a2risk__little_investment_experience: 'strongly_disagree',
        a2risk__long_time_to_decide: 'strongly_disagree',
        a2risk__prefer_bank_deposits: 'strongly_disagree',
        a2risk__prefer_high_risk_investments: 'strongly_disagree',
        a2risk__prefer_safer_investments: 'strongly_disagree',
        a2risk__risk_equals_opportunity: 'strongly_disagree',
        a2risk__willing_to_risk: 'strongly_disagree',
      },
    },
  });

  assert.deepEqual(
    result,
    {
      a2risk__anxious_about_decisions: 'strongly_disagree',
      a2risk__comfortable_investing: 'strongly_disagree',
      a2risk__concerned_about_volatility: 'strongly_disagree',
      a2risk__describe_as_cautious: 'strongly_disagree',
      a2risk__investment_easy_to_understand: 'strongly_disagree',
      a2risk__little_investment_experience: 'strongly_disagree',
      a2risk__long_time_to_decide: 'strongly_disagree',
      a2risk__prefer_bank_deposits: 'strongly_disagree',
      a2risk__prefer_high_risk_investments: 'strongly_disagree',
      a2risk__prefer_safer_investments: 'strongly_disagree',
      a2risk__risk_equals_opportunity: 'strongly_disagree',
      a2risk__willing_to_risk: 'strongly_disagree',
      investment__a2risk_profile: null,
      a2risk__manual_override: null,
    },
    'maps to server'
  );
});

test('atr mapper', assert => {
  assert.plan(1);

  const result = from(
    {
      a2risk__anxious_about_decisions: 'strongly_agree',
      a2risk__comfortable_investing: 'strongly_disagree',
      a2risk__concerned_about_volatility: 'agree',
      a2risk__describe_as_cautious: 'strongly_agree',
      a2risk__investment_easy_to_understand: 'strongly_disagree',
      a2risk__little_investment_experience: 'strongly_disagree',
      a2risk__long_time_to_decide: 'strongly_disagree',
      a2risk__prefer_bank_deposits: 'strongly_disagree',
      a2risk__prefer_high_risk_investments: 'strongly_disagree',
      a2risk__prefer_safer_investments: 'strongly_disagree',
      a2risk__risk_equals_opportunity: 'strongly_disagree',
      a2risk__willing_to_risk: 'strongly_disagree',
      investment__a2risk_profile: undefined,
    },
    require('../../../../../content/investment/athena/investment.json')
      .atrQuestions
  );

  assert.deepEqual(
    result,
    {
      data: {
        a2risk__anxious_about_decisions: 'strongly_agree',
        a2risk__comfortable_investing: 'strongly_disagree',
        a2risk__concerned_about_volatility: 'agree',
        a2risk__describe_as_cautious: 'strongly_agree',
        a2risk__investment_easy_to_understand: 'strongly_disagree',
        a2risk__little_investment_experience: 'strongly_disagree',
        a2risk__long_time_to_decide: 'strongly_disagree',
        a2risk__prefer_bank_deposits: 'strongly_disagree',
        a2risk__prefer_high_risk_investments: 'strongly_disagree',
        a2risk__prefer_safer_investments: 'strongly_disagree',
        a2risk__risk_equals_opportunity: 'strongly_disagree',
        a2risk__willing_to_risk: 'strongly_disagree',
      },
      override: false,
      profile: {},
    },
    'maps atr back to state'
  );
});

test('atr mapper', assert => {
  assert.plan(1);

  const result = from(
    {
      a2risk__anxious_about_decisions: 'strongly_agree',
      a2risk__comfortable_investing: 'strongly_disagree',
      a2risk__concerned_about_volatility: 'agree',
      a2risk__describe_as_cautious: 'strongly_agree',
      a2risk__investment_easy_to_understand: 'strongly_disagree',
      a2risk__little_investment_experience: 'strongly_disagree',
      a2risk__long_time_to_decide: 'strongly_disagree',
      a2risk__prefer_bank_deposits: 'strongly_disagree',
      a2risk__prefer_high_risk_investments: 'strongly_disagree',
      a2risk__prefer_safer_investments: 'strongly_disagree',
      a2risk__risk_equals_opportunity: 'strongly_disagree',
      a2risk__willing_to_risk: 'strongly_disagree',
      investment__a2risk_profile: undefined,
      a2risk__manual_override: 'Confident',
    },
    require('../../../../../content/investment/athena/investment.json')
      .atrQuestions
  );

  assert.deepEqual(
    result,
    {
      data: {
        a2risk__anxious_about_decisions: 'strongly_agree',
        a2risk__comfortable_investing: 'strongly_disagree',
        a2risk__concerned_about_volatility: 'agree',
        a2risk__describe_as_cautious: 'strongly_agree',
        a2risk__investment_easy_to_understand: 'strongly_disagree',
        a2risk__little_investment_experience: 'strongly_disagree',
        a2risk__long_time_to_decide: 'strongly_disagree',
        a2risk__prefer_bank_deposits: 'strongly_disagree',
        a2risk__prefer_high_risk_investments: 'strongly_disagree',
        a2risk__prefer_safer_investments: 'strongly_disagree',
        a2risk__risk_equals_opportunity: 'strongly_disagree',
        a2risk__willing_to_risk: 'strongly_disagree',
      },
      override: { key: 'a2risk__manual_override', profile: 'confident' },
      profile: {},
    },
    'maps atr back to state with override'
  );
});
