import R from 'ramda';
import { upperCaseFirst } from './utils';

export const to = state => ({
  ...R.pathOr({}, ['atr', 'data'])(state),
  investment__a2risk_profile: R.pathOr(null, ['atr', 'profile'])(state),
  a2risk__manual_override: R.compose(
    upperCaseFirst,
    R.path(['atr', 'override', 'profile'])
  )(state),
});

export const from = (factFind, questions) => {
  const payloadIds = R.map(R.prop('answerKey'))(questions);
  return {
    data: payloadIds.reduce((acc, id) => {
      // eslint-disable-next-line
      acc[id] = R.path([id])(factFind);
      return acc;
    }, {}),
    profile: factFind.investment__a2risk_profile || {},
    override: factFind.a2risk__manual_override
      ? {
          key: 'a2risk__manual_override',
          profile: factFind.a2risk__manual_override.toLowerCase(),
        }
      : false,
  };
};
