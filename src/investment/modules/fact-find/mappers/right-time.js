import R from 'ramda';
import {
  findRightTimeAnswer,
  findISAHelpAnswer,
} from './utils';

export const to = R.applySpec({
  right_time__relationship_break: findRightTimeAnswer(
    'right_time__relationship_break',
    'rightTime'
  ),
  right_time__bereavement: findRightTimeAnswer(
    'right_time__bereavement',
    'rightTime'
  ),
  right_time__employment_change: findRightTimeAnswer(
    'right_time__employment_change',
    'rightTime'
  ),
  right_time__health_issues: findRightTimeAnswer(
    'right_time__health_issues',
    'rightTime'
  ),
  right_time__ISA_help: findRightTimeAnswer(
    'right_time__ISA_help',
    'rightTime'
  ),
  right_time__struggle_to_understand: findISAHelpAnswer(
    'right_time__struggle_to_understand',
    'rightTime'
  ),
  right_time__struggle_with_reading: findISAHelpAnswer(
    'right_time__struggle_with_reading',
    'rightTime'
  ),
  right_time__english_not_first_language: findISAHelpAnswer(
    'right_time__english_not_first_language',
    'rightTime'
  ),
  right_time__more_reassured: findISAHelpAnswer(
    'right_time__more_reassured',
    'rightTime'
  ),
  right_time__not_good_with_technology: findISAHelpAnswer(
    'right_time__not_good_with_technology',
    'rightTime'
  ),
});

export const from = (dataLoadedFromTheServer, content) => {
  const userInput = {};

  content.questions.forEach((question, i) => {
    userInput[i] = {
      answerKey: question.answerKey,
      answerId: question.answers.findIndex(answer => answer.value === dataLoadedFromTheServer[question.answerKey]),
      answerValue: dataLoadedFromTheServer[question.answerKey],
    };
  });

  const checkboxInput = {};

  content.questions
    .filter(question => !!question.multipleChoice)
    .forEach(question => question.multipleChoice.answers.forEach(answer => {
      checkboxInput[answer.value] = dataLoadedFromTheServer[answer.value] || false;
    }));

  return {
    userInput,
    checkboxInput,
  };
};
