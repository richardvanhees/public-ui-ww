import test from 'tape';
import { to, from } from './final-checks';

test('Final checks mapper', assert => {
  assert.plan(1);

  const result = to({
    finalChecks: {
      investment__money_to_invest_readily_available: 'foo',
      investment__min_amount_in_savings_available: 'bar',
      investment__borrowing_above_apr_level: 'baz',
      investment__maxing_your_employer_pension_scheme: 'foo',
      investment__reconsider_pay_into_pension: 'continue',
      investment__completed_at: 'bar',
      progress: 100,
      investment__hid_borrowing_above_30: 'no',
      investment__hid_borrowing_below_30: 'no',
      investment__hid_mortgage: 'no',
      investment__hid_borrowing_above_30_short_term: 'no',
      investment__hid_borrowing_above_30_taken_advice: 'no',
      investment__hid_borrowing_above_30_getting_worse: 'no',
      investment__hid_borrowing_above_30_struggling: 'no',
    },
  });

  assert.deepEqual(
    result,
    {
      investment__money_to_invest_readily_available: 'foo',
      investment__min_amount_in_savings_available: 'bar',
      investment__borrowing_above_apr_level: 'baz',
      investment__maxing_your_employer_pension_scheme: 'foo',
      investment__reconsider_pay_into_pension: 'continue',
      investment__completed_at: 'bar',
      investment__hid_borrowing_above_30: 'no',
      investment__hid_borrowing_below_30: 'no',
      investment__hid_mortgage: 'no',
      investment__hid_borrowing_above_30_short_term: 'no',
      investment__hid_borrowing_above_30_taken_advice: 'no',
      investment__hid_borrowing_above_30_getting_worse: 'no',
      investment__hid_borrowing_above_30_struggling: 'no',
    },
    'maps to server'
  );
});

test('Final checks mapper', assert => {
  assert.plan(1);

  const result = from({
    investment__money_to_invest_readily_available: 'foo',
    investment__min_amount_in_savings_available: 'bar',
    investment__borrowing_above_apr_level: 'baz',
    investment__maxing_your_employer_pension_scheme: 'foo',
    investment__reconsider_pay_into_pension: 'continue',
    investment__completed_at: 'bar',
    investment__hid_borrowing_above_30: 'no',
    investment__hid_borrowing_below_30: 'yes',
    investment__hid_mortgage: 'no',
    investment__hid_borrowing_above_30_short_term: 'no',
    investment__hid_borrowing_above_30_taken_advice: 'no',
    investment__hid_borrowing_above_30_getting_worse: 'no',
    investment__hid_borrowing_above_30_struggling: 'no',
  });

  assert.deepEqual(
    result,
    {
      investment__money_to_invest_readily_available: 'foo',
      investment__min_amount_in_savings_available: 'bar',
      investment__borrowing_above_apr_level: 'baz',
      investment__maxing_your_employer_pension_scheme: 'foo',
      investment__reconsider_pay_into_pension: 'continue',
      investment__completed_at: 'bar',
      investment__hid_borrowing_above_30: 'no',
      investment__hid_borrowing_below_30: 'yes',
      investment__hid_mortgage: 'no',
      investment__hid_borrowing_above_30_short_term: 'no',
      investment__hid_borrowing_above_30_taken_advice: 'no',
      investment__hid_borrowing_above_30_getting_worse: 'no',
      investment__hid_borrowing_above_30_struggling: 'no',
    },
    'maps to ui'
  );
});
