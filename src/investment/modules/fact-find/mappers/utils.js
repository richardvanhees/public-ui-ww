import R from 'ramda';

export const findAnswer = (answerKey, sectionKey) => factFind =>
  R.compose(
    R.pathOr(null, [0, 'answerValue']),
    R.filter(key => key.answerKey === answerKey),
    R.values(),
    R.path(['data'])
  )(factFind[sectionKey]);

export const findRightTimeAnswer = (answerKey, sectionKey) => factFind =>
  R.compose(
    R.pathOr(null, [0, 'answerValue']),
    R.filter(key => key.answerKey === answerKey),
    R.values(),
    R.path(['userInput'])
  )(factFind[sectionKey]);

export const findISAHelpAnswer = (answerKey, sectionKey) => factFind =>
  R.compose(
    R.pathOr(false, [answerKey]),
    R.path(['checkboxInput'])
  )(factFind[sectionKey]);

export const mapKeyValuesFromServerToUIComponentsDataStructure = (
  dataLoadedFromTheServer,
  contentData
) => {
  const stateData = [];
  contentData.forEach(questionDefinition => {
    const answer = questionDefinition.answers.find(
      a =>
        a.value ===
        R.path([questionDefinition.answerKey])(dataLoadedFromTheServer)
    );

    const answerData = answer
      ? { answerId: answer.id, answerValue: answer.value }
      : {};

    stateData.push({ answerKey: questionDefinition.answerKey, ...answerData });
  });
  return stateData;
};

export const upperCaseFirst = (string, defaultReturnValue = null) =>
  string
    ? string.charAt(0).toUpperCase() + string.slice(1)
    : defaultReturnValue;
