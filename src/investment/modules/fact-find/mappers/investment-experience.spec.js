import test from 'tape';
import { to, from } from './investment-experience';

test('investment-experience/mapper', assert => {
  assert.plan(1);

  const result = to({});

  assert.deepEqual(
    result,
    {
      investment_experience__manual_override: null,
      investment_experience_profile: null,
    },
    'handles nothing in the state, everything set to null so that keys not sent to server'
  );
});

test('investment-experience/mapper', assert => {
  assert.plan(1);

  const result = to({
    experience: {
      override: {
        key: 'investment_experience__manual_override',
        profile: 'confident',
      },
      data: {
        investment_experience__bank_or_savings: 'yes',
        investment_experience__national_savings: 'yes',
        investment_experience__cash_isas: 'yes',
        investment_experience__endowment: 'yes',
        investment_experience__sharesisa: 'yes',
        investment_experience__trusts: 'yes',
        investment_experience__portfolio: 'yes',
        investment_experience__general_experience: 'yes',
      },
      profile: {},
    },
  });

  assert.deepEqual(
    result,
    {
      investment_experience__bank_or_savings: 'yes',
      investment_experience__cash_isas: 'yes',
      investment_experience__endowment: 'yes',
      investment_experience__general_experience: 'yes',
      investment_experience__national_savings: 'yes',
      investment_experience__portfolio: 'yes',
      investment_experience__sharesisa: 'yes',
      investment_experience__trusts: 'yes',
      investment_experience_profile: {},
      investment_experience__manual_override: 'Confident',
    },
    'maps IE into payload'
  );
});

test('investment-experience/mapper', assert => {
  assert.plan(1);

  const result = from(
    {
      investment_experience__general_experience: 'yes',
      investment_experience__bank_or_savings: 'yes',
      investment_experience__cash_isas: 'yes',
      investment_experience__endowment: 'yes',
      investment_experience__national_savings: 'yes',
      investment_experience__portfolio: 'yes',
      investment_experience__sharesisa: 'yes',
      investment_experience__trusts: 'yes',
      investment_experience_profile: {},
      investment_experience__manual_override: 'Confident',
    },
    require('../../../../../content/investment/athena/investment.json')
      .experienceQuestions
  );

  assert.deepEqual(
    result,
    {
      data: {
        investment_experience__bank_or_savings: 'yes',
        investment_experience__cash_isas: 'yes',
        investment_experience__endowment: 'yes',
        investment_experience__general_experience: 'yes',
        investment_experience__national_savings: 'yes',
        investment_experience__portfolio: 'yes',
        investment_experience__sharesisa: 'yes',
        investment_experience__trusts: 'yes',
      },
      override: {
        key: 'investment_experience__manual_override',
        profile: 'confident',
      },
      profile: {},
    },
    'maps IE back into UI state'
  );
});
