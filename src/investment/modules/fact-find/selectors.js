import R from 'ramda';

export const investmentGoalSelector = R.path(['investmentGoal']);
export const atrSelector = R.path(['atr', 'data']);
export const experienceSelector = R.path(['experience', 'data']);
export const resilienceSelector = R.path(['resilience', 'data']);
export const flexibilitySelector = R.path(['flexibility', 'data']);
export const personalSelector = R.path(['aboutYou']);
export const finalChecksSelector = R.path(['finalChecks']);
export const isaSelector = R.path(['isa']);
export const atrOverride = R.path(['atr', 'override']);
export const experienceOverride = R.path(['experience', 'override']);
export const resilienceOverride = R.path(['resilience', 'override']);
export const flexibilityOverride = R.path(['flexibility', 'override']);

export const lookupValueInStore = (key, factFind) => factFind[key];
export const lookupFactFindData = factFind => factFind.data;
export const isLoggedIn = factFind => factFind.isLoggedIn;

export const lookupGeneratedAtRiskProfileTimestamps = R.applySpec({
  atr: R.path(['atr', 'profile', 'generated_at']),
  experience: R.path(['experience', 'profile', 'generated_at']),
  resilience: R.path(['resilience', 'profile', 'generated_at']),
  flexibility: R.path(['flexibility', 'profile', 'generated_at']),
});
