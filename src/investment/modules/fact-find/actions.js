import factFindTypes from './types';

export const getFactFindRequest = (
  redirectPath,
  requestIsBeingMadeAfterALogin
) => ({
  type: factFindTypes.GET_FACT_FIND_REQUEST,
  redirectPath,
  requestIsBeingMadeAfterALogin,
});

export const getFactFindSuccess = data => ({
  type: factFindTypes.GET_FACT_FIND_SUCCESS,
  data,
});

export const getFactFindFailure = error => ({
  type: factFindTypes.GET_FACT_FIND_FAILURE,
  error,
});

export const saveFactFindSuccess = isLoading => ({
  type: factFindTypes.SAVE_FACT_FIND_SUCCESS,
  isLoading,
});

export const setReferralFlagInState = () => ({
  type: factFindTypes.SET_REFERRAL_FLAG,
});

export const saveFactFindFailure = error => ({
  type: factFindTypes.SAVE_FACT_FIND_FAILURE,
  error,
});

export const saveFactFindRequest = ({
  redirectPath,
  recordSolution,
  doInBackground,
  withTimestamp,
  createFactFind,
  setCompletedAt,
  callback,
}) => ({
  type: factFindTypes.SAVE_FACT_FIND_REQUEST,
  redirectPath,
  recordSolution,
  doInBackground,
  withTimestamp,
  createFactFind,
  setCompletedAt,
  callback,
});

export const factFindAndSolutionFinish = () => ({
  type: factFindTypes.FACT_FIND_AND_SOLUTION_FINISH,
});

export const MAP_FROM_SERVER = 'globals/MAP_FROM_SERVER';
export const RESET_STATE = 'globals/RESET_STATE';
export const RESET_RISK_SECTIONS = 'globals/RESET_RISK_SECTIONS';

export const mapFromServer = data => ({
  type: MAP_FROM_SERVER,
  data,
});

export const setIsLoggedIn = () => ({
  type: factFindTypes.SET_IS_LOGGED_IN,
});

export const resetState = () => ({
  type: RESET_STATE,
});
