import factFindTypes from './types';

const getFactFindRequest = state => ({
  ...state,
  isLoading: true,
});

const getFactFindSuccess = (state, { data }) => ({
  ...state,
  data,
  isLoading: false,
});

const getFactFindFailure = (state, { error }) => ({
  ...state,
  error,
  isLoading: false,
});

const saveFactFindRequest = state => ({
  ...state,
  isLoading: true,
});

const saveFactFindSuccess = (state, { isLoading }) => ({
  ...state,
  isLoading,
});

const saveFactFindFailure = (state, { error }) => ({
  ...state,
  isLoading: false,
  error,
});

const factFindAndSolutionFinish = state => ({
  ...state,
  isLoading: false,
});

const setReferralFlag = state => ({
  ...state,
  referred: true,
});

const initialState = {
  isLoading: false,
  referred: false,
  isLoggedIn: false,
};

const initialLoad = (state, { isLoggedIn }) => ({
  ...state,
  isLoggedIn,
});
const setIsLoggedIn = state => ({
  ...state,
  isLoggedIn: true,
});

const reducer = {
  [factFindTypes.GET_FACT_FIND_REQUEST]: getFactFindRequest,
  [factFindTypes.GET_FACT_FIND_SUCCESS]: getFactFindSuccess,
  [factFindTypes.GET_FACT_FIND_FAILURE]: getFactFindFailure,
  [factFindTypes.SAVE_FACT_FIND_REQUEST]: saveFactFindRequest,
  [factFindTypes.SAVE_FACT_FIND_SUCCESS]: saveFactFindSuccess,
  [factFindTypes.SAVE_FACT_FIND_FAILURE]: saveFactFindFailure,
  [factFindTypes.FACT_FIND_AND_SOLUTION_FINISH]: factFindAndSolutionFinish,
  [factFindTypes.SET_REFERRAL_FLAG]: setReferralFlag,
  [factFindTypes.SET_IS_LOGGED_IN]: setIsLoggedIn,
  [factFindTypes.INITIAL_LOAD]: initialLoad,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
