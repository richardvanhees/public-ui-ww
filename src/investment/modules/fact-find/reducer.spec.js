import test from 'tape';
import reducer from './reducer.js';
import factFindTypes from './types';
import { MAP_FROM_SERVER } from './actions';

test('FactFindReducer', t => {
  let result;

  result = reducer({}, { type: factFindTypes.GET_FACT_FIND_REQUEST });
  t.deepEqual(result, { isLoading: true }, 'GET_FACT_FIND_REQUEST');

  result = reducer({}, { type: factFindTypes.GET_FACT_FIND_SUCCESS, data: {} });
  t.deepEqual(result, { isLoading: false, data: {} }, 'GET_FACT_FIND_SUCCESS');

  result = reducer(
    {},
    { type: factFindTypes.GET_FACT_FIND_FAILURE, error: 'testError' }
  );
  t.deepEqual(
    result,
    { isLoading: false, error: 'testError' },
    'GET_FACT_FIND_FAILURE'
  );

  result = reducer({}, { type: factFindTypes.SAVE_FACT_FIND_REQUEST });
  t.deepEqual(result, { isLoading: true }, 'SAVE_FACT_FIND_REQUEST');

  result = reducer(
    {},
    { type: factFindTypes.SAVE_FACT_FIND_SUCCESS, isLoading: true }
  );
  t.deepEqual(result, { isLoading: true }, 'SAVE_FACT_FIND_SUCCESS');

  result = reducer(
    {},
    { type: factFindTypes.SAVE_FACT_FIND_FAILURE, error: 'testError' }
  );
  t.deepEqual(
    result,
    { isLoading: false, error: 'testError' },
    'SAVE_FACT_FIND_FAILURE'
  );

  t.end();
});

test('FactFindReducer', assert => {
  assert.plan(1);

  const result = reducer({}, { type: factFindTypes.SET_REFERRAL_FLAG });
  assert.deepEqual(result, { referred: true }, 'referred set to true');
});

test('FactFindReducer', assert => {
  assert.plan(1);

  const result = reducer(
    {},
    { type: factFindTypes.INITIAL_LOAD, isLoggedIn: true }
  );
  assert.deepEqual(
    result,
    { isLoggedIn: true },
    'logged in true set when INITIAL_LOAD action fired'
  );
});
