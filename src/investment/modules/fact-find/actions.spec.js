import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('factFindActions', t => {
  let result;

  result = actions.getFactFindRequest('redirectPath', true);
  t.deepEqual(
    result,
    {
      type: types.GET_FACT_FIND_REQUEST,
      redirectPath: 'redirectPath',
      requestIsBeingMadeAfterALogin: true,
    },
    'getFactFindRequest'
  );

  result = actions.getFactFindSuccess('testData');
  t.deepEqual(
    result,
    {
      type: types.GET_FACT_FIND_SUCCESS,
      data: 'testData',
    },
    'getFactFindSuccess'
  );

  result = actions.getFactFindFailure('testError');
  t.deepEqual(
    result,
    {
      type: types.GET_FACT_FIND_FAILURE,
      error: 'testError',
    },
    'getFactFindFailure'
  );

  result = actions.saveFactFindSuccess(true);
  t.deepEqual(
    result,
    {
      type: types.SAVE_FACT_FIND_SUCCESS,
      isLoading: true,
    },
    'saveFactFindSuccess'
  );

  result = actions.saveFactFindFailure('testError');
  t.deepEqual(
    result,
    {
      type: types.SAVE_FACT_FIND_FAILURE,
      error: 'testError',
    },
    'saveFactFindFailure'
  );

  result = actions.saveFactFindRequest({
    recordSolution: true,
    redirectPath: 'somewhere',
    doInBackground: true,
  });
  t.deepEqual(
    result,
    {
      type: types.SAVE_FACT_FIND_REQUEST,
      recordSolution: true,
      redirectPath: 'somewhere',
      doInBackground: true,
      withTimestamp: undefined,
      createFactFind: undefined,
      setCompletedAt: undefined,
      callback: undefined,
    },
    'saveFactFindRequest'
  );

  result = actions.saveFactFindRequest({
    withTimestamp: 'some_key',
  });
  t.deepEqual(
    result,
    {
      type: types.SAVE_FACT_FIND_REQUEST,
      withTimestamp: 'some_key',
      recordSolution: undefined,
      redirectPath: undefined,
      doInBackground: undefined,
      createFactFind: undefined,
      setCompletedAt: undefined,
      callback: undefined,
    },
    'saveFactFindRequest - withTimestamp'
  );

  t.end();
});

test('factFindActions', assert => {
  assert.plan(1);

  assert.deepEqual(
    actions.setReferralFlagInState(),
    {
      type: types.SET_REFERRAL_FLAG,
    },
    'setReferralFlagInState'
  );
});

test('factFindActions', assert => {
  assert.plan(1);

  assert.deepEqual(
    actions.resetState(),
    {
      type: 'globals/RESET_STATE',
    },
    'resetState'
  );
});
