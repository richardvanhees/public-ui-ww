import test from 'tape';
import {
  SET_INVESTMENT_GOAL,
  SET_INVESTMENT_GOAL_LENGTH,
  SET_INVESTMENT_GOAL_AMOUNT,
  SET_INVESTMENT_GOAL_COMPLETION,
  setInvestmentGoal,
  setInvestmentGoalAmount,
  setInvestmentGoalLength,
  setInvestmentGoalCompletion,
} from './actions';

test('InvestmentGoalActions', t => {
  result;

  let result = setInvestmentGoalLength({ length: 'testData', lengthValue: 3 });
  t.deepEqual(
    result,
    {
      length: 'testData',
      lengthValue: 3,
      type: 'investment-goal/SET_INVESTMENT_GOAL_LENGTH',
    },
    'SET_INVESTMENT_GOAL_LENGTH'
  );

  let result2 = setInvestmentGoalAmount({ amount: 1 });
  t.deepEqual(
    result2,
    {
      amount: { amount: 1 },
      type: 'investment-goal/SET_INVESTMENT_GOAL_AMOUNT',
    },
    'SET_INVESTMENT_GOAL_AMOUNT'
  );

  let result3 = setInvestmentGoal({
    goal: 'testData',
    icon: 'icon',
    value: 'whoooooda',
  });
  t.deepEqual(
    result3,
    {
      goal: 'whoooooda',
      icon: 'icon',
      type: 'investment-goal/SET_INVESTMENT_GOAL',
    },
    'SET_INVESTMENT_GOAL'
  );

  let result4 = setInvestmentGoalCompletion('2018-06-07');
  t.deepEqual(
    result4,
    {
      date: '2018-06-07',
      type: 'investment-goal/SET_INVESTMENT_GOAL_COMPLETION',
    },
    'SET_INVESTMENT_GOAL_COMPLETION'
  );

  t.end();
});
