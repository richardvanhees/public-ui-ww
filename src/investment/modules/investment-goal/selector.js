import { createStructuredSelector } from 'reselect';
import R from 'ramda';

export const preRegistrationDataIsComplete = (investmentGoal, rightTime) =>
  (investmentGoal.amount > 0 || investmentGoal.monthly > 0) &&
  investmentGoal.goal &&
  R.keys(rightTime.userInput).length === 5;

const length = state => R.path(['investmentGoal', 'length'], state);
const amount = state => R.path(['investmentGoal', 'amount'], state);
const monthly = state => R.path(['investmentGoal', 'monthly'], state);
const date = state => R.path(['investmentGoal', 'date'], state);

export const onlyMonthlyInvestment = state => (
  amount(state) === 0 && monthly(state) > 0
);

export default createStructuredSelector({
  length,
  amount,
  monthly,
  date,
});
