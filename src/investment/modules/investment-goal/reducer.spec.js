import test from 'tape';
import reducer from './reducer.js';
import {
  SET_INVESTMENT_GOAL,
  SET_INVESTMENT_GOAL_COMPLETION,
  SET_INVESTMENT_GOAL_LENGTH,
} from './actions';
import { MAP_FROM_SERVER } from '../fact-find/actions';

test('InvestmentGoalsReducer', t => {
  let result;

  result = reducer(
    {},
    { type: SET_INVESTMENT_GOAL, goal: 'testData', icon: 'icon' }
  );
  t.deepEqual(result, { goal: 'testData', icon: 'icon' }, SET_INVESTMENT_GOAL);

  t.end();
});

test('InvestmentGoalsReducer', t => {
  let result;

  result = reducer(
    {},
    { type: SET_INVESTMENT_GOAL_LENGTH, length: 'foo', lengthValue: 5 }
  );
  t.deepEqual(
    result,
    { length: 'foo', lengthValue: 5 },
    SET_INVESTMENT_GOAL_LENGTH
  );

  t.end();
});

test('InvestmentGoalsReducer', assert => {
  assert.plan(1);
  const res = reducer(
    {},
    { type: MAP_FROM_SERVER, data: { investmentGoal: { foo: 'baa' } } }
  );
  assert.deepEqual(res, { foo: 'baa' }, 'maps from server');
});

test('InvestmentGoalsReducer', assert => {
  assert.plan(1);
  const res = reducer(
    {},
    { type: SET_INVESTMENT_GOAL_COMPLETION, date: '2018-06-07' }
  );
  assert.deepEqual(res, { completedAt: '2018-06-07' }, 'Completion date set');
});
