export const SET_INVESTMENT_GOAL = 'investment-goal/SET_INVESTMENT_GOAL';
export const SET_INVESTMENT_GOAL_LENGTH =
  'investment-goal/SET_INVESTMENT_GOAL_LENGTH';
export const SET_INVESTMENT_GOAL_AMOUNT =
  'investment-goal/SET_INVESTMENT_GOAL_AMOUNT';
export const SET_INVESTMENT_TARGET_AMOUNT =
  'investment-goal/SET_INVESTMENT_TARGET_AMOUNT';
export const SET_INVESTMENT_MONTHLY_AMOUNT =
  'investment-goal/SET_INVESTMENT_MONTHLY_AMOUNT';
export const SET_INVESTMENT_GOAL_COMPLETION =
  'investment-goal/SET_INVESTMENT_GOAL_COMPLETION';

export const setInvestmentGoal = data => ({
  type: SET_INVESTMENT_GOAL,
  goal: data.value,
  icon: data.icon,
});

export const setInvestmentGoalAmount = amount => ({
  type: SET_INVESTMENT_GOAL_AMOUNT,
  amount,
});

export const setInvestmentMonthlyAmount = monthly => ({
  type: SET_INVESTMENT_MONTHLY_AMOUNT,
  monthly,
});

export const setInvestmentTargetAmount = target => ({
  type: SET_INVESTMENT_TARGET_AMOUNT,
  target,
});

export const setInvestmentGoalLength = data => ({
  type: SET_INVESTMENT_GOAL_LENGTH,
  length: data.length,
  lengthValue: data.lengthValue,
});

export const setInvestmentGoalCompletion = date => ({
  type: SET_INVESTMENT_GOAL_COMPLETION,
  date,
});
