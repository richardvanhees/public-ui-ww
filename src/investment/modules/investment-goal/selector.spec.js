import test from 'tape';
import selector, { preRegistrationDataIsComplete } from './selector.js';

test('investmentGoalSelector', t => {
  const result = selector({
    investmentGoal: {
      length: '',
      amount: 0,
      monthly: 69,
      date: '2018-06-07',
    },
  });

  t.deepEqual(result, {
    length: '',
    amount: 0,
    monthly: 69,
    date: '2018-06-07',
  });

  t.end();
});

test('investmentGoalSelector', t => {
  const result = preRegistrationDataIsComplete(
    {
      length: '',
      amount: 0,
      monthly: 69,
      date: '2018-06-07',
    },
    {
      userInput: {},
    }
  );

  t.false(result, 'right time not answered');

  t.end();
});

test('investmentGoalSelector', t => {
  const result = preRegistrationDataIsComplete(
    {
      length: '',
      amount: 0,
      monthly: 69,
      date: '2018-06-07',
      goal: 'a goal',
    },
    {
      userInput: {
        0: { foo: 'ba' },
        1: { foo: 'ba' },
        2: { foo: 'ba' },
        3: { foo: 'ba' },
        4: { foo: 'ba' },
      },
    }
  );

  t.true(result, 'everything set');

  t.end();
});

test('investmentGoalSelector', t => {
  const result = preRegistrationDataIsComplete(
    {
      length: '',
      amount: 0,
      monthly: 0,
      date: '2018-06-07',
      goal: 'a goal',
    },
    {
      userInput: {
        0: { foo: 'ba' },
        1: { foo: 'ba' },
        2: { foo: 'ba' },
        3: { foo: 'ba' },
        4: { foo: 'ba' },
      },
    }
  );

  t.false(result, 'monthly and amount 0');

  t.end();
});
