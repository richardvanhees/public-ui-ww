import {
  SET_INVESTMENT_GOAL,
  SET_INVESTMENT_GOAL_AMOUNT,
  SET_INVESTMENT_TARGET_AMOUNT,
  SET_INVESTMENT_GOAL_LENGTH,
  SET_INVESTMENT_MONTHLY_AMOUNT,
  SET_INVESTMENT_GOAL_COMPLETION,
} from './actions';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

const initialState = {
  length: '10 years',
  lengthValue: 10,
  goal: '',
  amount: 0,
  monthly: 0,
  icon: '',
  target: 0,
  completedAt: null,
};

const mapFromServer = (state, action) => ({
  ...state,
  ...action.data.investmentGoal,
});

const reducer = {
  [SET_INVESTMENT_GOAL]: (state, action) => ({
    ...state,
    goal: action.goal,
    icon: action.icon,
  }),
  [SET_INVESTMENT_GOAL_LENGTH]: (state, action) => ({
    ...state,
    length: action.length,
    lengthValue: action.lengthValue,
  }),
  [SET_INVESTMENT_GOAL_AMOUNT]: (state, { amount }) => ({
    ...state,
    amount,
  }),
  [SET_INVESTMENT_MONTHLY_AMOUNT]: (state, { monthly }) => ({
    ...state,
    monthly,
  }),
  [SET_INVESTMENT_TARGET_AMOUNT]: (state, { target }) => ({
    ...state,
    target,
  }),
  [SET_INVESTMENT_GOAL_COMPLETION]: (state, { date }) => ({
    ...state,
    completedAt: date,
  }),
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
