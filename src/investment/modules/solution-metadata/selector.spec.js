import test from 'tape';
import { individualSolution,  downloaded } from './selector';

test('solutionMetadataSelector', assert => {
  assert.plan(1);
  const result = individualSolution(
    {
      data: [
        {
          solution_id: 'foo',
          accepted_advice: true,
          read_documents: true
        }
      ]
    },
    'foo'
  );

  assert.deepEqual(result, {
    solution_id: 'foo',
    accepted_advice: true,
    read_documents: true
  },
  'should find matching solution');
  assert.end();
});


test('solutionMetadataSelector', assert => {
  assert.plan(1);
  const result = individualSolution(
    {
      data: [
        {
          solution_id: 'foo',
          accepted_advice: true,
          read_documents: true
        }
      ]
    },
    'bar'
  );

  assert.deepEqual(result, undefined,
  'should return default of undefined');
  assert.end();
});

test('solutionMetadataSelector', assert => {
  assert.plan(1);
  const result = downloaded(
    {
      data: [
        {
          solution_id: 'foo',
          accepted_advice: true,
          read_documents: true,
          downloaded: true
        }
      ]
    },
    'foo')
  assert.true(result, 'correctly returns boolean');
  assert.end();
});
