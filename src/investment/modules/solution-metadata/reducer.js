import acceptanceTypes from './types';
import solutionTypes from '../solution/types';
import {
  MAP_FROM_SERVER,
} from '../../modules/fact-find/actions';
import R from 'ramda';

const initialState = {
  data: [],
};

const defaultSolution = {
  solution_id: null,
  accepted_advice: false,
  read_documents: false,
  downloaded: false,
};

const solutionMetadataUpdated = (state, { solutionId, readDocuments, acceptedAdvice, downloaded }) => {
  const data = [...state.data || []];
  const previousSolution = R.find(R.propEq('solution_id', solutionId), data);

  if (R.isNil(previousSolution)) {
    data.push({
      solution_id: solutionId,
      accepted_advice: R.isNil(acceptedAdvice) ? defaultSolution.accepted_advice : acceptedAdvice,
      read_documents: R.isNil(readDocuments) ? defaultSolution.read_documents : readDocuments,
      downloaded: R.isNil(downloaded) ? defaultSolution.downloaded : downloaded,
    });
  } else {
    data[data.indexOf(previousSolution)] = {
      solution_id: solutionId,
      accepted_advice: R.isNil(acceptedAdvice) ? previousSolution.accepted_advice : acceptedAdvice,
      read_documents: R.isNil(readDocuments) ? previousSolution.read_documents : readDocuments,
      downloaded: R.isNil(downloaded) ? previousSolution.downloaded : downloaded,
    };
  }

  return {
    ...state,
    data,
  };
};

const mapFromServer = (state, { data }) => ({
  ...state,
  ...data.solutionMetadata,
});

const solutionMetadataDownloaded = (state, { solutionId }) =>
  solutionMetadataUpdated(state, { solutionId, downloaded: true });

const reducer = {
  [acceptanceTypes.SOLUTION_METADATA_UPDATED]: solutionMetadataUpdated,
  [solutionTypes.DOWNLOAD_REPORT_SUCCESS]: solutionMetadataDownloaded,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
