import test from 'tape';
import reducer from './reducer.js';
import types from './types';
import solutionTypes from '../solution/types';

import {
  MAP_FROM_SERVER,
} from '../../modules/fact-find/actions';

const initialState = {
  data: []
};

test('solutionMetadataReducer', assert => {
  assert.plan(1);

  const res = reducer(
    initialState,
    {
      type: types.SOLUTION_METADATA_UPDATED,
      solutionId: '43o2uhfsdhfas998f',
      readDocuments: true,
      acceptedAdvice: true,
      downloaded: true
    }
  );

  assert.deepEqual(
    res,
    {
      data: [
        {
          solution_id: '43o2uhfsdhfas998f',
          read_documents: true,
          accepted_advice: true,
          downloaded: true
        }
      ]
    },
    'SOLUTION_METADATA_UPDATED from initialState'
  );
  assert.end();
});

test('solutionMetadataReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {
      data: [
        {
          solution_id: '43o2uhfsdhfas998f',
          read_documents: false,
          accepted_advice: false,
          downloaded: false
        }
      ]
    },
    {
      type: types.SOLUTION_METADATA_UPDATED,
      solutionId: '43o2uhfsdhfas998f',
      readDocuments: true,
      acceptedAdvice: true,
      downloaded: false
    }
  );

  assert.deepEqual(
    res,
    {
      data: [
        {
          solution_id: '43o2uhfsdhfas998f',
          read_documents: true,
          accepted_advice: true,
          downloaded: false
        }
      ]
    },
    'SOLUTION_METADATA_UPDATED from prior'
  );
  assert.end();
});

test('solutionMetadataReducer', assert => {
  assert.plan(1);

  const acceptedSolutions = [
        {
          solution_id: '43o2uhfsdhfas998f',
          read_documents: true,
          accepted_advice: true,
          downloaded: false
        }
    ];

  const actual = reducer(initialState, {
    type: MAP_FROM_SERVER,
    data: {
      solutionMetadata: {
        data: acceptedSolutions
      }
    },
  });

  const expected = {
    data: acceptedSolutions
  };

  assert.deepEqual(actual, expected, 'maps from server');
  assert.end();
});


test('solutionMetadataReducer', assert => {
  assert.plan(1);
  const solutionId = '43o2uhfsdhfas998f';

  const actual = reducer({
    data: [
      {
        solution_id: solutionId,
        read_documents: true,
        accepted_advice: true,
        downloaded: false
      }
    ]
  }, {
    type: solutionTypes.DOWNLOAD_REPORT_SUCCESS,
    solutionId
  });

  const expected = {
    data: [
      {
        solution_id: solutionId,
        read_documents: true,
        accepted_advice: true,
        downloaded: true
      }
    ]
  };

  assert.deepEqual(actual, expected, 'updates metadata when downloaded');
  assert.end();
});
