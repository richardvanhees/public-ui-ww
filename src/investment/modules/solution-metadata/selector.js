import R from 'ramda';

export const individualSolution = (state, solutionId) =>
  R.find(
    R.propEq('solution_id', solutionId),
    R.pathOr([], ['data'], state)
  );

export const downloaded = (state, solutionId) =>
  R.path(['downloaded'], individualSolution(state, solutionId));
