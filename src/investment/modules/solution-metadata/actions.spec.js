import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('SolutionMetadataActions', assert => {
  assert.plan(1);
  const solutionId = '123';
  const readDocuments = true;
  const acceptedAdvice = true;
  const downloaded = false;
  const result = actions.solutionMetadataUpdated(solutionId, { readDocuments, acceptedAdvice, downloaded });
  assert.deepEqual(
    result,
    {
      type: types.SOLUTION_METADATA_UPDATED,
      solutionId,
      readDocuments,
      acceptedAdvice,
      downloaded
    },
    'solutionMetadataUpdated'
  );
});
