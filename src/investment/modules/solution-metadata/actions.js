import solutionMetadataTypes from './types';

export const solutionMetadataUpdated = (solutionId, { readDocuments, acceptedAdvice, downloaded }) => ({
  type: solutionMetadataTypes.SOLUTION_METADATA_UPDATED,
  solutionId,
  readDocuments,
  acceptedAdvice,
  downloaded,
});
