import test from 'tape';
import * as actions from './actions.js';
import types from './types';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

test('RightTimeActions', assert => {
  let result;
  assert.plan(5);

  const fakeAnswerData = { answerId: 0, answerKey: "pretty__decent", answerValue: "i_need_to_ask_my_mom" };
  result = actions.setAnswer(fakeAnswerData);
  assert.deepEqual(result, { type: types.SET_ANSWER, answerData: { answerId: 0, answerKey: "pretty__decent", answerValue: "i_need_to_ask_my_mom" } }, 'setAnswer');

  result = actions.setCheckbox('fake__key', true);
  assert.deepEqual(result, { type: types.SET_CHECKBOX, key: 'fake__key', value: true }, 'setCheckbox');

  result = actions.setAllData('testData');
  assert.deepEqual(result, { type: types.SET_ALL_DATA, data: 'testData' }, 'setAllData');

  result = actions.mapFromServer('testData');
  assert.deepEqual(result, { type: MAP_FROM_SERVER, data: 'testData' }, 'mapFromServer');

  result = actions.setCompletedAt('2018-11-02');
  assert.deepEqual(result, { type: types.SET_RIGHT_TIME_COMPLETION, date: '2018-11-02' }, 'SET_RIGHT_TIME_COMPLETION');

});
