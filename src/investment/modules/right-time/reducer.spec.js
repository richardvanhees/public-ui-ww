import test from 'tape';
import reducer from './reducer.js';
import rightTimeTypes from './types';
import { MAP_FROM_SERVER } from '../fact-find/actions';

test('right time reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: rightTimeTypes.SET_ANSWER, answerData: { 0: 'fakeData' } });
  assert.deepEqual(res, { userInput: { 0: 'fakeData' } }, 'SET_ANSWER');
});

test('right time reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: rightTimeTypes.SET_CHECKBOX, key: 'fakeKey', value: 'fakeValue' });
  assert.deepEqual(res, { checkboxInput: { fakeKey: 'fakeValue' } }, 'SET_CHECKBOX');
});

test('right time reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: rightTimeTypes.SET_ALL_DATA, data: 'fakeData' });
  assert.deepEqual(res, { checkboxInput: {}, userInput: {} }, 'SET_ALL_DATA');
});

test('right time reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: MAP_FROM_SERVER, data: { rightTime: 'fakeData' } });
  assert.deepEqual(res, 'fakeData', 'MAP_FROM_SERVER');
});