import rightTimeTypes from './types';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

export const setAnswer = answerData => ({
  type: rightTimeTypes.SET_ANSWER,
  answerData,
});

export const setCheckbox = (key, value) => ({
  type: rightTimeTypes.SET_CHECKBOX,
  key,
  value,
});

export const setAllData = data => ({
  type: rightTimeTypes.SET_ALL_DATA,
  data,
});

export const mapFromServer = data => ({
  type: MAP_FROM_SERVER,
  data,
});

export const setCompletedAt = date => ({
  type: rightTimeTypes.SET_RIGHT_TIME_COMPLETION,
  date,
});
