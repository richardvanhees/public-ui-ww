import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const userInput = state => R.path(['rightTime', 'userInput'], state);
const checkboxInput = state => R.path(['rightTime', 'checkboxInput'], state);
const isVulnerable = state => (Object.keys(R.path(['rightTime', 'userInput'], state)).length && Object.values(R.path(['rightTime', 'userInput'], state)).map(answer => answer.answerValue).includes('yes')) || false;

export default createStructuredSelector({
  userInput,
  checkboxInput,
  isVulnerable,
});
