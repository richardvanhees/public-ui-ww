export default {
  SET_ANSWER: 'right-time/SET_ANSWER',
  SET_CHECKBOX: 'right-time/SET_CHECKBOX',
  SET_ALL_DATA: 'right-time/SET_ALL_DATA',
  SET_RIGHT_TIME_COMPLETION: 'right-time/SET_RIGHT_TIME_COMPLETION',
};
