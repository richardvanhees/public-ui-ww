import rightTimeTypes from './types';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

const initialState = {
  userInput: {},
  checkboxInput: {},
};

const setAnswer = (state, { answerData }) => ({
  ...state,
  userInput: {
    ...state.userInput,
    ...answerData,
  },
});

const setCheckbox = (state, { key, value }) => ({
  ...state,
  checkboxInput: {
    ...state.checkboxInput,
    [key]: value,
  },
});

const setAllData = (state, { data }) => ({
  ...state,
  userInput: { ...data.userInput },
  checkboxInput: { ...data.checkboxInput },
});

const setCompletedAt = (state, { date }) => ({
  ...state,
  completedAt: date,
});

const mapFromServer = (state, { data }) => data.rightTime;

const reducer = {
  [rightTimeTypes.SET_ANSWER]: setAnswer,
  [rightTimeTypes.SET_CHECKBOX]: setCheckbox,
  [rightTimeTypes.SET_ALL_DATA]: setAllData,
  [rightTimeTypes.SET_RIGHT_TIME_COMPLETION]: setCompletedAt,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
