import test from 'tape';
import selector from './selector.js';

test('RightTimeSelector - not vulnerable', t => {
  const result = selector({
    rightTime: {
      userInput: 'fakeUserInput',
      checkboxInput: 'fakeCheckboxInput',
    },
  });

  t.deepEqual(result, {
    userInput: 'fakeUserInput',
    checkboxInput: 'fakeCheckboxInput',
    isVulnerable: false,
  });

  t.end();
});


test('RightTimeSelector - vulnerable', t => {
  const result = selector({
    rightTime: {
      userInput: { 0: { answerValue: 'yes' } },
      checkboxInput: 'fakeCheckboxInput',
    },
  });

  t.deepEqual(result, {
    userInput: { 0: { answerValue: 'yes' } },
    checkboxInput: 'fakeCheckboxInput',
    isVulnerable: true,
  });

  t.end();
});
