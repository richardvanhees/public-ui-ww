import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const userInput = state => R.path(['atr', 'data'], state);
const profile = state => R.path(['atr', 'profile'], state);

/**
 * An odd function this, but it fudges the data into the weird format
 * that the QuestionRange component requires. It prevents us leaking the component
 * requirements into the redux state and over complicating the data mappers
 */
export const transformStateForQuestionRange = (
  content,
  contentQuestionsKey,
  stateElement
) => {
  const formattedForUI = {};

  content[contentQuestionsKey].forEach((question, index) => {
    const answerValue = stateElement.data[question.answerKey];
    const answerId = R.findIndex(R.propEq('value', answerValue))(question.answers);

    formattedForUI[index] = {
      answerKey: question.answerKey,
      ...(answerValue ? { answerValue } : {}),
      ...(answerId > -1 ? { answerId } : {}),
    };
  });

  return formattedForUI;
};

export default createStructuredSelector({
  userInput,
  profile,
});
