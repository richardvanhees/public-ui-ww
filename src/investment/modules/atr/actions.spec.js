import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('ATRActions', t => {
  let result;

  result = actions.generateATRProfileStart();
  t.deepEqual(
    result,
    { type: types.GENERATE_ATR_PROFILE_START },
    'generateATRProfileStart'
  );

  result = actions.generateATRProfile();
  t.deepEqual(
    result,
    { type: types.GENERATE_ATR_PROFILE },
    'generateATRProfile'
  );

  result = actions.generateATRProfileSuccess('testData');
  t.deepEqual(
    result,
    { type: types.GENERATE_ATR_PROFILE_SUCCESS, profile: 'testData' },
    'generateATRProfileSuccess'
  );

  result = actions.generateATRProfileFailed();
  t.deepEqual(
    result,
    { type: types.GENERATE_ATR_PROFILE_FAILED },
    'generateATRProfileFailed'
  );

  t.end();
});

test('ATRActions', assert => {
  assert.plan(1);
  const result = actions.updateProgress();
  assert.deepEqual(
    result,
    { type: types.ATR_UPDATE_PROGRESS },
    'updateProgress'
  );
});
