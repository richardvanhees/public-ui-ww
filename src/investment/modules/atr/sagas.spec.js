import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generateATRProfileSaga, {
  getATRInput,
  callToGenerateATRProfile,
  generateATRProfile,
} from './sagas';
import atrTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

test('generateATRProfileSaga', t => {
  let target;

  const testData = { a2risk__foo: 'baa' };
  const formattedTestData = { foo: 'baa' };
  const testResponse = { data: 'atrProfile' };
  target = testSaga(generateATRProfile)
    .next()
    .put(setIsLoading(true))
    .next()
    .put({ type: atrTypes.GENERATE_ATR_PROFILE_START })
    .next()
    .select(getATRInput)
    .next(testData)
    .call(callToGenerateATRProfile, formattedTestData)
    .next(testResponse)
    .put(setIsLoading(false))
    .next()
    .put({
      type: atrTypes.GENERATE_ATR_PROFILE_SUCCESS,
      profile: testResponse.data,
    })
    .next();
  t.equals(!!target.isDone(), true, 'generateATRProfile success');

  t.end();
});
