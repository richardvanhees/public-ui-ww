import atrTypes from './types';

export const generateATRProfile = () => ({
  type: atrTypes.GENERATE_ATR_PROFILE,
});

export const generateATRProfileStart = () => ({
  type: atrTypes.GENERATE_ATR_PROFILE_START,
});

export const generateATRProfileSuccess = profile => ({
  type: atrTypes.GENERATE_ATR_PROFILE_SUCCESS,
  profile,
});

export const generateATRProfileFailed = () => ({
  type: atrTypes.GENERATE_ATR_PROFILE_FAILED,
});

export const resetATRSection = () => ({
  type: atrTypes.RESET_ATR,
});

export const atrManualOverride = override => ({
  type: atrTypes.ATR_MANUAL_OVERRIDE,
  override,
});

export const handleFieldChange = (name, value) => ({
  type: atrTypes.ATR_FIELD_UPDATED,
  name,
  value,
});

export const updateProgress = () => ({
  type: atrTypes.ATR_UPDATE_PROGRESS,
});
