import test from 'tape';
import reducer from './reducer.js';
import atrTypes from './types';
import { MAP_FROM_SERVER, RESET_RISK_SECTIONS } from '../fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

test('ATRReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: atrTypes.ATR_FIELD_UPDATED,
      name: 'testName',
      value: {
        answerKey: 'foo',
        answerValue: 'baa',
      },
    }
  );
  t.deepEqual(result, { data: { foo: 'baa' } }, 'ATR_FIELD_UPDATED');

  result = reducer({}, { type: atrTypes.RESET_ATR });
  t.deepEqual(result, initialState, 'RESET_ATR');

  result = reducer(
    {},
    {
      type: atrTypes.ATR_MANUAL_OVERRIDE,
      override: { key: 'testKey', profile: 'testProfile' },
    }
  );
  t.deepEqual(
    result,
    { override: { key: 'testKey', profile: 'testProfile' } },
    'ATR_MANUAL_OVERRIDE'
  );

  result = reducer({}, { type: atrTypes.GENERATE_ATR_PROFILE_START });
  t.deepEqual(
    result,
    { isLoading: true, override: false, profile: {} },
    'GENERATE_ATR_PROFILE_START'
  );

  result = reducer(
    {},
    { type: atrTypes.GENERATE_ATR_PROFILE_SUCCESS, profile: 'testProfile' }
  );
  t.deepEqual(
    result,
    { profile: 'testProfile', isLoading: false },
    'GENERATE_ATR_PROFILE_SUCCESS'
  );

  result = reducer({}, { type: atrTypes.GENERATE_ATR_PROFILE_FAILED });
  t.deepEqual(result, { isLoading: false }, 'GENERATE_ATR_PROFILE_FAILED');

  t.end();
});

test('ATRReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: MAP_FROM_SERVER,
      data: {
        atr: {
          data: { foo: 'baa' },
          profile: { foo: 'profile' },
          override: false,
        },
      },
    }
  );

  assert.deepEqual(
    res,
    {
      data: { foo: 'baa' },
      profile: { foo: 'profile' },
      override: false,
    },
    'maps from server'
  );
});

test('ATRReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: RESET_RISK_SECTIONS,
      data: {
        atr: {
          data: { foo: 'baa' },
          profile: { foo: 'profile' },
          override: false,
        },
      },
    }
  );

  assert.deepEqual(res, initialState, 'RESET_RISK_SECTIONS');
});
