import test from 'tape';
import selector, { transformStateForQuestionRange } from './selectors.js';

test('atr/selector', t => {
  const result = selector({
    atr: {
      data: 'testUserInput',
      profile: 'testProfile',
    },
  });

  t.deepEqual(result, {
    userInput: 'testUserInput',
    profile: 'testProfile',
  });

  t.end();
});

test('atr/selector/transformStateForQuestionRange', t => {
  const result = transformStateForQuestionRange(
    {
      atrQuestions: [
        {
          answerKey: 'a2risk__describe_as_cautious',
          answers: [
            {
              id: 0,
              value: 'strongly_agree',
              answer: 'Strongly agree',
            },
            {
              id: 1,
              value: 'agree',
              answer: 'Agree',
            },
          ],
        },
        {
          answerKey: 'a2risk__comfortable_investing',
          answers: [
            {
              id: 0,
              value: 'strongly_agree',
              answer: 'Strongly agree',
            },
            {
              id: 1,
              value: 'agree',
              answer: 'Agree',
            },
          ],
        },
      ],
    },
    'atrQuestions',
    {
      data: {
        a2risk__describe_as_cautious: 'strongly_agree',
        a2risk__comfortable_investing: 'agree',
      },
      profile: 'testProfile',
    }
  );

  t.deepEqual(result, {
    0: {
      answerId: 0,
      answerKey: 'a2risk__describe_as_cautious',
      answerValue: 'strongly_agree',
    },
    1: {
      answerId: 1,
      answerKey: 'a2risk__comfortable_investing',
      answerValue: 'agree',
    },
  });

  t.end();
});
