import { call, put, select, takeLatest } from 'redux-saga/effects';

import { post } from '../../../../modules/utils/AxiosWrapper';
import {
  generateATRProfileSuccess,
  generateATRProfileFailed,
  generateATRProfileStart,
} from './actions';
import atrSelector from './selectors';
import atrTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

export const getATRInput = state => atrSelector(state).userInput;

export const callToGenerateATRProfile = data =>
  post({
    route: '/v1/attitude-to-risk',
    data,
  });

export function* generateATRProfile() {
  try {
    yield put(setIsLoading(true));
    yield put(generateATRProfileStart());
    const userInput = yield select(getATRInput);

    const formattedInput = {};
    Object.keys(userInput).forEach(key => {
      formattedInput[key.replace(/a2risk__/g, '')] = userInput[key];
    });

    const profile = yield call(callToGenerateATRProfile, formattedInput);
    yield put(setIsLoading(false));
    yield put(generateATRProfileSuccess(profile.data));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(generateATRProfileFailed());
  }
}

export default function* generateATRProfileSaga() {
  yield takeLatest(atrTypes.GENERATE_ATR_PROFILE, generateATRProfile);
}
