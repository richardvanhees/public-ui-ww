import atrTypes from './types';
import {
  MAP_FROM_SERVER,
  RESET_RISK_SECTIONS,
} from '../../modules/fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

const atrFieldUpdated = (state, action) => ({
  ...state,
  data: {
    ...state.data,
    [action.value.answerKey]: action.value.answerValue,
  },
});

const resetSection = () => initialState;

const generateATRProfileStart = state => ({
  ...state,
  profile: {},
  isLoading: true,
  override: false,
});

const generateATRProfileSuccess = (state, action) => ({
  ...state,
  isLoading: false,
  profile: action.profile,
});

const generateATRProfileFailed = state => ({
  ...state,
  isLoading: false,
});

const setManualOverride = (state, action) => ({
  ...state,
  override: action.override,
});

const mapFromServer = (state, action) => ({
  ...state,
  profile: action.data.atr.profile,
  override: action.data.atr.override,
  data: {
    ...state.data,
    ...action.data.atr.data,
  },
});

const reducer = {
  [atrTypes.ATR_FIELD_UPDATED]: atrFieldUpdated,
  [atrTypes.RESET_ATR]: resetSection,
  [atrTypes.ATR_MANUAL_OVERRIDE]: setManualOverride,

  [atrTypes.GENERATE_ATR_PROFILE_START]: generateATRProfileStart,
  [atrTypes.GENERATE_ATR_PROFILE_SUCCESS]: generateATRProfileSuccess,
  [atrTypes.GENERATE_ATR_PROFILE_FAILED]: generateATRProfileFailed,
  [MAP_FROM_SERVER]: mapFromServer,
  [RESET_RISK_SECTIONS]: resetSection,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
