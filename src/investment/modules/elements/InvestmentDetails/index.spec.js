import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import InvestmentDetails from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  data: [
    { type: 'goal', icon: 'fake-icon', value: 'fake value', heading: 'fake heading' },
    { type: 'lump-sum', icon: 'fake-icon', value: 'fake value 2', heading: 'fake heading 2' },
  ]
};

test('<InvestmentDetails>', t => {

  t.equals(typeof InvestmentDetails.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<InvestmentDetails>', t => {

  t.equals(shallow(<InvestmentDetails {...fakeRequiredProps} />).find('.investment-details').length, 1, 'InvestmentDetails is rendered');

  t.end();
});

test('<InvestmentDetails>', t => {

  t.equals(shallow(<InvestmentDetails {...fakeRequiredProps} />).find('.investment-details__block').length, 2, 'Correct amount of detail blocks are rendered');

  t.end();
});