import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../../modules/elements/Icon';

const InvestmentDetails = ({ className, style, data }) => (
  <div className={`investment-details ${className}`} style={style}>
    {data.map(item =>
      <div key={item.type} className="investment-details__block">
        <div className="investment-details__icon-container">
          <Icon name={item.icon} className={`investment-details__icon investment-details__icon--${item.type}`} />
        </div>
        <div className="investment-details__content-container">
          <div className="investment-details__heading">{item.heading}</div>
          <div className="investment-details__value" data-test={`detail-value-${item.type}`}>{item.value}</div>
        </div>
      </div>
    )}
  </div>
);

InvestmentDetails.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  data: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    heading: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  })).isRequired,
};
InvestmentDetails.defaultProps = {
  className: '',
};

export default InvestmentDetails;
