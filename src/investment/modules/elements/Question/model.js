import PropTypes from 'prop-types';

export const propTypes = {
  id: PropTypes.number.isRequired,
  question: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  hasImages: PropTypes.bool,
  answers: PropTypes.array.isRequired,
  userInput: PropTypes.object.isRequired,
  headerImage: PropTypes.string,
  answerPairCount: PropTypes.number,
  tooltipPopupCallback: PropTypes.func,
};

export const defaultProps = {
  answerPairCount: 1,
};
