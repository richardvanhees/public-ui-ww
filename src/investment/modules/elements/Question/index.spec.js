import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';

import Question from './index';
import Answer from '../../../../../modules/elements/Answer/index';
import sinon from 'sinon';
import { questions } from '../../../../../server/money-wizard/db/setup/questions_v1';

const question0 = questions.questions[0];
const question1 = questions.questions[1];

const sandbox = sinon.sandbox.create();

const setAnswerSpy = sandbox.stub();
const setActiveQuestionSpy = sandbox.stub();
const unusedStub = sandbox.stub();

const fakeRequiredProps = {
  questionCount: questions.questions.length,
  active: true,
  setActiveQuestion: setActiveQuestionSpy,
  setAnswer: setAnswerSpy,
  setActiveQuestionTest: setAnswerSpy,
  animationTimeout: 0,
  generatePrioritySuggestion: unusedStub,
  getPriorities: unusedStub,
  setUserInput: unusedStub,
  userInput: {},
};

test('<Question>', t => {
  t.equals(
    typeof Question.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.end();
});
