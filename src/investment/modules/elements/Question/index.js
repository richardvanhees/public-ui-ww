import React, { PureComponent } from 'react';
import R from 'ramda';

import { propTypes, defaultProps } from './model';
import AnswerGroup from '../../../../../modules/elements/AnswerGroup/index';
import Icon from '../../../../../modules/elements/Icon/index';

export default class Question extends PureComponent {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    R.isNil(R.path([props.id, 'answerId'], props.userInput)) && props.pagerProps.hideNext();
  }

  setAnswer = async answerData => {
    const { id: thisQuestionId, onChange, pagerProps } = this.props;

    if (answerData) {
      await onChange(thisQuestionId, answerData);
      await pagerProps.next();
      pagerProps.showNext();
    }
  };

  render = () => {
    const { id: questionId, question, answerKey, userInput, answers, tooltip, tooltipPopupCallback } = this.props;

    return (
      <div className={`question question--active ${answers.length === 2 ? 'question--dual-answer' : ''}`}>
        <div className="question__header">
          <div
            className="question__label"
            data-test={`question-${questionId + 1}`}
            onClick={() => tooltipPopupCallback && tooltipPopupCallback(tooltip)}
          >
            {question}
            {tooltip && (
              <Icon
                dataTip={tooltip}
                className="question__tooltip"
                data-test={`question-${questionId + 1}-tooltip`}
                name="question-circle"
              />
            )}
          </div>
        </div>

        <AnswerGroup
          wrapperClass="answer--iw-answer"
          answers={answers}
          dataTest={`q${questionId + 1}`}
          answerKey={answerKey}
          questionId={questionId}
          selected={R.path([questionId, 'answerId'], userInput)}
          onChange={this.setAnswer}
          smallButtons
        />
      </div>
    );
  }
}
