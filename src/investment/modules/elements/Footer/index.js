import React from 'react';
import PropTypes from 'prop-types';
import { wwLogoMono } from '../../../../../assets/images/iw-images/index';
import WealthWizards from '../../../../../modules/wealthwizards';

const Footer = ({ darkBackground, showWWLogo, className }) => (
  <div
    className={`footer ${darkBackground ? 'footer--light' : ''} ${className ||
      ''}`}
  >
    {showWWLogo && (
      <div className={`footer__logo ${className ? `${className}__logo` : ''}`}>
        <img
          className="footer__logo-src"
          src={wwLogoMono}
          alt="Wealth Wizards Logo"
        />
      </div>
    )}
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      <a
        className={`link footer__link ${
          darkBackground ? 'footer__link--light' : ''
        }`}
        target="_blank"
        href="https://www.wealthwizards.com/cookie-policy/"
      >
        View cookie policy
      </a>
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      <a
        className={`link footer__link ${
          darkBackground ? 'footer__link--light' : ''
        }`}
        target="_blank"
        href="https://www.wealthwizards.com/privacy-policy/"
      >
        View privacy policy
      </a>
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      <a
        className={`link footer__link ${
          darkBackground ? 'footer__link--light' : ''
        }`}
        target="_blank"
        href="https://www.wealthwizards.com/terms-of-business.pdf"
      >
        Terms of business
      </a>
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      Copyright © {`${WealthWizards.YEAR}`} Wealth Wizards Limited
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      Wealth Wizards Advisers Limited is authorised and regulated by the
      Financial Conduct Authority and is entered on the Financial Services
      Register under reference 596436.
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      Registered Address: Wizards House, 8 Athena Court, Tachbrook Park,
      Leamington Spa, CV34 6RT.
    </div>
    <div
      className={`footer__section ${className ? `${className}__section` : ''}`}
    >
      Registered in England & Wales, No. 7273385. The information contained
      within this site is intended for UK consumers only and is subject to the
      UK regulatory regime.
    </div>
  </div>
);

Footer.propTypes = {
  darkBackground: PropTypes.bool,
  showWWLogo: PropTypes.bool,
  className: PropTypes.string,
};

Footer.defaultProps = {
  showWWLogo: true,
};

export default Footer;
