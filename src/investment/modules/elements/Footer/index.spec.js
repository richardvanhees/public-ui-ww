import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import Footer from './index';

test('<Footer>', t => {
  t.equals(typeof Footer.propTypes, 'object', 'Proptypes are defined');

  t.end();
});

test('<Footer>', t => {
  t.equals(mount(<Footer />).find('.footer').length, 1, 'Footer is rendered correctly');

  t.end();
});

test('<Footer>', t => {
  const component = mount(<Footer darkBackground />);

  t.equals(component.find('.footer--light').length, 1, 'Footer is rendered with white text');

  t.end();
});