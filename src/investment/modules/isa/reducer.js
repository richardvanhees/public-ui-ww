import isaTypes from './types';
import R from 'ramda';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

const initialState = {
  isas: [
    {
      type: 'cash',
      amount: 0,
      in_use: 'no',
    },
    {
      type: 'lifetime',
      amount: 0,
      in_use: 'no',
    },
    {
      type: 'innovation',
      amount: 0,
      in_use: 'no',
    },
  ],
  hasExisitingStocksAndSharesIsa: 'no',
  contributedToIsas: null,
  excessContributionAmount: 0,
};

const updateIsas = (isas, isaType, value, key = 'in_use') =>
  R.map(
    R.ifElse(R.propEq('type', isaType), R.assoc(key, value), R.identity),
    isas
  );

const setIsaInUse = (state, action) => ({
  ...state,
  isas: updateIsas(state.isas, action.isaType, action.value),
});

const setIsaAmount = (state, action) => {
  const nextState = {
    ...state,
    isas: updateIsas(state.isas, action.isaType, action.amount, 'amount'),
  };
  return {
    ...nextState,
  };
};

const resetIsas = state => ({
  ...state,
  isas: initialState.isas,
});

const setExistingStocksAndShares = (state, action) => {
  const nextState = {
    ...state,
    hasExisitingStocksAndSharesIsa: action.value,
  };
  return {
    ...nextState,
  };
};

const setExcessContributionsAmount = (state, action) => ({
  ...state,
  excessContributionAmount: action.value,
});

const setContributedToIsas = (state, action) => {
  const nextState = {
    ...state,
    contributedToIsas: action.value,
    isas: action.value === 'no' ? initialState.isas : state.isas,
  };
  return {
    ...nextState,
  };
};

const mapFromServer = (state, action) => ({
  ...state,
  ...action.data.existingIsas,
});

const reducer = {
  [isaTypes.SET_ISA_IN_USE]: setIsaInUse,
  [isaTypes.SET_ISA_AMOUNT]: setIsaAmount,
  [isaTypes.RESET_ISAS]: resetIsas,
  [isaTypes.SET_EXCESS_CONTRIBUTION_AMOUNT]: setExcessContributionsAmount,
  [isaTypes.SET_EXISTING_STOCKS_AND_SHARES]: setExistingStocksAndShares,
  [isaTypes.SET_CONTRIBUTED_TO_ISAS]: setContributedToIsas,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
