import test from 'tape';
import {
  calculateContributionSum,
  calculateChangeAmount,
} from './isa-calculations';
import timekeeper from 'timekeeper';
import moment from 'moment';

test('calculateContributionSum', t => {
  let target;

  let testIsas = [
    { amount: 3, in_use: 'yes' },
    { amount: 15, in_use: 'yes' },
    { amount: 1000, in_use: 'no' },
  ];

  target = calculateContributionSum(testIsas);
  t.equals(target, 18, 'should be sum of amount for all isas where in_use');

  t.end();
});

test('calculateChangeAmount', t => {
  let target;

  target = calculateChangeAmount(14000, 8000, 0);
  t.equals(
    target,
    2000,
    'should calculate discrepency in investment goal amount'
  );

  t.end();
});

test('calculateChangeAmount', t => {
  let target;

  timekeeper.freeze(moment(`${moment().utc().year()}-05-05`, 'YYYY-MM-DD').utc().format());
  target = calculateChangeAmount(14000, 6000, 180);
  timekeeper.reset();

  t.equals(
    target,
    1980,
    'should calculate discrepency in investment goal amount with monthly contribtion'
  );

  t.end();
});
