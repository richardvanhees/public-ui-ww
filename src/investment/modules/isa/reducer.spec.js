import test from 'tape';
import reducer from './reducer.js';
import atrTypes from './types';
import { MAP_FROM_SERVER } from '../fact-find/actions';

test('IsaReducer', t => {
  let result;
  const state = {
    isas: [
      { type: 'cash', amount: 1000, in_use: 'yes' },
      { type: 'innovation', amount: 0, in_use: 'no' },
    ],
    hasExisitingStocksAndSharesIsa: 'no',
    contributedToIsas: 'yes',
  };

  result = reducer(state, {
    type: atrTypes.SET_ISA_IN_USE,
    isaType: 'cash',
    value: 'no',
  });
  t.deepEqual(
    result,
    {
      isas: [
        { type: 'cash', amount: 1000, in_use: 'no' },
        { type: 'innovation', amount: 0, in_use: 'no' },
      ],
      hasExisitingStocksAndSharesIsa: 'no',
      contributedToIsas: 'yes',
    },
    'SET_ISA_IN_USE'
  );

  result = reducer(state, {
    type: atrTypes.SET_ISA_AMOUNT,
    isaType: 'innovation',
    amount: 5000,
  });
  t.deepEqual(
    result,
    {
      isas: [
        { type: 'cash', amount: 1000, in_use: 'yes' },
        { type: 'innovation', amount: 5000, in_use: 'no' },
      ],
      hasExisitingStocksAndSharesIsa: 'no',
      contributedToIsas: 'yes',
    },
    'SET_ISA_AMOUNT'
  );

  result = reducer(state, { type: atrTypes.RESET_ISAS });
  t.deepEqual(
    result,
    {
      ...state,
      isas: [
        { type: 'cash', amount: 0, in_use: 'no' },
        { type: 'lifetime', amount: 0, in_use: 'no' },
        { type: 'innovation', amount: 0, in_use: 'no' },
      ],
    },
    'RESET_ISAS'
  );

  result = reducer(state, {
    type: atrTypes.SET_EXISTING_STOCKS_AND_SHARES,
    value: 'yes',
  });
  t.deepEqual(
    result,
    { ...state, hasExisitingStocksAndSharesIsa: 'yes' },
    'SET_EXISTING_STOCKS_AND_SHARES'
  );

  t.end();
});

test('IsaReducer', assert => {
  assert.plan(1);
  const result = reducer(
    {},
    {
      type: MAP_FROM_SERVER,
      data: {
        existingIsas: {
          hasExisitingStocksAndSharesIsa: 'no',
          contributedToIsas: 'no',
        },
      },
    }
  );
  assert.deepEqual(
    result,
    {
      hasExisitingStocksAndSharesIsa: 'no',
      contributedToIsas: 'no',
    },
    'maps from server'
  );
});

test('IsaReducer', assert => {
  assert.plan(1);

  const res = reducer(
    { hasExisitingStocksAndSharesIsa: 'no', isas: [] },
    {
      type: atrTypes.SET_CONTRIBUTED_TO_ISAS,
      value: 'yes',
    }
  );

  assert.deepEqual(
    res,
    {
      hasExisitingStocksAndSharesIsa: 'no',
      contributedToIsas: 'yes',
      isas: [],
    },
    'sets contributedToIsa'
  );
});

test('IsaReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {
      hasExisitingStocksAndSharesIsa: 'no',
      isas: [
        { type: 'cash', amount: 1000, in_use: 'yes' },
        { type: 'lifetime', amount: 0, in_use: 'no' },
        { type: 'innovation', amount: 0, in_use: 'no' },
      ],
    },
    {
      type: atrTypes.SET_CONTRIBUTED_TO_ISAS,
      value: 'no',
    }
  );

  assert.deepEqual(
    res,
    {
      hasExisitingStocksAndSharesIsa: 'no',
      contributedToIsas: 'no',
      isas: [
        { type: 'cash', amount: 0, in_use: 'no' },
        { type: 'lifetime', amount: 0, in_use: 'no' },
        { type: 'innovation', amount: 0, in_use: 'no' },
      ],
    },
    'when no : sets contributedToIsa'
  );
});
