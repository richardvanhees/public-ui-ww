import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('IsasActions', t => {
  let result;

  result = actions.setIsaInUse('foo', 'bar');
  t.deepEqual(
    result,
    { type: types.SET_ISA_IN_USE, isaType: 'foo', value: 'bar' },
    'setIsaInUse success'
  );

  result = actions.setIsaAmount('foo', 'bar');
  t.deepEqual(
    result,
    { type: types.SET_ISA_AMOUNT, isaType: 'foo', amount: 'bar' },
    'setIsaAmount success'
  );

  result = actions.resetIsas('foo', 'bar');
  t.deepEqual(result, { type: types.RESET_ISAS }, 'resetIsas success');

  result = actions.setExcessContributionAmount('foo');
  t.deepEqual(
    result,
    { type: types.SET_EXCESS_CONTRIBUTION_AMOUNT, value: 'foo' },
    'setExcessContributionAmount success'
  );

  result = actions.setExistingStocksAndShares('foo');
  t.deepEqual(
    result,
    { type: types.SET_EXISTING_STOCKS_AND_SHARES, value: 'foo' },
    'setExistingStocksAndShares success'
  );

  result = actions.setContributedToIsas('yes');
  t.deepEqual(
    result,
    { type: types.SET_CONTRIBUTED_TO_ISAS, value: 'yes' },
    'setContributedToIsas success'
  );

  t.end();
});
