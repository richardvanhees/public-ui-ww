import R from 'ramda';
import { contributionValueForTheTaxYr } from '../../utils/contribution-calculators';

export const calculateContributionSum = isas =>
  R.compose(R.sum, R.pluck('amount'), R.filter(R.propEq('in_use', 'yes')))(
    isas
  );

export const calculateChangeAmount = (
  contributedAmount,
  investmentGoalAmount,
  investmentGoalMonthly = 0
) => (investmentGoalAmount + contributionValueForTheTaxYr(investmentGoalMonthly)) - (20000 - contributedAmount);
