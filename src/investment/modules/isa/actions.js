import isaTypes from './types';

export const setIsaInUse = (isaType, value) => ({
  type: isaTypes.SET_ISA_IN_USE,
  isaType,
  value,
});

export const setIsaAmount = (isaType, amount) => ({
  type: isaTypes.SET_ISA_AMOUNT,
  isaType,
  amount,
});

export const resetIsas = () => ({
  type: isaTypes.RESET_ISAS,
});

export const setExistingStocksAndShares = value => ({
  type: isaTypes.SET_EXISTING_STOCKS_AND_SHARES,
  value,
});

export const setExcessContributionAmount = value => ({
  type: isaTypes.SET_EXCESS_CONTRIBUTION_AMOUNT,
  value,
});

export const setContributedToIsas = value => ({
  type: isaTypes.SET_CONTRIBUTED_TO_ISAS,
  value,
});
