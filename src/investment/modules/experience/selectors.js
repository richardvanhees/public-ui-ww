import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const userInput = state => R.path(['experience', 'data'], state);
const profile = state => R.path(['experience', 'profile'], state);

export default createStructuredSelector({
  userInput,
  profile,
});
