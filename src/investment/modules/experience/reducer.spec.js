import test from 'tape';
import reducer from './reducer.js';
import atrTypes from './types';
import { MAP_FROM_SERVER, RESET_RISK_SECTIONS } from '../fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

test('ExperienceReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: atrTypes.EXPERIENCE_FIELD_UPDATED,
      value: {
        answerKey: 'foo',
        answerValue: 'baa',
      },
    }
  );
  t.deepEqual(
    result,
    {
      data: {
        foo: 'baa',
      },
    },
    'EXPERIENCE_FIELD_UPDATED'
  );

  result = reducer(
    {},
    {
      type: atrTypes.RESET_EXPERIENCE,
    }
  );
  t.deepEqual(result, initialState, 'RESET_EXPERIENCE');

  result = reducer(
    {},
    {
      type: atrTypes.EXPERIENCE_MANUAL_OVERRIDE,
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    }
  );
  t.deepEqual(
    result,
    {
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    },
    'EXPERIENCE_MANUAL_OVERRIDE'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_EXPERIENCE_PROFILE_START,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: true,
      override: false,
      profile: {},
    },
    'GENERATE_EXPERIENCE_PROFILE_START'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_EXPERIENCE_PROFILE_SUCCESS,
      profile: 'testProfile',
    }
  );
  t.deepEqual(
    result,
    {
      profile: 'testProfile',
      isLoading: false,
    },
    'GENERATE_EXPERIENCE_PROFILE_SUCCESS'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_EXPERIENCE_PROFILE_FAILED,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: false,
    },
    'GENERATE_EXPERIENCE_PROFILE_FAILED'
  );

  t.end();
});

test('ExperienceReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: MAP_FROM_SERVER,
      data: {
        experience: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(
    res,
    {
      data: {
        foo: 'baa',
      },
      profile: 'fake profile',
      override: 'fake override',
    },
    'maps from server'
  );
});

test('ExperienceReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: RESET_RISK_SECTIONS,
      data: {
        experience: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(res, initialState, 'resets risk data');
});
