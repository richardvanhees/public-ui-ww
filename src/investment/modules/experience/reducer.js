import experienceTypes from './types';
import {
  MAP_FROM_SERVER,
  RESET_RISK_SECTIONS,
} from '../../modules/fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: false,
};

const experienceFieldUpdated = (state, action) => ({
  ...state,
  data: {
    ...state.data,
    [action.value.answerKey]: action.value.answerValue,
  },
});

const mapFromServer = (state, action) => ({
  ...state,
  profile: action.data.experience.profile,
  override: action.data.experience.override,
  data: {
    ...state.data,
    ...action.data.experience.data,
  },
});

const resetSection = () => initialState;

const generateExperienceProfileStart = state => ({
  ...state,
  isLoading: true,
  override: false,
  profile: {},
});

const generateExperiencerofileSuccess = (state, action) => ({
  ...state,
  isLoading: false,
  profile: action.profile,
});

const generateExperiencerofileFailed = state => ({
  ...state,
  isLoading: false,
});

const setManualOverride = (state, action) => ({
  ...state,
  override: action.override,
});

const reducer = {
  [experienceTypes.EXPERIENCE_FIELD_UPDATED]: experienceFieldUpdated,
  [experienceTypes.RESET_EXPERIENCE]: resetSection,
  [RESET_RISK_SECTIONS]: resetSection,
  [experienceTypes.EXPERIENCE_MANUAL_OVERRIDE]: setManualOverride,
  [experienceTypes.GENERATE_EXPERIENCE_PROFILE_START]: generateExperienceProfileStart,
  [experienceTypes.GENERATE_EXPERIENCE_PROFILE_SUCCESS]: generateExperiencerofileSuccess,
  [experienceTypes.GENERATE_EXPERIENCE_PROFILE_FAILED]: generateExperiencerofileFailed,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
