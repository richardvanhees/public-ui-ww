import test from 'tape';
import selector from './selectors.js';

test('ExperienceSelector', t => {
  const result = selector({
    experience: {
      data: 'testUserInput',
      profile: 'testProfile',
    },
  });

  t.deepEqual(result, {
    userInput: 'testUserInput',
    profile: 'testProfile',
  });

  t.end();
});
