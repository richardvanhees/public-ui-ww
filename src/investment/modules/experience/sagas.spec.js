import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generateExperienceProfileSaga, {
  getExperienceInput,
  callToGenerateExperienceProfile,
  generateExperienceProfile,
} from './sagas';
import atrTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

test('generateExperienceProfileSaga', t => {
  let target;

  const testData = { investment_experience__foo: 'baa' };
  const formattedTestData = { foo: 'baa' };
  const testResponse = { data: 'atrProfile' };
  target = testSaga(generateExperienceProfile)
    .next()
    .put(setIsLoading(true))
    .next()
    .put({ type: atrTypes.GENERATE_EXPERIENCE_PROFILE_START })
    .next()
    .select(getExperienceInput)
    .next(testData)
    .call(callToGenerateExperienceProfile, formattedTestData)
    .next(testResponse)
    .put(setIsLoading(false))
    .next()
    .put({
      type: atrTypes.GENERATE_EXPERIENCE_PROFILE_SUCCESS,
      profile: testResponse.data,
    })
    .next();
  t.equals(!!target.isDone(), true, 'generateExperienceProfile success');

  t.end();
});
