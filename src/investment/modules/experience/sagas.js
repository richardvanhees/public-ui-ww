import { call, put, select, takeLatest } from 'redux-saga/effects';

import { post } from '../../../../modules/utils/AxiosWrapper';
import {
  generateExperienceProfileSuccess,
  generateExperienceProfileFailed,
  generateExperienceProfileStart,
} from './actions';
import experienceSelector from './selectors';
import experienceTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

export const getExperienceInput = state => experienceSelector(state).userInput;

export const callToGenerateExperienceProfile = data =>
  post({
    route: '/v1/investment-experience',
    data,
  });

export function* generateExperienceProfile() {
  try {
    yield put(setIsLoading(true));
    yield put(generateExperienceProfileStart());
    const userInput = yield select(getExperienceInput);

    const formattedInput = {};
    Object.keys(userInput).forEach(key => {
      formattedInput[key.replace(/investment_experience__/g, '')] =
        userInput[key];
    });

    const profile = yield call(callToGenerateExperienceProfile, formattedInput);
    yield put(setIsLoading(false));
    yield put(generateExperienceProfileSuccess(profile.data));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(generateExperienceProfileFailed());
  }
}

export default function* generateExperienceProfileSaga() {
  yield takeLatest(
    experienceTypes.GENERATE_EXPERIENCE_PROFILE,
    generateExperienceProfile
  );
}
