import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('ExperienceActions', t => {
  let result;

  result = actions.generateExperienceProfileStart();
  t.deepEqual(
    result,
    { type: types.GENERATE_EXPERIENCE_PROFILE_START },
    'generateExperienceProfileStart'
  );

  result = actions.generateExperienceProfile();
  t.deepEqual(
    result,
    { type: types.GENERATE_EXPERIENCE_PROFILE },
    'generateExperienceProfile'
  );

  result = actions.generateExperienceProfileSuccess('testData');
  t.deepEqual(
    result,
    { type: types.GENERATE_EXPERIENCE_PROFILE_SUCCESS, profile: 'testData' },
    'generateExperienceProfileSuccess'
  );

  result = actions.generateExperienceProfileFailed();
  t.deepEqual(
    result,
    { type: types.GENERATE_EXPERIENCE_PROFILE_FAILED },
    'generateExperienceProfileFailed'
  );

  t.end();
});

test('ExperienceActions', assert => {
  assert.plan(1);
  const result = actions.updateProgress();
  assert.deepEqual(
    result,
    { type: types.EXPERIENCE_UPDATE_PROGRESS },
    'updateProgress'
  );
});
