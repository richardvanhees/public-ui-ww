import experienceTypes from './types';

export const generateExperienceProfile = () => ({
  type: experienceTypes.GENERATE_EXPERIENCE_PROFILE,
});

export const generateExperienceProfileStart = () => ({
  type: experienceTypes.GENERATE_EXPERIENCE_PROFILE_START,
});

export const generateExperienceProfileSuccess = profile => ({
  type: experienceTypes.GENERATE_EXPERIENCE_PROFILE_SUCCESS,
  profile,
});

export const generateExperienceProfileFailed = () => ({
  type: experienceTypes.GENERATE_EXPERIENCE_PROFILE_FAILED,
});

export const resetExperienceSection = () => ({
  type: experienceTypes.RESET_EXPERIENCE,
});

export const experienceManualOverride = override => ({
  type: experienceTypes.EXPERIENCE_MANUAL_OVERRIDE,
  override,
});

export const handleFieldChange = (name, value) => ({
  type: experienceTypes.EXPERIENCE_FIELD_UPDATED,
  name,
  value,
});

export const updateProgress = () => ({
  type: experienceTypes.EXPERIENCE_UPDATE_PROGRESS,
});
