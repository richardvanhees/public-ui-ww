import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const solutionId = state => R.path(['solution', 'solution_id'], state);
const paymentStatus = state =>
  R.path(['solution', 'data', 'paymentStatus'], state);
const referralStatus = state => R.path(['solution', 'referred'], state);

export const alertBasedReferral = solution =>
  R.compose(
    // an adviser can override this by setting the solution to be
    // unlocked in RPP
    alerts => alerts.length > 0 && !R.path(['data', 'unlocked'])(solution),
    R.pathOr([], ['data', 'alerts'])
  )(solution);

export const referralReasons = R.pathOr([], ['referralReasons']);

export const lisaReferral = R.compose(
  R.allPass([R.contains('lisa'), reasons => reasons.length === 1]),
  referralReasons
);

export default createStructuredSelector({
  solutionId,
  paymentStatus,
  referralStatus,
});
