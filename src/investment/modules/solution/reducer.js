import generateSolutionTypes from './types';
import factFindTypes from '../fact-find/types';

const initialState = {
  isLoading: false,
  solution_id: '',
  data: {},
  referral: false,
};

const generateSolutionRequest = state => ({
  ...state,
  isLoading: true,
});

const generateSolutionFailure = (state, { error }) => ({
  ...state,
  error,
  isLoading: false,
});

const downloadReportRequest = (state, { viewType }) => ({
  ...state,
  viewType,
  isLoading: true,
});

const downloadReportSuccess = state => ({
  ...state,
  isLoading: false,
});

const setPublishedToCrm = state => ({
  ...state,
  data: {
    ...state.data,
    submittedToIO: true,
  },
});

const downloadReportFailure = (state, { error }) => ({
  ...state,
  error,
  isLoading: false,
});

const setSolutionData = (state, { data }) => ({
  ...state,
  solution_id: data.solution_id,
  isLoading: false,
  referralReasons: data.solution.referralReasons || [],
  data: {
    ...state.data,
    ...data.solution,
    submittedToIO: data.submittedToIO,
  },
});

const initialLoad = (state, { solution }) => {
  if (!solution) {
    return { ...state };
  }
  if (solution.referral) {
    return {
      ...state,
      referral: true,
      referralReasons: solution.referralReasons,
    };
  }
  return {
    ...state,
    solution_id: solution.solution_id,
    data: {
      ...state.data,
      date_generated: solution.solution_creation,
      riskLevel: solution.risk_level,
      investmentIsaAllocations: solution.investmentIsaAllocations,
      paymentStatus: solution.paymentStatus,
      alerts: solution.alerts,
      submittedToIO: solution.submittedToIO,
      unlocked: solution.unlocked,
    },
  };
};

const setSolutionPaidSuccess = state => ({
  ...state,
  data: {
    ...state.data,
    paymentStatus: 'PAID',
  },
});

const referral = state => ({
  ...state,
  referral: true,
});

const reducer = {
  [generateSolutionTypes.GENERATE_SOLUTION_REQUEST]: generateSolutionRequest,
  [generateSolutionTypes.GENERATE_SOLUTION_SUCCESS]: setSolutionData,
  [generateSolutionTypes.GENERATE_SOLUTION_FAILURE]: generateSolutionFailure,
  [generateSolutionTypes.DOWNLOAD_REPORT_REQUEST]: downloadReportRequest,
  [generateSolutionTypes.DOWNLOAD_REPORT_SUCCESS]: downloadReportSuccess,
  [generateSolutionTypes.DOWNLOAD_REPORT_FAILURE]: downloadReportFailure,
  [generateSolutionTypes.SET_SOLUTION_PAID_SUCCESS]: setSolutionPaidSuccess,
  [generateSolutionTypes.REFERRAL]: referral,
  [generateSolutionTypes.PUBLISHED_TO_CRM]: setPublishedToCrm,
  [factFindTypes.INITIAL_LOAD]: initialLoad,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
