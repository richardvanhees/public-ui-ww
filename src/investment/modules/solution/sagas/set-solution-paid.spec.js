import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import {
  setSolutionPaid,
  callToSetPaymentStatus,
  getSolutionId,
  getPaymentStatus,
  getReferralStatus,
} from './set-solution-paid';
import { setSolutionPaidSuccess } from '../actions';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';

test('setSolutionPaid', t => {
  let target;

  const solutionData = {
    solution_id: '5bdc5d54868fb40024ece2e3',
  };

  target = testSaga(setSolutionPaid, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionData.solution_id)
    .select(getPaymentStatus)
    .next('CLAIMED')
    .select(getReferralStatus)
    .next(false)
    .call(callToSetPaymentStatus, solutionData.solution_id)
    .next()
    .put(setSolutionPaidSuccess())
    .next()
    .put(setIsLoading(false))
    .next();

  t.equals(!!target.isDone(), true, 'solution payment status was set to paid');

  t.end();
});

test('setSolutionPaid', t => {
  let target;

  const solutionData = {
    solution_id: null,
  };

  target = testSaga(setSolutionPaid, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionData.solution_id)
    .select(getPaymentStatus)
    .next('CLAIMED')
    .select(getReferralStatus)
    .next(false)
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'solution not generated');

  t.end();
});

test('setSolutionPaid', t => {
  let target;

  const solutionData = {
    solution_id: '123',
  };

  target = testSaga(setSolutionPaid, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionData.solution_id)
    .select(getPaymentStatus)
    .next('CLAIMED')
    .select(getReferralStatus)
    .next(false)
    .call(callToSetPaymentStatus, solutionData.solution_id)
    .throw('aaa')
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'Your payment has succeeded, but we were unable to update the status in our system.'
      )
    )
    .next();

  t.true(target.isDone(), 'solution not generated, error shown');

  t.end();
});
