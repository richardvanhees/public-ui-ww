import { call, put, takeLatest, select } from 'redux-saga/effects';
import WealthWizards from '../../../../../modules/wealthwizards';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';
import { generateSolutionSuccess, referral } from '../actions';
import generateSolutionTypes from '../types';
import R from 'ramda';
import { push } from 'react-router-redux';
import { post } from '../../../../../modules/utils/refresh-request';
import { getItem } from '../../../../../modules/utils/local-storage';

export const generateSolutionCall = () =>
  post(
    `${
      WealthWizards.SOLUTION_STORE_URL
    }/v1/user/solution?capability=investment`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

export const getContent = state =>
  R.path(['content', 'data', 'investment'])(state);

export function* generateSolution({ redirectPath }) {
  try {
    yield put(setIsLoading(true));

    const { data } = yield call(generateSolutionCall);

    yield put(generateSolutionSuccess(data));

    yield put(setIsLoading(false));

    if (R.path(['solution', 'referralReasons', 'length'])(data)) {
      yield put.resolve(referral());
      yield put(push(`${WealthWizards.CONTEXT_ROUTE}/referral`));
    } else if (redirectPath) {
      yield put(push(`${WealthWizards.CONTEXT_ROUTE}${redirectPath}`));
    }
  } catch (error) {
    yield put(setIsLoading(false));

    const statusCode = R.path(['response', 'status'])(error);

    if (!statusCode) {
      yield put(
        setError(
          'You appear to be offline, please check your internet connection.'
        )
      );
      return;
    }

    const errorCode = R.path(['response', 'data', 'code'])(error);
    const content = yield select(getContent);
    const errorMessage = R.path(['errorMessages', errorCode])(content);

    if (errorMessage) {
      if (errorCode === 'INVALID_INVESTMENT_AMOUNT') {
        yield put.resolve(push(`${WealthWizards.CONTEXT_ROUTE}/amount`));
      }
      yield put(setError(errorMessage));
    } else {
      yield put(
        setError(
          'We are currently unable to generate a solution, please try again later.'
        )
      );
    }
  }
}

export default function* generateSolutionSaga() {
  yield takeLatest(
    generateSolutionTypes.GENERATE_SOLUTION_REQUEST,
    generateSolution
  );
}
