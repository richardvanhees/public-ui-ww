import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import WealthWizards from '../../../../../modules/wealthwizards';
import generateSolutionTypes from '../types';
import publishToCrmSaga, {
  publishToCrm,
  publishedAlready,
  publishToCrmCall,
  __RewireAPI__,
} from './publish-to-crm';
import sinon from 'sinon';
import { push } from 'react-router-redux';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import { saveFactFindRequest } from '../../fact-find/actions';
import { setPublishedToCrm } from '../actions';

test('publish-to-crm', t => {
  const target = testSaga(publishToCrm)
    .next()
    .select(publishedAlready)
    .next(false)
    .put(setIsLoading(true))
    .next()
    .call(publishToCrmCall)
    .next()
    .put(saveFactFindRequest({ doInBackground: false }))
    .next()
    .put(setPublishedToCrm())
    .next()
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'publish-to-crm success');

  t.end();
});

test('publish-to-crm', t => {
  const target = testSaga(publishToCrm)
    .next()
    .select(publishedAlready)
    .next(false)
    .put(setIsLoading(true))
    .next()
    .call(publishToCrmCall)
    .throw(new Error('aaaa'))
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'We are currently unable to confirm your objective, please try again later.'
      )
    )
    .next();

  t.true(target.isDone(), 'publish-to-crm failure');

  t.end();
});

test('publish-to-crm', t => {
  const target = testSaga(publishToCrm)
    .next()
    .select(publishedAlready)
    .next(true);

  t.true(target.isDone(), 'publish-to-crm already done');

  t.end();
});
