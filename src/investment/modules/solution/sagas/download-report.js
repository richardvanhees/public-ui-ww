import { call, put, select, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../../../../modules/wealthwizards';
import { downloadReportSuccess } from '../actions';
import solutionSelector from '../selectors';
import generateSolutionTypes from '../types';
import filesaver from 'file-saver';
import { get } from '../../../../../modules/utils/refresh-request';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import openPdf from '../../../../../modules/utils/open-pdf';
import { getItem } from '../../../../../modules/utils/local-storage';
import { saveFactFindRequest } from '../../fact-find/actions';
import { downloaded } from '../../solution-metadata/selector';

export const getSolutionId = state => solutionSelector(state).solutionId;

export const saveDocument = data => {
  filesaver(data, 'report.pdf');
};

export const downloadReportCall = solutionId =>
  get(
    `${
      WealthWizards.SOLUTION_STORE_URL
    }/v1/user/solution/${solutionId}/suitability-report`,
    `Bearer ${getItem('user_token')}`,
    {},
    'blob'
  );

export const solutionHasDownloaded = ({ solutionMetadata, solution }) => downloaded(
  solutionMetadata,
  solution.solution_id
);

export function* downloadReport({ data: pdf }) {
  try {
    yield put(setIsLoading(true));
    const solutionId = yield select(getSolutionId);
    const hasDownloaded = yield select(solutionHasDownloaded);
    const { data } = yield call(downloadReportCall, solutionId);

    if (pdf.viewType === 'view') {
      if (!hasDownloaded) {
        yield put(downloadReportSuccess(solutionId));
        yield put(saveFactFindRequest({
          doInBackground: true,
          callback: () => (openPdf(data)),
        }));
      } else {
        yield call(openPdf, data);
      }
    } else {
      yield call(saveDocument, data);
      if (!hasDownloaded) {
        yield put(downloadReportSuccess(solutionId));
        yield put(saveFactFindRequest({ doInBackground: true }));
      }
    }

    yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(
      setError(
        'We are currently unable download your report, please try again later.'
      )
    );
  }
}

export default function* downloadReportSaga() {
  yield takeLatest(
    generateSolutionTypes.DOWNLOAD_REPORT_REQUEST,
    downloadReport
  );
}
