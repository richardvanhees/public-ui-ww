import { call, put, takeLatest, select } from 'redux-saga/effects';
import WealthWizards from '../../../../../modules/wealthwizards';
import generateSolutionTypes from '../types';
import { post } from '../../../../../modules/utils/refresh-request';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import { getItem } from '../../../../../modules/utils/local-storage';
import { saveFactFindRequest } from '../../fact-find/actions';
import { setPublishedToCrm } from '../actions';
import R from 'ramda';

export const publishToCrmCall = () =>
  post(
    `${
      WealthWizards.SOLUTION_STORE_URL
    }/v1/user/solution/publish-to-crm?capability=investment`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

export const publishedAlready = R.pathOr(false, [
  'solution',
  'data',
  'submittedToIO',
]);

export function* publishToCrm() {
  try {
    const beenPublishedAlready = yield select(publishedAlready);

    if (beenPublishedAlready) {
      return;
    }

    yield put(setIsLoading(true));

    yield call(publishToCrmCall);

    yield put(saveFactFindRequest({ doInBackground: false }));

    yield put(setPublishedToCrm());

    yield put(setIsLoading(false));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(
      setError(
        'We are currently unable to confirm your objective, please try again later.'
      )
    );
  }
}

export default function* publishToCrmSaga() {
  yield takeLatest(generateSolutionTypes.PUBLISH_TO_CRM, publishToCrm);
}
