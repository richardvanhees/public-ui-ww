import { call, put, select, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../../../../modules/wealthwizards';
import generateSolutionTypes from '../types';
import { post } from '../../../../../modules/utils/refresh-request';
import {
  showDownloadNotification,
  setIsLoading,
  setError,
} from '../../../../../modules/browser/actions';
import { getItem } from '../../../../../modules/utils/local-storage';
import { downloaded } from '../../solution-metadata/selector';

export const generateUidCall = () =>
  post(
    `${WealthWizards.INVESTMENT_FULFILLMENT_URL}webhook`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

const buildUrl = uid => {
  const callback = `${
    WealthWizards.INVESTMENT_FULFILLMENT_URL
  }webhook/claim?uid=${uid}`;

  const fallback = `https://${
    WealthWizards.FQDN
  }/investment/dashboard?paymentError`;

  const queryParams = `?journeyId=ww&callback=${encodeURIComponent(
    callback
  )}&fallback=${encodeURIComponent(fallback)}`;

  return `${WealthWizards.THIRD_PARTY_PAYMENT_GATEWAY}${queryParams}`;
};

export const redirect = url => (window.location = url);
export const solutionHasDownloaded = ({ solutionMetadata, solution }) =>
  downloaded(solutionMetadata, solution.solution_id);

export function* requestPaymentUid() {
  try {
    yield put(setIsLoading(true));
    const hasDownloaded = yield select(solutionHasDownloaded);
    if (hasDownloaded) {
      const { data } = yield call(generateUidCall);
      yield put(setIsLoading(false));
      yield put(redirect(buildUrl(data.uid)));
    } else {
      yield put(showDownloadNotification());
      yield put(setIsLoading(false));
    }
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(
      setError('We are currently process your request, please try again later.')
    );
  }
}

export default function* requestPaymentUidSaga() {
  yield takeLatest(
    generateSolutionTypes.PAYMENT_UID_REQUEST,
    requestPaymentUid
  );
}
