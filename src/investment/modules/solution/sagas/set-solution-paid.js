import { call, put, select, takeLatest } from 'redux-saga/effects';
import { event } from 'react-ga';
import WealthWizards from '../../../../../modules/wealthwizards';
import solutionTypes from '../types';
import { post } from '../../../../../modules/utils/refresh-request';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import { setSolutionPaidSuccess } from '../actions';
import { getItem } from '../../../../../modules/utils/local-storage';
import solutionSelector from '../selectors';

export const callToSetPaymentStatus = solutionId =>
  post(
    `${WealthWizards.INVESTMENT_FULFILLMENT_URL}solution/${solutionId}/paid`,
    `Bearer ${getItem('user_token')}`,
    {}
  );

export const getSolutionId = state => solutionSelector(state).solutionId;
export const getPaymentStatus = state => solutionSelector(state).paymentStatus;
export const getReferralStatus = state =>
  solutionSelector(state).referralStatus;

export function* setSolutionPaid() {
  try {
    yield put(setIsLoading(true));
    const solutionId = yield select(getSolutionId);
    const paymentStatus = yield select(getPaymentStatus);
    const referred = yield select(getReferralStatus);

    if (solutionId && !referred && paymentStatus === 'CLAIMED') {
      yield call(callToSetPaymentStatus, solutionId);
      yield put(setSolutionPaidSuccess());
    }
    yield put(setIsLoading(false));
  } catch (error) {
    event({
      category: 'Error',
      action: 'Failed to update payment status after payment succeeded',
    });
    yield put(setIsLoading(false));
    yield put(
      setError(
        'Your payment has succeeded, but we were unable to update the status in our system.'
      )
    );
  }
}

export default function* setSolutionPaidSaga() {
  yield takeLatest(solutionTypes.SET_SOLUTION_PAID, setSolutionPaid);
}
