import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import WealthWizards from '../../../../../modules/wealthwizards';
import generateSolutionTypes from '../types';
import requestPaymentUidSaga, {
  requestPaymentUid,
  generateUidCall,
  redirect,
  solutionHasDownloaded,
  __RewireAPI__,
} from './request-payment-uid';
import sinon from 'sinon';
import {
  showDownloadNotification,
  setIsLoading,
  setError,
} from '../../../../../modules/browser/actions';

const sandbox = sinon.sandbox.create();
const uid = 'test_138653865438345vdkjhfd';

test('requestPaymentUid success downloaded report', t => {
  __RewireAPI__.__Rewire__('WealthWizards', {
    INVESTMENT_FULFILLMENT_URL:
      'https://athena-blue.wealthwizards.io/investment-fulfillment/v1/',
    FQDN: 'https://athena.wealthwizards.io/investment',
    THIRD_PARTY_PAYMENT_GATEWAY:
      'https://test.legalandgeneral.com/investments/isas/apply/assets/static/isa-apply/',
  });

  const redirectUrl =
    'https://test.legalandgeneral.com/investments/isas/apply/assets/static/isa-apply/?journeyId=ww&callback=https%3A%2F%2Fathena-blue.wealthwizards.io%2Finvestment-fulfillment%2Fv1%2Fwebhook%2Fclaim%3Fuid%3Dtest_138653865438345vdkjhfd&fallback=https%3A%2F%2Fhttps%3A%2F%2Fathena.wealthwizards.io%2Finvestment%2Finvestment%2Fdashboard%3FpaymentError';

  const uidData = { data: { uid } };
  const target = testSaga(requestPaymentUid)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(solutionHasDownloaded)
    .next(true)
    .call(generateUidCall)
    .next(uidData)
    .put(setIsLoading(false))
    .next()
    .put(redirect(redirectUrl))
    .next(true);

  t.true(target.isDone(), 'requestPaymentUid success');
  t.end();

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('requestPaymentUid not downloaded report', t => {
  const uidData = { data: { uid } };
  const target = testSaga(requestPaymentUid)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(solutionHasDownloaded)
    .next(false)
    .put(showDownloadNotification())
    .next()
    .put(setIsLoading(false))
    .next();

  t.true(target.isDone(), 'requestPaymentUid not sucessfull');
  t.end();
});
