import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import WealthWizards from '../../../../../modules/wealthwizards';
import {
  responseErrorHandler,
  getCorrelationIdHeader,
} from '../../../../../modules/utils';
import { downloadReportSuccess, downloadReportFailure } from '../actions';
import generateSolutionTypes from '../types';
import downloadReportSaga, {
  downloadReport,
  downloadReportCall,
  getSolutionId,
  saveDocument,
  solutionHasDownloaded,
  callbackFactory,
  __RewireAPI__,
} from './download-report';
import sinon from 'sinon';
import { push } from 'react-router-redux';
import { setIsLoading, setError } from '../../../../../modules/browser/actions';
import openPdf from '../../../../../modules/utils/open-pdf';
import { saveFactFindRequest } from '../../fact-find/actions';

const sandbox = sinon.sandbox.create();
const openPdfStub = sandbox.stub();
__RewireAPI__.__Rewire__('openPdf', openPdfStub);

test('View Report with success', t => {
  let target;
  const solutionId = 999877;
  const data = { data: { solution_id: solutionId } };
  const pdfData = { data: { viewType: 'view' } };
  target = testSaga(downloadReport, pdfData)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionId)
    .select(solutionHasDownloaded)
    .next(true)
    .call(downloadReportCall, solutionId)
    .next(data)
    .call(openPdfStub, { solution_id: solutionId })
    .next()
    .put(setIsLoading(false))
    .next();
  t.true(target.isDone(), 'downloadReport success');

  t.end();
});


test('downloadReport with success', t => {
  let target;
  const solutionId = 999877;
  const data = { data: { solution_id: solutionId } };
  const pdfData = { data: { viewType: 'download' } };
  target = testSaga(downloadReport, pdfData)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionId)
    .select(solutionHasDownloaded)
    .next(false)
    .call(downloadReportCall, solutionId)
    .next(data)
    .call(saveDocument, { solution_id: solutionId })
    .next()
    .put({ type: 'modules/SOLUTION/DOWNLOAD_REPORT_SUCCESS', solutionId: solutionId })
    .next()
    .put(saveFactFindRequest({ doInBackground: true }))
    .next()
    .put(setIsLoading(false))
    .next();
  t.true(target.isDone(), 'downloadReport success');

  t.end();
});

test('downloadReport with success - already downloaded', t => {
  let target;
  const solutionId = 999877;
  const data = { data: { solution_id: solutionId } };
  const pdfData = { data: { viewType: 'download' } };
  target = testSaga(downloadReport, pdfData)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionId)
    .select(solutionHasDownloaded)
    .next(true)
    .call(downloadReportCall, solutionId)
    .next(data)
    .call(saveDocument, { solution_id: solutionId })
    .next()
    .put(setIsLoading(false))
    .next();
  t.true(target.isDone(), 'downloadReport success');

  t.end();
});

test('downloadReport with failure', t => {
  let target;
  const solutionId = 999877;
  const data = { data: { solution_id: solutionId } };
  const pdfData = { data: { viewType: 'download' } };
  target = testSaga(downloadReport, pdfData)
    .next()
    .put(setIsLoading(true))
    .next()
    .select(getSolutionId)
    .next(solutionId)
    .select(solutionHasDownloaded)
    .next(false)
    .call(downloadReportCall, solutionId)
    .next(data)
    .call(saveDocument, { solution_id: solutionId })
    .throw('aaaaaaa')
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'We are currently unable download your report, please try again later.'
      )
    )
    .next();
  t.true(target.isDone(), 'downloadReport failure');

  t.end();
});
