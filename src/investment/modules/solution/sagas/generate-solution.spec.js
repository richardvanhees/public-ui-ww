import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import WealthWizards from '../../../../../modules/wealthwizards';
import {
  responseErrorHandler,
  getCorrelationIdHeader,
} from '../../../../../modules/utils';
import {
  generateSolutionSuccess,
  generateSolutionFailure,
  setSolutionId,
  referral,
} from '../actions';
import generateSolutionTypes from '../types';
import generateSolutionSaga, {
  generateSolution,
  generateSolutionCall,
  getContent,
} from './generate-solution';
import { push } from 'react-router-redux';
import { setError, setIsLoading } from '../../../../../modules/browser/actions';

test('generateSolution', t => {
  let target;

  const testDataHasLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  target = testSaga(generateSolution, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .next({ data: testDataHasLetter })
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_SUCCESS',
      data: {
        solution_id: 999877,
        solution: { referralReasons: [], riskLevel: 4 },
      },
    })
    .next()
    .put(setIsLoading(false))
    .next();
  t.equals(
    !!target.isDone(),
    true,
    'generateSolution with suitability letter success'
  );

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: ['testReason'],
    },
  };

  target = testSaga(generateSolution, {})
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .next({ data: testDataWithoutLetter })
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_SUCCESS',
      data: {
        solution_id: 999877,
        solution: { referralReasons: ['testReason'], riskLevel: 4 },
      },
    })
    .next()
    .put(setIsLoading(false))
    .next()
    .put.resolve(referral())
    .next()
    .put(push(`${WealthWizards.CONTEXT_ROUTE}/referral`))
    .next();
  t.equals(
    !!target.isDone(),
    true,
    'generateSolution without suitability letter success'
  );

  t.end();
});

test('generateSolution', assert => {
  assert.plan(1);

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  const target = testSaga(generateSolution, { redirectPath: '/somewhere' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .next({ data: testDataWithoutLetter })
    .put({
      type: 'modules/SOLUTION/GENERATE_SOLUTION_SUCCESS',
      data: testDataWithoutLetter,
    })
    .next()
    .put(setIsLoading(false))
    .next()
    .put(push(`${WealthWizards.CONTEXT_ROUTE}/somewhere`))
    .next();

  assert.equals(
    !!target.isDone(),
    true,
    'supports redirect path when no referral reasons'
  );
});

test('generateSolution', assert => {
  assert.plan(1);

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  const target = testSaga(generateSolution, { redirectPath: '/somewhere' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .throw({ response: { status: undefined } })
    .put(setIsLoading(false))
    .next()
    .put(
      setError(
        'You appear to be offline, please check your internet connection.'
      )
    )
    .next();

  assert.true(target.isDone(), 'handles offline');
});

test('generateSolution', assert => {
  assert.plan(1);

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  const target = testSaga(generateSolution, { redirectPath: '/somewhere' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .throw({ response: { status: 500 } })
    .put(setIsLoading(false))
    .next()
    .select(getContent)
    .next({
      errorMessages: {
        MAX_SOLUTIONS_REACHED: 'MAX_SOLUTIONS_REACHED error message',
      },
    })
    .put(
      setError(
        'We are currently unable to generate a solution, please try again later.'
      )
    )
    .next();

  assert.true(
    target.isDone(),
    'supports redirect path when no referral reasons'
  );
});

test('generateSolution', assert => {
  assert.plan(1);

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  const target = testSaga(generateSolution, { redirectPath: '/somewhere' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .throw({
      response: {
        status: 400,
        data: {
          code: 'MAX_SOLUTIONS_REACHED',
        },
      },
    })
    .put(setIsLoading(false))
    .next()
    .select(getContent)
    .next({
      errorMessages: {
        MAX_SOLUTIONS_REACHED: 'MAX_SOLUTIONS_REACHED error message',
      },
    })
    .put(setError('MAX_SOLUTIONS_REACHED error message'))
    .next();

  assert.true(
    target.isDone(),
    'supports redirect path when no referral reasons'
  );
});

test('getContent', assert => {
  assert.plan(1);

  assert.equal(
    getContent({
      content: {
        data: {
          investment: 'asdasjkhdgaskjdga',
        },
      },
    }),
    'asdasjkhdgaskjdga',
    'should rip out the content correctly'
  );
});

test('generateSolution invalid amount error passed back', assert => {
  assert.plan(1);

  const testDataWithoutLetter = {
    solution_id: 999877,
    solution: {
      riskLevel: 4,
      referralReasons: [],
    },
  };

  const target = testSaga(generateSolution, { redirectPath: '/somewhere' })
    .next()
    .put(setIsLoading(true))
    .next()
    .call(generateSolutionCall)
    .throw({
      response: {
        status: 400,
        data: {
          code: 'INVALID_INVESTMENT_AMOUNT',
        },
      },
    })
    .put(setIsLoading(false))
    .next()
    .select(getContent)
    .next({
      errorMessages: {
        INVALID_INVESTMENT_AMOUNT: 'INVALID_INVESTMENT_AMOUNT error message',
      },
    })
    .put.resolve(push(`${WealthWizards.CONTEXT_ROUTE}/amount`))
    .next()
    .put(setError('INVALID_INVESTMENT_AMOUNT error message'))
    .next();

  assert.true(
    target.isDone(),
    'supports redirect path when no referral reasons'
  );
});
