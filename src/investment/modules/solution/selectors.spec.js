import test from 'tape';
import { alertBasedReferral, lisaReferral } from './selectors';
import selector from './selectors';

test('SolutionSelector', t => {
  const result = selector({
    solution: {
      data: { paymentStatus: 'FAKESTATUS' },
      referred: false,
      solution_id: 'fakeSolutionId',
    },
  });

  t.deepEqual(result, {
    paymentStatus: 'FAKESTATUS',
    referralStatus: false,
    solutionId: 'fakeSolutionId',
  });

  t.end();
});

test('solution selectors', assert => {
  assert.plan(1);

  assert.true(
    alertBasedReferral({ data: { alerts: ['alert 1'] } }),
    'alertBasedReferral true'
  );
});

test('solution selectors', assert => {
  assert.plan(1);

  assert.false(
    alertBasedReferral({ data: { alerts: ['alert 1'], unlocked: true } }),
    'alertBasedReferral false when unlocked = true despite there being alerts'
  );
});

test('solution selectors', assert => {
  assert.plan(1);

  assert.false(
    alertBasedReferral({ data: { alerts: [] } }),
    'alertBasedReferral false'
  );
});

test('solution selectors', assert => {
  assert.plan(1);

  assert.true(
    lisaReferral({ referralReasons: ['lisa'] }),
    'lisa referral true'
  );
});

test('solution selectors', assert => {
  assert.plan(1);

  assert.false(
    lisaReferral({ referralReasons: ['lisa', 'some_other_reason'] }),
    'lisa referral true'
  );
});
