import * as actions from './actions';
import types from './types';
import test from 'tape';

test('SolutionActions', t => {
  let result;

  result = actions.generateSolutionRequest({ factFind: 'fakeFactFind', redirectPath: 'fakeRedirectPath' });
  t.deepEqual(
    result,
    { type: types.GENERATE_SOLUTION_REQUEST, factFind: 'fakeFactFind', redirectPath: 'fakeRedirectPath' },
    'generateSolutionRequest'
  );

  result = actions.generateSolutionSuccess('fakeData');
  t.deepEqual(
    result,
    { type: types.GENERATE_SOLUTION_SUCCESS, data: 'fakeData' },
    'generateSolutionSuccess'
  );

  result = actions.setPublishedToCrm();
  t.deepEqual(
    result,
    { type: types.PUBLISHED_TO_CRM },
    'setPublishedToCrm'
  );

  result = actions.referral();
  t.deepEqual(
    result,
    { type: types.REFERRAL },
    'referral'
  );

  result = actions.generateSolutionFailure('fakeError');
  t.deepEqual(
    result,
    { type: types.GENERATE_SOLUTION_FAILURE, error: 'fakeError' },
    'generateSolutionFailure'
  );

  result = actions.downloadReportRequest('fakeData');
  t.deepEqual(
    result,
    { type: types.DOWNLOAD_REPORT_REQUEST, data: 'fakeData' },
    'downloadReportRequest'
  );

  result = actions.downloadReportSuccess('fakeSolutionId');
  t.deepEqual(
    result,
    { type: types.DOWNLOAD_REPORT_SUCCESS, solutionId: 'fakeSolutionId' },
    'downloadReportSuccess'
  );

  result = actions.downloadReportFailure('fakeError');
  t.deepEqual(
    result,
    { type: types.DOWNLOAD_REPORT_FAILURE, error: 'fakeError' },
    'downloadReportFailure'
  );

  result = actions.paymentUidRequest();
  t.deepEqual(
    result,
    { type: types.PAYMENT_UID_REQUEST },
    'paymentUidRequest'
  );

  result = actions.publishToCrm('fakeCallback');
  t.deepEqual(
    result,
    { type: types.PUBLISH_TO_CRM, callback: 'fakeCallback' },
    'publishToCrm'
  );

  result = actions.setSolutionPaid();
  t.deepEqual(
    result,
    { type: types.SET_SOLUTION_PAID },
    'setSolutionPaid'
  );

  result = actions.setSolutionPaidSuccess();
  t.deepEqual(
    result,
    { type: types.SET_SOLUTION_PAID_SUCCESS },
    'setSolutionPaidSuccess'
  );

  t.end();
});