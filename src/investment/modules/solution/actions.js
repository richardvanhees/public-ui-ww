import generateSolutionTypes from './types';

export const generateSolutionRequest = ({ factFind, redirectPath }) => ({
  type: generateSolutionTypes.GENERATE_SOLUTION_REQUEST,
  factFind,
  redirectPath,
});

export const generateSolutionSuccess = data => ({
  type: generateSolutionTypes.GENERATE_SOLUTION_SUCCESS,
  data,
});

export const setPublishedToCrm = () => ({
  type: generateSolutionTypes.PUBLISHED_TO_CRM,
});

export const referral = () => ({
  type: generateSolutionTypes.REFERRAL,
});

export const generateSolutionFailure = error => ({
  type: generateSolutionTypes.GENERATE_SOLUTION_FAILURE,
  error,
});

export const downloadReportRequest = data => ({
  type: generateSolutionTypes.DOWNLOAD_REPORT_REQUEST,
  data,
});

export const downloadReportSuccess = solutionId => ({
  type: generateSolutionTypes.DOWNLOAD_REPORT_SUCCESS,
  solutionId,
});

export const downloadReportFailure = error => ({
  type: generateSolutionTypes.DOWNLOAD_REPORT_FAILURE,
  error,
});

export const paymentUidRequest = () => ({
  type: generateSolutionTypes.PAYMENT_UID_REQUEST,
});

export const publishToCrm = callback => ({
  type: generateSolutionTypes.PUBLISH_TO_CRM,
  callback,
});

export const setSolutionPaid = () => ({
  type: generateSolutionTypes.SET_SOLUTION_PAID,
});

export const setSolutionPaidSuccess = () => ({
  type: generateSolutionTypes.SET_SOLUTION_PAID_SUCCESS,
});

