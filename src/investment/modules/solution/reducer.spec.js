import test from 'tape';
import reducer from './reducer.js';
import generateSolutionTypes from './types';
import factFindTypes from '../fact-find/types';

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: generateSolutionTypes.REFERRAL });
  assert.deepEqual(res, { referral: true }, 'REFERRAL');
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    { type: generateSolutionTypes.GENERATE_SOLUTION_REQUEST }
  );
  assert.deepEqual(res, { isLoading: true }, 'GENERATE_SOLUTION_REQUEST');
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.GENERATE_SOLUTION_SUCCESS,
      data: { solution_id: 1, solution: { foo: 'baa' }, submittedToIO: true },
    }
  );
  assert.deepEqual(
    res,
    {
      solution_id: 1,
      isLoading: false,
      data: { foo: 'baa', submittedToIO: true },
      referralReasons: [],
    },
    'GENERATE_SOLUTION_SUCCESS'
  );
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.GENERATE_SOLUTION_SUCCESS,
      data: {
        solution_id: 1,
        solution: { foo: 'baa', referralReasons: ['lisa'] },
        submittedToIO: true,
      },
    }
  );
  assert.deepEqual(
    res,
    {
      solution_id: 1,
      isLoading: false,
      data: { foo: 'baa', submittedToIO: true, referralReasons: ['lisa'] },
      referralReasons: ['lisa'],
    },
    'GENERATE_SOLUTION_SUCCESS with referrals'
  );
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.GENERATE_SOLUTION_FAILURE,
      error: 'fakeError',
    }
  );
  assert.deepEqual(
    res,
    { isLoading: false, error: 'fakeError' },
    'GENERATE_SOLUTION_FAILURE'
  );
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.DOWNLOAD_REPORT_REQUEST,
      viewType: 'fakeViewType',
    }
  );
  assert.deepEqual(
    res,
    { isLoading: true, viewType: 'fakeViewType' },
    'DOWNLOAD_REPORT_REQUEST'
  );
});
test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    { type: generateSolutionTypes.DOWNLOAD_REPORT_SUCCESS }
  );
  assert.deepEqual(res, { isLoading: false }, 'DOWNLOAD_REPORT_SUCCESS');
});
test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    { type: generateSolutionTypes.DOWNLOAD_REPORT_FAILURE, error: 'fakeError' }
  );
  assert.deepEqual(
    res,
    { isLoading: false, error: 'fakeError' },
    'DOWNLOAD_REPORT_FAILURE'
  );
});
test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: factFindTypes.INITIAL_LOAD,
      solution: {
        solution_id: 1,
        solution_creation: 'fakeDate',
        risk_level: 4,
        referral: false,
        investmentIsaAllocations: 'fake investmentIsaAllocations',
        paymentStatus: null,
        alerts: [],
        submittedToIO: true,
        unlocked: true,
      },
    }
  );
  assert.deepEqual(
    res,
    {
      solution_id: 1,
      data: {
        date_generated: 'fakeDate',
        riskLevel: 4,
        investmentIsaAllocations: 'fake investmentIsaAllocations',
        paymentStatus: null,
        alerts: [],
        submittedToIO: true,
        unlocked: true,
      },
    },
    'happy day initial load'
  );
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: factFindTypes.INITIAL_LOAD,
      solution: {
        solution_id: 1,
        solution_creation: 'fakeDate',
        risk_level: 4,
        referral: true,
        referralReasons: [],
      },
    }
  );
  assert.deepEqual(res, { referral: true, referralReasons: [] }, 'referral');
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: factFindTypes.INITIAL_LOAD,
    }
  );
  assert.deepEqual(res, {}, 'no solution object');
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.PUBLISHED_TO_CRM,
    }
  );
  assert.deepEqual(res, { data: { submittedToIO: true } }, 'PUBLISHED_TO_CRM');
});

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: generateSolutionTypes.SET_SOLUTION_PAID_SUCCESS,
    }
  );
  assert.deepEqual(
    res,
    { data: { paymentStatus: 'PAID' } },
    'SET_SOLUTION_PAID_SUCCESS'
  );
});
