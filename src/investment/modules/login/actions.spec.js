import test from 'tape';
import * as actions from './actions.js';

test('login actions', t => {
  let result;

  result = actions.login('fakeUser', 'fakePassword');
  t.deepEqual(
    result,
    { type: actions.LOGIN, username: 'fakeUser', password: 'fakePassword' },
    'login'
  );

  result = actions.setError('fakeError');
  t.deepEqual(
    result,
    { type: actions.SET_ERROR, error: 'fakeError' },
    'setError'
  );

  result = actions.logout(false, 'redirectUrl');
  t.deepEqual(
    result,
    {
      type: actions.LOGOUT,
      redirectAfterLoginIfPossible: false,
      redirectUrl: 'redirectUrl',
    },
    'logout'
  );

  result = actions.toggleAfterReturnMessage(true);
  t.deepEqual(
    result,
    { type: actions.TOGGLE_AFTER_RETURN_MESSAGE, active: true },
    'toggleAfterReturnMessage'
  );

  t.end();
});
