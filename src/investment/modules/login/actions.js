export const LOGIN = 'login/LOGIN';
export const SET_ERROR = 'login/SET_ERROR';
export const LOGOUT = 'login/LOGOUT';
export const TOGGLE_AFTER_RETURN_MESSAGE = 'login/TOGGLE_AFTER_RETURN_MESSAGE';
export const RESEND_ACTIVATION_EMAIL = 'login/SEND_ACTIVATION_EMAIL';

export const login = (username, password) => ({
  type: LOGIN,
  username,
  password,
});

export const setError = error => ({
  type: SET_ERROR,
  error,
});

export const logout = (redirectAfterLoginIfPossible = false, redirectUrl) => ({
  type: LOGOUT,
  redirectAfterLoginIfPossible,
  redirectUrl,
});

export const toggleAfterReturnMessage = active => ({
  type: TOGGLE_AFTER_RETURN_MESSAGE,
  active,
});

export const resendActivationEmail = email => ({
  type: RESEND_ACTIVATION_EMAIL,
  email,
});
