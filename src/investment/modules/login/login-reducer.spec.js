import test from 'tape';
import reducer from './login-reducer.js';
import * as loginTypes from './actions';

test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: loginTypes.SET_ERROR, error: 'fakeError' });
  assert.deepEqual(res, { error: 'fakeError' }, 'SET_ERROR');
});
test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer({}, { type: loginTypes.TOGGLE_AFTER_RETURN_MESSAGE, active: true });
  assert.deepEqual(res, { afterReturnActive: true }, 'TOGGLE_AFTER_RETURN_MESSAGE');
});
test('solution reducer', assert => {
  assert.plan(1);

  const res = reducer({ afterReturnActive: true }, { type: loginTypes.TOGGLE_AFTER_RETURN_MESSAGE });
  assert.deepEqual(res, { afterReturnActive: false }, 'TOGGLE_AFTER_RETURN_MESSAGE');
});