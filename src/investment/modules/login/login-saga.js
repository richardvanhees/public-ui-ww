import { call, put, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../../../modules/wealthwizards';
import { LOGIN, setError, toggleAfterReturnMessage } from './actions';
import { setIsFullLoading } from '../../../../modules/browser/actions';
import axios from 'axios';
import { getFactFindRequest } from '../fact-find/actions';
import { setItem } from '../../../../modules/utils/local-storage';
import R from 'ramda';

export const loginCall = (username, password) =>
  axios({
    method: 'POST',
    url: `${WealthWizards.AUTH_SVC_URL}/v1/oauth/token`,
    data: { username, password },
    headers: {
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
    timeout: WealthWizards.CLIENT_REQUEST_TIMEOUT,
  });

function getUrlParameter(paramName) {
  const name = paramName.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  const regex = new RegExp(`[\\?&]${name}=([^&#]*)`);
  const results = regex.exec(location.search);
  return results === null
    ? ''
    : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

export function* login({ username, password }) {
  try {
    const referParam = getUrlParameter('refer');

    yield put(setIsFullLoading(true));

    const { data } = yield call(loginCall, username, password);

    setItem('user_token', data.jwt);
    setItem('refresh_token', data.refresh_token);

    yield put(toggleAfterReturnMessage(true));

    // true param tells saga that it is fetching fact find after a successful
    // login and not via a page refresh
    return yield put(getFactFindRequest(referParam, true));
  } catch (error) {
    yield put(setIsFullLoading(false));

    const statusCode = R.path(['response', 'status'])(error);

    if (statusCode === 401 || statusCode === 400) {
      return yield put(setError("Your email and / or password isn't right"));
    }

    if (!statusCode) {
      return yield put(
        setError(
          'You appear to be offline, please check your internet connection.'
        )
      );
    }

    return yield put(
      setError(
        'We are currently unable to sign you in, please try again later.'
      )
    );
  }
}

export default function* loginSaga() {
  yield takeLatest(LOGIN, login);
}
