import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { login, loginCall } from './login-saga';
import { getFactFindRequest } from '../fact-find/actions';
import { setIsFullLoading } from '../../../../modules/browser/actions';
import { setError, toggleAfterReturnMessage } from './actions';

test('login saga', t => {
  const backupLocation = global.location;
  global.location = { search: '?refer=testurl' };

  const target = testSaga(login, {
    username: 'test@support.com',
    password: 'password',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .call(loginCall, 'test@support.com', 'password')
    .next({ data: { jwt: 'jwt', refresh_token: 'refresh_token' } })
    .put(toggleAfterReturnMessage(true))
    .next()
    .put(getFactFindRequest('testurl', true))
    .next();

  t.true(target.isDone(), 'user logged in');

  t.end();
  global.location = backupLocation;
});

test('login saga', t => {
  const target = testSaga(login, {
    username: 'test@support.com',
    password: 'password',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .call(loginCall, 'test@support.com', 'password')
    .throw({ response: { status: 401 } })
    .put(setIsFullLoading(false))
    .next()
    .put(setError("Your email and / or password isn't right"))
    .next();

  t.true(target.isDone(), 'user logged in failure - 401');

  t.end();
});

test('login saga', t => {
  const target = testSaga(login, {
    username: 'test@support.com',
    password: 'password',
  })
    .next()
    .put(setIsFullLoading(true))
    .next()
    .call(loginCall, 'test@support.com', 'password')
    .throw({ response: { status: 500 } })
    .put(setIsFullLoading(false))
    .next()
    .put(
      setError(
        'We are currently unable to sign you in, please try again later.'
      )
    )
    .next();

  t.true(target.isDone(), 'user logged in failure - 500');

  t.end();
});
