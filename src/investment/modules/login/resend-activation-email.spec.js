import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { resend, callToResendEmail} from './resend-activation-email'


test('resend email saga SUCCESS', t => {
  const email = {email: 'email@email.com'};
  const target = testSaga(resend, email)
    .next()
    .call(callToResendEmail, 'email@email.com')
    .next()
  t.true(target.isDone(), 'email resent');

  t.end();
});
