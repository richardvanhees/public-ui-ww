import { fork, put, takeLatest, select } from 'redux-saga/effects';
import WealthWizards from '../../../../modules/wealthwizards';
import { LOGOUT } from './actions';
import { setIsLoading } from '../../../../modules/browser/actions';
import axios from 'axios';
import { resetState } from '../fact-find/actions';
import { push } from 'react-router-redux';
import { getItem, removeItem } from '../../../../modules/utils/local-storage';

export const logoutCall = jwt =>
  axios({
    method: 'POST',
    url: `${WealthWizards.AUTH_SVC_URL}/v1/oauth/revoke-token`,
    data: { jwt },
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  });

export const getRouting = state => state.routing;

export function* logout({ redirectAfterLoginIfPossible, redirectUrl }) {
  if (getItem('user_token')) {
    yield put(setIsLoading(true));

    try {
      yield fork(logoutCall, getItem('user_token'));
    } catch (e) {
      // deliberately swallow, we try and revoke if possible
    }

    removeItem('user_token');
    removeItem('refresh_token');
    removeItem('first_login');
  }

  let referParam = '';

  if (redirectAfterLoginIfPossible) {
    const routing = yield select(getRouting);
    referParam = `?refer=${routing.location.pathname}${
      routing.location.search
    }`;
  }

  // see magic in modules/boilerplate/configure-store.js
  yield put(resetState());

  if (redirectUrl) {
    return yield put(push(redirectUrl));
  }

  return yield put(push(`${WealthWizards.CONTEXT_ROUTE}/login${referParam}`));
}

export default function* logoutSaga() {
  yield takeLatest(LOGOUT, logout);
}
