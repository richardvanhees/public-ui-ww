import { createStructuredSelector } from 'reselect';
import R from 'ramda';

const loginError = state => R.path(['login', 'error'], state);

export default createStructuredSelector({
  loginError,
});
