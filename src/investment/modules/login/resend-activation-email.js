import { call, put, takeLatest } from 'redux-saga/effects';
import { RESEND_ACTIVATION_EMAIL, setError } from './actions';
import { setIsFullLoading } from '../../../../modules/browser/actions';
import WealthWizards from '../../../../modules/wealthwizards';
import axios from 'axios';

export const callToResendEmail = email =>
  axios({
    method: 'POST',
    url: `${WealthWizards.AUTH_SVC_URL}/v1/resend-activation-email`,
    data: { username: email },
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  });


export function* resend({ email }) {
  try {
    return yield call(callToResendEmail, email);
  } catch (error) {
    yield put(setIsFullLoading(false));
    return yield put(setError('Failed to resend activation email'));
  }
}

export default function* resendSaga() {
  yield takeLatest(RESEND_ACTIVATION_EMAIL, resend);
}
