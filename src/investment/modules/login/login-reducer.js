import {
  SET_ERROR,
  TOGGLE_AFTER_RETURN_MESSAGE,
} from './actions';

const initialState = {
  error: '',
  afterReturnActive: false,
};

const reducer = {
  [SET_ERROR]: (state, { error }) => ({
    ...state,
    error,
  }),
  [TOGGLE_AFTER_RETURN_MESSAGE]: (state, { active }) => ({
    ...state,
    afterReturnActive: typeof active === 'boolean' ? active : !state.afterReturnActive,
  }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
