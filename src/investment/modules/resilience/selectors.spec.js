import test from 'tape';
import selector, { getDiscretionaryIncome } from './selectors.js';

test('ResilienceSelector', t => {
  const result = selector({
    resilience: {
      data: 'testUserInput',
      profile: 'testProfile',
    },
  });

  t.deepEqual(result, {
    userInput: 'testUserInput',
    profile: 'testProfile',
  });

  t.end();
});

test('ResilienceSelector', t => {
  const result = getDiscretionaryIncome({
    resilience: {
      data: {
        investment_resilience__monthly_income: 12,
        investment_resilience__monthly_expenses: 9,
      },
    },
  });

  t.equal(result, 36, 'getDiscretionaryIncome');

  t.end();
});

test('ResilienceSelector', t => {
  const result = getDiscretionaryIncome({
    resilience: {
      data: {
        investment_resilience__monthly_income: 12,
        investment_resilience__monthly_expenses: 13,
      },
    },
  });

  t.equal(
    result,
    0,
    'getDiscretionaryIncome returns 0 when expenses higher than income'
  );

  t.end();
});
