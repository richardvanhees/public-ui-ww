import { call, put, select, takeLatest } from 'redux-saga/effects';

import { post } from '../../../../modules/utils/AxiosWrapper';
import {
  generateResilienceProfileSuccess,
  generateResilienceProfileFailed,
  generateResilienceProfileStart,
} from './actions';
import resilienceTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';
import { getDiscretionaryIncome } from './selectors';

export const getResilienceInput = state => state.resilience.data;

export const callToGenerateResilienceProfile = data =>
  post({
    route: '/v1/financial-resilience',
    data,
  });

export function* generateResilienceProfile() {
  try {
    yield put(setIsLoading(true));
    yield put(generateResilienceProfileStart());
    const userInput = yield select(getResilienceInput);
    const formattedInput = {};
    Object.keys(userInput).forEach(answerKey => {
      formattedInput[answerKey.replace(/investment_resilience__/g, '')] =
        userInput[answerKey];
    });

    // getDiscretionaryIncome
    formattedInput.discretionary_income = getDiscretionaryIncome({
      resilience: {
        data: userInput,
      },
    });

    const profile = yield call(callToGenerateResilienceProfile, formattedInput);
    yield put(setIsLoading(false));
    yield put(generateResilienceProfileSuccess(profile.data));
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(generateResilienceProfileFailed());
  }
}

export default function* generateResilienceProfileSaga() {
  yield takeLatest(
    resilienceTypes.GENERATE_RESILIENCE_PROFILE,
    generateResilienceProfile
  );
}
