import {
  MAP_FROM_SERVER,
  RESET_RISK_SECTIONS,
} from '../../modules/fact-find/actions';
import resilienceTypes from './types';

const initialState = {
  data: {},
  profile: {},
  override: null,
};

const resilienceFieldUpdated = (state, action) => ({
  ...state,
  data: {
    ...state.data,
    [action.name]: action.value,
  },
});

const mapFromServer = (state, action) => ({
  ...state,
  profile: action.data.resilience.profile,
  override: action.data.resilience.override,
  data: {
    ...state.data,
    ...action.data.resilience.data,
  },
});

const resetSection = () => initialState;

const generateResilienceProfileStart = state => ({
  ...state,
  isLoading: true,
  override: null,
  profile: {},
});

const generateResilienceProfileSuccess = (state, action) => ({
  ...state,
  isLoading: false,
  profile: action.profile,
});

const generateResilienceProfileFailed = state => ({
  ...state,
  isLoading: false,
});

const setManualOverride = (state, action) => ({
  ...state,
  override: action.override,
});

const reducer = {
  [resilienceTypes.RESILIENCE_FIELD_UPDATED]: resilienceFieldUpdated,
  [resilienceTypes.RESET_RESILIENCE]: resetSection,
  [RESET_RISK_SECTIONS]: resetSection,
  [resilienceTypes.RESILIENCE_MANUAL_OVERRIDE]: setManualOverride,

  [resilienceTypes.GENERATE_RESILIENCE_PROFILE_START]: generateResilienceProfileStart,
  [resilienceTypes.GENERATE_RESILIENCE_PROFILE_SUCCESS]: generateResilienceProfileSuccess,
  [resilienceTypes.GENERATE_RESILIENCE_PROFILE_FAILED]: generateResilienceProfileFailed,
  [MAP_FROM_SERVER]: mapFromServer,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
