import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('ResilienceActions', t => {
  let result;

  result = actions.generateResilienceProfileStart();
  t.deepEqual(
    result,
    { type: types.GENERATE_RESILIENCE_PROFILE_START },
    'generateResilienceProfileStart'
  );

  result = actions.generateResilienceProfile();
  t.deepEqual(
    result,
    { type: types.GENERATE_RESILIENCE_PROFILE },
    'generateResilienceProfile'
  );

  result = actions.generateResilienceProfileSuccess('testData');
  t.deepEqual(
    result,
    { type: types.GENERATE_RESILIENCE_PROFILE_SUCCESS, profile: 'testData' },
    'generateResilienceProfileSuccess'
  );

  result = actions.generateResilienceProfileFailed();
  t.deepEqual(
    result,
    { type: types.GENERATE_RESILIENCE_PROFILE_FAILED },
    'generateResilienceProfileFailed'
  );

  t.end();
});

test('ResilienceActions', assert => {
  assert.plan(1);
  const result = actions.updateProgress();
  assert.deepEqual(
    result,
    { type: types.RESILIENCE_UPDATE_PROGRESS },
    'updateProgress'
  );
});
