import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import generateResilienceProfileSaga, {
  getResilienceInput,
  callToGenerateResilienceProfile,
  generateResilienceProfile,
} from './sagas';
import atrTypes from './types';
import { setIsLoading } from '../../../../modules/browser/actions';

test('generateResilienceProfileSaga', t => {
  let target;

  const testData = {
    investment_resilience__foo: 'ba',
    discretionary_income: 0,
  };
  const formattedTestData = { foo: 'ba', discretionary_income: 0 };
  const testResponse = { data: 'atrProfile' };
  target = testSaga(generateResilienceProfile)
    .next()
    .put(setIsLoading(true))
    .next()
    .put({ type: atrTypes.GENERATE_RESILIENCE_PROFILE_START })
    .next()
    .select(getResilienceInput)
    .next(testData)
    .call(callToGenerateResilienceProfile, formattedTestData)
    .next(testResponse)
    .put(setIsLoading(false))
    .next()
    .put({
      type: atrTypes.GENERATE_RESILIENCE_PROFILE_SUCCESS,
      profile: testResponse.data,
    })
    .next();
  t.equals(!!target.isDone(), true, 'generateResilienceProfile success');

  t.end();
});
