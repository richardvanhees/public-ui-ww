import { createStructuredSelector } from 'reselect';
import R from 'ramda';
import rounder from '../../../../modules/utils/round-decimals';

const userInput = state => R.path(['resilience', 'data'], state);
const profile = state => R.path(['resilience', 'profile'], state);

export const getDiscretionaryIncome = state => {
  const monthlyIncome = R.pathOr(0, [
    'resilience',
    'data',
    'investment_resilience__monthly_income',
  ])(state);
  const monthlyExpenses = R.pathOr(0, [
    'resilience',
    'data',
    'investment_resilience__monthly_expenses',
  ])(state);
  return rounder(Math.max((monthlyIncome - monthlyExpenses) * 12, 0));
};

export default createStructuredSelector({
  userInput,
  profile,
});
