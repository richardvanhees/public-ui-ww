import resilienceTypes from './types';

export const generateResilienceProfile = () => ({
  type: resilienceTypes.GENERATE_RESILIENCE_PROFILE,
});

export const generateResilienceProfileStart = () => ({
  type: resilienceTypes.GENERATE_RESILIENCE_PROFILE_START,
});

export const generateResilienceProfileSuccess = profile => ({
  type: resilienceTypes.GENERATE_RESILIENCE_PROFILE_SUCCESS,
  profile,
});

export const generateResilienceProfileFailed = () => ({
  type: resilienceTypes.GENERATE_RESILIENCE_PROFILE_FAILED,
});

export const resetResilienceSection = () => ({
  type: resilienceTypes.RESET_RESILIENCE,
});

export const resilienceManualOverride = override => ({
  type: resilienceTypes.RESILIENCE_MANUAL_OVERRIDE,
  override,
});

export const handleFieldChange = (name, value) => ({
  type: resilienceTypes.RESILIENCE_FIELD_UPDATED,
  name,
  value,
});

export const updateProgress = () => ({
  type: resilienceTypes.RESILIENCE_UPDATE_PROGRESS,
});
