import test from 'tape';
import reducer from './reducer.js';
import atrTypes from './types';
import { MAP_FROM_SERVER, RESET_RISK_SECTIONS } from '../fact-find/actions';

const initialState = {
  data: {},
  profile: {},
  override: null,
};

test('ResilienceReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: atrTypes.RESILIENCE_FIELD_UPDATED,
      name: 'testName',
      value: 'testValue',
    }
  );
  t.deepEqual(
    result,
    {
      data: {
        testName: 'testValue',
      },
    },
    'RESILIENCE_FIELD_UPDATED'
  );

  result = reducer(
    {},
    {
      type: atrTypes.RESET_RESILIENCE,
    }
  );
  t.deepEqual(result, initialState, 'RESET_RESILIENCE');

  result = reducer(
    {},
    {
      type: atrTypes.RESILIENCE_MANUAL_OVERRIDE,
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    }
  );
  t.deepEqual(
    result,
    {
      override: {
        key: 'testKey',
        profile: 'testProfile',
      },
    },
    'RESILIENCE_MANUAL_OVERRIDE'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_RESILIENCE_PROFILE_START,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: true,
      override: null,
      profile: {},
    },
    'GENERATE_RESILIENCE_PROFILE_START'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_RESILIENCE_PROFILE_SUCCESS,
      profile: 'testProfile',
    }
  );
  t.deepEqual(
    result,
    {
      profile: 'testProfile',
      isLoading: false,
    },
    'GENERATE_RESILIENCE_PROFILE_SUCCESS'
  );

  result = reducer(
    {},
    {
      type: atrTypes.GENERATE_RESILIENCE_PROFILE_FAILED,
    }
  );
  t.deepEqual(
    result,
    {
      isLoading: false,
    },
    'GENERATE_RESILIENCE_PROFILE_FAILED'
  );

  t.end();
});

test('ResilienceReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: MAP_FROM_SERVER,
      data: {
        resilience: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(
    res,
    {
      data: {
        foo: 'baa',
      },
      profile: 'fake profile',
      override: 'fake override',
    },
    'maps from server'
  );
});

test('ResilienceReducer', assert => {
  assert.plan(1);

  const res = reducer(
    {},
    {
      type: RESET_RISK_SECTIONS,
      data: {
        resilience: {
          data: {
            foo: 'baa',
          },
          profile: 'fake profile',
          override: 'fake override',
        },
      },
    }
  );

  assert.deepEqual(res, initialState, 'RESET_RISK_SECTIONS');
});
