import R from 'ramda';

export const validateForm = (content, finalChecks) =>
  R.compose(
    unansweredQuestion => ({ ok: unansweredQuestion === undefined }),
    R.find(question => !R.contains(finalChecks[question.id], ['yes', 'no'])),
    R.pathOr([], ['questions'])
  )(content);

export const userShouldBeReferred = R.anyPass([
  R.propEq('investment__money_to_invest_readily_available', 'no'),
  R.propEq('investment__borrowing_above_apr_level', 'yes'),
]);

export const savingsMinimum = R.compose(
  R.multiply(3),
  R.pathOr(0, ['data', 'investment_resilience__monthly_expenses'])
);

export const userWantsToWait = R.anyPass([
  R.propEq('investment__reconsider_pay_into_pension', 'pause'),
]);

export const multiQuestionHelper = R.compose(
  R.mapObjIndexed(([key, value]) => ({
    answerId: value ? Number(value === 'no' || value === 'continue') : value,
    answerKey: key,
  })),
  R.filter(i => !R.isNil(i[1])),
  R.toPairs
);
