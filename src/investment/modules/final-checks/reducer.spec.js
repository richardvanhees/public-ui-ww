import test from 'tape';
import deepFreeze from 'deep-freeze';
import {
  FINAL_CHECKS_FIELD_UPDATED, FINAL_CHECKS_REMOVE_KEY,
  FINAL_CHECKS_SET_COMPLETED_AT,
} from './types';
import reducer from './reducer';
import { MAP_FROM_SERVER } from '../fact-find/actions';

test('final checks reducer', assert => {
  assert.plan(1);

  const fakeState = {};
  const target = reducer;

  const actual = target(fakeState, {
    name: 'cheese',
    value: 'wine',
  });
  const expected = fakeState;

  assert.equal(
    actual,
    expected,
    'should return the state when no reducer is defined'
  );
});

test('final checks reducer', assert => {
  assert.plan(1);

  const fakeState = {};
  const target = reducer;

  const actual = target(fakeState, {
    type: FINAL_CHECKS_FIELD_UPDATED,
    name: 'cheese',
    value: 'wine',
  });
  const expected = { cheese: 'wine' };

  assert.deepEqual(
    actual,
    expected,
    'should return the state with name and value set'
  );
});

test('final checks reducer', assert => {
  assert.plan(1);

  const fakeState = {};
  const target = reducer;

  const actual = target(fakeState, {
    type: MAP_FROM_SERVER,
    data: { finalChecks: { foo: 'baa' } },
  });
  const expected = { foo: 'baa' };

  assert.deepEqual(actual, expected, 'maps from server');
});

test('final checks reducer', assert => {
  assert.plan(1);

  const fakeState = {};
  const target = reducer;

  const actual = target(fakeState, {
    type: FINAL_CHECKS_SET_COMPLETED_AT,
    date: 'date',
  });
  const expected = { investment__completed_at: 'date' };

  assert.deepEqual(actual, expected, 'FINAL_CHECKS_SET_COMPLETED_AT');
});

test('final checks reducer', assert => {
  assert.plan(1);

  const fakeState = {
    foo: 'baa',
    bar: 'meh',
  };
  const target = reducer;

  const actual = target(fakeState, {
    type: FINAL_CHECKS_REMOVE_KEY,
    key: 'bar',
  });
  const expected = { foo: 'baa' };

  assert.deepEqual(actual, expected, 'removes key');
});
