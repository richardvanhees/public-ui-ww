const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('final checks - selectors', assert => {
  assert.plan(1);

  const { validateForm } = proxyquire('./selectors', {});

  const res = validateForm(
    {
      questions: [
        {
          id: 'testQ',
        },
      ],
    },
    { testQ: 'yes' }
  );

  assert.deepEqual(res, { ok: true }, 'form is valid, yes is answered');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { validateForm } = proxyquire('./selectors', {});

  const res = validateForm(
    {
      questions: [
        {
          id: 'testQ',
        },
      ],
    },
    { testQ: 'no' }
  );

  assert.deepEqual(res, { ok: true }, 'form is valid, no is answered');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { validateForm } = proxyquire('./selectors', {});

  const res = validateForm(
    {
      questions: [
        {
          id: 'testQ',
        },
      ],
    },
    { testQ: null }
  );

  assert.deepEqual(res, { ok: false }, 'form is invalid, not answered');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { userShouldBeReferred } = proxyquire('./selectors', {});

  const res = userShouldBeReferred({
    investment__money_to_invest_readily_available: 'no',
    investment__min_amount_in_savings_available: 'yes',
    investment__borrowing_above_apr_level: 'no',
    investment__maxing_your_employer_pension_scheme: 'yes',
  });

  assert.true(res, 'investment__money_to_invest_readily_available no');
});


test('final checks - selectors', assert => {
  assert.plan(1);

  const { userShouldBeReferred } = proxyquire('./selectors', {});

  const res = userShouldBeReferred({
    investment__money_to_invest_readily_available: 'yes',
    investment__min_amount_in_savings_available: 'yes',
    investment__borrowing_above_apr_level: 'yes',
    investment__maxing_your_employer_pension_scheme: 'yes',
  });

  assert.true(res, 'investment__borrowing_above_apr_level yes');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { userShouldBeReferred } = proxyquire('./selectors', {});

  const res = userShouldBeReferred({
    investment__money_to_invest_readily_available: 'yes',
    investment__min_amount_in_savings_available: 'no',
    investment__borrowing_above_apr_level: 'no',
    investment__maxing_your_employer_pension_scheme: 'yes',
  });

  assert.false(res, 'not referred');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { savingsMinimum } = proxyquire('./selectors', {});

  const res = savingsMinimum({
    data: {
      investment_resilience__monthly_expenses: 3,
    },
  });

  assert.equal(res, 9, 'savingsMinimum is 9');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { savingsMinimum } = proxyquire('./selectors', {});

  const res = savingsMinimum({
    data: [{}],
  });

  assert.equal(res, 0, 'savingsMinimum is not defined, 0 returned');
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { multiQuestionHelper } = proxyquire('./selectors', {});
  const res = multiQuestionHelper({
    foo__first: 'no',
    foo__second: 'no',
    foo__third: null,
    foo__fourth: 'yes',
  });

  const expected = {
    0: {
      answerId: 1,
      answerKey: 'foo__first',
    },
    1: {
      answerId: 1,
      answerKey: 'foo__second',
    },
    2: {
      answerId: 0,
      answerKey: 'foo__fourth',
    },
  };

  assert.deepEqual(
    res,
    expected,
    'multi question helper builds correct object'
  );
  assert.end();
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { userWantsToWait } = proxyquire('./selectors', {});

  const res = userWantsToWait({
    investment__reconsider_pay_into_pension: 'pause',
  });

  assert.true(res, 'user wants to wait');
  assert.end();
});

test('final checks - selectors', assert => {
  assert.plan(1);

  const { userWantsToWait } = proxyquire('./selectors', {});

  const res = userWantsToWait({
    investment__reconsider_pay_into_pension: 'continue',
  });

  assert.false(res, 'user wants to continue');
  assert.end();
});
