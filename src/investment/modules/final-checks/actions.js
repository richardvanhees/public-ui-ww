import { FINAL_CHECKS_FIELD_UPDATED, FINAL_CHECKS_SET_COMPLETED_AT, FINAL_CHECKS_REMOVE_KEY } from './types';

export const fieldUpdated = (name, value) => ({
  type: FINAL_CHECKS_FIELD_UPDATED,
  name,
  value,
});

export const setCompletedAt = date => ({
  type: FINAL_CHECKS_SET_COMPLETED_AT,
  date,
});

export const removeKey = key => ({
  type: FINAL_CHECKS_REMOVE_KEY,
  key,
});

