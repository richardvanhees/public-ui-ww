const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('final checks - actions', assert => {
  assert.plan(1);

  const { fieldUpdated } = proxyquire('./actions', {});

  const res = fieldUpdated('name', 'value');

  assert.deepEqual(
    res,
    {
      name: 'name',
      type: 'modules/FINAL_CHECKS/FIELD_UPDATED',
      value: 'value',
    },
    'fieldUpdated'
  );
});

test('final checks - actions', assert => {
  assert.plan(1);

  const { setCompletedAt } = proxyquire('./actions', {});

  const res = setCompletedAt('date');

  assert.deepEqual(
    res,
    {
      type: 'modules/FINAL_CHECKS/SET_COMPLETED_AT',
      date: 'date',
    },
    'setCompletedAt'
  );
});

test('final checks - actions', assert => {
  assert.plan(1);

  const { removeKey } = proxyquire('./actions', {});

  const res = removeKey('fake__key');

  assert.deepEqual(
    res,
    {
      type: 'modules/FINAL_CHECKS/REMOVE_KEY',
      key: 'fake__key',
    },
    'removeKey'
  );
});