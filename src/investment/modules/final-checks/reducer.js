import R from 'ramda';
import {
  FINAL_CHECKS_FIELD_UPDATED,
  FINAL_CHECKS_SET_COMPLETED_AT,
  FINAL_CHECKS_REMOVE_KEY,
} from './types';
import { MAP_FROM_SERVER } from '../../modules/fact-find/actions';

const initialState = {};

const fieldUpdated = (state, action) => ({
  ...state,
  [action.name]: action.value,
});

const mapFromServer = (state, { data }) => ({
  ...state,
  ...data.finalChecks,
});

const setCompletedAt = (state, { date }) => ({
  ...state,
  investment__completed_at: date,
});

const removeKey = (state, { key }) => R.omit(key, state);

const reducer = {
  [FINAL_CHECKS_FIELD_UPDATED]: fieldUpdated,
  [MAP_FROM_SERVER]: mapFromServer,
  [FINAL_CHECKS_SET_COMPLETED_AT]: setCompletedAt,
  [FINAL_CHECKS_REMOVE_KEY]: removeKey,
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
