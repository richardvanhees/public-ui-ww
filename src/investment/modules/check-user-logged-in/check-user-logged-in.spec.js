import test from 'tape';
import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import checkUserLoggedIn, { __RewireAPI__ } from './';
import DashboardContainer from '../../components/pages/dashboard/DashboardContainer';
import { push } from 'react-router-redux';

import sinon from 'sinon';

test('user logged in so dashboard is rendered', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const pushStub = sandbox.stub();

  localStorage.setItem('user_token', 'a jwt');
  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => {
      redirect: pushStub;
    },
  };

  const Target = checkUserLoggedIn(DashboardContainer);

  const actual = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  assert.equal(
    pushStub.callCount,
    0,
    'redirect is not called as a jwt is present'
  );

  __RewireAPI__.__ResetDependency__('push');
  localStorage.removeItem('user_token');
});

test('user NOT logged in so redirect is dispatched', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const pushStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => {},
  };

  const Target = checkUserLoggedIn('landing')(DashboardContainer);

  const actual = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  assert.equal(
    pushStub.callCount,
    1,
    'redirect IS called as a jwt is NOT present'
  );

  __RewireAPI__.__ResetDependency__('push');
});

test('user logged in so redirect is dispatched when trying to open login page and inverse true', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();
  const pushStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushStub);

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => {},
  };

  localStorage.setItem('user_token', '12345');

  const Target = checkUserLoggedIn('dashboard', true)(DashboardContainer);

  const actual = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  assert.equal(
    pushStub.callCount,
    1,
    'redirect IS called as a jwt is NOT present'
  );

  localStorage.removeItem('user_token');

  __RewireAPI__.__ResetDependency__('push');
});
