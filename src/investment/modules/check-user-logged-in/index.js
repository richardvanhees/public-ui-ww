import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { compose, branch } from 'recompose';
import R from 'ramda';
import withLifecycle from '@hocs/with-lifecycle';
import { getItem } from '../../../../modules/utils/local-storage';

const userCouldBeStillLoggedIn = inverse => () => {
  const exists = R.is(String, getItem('user_token'));
  return inverse ? exists : !exists;
};

export default (redirectUrl, inverse = false) =>
  compose(
    connect(null, dispatch => ({
      redirect: () => dispatch(push(redirectUrl)),
    })),
    branch(
      userCouldBeStillLoggedIn(inverse),
      withLifecycle({
        onDidMount({ redirect }) {
          redirect();
        },
      })
    )
  );
