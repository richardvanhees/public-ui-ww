import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import AppWrapperComponent, { __RewireAPI__ } from './AppWrapperComponent';
import Spinner from '../../../modules/elements/Spinner';

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const callToActionFunctionStub = sandbox.stub();
  const dispatchActionStub = sandbox.stub();
  const resetAlertStub = sandbox.stub();

  const fakeRequiredProps = {
    children: <div />,
    history: { location: { pathname: '' } },
    resetError: () => ({}),
    errorMsg: null,
    showError: false,
    loading: false,
    isLoggedIn: true,
    logout: () => ({}),
    login: () => ({}),
    isMobile: false,
    alert: {
      title: 'title',
      message: 'message',
      callToActionLabel: 'callToActionLabel',
      callToActionFunction: callToActionFunctionStub,
    },
    resetAlert: resetAlertStub,
    dispatchAction: dispatchActionStub,
  };

  __RewireAPI__.__Rewire__('Contact', () => <div />);

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  const alertPopupProps = component.find('Popup').get(1).props;

  assert.equal(alertPopupProps.title, 'title', 'alert popup title set');
  assert.equal(alertPopupProps.children, 'message', 'alert popup message set');
  assert.equal(alertPopupProps.buttons.length, 2);

  assert.equal(dispatchActionStub.callCount, 0);
  alertPopupProps.buttons[1].onClick();
  assert.equal(dispatchActionStub.callCount, 1);
  assert.deepEqual(dispatchActionStub.args, [[callToActionFunctionStub]]);

  assert.equal(resetAlertStub.callCount, 1);
  alertPopupProps.onClickToClose();
  assert.equal(resetAlertStub.callCount, 2);

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const callToActionFunctionStub = sandbox.stub();
  const dispatchActionStub = sandbox.stub();
  const resetAlertStub = sandbox.stub();

  const fakeRequiredProps = {
    children: <div />,
    history: { location: { pathname: '' } },
    resetError: () => ({}),
    errorMsg: null,
    showError: false,
    loading: false,
    fullLoading: true,
    isLoggedIn: true,
    logout: () => ({}),
    login: () => ({}),
    isMobile: false,
    alert: {
      title: 'title',
      message: 'message',
      callToActionLabel: 'callToActionLabel',
      callToActionFunction: callToActionFunctionStub,
    },
    resetAlert: resetAlertStub,
    dispatchAction: dispatchActionStub,
  };

  __RewireAPI__.__Rewire__('Contact', () => <div />);

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  const appWrapperLoaded = !!component.find('.app-wrapper').length;

  assert.equal(
    appWrapperLoaded,
    false,
    'AppWrapper is not rendered if fullLoading is false'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const callToActionFunctionStub = sandbox.stub();
  const dispatchActionStub = sandbox.stub();
  const resetAlertStub = sandbox.stub();

  const fakeRequiredProps = {
    children: <div />,
    history: { location: { pathname: '' } },
    resetError: () => ({}),
    errorMsg: null,
    showError: false,
    loading: true,
    fullLoading: false,
    isLoggedIn: true,
    logout: () => ({}),
    login: () => ({}),
    isMobile: false,
    alert: {
      title: 'title',
      message: 'message',
      callToActionLabel: 'callToActionLabel',
      callToActionFunction: callToActionFunctionStub,
    },
    resetAlert: resetAlertStub,
    dispatchAction: dispatchActionStub,
  };

  __RewireAPI__.__Rewire__('Contact', () => <div />);

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  const spinnerVisible = !!component
    .find(Spinner)
    .find('.spinner__container--visible').length;

  assert.equal(spinnerVisible, true, 'Spinner is visible if loading is true');

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const callToActionFunctionStub = sandbox.stub();
  const dispatchActionStub = sandbox.stub();
  const resetAlertStub = sandbox.stub();
  const onCloseCallToActionFunctionStub = sandbox.stub();

  const fakeRequiredProps = {
    children: <div />,
    history: { location: { pathname: '' } },
    resetError: () => ({}),
    errorMsg: null,
    showError: false,
    loading: true,
    fullLoading: false,
    isLoggedIn: true,
    logout: () => ({}),
    login: () => ({}),
    isMobile: false,
    alert: {
      title: 'title',
      message: 'message',
      callToActionLabel: 'callToActionLabel',
      callToActionFunction: callToActionFunctionStub,
      onCloseCallToActionFunction: onCloseCallToActionFunctionStub,
    },
    resetAlert: resetAlertStub,
    dispatchAction: dispatchActionStub,
  };

  __RewireAPI__.__Rewire__('Contact', () => <div />);

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component
    .find('Popup')
    .get(1)
    .props.onClickToClose();

  assert.equal(
    resetAlertStub.callCount,
    1,
    'resetAlert called on close of popup'
  );
  assert.equal(
    dispatchActionStub.callCount,
    1,
    'dispatchAction called on close of popup'
  );
  assert.deepEqual(
    dispatchActionStub.args[0][0],
    onCloseCallToActionFunctionStub,
    'onCloseCallToActionFunction passed to dispatch action prop on close of popup'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

const baseFakeProps = sandbox => {
  const callToActionFunctionStub = sandbox.stub();
  const dispatchActionStub = sandbox.stub();
  const resetAlertStub = sandbox.stub();
  const homeStub = sandbox.stub();
  return {
    children: <div />,
    history: { location: { pathname: '/atr' } },
    resetError: () => ({}),
    errorMsg: null,
    showError: false,
    loading: true,
    fullLoading: false,
    isLoggedIn: true,
    logout: () => ({}),
    login: () => ({}),
    isMobile: false,
    alert: {
      title: 'title',
      message: 'message',
      callToActionLabel: 'callToActionLabel',
      callToActionFunction: callToActionFunctionStub,
    },
    resetAlert: resetAlertStub,
    dispatchAction: dispatchActionStub,
    userHasGeneratedASolution: false,
    home: homeStub,
  };
};

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: true,
    userHasBeenReferred: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'redirected home when completed solution and on atr page'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: true,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'redirected home when referred and not completed solution and on disallowed page'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

[
  '/investment/payment',
  '/investment/payment-success',
  '/investment/advice-accepted',
].forEach(uri => {
  test('<AppWrapperComponent>', assert => {
    const sandbox = sinon.sandbox.create();

    const fakeRequiredProps = baseFakeProps(sandbox);
    fakeRequiredProps.history.location.pathname = uri;

    __RewireAPI__.__Rewire__('Contact', () => <div />);
    __RewireAPI__.__Rewire__('WealthWizards', {
      CONTEXT_ROUTE: '/investment',
    });

    const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

    component.setProps({
      userHasGeneratedASolution: true,
      userHasBeenReferred: false,
      userHasBeenReferredDueToAnAlert: true,
    });

    assert.equal(
      fakeRequiredProps.home.callCount,
      1,
      `redirected home when trying to visit ${uri} page but alerts have been generated for the solution`
    );

    assert.end();

    __RewireAPI__.__ResetDependency__('Contact');
    __RewireAPI__.__ResetDependency__('WealthWizards');
  });
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-summary/1';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: true,
    userHasBeenReferred: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    0,
    'not redirected home when going to page they are elligible to see'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-summary/1';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: true,
    userHasBeenReferred: true,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'redirected home when going to advice-summary page but they are referred'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/referral';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: true,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    0,
    'not redirected home when referred and going to page they are elligible to see'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-summary/1';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: false,
    fullLoading: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'home called when trying to access advice-summary/1 when solution not generated and full loading is false'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-summary/2';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: false,
    fullLoading: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'home called when trying to access advice-summary/2 when solution not generated and full loading is false'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-summary/3';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: false,
    fullLoading: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'home called when trying to access advice-summary/3 when solution not generated and full loading is false'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent>', assert => {
  const sandbox = sinon.sandbox.create();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/advice-accepted';

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: false,
    userHasBeenReferred: false,
    fullLoading: false,
  });

  assert.equal(
    fakeRequiredProps.home.callCount,
    1,
    'home called when trying to access advice-accepted when solution not generated and full loading is false'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AppWrapperComponent> - user redirected from dashboard if referred', assert => {
  const sandbox = sinon.sandbox.create();
  const redirectToReferralPageStub = sandbox.stub();

  const fakeRequiredProps = baseFakeProps(sandbox);
  fakeRequiredProps.history.location.pathname = '/investment/dashboard';

  fakeRequiredProps.redirectToReferralPage = redirectToReferralPageStub;

  __RewireAPI__.__Rewire__('Contact', () => <div />);
  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTEXT_ROUTE: '/investment',
  });

  const component = mount(<AppWrapperComponent {...fakeRequiredProps} />);

  component.setProps({
    userHasGeneratedASolution: true,
    userHasBeenReferred: true,
  });

  assert.equal(
    fakeRequiredProps.redirectToReferralPage.callCount,
    1,
    'referred user redirected to referral page when on dashboard'
  );

  assert.end();

  __RewireAPI__.__ResetDependency__('Contact');
  __RewireAPI__.__ResetDependency__('WealthWizards');
});
