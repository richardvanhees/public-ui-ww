import { connect } from 'react-redux';
import AppWrapperComponent from './AppWrapperComponent';
import { compose } from 'recompose';
import {
  resetError,
  resetAlert,
  resetNotification,
} from '../../../modules/browser';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import WealthWizards from '../../../modules/wealthwizards';
import { logout } from '../modules/login/actions';
import { lookContentByKey } from '../../../modules/content';
import { alertBasedReferral } from '../modules/solution/selectors';

const mapStateToProps = state => ({
  isMobile: state.browser.breakpoint === 'phablet',
  showError: state.browser.error,
  errorMsg: state.browser.errorMsg,
  loading: state.browser.loading,
  fullLoading: state.browser.fullLoading,
  alert: state.browser.alert,
  isLoggedIn: state.factFind.isLoggedIn,
  notification: state.browser.notification,
  userHasGeneratedASolution: state.solution.solution_id ? true : false, // eslint-disable-line
  userHasBeenReferred: state.solution.referral,
  userHasBeenReferredDueToAnAlert: alertBasedReferral(state.solution),
  content: lookContentByKey('investment', state.content),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      closeNotification: resetNotification,
      home: () => push(`${WealthWizards.CONTEXT_ROUTE}/dashboard`),
      resetError,
      resetAlert,
      login: () => push(`${WealthWizards.CONTEXT_ROUTE}/login`),
      redirectToErrorPage: () =>
        push(`${WealthWizards.CONTEXT_ROUTE}/services-down`),
      redirectToReferralPage: () =>
        push(`${WealthWizards.CONTEXT_ROUTE}/referral`),
      logout: () => logout(),
      dispatchAction: action => action(),
    },
    dispatch
  );

const AppWrapperContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default AppWrapperContainer(AppWrapperComponent);
