import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ResponsiveHeader from '../../../modules/elements/ResponsiveHeader';
import ContentWrapper from '../../../modules/elements/ContentWrapper';
import WealthWizards from '../../../modules/wealthwizards';
import ImageLoader from '../../../modules/elements/ImageLoader';
import Spinner from '../../../modules/elements/Spinner';
import Notification from '../../../modules/elements/Notification';
import Popup from '../../../modules/elements/Popup';
import { characterLogo } from '../../../assets/images/iw-images';
import Contact from '../../../modules/elements/Contact';

const pagesWithoutHeader = ['/login'];
const checkIfPageHasHeader = currentPage =>
  !(
    pagesWithoutHeader.indexOf(
      currentPage.split(WealthWizards.CONTEXT_ROUTE)[1]
    ) > -1
  );

const pagesUserCannotSeeBeforeSolutionGenerated = [
  '/advice-summary/1',
  '/advice-summary/2',
  '/advice-summary/3',
  '/advice-summary/4',
  '/advice-accepted',
  '/payment',
  '/payment-success',
  '/alert-referral',
];

const pagesUserCanSeeWhenSolutionGenerated = [
  '/advice-summary/1',
  '/advice-summary/2',
  '/advice-summary/3',
  '/advice-summary/4',
  '/dashboard',
  '/advice-accepted',
  '/payment',
  '/payment-success',
  '/alert-referral',
];
const pagesUserCanSeeWhenReferred = ['/dashboard', '/referral'];

const pagesUserCannotSeeWhenReferredDueToAnAlert = [
  '/advice-accepted',
  '/payment',
  '/payment-success',
];

const userNotAllowedToViewThisPage = (flag, allowedPages, history) =>
  flag &&
  allowedPages.indexOf(
    history.location.pathname.split(WealthWizards.CONTEXT_ROUTE)[1]
  ) === -1;

const userTryingToAccessAdviceSpecificPage = (flag, allowedPages, history) =>
  flag &&
  allowedPages.indexOf(
    history.location.pathname.split(WealthWizards.CONTEXT_ROUTE)[1]
  ) > -1;

export default class AppWrapperComponent extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    history: PropTypes.object.isRequired,
    loading: PropTypes.bool,
    fullLoading: PropTypes.bool,
    resetError: PropTypes.func,
    errorMsg: PropTypes.string,
    showError: PropTypes.bool,
    isLoggedIn: PropTypes.bool.isRequired,
    logout: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    isMobile: PropTypes.bool,
    notification: PropTypes.object.isRequired,
    closeNotification: PropTypes.func.isRequired,
    home: PropTypes.func.isRequired,
    alert: PropTypes.object,
    resetAlert: PropTypes.func.isRequired,
    dispatchAction: PropTypes.func.isRequired,
    redirectToErrorPage: PropTypes.func.isRequired,
    redirectToReferralPage: PropTypes.func.isRequired,
    userHasGeneratedASolution: PropTypes.bool.isRequired,
    userHasBeenReferred: PropTypes.bool.isRequired,
    userHasBeenReferredDueToAnAlert: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    alert: {
      callToActionLabel: '',
      message: '',
    },
  };

  componentDidUpdate() {
    const {
      home,
      userHasGeneratedASolution,
      userHasBeenReferred,
      history,
      fullLoading,
      alert,
      errorMsg,
      userHasBeenReferredDueToAnAlert,
    } = this.props;

    if (
      userTryingToAccessAdviceSpecificPage(
        userHasBeenReferredDueToAnAlert,
        pagesUserCannotSeeWhenReferredDueToAnAlert,
        history
      ) ||
      userNotAllowedToViewThisPage(
        userHasGeneratedASolution && !userHasBeenReferred,
        pagesUserCanSeeWhenSolutionGenerated,
        history
      ) ||
      userNotAllowedToViewThisPage(
        userHasBeenReferred,
        pagesUserCanSeeWhenReferred,
        history
      ) ||
      (!fullLoading &&
        userTryingToAccessAdviceSpecificPage(
          !userHasGeneratedASolution,
          pagesUserCannotSeeBeforeSolutionGenerated,
          history
        ))
    ) {
      home();
    }

    if (this.errorPopup && errorMsg !== '') {
      this.errorPopup.toggle(true);
    } else if (alert && alert.message !== '' && this.alertPopup) {
      this.alertPopup.toggle(true);
    }
  }

  componentDidCatch() {
    if (!this.props.content) {
      this.props.redirectToErrorPage();
    }
  }

  render() {
    const {
      history,
      children,
      loading,
      fullLoading,
      resetError,
      errorMsg,
      isLoggedIn,
      logout,
      login,
      notification,
      closeNotification,
      home,
      alert,
      resetAlert,
      dispatchAction,
      isMobile,
      content,
      userHasBeenReferred,
      redirectToReferralPage,
    } = this.props;

    const isLandingPageOrLogin =
      !!history.location.pathname.match(/\/investment\/?$/) ||
      window.location.href.indexOf('/landing') !== -1 ||
      window.location.href.indexOf('/login') !== -1;

    if (isLandingPageOrLogin && content) {
      return children;
    }

    if (fullLoading) {
      return <Spinner visible />;
    }

    const areWeOnDashboard = history.location.pathname.includes('/dashboard');
    const overrideBGColor = areWeOnDashboard;
    if (areWeOnDashboard && userHasBeenReferred) {
      redirectToReferralPage();
    }

    return (
      <div className={'app-wrapper'}>
        {checkIfPageHasHeader(history.location.pathname) && (
          <ResponsiveHeader
            image={characterLogo}
            home={home}
            isMobile={isMobile}
            navigation={
              content
                ? {
                    buttons: [
                      {
                        onClick: isLoggedIn ? logout : login,
                        label: isLoggedIn ? 'Sign out' : 'Sign in',
                        type: 'outline',
                        dataTest: isLoggedIn ? 'sign-out' : 'sign-in',
                      },
                    ],
                  }
                : { buttons: [] }
            }
          />
        )}

        <Notification {...notification} close={closeNotification} />
        <ImageLoader />

        {!isMobile && <Contact />}
        {<Spinner visible={loading} />}

        <Popup
          ref={c => {
            this.errorPopup = c;
          }}
          isError
          closeButtonTop
          closeClickOutside
          buttons={[
            {
              label: 'Close',
              onClick: () => {
                this.errorPopup.toggle(false);
                resetError();
              },
            },
          ]}
        >
          {errorMsg}
        </Popup>

        <Popup
          ref={c => {
            this.alertPopup = c;
          }}
          closeButtonTop
          closeClickOutside
          isNarrow
          title={alert && alert.title}
          onClickToClose={() => {
            resetAlert();
            alert &&
              alert.onCloseCallToActionFunction &&
              dispatchAction(alert.onCloseCallToActionFunction);
          }}
          buttons={[
            {
              label: 'Close',
              onClick: () => {
                this.alertPopup.toggle(false);
                resetAlert();
                alert &&
                  alert.onCloseCallToActionFunction &&
                  dispatchAction(alert.onCloseCallToActionFunction);
              },
            },
            {
              label: alert && alert.callToActionLabel,
              className: 'btn--primary',
              dataTest: 'popup-call-to-action',
              onClick: () => {
                this.alertPopup.toggle(false);
                resetAlert();
                dispatchAction(alert.callToActionFunction);
              },
            },
          ]}
        >
          {alert && alert.message}
        </Popup>

        <ContentWrapper className={overrideBGColor ? 'bg-color-grey' : ''}>
          {children}
        </ContentWrapper>
      </div>
    );
  }
}
