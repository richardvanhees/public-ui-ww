import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import InvestmentGoalComponent, {
  __RewireAPI__,
} from './InvestmentGoalComponent';

const sandbox = sinon.sandbox.create();
const redirectStub = sandbox.stub();

const FakeComponent = () => () => <div />;

const fakeProps = {
  content: {
    investmentGoals: {
      errors: ['a', 'a'],
      types: [
        {
          value: 'goal1',
          title: 'Goal 1',
          descriptionItems: [],
        },
        {
          value: 'goal2',
          title: 'Goal 2',
          descriptionItems: [],
        },
      ],
    },
  },
  next: () => ({}),
  back: () => ({}),
  setGoal: () => ({}),
  setTarget: () => ({}),
  push: () => ({}),
  redirect: redirectStub,
  goal: 'goal1',
  target: 100,
};

test('<InvestmentGoalComponent />', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('PageHeading', FakeComponent());
  __RewireAPI__.__Rewire__('PageNavigation', FakeComponent());
  __RewireAPI__.__Rewire__('Error', FakeComponent());
  __RewireAPI__.__Rewire__('Icon', FakeComponent());

  const target = mount(<InvestmentGoalComponent {...fakeProps} />);

  assert.true(
    target.find('div[data-test="goal-a0"]').exists(),
    'goal lookup works correctly from content'
  );

  __RewireAPI__.__ResetDependency__('PageHeading');
  __RewireAPI__.__ResetDependency__('PageNavigation');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Icon');

  sandbox.reset();
});
