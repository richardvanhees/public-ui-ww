import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import Error from '../../../../../modules/elements/Error';
import R from 'ramda';
import Icon from '../../../../../modules/elements/Icon';

export default class InvestmentGoalComponent extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    setGoal: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    goal: PropTypes.string.isRequired,
    target: PropTypes.number.isRequired,
    redirect: PropTypes.func.isRequired,
    rightTimeCompletedAt: PropTypes.string,
    doRightTimeCheck: PropTypes.bool,
  };

  static defaultProps = {
    doRightTimeCheck: true,
  };

  constructor(props) {
    super(props);
    this.state = {
      errorMessage: false,
    };
  }

  process = goal => {
    this.setState({ errorMessage: R.isEmpty(goal) });
    !R.isEmpty(goal) && this.props.next();
  };

  processBack = () => this.props.back();

  render = () => (
    <div className="wrapper investment-goal">
      <PageHeading
        largerSpacing
        title={this.props.content.investmentGoals.title}
        subTitle={this.props.content.investmentGoals.subTitle}
      />
      <div className="center-desktop lower-vertical-spacing investment-goal__wrapper">
        {this.props.content.investmentGoals.types.map((type, key) => (
          <div
            key={key}
            className={`panel-button ${
              this.props.goal === type.value ? 'panel-button--selected' : ''
            }`}
            data-test={`goal-a${key}`}
            onClick={() => {
              this.props.setGoal(type);
              this.process(type);
            }}
          >
            <div className="investment-goal__button-contents-container">
              <div className="investment-goal__icon-container">
                <Icon className="investment-goal__icon" name={type.image} />
                <h3 className="investment-goal__item-title">{type.title}</h3>
              </div>
              <div className="investment-goal__icon-descriptions-container">
                {type.descriptionItems.map((item, i) => (
                  <span className="investment-goal__descriptions-text" key={i}>
                    {item}
                    {i < type.descriptionItems.length - 1 && (
                      <span className="investment-goal__bullet">&#8226;</span>
                    )}
                  </span>
                ))}
              </div>
            </div>
          </div>
        ))}

        <Error
          className="investment-goal__error"
          dataTest="goal-error-message"
          title={this.props.content.investmentGoals.errors[2]}
          active={this.state.errorMessage}
          compact
        />
        <PageNavigation
          className="investment-goal__buttons"
          next={this.props.goal && (() => this.process(this.props.goal))}
          back={this.processBack}
          testNext={'goal'}
          testBack={'goal'}
        />
      </div>
    </div>
  );
}
