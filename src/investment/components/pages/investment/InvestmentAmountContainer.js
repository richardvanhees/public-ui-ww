import { connect } from 'react-redux';
import InvestmentAmountComponent from './InvestmentAmountComponent';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { compose } from 'recompose';
import { lookContentByKey } from '../../../../../modules/content';
import {
  setInvestmentGoalAmount,
  setInvestmentMonthlyAmount,
  setInvestmentGoalCompletion,
} from '../../../modules/investment-goal/actions';
import {
  removeKey,
} from '../../../modules/final-checks/actions';
import { bindActionCreators } from 'redux';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({
  content,
  investmentGoal,
  isa,
  factFind,
  rightTime,
}) => ({
  content: lookContentByKey(contentKey, content),
  investmentGoal,
  isas: isa.isas,
  monthlyContributions: WealthWizards.MONTHLY_CONTRIBUTIONS === 'true',
  isLoggedIn: factFind.isLoggedIn,
  minContribution: WealthWizards.MIN_CONTRIBUTION,
  minMonthlyContribution: WealthWizards.MIN_MONTHLY_CONTRIBUTION,
  rightTimeCompletedAt: rightTime.completedAt,
  doRightTimeCheck: !factFind.isLoggedIn,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      next: isLoggedIn =>
        saveFactFindRequest({
          redirectPath: `${isLoggedIn ? '/dashboard' : '/register'}`,
          doInBackground: true,
          withTimestamp: 'investment_investmentgoal_completed_at',
          setCompletedAt: setInvestmentGoalCompletion,
        }),
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/goal`),
      setAmount: setInvestmentGoalAmount,
      setMonthly: setInvestmentMonthlyAmount,
      redirect: url => push(url),
      resetMoneyToInvest: () => removeKey(
        'investment__money_to_invest_readily_available'
      ),
    },
    dispatch
  );

const NewInvestmentAmountComponent = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default NewInvestmentAmountComponent(InvestmentAmountComponent);
