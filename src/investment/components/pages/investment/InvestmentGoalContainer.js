import { connect } from 'react-redux';
import InvestmentGoalComponent from './InvestmentGoalComponent';
import { push, goBack } from 'react-router-redux';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';
import {
  setInvestmentGoal,
  setInvestmentTargetAmount,
} from '../../../modules/investment-goal/actions';
import { bindActionCreators } from 'redux';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import { getItem } from '../../../../../modules/utils/local-storage';

const contentKey = 'investment';

const mapStateToProps = ({ content, investmentGoal, rightTime }) => ({
  content: lookContentByKey(contentKey, content),
  goal: investmentGoal.goal,
  rightTimeCompletedAt: rightTime.completedAt,
  doRightTimeCheck: getItem('user_token') === null,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      next: () =>
        saveFactFindRequest({ redirectPath: '/amount', doInBackground: true }),
      back: () => goBack(),
      setGoal: setInvestmentGoal,
      setTarget: setInvestmentTargetAmount,
      push,
      redirect: url => push(url),
    },
    dispatch
  );

const NewInvestmentGoalComponent = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default NewInvestmentGoalComponent(InvestmentGoalComponent);
