import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import Explanation from '../../../../../modules/elements/Explanation';
import Icon from '../../../../../modules/elements/Icon';
import CurrencyInput from '../../../../../modules/elements/CurrencyInput';
import Error from '../../../../../modules/elements/Error';
import interpolate from '../../../../../modules/utils/interpolate';
import {
  calculateChangeAmount,
  calculateContributionSum,
} from '../../../modules/isa/isa-calculations';
import R from 'ramda';
import { contributionValueForTheTaxYr } from '../../../utils/contribution-calculators';

class InvestmentAmountComponent extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    investmentGoal: PropTypes.object.isRequired,
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    setAmount: PropTypes.func.isRequired,
    setMonthly: PropTypes.func.isRequired,
    excessContributionAmount: PropTypes.number,
    setExcessContributionAmount: PropTypes.func,
    isas: PropTypes.array,
    location: PropTypes.object,
    monthlyContributions: PropTypes.bool.isRequired,
    minContribution: PropTypes.number.isRequired,
    minMonthlyContribution: PropTypes.number.isRequired,
    redirect: PropTypes.func.isRequired,
    rightTimeCompletedAt: PropTypes.string,
    resetMoneyToInvest: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    const goalTypes = props.content.investmentGoals.types;

    const matchingGoalDefinition = R.find(
      R.propEq('value', props.investmentGoal.goal)
    )(goalTypes);

    let goal = matchingGoalDefinition
      ? ` ${matchingGoalDefinition.title.toLowerCase()}`
      : '';

    if (props.investmentGoal.goal.match(/Other|Just invest/)) goal = '';

    this.contributionSum = calculateContributionSum(props.isas);
    this.excessContributionAmount = calculateChangeAmount(
      props.investmentGoal.amount,
      this.contributionSum,
      props.investmentGoal.monthly
    );

    this.state = {
      goal,
      excessContributionAmount: props.excessContributionAmount,
      minimumAmountNotProvided: false,
      errorMessages: {
        top: this.generateErrorMessageTop(props),
        bottom: this.generateErrorMessageBottom(props),
      },
    };
  }

  componentDidMount = () => {
    const { excessContributionAmount, back, location } = this.props;

    if (
      !excessContributionAmount &&
      location.pathname.match(/amend-investment/)
    ) {
      back();
    }
  };

  componentDidUpdate = nextProps => {
    this.contributionSum = calculateContributionSum(nextProps.isas);
    this.excessContributionAmount = calculateChangeAmount(
      nextProps.investmentGoal.amount,
      this.contributionSum,
      nextProps.investmentGoal.monthly
    );
  };

  generateErrorMessageTop = (props, monthlyOverload, startingOverload) => {
    let title;
    let active = false;

    const { investmentGoal, content } = props;

    let ECAmount;
    let monthlyAmount;

    if (R.isNil(monthlyOverload)) {
      ECAmount = this.excessContributionAmount;
      monthlyAmount = investmentGoal.monthly;
    } else {
      ECAmount = calculateChangeAmount(
        props.investmentGoal.amount,
        this.contributionSum,
        monthlyOverload
      );
      monthlyAmount = monthlyOverload;
    }

    const startingAmount = R.isNil(startingOverload)
      ? investmentGoal.amount
      : startingOverload;

    const monthly = investmentGoal.monthly > 0;
    const excessCont = props.excessContributionAmount > 0;
    const isaContributions = this.contributionSum > 0;
    const amendInvestment = location.pathname.match(/amend-investment/);

    if (monthly && excessCont) {
      active = R.isNil(ECAmount) ? true : ECAmount > 0;
      title = interpolate(content.investmentAmount.reductionErrorTop, {
        amount: ECAmount,
      });
    }

    if (!active && monthly && isaContributions) {
      const overAmount = calculateChangeAmount(
        startingAmount,
        this.contributionSum,
        monthlyAmount
      );

      if (overAmount > 0) {
        active = true;
        const text = amendInvestment
          ? content.investmentAmount.reductionErrorTop
          : content.investmentAmount.errors[2];
        title = interpolate(text, {
          amount: overAmount,
        });
      }
    }

    if (!active) {
      active = this.displayChangeInvestmentError(startingAmount, monthlyAmount);

      title = interpolate(content.investmentAmount.errors[2], {
        amount: this.calculateAmountNeededToReduce(
          startingAmount,
          monthlyAmount
        ),
      });
    }

    if (investmentGoal.monthly === 0) {
      ECAmount = this.excessContributionAmount;
      if (props.excessContributionAmount > 0) {
        active = R.isNil(ECAmount) ? true : ECAmount > 0;
        title = interpolate(content.investmentAmount.reductionErrorMiddle, {
          amount: ECAmount,
        });
      }

      if (!active && !!this.contributionSum) {
        const overAmount = calculateChangeAmount(
          investmentGoal.amount,
          this.contributionSum,
          0
        );

        if (overAmount > 0) {
          active = true;
          title = interpolate(content.investmentAmount.reductionErrorMiddle, {
            amount: overAmount,
          });
        }
      }
    }

    return { title, active };
  };

  generateErrorMessageBottom = (props, monthlyOverload, startingOverload) => {
    const { investmentGoal, content } = props;
    const monthlyAmount = R.isNil(monthlyOverload)
      ? investmentGoal.monthly
      : monthlyOverload;
    const startingAmount = R.isNil(startingOverload)
      ? investmentGoal.amount
      : startingOverload;

    const overAmount = calculateChangeAmount(
      startingAmount,
      this.contributionSum,
      monthlyAmount
    );
    const active = R.isNil(monthlyOverload)
      ? !!monthlyAmount && overAmount > 0
      : true;

    const title = interpolate(content.investmentAmount.monthlyError, {
      amount: contributionValueForTheTaxYr(monthlyAmount),
    });

    return { title, active };
  };

  checkErrorMessages = () => {
    const nextErrorMessage = {
      top: this.generateErrorMessageTop(this.props),
      bottom: this.generateErrorMessageBottom(this.props),
    };
    this.setState({
      errorMessages: nextErrorMessage,
    });
    return !nextErrorMessage.top.active;
  };

  minimumAmountsSelected = () => {
    const {
      minMonthlyContribution,
      minContribution,
      investmentGoal,
    } = this.props;
    const { amount, monthly } = investmentGoal;
    const monthlyMinimum =
      monthly !== 0 ? monthly >= minMonthlyContribution : true;
    const amountMinimum = amount !== 0 ? amount >= minContribution : true;
    const noValuesProvided = monthly === 0 && amount === 0;
    const minimumProvided =
      amountMinimum && monthlyMinimum && !noValuesProvided;
    this.setState({ minimumAmountNotProvided: !minimumProvided });
    return minimumProvided;
  };

  proceed = () => {
    if (this.minimumAmountsSelected() && this.checkErrorMessages()) {
      this.props.setExcessContributionAmount &&
        this.props.setExcessContributionAmount(
          this.state.excessContributionAmount
        );
      this.props.next(this.props.isLoggedIn);
    }
  };

  displayChangeInvestmentError = (goalAmount, goalMonthly = 0) =>
    R.add(goalAmount, contributionValueForTheTaxYr(goalMonthly)) > 20000;

  calculateAmountNeededToReduce = (goalAmount, goalMonthly = 0) =>
    R.add(goalAmount, contributionValueForTheTaxYr(goalMonthly)) - 20000;

  render = () => {
    const {
      content,
      investmentGoal,
      back,
      setMonthly,
      setAmount,
      monthlyContributions,
      resetMoneyToInvest,
    } = this.props;
    const { goal, errorMessages, minimumAmountNotProvided } = this.state;

    return (
      <div className="wrapper">
        <div className="investment-amount-icon">
          <Icon
            name={
              investmentGoal.goal !== 'Other'
                ? investmentGoal.icon
                : 'chart-line'
            }
          />
        </div>
        <PageHeading
          largerSpacing
          title={`Your${goal} investment`}
          subTitle={content.investmentAmount.subTitle}
        />

        <div className="center-desktop default-vertical-spacing investment-amount">
          <Error
            className="investment-amount__error"
            dataTest="error-message-top"
            title={errorMessages.top.title}
            active={errorMessages.top.active}
            compact
            center
          />
          <div className="investment-amount--margin-top">
            <Error
              className="investment-amount__error "
              dataTest="error-message-minimum-amounts"
              title={content.investmentAmount.minimumProvidedError}
              active={minimumAmountNotProvided}
              compact
              center
            />
          </div>
          <div className="current-input-container investment-amount--margin-top">
            <CurrencyInput
              label={content.investmentAmount.sliderTitle[0]}
              value={investmentGoal.amount}
              onChange={(value) => {
                setAmount(value);
                if (value === 0) {
                  resetMoneyToInvest();
                }
              }}
              dataTest="slider-input-amount"
              error={errorMessages.amountError || minimumAmountNotProvided}
            />
          </div>
          {monthlyContributions && (
            <div className="current-input-container">
              <CurrencyInput
                label={content.investmentAmount.sliderTitle[1]}
                value={investmentGoal.monthly}
                onChange={setMonthly}
                dataTest="slider-input-monthly"
                error={errorMessages.monthlyError || minimumAmountNotProvided}
              />
              <Error
                className="investment-amount__error"
                dataTest="error-message-bottom"
                title={errorMessages.bottom.title}
                active={errorMessages.bottom.active}
                compact
                dark
              />
            </div>
          )}

          {(!!investmentGoal.amount || !!investmentGoal.monthly) && (
            <Explanation
              visible
              className="investment-amount__explanation"
              hideClose
            >
              <div className="explanation__wrapper">
                <Icon name="info-circle" />
                <p className="explanation__text">
                  {!!investmentGoal.amount &&
                    content.investmentAmount.information}
                  {!!investmentGoal.amount && !!investmentGoal.monthly && ' '}
                  {!!investmentGoal.monthly &&
                    content.investmentAmount.informationMonthly}
                </p>
              </div>
            </Explanation>
          )}

          <PageNavigation
            next={() => this.proceed()}
            back={back}
            nextLabel={
              this.props.excessContributionAmount ? 'Confirm change' : undefined
            }
            noSideMargins
            testNext={'amount'}
            testBack={'amount'}
          />
        </div>
      </div>
    );
  };
}

export default InvestmentAmountComponent;
