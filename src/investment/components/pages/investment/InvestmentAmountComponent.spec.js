import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import InvestmentAmountComponent from './InvestmentAmountComponent';
import { Provider } from 'react-redux';

const sandbox = sinon.sandbox.create();
const nextStub = sandbox.stub();
const backStub = sandbox.stub();
const setAmountStub = sandbox.stub();
const setMonthlyStub = sandbox.stub();
const ExcessStub = sandbox.stub();
const redirectStub = sandbox.stub();

const timekeeper = require('timekeeper');
const moment = require('moment');

const setup = (override = {}) => {
  const fakeProps = {
    content: {
      investmentGoal: {
        subTitle: 'What are you investing for?',
        title: 'Your investment goal',
      },
      investmentGoals: {
        types: [
          {
            title: 'Build up a pot',
            value: 'buildAPot',
          },
        ],
      },
      investmentAmount: {
        title: 'fake title',
        subTitle: 'fake subtitle',
        errors: ['error 0', 'error 1', 'error 2'],
        sliderTitle: ['first title', 'second title'],
        monthlyError: 'fake monthly error ${{amount}}',
        reductionErrorTop: 'fake reduction error ${{amount}}',
        reductionErrorMIddle: 'fake reduction error ${{amount}}',
      },
    },
    investmentGoal: {
      goal: 'buildAPot',
      name: 'fake name',
      amount: 500,
      monthly: 0,
    },
    next: nextStub,
    back: backStub,
    setAmount: setAmountStub,
    setMonthly: setMonthlyStub,
    redirect: redirectStub,
    excessContributionAmount: 0,
    setExcessContributionAmount: ExcessStub,
    isas: [],
    location: {
      pathname: 'investment/amount',
    },
    monthlyContributions: false,
    isLoggedIn: true,
    minMonthlyContribution: 20,
    minContribution: 100,
  };

  const overrideProps = {
    ...fakeProps,
    ...override,
  };

  return mount(<InvestmentAmountComponent {...overrideProps} />);
};

test('InvestmentAmountComponent initial state is correct from props passed', assert => {
  assert.plan(1);

  const context = setup();

  assert.deepEquals(
    context.state(),
    {
      errorMessages: {
        bottom: { active: false, title: 'fake monthly error $0' },
        top: { active: false, title: 'error 2' },
      },
      excessContributionAmount: 0,
      goal: ' build up a pot',
      minimumAmountNotProvided: false,
    },
    'check initial state'
  );
  sandbox.reset();
});

test('InvestmentAmountComponent happy day - next is invoked successfully', assert => {
  assert.plan(1);

  const context = setup();

  context
    .find('.currency-input-wrapper__input')
    .get(0)
    .props.onChange({ target: { value: 500 } });

  context.find('PageNavigation').prop('next')();
  assert.equal(nextStub.called, true, 'next function IS invoked');
  nextStub.reset();
  sandbox.reset();
});

test('InvestmentAmountComponent next not invoked when amount < 100 && no monthly', assert => {
  assert.plan(1);

  const context = setup({
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 1,
      monthly: 0,
    },
  });

  context
    .find('.currency-input-wrapper__input')
    .get(0)
    .props.onChange({ target: { value: 1 } });

  context.find('PageNavigation').prop('next')();
  assert.equal(
    nextStub.called,
    false,
    'next function NOT invoked as validation fails'
  );
  nextStub.reset();
  sandbox.reset();
});

test('InvestmentAmountComponent next not invoked when monthly < 20 && no amount', assert => {
  assert.plan(1);

  const context = setup({
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 0,
      monthly: 10,
    },
  });

  context
    .find('.currency-input-wrapper__input')
    .get(0)
    .props.onChange({ target: { value: 1 } });

  context.find('PageNavigation').prop('next')();
  assert.equal(
    nextStub.called,
    false,
    'next function NOT invoked as validation fails'
  );
  nextStub.reset();
  sandbox.reset();
});

test('InvestmentAmountComponent next IS invoked when monthly >= 20', assert => {
  assert.plan(1);

  const context = setup({
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 0,
      monthly: 20,
    },
  });

  context
    .find('.currency-input-wrapper__input')
    .get(0)
    .props.onChange({ target: { value: 1 } });

  context.find('PageNavigation').prop('next')();
  assert.equal(
    nextStub.called,
    true,
    'next function NOT invoked as validation fails'
  );
  nextStub.reset();
  sandbox.reset();
});

test('InvestmentAmountComponent next not invoked when amount monthly amount equates to over 20000', assert => {
  assert.plan(1);
  const context = setup({
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 22000,
      monthly: 100,
    },
  });

  context.find('[data-test="next-btn-amount"]').simulate('click');
  assert.equal(
    nextStub.called,
    false,
    'next function NOT invoked as validation fails'
  );
  nextStub.reset();
  sandbox.reset();
});

test('InvestmentAmountComponent top error message shown when excessContributionAmount > 0 with monthly and lump', assert => {
  assert.plan(1);

  const context = setup({
    excessContributionAmount: 1000,
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 5000,
      monthly: 100,
    },
  });
  const errorMessages = context.instance().state.errorMessages;

  assert.equal(errorMessages.top.active, true, 'top error message is visible');
});

test('InvestmentAmountComponent top error message shown when excessContributionAmount > 0 with lump', assert => {
  assert.plan(1);

  const context = setup({
    excessContributionAmount: 1000,
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 0,
    },
  });
  const errorMessages = context.instance().state.errorMessages;

  assert.equal(
    errorMessages.top.active,
    true,
    'top error message is not visible'
  );
  sandbox.reset();
});

test('InvestmentAmountComponent middle error message shown when lump & isa > 20000', assert => {
  assert.plan(1);

  const context = setup({
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 0,
    },
  });
  const errorMessages = context.instance().state.errorMessages;

  assert.equal(
    errorMessages.top.active,
    true,
    'top error message is not visible'
  );
  sandbox.reset();
});

test('InvestmentAmountComponent top error message shown when lump, month & isa > 20000', assert => {
  assert.plan(1);

  const context = setup({
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 100,
    },
  });
  const errorMessages = context.instance().state.errorMessages;

  assert.equal(
    errorMessages.top.active,
    true,
    'top error message is not visible'
  );
});

test('InvestmentAmountComponent monthly contributions on', assert => {
  assert.plan(1);

  const context = setup({
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 100,
    },
    monthlyContributions: true,
  });
  const target = context.find('[data-test="slider-input-monthly"]').length;
  assert.equal(target, 1, 'monthly target visible');
  sandbox.reset();
});

test('InvestmentAmountComponent monthly contributions off', assert => {
  assert.plan(1);

  const context = setup({
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 100,
    },
    monthlyContributions: false,
  });
  const target = context.find('[data-test="slider-input-monthly"]').length;

  assert.equal(target, 0, 'monthly target hidden');
  sandbox.reset();
});

test('InvestmentAmountComponent page is reloaded on amend investment', assert => {
  assert.plan(1);

  const context = setup({
    isas: [
      {
        amount: 18000,
        in_use: 'yes',
      },
    ],
    investmentGoal: {
      goal: 'my fake goal',
      name: 'fake name',
      amount: 3000,
      monthly: 100,
    },
    location: {
      pathname: 'investment/isa/amend-investment',
    },
    monthlyContributions: false,
  });

  assert.equal(backStub.callCount, 1, 'back should be called');
  sandbox.reset();
});
