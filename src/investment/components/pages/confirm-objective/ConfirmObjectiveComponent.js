import React, { Component } from 'react';
import PropTypes from 'prop-types';
import interpolate from '../../../../../modules/utils/interpolate';
import { capitalize } from '../../../../../modules/utils/formatting';
import checkmark from '../../../../../assets/images/iw-images/checkmark.png';
import { userWantsToWait } from '../../../modules/final-checks/selectors';

import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import ConfirmObjectivePuzzle from '../../../../../modules/elements/ConfirmObjectivePuzzle';
import * as puzzleImages from '../../../../../assets/images/iw-images';
import InvestmentDetails from '../../../modules/elements/InvestmentDetails';
import Popup from '../../../../../modules/elements/Popup';

export default class ConfirmObjectiveComponent extends Component {
  static propTypes = {
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    fieldUpdated: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    investmentGoal: PropTypes.object.isRequired,
    atrProfile: PropTypes.object.isRequired,
    experienceProfile: PropTypes.object.isRequired,
    flexibilityProfile: PropTypes.object.isRequired,
    resilienceProfile: PropTypes.object.isRequired,
    finalChecks: PropTypes.object.isRequired,
    navigateTo: PropTypes.func.isRequired,
  };

  proceed = () => {
    if (userWantsToWait(this.props.finalChecks)) {
      this.reconsiderPopup.toggle();
    } else {
      this.props.next();
    }
  };

  proceedFromPopup = async () => {
    await this.props.fieldUpdated('investment__reconsider_pay_into_pension', 'continue');
    this.props.next();
  };

  render = () => {
    const { content, back, investmentGoal, atrProfile, experienceProfile, resilienceProfile } = this.props;
    const sectionContent = content.confirmObjective;

    const goalContentConfig =
      content.investmentGoals.types.find(
        ({ value }) => value === investmentGoal.goal
      ) || {};

    return (
      <div className="confirm-objective wrapper">
        <PageHeading title={sectionContent.title} smallSubTitle>
          <span>{sectionContent.subTitle}</span>
        </PageHeading>

        <div className="center-desktop">
          <div className="confirm-objective__content">

            <InvestmentDetails
              className="confirm-objective__block confirm-objective__block--investment-details"
              data={[
                { type: 'goal', icon: goalContentConfig.image, value: goalContentConfig.title, heading: sectionContent.goalHeading },
                { type: 'lump-sum', icon: 'pound-sign', value: `£${investmentGoal.amount.toLocaleString()}`, heading: sectionContent.lumpSumHeading },
                { type: 'monthly', icon: 'pound-sign', value: `£${investmentGoal.monthly.toLocaleString()}`, heading: sectionContent.monthlyHeading },
              ]}
            />

            <div className="confirm-objective__block-container">
              <ConfirmObjectivePuzzle
                value={capitalize(atrProfile.profile)}
                profileTitle={sectionContent.attitudeTitle}
                image={puzzleImages.puzzlePieceTopLeft}
                dataTestAttr="con-obj-puzzle-top-left"
              />
              <ConfirmObjectivePuzzle
                value={'Considerable'}
                profileTitle={sectionContent.flexibilityTitle}
                image={puzzleImages.puzzlePieceTopRight}
                dataTestAttr="con-obj-puzzle-top-right"
              />
              <ConfirmObjectivePuzzle
                value={capitalize(experienceProfile.profile)}
                profileTitle={sectionContent.experienceTitle}
                image={puzzleImages.puzzlePieceBottomLeft}
                dataTestAttr="con-obj-puzzle-bottom-left"
              />
              <ConfirmObjectivePuzzle
                value={capitalize(resilienceProfile.profile)}
                profileTitle={sectionContent.resilienceTitle}
                image={puzzleImages.puzzlePieceBottomRight}
                dataTestAttr="con-obj-puzzle-bottom-right"
              />
            </div>
          </div>

          <Instructions
            list
            bullet={<img src={checkmark} alt={'checkmark'} className="confirm-objective__checkmark" />}
            className="confirm-objective__instructions"
            items={interpolate(sectionContent.items, {
              amount: investmentGoal.amount.toLocaleString(),
              duration: investmentGoal.length.toLowerCase(),
            })}
            listClassName="confirm-objective__instructions-list"
            itemClassName="confirm-objective__instructions-item"
          />

          <PageNavigation
            back={back}
            next={this.proceed}
            nextLabel={sectionContent.callToActionLabel}
            noSideMargins
            testNext={'confirm-objective'}
            testBack={'confirm-objective'}
          />

          <Popup
            ref={c => {
              this.reconsiderPopup = c;
            }}
            isNarrow
            closeClickOutside
            closeButtonTop
            title={sectionContent.reconsiderPopup.title}
            buttons={[{
              label: sectionContent.reconsiderPopup.back,
              onClick: () => this.props.navigateTo('dashboard'),
            }, {
              label: sectionContent.reconsiderPopup.continue,
              type: 'primary',
              onClick: this.proceedFromPopup,
            }]}
          >
            {sectionContent.reconsiderPopup.content}
          </Popup>

        </div>
      </div>
    );
  };
}
