import { connect } from 'react-redux';
import { goBack, push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import { fieldUpdated } from '../../../modules/final-checks/actions';

import ConfirmObjectiveComponent from './ConfirmObjectiveComponent';

const contentKey = 'investment';

export const mapStateToProps = ({ content, investmentGoal, atr, experience, flexibility, resilience, finalChecks }) => ({
  content: lookContentByKey(contentKey, content),
  investmentGoal,
  finalChecks,
  atrProfile: atr.override || atr.profile,
  experienceProfile: experience.override || experience.profile,
  flexibilityProfile: flexibility.override || flexibility.profile,
  resilienceProfile: resilience.override || resilience.profile,
});

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      navigateTo: page => push(`${WealthWizards.CONTEXT_ROUTE}/${page}`),
      back: goBack,
      fieldUpdated,
      next: () =>
        saveFactFindRequest({
          recordSolution: true,
          redirectPath: '/advice-summary/1',
        }),
    },
    dispatch
  );

export default () => {
  const ConfirmObjectiveContainer = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );
  return ConfirmObjectiveContainer(ConfirmObjectiveComponent);
};
