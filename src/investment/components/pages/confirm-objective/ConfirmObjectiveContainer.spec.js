import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import ConfirmObjectiveContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './ConfirmObjectiveContainer';

test('<ConfirmObjectiveContainer />', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('lookContentByKey', () => {
    return {};
  });

  const result = mapStateToProps({
    content: {},
    investmentGoal: 'fake investment goal',
    factFind: {
      isLoading: true,
    },
    finalChecks: {},
    atr: {
      override: 'this is an atr override',
    },
    experience: {
      override: 'this is an experience override',
    },
    flexibility: {
      override: 'this is an flexibility override',
    },
    resilience: {
      override: 'this is an resilience override',
    },
  });

  assert.deepEqual(
    result,
    {
      finalChecks: {},
      atrProfile: 'this is an atr override',
      content: {},
      experienceProfile: 'this is an experience override',
      flexibilityProfile: 'this is an flexibility override',
      investmentGoal: 'fake investment goal',
      resilienceProfile: 'this is an resilience override',
    },
    'mapStateToProps maps with overrides'
  );

  __RewireAPI__.__ResetDependency__('lookContentByKey');
});

test('<ConfirmObjectiveContainer />', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('lookContentByKey', () => {
    return {};
  });

  const result = mapStateToProps({
    content: {},
    investmentGoal: 'fake investment goal',
    factFind: {
      isLoading: true,
    },
    atr: {
      profile: 'this is an atr profile',
    },
    experience: {
      profile: 'this is an experience profile',
    },
    flexibility: {
      profile: 'this is an flexibility profile',
    },
    resilience: {
      profile: 'this is an resilience profile',
    },
    finalChecks: {},
  });

  assert.deepEqual(
    result,
    {
      atrProfile: 'this is an atr profile',
      content: {},
      finalChecks: {},
      experienceProfile: 'this is an experience profile',
      flexibilityProfile: 'this is an flexibility profile',
      investmentGoal: 'fake investment goal',
      resilienceProfile: 'this is an resilience profile',
    },
    'mapStateToProps maps with profile'
  );

  __RewireAPI__.__ResetDependency__('lookContentByKey');
});

test('<ConfirmObjectiveContainer />', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const pushStub = sandbox.stub();
  const saveFactFindRequestStub = sandbox.stub();
  const bindActionCreatorsStub = sandbox.stub();
  const dispatchStub = sandbox.stub();

  __RewireAPI__.__Rewire__('WealthWizards', { CONTEXT_ROUTE: '/investment' });
  __RewireAPI__.__Rewire__('push', pushStub);
  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestStub);
  __RewireAPI__.__Rewire__('bindActionCreators', bindActionCreatorsStub);

  mapDispatchToProps(dispatchStub);
  bindActionCreatorsStub.args[0][0].next();

  assert.deepEqual(
    saveFactFindRequestStub.args[0],
    [
      {
        recordSolution: true,
        redirectPath: '/advice-summary/1',
      },
    ],
    'saveFactFindRequestStub fired with record solution parameter'
  );

  __RewireAPI__.__ResetDependency__('push');
});
