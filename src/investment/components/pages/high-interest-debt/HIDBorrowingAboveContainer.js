import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import HIDComponent from './HIDBorrowingAboveComponent';
import {
  fieldUpdated,
  setCompletedAt,
} from '../../../modules/final-checks/actions';
import { validateForm } from '../../../modules/final-checks/selectors';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

export const mapStateToProps = ({ content, finalChecks }) => ({
  content: lookContentByKey(contentKey, content),
  getValue: id => finalChecks[id],
  formValidation: moduleContent => validateForm(moduleContent, finalChecks),
  finalChecks,
});

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: goBack,
      next: referUser =>
        referUser
          ? saveFactFindRequest({
              redirectPath: '/referral',
              recordSolution: true,
              withTimestamp: 'investment__completed_at',
              setCompletedAt,
            })
          : saveFactFindRequest({
              redirectPath: '/dashboard',
              doInBackground: true,
              withTimestamp: 'investment__completed_at',
              setCompletedAt,
            }),
      fieldUpdated: (name, value) => fieldUpdated(name, value),
    },
    dispatch
  );

export default () => {
  const HIDContainer = compose(connect(mapStateToProps, mapDispatchToProps));

  return HIDContainer(HIDComponent);
};
