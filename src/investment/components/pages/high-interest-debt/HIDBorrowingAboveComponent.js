import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import ErrorComponent from '../../../../../modules/elements/Error';
import Radio from '../../../../../modules/elements/Radio';
import R from 'ramda';
import reactHTMLParser from 'react-html-parser';

export default class HIDComponent extends PureComponent {
  static propTypes = {
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    fieldUpdated: PropTypes.func.isRequired,
    finalChecks: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    const keys = [
      'investment__hid_borrowing_above_30_short_term',
      'investment__hid_borrowing_above_30_taken_advice',
      'investment__hid_borrowing_above_30_getting_worse',
      'investment__hid_borrowing_above_30_struggling',
    ];

    const selected = R.compose(
      R.flatten,
      R.filter(([k, v]) => (
        keys.includes(k) && v === 'yes'
      )),
      R.toPairs
    )(props.finalChecks)[0] || null;

    this.onFieldChange = this.onFieldChange.bind(this);
    this.state = {
      selected,
      showError: false,
      radioValues: {
        investment__hid_borrowing_above_30_short_term: 'no',
        investment__hid_borrowing_above_30_taken_advice: 'no',
        investment__hid_borrowing_above_30_getting_worse: 'no',
        investment__hid_borrowing_above_30_struggling: 'no',
      },
    };
  }

  onFieldChange({ answerKey, answerValue }) {
    this.props.fieldUpdated(answerKey, answerValue);
  }

  setSelected(val) {
    this.setState({
      showError: false,
      selected: val,
      radioValues: {
        investment__hid_borrowing_above_30_short_term:
          val === 'investment__hid_borrowing_above_30_short_term'
            ? 'yes'
            : 'no',
        investment__hid_borrowing_above_30_taken_advice:
          val === 'investment__hid_borrowing_above_30_taken_advice'
            ? 'yes'
            : 'no',
        investment__hid_borrowing_above_30_getting_worse:
          val === 'investment__hid_borrowing_above_30_getting_worse'
            ? 'yes'
            : 'no',
        investment__hid_borrowing_above_30_struggling:
          val === 'investment__hid_borrowing_above_30_struggling'
            ? 'yes'
            : 'no',
      },
    });
    this.onFieldChange({
      answerKey: 'investment__hid_borrowing_above_30_short_term',
      answerValue:
        val === 'investment__hid_borrowing_above_30_short_term' ? 'yes' : 'no',
    });
    this.onFieldChange({
      answerKey: 'investment__hid_borrowing_above_30_taken_advice',
      answerValue:
        val === 'investment__hid_borrowing_above_30_taken_advice'
          ? 'yes'
          : 'no',
    });
    this.onFieldChange({
      answerKey: 'investment__hid_borrowing_above_30_getting_worse',
      answerValue:
        val === 'investment__hid_borrowing_above_30_getting_worse'
          ? 'yes'
          : 'no',
    });
    this.onFieldChange({
      answerKey: 'investment__hid_borrowing_above_30_struggling',
      answerValue:
        val === 'investment__hid_borrowing_above_30_struggling' ? 'yes' : 'no',
    });
  }

  render() {
    const { content } = this.props;
    const { radioQuestions, radioErrorMessage } = content.highInterestDebt;

    return (
      <div className="wrapper hid-borrowing-above">
        <div className="hid-borrowing-above__heading-container">
          <h3 className="hid-borrowing-above__heading-title">
            {reactHTMLParser(radioQuestions.title)}
          </h3>

          <div className="hid-borrowing-above__questions">
            <div className="hid-borrowing-above__questions-question-container">
              <Radio
                items={radioQuestions.answers}
                groupName="borrowing-above"
                onChange={e => this.setSelected(e)}
                selected={this.state.selected}
              />
            </div>

            {this.state.showError && (
              <ErrorComponent
                compact
                active
                title={radioErrorMessage}
                type="lock-to-page-navigation-component"
              />
            )}
            <PageNavigation
              next={() => {
                if (this.state.selected) {
                  const referUser = R.anyPass([
                    R.propEq(
                      'investment__hid_borrowing_above_30_getting_worse',
                      'yes'
                    ),
                    R.propEq(
                      'investment__hid_borrowing_above_30_struggling',
                      'yes'
                    ),
                  ])(this.state.radioValues);
                  this.props.next(referUser);
                } else {
                  this.setState({
                    showError: true,
                  });
                }
              }}
              back={this.props.back}
              noSideMargins
              testNext="hid"
              testBack="hid"
              className="hid-borrowing-above__button-container"
            />
          </div>
        </div>
      </div>
    );
  }
}
