import { connect } from 'react-redux';
import { push, goBack } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import HIDComponent from './HIDComponent';
import {
  fieldUpdated,
  setCompletedAt,
} from '../../../modules/final-checks/actions';
import {
  validateForm,
  userShouldBeReferred,
} from '../../../modules/final-checks/selectors';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import isMobile from '../../../../../modules/utils/is-mobile';
import R from 'ramda';

const contentKey = 'investment';

export const mapStateToProps = ({ content, finalChecks, browser }) => ({
  content: lookContentByKey(contentKey, content),
  getValue: id => finalChecks[id],
  formValidation: moduleContent => validateForm(moduleContent, finalChecks),
  userShouldBeReferred: userShouldBeReferred(finalChecks),
  isMobile: isMobile(browser),
  finalChecks,
});

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: goBack,
      next: (referUser, redirectPath) => {
        let request = {
          redirectPath,
          doInBackground: true,
        };
        if (!referUser && !!redirectPath.match('dashboard')) {
          request = R.merge(request, {
            withTimestamp: 'investment__completed_at',
            setCompletedAt,
          });
        }
        if (referUser) {
          request = R.merge(request, {
            redirectPath: '/referral',
            recordSolution: true,
          });
        }
        return saveFactFindRequest(request);
      },
      fieldUpdated: (name, value) => fieldUpdated(name, value),
      push,
    },
    dispatch
  );

export default () => {
  const HIDContainer = compose(connect(mapStateToProps, mapDispatchToProps));

  return HIDContainer(HIDComponent);
};
