import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import AnswerGroup from '../../../../../modules/elements/AnswerGroup';
import ErrorComponent from '../../../../../modules/elements/Error';
import Popup from '../../../../../modules/elements/Popup';
import interpolate from '../../../../../modules/utils/interpolate';
import R from 'ramda';
import Icon from '../../../../../modules/elements/Icon';
import reactHTMLParser from 'react-html-parser';

export default class HIDComponent extends PureComponent {
  static propTypes = {
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    finalChecks: PropTypes.object.isRequired,
    fieldUpdated: PropTypes.func.isRequired,
    getValue: PropTypes.func.isRequired,
    formValidation: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.onFieldChange = this.onFieldChange.bind(this);
    this.togglePopup = this.togglePopup.bind(this);
    this.state = { formValidationResult: null, popupText: null };
  }

  componentDidMount() {}

  onFieldChange({ answerKey, answerValue }) {
    this.props.fieldUpdated(answerKey, answerValue);
  }

  togglePopup(popupText, type = 'help') {
    this.setState({
      ...this.state,
      popupText: typeof popupText === 'string' ? popupText : '',
    });
    if (type === 'borrowing') {
      this.borrowingTextPopup.toggle();
    } else {
      this.helpTextPopup.toggle();
    }
  }

  showBompPopup() {
    const {
      behindOnPaymentsPopupContent,
    } = this.props.content.highInterestDebt;
    const shouldWeShowPopup = R.anyPass([
      R.propEq('investment__hid_borrowing_above_30', 'yes'),
      R.propEq('investment__hid_borrowing_below_30', 'yes'),
      R.propEq('investment__hid_mortgage', 'yes'),
    ]);
    if (shouldWeShowPopup(this.props.finalChecks)) {
      this.togglePopup(behindOnPaymentsPopupContent, 'borrowing');
    }

    return shouldWeShowPopup(this.props.finalChecks);
  }

  render() {
    const { content, getValue, formValidation } = this.props;
    const {
      title,
      questions,
      incompleteFormErrorMessage,
      helpPopupContent,
    } = content.highInterestDebt;
    const formValidationResult = formValidation(content.highInterestDebt);

    return (
      <div className="wrapper hid">
        <div className="hid__heading-container">
          <h3 className="hid__heading-title">{reactHTMLParser(title)}</h3>
          <div
            className="hid__heading-icon"
            data-test="hid-help-icon"
            onClick={() => this.togglePopup(helpPopupContent)}
          >
            <Icon name="info-circle" />
          </div>
        </div>
        <div className="hid-container">
          <div className="hid__questions">
            <div className="hid__questions-question-container">
              <div>
                <p
                  data-test="hid-borrowing-above-30-p"
                  className="hid__questions-question-label hid__questions-question-label-tall"
                >
                  {reactHTMLParser(questions[0].question)}
                </p>
              </div>
              <AnswerGroup
                wrapperClass="answer--iw-answer"
                answers={questions[0].answers}
                answerKey={questions[0].id}
                dataTest="hid-borrowing-above-30"
                onChange={this.onFieldChange}
                answerWidth={100}
                selected={getValue(questions[0].id)}
                forceAnswerWidth
              />
            </div>
            <div className="hid__questions-question-container">
              <p
                data-test="hid-borrowing-below-30-p"
                className="hid__questions-question-label"
              >
                {reactHTMLParser(questions[1].question)}
              </p>
              <AnswerGroup
                wrapperClass="answer--iw-answer"
                answers={questions[1].answers}
                answerKey={questions[1].id}
                dataTest="hid-borrowing-below-30"
                onChange={this.onFieldChange}
                selected={getValue(questions[1].id)}
                answerWidth={100}
                forceAnswerWidth
              />
            </div>
            <div className="hid__questions-question-container">
              <p
                data-test="hid-mortgage-p"
                className="hid__questions-question-label"
              >
                {reactHTMLParser(interpolate(questions[2].question))}
              </p>
              <AnswerGroup
                wrapperClass="answer--iw-answer"
                answers={questions[2].answers}
                answerKey={questions[2].id}
                dataTest="hid-mortgage"
                onChange={this.onFieldChange}
                selected={getValue(questions[2].id)}
                answerWidth={100}
                forceAnswerWidth
              />
            </div>
            {this.state.formValidationResult &&
            !this.state.formValidationResult.ok ? (
              <ErrorComponent
                compact
                active
                title={incompleteFormErrorMessage}
                type="lock-to-page-navigation-component"
              />
            ) : (
              <div className="hid__questions-error-spacer" />
            )}
            <PageNavigation
              next={() => {
                if (formValidationResult.ok && !this.showBompPopup()) {
                  this.props.next(false, '/dashboard');
                } else {
                  this.setState({ formValidationResult });
                }
              }}
              back={this.props.back}
              noSideMargins
              testNext="hid"
              testBack="hid"
              className="hid__button-container"
            />
          </div>
        </div>
        <Popup
          ref={c => {
            this.borrowingTextPopup = c;
          }}
          title="Types of borrowing"
          closeButtonTop
          closeClickOutside
          isError
          buttons={[
            {
              label: 'No',
              onClick: () => {
                this.props.fieldUpdated(
                  'investment__hid_behind_on_payments',
                  'no'
                );
                const showRadioBtns = R.propEq(
                  'investment__hid_borrowing_above_30',
                  'yes'
                )(this.props.finalChecks);
                if (showRadioBtns) {
                  this.props.next(false, '/investment/borrowing-above-30');
                } else {
                  this.props.next(false, '/investment/dashboard');
                }
              },
            },
            {
              label: 'Yes',
              onClick: () => {
                this.props.fieldUpdated(
                  'investment__hid_behind_on_payments',
                  'yes'
                );
                this.props.next(true);
              },
            },
          ]}
        >
          {this.state.popupText}
        </Popup>
        <Popup
          ref={c => {
            this.helpTextPopup = c;
          }}
          title="What's an outstanding balance?"
          closeButtonTop
          closeClickOutside
        >
          {reactHTMLParser(this.state.popupText)}
        </Popup>
      </div>
    );
  }
}
