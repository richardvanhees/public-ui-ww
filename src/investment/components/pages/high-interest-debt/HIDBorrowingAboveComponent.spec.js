import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import HIDBorrowingAboveComponent, {
  __RewireAPI__,
} from './HIDBorrowingAboveComponent';
import ErrorComponent from '../../../../../modules/elements/Error';
import { Provider } from 'react-redux';
import Tooltip from '../../../../../modules/elements/Tooltip';

test('HIDBorrowingAboveComponent error shown', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const nextStub = sandbox.stub();
  const backStub = sandbox.stub();
  const fieldUpdatedStub = sandbox.stub();
  const formValidationStub = () => ({ ok: true });
  const getValueStub = () => 'a fake value';

  const fakeAnswers = [
    {
      id: 0,
      value: 'yes',
      answer: 'Yes',
    },
    {
      id: 1,
      value: 'no',
      answer: 'No',
    },
  ];

  const fakeProps = {
    next: nextStub,
    back: backStub,
    content: {
      highInterestDebt: {
        incompleteFormErrorMessage: 'incompleteFormErrorMessage',
        title: 'title',
        radioQuestions: {
          title:
            "You've told us you have borrowing above 30% APR\nWhich of the following applies to you",
          answers: [
            {
              id: 'investment__hid_borrowing_above_30_short_term',
              label:
                'This is short term, I can repay it and i dont need any help',
            },
            {
              id: 'investment__hid_borrowing_above_30_taken_advice',
              label:
                "My debt situation has been hard, but I've taken advice and I have an action plan",
            },
            {
              id: 'investment__hid_borrowing_above_30_getting_worse',
              label:
                'My debt situation is getting worse, it may get out of control',
            },
            {
              id: 'investment__hid_borrowing_above_30_struggling',
              label: 'Im struggling with debt and I may need some help',
            },
          ],
        },
      },
    },
    fieldUpdated: fieldUpdatedStub,
    getValue: getValueStub,
  };

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => null,
  };

  class FakeRadio extends React.Component {
    render() {
      return <div />;
    }
  }

  __RewireAPI__.__Rewire__('Radio', FakeRadio);

  const target = mount(
    <Provider store={fakeStore}>
      <HIDBorrowingAboveComponent {...fakeProps} />
    </Provider>
  );

  target.find('button[data-test="next-btn-hid"]').simulate('click');

  assert.equals(
    target
      .find('.error-message')
      .children()
      .html(),
    '<div class="error-message__body error-message__body--compact"><i data-tip="" class="fas fa-exclamation "></i> <p class="error-message__title error-message__title--compact"></p></div>',
    'error shown when no answer selected'
  );

  __RewireAPI__.__ResetDependency__('Radio');
});

test('HIDBorrowingAboveComponent next', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const nextStub = sandbox.stub();
  const backStub = sandbox.stub();
  const fieldUpdatedStub = sandbox.stub();
  const formValidationStub = () => ({ ok: false });
  const getValueStub = () => 'a fake value';

  const fakeAnswers = [
    {
      id: 0,
      value: 'yes',
      answer: 'Yes',
    },
    {
      id: 1,
      value: 'no',
      answer: 'No',
    },
  ];

  const fakeProps = {
    next: nextStub,
    back: backStub,
    content: {
      highInterestDebt: {
        incompleteFormErrorMessage: 'incompleteFormErrorMessage',
        title: 'title',
        radioQuestions: {
          title:
            "You've told us you have borrowing above 30% APR\nWhich of the following applies to you",
          answers: [
            {
              id: 'investment__hid_borrowing_above_30_short_term',
              label:
                'This is short term, I can repay it and i dont need any help',
            },
            {
              id: 'investment__hid_borrowing_above_30_taken_advice',
              label:
                "My debt situation has been hard, but I've taken advice and I have an action plan",
            },
            {
              id: 'investment__hid_borrowing_above_30_getting_worse',
              label:
                'My debt situation is getting worse, it may get out of control',
            },
            {
              id: 'investment__hid_borrowing_above_30_struggling',
              label: 'Im struggling with debt and I may need some help',
            },
          ],
        },
      },
    },
    fieldUpdated: fieldUpdatedStub,
    getValue: getValueStub,
  };

  const target = shallow(<HIDBorrowingAboveComponent {...fakeProps} />);

  const updatedState = {
    selected: 'investment__hid_borrowing_above_30_short_term',
    showError: false,
    radioValues: {
      investment__hid_borrowing_above_30_short_term: 'no',
      investment__hid_borrowing_above_30_taken_advice: 'no',
      investment__hid_borrowing_above_30_getting_worse: 'no',
      investment__hid_borrowing_above_30_struggling: 'no',
    },
  };
  target.setState(updatedState);

  target.find('PageNavigation').prop('next')();

  assert.equal(
    nextStub.callCount,
    1,
    'next function IS invoked when there is no error'
  );
});
