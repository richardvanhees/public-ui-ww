import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import HIDComponent, { __RewireAPI__ } from './HIDComponent';

test('HIDComponent next', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const nextStub = sandbox.stub();
  const fieldUpdatedStub = sandbox.stub();
  const formValidationStub = () => ({ ok: false });
  const getValueStub = () => 'a fake value';

  const fakeAnswers = [
    {
      id: 0,
      value: 'yes',
      answer: 'Yes',
    },
    {
      id: 1,
      value: 'no',
      answer: 'No',
    },
  ];

  const fakeProps = {
    animate: false,
    next: nextStub,
    content: {
      highInterestDebt: {
        incompleteFormErrorMessage: 'incompleteFormErrorMessage',
        title: 'title',
        questions: [
          {
            id: 1,
            answers: fakeAnswers,
          },
          {
            id: 2,
            answers: fakeAnswers,
          },
          {
            id: 3,
            answers: fakeAnswers,
          },
          {
            id: 4,
            answers: fakeAnswers,
          },
        ],
      },
    },
    fieldUpdated: fieldUpdatedStub,
    getValue: getValueStub,
    formValidation: formValidationStub,
    userShouldBeReferred: false,
    savingsMinimum: 100,
  };

  const target = shallow(<HIDComponent {...fakeProps} />);

  target.find('PageNavigation').prop('next')();

  assert.equal(
    nextStub.callCount,
    0,
    'next function not invoked when there is a validation error'
  );
});

test('HIDComponent - Back', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const nextStub = sandbox.stub();
  const backStub = sandbox.stub();
  const fieldUpdatedStub = sandbox.stub();
  const formValidationStub = () => ({ ok: false });
  const getValueStub = () => 'a fake value';

  const fakeAnswers = [
    {
      id: 0,
      value: 'yes',
      answer: 'Yes',
    },
    {
      id: 1,
      value: 'no',
      answer: 'No',
    },
  ];

  const fakeProps = {
    animate: false,
    next: nextStub,
    back: backStub,
    content: {
      highInterestDebt: {
        incompleteFormErrorMessage: 'incompleteFormErrorMessage',
        title: 'title',
        questions: [
          {
            id: 1,
            answers: fakeAnswers,
          },
          {
            id: 2,
            answers: fakeAnswers,
          },
          {
            id: 3,
            answers: fakeAnswers,
          },
          {
            id: 4,
            answers: fakeAnswers,
          },
        ],
      },
    },
    fieldUpdated: fieldUpdatedStub,
    getValue: getValueStub,
    formValidation: formValidationStub,
    userShouldBeReferred: false,
    savingsMinimum: 100,
  };

  const target = shallow(<HIDComponent {...fakeProps} />);

  target.find('PageNavigation').prop('back')();

  assert.equal(backStub.callCount, 1, 'back function invoked');
});

test('HIDComponent answers', assert => {
  assert.plan(9);

  const sandbox = sinon.sandbox.create();

  const nextStub = sandbox.stub();
  const backStub = sandbox.stub();
  const fieldUpdatedStub = sandbox.stub();
  const formValidationStub = () => ({ ok: false });
  const getValueStub = () => 'a fake value';

  const fakeAnswers = [
    {
      id: 0,
      value: 'yes',
      answer: 'Yes',
    },
    {
      id: 1,
      value: 'no',
      answer: 'No',
    },
  ];

  const fakeProps = {
    animate: false,
    next: nextStub,
    back: backStub,
    content: {
      highInterestDebt: {
        incompleteFormErrorMessage: 'incompleteFormErrorMessage',
        title: 'title',
        questions: [
          {
            id: 1,
            answers: fakeAnswers,
          },
          {
            id: 2,
            answers: fakeAnswers,
          },
          {
            id: 3,
            answers: fakeAnswers,
          },
        ],
      },
    },
    fieldUpdated: fieldUpdatedStub,
    getValue: getValueStub,
    formValidation: formValidationStub,
    userShouldBeReferred: false,
    savingsMinimum: 100,
  };

  class FakeAnswerGroup extends React.PureComponent {
    render() {
      return <div />;
    }
  }

  __RewireAPI__.__Rewire__('AnswerGroup', FakeAnswerGroup);

  const target = mount(<HIDComponent {...fakeProps} />);

  const firstAnswerGroup = target.find(FakeAnswerGroup).get(0);

  assert.deepEqual(
    firstAnswerGroup.props.answers,
    fakeAnswers,
    'fakeAnswers set correctly into answers prop'
  );

  assert.deepEqual(
    firstAnswerGroup.props.answerKey,
    1,
    'id set correctly into answerKey prop'
  );

  firstAnswerGroup.props.onChange({
    answerKey: 'answerKey',
    answerValue: 'answerValue',
  });

  assert.deepEqual(fieldUpdatedStub.callCount, 1, 'fieldUpdated executed');

  const secondAnswerGroup = target.find(FakeAnswerGroup).get(1);

  assert.deepEqual(
    secondAnswerGroup.props.answers,
    fakeAnswers,
    'fakeAnswers set correctly into answers prop'
  );

  assert.deepEqual(
    secondAnswerGroup.props.answerKey,
    2,
    'id set correctly into answerKey prop'
  );

  secondAnswerGroup.props.onChange({
    answerKey: 'answerKey',
    answerValue: 'answerValue',
  });

  assert.deepEqual(
    fieldUpdatedStub.callCount,
    2,
    'fieldUpdated executed for 2nd time'
  );

  const thirdAnswerGroup = target.find(FakeAnswerGroup).get(2);

  assert.deepEqual(
    thirdAnswerGroup.props.answers,
    fakeAnswers,
    'fakeAnswers set correctly into answers prop'
  );

  assert.deepEqual(
    thirdAnswerGroup.props.answerKey,
    3,
    'id set correctly into answerKey prop'
  );

  thirdAnswerGroup.props.onChange({
    answerKey: 'answerKey',
    answerValue: 'answerValue',
  });

  assert.deepEqual(
    fieldUpdatedStub.callCount,
    3,
    'fieldUpdated executed for 3rd time'
  );

  __RewireAPI__.__ResetDependency__('AnswerGroup');
});
