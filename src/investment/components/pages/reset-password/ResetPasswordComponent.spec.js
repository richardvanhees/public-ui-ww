import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import ResetPasswordComponent from './ResetPasswordComponent';

test('ResetPasswordComponent', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const changeStub = sandbox.stub();

  const fakeProps = {
    change: changeStub,
    login: loginStub,
  };

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => null,
  };

  const target = mount(
    <Provider store={fakeStore}>
      <ResetPasswordComponent {...fakeProps} />
    </Provider>
  );

  target.find('input').simulate('change', { target: { value: 'aaaaaaaa1' } });
  target.find('button').simulate('click');
  assert.equal(changeStub.callCount, 1, 'changeStub called');

  assert.end();
});
