import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import ResetPasswordComponent from './ResetPasswordComponent';
import { resetPassword } from '../../../../../modules/password/actions';

export const mapStateToProps = () => ({});

export const mapDispatchToProps = (dispatch, props) =>
  bindActionCreators(
    {
      change: password => resetPassword(props.match.params.token, password),
      login: () => push(`${WealthWizards.CONTEXT_ROUTE}/login`),
    },
    dispatch
  );

export default () => {
  const ResetPasswordContainer = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );

  return ResetPasswordContainer(ResetPasswordComponent);
};
