import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import ResetPasswordContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './ResetPasswordContainer';

test('<ResetPasswordContainer />', assert => {
  const sandbox = sinon.sandbox.create();
  const resetPasswordStub = sandbox.stub();
  const bindActionCreatorsStub = sandbox.stub();
  const dispatchStub = sandbox.stub();
  const propsStub = sandbox.stub();
  propsStub.match = {
    params: {
      token: 'foo',
    },
  };

  __RewireAPI__.__Rewire__('bindActionCreators', bindActionCreatorsStub);
  __RewireAPI__.__Rewire__('resetPassword', resetPasswordStub);

  // check map state to props
  let result;
  result = mapStateToProps({});
  assert.deepEqual(result, {}, 'not used');

  // check map dispatch to props
  result = mapDispatchToProps(dispatchStub, propsStub);
  bindActionCreatorsStub.args[0][0].change('password');
  assert.deepEqual(
    resetPasswordStub.args[0],
    ['foo', 'password'],
    'resetPassword function executed with password and token from params'
  );

  __RewireAPI__.__ResetDependency__('bindActionCreators');
  __RewireAPI__.__ResetDependency__('resetPassword');
  assert.end();
});
