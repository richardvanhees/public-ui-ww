import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import { checkMark, resetPass } from '../../../../../assets/images/iw-images';
import ButtonComponent from '../../../../../modules/elements/Button';
import PasswordInput from '../../../../../modules/elements/PasswordInput';

class ResetPasswordComponent extends Component {
  static propTypes = {
    change: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.passwordInput = null;
    this.state = {
      sent: false,
      password: '',
      validPassword: false,
    };
  }

  requestChange = () => {
    if (this.state.validPassword) {
      this.props.change(this.state.password);
      this.setState({
        sent: true,
      });
    } else {
      this.passwordInput.validate();
    }
  };

  render() {
    return (
      <div className="reset-password__container">
        {!this.state.sent ? (
          <div className="reset-password__reset-form">
            <PageHeading image={resetPass} title="Reset password" />
            <PasswordInput
              ref={c => {
                this.passwordInput = c;
              }}
              label="New password"
              isValid={validPassword => this.setState({ validPassword })}
              onComplete={password => this.setState({ password })}
            />
            <ButtonComponent
              label="Save new password"
              onClick={this.requestChange}
              dataTest="reset-password-save-button"
              className="reset-password__save btn--primary"
            />
          </div>
        ) : (
          <div className="reset-password__sent">
            <PageHeading image={checkMark} title="Password changed!" />
            <ButtonComponent
              label="Return to Sign in"
              onClick={this.props.login}
              dataTest="reset-password-signin-button"
              className="reset-password__signin btn--primary"
            />
          </div>
        )}
      </div>
    );
  }
}

export default ResetPasswordComponent;
