import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../../modules/elements/Icon';
import Button from '../../../../../modules/elements/Button';
import * as images from '../../../../../assets/images/iw-images';
import moment from 'moment';
import ProgressCircle from '../../../../../modules/elements/ProgressCircle';

const ProgressSection = ({
  title,
  description,
  duration,
  buttonLabel,
  progress,
  icon,
  color,
  enabled,
  action,
  completedAt,
  dataTest,
}) => {
  const minutesLeft =
    progress !== 0 && Math.ceil(duration * (1 - progress / 100));

  const completedText = at => {
    if (!at) return 'date unknown';
    const m = time =>
      moment(time)
        .startOf('day')
        .format('DD/MM/YYYY');
    const then = m(at);
    if (then === m()) return 'today';
    return then;
  };

  return (
    <div className={'progress-section'}>
      {!enabled && <div className="progress-section__overlay" />}
      <div className="progress-section__bar">
        <ProgressCircle randomTimeout color={color} value={progress} />
      </div>

      <Icon {...icon} className="progress-section__icon" style={{ color }} />

      <div className="progress-section__title">{title}</div>
      {progress !== 100 && (
        <div
          className="progress-section__description"
          data-test={`${dataTest}-description`}
        >
          {description}
        </div>
      )}

      <div className={'progress-section__footer'}>
        {!(progress === 100) &&
          duration && (
            <div className="progress-section__duration">
              <Icon name={'clock'} iconSet={'regular'} />{' '}
              {minutesLeft || duration} minutes {!!minutesLeft && 'left'}
            </div>
          )}

        {buttonLabel && (
          <Button
            label={buttonLabel}
            style={{
              color,
              border: `1px solid ${color}`,
              marginTop: '40px',
              marginBottom: '10px',
            }}
            className="progress-section__button"
            onClick={enabled && action}
            invertOnHover
            dataTest={dataTest}
          />
        )}
      </div>

      {progress === 100 && (
        <div
          className={'progress-section__completed'}
          style={{ borderColor: color }}
        >
          Completed {completedText(completedAt)}
          <img
            src={images.celebrate}
            className="progress-section__completed-icon"
            alt={'celebrate'}
          />
        </div>
      )}
    </div>
  );
};

ProgressSection.propTypes = {
  title: PropTypes.node.isRequired,
  description: PropTypes.node.isRequired,
  duration: PropTypes.number,
  buttonLabel: PropTypes.node,
  progress: PropTypes.number.isRequired,
  icon: PropTypes.shape({
    name: PropTypes.string.isRequired,
    iconSet: PropTypes.string,
  }).isRequired,
  color: PropTypes.string.isRequired,
  enabled: PropTypes.bool.isRequired,
  action: PropTypes.func,
  completedAt: PropTypes.string,
  isMobile: PropTypes.bool,
  dataTest: PropTypes.string.isRequired,
};

ProgressSection.defaultProps = {
  enabled: true,
};

export default ProgressSection;
