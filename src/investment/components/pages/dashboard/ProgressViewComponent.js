import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Todo from '../../../../../modules/elements/Todo';
import DownloadProgressSection from './DownloadProgressSection';
import R from 'ramda';
import {
  generateRiskAction,
  generateAboutAction,
} from '../../../utils/progress-actions';
import { getItem } from '../../../../../modules/utils/local-storage';
import {
  calculateQuestionProgress,
  calculateResilienceProgress,
  calculateAboutProgress,
  calculateISAprogress,
  calculateFinalChecksProgress,
} from '../../../utils/progress-calculators';
import { initialState as aboutYouInitialState } from '../../../modules/about-you/reducer';

export default class ProgressViewComponent extends Component {
  static propTypes = {
    atr: PropTypes.object.isRequired,
    experience: PropTypes.object.isRequired,
    flexibility: PropTypes.object.isRequired,
    resilience: PropTypes.object.isRequired,
    goToPage: PropTypes.func.isRequired,
    isa: PropTypes.object.isRequired,
    finalChecks: PropTypes.object.isRequired,
    aboutYou: PropTypes.object.isRequired,
    investmentGoal: PropTypes.object.isRequired,
    userHasGeneratedASolution: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool.isRequired,
    downloadReport: PropTypes.func.isRequired,
    userHasBeenReferred: PropTypes.bool.isRequired,
    acceptedTerms: PropTypes.object,
    content: PropTypes.object.isRequired,
    solutionGenerationDate: PropTypes.string,
    toggleAfterReturnMessage: PropTypes.func.isRequired,
    afterReturnMessageActive: PropTypes.bool,
    flexibilityProgress: PropTypes.number.isRequired,
    solutionPaymentStatus: PropTypes.string,
    retryPayment: PropTypes.func,
    reportDownloaded: PropTypes.bool,
    alertBasedReferral: PropTypes.bool.isRequired,
    showNotification: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      adviceSummaryEnabled: false,
      headerText: 'Welcome back!',
      riskProgress: 0,
      aboutProgress: 0,
      adviceProgress: 0,
      riskProgressStateArray: [0, 0, 0, 0],
      aboutYouProgressStateArray: [0, 0, 0],
    };
  }

  componentDidMount() {
    this.renderLatestFiguresFromProps(this.props);
  }

  componentWillUnmount() {
    this.props.toggleAfterReturnMessage(false);
  }

  setHeader = (props, progressSections) => {
    const {
      content,
      userHasGeneratedASolution,
      userHasBeenReferred,
      afterReturnMessageActive,
      alertBasedReferral,
    } = props;

    const finishedCount = progressSections.filter(section => section === 100)
      .length;

    let headerText = '';

    if (userHasBeenReferred) {
      return this.setState({ headerText: content.dashboard.referral });
    }

    const isFirstLogin = getItem('first_login');

    if (
      !isFirstLogin &&
      userHasGeneratedASolution &&
      afterReturnMessageActive
    ) {
      return this.setState({ headerText: content.dashboard.afterReturn });
    }

    if (
      !isFirstLogin &&
      !(finishedCount === progressSections.length) &&
      afterReturnMessageActive
    ) {
      headerText += `${content.dashboard.afterReturn} `;
    }

    if (finishedCount === progressSections.length) {
      headerText += alertBasedReferral
        ? content.dashboard.paynow
        : content.dashboard.allDone;
    } else {
      headerText += content.dashboard[`finished${finishedCount}`];
    }

    return this.setState({ headerText });
  };

  setProgress = progress => {
    this.setState(progress);
  };

  calculateRiskProgress = progress => R.sum(progress) / progress.length;

  calculateAboutYouProgress = (aboutYou, isa, finalChecks, investmentGoal) => {
    const isaProgress = calculateISAprogress(isa);
    return (
      R.sum([
        calculateAboutProgress(aboutYou, aboutYouInitialState),
        calculateFinalChecksProgress(finalChecks, investmentGoal),
        isaProgress,
      ]) / (isaProgress ? 3 : 2)
    );
  };

  renderLatestFiguresFromProps(props) {
    const {
      atr,
      experience,
      resilience,
      aboutYou,
      isa,
      investmentGoal,
      acceptedTerms,
      finalChecks,
      content,
    } = props;

    const atrProgress = calculateQuestionProgress(
      atr.data,
      content.atrQuestions.length
    );

    const experienceProgress = calculateQuestionProgress(
      experience.data,
      content.experienceQuestions.length
    );

    const resilienceProgress = calculateResilienceProgress(resilience.data);

    const riskProgressStateArray = [
      atrProgress,
      experienceProgress,
      resilienceProgress,
    ];

    const riskProgress = this.calculateRiskProgress(riskProgressStateArray);

    const aboutProgress = this.calculateAboutYouProgress(
      aboutYou,
      isa,
      finalChecks,
      investmentGoal
    );

    const adviceProgress = R.allPass([
      R.pathOr(false, ['accepted_advice']),
      R.pathOr(false, ['read_documents']),
    ])(acceptedTerms)
      ? 100
      : 0;

    this.setProgress({
      riskProgress,
      aboutProgress,
      adviceProgress,
      riskProgressStateArray,
      aboutYouProgressStateArray: [
        calculateISAprogress(isa),
        calculateAboutProgress(aboutYou, aboutYouInitialState),
        calculateFinalChecksProgress(finalChecks),
      ],
    });
    this.setHeader(props, [100, riskProgress, aboutProgress]);
  }

  render() {
    const { riskProgress, aboutProgress, adviceProgress } = this.state;
    const {
      goToPage,
      resilience,
      finalChecks,
      investmentGoal,
      userHasGeneratedASolution,
      downloadReport,
      userHasBeenReferred,
      solutionGenerationDate,
      isMobile,
      acceptedTerms,
      solutionPaymentStatus,
      retryPayment,
      content,
      reportDownloaded,
      alertBasedReferral,
      showNotification,
    } = this.props;

    const riskAction = generateRiskAction(this.state.riskProgressStateArray);
    const aboutAction = generateAboutAction(
      this.state.aboutYouProgressStateArray
    );
    const viewType = this.props.isMobile ? { viewType: 'view' } : {};

    return (
      <div className="progress-view">
        <div className="progress-view__header">{this.state.headerText}</div>

        <div className="progress-view__section-container">
          {!userHasGeneratedASolution &&
            !userHasBeenReferred && (
              <Todo
                title={'Investment details'}
                completedAt={investmentGoal.completedAt}
                buttonLabel={'Review'}
                description={
                  "We'll ask what you're saving for and how much you want to invest."
                }
                progress={100}
                color={'#0F6b92'}
                action={() => goToPage('goal')}
                dataTest="progress-investment-details"
              />
            )}

          {!userHasGeneratedASolution &&
            !userHasBeenReferred && (
              <Todo
                title={'Risk Profiler'}
                dataTest="risk-profiler"
                buttonLabel={riskAction.label}
                completedAt={
                  resilience.profile && resilience.profile.generated_at
                }
                description={
                  "We'll help you identify how much risk you could take."
                }
                duration={12}
                progress={riskProgress}
                action={() => goToPage(riskAction.path)}
                color={'#e3832f'}
              />
            )}

          {!userHasGeneratedASolution &&
            !userHasBeenReferred && (
              <Todo
                title={'About you'}
                buttonLabel={aboutAction.label}
                dataTest="about-you"
                description={'Tell us about you and any other ISAs you have.'}
                duration={8}
                completedAt={finalChecks.investment__completed_at}
                progress={aboutProgress}
                color={'#4DB39A'}
                action={() => goToPage(aboutAction.path)}
              />
            )}

          {userHasGeneratedASolution &&
            !userHasBeenReferred && (
              <DownloadProgressSection
                title={'Advice summary'}
                buttonLabel={'Download'}
                description={'Download your report'}
                icon={{ name: 'hourglass', iconSet: 'regular' }}
                progress={adviceProgress}
                color={'#844196'}
                completedAt={solutionGenerationDate}
                action={downloadReport}
                isMobile={isMobile}
                downloadAction={() => downloadReport(viewType)}
                viewAction={() => goToPage('advice-summary/1')}
                acceptedTerms={R.allPass([
                  R.pathOr(false, ['accepted_advice']),
                  R.pathOr(false, ['read_documents']),
                ])(acceptedTerms)}
                solutionPaymentStatus={solutionPaymentStatus}
                retryPayment={retryPayment}
                content={content}
                reportDownloaded={reportDownloaded}
                alertBasedReferral={alertBasedReferral}
                showNotification={showNotification}
                customClassName="todo__payment-complete"
              />
            )}

          {!userHasGeneratedASolution &&
            (aboutProgress !== 100 || riskProgress !== 100) &&
            !userHasBeenReferred && (
              <Todo
                title={'Advice summary'}
                buttonLabel={'View'}
                description={
                  "This is unlocked once you've told us about you and your investment."
                }
                color={'#844196'}
                enabled={false}
                dataTest="advice-locked"
              />
            )}

          {userHasBeenReferred && (
            <Todo
              title={'Advice summary'}
              description={'We will be in touch'}
              color={'#844196'}
              enabled={false}
              dataTest="advice-referral"
            />
          )}

          {aboutProgress === 100 &&
            riskProgress === 100 &&
            !userHasGeneratedASolution &&
            !userHasBeenReferred && (
              <Todo
                title={'Advice summary'}
                buttonLabel={'View'}
                dataTest="view-advice-summary"
                color={'#844196'}
                enabled
                action={() => goToPage('confirm-objective')}
              />
            )}
        </div>
      </div>
    );
  }
}
