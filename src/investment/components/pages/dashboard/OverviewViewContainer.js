import { connect } from 'react-redux';
import OverviewViewComponent from './OverviewViewComponent';
import { bindActionCreators } from 'redux';
import iconFinder from '../../../../../modules/utils/icon-finder';

const mapStateToProps = ({ investmentGoal }, ownProps) => ({
  investmentGoal: iconFinder(ownProps.content, investmentGoal),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {},
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(OverviewViewComponent);
