import { connect } from 'react-redux';
import ProgressViewComponent from './ProgressViewComponent';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import {
  downloadReportRequest,
} from '../../../modules/solution/actions';
import { toggleAfterReturnMessage } from '../../../modules/login/actions';
import isMobile from '../../../../../modules/utils/is-mobile';
import {
  individualSolution,
  downloaded,
} from '../../../modules/solution-metadata/selector';
import iconFinder from '../../../../../modules/utils/icon-finder';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { calculateFlexibilityProgress } from '../../../modules/flexibility/selectors';
import { setNotification } from '../../../../../modules/browser';
import { alertBasedReferral } from '../../../modules/solution/selectors';

const mapStateToProps = (
  {
    atr,
    content,
    experience,
    resilience,
    flexibility,
    aboutYou,
    isa,
    finalChecks,
    investmentGoal,
    browser,
    solution,
    solutionMetadata,
    login,
  },
  ownProps
) => ({
  atr,
  content: lookContentByKey('investment', content),
  experience,
  resilience,
  flexibility,
  aboutYou,
  finalChecks,
  isa,
  investmentGoal: iconFinder(ownProps.content, investmentGoal),
  isMobile: isMobile(browser),
  userHasGeneratedASolution: !!solution.solution_id,
  userHasBeenReferred: solution.referral,
  alertBasedReferral: alertBasedReferral(solution),
  solutionGenerationDate: solution.data.date_generated,
  acceptedTerms: individualSolution(solutionMetadata, solution.solution_id),
  flexibilityProgress: calculateFlexibilityProgress(
    investmentGoal,
    flexibility.data,
    lookContentByKey('investment', content).flexibilityQuestions.length
  ),
  solutionPaymentStatus: solution.data.paymentStatus,
  reportDownloaded: downloaded(solutionMetadata, solution.solution_id),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      goToPage: page => push(`${WealthWizards.CONTEXT_ROUTE}/${page}`),
      downloadReport: data => downloadReportRequest(data),
      toggleAfterReturnMessage: active => toggleAfterReturnMessage(active),
      retryPayment: () => push(`${WealthWizards.CONTEXT_ROUTE}/payment`),
      showNotification: (msg, type, flash) => setNotification(msg, type, flash),
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  ProgressViewComponent
);
