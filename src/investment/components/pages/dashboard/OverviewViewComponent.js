import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import InvestmentDetails from '../../../modules/elements/InvestmentDetails';

const OverviewViewComponent = props => {
  const { investmentGoal, content } = props;

  const matchingGoalDefinition = (goal) => R.find(
    R.propEq('value', goal)
  )(content.investmentGoals.types);

  return (
    <div className="dashboard__overview-container">
      <InvestmentDetails
        className="dashboard__investment-details"
        data={[
          { type: 'goal', icon: 'piggy-bank', value: investmentGoal.goal && matchingGoalDefinition(investmentGoal.goal).title, heading: 'My goal' },
          { type: 'lump-sum', icon: 'pound-sign', value: `£${investmentGoal.amount.toLocaleString()}`, heading: 'Investment amount' },
          { type: 'monthly', icon: 'pound-sign', value: `£${investmentGoal.monthly.toLocaleString()}`, heading: 'Monthly amount' },
        ]}
      />
    </div>
  );
};

OverviewViewComponent.propTypes = {
  investmentGoal: PropTypes.object.isRequired,
  content: PropTypes.object.isRequired,
};

export default OverviewViewComponent;
