import { connect } from 'react-redux';
import DashboardComponent from './DashboardComponent';
import { compose } from 'recompose';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';

const contentKey = 'investment';

export const mapStateToProps = ({ content, browser }) => ({
  content: lookContentByKey(contentKey, content),
  isLoading: browser.loading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      retryPayment: () => push(`${WealthWizards.CONTEXT_ROUTE}/payment`),
    },
    dispatch
  );

export default () => {
  const DashboardContainer = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );

  return DashboardContainer(DashboardComponent);
};
