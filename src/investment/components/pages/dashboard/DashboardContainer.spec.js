import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import DashboardContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './DashboardContainer';

test('<DashboardContainer />', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('lookContentByKey', () => {
    return {};
  });

  const result = mapStateToProps({
    content: {},
    browser: {
      loading: false
    }
  });

  assert.deepEqual(result.content, {});

  __RewireAPI__.__ResetDependency__('lookContentByKey');
});
