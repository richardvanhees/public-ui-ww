import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import DownloadProgressSection from './DownloadProgressSection';
import R from 'ramda';

const sandbox = sinon.sandbox.create();
const actionStub = sandbox.stub();
const paymentStub = sandbox.stub();
const showNotificationStub = sandbox.stub();

const fakeProps = {
  title: 'TITLE',
  description: 'DESCRIPTION',
  buttonLabel: 'BUTTONLABEL',
  duration: 10,
  progress: 0,
  icon: {
    name: 'home',
  },
  color: '#fff',
  enabled: true,
  action: actionStub,
  solutionPaymentStatus: null,
  content: {
    notifications: {
      alertBasedReferral: 'alertBasedReferral',
    },
    dashboard: {
      paynow: 'CONTENT',
    },
  },
  retryPayment: paymentStub,
  showNotification: showNotificationStub,
};

test('DownloadProgressSection', assert => {
  assert.plan(1);

  const augmentedProps = {
    ...fakeProps,
    solutionPaymentStatus: 'PENDING',
    acceptedTerms: true,
    progress: 100,
    alertBasedReferral: true,
  };

  const wrapper = mount(<DownloadProgressSection {...augmentedProps} />);
  wrapper.find('.todo__paynow-link').at(0).prop('onClick')();

  assert.deepEqual(augmentedProps.showNotification.args, [
    ['alertBasedReferral', 'success', 20000],
  ]);

  assert.end();
});

test('DownloadProgressSection', assert => {
  assert.plan(2);
  const wrapper = mount(<DownloadProgressSection {...fakeProps} />);
  assert.equal(
    wrapper.find('.todo').length,
    1,
    'todo displayed'
  );
  assert.equal(
    wrapper.find('.todo__completed').length,
    0,
    'progress section completed not shown'
  );
  assert.end();
});

test('DownloadProgressSection - completed', assert => {
  assert.plan(3);
  const completedProps = {
    ...fakeProps,
    progress: 100,
  };
  const wrapper = mount(<DownloadProgressSection {...completedProps} />);
  assert.equal(
    wrapper.find('.todo').length,
    1,
    'progress section displayed'
  );
  assert.equal(
    wrapper.find('.todo__completed').length,
    1,
    'progress section completed shown'
  );
  assert.equal(
    wrapper.find('.todo__description').length,
    1,
    'description shown'
  );
  assert.end();
});

test('DownloadProgressSection - not accepted terms', assert => {
  assert.plan(2);
  const completedProps = {
    ...fakeProps,
    acceptedTerms: false,
  };
  const wrapper = mount(<DownloadProgressSection {...completedProps} />);
  assert.equal(
    wrapper.find('[data-test="view-report-button"]').length,
    1,
    'view button is displayed'
  );
  assert.equal(
    wrapper.find('[data-test="download-report-button"]').length,
    0,
    'download button is not displayed'
  );
  assert.end();
});

test('DownloadProgressSection - pay now', assert => {
  assert.plan(1);
  const completedProps = {
    ...fakeProps,
    progress: 100,
    solutionPaymentStatus: 'PENDING',
    acceptedTerms: true,
    reportDownloaded: true,
  };
  const wrapper = mount(<DownloadProgressSection {...completedProps} />);
  assert.equal(
    wrapper.find('[data-test="paynow-link"]').length,
    1,
    'pay now link displayed'
  );
  assert.end();
});

test('DownloadProgressSection - pay now', assert => {
  assert.plan(2);
  const completedProps = {
    ...fakeProps,
    progress: 100,
    solutionPaymentStatus: 'PENDING',
    acceptedTerms: true,
    reportDownloaded: false,
  };
  const wrapper = mount(<DownloadProgressSection {...completedProps} />);
  assert.equal(
    wrapper.find('[data-test="paynow-link"]').length,
    1,
    'pay now link displayed'
  );

  assert.equal(
    wrapper.find('.todo__paynow-link--disabled').length,
    2, // Both the Button component and the html rendered by that Button
    'pay now link displayed disabled'
  );
});
