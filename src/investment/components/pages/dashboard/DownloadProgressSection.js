import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../../modules/elements/Icon';
import Button from '../../../../../modules/elements/Button';
import moment from 'moment';
import checkmark from '../../../../../assets/images/iw-images/checkmark-no-bg.png';

const ProgressSection = ({
  title,
  description,
  progress,
  color,
  completedAt,
  downloadAction,
  viewAction,
  isMobile,
  acceptedTerms,
  solutionPaymentStatus,
  content,
  retryPayment,
  alertBasedReferral,
  showNotification,
  reportDownloaded,
  customClassName,
}) => {
  const completedText = at => {
    const m = time =>
      moment(time)
        .startOf('day')
        .format('DD/MM/YYYY');
    const then = m(at);
    if (then === m()) return 'today';
    return then;
  };

  const showPayNow =
    solutionPaymentStatus !== 'PAID' && acceptedTerms && progress === 100;

  return (
    <div className="todo">
      <div className="todo__icon-container">
        <div className="todo__circle" />
        <img className="todo__check" src={checkmark} alt={'checkmark'} />
      </div>

      <div className="todo__description-container">
        <div className="todo__title">{title}</div>
        <div className="todo__description" style={{ color }}>
          {description}
        </div>
      </div>

      <div className="todo__progress-container">
        {!showPayNow &&
          progress === 100 && (
            <div className={`todo__completed ${customClassName}`}>
              Generated {completedText(completedAt)}
            </div>
          )}

        {showPayNow && (
          <Button
            dataTest={'paynow-link'}
            onClick={() =>
              alertBasedReferral
                ? showNotification(
                    content.notifications.alertBasedReferral,
                    'success',
                    20000
                  )
                : retryPayment()
            }
            style={{ marginBottom: 10 }}
            type={'primary'}
            label={
              <span>
                <Icon name={'lock'} iconSet={'light'} />
                &nbsp;&nbsp;Pay now
              </span>
            }
            className={`todo__paynow-link ${
              reportDownloaded ? '' : 'todo__paynow-link--disabled'
            }`}
          />
        )}
        <Button
          label={`View ${isMobile ? '' : 'summary'}`}
          style={{
            color,
            border: `1px solid ${color}`,
          }}
          onClick={viewAction}
          invertOnHover
          dataTest="view-report-button"
        />

        {acceptedTerms &&
          !alertBasedReferral && (
            <Button
              label={`Download ${isMobile ? '' : 'report'}`}
              style={{ color, border: `1px solid ${color}`, marginTop: '10px' }}
              onClick={downloadAction}
              invertOnHover
              dataTest="download-report-button"
            />
          )}
      </div>
    </div>
  );
};

ProgressSection.propTypes = {
  title: PropTypes.node.isRequired,
  description: PropTypes.node.isRequired,
  duration: PropTypes.number,
  progress: PropTypes.number.isRequired,
  icon: PropTypes.shape({
    name: PropTypes.string.isRequired,
    iconSet: PropTypes.string,
  }).isRequired,
  color: PropTypes.string.isRequired,
  completedAt: PropTypes.string,
  downloadAction: PropTypes.func,
  viewAction: PropTypes.func,
  isMobile: PropTypes.bool,
  acceptedTerms: PropTypes.bool,
  reportDownloaded: PropTypes.bool,
  solutionPaymentStatus: PropTypes.string,
  retryPayment: PropTypes.func,
  content: PropTypes.object.isRequired,
  alertBasedReferral: PropTypes.bool,
  showNotification: PropTypes.func,
  customClassName: PropTypes.string,
};

export default ProgressSection;
