import React from 'react';
import PropTypes from 'prop-types';
import ProgressView from './ProgressViewContainer';
import OverviewView from './OverviewViewContainer';
import Popup from '../../../../../modules/elements/Popup';

const DashboardComponent = ({ content, retryPayment, location }) =>
  content && content.dashboard ? (
    <div className="dashboard">
      <Popup
        visible={!!location.search.match(/paymentError/)}
        closeButtonTop
        buttons={[
          {
            label: 'Try again',
            onClick: () => retryPayment(),
            type: 'primary',
            dataTest: 'payment-try-again-btn',
          },
        ]}
        isError
        title={content.dashboard.paymentError.title}
        className="dashboard__payment-popup"
      >
        {content.dashboard.paymentError.content}
      </Popup>

      <OverviewView content={content} />
      <ProgressView content={content} />
    </div>
  ) : null;

DashboardComponent.propTypes = {
  content: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  location: PropTypes.object,
  retryPayment: PropTypes.func.isRequired,
};

export default DashboardComponent;
