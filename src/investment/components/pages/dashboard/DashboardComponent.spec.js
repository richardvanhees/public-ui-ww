import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import { Provider } from 'react-redux';

import DashboardComponent from './DashboardComponent';

test('DashboardComponent', assert => {
  assert.plan(2);
  const content = {
    foo: 'bar',
    dashboard: {
      paymentError: {
        title: 'TITLE',
        content: 'CONTENT'
      }
    }
  }
  const fakeProps = {
    content,
    location: {
      search: 'foo?paymentError'
    },
    retryPayment: () => {}
  };

  const wrapper = shallow(<DashboardComponent {...fakeProps} />);
  const target = wrapper.find('.dashboard').get(0);

  assert.equal(target.props.children.length, 3, 'three elements found');
  assert.deepEqual(target.props.children[1].props.content, content, 'second element has content passed to it');
});

test('DashboardComponent', assert => {
  assert.plan(1);

  const fakeProps = {
    content: {
      foo: 'bar',
      dashboard: {
        paymentError: {
          title: 'TITLE',
          content: 'CONTENT'
        }
      }
    },
    location: {
      search: 'foo?paymentError'
    },
    retryPayment: () => {}
  };

  const wrapper = shallow(<DashboardComponent {...fakeProps} />);
  const target = wrapper.find('.dashboard__payment-popup');

  assert.equal(target.length, 1, 'popup shown when paymentError');
});
