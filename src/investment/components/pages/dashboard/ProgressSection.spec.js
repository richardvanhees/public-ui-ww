import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import ProgressSection from './ProgressSection';
import R from 'ramda';

const sandbox = sinon.sandbox.create();
const actionStub = sandbox.stub();

const fakeProps = {
  title: 'TITLE',
  description: 'DESCRIPTION',
  buttonLabel: 'BUTTONLABEL',
  duration: 10,
  progress: 0,
  icon: {
    name: 'home',
  },
  color: '#fff',
  enabled: true,
  action: actionStub,
};


test('ProgressSection', assert => {
  assert.plan(3);
  const wrapper = mount(<ProgressSection {...fakeProps} />);
  assert.equal(wrapper.find('.progress-section').length, 1, 'progress section displayed');
  assert.equal(wrapper.find('.progress-section__completed').length, 0, 'progress section completed not shown');
  assert.equal(wrapper.find('.progress-section__duration').length, 1, 'duration shown');
});

test('ProgressSection - completed', assert => {
  assert.plan(4);
  const completedProps = {
    ...fakeProps,
    progress: 100
  };
  const wrapper = mount(<ProgressSection {...completedProps} />);
  assert.equal(wrapper.find('.progress-section').length, 1, 'progress section displayed');
  assert.equal(wrapper.find('.progress-section__completed').length, 1, 'progress section completed shown');
  assert.equal(wrapper.find('.progress-section__duration').length, 0, 'duration not shown');
  assert.equal(wrapper.find('.progress-section__description').length, 0, 'description not shown');
});

test('ProgressSection - disabled state', assert => {
  assert.plan(1);
  const disabledProps = {
    ...fakeProps,
    enabled: false
  };
  const wrapper = mount(<ProgressSection {...disabledProps} />);
  assert.equal(wrapper.find('.progress-section__overlay').length, 1, 'overlay is shown when disabled');
});
