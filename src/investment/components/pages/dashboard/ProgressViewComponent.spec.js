import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { Provider } from 'react-redux';

import ProgressViewComponent, { __RewireAPI__ } from './ProgressViewComponent';
import R from 'ramda';

const sandbox = sinon.sandbox.create();
const goToPageStub = sandbox.stub();
const downloadReportStub = sandbox.stub();

const fakeRequiredProps = {
  alertBasedReferral: false,
  goToPage: goToPageStub,
  userHasGeneratedASolution: false,
  isMobile: false,
  downloadReport: downloadReportStub,
  userHasBeenReferred: false,
  atr: {
    progress: 0,
  },
  experience: {
    progress: 0,
  },
  flexibility: {
    progress: 0,
  },
  resilience: {
    progress: 0,
    data: {},
  },
  isa: {
    progress: 0,
  },
  finalChecks: {
    progress: 0,
  },
  aboutYou: {
    progress: 0,
  },
  acceptedTerms: {
    read_documents: false,
    accepted_advice: false,
  },
  investmentGoal: {},
  content: {
    atrQuestions: [],
    experienceQuestions: [],
    flexibilityQuestions: [],
    dashboard: {
      afterReturn: 'testAfterReturn',
      finished1: 'testFinished1',
      finished2: 'testFinished2',
      allDone: 'testAllDone',
      paynow: 'testPayNow',
      referral: "Looks like we need to check a few details, we'll be in touch",
    },
  },
};

test('ProgressViewComponent', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('getItem', () => null);

  const fakeProps = {
    ...fakeRequiredProps,
    userHasGeneratedASolution: true,
    userHasBeenReferred: false,
    acceptedTerms: {},
  };

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(
    headerText,
    'testFinished1',
    'Headertext correct - solution generated, and not first login'
  );

  __RewireAPI__.__ResetDependency__('getItem');
});

test('ProgressViewComponent', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('getItem', () => 'sdfsdfs');

  const fakeProps = {
    ...fakeRequiredProps,
    userHasGeneratedASolution: false,
    userHasBeenReferred: true,
    acceptedTerms: {},
  };

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(
    headerText,
    "Looks like we need to check a few details, we'll be in touch",
    'Headertext correct - solution generated, user referred and is first login'
  );

  __RewireAPI__.__ResetDependency__('getItem');
});

test('ProgressViewComponent - flexibility not required', assert => {
  assert.plan(1);

  const props = {
    ...fakeRequiredProps,
  };

  __RewireAPI__.__Rewire__('calculateAboutProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateISAprogress', () => 0);
  __RewireAPI__.__Rewire__('calculateFinalChecksProgress', () => 0);
  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => {
    return 100;
  });
  __RewireAPI__.__Rewire__('calculateResilienceProgress', () => 100);

  const wrapper = shallow(<ProgressViewComponent {...props} />);

  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;

  assert.equal(children[1].props.progress, 100, 'risk progress is 100');

  __RewireAPI__.__ResetDependency__('calculateAboutProgress');
  __RewireAPI__.__ResetDependency__('calculateISAprogress');
  __RewireAPI__.__ResetDependency__('calculateFinalChecksProgress');
  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
  __RewireAPI__.__ResetDependency__('calculateResilienceProgress');
});

test('ProgressViewComponent - render items', assert => {
  assert.plan(2);

  const wrapper = shallow(<ProgressViewComponent {...fakeRequiredProps} />);
  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(
    children.length,
    7,
    "seven ProgressSection's elements found (multiple scenarios for advice summary)"
  );
  assert.equal(headerText, 'testFinished1', 'Headertext correct');
});

test('ProgressViewComponent - after login', assert => {
  assert.plan(2);

  const fakeProps = {
    ...fakeRequiredProps,
    afterReturnMessageActive: true,
  };

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(
    children.length,
    7,
    "seven ProgressSection's elements found (multiple scenarios for advice summary)"
  );
  assert.equal(
    headerText,
    'testAfterReturn testFinished1',
    'Headertext correct'
  );
});

test('ProgressViewComponent - pass correct progress to each section', assert => {
  assert.plan(4);

  const fakeProps = {
    ...fakeRequiredProps,
    flexibilityProgress: 10,
  };

  __RewireAPI__.__Rewire__('calculateAboutProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateISAprogress', () => 0);
  __RewireAPI__.__Rewire__('calculateFinalChecksProgress', () => 0);

  let calculateCount = 0;
  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => {
    calculateCount++;
    if (calculateCount === 1) {
      // atr
      return 10;
    }
    if (calculateCount === 2) {
      // experience
      return 10;
    }
  });
  __RewireAPI__.__Rewire__('calculateResilienceProgress', () => 10);

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(children[0].props.progress, 100, 'investment progress is 100');
  assert.equal(children[1].props.progress, 10, 'risk progress is 10');
  assert.equal(children[2].props.progress, 50, 'about you progress is 50');
  assert.equal(headerText, 'testFinished1', 'Headertext correct');

  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
  __RewireAPI__.__ResetDependency__('calculateResilienceProgress');
  __RewireAPI__.__ResetDependency__('calculateAboutProgress');
  __RewireAPI__.__ResetDependency__('calculateISAprogress');
  __RewireAPI__.__ResetDependency__('calculateFinalChecksProgress');
});

test('ProgressViewComponent - two sections finished', assert => {
  assert.plan(4);

  const fakeProps = {
    ...fakeRequiredProps,
    atr: {
      progress: 10,
    },
    experience: {
      progress: 10,
    },
    flexibilityProgress: 10,
    resilience: {
      progress: 10,
    },
    aboutYou: {
      progress: 200,
    },
    acceptedTerms: {
      read_documents: false,
      accepted_advice: false,
    },
  };

  __RewireAPI__.__Rewire__('calculateAboutProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateISAprogress', () => 100);
  __RewireAPI__.__Rewire__('calculateFinalChecksProgress', () => 100);

  let calculateCount = 0;
  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => {
    calculateCount++;
    if (calculateCount === 1) {
      // atr
      return 10;
    }
    if (calculateCount === 2) {
      // experience
      return 10;
    }
  });
  __RewireAPI__.__Rewire__('calculateResilienceProgress', () => 10);

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(children[0].props.progress, 100, 'investment progress is 100');
  assert.equal(children[1].props.progress, 10, 'risk progress is 10');
  assert.equal(children[2].props.progress, 100, 'about you progress is 100');
  assert.equal(headerText, 'testFinished2', 'Headertext correct');

  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
  __RewireAPI__.__ResetDependency__('calculateResilienceProgress');
  __RewireAPI__.__ResetDependency__('calculateAboutProgress');
  __RewireAPI__.__ResetDependency__('calculateISAprogress');
  __RewireAPI__.__ResetDependency__('calculateFinalChecksProgress');
});

test('ProgressViewComponent - each section complete', assert => {
  __RewireAPI__.__Rewire__('getItem', () => 'sdfsdfs');

  assert.plan(3);
  const downloadReportStub = sandbox.stub();

  let calculateCount = 0;
  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => {
    calculateCount++;
    if (calculateCount === 1) {
      // atr
      return 100;
    }
    if (calculateCount === 2) {
      // experience
      return 100;
    }
  });
  __RewireAPI__.__Rewire__('calculateResilienceProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateAboutProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateISAprogress', () => 100);
  __RewireAPI__.__Rewire__('calculateFinalChecksProgress', () => 100);

  const fakeProps = {
    ...fakeRequiredProps,
    userHasGeneratedASolution: true,
    downloadReport: downloadReportStub,
    atr: {
      progress: 100,
    },
    experience: {
      progress: 100,
    },
    flexibilityProgress: 100,
    resilience: {
      progress: 100,
    },
    isa: {
      progress: 100,
    },
    finalChecks: {
      progress: 100,
    },
    aboutYou: {
      progress: 100,
    },
    investmentGoal: {},
    acceptedTerms: {
      read_documents: true,
      accepted_advice: true,
    },
  };

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const children = wrapper.find('.progress-view').get(0).props.children[1].props
    .children;
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(children[3].props.progress, 100, 'advice progress is 100');

  children[3].props.downloadAction();

  assert.equals(
    downloadReportStub.callCount,
    1,
    'downloadReport called when action prop invoked & userHasGeneratedASolution'
  );
  assert.equal(headerText, 'testAllDone', 'Headertext correct');

  __RewireAPI__.__ResetDependency__('getItem');
  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
  __RewireAPI__.__ResetDependency__('calculateResilienceProgress');
  __RewireAPI__.__ResetDependency__('calculateAboutProgress');
  __RewireAPI__.__ResetDependency__('calculateISAprogress');
  __RewireAPI__.__ResetDependency__('calculateFinalChecksProgress');
});

test('ProgressViewComponent - each section complete', assert => {
  __RewireAPI__.__Rewire__('getItem', () => 'sdfsdfs');

  assert.plan(1);
  const downloadReportStub = sandbox.stub();

  let calculateCount = 0;
  __RewireAPI__.__Rewire__('calculateQuestionProgress', () => {
    calculateCount++;
    if (calculateCount === 1) {
      // atr
      return 100;
    }
    if (calculateCount === 2) {
      // experience
      return 100;
    }
  });
  __RewireAPI__.__Rewire__('calculateResilienceProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateAboutProgress', () => 100);
  __RewireAPI__.__Rewire__('calculateISAprogress', () => 100);
  __RewireAPI__.__Rewire__('calculateFinalChecksProgress', () => 100);

  const fakeProps = {
    ...fakeRequiredProps,
    userHasGeneratedASolution: true,
    alertBasedReferral: true,
    downloadReport: downloadReportStub,
    atr: {
      progress: 100,
    },
    experience: {
      progress: 100,
    },
    flexibilityProgress: 100,
    resilience: {
      progress: 100,
    },
    isa: {
      progress: 100,
    },
    finalChecks: {
      progress: 100,
    },
    aboutYou: {
      progress: 100,
    },
    investmentGoal: {},
    acceptedTerms: {
      read_documents: true,
      accepted_advice: true,
    },
  };

  const wrapper = shallow(<ProgressViewComponent {...fakeProps} />);
  const headerText = wrapper.find('.progress-view').get(0).props.children[0]
    .props.children;

  assert.equal(headerText, 'testPayNow', 'Headertext correct');

  __RewireAPI__.__ResetDependency__('getItem');
  __RewireAPI__.__ResetDependency__('calculateQuestionProgress');
  __RewireAPI__.__ResetDependency__('calculateResilienceProgress');
  __RewireAPI__.__ResetDependency__('calculateAboutProgress');
  __RewireAPI__.__ResetDependency__('calculateISAprogress');
  __RewireAPI__.__ResetDependency__('calculateFinalChecksProgress');
});
