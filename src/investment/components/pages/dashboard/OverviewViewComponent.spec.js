import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';

import OverviewViewComponent, { __RewireAPI__ } from './OverviewViewComponent';

const fakeRequiredProps = {
  content: {
    investmentGoals: {
      types: [{
        title: 'Fake Goal',
        value: 'fakeGoal'
      }]
    }
  },
  investmentGoal: {
    goal: 'fakeGoal',
    amount: 1000,
    monthly: 100
  }
};

test('<OverviewViewComponent>', t => {

  t.equals(typeof OverviewViewComponent.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<OverviewViewComponent>', t => {

  t.equals(shallow(<OverviewViewComponent {...fakeRequiredProps} />).find('.dashboard__overview-container').length, 1, 'OverviewViewComponent is rendered');

  t.end();
});
