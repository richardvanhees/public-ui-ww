import test from 'tape';
import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import ReferralContainer, { __RewireAPI__ } from './ReferralContainer';

test('ReferralContainer', assert => {
  assert.plan(1);

  const fakeStore = {
    getState: () => ({
      content: { data: { investment: { referralNew: { items: [] } } } },
      solution: {
        isLoading: false,
      },
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  const fakeProps = {};

  const actual = mount(
    <Provider store={fakeStore}>
      <ReferralContainer {...fakeProps} />
    </Provider>
  ).find('ReferralComponent');

  assert.true(actual.exists(), 'ReferralComponent exists');
});
