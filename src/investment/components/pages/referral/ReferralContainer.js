import { connect } from 'react-redux';
import ReferralComponent from './ReferralComponent';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { alertBasedReferral, lisaReferral } from '../../../modules/solution/selectors';

const contentKey = 'investment';

const mapStateToProps = ({ content, solution }) => ({
  content: lookContentByKey(contentKey, content),
  isLoading: solution.isLoading,
  referral: solution.referral,
  alertBasedReferral: alertBasedReferral(solution),
  lisaReferral: lisaReferral(solution),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/`),
    },
    dispatch
  );

const ReferralContainer = compose(connect(mapStateToProps, mapDispatchToProps));

export default ReferralContainer(ReferralComponent);
