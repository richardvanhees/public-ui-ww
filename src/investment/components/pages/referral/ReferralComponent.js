import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import MoreInfo from '../../../../../modules/elements/MoreInfo';

import Spinner from '../../../../../modules/elements/Spinner';

const ReferralComponent = ({ content, isLoading, alertBasedReferral, referral, lisaReferral }) => {
  let sectionContent = null;

  if (alertBasedReferral && !referral) {
    sectionContent = content.alertBasedReferralPage;
  } else if (lisaReferral) {
    sectionContent = content.referralLisa;
  } else {
    sectionContent = content.referralNew;
  }

  return !isLoading ? (
    <div className="referral wrapper">
      <PageHeading
        title={sectionContent.title}
        subTitle={sectionContent.subTitle}
        smallSubTitle
        dataTest={'referral'}
      />

      <Instructions
        list
        title={sectionContent.instructionsTitle}
        items={sectionContent.items}
        contactDetails={sectionContent.instructionsContact}
      />

      <MoreInfo
        link={sectionContent.moreInfoHref}
      />
    </div>
  ) : (
    <Spinner />
  );
};

ReferralComponent.propTypes = {
  content: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  alertBasedReferral: PropTypes.bool.isRequired,
  referral: PropTypes.bool.isRequired,
  lisaReferral: PropTypes.bool.isRequired,
};

export default ReferralComponent;
