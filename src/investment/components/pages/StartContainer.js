import { connect } from 'react-redux';
import StartComponent from './StartComponent';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../modules/content';
import { compose } from 'recompose';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch => ({
  next: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/goal`)),
});

const NewStartComponent = compose(connect(mapStateToProps, mapDispatchToProps));

export default NewStartComponent(StartComponent);
