import React from 'react';
// import PropTypes from 'prop-types';
import TypeSlider from '../../../../../modules/elements/TypeSlider';
import { plant } from '../../../../../assets/images/iw-images';

const riskLevels = {
  1: 'Cautious',
  2: 'Measured',
  3: 'Balanced',
  4: 'Confident',
  5: 'Adventurous',
};

class InvestInFutureComponent extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      startingInvestment: 5000,
      lengthOfInvestment: 3,
      riskLevel: 4,
    };
    this.onSliderChange = this.onSliderChange.bind(this);
  }

  onSliderChange(stateKey, value) {
    this.setState({
      ...this.state,
      [stateKey]: value,
    });
  }

  render() {
    return (
      <div className="invest-in-future">
        <h3 className="invest-in-future__title">Invest in future you</h3>
        <div className="invest-in-future__main-content">
          <div className="invest-in-future__slider-containers-wrapper">
            <div className="invest-in-future__slider-containers">
              <p>
                Starting investment: <b>£{this.state.startingInvestment}</b>
              </p>
              <TypeSlider
                initialValue={this.state.startingInvestment}
                onChange={value =>
                  this.onSliderChange('startingInvestment', value)
                }
                steps={10}
                minValue={0}
                maxValue={30000}
                hideDots
              />
            </div>
            <div className="invest-in-future__slider-containers">
              <p>
                Length of investment:{' '}
                <b>{this.state.lengthOfInvestment} years</b>
              </p>
              <TypeSlider
                ref={c => {
                  this.slider = c;
                }}
                selected={1}
                onChange={value =>
                  this.onSliderChange('lengthOfInvestment', value)
                }
                steps={1}
                initialValue={this.state.lengthOfInvestment}
                minValue={0}
                maxValue={10}
                hideDots
              />
            </div>
            <div className="invest-in-future__slider-containers">
              <p>
                Risk profile: <b>{riskLevels[this.state.riskLevel]}</b>
              </p>
              <TypeSlider
                ref={c => {
                  this.slider = c;
                }}
                onChange={value => this.onSliderChange('riskLevel', value)}
                steps={1}
                minValue={0}
                maxValue={5}
                initialValue={this.state.riskLevel}
                hideDots
              />
            </div>
          </div>
          <div className="invest-in-future__widget-container">
            <img alt="Growing investment" src={plant} />
          </div>
        </div>
      </div>
    );
  }
}

export default InvestInFutureComponent;
