import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  iwLogo,
  plantInhands,
  mma,
  ppa,
  ukpa,
  wif,
  checkmark,
  exclaim,
} from '../../../../../assets/images/iw-images';
import Table from '../../../../../modules/elements/Table';
import Button from '../../../../../modules/elements/Button';
import Popup from '../../../../../modules/elements/Popup';
import Footer from '../../../modules/elements/Footer';
import reactHTMLParser from 'react-html-parser';

const awardImages = { mma, ppa, ukpa, wif };

export default class LandingPageComponent extends PureComponent {
  static propTypes = {
    content: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    start: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.popup = null;
    this.ongoingReviewServicePopup = null;
  }

  render() {
    const { content, login, start } = this.props;

    if (!content) {
      return null;
    }

    const {
      title,
      youMustTitle,
      youMustList,
      additionallyTitle,
      additionallyList,
      extraInfo,
    } = content.landing.eligibilityPopup;

    const adviceFeeTable = content.landing.feeTable;

    const topSectionContent = content.landing.topSection;
    const middleSectionContent = content.landing.middleSection;
    const bottomSectionContent = content.landing.bottomSection;

    const ongoingReviewServicePopupContent =
      content.landing.ongoingReviewServicePopup;

    const awards = [
      [middleSectionContent.awards[0], middleSectionContent.awards[1]],
      [middleSectionContent.awards[2], middleSectionContent.awards[3]],
    ];

    return (
      <div className="landing">
        <div className="landing-nav-container">
          <div className="landing-nav">
            <div className="landing-nav__title">
              <img
                className="landing-nav__logo"
                alt="presentation"
                alt="Investment Wizard"
                src={iwLogo}
              />
            </div>
            <Button
              label="Sign in"
              onClick={login}
              type="outline"
              invertOnHover
              className="landing-nav__sign-in btn--next"
              dataTest="sign-in-button"
            />
          </div>
        </div>

        <div className="landing-top-container">
          <div className="landing-top">
            <div className="landing-top__row">
              <div className="landing-top__cell">
                <h1 className="landing-top__title">
                  {topSectionContent.topLeft.title}
                </h1>
                <div className="landing-top__text">
                  {reactHTMLParser(topSectionContent.topLeft.text)}
                </div>
              </div>
              <div className="landing-top__cell landing-top__cell--with-image">
                <img
                  alt="presentation"
                  className="landing-top__cell-image"
                  src={plantInhands}
                />
              </div>
            </div>
            <div className="landing-top__row">
              <div className="landing-top__cell landing-top__cell--with-background">
                <img
                  alt="presentation"
                  className="landing-top__cell-mini-image"
                  src={checkmark}
                />
                <div>
                  <h2>{topSectionContent.bottomLeft.title}</h2>
                  <ul>
                    {topSectionContent.bottomLeft.list.map((item, index) => (
                      <li key={index}>{item}</li>
                    ))}
                  </ul>
                </div>
              </div>
              <div className="landing-top__cell landing-top__cell--with-background">
                <img
                  alt="presentation"
                  className="landing-top__cell-mini-image"
                  src={exclaim}
                />
                <div>
                  <h2>{topSectionContent.bottomRight.title}</h2>
                  <ul>
                    {topSectionContent.bottomRight.list.map((item, index) => (
                      <li key={index}>{item}</li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="landing-middle-container">
          <div className="landing-middle">
            <h4 className="landing-middle__title">
              {middleSectionContent.title}
            </h4>
            <div className="landing-middle__row">
              {awards.map(([awardOne, awardTwo], index) => (
                <div key={index} className="landing-middle__group">
                  <div key={awardOne.logo} className="landing-middle__cell">
                    <div className="landing-middle__cell-image-container">
                      <img
                        alt="presentation"
                        className={`landing-middle__cell-image landing-middle__cell-image--${
                          awardOne.logo
                        }`}
                        src={awardImages[awardOne.logo]}
                      />
                    </div>
                    <span className="landing-middle__cell-text">
                      {awardOne.text}
                    </span>
                  </div>
                  <div key={awardTwo.logo} className="landing-middle__cell">
                    <div className="landing-middle__cell-image-container">
                      <img
                        alt="presentation"
                        className={`landing-middle__cell-image landing-middle__cell-image--${
                          awardTwo.logo
                        }`}
                        src={awardImages[awardTwo.logo]}
                      />
                    </div>
                    <span className="landing-middle__cell-text">
                      {awardTwo.text}
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="landing-bottom-container">
          <div className="landing-bottom">
            <h4 className="landing-bottom__title">
              {bottomSectionContent.title}
            </h4>
            <div className="landing-bottom__row">
              <div className="landing-bottom__cell">
                <Table
                  className="landing-bottom__table"
                  rows={adviceFeeTable.rows}
                  headerLabels={adviceFeeTable.headerLabels}
                />
              </div>
              <div className="landing-bottom__cell">
                {reactHTMLParser(bottomSectionContent.text)}
                <a
                  className={'landing-bottom__link'}
                  onClick={() => this.ongoingReviewServicePopup.toggle()}
                >
                  Find out more
                </a>
                {reactHTMLParser(bottomSectionContent.finalTextSection)}
              </div>
            </div>
          </div>
        </div>

        <div className="landing-start-investing-container">
          <div className="landing-start-investing">
            <Button
              className="landing-start-investing__button btn--next"
              type="primary"
              label="Start investing"
              dataTest="start-investing-button"
              onClick={() => this.popup.toggle()}
            />
          </div>
        </div>

        <Footer className="landing-footer" />

        <Popup
          ref={c => {
            this.ongoingReviewServicePopup = c;
          }}
          title={ongoingReviewServicePopupContent.title}
          closeButtonTop
          closeClickOutside
          layout={'column'}
        >
          <div>
            {reactHTMLParser(ongoingReviewServicePopupContent.whatIsIt)}
            {reactHTMLParser(
              ongoingReviewServicePopupContent.keyProvisionsTitle
            )}
            <ul>
              {ongoingReviewServicePopupContent.keyProvisions.map(provision => (
                <li>{reactHTMLParser(provision)}</li>
              ))}
            </ul>
          </div>
          <p>{reactHTMLParser(ongoingReviewServicePopupContent.changes)}</p>
        </Popup>

        <Popup
          ref={c => {
            this.popup = c;
          }}
          title={title}
          closeButtonTop
          closeClickOutside
          layout={'column'}
          buttons={[
            {
              label: 'Invest with advice',
              dataTest: 'invest-with-advice',
              onClick: start,
              type: 'primary',
              className: 'btn--next',
            },
          ]}
        >
          <div className="eligibility-popup">
            <div>{youMustTitle}</div>
            <ul className="landing__eligibility-popup-list">
              {youMustList.map((item, i) => (
                <li key={`eligibility-item-${i}`}>{item}</li>
              ))}
            </ul>
            <div>{additionallyTitle}</div>
            <ul className="landing__eligibility-popup-list">
              {additionallyList.map((item, i) => (
                <li key={`additionally-item-${i}`}>{item}</li>
              ))}
            </ul>
            <div>{extraInfo}</div>
          </div>
        </Popup>
      </div>
    );
  }
}
