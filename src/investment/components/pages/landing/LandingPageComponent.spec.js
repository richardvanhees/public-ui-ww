import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import LandingPageComponent from './LandingPageComponent';

const sandbox = sinon.sandbox.create();
const startStub = sandbox.stub();
const loginStub = sandbox.stub();

const fakeRequiredProps = {
  content: {
    adviceSummary: {
      page3: {
        feeTable: {
          headerLabels: ['fakeHeader'],
          rows: [['fakeCell']],
        },
      },
    },
    landing: {
      ongoingReviewServicePopup: {
        title: 'Ongoing Review Service',
        whatIsIt:
          '<p>With investments it is essential to have a regular review, to assess the investments, your risk profile and other circumstances, so that your investment strategy continues to be appropriate for you.</p>',
        keyProvisionsTitle: 'The ongoing review service will provide:',
        keyProvisions: [
          'The opportunity for you to check the data we hold for you',
          'A new assessment of your risk profile',
          'A review of the ongoing suitability of the product, fund and contributions',
        ],
        changes:
          'It is very important that you notify us of any changes in your circumstances so that the investment strategy we recommend is appropriate for you.',
        contactUsText:
          'Additionally, please notify us immediately if your employment status, health or other circumstances change significantly. Please contact us on <a href="mailto:thewizard@wealthwizards.com">thewizard@wealthwizards.com</a> or call us on <a href="tel:01926671469">01926 671469</a>.',
      },
      feeTable: {
        headerLabels: ['HEADER1', 'HEADER2'],
        rows: [['ONE', '1'], ['TWO', '2'], ['THREE', '3']],
      },
      topSection: {
        topLeft: {
          title: 'TITLE',
          text: 'TEXT',
        },
        bottomLeft: {
          title: 'TITLE',
          list: ['ONE', 'TWO', 'THREE'],
        },
        bottomRight: {
          title: 'TITLE',
          list: ['ONE', 'TWO', 'THREE'],
        },
      },
      middleSection: {
        title: 'TITLE',
        awards: [
          {
            logo: 'ppa',
            text: 'TEXT',
          },
          {
            logo: 'wif',
            text: 'TEXT',
          },
          {
            logo: 'mma',
            text: 'TEXT',
          },
          {
            logo: 'ukpa',
            text: 'TEXT',
          },
        ],
      },
      bottomSection: {
        title: 'TITLE',
        text: 'TEXT',
      },
      eligibilityPopup: {
        title: 'TITLE',
        youMustTitle: 'YOUMUSTTITLE',
        youMustList: ['ONE', 'TWO', 'THREE'],
        additionallyTitle: 'ADDITIONALLYTITLE',
        additionallyList: ['ONE', 'TWO', 'THREE'],
        extraInfo: 'EXTRAINFO',
      },
    },
  },
  start: startStub,
  login: loginStub,
};

test('LandingPageComponent', assert => {
  assert.plan(2);
  const wrapper = mount(<LandingPageComponent {...fakeRequiredProps} />);

  wrapper.find('[data-test="start-investing-button"]').simulate('click');
  wrapper.find('[data-test="invest-with-advice"]').simulate('click');
  wrapper.find('[data-test="sign-in-button"]').simulate('click');

  assert.equals(startStub.called, true, 'Start investing button clicked');
  assert.equals(loginStub.called, true, 'Sign in button clicked');

  assert.end();
});

test('LandingPageComponent', assert => {
  assert.plan(4);
  const wrapper = mount(<LandingPageComponent {...fakeRequiredProps} />);

  assert.equals(
    wrapper.find('.landing-middle__cell-image--ppa').length,
    1,
    'award ppa shown'
  );
  assert.equals(
    wrapper.find('.landing-middle__cell-image--mma').length,
    1,
    'award mma shown'
  );
  assert.equals(
    wrapper.find('.landing-middle__cell-image--ukpa').length,
    1,
    'award ukpa shown'
  );
  assert.equals(
    wrapper.find('.landing-middle__cell-image--wif').length,
    1,
    'award wif shown'
  );

  assert.end();
});
