import { connect } from 'react-redux';
import LandingPageComponent from './LandingPageComponent';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';
import WealthWizards from '../../../../../modules/wealthwizards';
import { push } from 'react-router-redux';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/login`)),
  start: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/right-time/1`)),
});

const NewLandingPageComponent = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default NewLandingPageComponent(LandingPageComponent);
