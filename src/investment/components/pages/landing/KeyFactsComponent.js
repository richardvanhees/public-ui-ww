import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../../../modules/elements/Icon';

const KeyFactsComponent = props => (
  <div className="key-facts">
    <h3 className="key-facts__title">{props.content.title}</h3>
    <div className="key-facts__container">
      {props.content.facts.map((fact, i) => (
        <div key={`fact-${props.content.title}-${i}`} className={`key-facts__fact ${props.iconColorClassName}`}>
          <Icon name={fact.icon} iconSet={fact.iconSet} />
          {fact.title && (
            <h3 className="key-facts__fact-title">{fact.title}</h3>
          )}
          <p className="key-facts__fact-subtitle">{fact.subtitle}</p>
        </div>
      ))}
    </div>
  </div>
);

KeyFactsComponent.propTypes = {
  content: PropTypes.object.isRequired,
  iconColorClassName: PropTypes.string.isRequired,
};

export default KeyFactsComponent;
