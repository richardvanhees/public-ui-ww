import React, { Component } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import WealthWizards from '../../../../../modules/wealthwizards';
import { emailIsValid } from '../../../../../modules/utils/regexes';

import ReCaptcha from '../../../../../modules/elements/ReCaptcha';
import PageHeading from '../../../../../modules/elements/PageHeading';
import PasswordInput from '../../../../../modules/elements/PasswordInput';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import Input from '../../../../../modules/elements/Input';
import Checkbox from '../../../../../modules/elements/Checkbox';
import Error from '../../../../../modules/elements/Error';
import Dropdown from '../../../../../modules/elements/Dropdown';

const firstNameRegex = /^[a-zA-Z0-9 '\-]{1,36}$/;
const lastNameRegex = /^[a-zA-Z0-9 '\-]{1,38}$/;

export const firstNameIsValid = firstName => firstName.match(firstNameRegex);

export const lastNameIsValid = lastName => lastName.match(lastNameRegex);

export default class RegisterComponent extends Component {
  static propTypes = {
    back: PropTypes.func.isRequired,
    handleUserInput: PropTypes.func.isRequired,
    registerUser: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    error: PropTypes.object,
    isLoading: PropTypes.bool.isRequired,
    investmentGoalCompletedAt: PropTypes.string,
    rightTimeCompletedAt: PropTypes.string,
    redirect: PropTypes.func.isRequired,
    preRegistrationDataIsComplete: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.passwordInput = null;
    if (!props.preRegistrationDataIsComplete) {
      props.redirect(`${WealthWizards.CONTEXT_ROUTE}/right-time/1`);
    }

    this.state = {
      hoveredAnswer: null,
      firstName: '',
      lastName: '',
      email: '',
      title: '',
      password: '',
      recaptcha: false,
      checkboxChecked: false,
      passwordValid: false,
      popupActive: false,
      lgEmployee: false,
      errors: {
        firstName: false,
        lastName: false,
        email: false,
        privacy: false,
        title: false,
        password: false,
        recaptcha: false,
        lgEmployee: false,
      },
      showPasswordErrors: false,
    };
  }

  onRecaptchaExpire = () => this.setState({ recaptcha: false });

  onRecaptchaVerify = response => this.setState({ recaptcha: response });

  setValidationError(err) {
    this.setState({ ...err, recaptcha: false });
    typeof grecaptcha !== 'undefined' && grecaptcha.reset(); // eslint-disable-line no-undef
  }

  emailAlreadyInUse = err => R.path(['response', 'status'])(err) === 400;

  errorExists = err => err;

  proceed = () => {
    const {
      title,
      firstName,
      lastName,
      email,
      password,
      passwordValid,
      checkboxChecked,
      recaptcha,
      lgEmployee,
    } = this.state;
    this.passwordInput && this.passwordInput.validate();

    const emailIsValidResult = !!emailIsValid(email);

    this.setState({
      showPasswordErrors: emailIsValidResult,
    });

    if (!title || title === '') {
      this.setValidationError({ errors: { title: true } });
    } else if (!firstNameIsValid(firstName)) {
      this.setValidationError({ errors: { firstName: true } });
    } else if (!lastNameIsValid(lastName)) {
      this.setValidationError({ errors: { lastName: true } });
    } else if (!emailIsValidResult) {
      this.setValidationError({ errors: { email: true } });
    } else if (!passwordValid) {
      this.setValidationError({ errors: { password: true } });
    } else if (!checkboxChecked) {
      this.setValidationError({ errors: { privacy: true } });
    } else if (!lgEmployee) {
      this.setValidationError({ errors: { lgEmployee: true } });
    } else if (!recaptcha) {
      this.setValidationError({ errors: { recaptcha: true } });
    } else {
      this.props.handleUserInput({
        email,
        firstName,
        lastName,
        title,
        consentedPrivacyPolicy: checkboxChecked,
        lgEmployee,
      });
      this.props.registerUser(password, recaptcha);
    }
  };

  render() {
    const { content, back, error } = this.props;
    const sectionContent = content.register;
    const recaptchaKey = RegExp(/demo_[A-Za-z0-9\-]*(@wealthwizards.io)/g).test(
      this.state.email
    )
      ? WealthWizards.RECAPTCHA_AUTOMATION_KEY
      : WealthWizards.RECAPTCHA_KEY;

    return (
      <div className="wrapper register-component">
        <PageHeading
          smallSubTitle
          title={sectionContent.title}
          subTitle={sectionContent.subTitle}
          subTitleClassName={'register-component__subtitle'}
        />

        <div className="register-component__form-wrapper">
          <div className="register-component__form-component">
            <div className="register-component__form-component__multiple-fields">
              <div className="title-container">
                <div className={'input-wrapper '}>
                  <p className={'input-wrapper__label '}>Title</p>
                  <Dropdown
                    options={sectionContent.titleOptions}
                    onChange={title => this.setState({ title })}
                    dataTest={'title-input'}
                    className="input-wrapper__select"
                  />
                </div>
                <Error
                  compact
                  title="Select your title"
                  active={this.state.errors.title}
                />
              </div>
              <div className="name-container">
                <Input
                  dataTest="first-name-input"
                  onChange={e => this.setState({ firstName: e.target.value })}
                  label="First name"
                  value={this.state.firstName}
                  wrapperClassName={'register-component__input'}
                  error={this.state.errors.firstName}
                />
                <Error
                  compact
                  title="Enter your first name to continue"
                  active={this.state.errors.firstName}
                />
              </div>
            </div>
            <Input
              dataTest="last-name-input"
              onChange={e => this.setState({ lastName: e.target.value })}
              label="Last name"
              value={this.state.lastName}
              wrapperClassName={'register-component__input'}
              error={this.state.errors.lastName}
            />
            <Error
              compact
              title="Enter your last name to continue"
              active={this.state.errors.lastName}
            />
            <Input
              dataTest="email-input"
              onChange={e => this.setState({ email: e.target.value })}
              label="Email address"
              value={this.state.email}
              type="email"
              wrapperClassName={'register-component__input'}
              error={this.state.errors.email}
            />
            <Error
              compact
              title="Enter a valid email address to continue"
              active={this.state.errors.email}
            />
            <Error
              compact
              title="This email address already exists"
              active={this.emailAlreadyInUse(error)}
            />

            <PasswordInput
              ref={c => {
                this.passwordInput = c;
              }}
              onComplete={password => {
                this.setState({ password });
              }}
              isValid={valid => {
                this.setState({
                  passwordValid: valid,
                });
              }}
              showErrors={this.state.showPasswordErrors}
            />
          </div>

          <div className="register-component__form-component">
            <Checkbox
              label={
                <span>
                  {sectionContent.checkbox.label}
                  <a
                    target={'_blank'}
                    className="register-component__url"
                    href={sectionContent.checkbox.url[0].url}
                  >
                    {sectionContent.checkbox.url[0].label}
                  </a>{' '}
                  and{' '}
                  <a
                    target={'_blank'}
                    className="register-component__url"
                    href={sectionContent.checkbox.url[1].url}
                  >
                    {sectionContent.checkbox.url[1].label}
                  </a>
                </span>
              }
              checked={this.state.checkboxChecked}
              onChange={val => this.setState({ checkboxChecked: val })}
              name={sectionContent.checkbox.id}
              dataTest="checkbox-privacy-policy"
              type="custom"
            />
            <Error
              compact
              title="You need to state that you have read and agree before you can create your account"
              active={this.state.errors.privacy}
              style={{ marginTop: '5px' }}
            />
          </div>

          <div className="register-component__form-component">
            <Checkbox
              label={<span>{sectionContent.lgEmployee.label}</span>}
              onChange={val => this.setState({ lgEmployee: val })}
              name={sectionContent.lgEmployee.id}
              checked={this.state.lgEmployee}
              dataTest="checkbox-lg-employee"
              type="custom"
            />
            <Error
              compact
              title="You need to confirm you are an employee of Legal and General"
              active={this.state.errors.lgEmployee}
              style={{ marginTop: '5px' }}
            />
          </div>

          <ReCaptcha
            siteKey={recaptchaKey}
            onExpire={this.onRecaptchaExpire}
            onVerify={this.onRecaptchaVerify}
          />
          <Error
            active={this.state.errors.recaptcha}
            compact
            title={"You need to confirm you're a human"}
            style={{ marginTop: 5 }}
          />
        </div>
        <PageNavigation
          back={back}
          next={this.proceed}
          nextLabel={'Sign up'}
          testNext={'register'}
        />
      </div>
    );
  }
}
