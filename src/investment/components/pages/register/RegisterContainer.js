import { connect } from 'react-redux';
import RegisterComponent from './RegisterComponent';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import {
  handleUserInput,
  createUser,
  resetError,
} from '../../../modules/about-you';
import { preRegistrationDataIsComplete } from '../../../modules/investment-goal/selector';

const contentKey = 'investment';

const mapStateToProps = ({ content, aboutYou, investmentGoal, rightTime }) => ({
  content: lookContentByKey(contentKey, content),
  error: aboutYou.error,
  isLoading: aboutYou.isLoading,
  investmentGoalCompletedAt: investmentGoal.completedAt,
  rightTimeCompletedAt: rightTime.completedAt,
  preRegistrationDataIsComplete: preRegistrationDataIsComplete(
    investmentGoal,
    rightTime
  ),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/amount`),
      redirect: url => push(url),
      handleUserInput,
      registerUser: (pass, captcha) => createUser(pass, captcha),
      resetError,
    },
    dispatch
  );

const RegisterContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default RegisterContainer(RegisterComponent);
