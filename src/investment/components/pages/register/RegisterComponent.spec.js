import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RegisterComponent, {
  firstNameIsValid,
  lastNameIsValid,
  emailIsValid,
} from './RegisterComponent';
import Spinner from '../../../../../modules/elements/Spinner';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import configureMockStore from 'redux-mock-store';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  next: () => {
  },
  back: () => {
  },
  redirect: () => {
  },
  investmentGoalCompletedAt: '2010/10/10',
  handleUserInput: () => {
  },
  registerUser: () => {
  },
  userCreationSuccess: true,
  content: {
    register: {
      title: 'Now lets create your account',
      subTitle: "We'll save your progress",
      inputFields: [
        {
          label: 'Full name',
          id: 'fullName',
        },
        {
          label: 'email',
          id: 'email',
        },
      ],
      lgEmployee: {
        label: 'I confirm I am an employee of Legal & General',
        id: 'lg-employee',
      },
      checkbox: {
        label: 'I have read and agree to the ',
        id: 'privacy-policy',
        url: [
          {
            label: 'privacy policy',
            url: 'http://www.wealthwizards.com/privacy-policy',
          },
          {
            label: 'Terms of business',
            url:
              'https://www.wealthwizards.com/terms-of-business.pdf',
          },
        ],
      },
      titleOptions: ['Mr', 'Miss', 'Ms', 'Mrs', 'Dr'],
    },
  },
  isLoading: false,
};

test('<RegisterComponent> ', t => {
  const sandbox = sinon.sandbox.create();

  global.grecaptcha = {
    reset: sandbox.stub(),
  };

  t.equals(
    typeof RegisterComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  const ShallowRegistercomponent = shallow(
    <RegisterComponent {...fakeRequiredProps} />
  );
  t.equals(
    ShallowRegistercomponent.find(Spinner).length,
    0,
    'Spinner NOT shown when prop is false'
  );

  t.equals(
    ShallowRegistercomponent.find(PageNavigation).length,
    1,
    'Page Nav exists'
  );

  const initialState = {
    hoveredAnswer: null,
    firstName: '',
    lastName: '',
    email: '',
    title: '',
    password: '',
    checkboxChecked: false,
    passwordValid: false,
    popupActive: false,
    recaptcha: false,
    lgEmployee: false,
    errors: {
      firstName: false,
      lastName: false,
      email: false,
      privacy: false,
      password: false,
      title: false,
      recaptcha: false,
      lgEmployee: false,
    },
    showPasswordErrors: false,
  };

  ShallowRegistercomponent.setState(initialState);

  t.deepEquals(
    ShallowRegistercomponent.state(),
    {
      checkboxChecked: false,
      passwordValid: false,
      email: '',
      errors: {
        email: false,
        firstName: false,
        lastName: false,
        password: false,
        privacy: false,
        title: false,
        recaptcha: false,
        lgEmployee: false,
      },
      firstName: '',
      lastName: '',
      hoveredAnswer: null,
      password: '',
      popupActive: false,
      title: '',
      recaptcha: false,
      lgEmployee: false,
      showPasswordErrors: false,
    },
    'check initial state'
  );

  ShallowRegistercomponent.instance().proceed();

  t.deepEquals(
    ShallowRegistercomponent.state(),
    {
      checkboxChecked: false,
      email: '',
      errors: {
        title: true,
      },
      firstName: '',
      lastName: '',
      hoveredAnswer: null,
      password: '',
      passwordValid: false,
      popupActive: false,
      title: '',
      recaptcha: false,
      lgEmployee: false,
      showPasswordErrors: false,
    },
    'check state after proceed called'
  );

  const formPartaillyComplete = {
    hoveredAnswer: null,
    firstName: 'Gary',
    lastName: 'George',
    email: 'gary@ww.com',
    title: 'Mr',
    password: '123y',
    checkboxChecked: true,
    popupActive: false,
    lgEmployee: false,
    recaptcha: 'all good',
    errors: {
      firstName: false,
      lastName: false,
      email: false,
      privacy: false,
      password: true,
      title: false,
      recaptcha: false,
      lgEmployee: false,
    },
    showPasswordErrors: true,
  };

  ShallowRegistercomponent.setState(formPartaillyComplete);

  ShallowRegistercomponent.instance().proceed();

  const formFullyComplete = {
    hoveredAnswer: null,
    firstName: 'Gary',
    lastName: 'George',
    email: 'gary@ww.com',
    title: 'Mr',
    password: '123y7686yuhuhu',
    checkboxChecked: true,
    passwordValid: true,
    popupActive: false,
    recaptcha: false,
    lgEmployee: false,
    errors: {
      firstName: false,
      lastName: false,
      email: false,
      privacy: false,
      title: false,
      recaptcha: false,
      lgEmployee: false,
    },
    showPasswordErrors: true,
  };

  ShallowRegistercomponent.setState(formFullyComplete);

  ShallowRegistercomponent.instance().proceed();

  t.equals(global.grecaptcha.reset.callCount, 3, 'reset called on recaptcha');

  t.end();

  delete global.grecaptcha;
});

test('<RegisterComponent> ', t => {
  const sandbox = sinon.sandbox.create();
  const redirectStub = sandbox.stub();

  const fakeProps = {
    ...fakeRequiredProps,
    redirect: redirectStub,
    investmentGoalCompletedAt: null,
    rightTimeCompletedAt: 'date',
    recaptcha: false,
  };

  const ShallowRegistercomponent = shallow(
    <RegisterComponent {...fakeProps} />
  );

  t.equals(redirectStub.callCount, 1, 'redriect stub called');
  t.end();
});

test('firstNameIsValid', assert => {
  assert.true(firstNameIsValid('1'), '1 char');
  assert.true(
    firstNameIsValid('123456789012345678901234567890123456'),
    '36 chars'
  );
  assert.false(
    firstNameIsValid('1234567890123456789012345678901234567'),
    '37 chars'
  );
  assert.true(firstNameIsValid('-'), 'allows -');
  assert.true(firstNameIsValid("'"), "allows '");
  assert.true(firstNameIsValid(' '), 'allows spaces');
  assert.false(firstNameIsValid('&'), 'doesnt allow other chars');

  assert.end();
});

test('lastNameIsValid', assert => {
  assert.true(lastNameIsValid('1'), '1 char');
  assert.true(
    lastNameIsValid('12345678901234567890123456789012345678'),
    '38 chars'
  );
  assert.false(
    lastNameIsValid('123456789012345678901234567890123456789'),
    '39 chars'
  );
  assert.true(lastNameIsValid('-'), 'allows -');
  assert.true(lastNameIsValid(' '), 'allows spaces');
  assert.true(lastNameIsValid("'"), "allows '");
  assert.false(lastNameIsValid('&'), 'doesnt allow other chars');

  assert.end();
});
