import React from 'react';
import PropTypes from 'prop-types';
import QuestionRange from '../../../../../modules/elements/QuestionRange';
import WealthWizards from '../../../../../modules/wealthwizards';
import { puzzleTopRight } from '../../../../../assets/images/iw-images';

const FlexibilityQuestionComponent = props => (
  <div className="wrapper flexibility">
    <div className="questions-component__question-container">
      <QuestionRange
        {...props}
        headerImage={puzzleTopRight}
        section={'flexibility'}
        questions={props.content.flexibilityQuestions}
        previousPage={`${WealthWizards.CONTEXT_ROUTE}/flexibility/start`}
        nextPage={`${WealthWizards.CONTEXT_ROUTE}/flexibility/playback`}
        onChange={(key, answer) => {
          props.handleFieldChange(key, answer);
        }}
        customSetPageAction={props.customSetPageAction}
        smallButtons
      />
    </div>
  </div>
);

FlexibilityQuestionComponent.propTypes = {
  content: PropTypes.object.isRequired,
  handleFieldChange: PropTypes.func.isRequired,
  customSetPageAction: PropTypes.func.isRequired,
};

export default FlexibilityQuestionComponent;
