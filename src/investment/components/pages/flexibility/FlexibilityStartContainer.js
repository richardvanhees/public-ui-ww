import { connect } from 'react-redux';
import FlexibilityStartComponent from './FlexibilityStartComponent';
import { push, goBack } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch => ({
  next: () =>
    dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/flexibility/question/1`)),
  back: () => dispatch(goBack()),
});

const FlexibilityStartContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default FlexibilityStartContainer(FlexibilityStartComponent);
