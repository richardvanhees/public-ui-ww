import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import { puzzleTopRight } from '../../../../../assets/images/iw-images';

const FlexibilityStartComponent = ({ content, next, back }) => (
  <div className="wrapper flexibility">
    <PageHeading
      title={content.flexibilityStart.title}
      subTitle={content.flexibilityStart.subTitle}
      image={puzzleTopRight}
      smallSubTitle
    />

    <div className="center-desktop">
      <Instructions
        title={content.flexibilityStart.instructionsTitle}
        items={content.flexibilityStart.instructions}
        list
      />

      <PageNavigation
        next={next}
        back={back}
        noSideMargins
        testNext={'flexibility-start'}
        testBack={'flexibility-start'}
      />
    </div>
  </div>
);

FlexibilityStartComponent.propTypes = {
  content: PropTypes.object.isRequired,
  next: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
};

export default FlexibilityStartComponent;
