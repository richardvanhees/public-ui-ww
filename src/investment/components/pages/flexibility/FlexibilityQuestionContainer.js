import { connect } from 'react-redux';
import FlexibilityQuestionComponent from './FlexibilityQuestionComponent';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';
import { handleFieldChange } from '../../../modules/flexibility';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
  customSetPageAction: redirectPath =>
    saveFactFindRequest({ redirectPath, doInBackground: true }),
});

const mapDispatchToProps = dispatch => ({
  handleFieldChange: (name, value) => dispatch(handleFieldChange(name, value)),
});

const FlexibilityQuestionContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default FlexibilityQuestionContainer(FlexibilityQuestionComponent);
