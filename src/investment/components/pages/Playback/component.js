import React, { Component } from 'react';
import PropTypes from 'prop-types';
import interpolate from '../../../../../modules/utils/interpolate';

import Icon from '../../../../../modules/elements/Icon';
import Explanation from '../../../../../modules/elements/Explanation';
import PageHeading from '../../../../../modules/elements/PageHeading';
import TypePicker from '../../../../../modules/elements/TypePicker';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import Spinner from '../../../../../modules/elements/Spinner';
import Popup from '../../../../../modules/elements/Popup';
import Button from '../../../../../modules/elements/Button';

export default class PlaybackComponent extends Component {
  static propTypes = {
    section: PropTypes.string.isRequired,
    content: PropTypes.object.isRequired,
    next: PropTypes.func.isRequired,
    back: PropTypes.func,
    profile: PropTypes.object.isRequired,
    resetSection: PropTypes.func.isRequired,
    isNarrow: PropTypes.bool,
    isMobile: PropTypes.bool,
    generateProfile: PropTypes.func.isRequired,
    manualOverride: PropTypes.func.isRequired,
    backToDashboard: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
    override: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  };

  static getDerivedStateFromProps = (props, state) => {
    if (props.profile && !!Object.keys(state).length) {
      if (props.profile.profile !== state.level && !props.override) {
        return { level: props.profile.profile };
      }
      if (props.override) {
        return { level: props.override.profile };
      }
      return null;
    }
    return null;
  };

  constructor(props) {
    super(props);

    this.state = {
      checkedForAlerts: false,
      level: props.override || props.profile.profile,
      alertPopupShown: false,
      alertPopup: {
        content: '',
        buttons: [],
      },
    };
  }

  componentDidMount() {
    this.props.generateProfile(this.props.section);
  }

  componentDidUpdate = () => {
    if (!this.state.checkedForAlerts && this.props.profile.alerts) {
      this.checkForAlerts();
      this.setState({ checkedForAlerts: true });
    }
  };

  setLevel = newLevel => {
    this.props.manualOverride({
      key: `${this.props.section}__manual_override`,
      profile: newLevel,
    });
  };

  kebabCase = string =>
    string
      .replace(/([a-z])([A-Z])/g, '$1-$2')
      .replace(/\s+/g, '-')
      .toLowerCase();

  replaceAwithAn = string =>
    typeof string === 'string'
      ? string.replace(/a adventurous/g, 'an adventurous')
      : null;

  checkForAlerts = () => {
    const { content, section, isMobile } = this.props;
    const { alertOptions } = content[`${section}Playback`];
    const { alerts, profile } = this.props.profile;

    if (alerts && alerts.length) {
      const alertTag = alerts.includes('too_many_middle_atr_answers')
        ? 'middleAnswers'
        : 'other';

      this.setState(
        {
          alertPopup: {
            content: interpolate(alertOptions[alertTag].content, {
              level: profile,
              article: profile === 'adventurous' ? 'an' : 'a',
            }),
            buttons: [
              {
                label: alertOptions[alertTag].retakeLabel,
                onClick: this.resetSection,
                dataTest: 'popup-retake-btn',
              },
              {
                className: 'btn--primary',
                dataTest: this.kebabCase(
                  `${alertTag}-${alertOptions[alertTag].closeLabel}`
                ),
                label: interpolate(
                  isMobile
                    ? alertOptions[alertTag].closeLabelMobile
                    : alertOptions[alertTag].closeLabel,
                  { level: profile }
                ),
                onClick: () => {
                  this.alertPopup.toggle();
                },
              },
            ],
          },
        },
        () => {
          setTimeout(() => {
            if (!this.state.alertPopupShown) {
              this.setState({ alertPopupShown: true }, this.alertPopup.toggle);
            }
          }, 1000);
        }
      );
    }
  };

  resetSection = () => {
    const { resetSection, section, back } = this.props;

    resetSection(section);
    back();
  };

  render() {
    const {
      content,
      section,
      next,
      profile,
      isNarrow,
      isMobile,
      isLoading,
      backToDashboard,
    } = this.props;
    const sectionContent = content[`${section}Playback`];
    const level = profile.profile;
    const infoTitle = this.replaceAwithAn(
      interpolate(sectionContent.infoTitle, { level: this.state.level })
    );
    const selectedLevel = sectionContent.playbackTypes.find(
      type => type.name === this.state.level
    );

    const readMoreContent =
      (selectedLevel &&
        selectedLevel.readMore &&
        interpolate(selectedLevel.readMore)) ||
      '';

    return !isLoading ? (
      <div className={'playback'}>
        {level ? (
          <div className={'playback__content'}>
            <PageHeading
              smallSubTitle
              title={sectionContent.title}
              subTitle={
                <span>
                  {sectionContent.subTitle.replace(
                    /\ba\b/g,
                    this.state.level === 'adventurous' ? 'an' : 'a'
                  )}
                  <span className="page-heading--bold">
                    {interpolate(sectionContent.level, { level })}
                  </span>
                </span>
              }
              dataTest={section}
            />

            {section === 'atr' && (
              <Explanation title={infoTitle}>
                {sectionContent.explanationContent}
              </Explanation>
            )}

            <TypePicker
              types={sectionContent.playbackTypes}
              selectedLevel={this.state.level}
              descriptionIntro={sectionContent.descriptionIntro.replace(
                /\ba\b/g,
                this.state.level === 'adventurous' ? 'an' : 'a'
              )}
              level={interpolate(sectionContent.level, {
                level: this.state.level,
              })}
              onChange={this.setLevel}
              isMobile={isMobile}
              subject={sectionContent.subject}
              readMore={readMoreContent}
              manualOverrideEnabled={section === 'atr'}
              popupToggle={() => {
                this.readMorePopup.toggle();
              }}
            />

            <PageNavigation
              next={() => next()}
              back={this.resetSection}
              nextLabel={
                isMobile
                  ? 'Confirm'
                  : `Confirm '${interpolate(sectionContent.level, {
                    level: this.state.level,
                  })}'`
              }
              backLabel={
                <div>
                  <Icon name="angle-left" />{' '}
                  {isNarrow ? 'Retake' : 'Retake questions'}
                </div>
              }
              testNext={`${section}-playback`}
              testBack={`${section}-playback`}
              noSideMargins
            />

            <Popup
              ref={c => {
                this.readMorePopup = c;
              }}
              title={interpolate(sectionContent.level, {
                level: this.state.level,
              })}
              closeButtonTop
              closeClickOutside
              buttons={[
                {
                  label: 'Close',
                  onClick: () => {
                    this.readMorePopup.toggle();
                  },
                },
              ]}
            >
              {readMoreContent}
            </Popup>

            <Popup
              ref={c => {
                this.alertPopup = c;
              }}
              title={'Your attitude to risk'}
              closeButtonTop
              closeClickOutside
              isNarrow
              buttons={this.state.alertPopup.buttons}
            >
              {this.state.alertPopup.content}
            </Popup>
          </div>
        ) : (
          <div className={'playback__content playback__content--text-centered'}>
            <p>Something went wrong when we tried to define your profile</p>
            <Button
              className={'btn--primary'}
              label="Back to dashboard"
              onClick={backToDashboard}
              data-test={'go-to-dashboard'}
            />
          </div>
        )}
      </div>
    ) : (
      <Spinner />
    );
  }
}
