import { connect } from 'react-redux';
import PlaybackComponent from './component';
import { push } from 'react-router-redux';
import R from 'ramda';

import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import {
  generateATRProfile,
  resetATRSection,
  atrManualOverride,
} from '../../../modules/atr';
import {
  generateExperienceProfile,
  resetExperienceSection,
  experienceManualOverride,
} from '../../../modules/experience';
import {
  generateFlexibilityProfile,
  resetFlexibilitySection,
  flexibilityManualOverride,
} from '../../../modules/flexibility';
import {
  generateResilienceProfile,
  resetResilienceSection,
  resilienceManualOverride,
} from '../../../modules/resilience';

import { compose } from 'recompose';

const contentKey = 'investment';

const mapStateToProps = (state, ownProps) => ({
  content: lookContentByKey(contentKey, state.content),
  isNarrow:
    state.browser.breakpoint === 'phablet' ||
    state.browser.breakpoint === 'stablet',
  isMobile: state.browser.breakpoint === 'phablet',
  profile: R.evolve({ profile: R.toLower() })(state[ownProps.section].profile),
  override: state[ownProps.section].override,
  isLoading: state[ownProps.section].isLoading,
});

const getNextURL = (ownProps, flexibilityRequired) => {
  const sectionOrder = flexibilityRequired
    ? ['atr', 'experience', 'flexibility', 'resilience']
    : ['atr', 'experience', 'resilience'];

  let route;
  if (sectionOrder.indexOf(ownProps.section) === sectionOrder.length + 1) {
    route = `${WealthWizards.CONTEXT_ROUTE}/`;
  } else if (ownProps.section === sectionOrder[sectionOrder.length - 1]) {
    route = `${WealthWizards.CONTEXT_ROUTE}/dashboard`;
  } else {
    route = `${WealthWizards.CONTEXT_ROUTE}/${
      sectionOrder[sectionOrder.indexOf(ownProps.section) + 1]
    }/start`;
  }
  return route;
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const sectionFunctions = {
    atr: {
      resetSection: () => dispatch(resetATRSection()),
      manualOverride: override => dispatch(atrManualOverride(override)),
      generateProfile: () => dispatch(generateATRProfile()),
    },
    experience: {
      resetSection: () => dispatch(resetExperienceSection()),
      manualOverride: override => dispatch(experienceManualOverride(override)),
      generateProfile: () => dispatch(generateExperienceProfile()),
    },
    flexibility: {
      resetSection: () => dispatch(resetFlexibilitySection()),
      manualOverride: override => dispatch(flexibilityManualOverride(override)),
      generateProfile: () => dispatch(generateFlexibilityProfile()),
    },
    resilience: {
      resetSection: () => dispatch(resetResilienceSection()),
      manualOverride: override => dispatch(resilienceManualOverride(override)),
      generateProfile: () => dispatch(generateResilienceProfile()),
    },
  };

  return {
    next: flexibilityRequired =>
      dispatch(
        saveFactFindRequest({
          redirectPath: getNextURL(ownProps, flexibilityRequired),
          doInBackground: true,
        })
      ),
    backToDashboard: () =>
      dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/dashboard`)),
    back: () =>
      dispatch(
        push(
          ownProps.section === 'resilience'
            ? `${WealthWizards.CONTEXT_ROUTE}/${ownProps.section}/1`
            : `${WealthWizards.CONTEXT_ROUTE}/${ownProps.section}/start`
        )
      ),
    ...sectionFunctions[ownProps.section],
  };
};

const PlaybackContainer = compose(connect(mapStateToProps, mapDispatchToProps));

export default PlaybackContainer(PlaybackComponent);
