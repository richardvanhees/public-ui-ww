import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import Playback from './component';
import content from '../../../../../content/investment/athena/investment.json';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const generateProfileStub = sandbox.stub();
const nextStub = sandbox.stub();
const resetSectionStub = sandbox.stub();
const manualOverrideStub = sandbox.stub();
const popupToggleTestStub = sandbox.stub();

const fakeRequiredProps = {
  content,
  profile: {
    provider: 'a2risk',
    raw_score: 22,
    score: 4,
    profile: 'balanced',
    profile_data: {
      id: 'balanced',
      score: 4,
      min: 18,
      max: 24,
    },
    alerts: ['too_many_middle_atr_answers'],
  },
  section: 'atr',
  generateProfile: generateProfileStub,
  next: nextStub,
  resetSection: resetSectionStub,
  manualOverride: manualOverrideStub,
  isMobile: false,
};

test('<Playback>', assert => {
  assert.plan(1);
  assert.equals(typeof Playback.propTypes, 'object', 'PropTypes are defined');
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  assert.equals(
    generateProfileStub.called,
    true,
    'Profile generation started on mount'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  component
    .find('.line-item__circle')
    .first()
    .simulate('click');
  assert.equal(
    manualOverrideStub.args[0][0].profile,
    'adventurous',
    'Level changed on clicking element 2 levels above'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  component.find('div[data-test="type-line-item-1"]').simulate('click');

  assert.equal(
    manualOverrideStub.args[0][0].profile,
    'confident',
    'Level changed on clicking element 1 level above'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  component.find('div[data-test="type-line-item-3"]').simulate('click');

  assert.equal(
    manualOverrideStub.args[0][0].profile,
    'measured',
    'Level changed on clicking element 1 level below'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  component.find('div[data-test="type-line-item-4"]').simulate('click');

  assert.equal(
    manualOverrideStub.args[0][0].profile,
    'cautious',
    'Level changed on clicking element 2 levels below'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} isMobile />);
  assert.equals(
    component.find('.type-slider').length,
    1,
    'Slider is shown on mobile view'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(<Playback {...fakeRequiredProps} />);
  assert.equals(
    component.find('.type-description__read-more').length,
    1,
    'Read more is shown if object has read more'
  );

  sandbox.reset();
  assert.end();
});

test('<Playback>', assert => {
  assert.plan(1);

  const component = mount(
    <Playback {...fakeRequiredProps} section={'experience'} />
  );
  assert.equals(
    component.find('.type-description__read-more').length,
    0,
    'Read more is hidden if object does not have read more'
  );

  sandbox.reset();
  assert.end();
});
