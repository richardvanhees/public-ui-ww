import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import ForgottenPasswordComponent from './ForgottenPasswordComponent';

const setup = () => {
  const sandbox = sinon.sandbox.create();
  const resetStub = sandbox.stub();
  const nextStub = sandbox.stub();

  const fakeProps = {
    next: nextStub,
    reset: resetStub,
    content: {
      forgottenPassword: {
        subTitle: 'SUBTITLE',
        buttonText: 'BUTTONTEXT',
      },
    },
  };

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => null,
  };

  const target = mount(
    <Provider store={fakeStore}>
      <ForgottenPasswordComponent {...fakeProps} />
    </Provider>
  );

  return {
    target,
    resetStub,
    nextStub,
  };
};

test('ForgottenPasswordComponent - valid email', assert => {
  const context = setup();
  assert.plan(1);

  context.target
    .find('input')
    .simulate('change', { target: { value: 'foo@example.com' } });
  context.target.find('button').simulate('click');

  assert.equal(context.resetStub.callCount, 1, 'reset stub called');
});

test('ForgottenPasswordComponent - invalid email', assert => {
  const context = setup();
  assert.plan(1);

  context.target
    .find('input')
    .simulate('change', { target: { value: 'bademail' } });
  context.target.find('button').simulate('click');

  assert.equal(context.resetStub.callCount, 0, 'reset stub not called');
});

test('ForgottenPasswordComponent - invalid email / error shown', assert => {
  const context = setup();
  assert.plan(1);

  context.target
    .find('input')
    .simulate('change', { target: { value: 'bademail' } });
  const errorLength = context.target.find('.error-message').length;

  assert.equal(errorLength, 1, 'error message shown');
});
