import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import ForgottenPasswordContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './ForgottenPasswordContainer';

test('<ForgottenPasswordContainer />', assert => {
  const sandbox = sinon.sandbox.create();
  const generatePasswordTokenStub = sandbox.stub();
  const bindActionCreatorsStub = sandbox.stub();
  const dispatchStub = sandbox.stub();
  __RewireAPI__.__Rewire__('bindActionCreators', bindActionCreatorsStub);
  __RewireAPI__.__Rewire__('generatePasswordToken', generatePasswordTokenStub);

  // check map state to props
  let result;
  result = mapStateToProps({
    content: {
      data: {
        investment: {},
      },
    },
  });
  assert.deepEqual(result.content, {});

  // check map dispatch to props
  result = mapDispatchToProps(dispatchStub);
  bindActionCreatorsStub.args[0][0].reset('email');
  assert.deepEqual(
    generatePasswordTokenStub.args[0],
    ['email'],
    'generatePasswordToken function executed with email'
  );

  __RewireAPI__.__ResetDependency__('bindActionCreators');
  __RewireAPI__.__ResetDependency__('generatePasswordToken');
  assert.end();
});
