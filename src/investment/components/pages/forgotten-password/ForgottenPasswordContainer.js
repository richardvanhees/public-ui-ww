import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import ForgottenPasswordComponent from './ForgottenPasswordComponent';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { generatePasswordToken } from '../../../../../modules/password/actions';

const contentKey = 'investment';

export const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      reset: email => generatePasswordToken(email),
      login: () => push(`${WealthWizards.CONTEXT_ROUTE}/login`),
    },
    dispatch
  );

export default () => {
  const ForgottenPasswordContainer = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );

  return ForgottenPasswordContainer(ForgottenPasswordComponent);
};
