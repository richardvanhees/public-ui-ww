import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import { resetPass } from '../../../../../assets/images/iw-images';
import ButtonComponent from '../../../../../modules/elements/Button';
import Input from '../../../../../modules/elements/Input';
import Error from '../../../../../modules/elements/Error';

class ForgottenPasswordComponent extends Component {
  static propTypes = {
    reset: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.state = {
      sent: false,
      email: '',
      invalidEmail: false,
    };
  }

  inputChange = email => {
    this.setState({
      email,
    });
  };

  validateEmail = email =>
    !email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

  requestReset = () => {
    if (!this.validateEmail(this.state.email)) {
      this.props.reset(this.state.email);
      this.setState({
        sent: true,
      });
    } else {
      this.setState({
        invalidEmail: true,
      });
    }
  };

  render() {
    const { content } = this.props;
    return (
      <div className="forgotten-password__container">
        {!this.state.sent ? (
          <div className="forgotten-password__reset-form">
            <PageHeading
              image={resetPass}
              smallSubTitle
              title="Reset password"
              subTitle={content.forgottenPassword.subTitle}
            />
            <Input
              label="Email address"
              wrapperClassName="forgotten-password__email-input-wrapper"
              inputClassName="forgotten-password__email-input"
              onChange={e => {
                this.inputChange(e.target.value);
              }}
              dataTest="forgotten-password-email-input"
              error={this.state.invalidEmail}
            />
            <Error
              compact
              className="forgotten-password__email-error"
              title="Enter a valid email address to continue"
              active={this.state.invalidEmail}
              type={this.state.invalidEmail ? 'active' : ''}
            />
            <ButtonComponent
              label={content.forgottenPassword.buttonText}
              onClick={this.requestReset}
              dataTest="forgotten-password-email-button"
              className="forgotten-password__reset-button btn--primary"
            />
            <a
              className="forgotten-password__link"
              data-test="forgotten-password-sign-in"
              onClick={this.props.login}
            >
              Return to Sign in
            </a>
          </div>
        ) : (
          <div className="forgotten-password__sent">
            <PageHeading
              image={resetPass}
              title="Thank you!"
              smallSubTitle
              subTitle={content.forgottenPassword.thankYouSubTitle}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ForgottenPasswordComponent;
