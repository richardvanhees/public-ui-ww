import React from 'react';
import PropTypes from 'prop-types';

import WealthWizards from '../../../../../modules/wealthwizards';
import { puzzleBottomLeft } from '../../../../../assets/images/iw-images';

import Pager from '../../../../../modules/elements/Pager';
import Question from '../../../modules/elements/Question';

const ExperienceQuestionComponent = props => {
  const pagerOptions = {
    pages: props.content.experienceQuestions.map(question => ({
        pageContent: Question,
        extraPropsToInject: {
          onChange: (key, answer) => props.handleFieldChange(key, answer),
          userInput: props.userInput,
          ...question,
        },
        titleImage: puzzleBottomLeft,
        style: { width: 'max-content', padding: 0 },
      })
    ),
    previousPage: `${WealthWizards.CONTEXT_ROUTE}/experience/start`,
    nextPage: `${WealthWizards.CONTEXT_ROUTE}/experience/playback`,
    onSetNextPage: props.onSetNextPage,
  };

  return (
    <div className="wrapper experience">
      <div className="questions-component__question-container">
        <Pager {...pagerOptions} {...props} />
      </div>
    </div>
  );
};

ExperienceQuestionComponent.propTypes = {
  content: PropTypes.object.isRequired,
  handleFieldChange: PropTypes.func.isRequired,
  userInput: PropTypes.object,
  onSetNextPage: PropTypes.func.isRequired,
};

export default ExperienceQuestionComponent;
