import { connect } from 'react-redux';
import ExperienceStartComponent from './ExperienceStartComponent';
import { push, goBack } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch => ({
  next: () =>
    dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/experience/question/1`)),
  back: () => dispatch(goBack()),
});

const ExperienceStartContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default ExperienceStartContainer(ExperienceStartComponent);
