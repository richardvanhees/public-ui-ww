import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import { lookContentByKey } from '../../../../../modules/content/selectors';
import { handleFieldChange } from '../../../modules/experience/actions';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import { transformStateForQuestionRange } from '../../../modules/atr/selectors';

import ExperienceQuestionComponent from './ExperienceQuestionComponent';

const contentKey = 'investment';

const mapStateToProps = ({ content, experience }) => ({
  content: lookContentByKey(contentKey, content),
  userInput: transformStateForQuestionRange(
    lookContentByKey(contentKey, content),
    'experienceQuestions',
    experience
  ),
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    handleFieldChange: (name, value) => handleFieldChange(name, value),
    onSetNextPage: () => saveFactFindRequest({ doInBackground: true }),
  },
  dispatch
);

export default () => {
  const ExperienceQuestionContainer = compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  );

  return ExperienceQuestionContainer(ExperienceQuestionComponent);
};
