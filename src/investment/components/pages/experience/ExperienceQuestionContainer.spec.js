import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ExperienceQuestionContainer, {
  __RewireAPI__,
} from './ExperienceQuestionContainer';
import { Provider } from 'react-redux';

test('ExperienceQuestionContainer', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const fakeStore = {
    getState: () => ({
      content: {
        data: {
          investment: {
            experienceQuestions: [],
          },
        },
      },
      investmentGoal: {},
      aboutYou: {},
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  class FakeExperienceQuestionComponent extends React.PureComponent {
    render() {
      return <div />;
    }
  }

  const saveFactFindRequestStub = sandbox.stub();

  __RewireAPI__.__Rewire__(
    'ExperienceQuestionComponent',
    FakeExperienceQuestionComponent
  );
  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestStub);

  const Target = ExperienceQuestionContainer();

  const target = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  const componentProps = target.find(FakeExperienceQuestionComponent).props();

  componentProps.onSetNextPage();

  assert.deepEqual(
    saveFactFindRequestStub.args,
    [[{ doInBackground: true }]],
    'invoking customSetPageAction causes save in background action to occur'
  );

  __RewireAPI__.__ResetDependency__('ExperienceQuestionComponent');
});
