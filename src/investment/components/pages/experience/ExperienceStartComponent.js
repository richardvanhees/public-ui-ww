import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import { puzzleBottomLeft } from '../../../../../assets/images/iw-images';

const ExperienceStartComponent = ({ content, next, back }) => (
  <div className="wrapper experience">
    <PageHeading
      title={content.experienceStart.title}
      subTitle={content.experienceStart.subTitle}
      image={puzzleBottomLeft}
      smallSubTitle
    />

    <div className="center-desktop">
      <Instructions
        title={content.experienceStart.instructionsTitle}
        items={content.experienceStart.instructions}
        list
      />

      <PageNavigation
        next={next}
        back={back}
        noSideMargins
        testNext={'experience-start'}
        testBack={'experience-start'}
      />
    </div>
  </div>
);

ExperienceStartComponent.propTypes = {
  content: PropTypes.object.isRequired,
  next: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
};

export default ExperienceStartComponent;
