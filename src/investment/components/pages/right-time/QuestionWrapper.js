import React, { Component } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import { setItem, getItem } from '../../../../../modules/utils/local-storage';

import AnswerGroup from '../../../../../modules/elements/AnswerGroup';
import Checkbox from '../../../../../modules/elements/Checkbox';

export default class QuestionWrapper extends Component {
  static propTypes = {
    pagerProps: PropTypes.object,
    setAnswer: PropTypes.func.isRequired,
    setCheckbox: PropTypes.func.isRequired,
    multipleChoice: PropTypes.object,
    question: PropTypes.string.isRequired,
    answerKey: PropTypes.string.isRequired,
    userInput: PropTypes.object.isRequired,
    checkboxInput: PropTypes.object.isRequired,
    answers: PropTypes.array.isRequired,
    id: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showAdditionalQuestion: this.setInitialAdditionalQuestionVisibility(),
      additionalAnswers: props.checkboxInput,
      actionsDisabled: false,
    };

    this.timeoutForButton = false;

    props.pagerProps.hideNext();
    this.setInitialNextButtonVisibility();
  }

  componentDidUpdate = prevProps => {
    if (
      Object.keys(prevProps.checkboxInput).length !==
      Object.keys(this.props.checkboxInput).length
    ) {
      this.setState({ additionalAnswers: this.props.checkboxInput });
    }
    if (
      Object.keys(prevProps.userInput).length !==
      Object.keys(this.props.userInput).length
    ) {
      this.timeoutForButton = setTimeout(
        this.setInitialNextButtonVisibility,
        300
      );
      this.setState({
        showAdditionalQuestion: this.setInitialAdditionalQuestionVisibility(),
      });
    }
  };

  componentWillUnmount = () => {
    clearTimeout(this.timeoutForButton);
  };

  setInitialAdditionalQuestionVisibility = () =>
    this.props.multipleChoice &&
    R.path(['userInput', this.props.id, 'answerValue'], this.props) ===
      this.props.multipleChoice.trigger;

  setInitialNextButtonVisibility = () =>
    this.props.userInput[this.props.id] &&
    (!this.props.multipleChoice ||
      (this.props.multipleChoice &&
        this.props.multipleChoice.trigger !==
          this.props.userInput[this.props.id].answerValue) ||
      (this.props.multipleChoice &&
        Object.values(this.props.checkboxInput).includes(true))) &&
    this.props.pagerProps.showNext();

  setCheckbox = async (key, val) => {
    const { setCheckbox, pagerProps } = this.props;

    await setCheckbox(key, val);
    await this.setState({
      additionalAnswers: {
        ...this.state.additionalAnswers,
        [key]: val,
      },
    });

    const rightTimeAnswers = (await JSON.parse(getItem('right-time'))) || {};
    const newRightTimeObject = {
      ...rightTimeAnswers,
      checkboxInput: {
        ...rightTimeAnswers.checkboxInput,
        [key]: val,
      },
    };
    await setItem('right-time', JSON.stringify(newRightTimeObject));

    if (
      this.state.showAdditionalQuestion &&
      Object.values(this.props.checkboxInput).includes(true)
    ) {
      pagerProps.showNext();
    } else {
      pagerProps.hideNext();
    }
  };

  setAnswer = async (id, answerData) => {
    const { setAnswer, multipleChoice, pagerProps } = this.props;

    if (this.state.actionsDisabled) return;
    this.setState({ actionsDisabled: true });

    await setAnswer({ [id]: answerData });

    const rightTimeAnswers = (await JSON.parse(getItem('right-time'))) || {};
    const newRightTimeObject = {
      ...rightTimeAnswers,
      userInput: {
        ...rightTimeAnswers.userInput,
        [id]: answerData,
      },
    };
    await setItem('right-time', JSON.stringify(newRightTimeObject));

    if (multipleChoice && multipleChoice.trigger === answerData.answerValue) {
      await pagerProps.hideNext();
      await this.setState({ showAdditionalQuestion: true });
      await this.setState({ actionsDisabled: false });
    } else {
      Object.keys(this.state.additionalAnswers).forEach(answerKey => {
        this.setCheckbox(answerKey, false);
      });
      this.setState({ showAdditionalQuestion: false, additionalAnswers: [] });
      setTimeout(async () => {
        await pagerProps.next();
        await pagerProps.showNext();
        await this.setState({ actionsDisabled: false });
      }, 75);
    }
  };

  render = () => {
    const {
      id: questionId,
      question,
      answerKey,
      userInput,
      checkboxInput,
      answers,
      multipleChoice,
    } = this.props;
    const { showAdditionalQuestion } = this.state;

    return (
      <div
        className={`question question--active question--visible question--added-padding ${
          multipleChoice ? 'question--has-additional' : ''
        }`}
      >
        <div className="question__header">
          <div
            className="question__label"
            data-test={`question-${questionId + 1}`}
          >
            {question}
          </div>
        </div>

        <AnswerGroup
          wrapperClass="answer--iw-answer"
          answers={answers}
          dataTest={`q${questionId + 1}`}
          answerKey={answerKey}
          questionId={questionId}
          selected={
            userInput[questionId] !== undefined &&
            userInput[questionId] &&
            userInput[questionId].answerId
          }
          onChange={answerData => this.setAnswer(questionId, answerData)}
          answerPairCount={2}
          smallButtons
        />

        {multipleChoice && (
          <div
            className={`question__additional ${
              showAdditionalQuestion ? 'question__additional--visible' : ''
            }`}
          >
            <div
              className={`question__additional-overlay ${
                showAdditionalQuestion
                  ? 'question__additional-overlay--hidden'
                  : ''
              }`}
            />
            <div className="question__additional-question">
              {multipleChoice.question}
            </div>

            <div className="question__additional-options-wrapper">
              <div className="question__additional-options">
                {multipleChoice.answers.map((option, i) => (
                  <Checkbox
                    key={`mc-${i}`}
                    label={option.answer}
                    checked={checkboxInput[option.value]}
                    onChange={val => this.setCheckbox(option.value, val)}
                    type={'custom'}
                    name={`question-${questionId}-multiple-choice`}
                    dataTest={`mc-${i}`}
                  />
                ))}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  };
}
