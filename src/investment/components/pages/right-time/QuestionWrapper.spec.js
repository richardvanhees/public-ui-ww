import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import QuestionWrapper from './QuestionWrapper';
import { Provider } from 'react-redux';

const sandbox = sinon.sandbox.create();
const nextStub = sandbox.stub();
const showNextStub = sandbox.stub();
const hideNextStub = sandbox.stub();
const setAnswerStub = sandbox.stub();
const setCheckboxStub = sandbox.stub();

const fakeStore = {
  getState: () => ({
    rightTime: {
      userInput: {},
      checkboxInput: {},
    },
    browser: {
      breakpoint: 'desktop'
    },
  }),
  subscribe: () => null,
  dispatch: () => null,
};

const fakeRequiredProps = {
  id: 0,
  question: "Is someone helping you with this ISA advice journey?",
  answerKey: "right_time__ISA_help",
  answers: [
    { answer: "Yes", value: "yes" },
    { answer: "No", value: "no" }
  ],
  multipleChoice: {
    trigger: "yes",
    question: "Why do you need their extra help with this ISA advice journey? Please tick anything below that applies to you:",
    answers: [
      {
        answer: "I struggle to understand financial matters",
        value: "right_time__struggle_to_understand"
      },
      {
        answer: "Reading is not one of my strengths",
        value: "right_time__struggle_with_reading"
      }
    ]
  },
  pagerProps: {
    showNext: showNextStub,
    hideNext: hideNextStub,
    next: nextStub,
  },
  setAnswer: setAnswerStub,
  setCheckbox: setCheckboxStub,
  userInput: {},
  checkboxInput: {},
};

test('QuestionWrapper', assert => {
  assert.plan(1);

  assert.equals(typeof QuestionWrapper.propTypes, 'object', 'PropTypes are defined');
  sandbox.reset();
  assert.end();
});

test('QuestionWrapper', async assert => {
  assert.plan(2);

  const wrapper = mount(
    <Provider store={fakeStore}>
      <QuestionWrapper {...fakeRequiredProps} />
    </Provider>
  );

  assert.equals(wrapper.find('.question__additional').length, 1, 'Multiple choice questions shown');
  assert.equals(wrapper.find('.question__additional-options').children().length, 2, 'Multiple choice answers shown');
  sandbox.reset();
  assert.end();
});

test('QuestionWrapper', async assert => {
  assert.plan(1);

  const wrapper = mount(
    <Provider store={fakeStore}>
      <QuestionWrapper {...fakeRequiredProps} />
    </Provider>
  );

  wrapper.find('.checkbox__checkbox').at(0).simulate('change');

  assert.equals(setCheckboxStub.called, true, 'setCheckbox called');
  sandbox.reset();
  assert.end();
});

test('QuestionWrapper', async assert => {
  assert.plan(1);

  const wrapper = mount(
    <Provider store={fakeStore}>
      <QuestionWrapper {...fakeRequiredProps} />
    </Provider>
  );

  wrapper.find('.answer').at(1).simulate('click');

  assert.equals(setAnswerStub.called, true, 'setAnswer called');
  sandbox.reset();
  assert.end();
});

test('QuestionWrapper', async assert => {
  assert.plan(3);
  const fakeUserInput = {
    userInput: {
      '0': {
        answerId: 0,
        answerKey: 'right_time__ISA_help',
        answerValue: 'yes'
      }
    },
    checkboxInput: {
      right_time__struggle_to_understand: true
    }
  };

  const wrapper = mount(
    <Provider store={fakeStore}>
      <QuestionWrapper {...fakeRequiredProps} {...fakeUserInput} />
    </Provider>
  );

  const checkbox = wrapper.find('input[name="question-0-multiple-choice"]').at(0);

  assert.equals(wrapper.find('.answer--selected').length, 1, 'Previous user input set correctly');
  assert.equals(wrapper.find('.question__additional--visible').length, 1, 'Multiple choice trigger handled');
  assert.equals(checkbox.props().checked, true, 'Multiple choice answer set correctly');
  sandbox.reset();
  assert.end();
});