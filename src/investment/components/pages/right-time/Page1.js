import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Page1 extends Component {
  static propTypes = {
    pagerProps: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    props.pagerProps.hideBack();
  }

  render = () => {
    const { content } = this.props;
    const sectionContent = content.rightTime;

    return (<div className="right-time-page-1">
      {sectionContent.paragraphs.map((paragraph, i) =>
        <div key={`paragraph-${i}`} className="right-time-page-1__paragraph">
          <span>{paragraph.content}</span>
          {paragraph.list && <ul className="right-time-page-1__list">
            {paragraph.list.map((item, n) => <li className="right-time-page-1__item" key={`right-time-item-${n}`}>{item}</li>)}
          </ul>}
        </div>
      )}
    </div>);
  }
}
