import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import RightTimeComponent, { __RewireAPI__ } from './RightTimeComponent';

const sandbox = sinon.sandbox.create();
const nextStub = sandbox.stub();
const needMoreTimeStub = sandbox.stub();
const setAnswerStub = sandbox.stub();
const setCheckboxStub = sandbox.stub();
const setAllDataStub = sandbox.stub();
const backToStartStub = sandbox.stub();
const popupToggleStub = sandbox.stub();
const goToQuestionStub = sandbox.stub();
const doTestStub = sandbox.stub();

const fakeRequiredProps = {
  fullState: {},
  content: {
    rightTime: {
      title: 'Is this the right time to be investing?',
      paragraphs: [
        {
          content:
            'Wealth Wizards have a duty of care to protect you and your interests. Sometimes, it might not be the right time for you to invest.',
        },
        {
          content: 'Examples of these circumstances might include:',
          list: [
            'If you are in poor health',
            'If you have caring responsibilities',
            "If you're going through a bereavement or a relationship breakdown",
            "If you're having a change of employment",
          ],
        },
        {
          content:
            "To make sure we're giving you the right advice, we need to ask you a few questions.",
        },
      ],
      callToAction: 'Start',
      questions: [
        {
          question:
            'Have you recently experienced or are you going through the break-up of a long-term relationship?',
          answerKey: 'right_time__relationship_break',
          answers: [
            {
              answer: 'Yes',
              value: 'yes',
            },
            {
              answer: 'No',
              value: 'no',
            },
          ],
        },
        {
          question:
            'Have you lost a family member or anyone close to you through bereavement over the last 12 months?',
          answerKey: 'right_time__bereavement',
          answers: [
            {
              answer: 'Yes',
              value: 'yes',
            },
            {
              answer: 'No',
              value: 'no',
            },
          ],
        },
        {
          question:
            'Has your employment status changed or do you expect this to change soon?',
          answerKey: 'right_time__employment_change',
          answers: [
            {
              answer: 'Yes',
              value: 'yes',
            },
            {
              answer: 'No',
              value: 'no',
            },
          ],
        },
        {
          question:
            'Do you have any health issues which might affect your ability to make financial decisions?',
          answerKey: 'right_time__health_issues',
          answers: [
            {
              answer: 'Yes',
              value: 'yes',
            },
            {
              answer: 'No',
              value: 'no',
            },
          ],
        },
        {
          question: 'Is someone helping you with this ISA advice journey?',
          answerKey: 'right_time__ISA_help',
          answers: [
            {
              answer: 'Yes',
              value: 'yes',
            },
            {
              answer: 'No',
              value: 'no',
            },
          ],
          multipleChoice: {
            trigger: 'yes',
            question:
              'Why do you need their extra help with this ISA advice journey? Please tick anything below that applies to you:',
            answers: [
              {
                answer: 'I struggle to understand financial matters',
                value: 'right_time__struggle_to_understand',
              },
              {
                answer: 'Reading is not one of my strengths',
                value: 'right_time__struggle_with_reading',
              },
              {
                answer: 'English is not my first language',
                value: 'right_time__english_not_first_language',
              },
              {
                answer: 'I feel more reassured',
                value: 'right_time__more_reassured',
              },
              {
                answer: "I'm not very good with technology",
                value: 'right_time__not_good_with_technology',
              },
            ],
          },
        },
      ],
    },
  },
  next: nextStub,
  needMoreTime: needMoreTimeStub,
  setAnswer: setAnswerStub,
  setCheckbox: setCheckboxStub,
  setAllData: setAllDataStub,
  backToStart: backToStartStub,
  doTest: doTestStub,
  userInput: {},
  checkboxInput: {},
  match: { params: { activePage: 2 } },
  goToQuestion: goToQuestionStub,
};

test('<RightTimeComponent />', assert => {
  assert.plan(1);

  assert.equals(
    typeof RightTimeComponent.propTypes,
    'object',
    'PropTypes are defined'
  );
  sandbox.reset();
  assert.end();
});

test('<RightTimeComponent />', assert => {
  assert.plan(1);
  const wrapper = shallow(
    <RightTimeComponent
      {...fakeRequiredProps}
      userInput={{ 0: 'fakeAnswer' }}
    />
  );

  wrapper.instance().checkVulnerability(true);

  assert.equals(doTestStub.called, true, 'Incomplete answers trigger popup');

  sandbox.reset();
  assert.end();
});

test('<RightTimeComponent />', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const redirectStub = sandbox.stub();

  const wrapper = shallow(
    <RightTimeComponent
      {...fakeRequiredProps}
      isLoggedIn
      redirect={redirectStub}
    />
  );

  wrapper.instance().checkVulnerability(true);

  assert.equals(
    redirectStub.callCount,
    1,
    'User cannot do right time when logged in'
  );

  sandbox.reset();
});
