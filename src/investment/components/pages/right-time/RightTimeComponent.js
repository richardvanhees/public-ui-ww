import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Pager from '../../../../../modules/elements/Pager';
import Page1 from './Page1';
import WealthWizards from '../../../../../modules/wealthwizards';
import QuestionWrapper from './QuestionWrapper';
import Spinner from '../../../../../modules/elements/Spinner';
import Popup from '../../../../../modules/elements/Popup';

export default class RightTimeComponent extends Component {
  static propTypes = {
    fullState: PropTypes.object.isRequired,
    content: PropTypes.object.isRequired,
    needMoreTime: PropTypes.func.isRequired,
    backToStart: PropTypes.func.isRequired,
    userInput: PropTypes.object.isRequired,
    checkboxInput: PropTypes.object.isRequired,
    doTest: PropTypes.func,
    setAllData: PropTypes.func.isRequired,
    match: PropTypes.object,
    goToQuestion: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    redirect: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    if (props.isLoggedIn) {
      props.redirect(`${WealthWizards.CONTEXT_ROUTE}/dashboard`);
    }
  }

  checkVulnerability = isTest => {
    const { userInput, content, doTest } = this.props;
    const userAnswers = Object.values(userInput).map(
      answer => answer.answerValue
    );

    if (userAnswers.length !== content.rightTime.questions.length) {
      isTest ? doTest() : this.incompletePopup.toggle();
      return false;
    }

    if (userAnswers.includes('yes')) {
      this.props.needMoreTime();
      return false;
    }

    return true;
  };

  render = () => {
    const pagerOptions = {
      pages: [
        {
          pageContent: Page1,
          title: this.props.content.rightTime.title,
          nextLabel: this.props.content.rightTime.callToAction,
          centerButtons: true,
          style: {
            width: 750,
          },
        },
        ...this.props.content.rightTime.questions.map((question, i) => ({
          pageContent: QuestionWrapper,
          extraPropsToInject: {
            key: `question-${i}`,
            userInput: this.props.userInput,
            checkboxInput: this.props.checkboxInput,
            id: i,
            ...question,
          },
          style: {
            width: 750,
          },
        })),
      ],
      afterLastPage: this.checkVulnerability,
      nextPage: `${WealthWizards.CONTEXT_ROUTE}/goal`,
    };

    return !this.props.content ? (
      <Spinner visible />
    ) : (
      <div className="wrapper right-time">
        <Pager {...this.props} {...pagerOptions} />

        <Popup
          ref={c => {
            this.incompletePopup = c;
          }}
          title={'Something went wrong'}
          isNarrow
          closeButtonTop
          closeClickOutside
          buttons={[
            {
              label: 'Close',
              type: 'primary',
              onClick: () => this.incompletePopup.toggle(),
            },
            {
              label: 'Back to start',
              onClick: () => {
                this.incompletePopup.toggle();
                this.props.backToStart();
              },
            },
          ]}
        >
          One or more answers seem to be missing. Please redo this section.
        </Popup>
      </div>
    );
  };
}
