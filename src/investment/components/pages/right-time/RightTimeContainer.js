import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import RightTimeComponent from './RightTimeComponent';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import {
  setAnswer,
  setCheckbox,
  setAllData,
} from '../../../modules/right-time';
import { getItem } from '../../../../../modules/utils/local-storage';

const contentKey = 'investment';

const mapStateToProps = state => ({
  fullState: state,
  content: lookContentByKey(contentKey, state.content),
  userInput: state.rightTime.userInput,
  checkboxInput: state.rightTime.checkboxInput,
  setCompletedAt: state.rightTime.setCompletedAt,
  isLoggedIn: getItem('user_token') !== null,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      needMoreTime: () => push(`${WealthWizards.CONTEXT_ROUTE}/need-more-time`),
      backToStart: () => push(`${WealthWizards.CONTEXT_ROUTE}/right-time/1`),
      goToQuestion: id => push(`${WealthWizards.CONTEXT_ROUTE}/right-time/${id}`),
      setAnswer,
      setCheckbox,
      setAllData,
      redirect: url => push(url),
    },
    dispatch
  );

export default () => {
  const RightTimeContainer = compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  );

  return RightTimeContainer(RightTimeComponent);
};
