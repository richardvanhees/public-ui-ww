import React from 'react';
import PropTypes from 'prop-types';
import Instructions from '../../../../modules/elements/InstructionsComponents/Instructions';
import PageHeading from '../../../../modules/elements/PageHeading';
import PageNavigation from '../../../../modules/elements/PageNavigation';

const StartComponent = ({ content, next }) => (
  <div className="wrapper start-component">
    <PageHeading
      title={content.start.title}
      subTitle={content.start.subTitle}
      smallSubTitle
    />

    <Instructions
      title={content.start.instructionsTitle}
      items={content.start.items}
      wide
    />

    <PageNavigation
      next={next}
      className={'start-component__button-container'}
      testNext={'plan'}
    />
  </div>
);

StartComponent.propTypes = {
  content: PropTypes.object.isRequired,
  next: PropTypes.func.isRequired,
  back: PropTypes.func,
  handleFieldChange: PropTypes.func,
};

export default StartComponent;
