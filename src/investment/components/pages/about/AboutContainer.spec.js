import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AboutContainer, { __RewireAPI__ } from './AboutContainer';
import { Provider } from 'react-redux';

test('AboutContainer', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const fakeStore = {
    getState: () => ({
      content: {
        data: {
          investment: {},
        },
      },
      investmentGoal: {},
      aboutYou: {},
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  class FakeAboutComponent extends React.PureComponent {
    render() {
      return <div />;
    }
  }

  const saveFactFindRequestStub = sandbox.stub();

  __RewireAPI__.__Rewire__('AboutComponent', FakeAboutComponent);
  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestStub);

  const Target = AboutContainer();

  const target = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  const componentProps = target.find(FakeAboutComponent).props();

  componentProps.next();

  assert.deepEqual(
    saveFactFindRequestStub.args,
    [[{ redirectPath: '/isa/capture' }]],
    'invoking button causes save in background action to occur'
  );

  __RewireAPI__.__ResetDependency__('AboutComponent');
});
