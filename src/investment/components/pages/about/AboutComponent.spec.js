import test from 'tape';
import React from 'react';
import { Provider } from 'react-redux';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AboutComponent from './AboutComponent';
import content from '../../../../../content/investment/athena/investment';
import R from 'ramda';
import sinon from 'sinon';
configure({ adapter: new Adapter() });
import timekeeper from 'timekeeper';
import moment from 'moment';

let fakeRequiredProps = {
  content: {
    aboutYou: {
      title: '',
      dob: '',
      niNumber: '',
      contactNumber: '',
      addressPostcode: '',
      subTitle: '',
      genders: [
        { value: 'male', title: 'm', image: 'image' },
        { value: 'female', title: 'f', image: 'image' },
      ],
    },
  },
  aboutYou: {
    title: '',
    dob: '',
    niNumber: '',
    contactNumber: '',
    addressPostcode: '',
    subTitle: '',
    genders: [
      { value: 'male', title: 'm', image: 'image' },
      { value: 'female', title: 'f', image: 'image' },
    ],
  },
  next: () => {},
  back: () => {},
};

const initialState = {
  niNumber: null,
  dob: null,
  contactNumber: null,
  postcode: null,

  nextEnabled: false,
  hoveredAnswer: null,
};

test('<AboutComponent> general checks', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '07967405765';
  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.deepEquals(
    ShallowAboutComponent.state(),
    {
      contactNumber: null,
      dob: null,
      hoveredAnswer: null,
      nextEnabled: '',
      niNumber: null,
      postcode: null,
      showAddAddressForm: false,
      showAddPreviousAddressBtn: false,
      showPreviousAddressForm: false,
      showThreeYearsSuppliedError: false,
      timeAtLength: '',
      timeAtMonths: null,
      timeAtYears: null,
    },
    'check initial state'
  );

  t.end();
});

test('<AboutComponent>', t => {
  const sandbox = sinon.sandbox.create();

  fakeRequiredProps.aboutYou.contactNumber = '1234567890';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.false(
    ShallowAboutComponent.instance().validateContactNumber(),
    '10 digits'
  );

  t.end();
});

test('<AboutComponent>', t => {
  const sandbox = sinon.sandbox.create();

  fakeRequiredProps.aboutYou.contactNumber = '12345678901';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.true(ShallowAboutComponent.instance().validateContactNumber(), '11 digits');

  t.end();
});

test('<AboutComponent>', t => {
  const sandbox = sinon.sandbox.create();

  fakeRequiredProps.aboutYou.contactNumber = '12345678901234567890';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.true(ShallowAboutComponent.instance().validateContactNumber(), '20 digits');

  t.end();
});

test('<AboutComponent>', t => {
  const sandbox = sinon.sandbox.create();

  fakeRequiredProps.aboutYou.contactNumber = '123456789012345678901';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.false(
    ShallowAboutComponent.instance().validateContactNumber(),
    '21 digits'
  );

  t.end();
});

test('<AboutComponent> Contact Number fail 1 - no digits', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '+++++';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    false,
    'false when no digits supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number fail 2 - only 1 digit supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '1';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    false,
    'false when 1 digit supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number fail 3 - only 5 chars supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '+4423';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    false,
    'false when only 5 chars supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number fail 4 - only 6 digits supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '079876';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    false,
    'false when only 6 digits supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number pass 1 - mobile number supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '079675978654';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    true,
    'true when mobile number supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number pass 2 - area code number supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '+447967856747';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    true,
    'true when area code number supplied'
  );

  t.end();
});

test('<AboutComponent> Contact Number pass 3 - landline number supplied', t => {
  const sandbox = sinon.sandbox.create();

  t.equals(typeof AboutComponent.propTypes, 'object', 'PropTypes are defined');

  fakeRequiredProps.aboutYou.contactNumber = '02476563542';

  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  t.equals(
    ShallowAboutComponent.instance().validateContactNumber(),
    true,
    'true when landline number supplied'
  );

  t.end();
});

test('<AboutComponent> years and months at current address - months only', t => {
  const sandbox = sinon.sandbox.create();
  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  timekeeper.freeze(
    moment(
      `${moment()
        .utc()
        .year()}-08-01`,
      'YYYY-MM-DD'
    )
      .utc()
      .format()
  );
  const testYear = moment()
    .utc()
    .year();
  t.equals(
    ShallowAboutComponent.instance().getTimeAtLength(testYear, '05'),
    '3 month(s)',
    'length returned is correct'
  );

  t.equals(
    ShallowAboutComponent.state().showAddPreviousAddressBtn,
    true,
    'previous button shown if current address is < 3 years'
  );

  t.end();
});

test('<AboutComponent> years and months at current address - year and months', t => {
  const sandbox = sinon.sandbox.create();
  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  timekeeper.freeze(
    moment(
      `${moment()
        .utc()
        .year()}-08-01`,
      'YYYY-MM-DD'
    )
      .utc()
      .format()
  );
  const testYear =
    moment()
      .utc()
      .year() - 1;
  t.equals(
    ShallowAboutComponent.instance().getTimeAtLength(testYear, '05'),
    '1 year(s), 3 month(s)',
    'length returned is correct'
  );

  t.end();
});

test('<AboutComponent> years and months at current address - year ago', t => {
  const sandbox = sinon.sandbox.create();
  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  timekeeper.freeze(
    moment(
      `${moment()
        .utc()
        .year()}-08-01`,
      'YYYY-MM-DD'
    )
      .utc()
      .format()
  );
  const testYear =
    moment()
      .utc()
      .year() - 1;
  t.equals(
    ShallowAboutComponent.instance().getTimeAtLength(testYear, '08'),
    '1 year(s)',
    'length returned is correct'
  );

  t.end();
});

test('<AboutComponent> years and months at current address - multiple years ago', t => {
  const sandbox = sinon.sandbox.create();
  const ShallowAboutComponent = shallow(
    <AboutComponent {...fakeRequiredProps} />
  );

  ShallowAboutComponent.setState(initialState);

  timekeeper.freeze(
    moment(
      `${moment()
        .utc()
        .year()}-08-01`,
      'YYYY-MM-DD'
    )
      .utc()
      .format()
  );
  const testYear =
    moment()
      .utc()
      .year() - 3;
  t.equals(
    ShallowAboutComponent.instance().getTimeAtLength(testYear, '04'),
    '3 year(s), 4 month(s)',
    'length returned is correct'
  );

  t.equals(
    ShallowAboutComponent.state().showAddPreviousAddressBtn,
    false,
    'previous button shown NOT if current address is > 3 years'
  );

  t.end();
});
