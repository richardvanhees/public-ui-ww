import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PageHeading from '../../../../../modules/elements/PageHeading';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import Input from '../../../../../modules/elements/Input';
import Address from '../../../../../modules/elements/Address';
import PreviousAddress from '../../../../../modules/elements/PreviousAddress';
import TimeAtAddress from '../../../../../modules/elements/TimeAtAddress';
import DatePicker from '../../../../../modules/elements/DatePicker';
import Error from '../../../../../modules/elements/Error';
import {
  POSTCODE_REGEX,
  validAddressLine,
  optionalValidAddressLine,
} from '../../../../../modules/utils/regexes';
import R from 'ramda';

class AboutComponent extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    back: PropTypes.func.isRequired,
    next: PropTypes.func.isRequired,
    aboutYou: PropTypes.object,
    sendPostcodeSearchRequest: PropTypes.func,
    handleUserInput: PropTypes.func,
    setTimeLivedAtCurrentAddress: PropTypes.func,
    setThreeYearsOfAddressesProvided: PropTypes.func,
    updatePreviousAddressArray: PropTypes.func,
    setAboutAdressLine1: PropTypes.func,
    setAboutAdressLine2: PropTypes.func,
    setAboutAdressLine3: PropTypes.func,
    setAboutPostCode: PropTypes.func,
    toggleManualInputs: PropTypes.func,
    isSearchingPostcode: PropTypes.bool,
    sendPostcodeSearchResponse: PropTypes.array,
    sendPostcodeSearchSelect: PropTypes.func,
    sendPostcodeSearchReset: PropTypes.func,
    setPostcodeToSearchFor: PropTypes.func,
    updateProgress: PropTypes.func,
  };

  static getDerivedStateFromProps = ({ aboutYou }) => {
    if (aboutYou) {
      const {
        addressPostcode,
        addressLine1,
        addressLine2,
        addressLine3,
        dob,
        niNumber,
        contactNumber,
        addressTimeLivedAt,
      } = aboutYou;

      return {
        nextEnabled:
        dob &&
        !!dob.trim() &&
        !!niNumber.trim() &&
        !!contactNumber.trim() &&
        validAddressLine(addressLine1.trim()) &&
        validAddressLine(addressLine2.trim()) &&
        optionalValidAddressLine(addressLine3.trim()) &&
        !!addressPostcode.trim() &&
        addressPostcode.match(POSTCODE_REGEX) &&
        addressTimeLivedAt,
      };
    }
    return null;
  };

  constructor(props) {
    super(props);

    this.state = {
      niNumber: null,
      dob: null,
      contactNumber: null,
      postcode: null,
      nextEnabled: false,
      hoveredAnswer: null,
      timeAtMonths: this.getMonthsFromProps(props),
      timeAtYears: this.getYearsFromProps(props),
      timeAtLength: this.getTimeAtLength(
        this.getYearsFromProps(props),
        this.getMonthsFromProps(props)
      ),
      showAddPreviousAddressBtn: false,
      showPreviousAddressForm: false,
      showAddAddressForm: false,
      showThreeYearsSuppliedError: false,
    };
  }

  componentDidMount = () => {
    this.getTimeAtLength(
      this.getYearsFromProps(this.props),
      this.getMonthsFromProps(this.props)
    );
  };

  getYearsFromProps = props => {
    let years = null;
    const addressTimeLivedAt = R.pathOr(
      '',
      ['aboutYou', 'addressTimeLivedAt'],
      props
    );
    const splitAdd = addressTimeLivedAt.split(' ');
    if (splitAdd[1]) {
      years = splitAdd[1];
    }
    return years;
  };

  getMonthsFromProps = props => {
    let month = null;
    const addressTimeLivedAt = R.pathOr(
      '',
      ['aboutYou', 'addressTimeLivedAt'],
      props
    );
    const splitAdd = addressTimeLivedAt.split(' ');
    if (splitAdd[1]) {
      month = moment()
        .month(splitAdd[0])
        .format('M');
    }
    return month;
  };

  getTimeAtLength = (year = null, month = null, startDate = moment().utc()) => {
    let msg = '';

    if (year && month) {
      const currentAddressDate = moment(
        `${year}-${month}-01`,
        'YYYY-MM-DD'
      ).utc();
      const dateDiffInMonths = currentAddressDate
        .diff(startDate, 'months')
        .toString()
        .split('-');

      const dateDiffInYears = Math.floor(dateDiffInMonths[1] / 12);
      const modulusMonths = dateDiffInMonths[1] % 12;
      if (dateDiffInMonths.length <= 1) {
        msg = '0 month(s)';
        this.showPreviousAddressBtn && this.showPreviousAddressBtn(true);
      } else {
        if (dateDiffInMonths[1] >= 12) {
          if (modulusMonths === 0) {
            msg = `${dateDiffInYears} year(s)`;
          } else {
            msg = `${dateDiffInYears} year(s), ${modulusMonths} month(s)`;
          }
        } else {
          msg = `${modulusMonths} month(s)`;
        }
        const lessThan36Months = dateDiffInMonths[1] < 36;
        this.showPreviousAddressBtn &&
        this.showPreviousAddressBtn(lessThan36Months);
      }
    }
    return msg;
  };

  setTimeAtLength = (year, month) => {
    if (
      year === null ||
      month === null ||
      year === 'Select...' ||
      month === 'Select...'
    ) {
      this.setState({ timeAtLength: null });
      this.props.setTimeLivedAtCurrentAddress(null);
    } else {
      const timeLivedAt = this.getTimeAtLength(year, month);
      this.setState({ timeAtLength: timeLivedAt });
      this.props.setTimeLivedAtCurrentAddress(
        `${moment(month, 'MM').format('MMMM')} ${year}`
      );
    }
  };

  setTimeAtMonths = month => {
    if (!month) {
      this.setState({ timeAtMonths: null });
      this.setTimeAtLength(null, null);
    } else {
      this.setState({ timeAtMonths: month });
      this.setTimeAtLength(this.state.timeAtYears, month);
    }
  };

  setTimeAtYears = year => {
    if (!year) {
      this.setState({ timeAtYears: null });
      this.setTimeAtLength(null, null);
    } else {
      this.setState({ timeAtYears: year });
      this.setTimeAtLength(year, this.state.timeAtMonths);
    }
  };

  showPreviousAddressBtn = showBtn => {
    this.setState({ showAddPreviousAddressBtn: showBtn });
  };

  calculateIfThreeYearsHaveBeenProvided = (addressPreviousArray = []) => {
    const startingDate = moment(
      `${this.getYearsFromProps(this.props)}-${this.getMonthsFromProps(
        this.props
      )}-01`,
      'YYYY-MM-DD'
    ).utc();
    const currentAddressDiffInMonths = moment(startingDate)
      .utc()
      .diff(moment().utc(), 'months')
      .toString()
      .split('-');
    const currentAddressTotalMonths =
      (R.is(Object, currentAddressDiffInMonths[1]) &&
        currentAddressDiffInMonths[1].length < 1) ||
      R.isNil(currentAddressDiffInMonths[1])
        ? 0
        : currentAddressDiffInMonths[1];
    const currentAddressMoreThanThreeYearsAgo =
      parseInt(currentAddressTotalMonths, 10) >= 36;

    let aPreviousAddressIsMoreThanThreeYearsAgo = false;

    addressPreviousArray.forEach((add, i) => {
      const previousAddressDiffInMonths = moment(
        `${addressPreviousArray[i].timeAtYears}-${
          addressPreviousArray[i].timeAtMonths
          }-01`,
        'YYYY-MM-DD'
      )
        .utc()
        .diff(moment().utc(), 'months')
        .toString()
        .split('-');
      const diffInMonths =
        (R.is(Object, previousAddressDiffInMonths[1]) &&
          previousAddressDiffInMonths[1].length < 1) ||
        R.isNil(previousAddressDiffInMonths[1])
          ? 0
          : previousAddressDiffInMonths[1];
      if (parseInt(diffInMonths, 10) >= 36) {
        aPreviousAddressIsMoreThanThreeYearsAgo = true;
      }
    });

    const threeYearsSupplied =
      currentAddressMoreThanThreeYearsAgo ||
      aPreviousAddressIsMoreThanThreeYearsAgo;

    this.setState({ showThreeYearsSuppliedError: !threeYearsSupplied });

    return threeYearsSupplied;
  };

  validateNINumber = () => {
    const niNumber = this.props.aboutYou.niNumber;
    const isNationalInsuranceNumber = /^(?!BG)(?!GB)(?!NK)(?!KN)(?!TN)(?!NT)(?!ZZ)(?:[A-CEGHJ-PR-TW-Z][A-CEGHJ-NPR-TW-Z])(?:\s*\d\s*){6}([A-D]|\s)$/gi;

    if (
      !!niNumber.match(isNationalInsuranceNumber) &&
      typeof niNumber === 'string' &&
      niNumber.length === 9
    ) {
      this.setState({ niNumber: null });
    } else {
      this.setState({ niNumber: 'Enter a valid NI number' });
    }
    return (
      !!niNumber.match(isNationalInsuranceNumber) &&
      typeof niNumber === 'string' &&
      niNumber.length === 9
    );
  };

  validateContactNumber = () => {
    const number = this.props.aboutYou.contactNumber;

    const isValidNumber = /((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/;
    if (
      isValidNumber.test(number) &&
      number.length >= 11 &&
      number.length <= 20
    ) {
      this.setState({ contactNumber: null });
      return true;
    }
    this.setState({
      contactNumber: 'Contact number must be between 11 and 20 digits',
    });
    return false;
  };

  validateDOB = () => {
    const dob = this.props.aboutYou.dob;
    const today = moment().startOf('day');
    const isValidDate = /^(\d{4})-(\d{2})-(\d{2})$/;
    let error = null;

    if (!!dob.match(isValidDate)) {
      if (moment(dob) > today) {
        error = 'The date exists in the future';
      } else if (
        today.diff(moment(dob), 'years') < 18 ||
        today.diff(moment(dob), 'years') >= 85
      ) {
        error = 'You must be between 18 - 85 years old to invest';
      }
    } else {
      error = 'Date of birth needs to be in the format dd/mm/yyyy';
    }

    this.setState({ dob: error });
    return !error;
  };

  validatePostcode = () => {
    const postcode = this.props.aboutYou.addressPostcode;

    if (!!postcode.replace(/ /g, '').match(POSTCODE_REGEX)) {
      this.setState({ postcode: null });
    } else {
      this.setState({ postcode: 'Enter a valid postcode' });
    }
    return !!postcode.replace(/ /g, '').match(POSTCODE_REGEX);
  };

  togglePreviousAddressForm = previousAddress => {
    this.setState({ showPreviousAddressForm: !previousAddress });
  };

  checkIfPopulated = aboutYou => {
    const {
      addressPostcode,
      addressLine1,
      addressLine2,
      addressLine3,
      dob,
      niNumber,
      contactNumber,
      addressTimeLivedAt,
    } = aboutYou;

    this.setState({
      nextEnabled:
      dob &&
      !!dob.trim() &&
      !!niNumber.trim() &&
      !!contactNumber.trim() &&
      validAddressLine(addressLine1.trim()) &&
      validAddressLine(addressLine2.trim()) &&
      optionalValidAddressLine(addressLine3.trim()) &&
      !!addressPostcode.trim() &&
      addressPostcode.match(POSTCODE_REGEX) &&
      addressTimeLivedAt,
    });
  };

  updateUserInput = values => {
    this.props.handleUserInput(values);
    this.props.updateProgress();
  };

  proceed = () => {
    const dob = this.validateDOB();
    const niNumber = this.validateNINumber();
    const contactNumber = this.validateContactNumber();
    const postcode = this.validatePostcode();
    const threeYearsOfAddressesProvided = this.calculateIfThreeYearsHaveBeenProvided(
      this.props.aboutYou.addressPreviousArray
    );

    if (threeYearsOfAddressesProvided) {
      this.props.setThreeYearsOfAddressesProvided(true);
      if (dob && niNumber && contactNumber && postcode) {
        this.props.next();
      }
    }
  };

  render = () =>
    <div className="wrapper">
      <div className="center-desktop about-wrapper">
        <PageHeading
          smallSubTitle
          title={this.props.content.aboutYou.title}
        />
        <div className="form-wrapper__container">
          <DatePicker
            name={'dob-input'}
            label={'Date of birth'}
            onChange={val => this.updateUserInput({ dob: val })}
            userInput={this.props.aboutYou.dob}
            dataTest={'dob-input'}
            pastOnly
          />
          <Error compact title={this.state.dob} active={this.state.dob} />

          <Input
            dataTest="ni-input"
            onChange={e => this.updateUserInput({ niNumber: e.target.value })}
            label="National insurance number"
            hintText="e.g. SS123456C"
            value={this.props.aboutYou.niNumber}
          />
          <Error
            compact
            title={this.state.niNumber}
            active={this.state.niNumber}
            type={this.state.niNumber ? 'test' : ''}
          />
          <Input
            dataTest="contact-number-input"
            onChange={e =>
              this.updateUserInput({ contactNumber: e.target.value })
            }
            label="Contact number"
            value={this.props.aboutYou.contactNumber}
            type="number"
          />
          <Error
            compact
            title={this.state.contactNumber}
            active={this.state.contactNumber}
          />
        </div>

        <div className="form-wrapper__container form-wrapper__container--bottom">
          <Address
            sendPostcodeSearchRequest={this.props.sendPostcodeSearchRequest}
            isSearchingPostcode={this.props.isSearchingPostcode}
            sendPostcodeSearchResponse={this.props.sendPostcodeSearchResponse}
            sendPostcodeSearchSelect={this.props.sendPostcodeSearchSelect}
            sendPostcodeSearchReset={this.props.sendPostcodeSearchReset}
            enterManually
            label="Current address"
            hintText="Postcode"
            addressLine1={this.props.aboutYou.addressLine1}
            addressLine2={this.props.aboutYou.addressLine2}
            addressLine3={this.props.aboutYou.addressLine3}
            showManualInputs={this.props.aboutYou.showManualInputs}
            addressPostCode={this.props.aboutYou.addressPostcode}
            setAboutAdressLine1={this.props.setAboutAdressLine1}
            setAboutAdressLine2={this.props.setAboutAdressLine2}
            setAboutAdressLine3={this.props.setAboutAdressLine3}
            setAboutPostCode={data => {
              this.props.setAboutPostCode(data);
              this.props.setPostcodeToSearchFor(data);
            }}
            toggleManualInputs={this.props.toggleManualInputs}
            error={this.state.postcode}
          />
          <TimeAtAddress
            setTimeAtMonths={val => this.setTimeAtMonths(val)}
            setTimeAtYears={val => this.setTimeAtYears(val)}
            timeAtLength={this.state.timeAtLength}
            timeAtMonths={this.state.timeAtMonths}
            timeAtYears={this.state.timeAtYears}
            showTimeAtAddress
          />
          <Error
            compact
            title="We require three years of addresses"
            active={this.state.showThreeYearsSuppliedError}
          />
          {this.state.showAddPreviousAddressBtn && (
            <div
              className="address-wrapper__manual-title-container"
              onClick={() =>
                this.togglePreviousAddressForm(
                  this.state.showPreviousAddressForm
                )
              }
            >
              <p
                className="address-wrapper__manual-title-container__title address-wrapper__add-previous-title"
                data-test="address-manual-link"
              >
                Add a previous address{' '}
              </p>
            </div>
          )}
        </div>
        <PreviousAddress
          showPreviousAddressForm={this.state.showPreviousAddressForm}
          previousAddressArray={R.pathOr(
            [],
            ['aboutYou', 'addressPreviousArray'],
            this.props
          )}
          updatePreviousAddressArray={this.props.updatePreviousAddressArray}
          getTimeAtLength={this.getTimeAtLength}
          togglePreviousAddressForm={val =>
            this.togglePreviousAddressForm(val)
          }
          currentAddressTime={{
            month: this.state.timeAtMonths,
            year: this.state.timeAtYears,
          }}
          lastPostcodeMappingTimestamp={
            this.props.aboutYou.lastPostcodeMappingTimestamp
          }
          {...this.props}
        />

        <PageNavigation
          noSideMargins
          next={this.state.nextEnabled && (() => this.proceed())}
          back={this.props.back}
          hasDisabledState
          className={'about-wrapper__navigation'}
          testNext={'about'}
          testBack={'about'}
        />
      </div>
    </div>
}

export default AboutComponent;
