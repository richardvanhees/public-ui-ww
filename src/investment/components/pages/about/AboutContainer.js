import { connect } from 'react-redux';
import AboutComponent from './AboutComponent';
import { goBack } from 'react-router-redux';
import { compose } from 'recompose';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { bindActionCreators } from 'redux';
import {
  sendPostcodeSearchSelect,
  sendPostcodeSearchReset,
  sendPostcodeSearchRequest,
  getApplicantPostcode,
  getSendPostcodeSearchResponse,
  getIsSearchingPostcode,
} from '../../../../../modules/postcode-anywhere';
import {
  toggleManualInputs,
  handleUserInput,
  setAboutAdressLine1,
  setAboutAdressLine2,
  setAboutAdressLine3,
  setAboutPostCode,
  setPostcodeToSearchFor,
  setTimeLivedAtCurrentAddress,
  updatePreviousAddressArray,
  setThreeYearsOfAddressesProvided,
  updateProgress,
  clearPendingRecords,
} from '../../../modules/about-you';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = state => {
  const { content, investmentGoal, aboutYou } = state;
  return {
    content: lookContentByKey(contentKey, content),
    investmentGoal,
    isSearchingPostcode: getIsSearchingPostcode(state, 'aboutYou'),
    sendPostcodeSearchResponse: getSendPostcodeSearchResponse(
      state,
      'aboutYou'
    ),
    aboutYou,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: goBack,
      setPostcodeToSearchFor: data => setPostcodeToSearchFor(data),
      next: () =>
        saveFactFindRequest({
          redirectPath: '/isa/capture',
        }),
      sendPostcodeSearchRequest: () =>
        sendPostcodeSearchRequest('aboutYou', getApplicantPostcode),
      sendPostcodeSearchSelect: (addressDetails, addressIndex) =>
        sendPostcodeSearchSelect(
          'aboutYou',
          addressDetails,
          'addressPreviousArray',
          addressIndex
        ),
      sendPostcodeSearchReset: addressDetails =>
        sendPostcodeSearchReset('aboutYou', addressDetails),
      handleUserInput,
      setAboutAdressLine1,
      setAboutAdressLine2,
      setAboutAdressLine3,
      setAboutPostCode,
      setTimeLivedAtCurrentAddress,
      setThreeYearsOfAddressesProvided,
      updatePreviousAddressArray,
      toggleManualInputs,
      updateProgress,
      clearPendingRecords,
    },
    dispatch
  );

export default () => {
  const NewAboutComponent = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );

  return NewAboutComponent(AboutComponent);
};
