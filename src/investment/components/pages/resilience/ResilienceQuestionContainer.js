import { connect } from 'react-redux';
import { compose } from 'recompose';
import isMobile from '../../../../../modules/utils/is-mobile';

import ResilienceQuestionComponent from './ResilienceQuestionComponent';
import { lookContentByKey } from '../../../../../modules/content';
import { handleFieldChange } from '../../../modules/resilience';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, resilience, browser, investmentGoal }) => ({
  content: lookContentByKey(contentKey, content),
  isMobile: isMobile(browser),
  resilienceData: resilience.data,
  investmentGoal,
});

const mapDispatchToProps = dispatch => ({
  handleFieldChange: (name, value) => dispatch(handleFieldChange(name, value)),
  onSetNextPage: () => dispatch(saveFactFindRequest({ doInBackground: true })),
});

const ResilienceQuestionContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default ResilienceQuestionContainer(ResilienceQuestionComponent);
