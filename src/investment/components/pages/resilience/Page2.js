import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../../../../../modules/elements/Dropdown';
import Error from '../../../../../modules/elements/Error';
import MultipleChoice from '../../../../../modules/elements/MultipleChoice';

export default class Page2 extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    resilienceData: PropTypes.object.isRequired,
    handleFieldChange: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.state = { errors: {} };
  }

  validate() {
    const {
      investment_resilience__investment_significance: investmentSignificance,
      investment_resilience__occupation: occupation,
      investment_resilience__health_status: healthStatus,
    } = this.props.resilienceData;

    if (!investmentSignificance) {
      this.setState({
        errors: { investmentSignificance: true },
      });
      return false;
    }
    if (!occupation) {
      this.setState({
        errors: { occupation: true },
      });
      return false;
    }
    if (!healthStatus) {
      this.setState({
        errors: { healthStatus: true },
      });
      return false;
    }

    this.setState({
      errors: {},
    });

    return true;
  }

  render() {
    return (
      <div className="resilience__page-2-container">
        <p>
          {
            this.props.content.resilienceQuestions[1].questionSections[0]
              .question
          }
        </p>
        <Dropdown
          dataTest="investment_resilience__investment_significance"
          onChange={val => {
            this.props.handleFieldChange(
              'investment_resilience__investment_significance',
              val
            );
          }}
          userInput={
            this.props.resilienceData
              .investment_resilience__investment_significance || null
          }
          options={
            this.props.content.resilienceQuestions[1].questionSections[0]
              .questionOptions.options
          }
          nullLabel={
            this.props.content.resilienceQuestions[1].questionSections[0]
              .questionOptions.nullLabel
          }
          className="resilience__dropdown"
        />
        <Error
          compact
          title={
            this.props.content.resilienceMessaging.investmentSignificanceError
          }
          active={this.state.errors.investmentSignificance}
          className="resilience__error-page-2"
        />
        <p>
          {
            this.props.content.resilienceQuestions[1].questionSections[1]
              .question
          }
        </p>
        <Dropdown
          dataTest="investment_resilience__occupation"
          onChange={val => {
            this.props.handleFieldChange(
              'investment_resilience__occupation',
              val
            );
          }}
          userInput={
            this.props.resilienceData.investment_resilience__occupation || null
          }
          options={
            this.props.content.resilienceQuestions[1].questionSections[1]
              .questionOptions.options
          }
          nullLabel={
            this.props.content.resilienceQuestions[1].questionSections[1]
              .questionOptions.nullLabel
          }
          className="resilience__dropdown"
        />
        <Error
          compact
          title={this.props.content.resilienceMessaging.occupationError}
          active={this.state.errors.occupation}
          className="resilience__error-page-2"
        />
        <p>
          {
            this.props.content.resilienceQuestions[1].questionSections[2]
              .question
          }
        </p>
        <MultipleChoice
          className="resilience__multiple-choice"
          answerClassName="answer--iw-multiple-choice-answer"
          onChange={val => {
            this.props.handleFieldChange(
              'investment_resilience__health_status',
              val
            );
          }}
          userInput={
            this.props.resilienceData.investment_resilience__health_status
          }
          values={
            this.props.content.resilienceQuestions[1].questionSections[2]
              .questionOptions.values
          }
        />
        <Error
          compact
          title={this.props.content.resilienceMessaging.healthStatusError}
          active={this.state.errors.healthStatus}
          className="resilience__error-page-2"
        />
      </div>
    );
  }
}
