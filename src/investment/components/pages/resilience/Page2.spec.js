import test from 'tape';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import Page2, { __RewireAPI__ } from './Page2';

test('<Page2> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__investment_significance: 'so high',
      investment_resilience__occupation: 'low rider',
      investment_resilience__health_status: 'top dawwwg',
    },
    pagerProps: {
      next: () => ({}),
    },
  };

  const FakeComponent = () => () => <div />;

  const DropdownStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const MultipleChoiceStub = FakeComponent();

  __RewireAPI__.__Rewire__('Dropdown', DropdownStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('MultipleChoice', MultipleChoiceStub);

  const target = mount(<Page2 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('Dropdown');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('MultipleChoice');

  assert.true(target.instance().validate(), 'inputs are valid');
});

test('<Page2> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__investment_significance: 'so high',
      investment_resilience__occupation: null,
      investment_resilience__health_status: 'top dawwwg',
    },
    pagerProps: {
      next: () => ({}),
    },
  };

  const FakeComponent = () => () => <div />;

  const DropdownStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const MultipleChoiceStub = FakeComponent();

  __RewireAPI__.__Rewire__('Dropdown', DropdownStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('MultipleChoice', MultipleChoiceStub);

  const target = mount(<Page2 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('Dropdown');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('MultipleChoice');

  assert.false(
    target.instance().validate(),
    'investment_resilience__occupation is null'
  );
});

test('<Page2> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__investment_significance: null,
      investment_resilience__occupation: 'low rider',
      investment_resilience__health_status: 'top dawwwg',
    },
    pagerProps: {
      next: () => ({}),
    },
  };

  const FakeComponent = () => () => <div />;

  const DropdownStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const MultipleChoiceStub = FakeComponent();

  __RewireAPI__.__Rewire__('Dropdown', DropdownStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('MultipleChoice', MultipleChoiceStub);

  const target = mount(<Page2 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('Dropdown');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('MultipleChoice');

  assert.false(
    target.instance().validate(),
    'investment_resilience__investment_significance is null'
  );
});

test('<Page2> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__investment_significance: 'so much',
      investment_resilience__occupation: 'low rider',
      investment_resilience__health_status: null,
    },
    pagerProps: {
      next: () => ({}),
    },
  };

  const FakeComponent = () => () => <div />;

  const DropdownStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const MultipleChoiceStub = FakeComponent();

  __RewireAPI__.__Rewire__('Dropdown', DropdownStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('MultipleChoice', MultipleChoiceStub);

  const target = mount(<Page2 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('Dropdown');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('MultipleChoice');

  assert.false(
    target.instance().validate(),
    'investment_resilience__health_status is null'
  );
});
