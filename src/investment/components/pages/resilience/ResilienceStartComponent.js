import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import { puzzleBottomRight } from '../../../../../assets/images/iw-images';

const ResilienceStartComponent = ({ content, next, back }) => (
  <div className="wrapper resilience">
    <PageHeading
      title={content.resilienceStart.title}
      subTitle={content.resilienceStart.subTitle}
      image={puzzleBottomRight}
      smallSubTitle
    />

    <div className="center-desktop">
      <Instructions
        title={content.resilienceStart.instructionsTitle}
        items={content.resilienceStart.instructions}
        list
      />

      <PageNavigation
        next={next}
        back={back}
        noSideMargins
        testNext={'resilience-start'}
        testBack={'resilience-start'}
      />
    </div>
  </div>
);

ResilienceStartComponent.propTypes = {
  content: PropTypes.object.isRequired,
  next: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
};

export default ResilienceStartComponent;
