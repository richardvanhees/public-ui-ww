import test from 'tape';
import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

import Page1, { __RewireAPI__ } from './Page1';

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23140,
      investment_resilience__monthly_expenses: 629,
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 50
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  assert.true(target.instance().validate(), 'inputs are valid');
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23140,
      investment_resilience__monthly_expenses: 626,
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 0
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(instance.validate(), 'expenses exceed gross income');
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 'aaa',
      investment_resilience__monthly_expenses: 629,
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 0
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(
    instance.validate(),
    'investment_resilience__gross_income not a number'
  );
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23140,
      investment_resilience__monthly_expenses: 'aaa',
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 0
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(
    instance.validate(),
    'investment_resilience__monthly_expenses not a number'
  );
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23140,
      investment_resilience__monthly_expenses: 626,
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 0
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(
    instance.validate(),
    'discretionary income is too high'
  );
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23139,
      investment_resilience__monthly_expenses: 1500,
      investment_resilience__monthly_income: 1592.95,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 93
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(
    instance.validate(),
    'discretionary income is too low for monthly payments'
  );
});

test('<Page1> ', assert => {
  assert.plan(1);

  const fakeProps = {
    content: require('../../../../../content/investment/athena/investment.json'),
    handleFieldChange: () => ({}),
    resilienceData: {
      investment_resilience__gross_income: 23140,
      investment_resilience__monthly_expenses: 626,
      investment_resilience__monthly_income: 1593.01,
    },
    pagerProps: {
      next: () => ({}),
    },
    investmentGoal: {
      monthly: 968
    },
  };

  const FakeComponent = () => () => <div />;

  const CurrencyInputStub = FakeComponent();
  const ErrorStub = FakeComponent();
  const PopupStub = FakeComponent();

  __RewireAPI__.__Rewire__('CurrencyInput', CurrencyInputStub);
  __RewireAPI__.__Rewire__('Error', ErrorStub);
  __RewireAPI__.__Rewire__('Popup', PopupStub);

  const target = mount(<Page1 {...fakeProps} />);

  __RewireAPI__.__ResetDependency__('CurrencyInput');
  __RewireAPI__.__ResetDependency__('Error');
  __RewireAPI__.__ResetDependency__('Popup');

  const instance = target.instance();
  assert.false(
    instance.validate(),
    'discretionary income is too low for monthly payments'
  );
});
