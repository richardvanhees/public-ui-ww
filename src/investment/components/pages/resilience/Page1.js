import React, { PureComponent } from 'react';
import CurrencyInput from '../../../../../modules/elements/CurrencyInput';
import Error from '../../../../../modules/elements/Error';
import axios from 'axios';

const CancelToken = axios.CancelToken;
import R from 'ramda';
import PropTypes from 'prop-types';
import Popup from '../../../../../modules/elements/Popup';
import WealthWizards from '../../../../../modules/wealthwizards';
import reactHTMLParser from 'react-html-parser';
import Tooltip from '../../../../../modules/elements/Tooltip';
import Icon from '../../../../../modules/elements/Icon';

export default class Page1 extends PureComponent {
  static propTypes = {
    content: PropTypes.object.isRequired,
    handleFieldChange: PropTypes.func.isRequired,
    resilienceData: PropTypes.object.isRequired,
    pagerProps: PropTypes.object.isRequired,
    investmentGoal: PropTypes.object.isRequired,
    isMobile: PropTypes.bool.isRequired,
  };

  static getDerivedStateFromProps(nextProps) {
    if (!nextProps.resilienceData.investment_resilience__gross_income) {
      return {
        calculatedTaxPerMonth: null,
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      calculatedTaxPerMonth: null,
      inputtedTaxPerMonth: null,
      incomeLoading: false,
      errors: {
        grossIncome: false,
        monthlyExpenses: false,
        monthlyIncome: false,
      },
      continueDespiteExpensesExceedingIncome: false,
    };
    this.calculateTax = this.calculateTax.bind(this);
    this.errorOnePopup = null;
    this.errorTwoPopup = null;
    this.errorThreePopup = null;
    this.toolTipPopup = null;
  }

  toggleTooltip = () => {
    if (!this.props.isMobile) return;
    this.toolTipPopup.toggle();
  };

  calculateTax(gross) {
    if (!R.is(Number, gross) || gross <= 0) {
      return this.setState({
        calculatedTaxPerMonth: null,
      });
    }

    this.setState({
      incomeLoading: true,
      errors: {
        ...this.state.errors,
        grossIncome: false,
      },
    });

    if (this.cancelToken) {
      this.cancelToken.cancel();
    }

    this.cancelToken = CancelToken.source();

    return axios({
      method: 'post',
      url: `${WealthWizards.INCOME_TAX_SVC_BASE_URL}/v1/calculate`,
      data: {
        gross_income: gross,
        income_period: 'ANNUALLY',
        apply_tax_reduction_thresholds: true,
      },
      cancelToken: this.cancelToken.token,
    })
      .then(res => {
        const calculatedTaxPerMonth = Number((
          (gross -
            res.data.total_tax -
            res.data.class_a_national_insurance) /
          12
        ).toFixed(2));
        this.setState({
          incomeLoading: false,
          calculatedTaxPerMonth,
        });
        this.monthlyIncomeInput.externalUpdateValue(calculatedTaxPerMonth);
      })
      .catch(e => {
        if (!axios.isCancel(e)) {
          this.setState({
            incomeLoading: false,
          });
        }
      });
  }

  resetPopupValues() {
    this.props.handleFieldChange(
      'investment_resilience__discretionary_income_excess_popup',
      'n/a'
    );
    this.props.handleFieldChange(
      'investment_resilience__low_monthly_discretionary_income_popup',
      'n/a'
    );
    this.props.handleFieldChange(
      'investment_resilience__discretionary_income_both_popup',
      'n/a'
    );
  }

  validate() {
    const {
      investment_resilience__gross_income: grossIncome,
      investment_resilience__monthly_expenses: monthlyExpenses,
      investment_resilience__monthly_income: monthlyIncome,
    } = this.props.resilienceData;

    this.resetPopupValues();

    const monthlyContribution = this.props.investmentGoal.monthly;
    const monthlyDiscretionaryIncome = monthlyIncome - monthlyExpenses;
    const annualDiscretionaryIncome = monthlyDiscretionaryIncome * 12;
    const excessDiscretionaryIncome = (annualDiscretionaryIncome / grossIncome) > 0.50;
    const lowMonthlyDiscretionaryIncome = monthlyContribution && monthlyDiscretionaryIncome < monthlyContribution;

    if (!R.is(Number, grossIncome) || grossIncome === 0) {
      this.setState({
        errors: { grossIncome: true },
      });
      return false;
    }

    if (!R.is(Number, monthlyIncome) || monthlyIncome === 0) {
      this.setState({
        errors: { monthlyIncome: true },
      });
      return false;
    }

    if (!R.is(Number, monthlyExpenses) || monthlyExpenses === 0) {
      this.setState({
        errors: { monthlyExpenses: true },
      });
      return false;
    }

    if (excessDiscretionaryIncome && !lowMonthlyDiscretionaryIncome) {
      this.errorOnePopup && this.errorOnePopup.toggle();
      return false;
    }

    if (lowMonthlyDiscretionaryIncome && !excessDiscretionaryIncome) {
      this.errorTwoPopup && this.errorTwoPopup.toggle();
      return false;
    }

    if (excessDiscretionaryIncome && lowMonthlyDiscretionaryIncome) {
      this.errorThreePopup && this.errorThreePopup.toggle();
      return false;
    }

    this.setState({
      errors: {},
    });

    return true;
  }

  render() {
    const { resilienceData, content, isMobile } = this.props;
    const tooltip = {
      title: content.resilienceMessaging.expenditureTooltipTitle,
      text: content.resilienceMessaging.expenditureTooltipText,
    };

    return (
      <div className="resilience__currency-container">
        <p className="resilience__currency-text">
          {content.resilienceQuestions[0].questionSections[0].question}
        </p>
        <CurrencyInput
          className="resilience__currency-input"
          dataTest="investment_resilience__gross_income"
          hintText="Gross income"
          onChange={grossIncome => {
            this.calculateTax(grossIncome);
            this.props.handleFieldChange(
              'investment_resilience__gross_income',
              grossIncome
            );
          }}
          value={resilienceData.investment_resilience__gross_income}
        />
        <Error
          compact
          title={content.resilienceMessaging.grossIncomeError}
          active={this.state.errors.grossIncome}
        />
        <p className="resilience__currency-text">
          {content.resilienceQuestions[0].questionSections[1].question}
        </p>
        <CurrencyInput
          ref={(input) => {
            this.monthlyIncomeInput = input;
          }}
          className="resilience__currency-input"
          dataTest="investment_resilience__monthly_income"
          hintText="Monthly income"
          value={resilienceData.investment_resilience__monthly_income}
          onChange={monthlyIncome => {
            this.props.handleFieldChange(
              'investment_resilience__monthly_income',
              monthlyIncome
            );
          }}
        />
        <Error
          compact
          title={content.resilienceMessaging.monthlyIncomeError}
          active={this.state.errors.monthlyIncome}
        />
        <p className="resilience__currency-text">
          {content.resilienceQuestions[0].questionSections[2].question}
          <span
            data-test="tooltip"
            onClick={() => this.toggleTooltip()}
            className="resilience__tooltip-container"
          >
            <Icon
              dataTip={`<strong>${tooltip.title}</strong> ${tooltip.text}`}
              name="question-circle"
              className="resilience__tooltip-icon"
            />
          </span>
        </p>
        <CurrencyInput
          className="resilience__currency-input"
          dataTest="investment_resilience__monthly_expenses"
          hintText="Essential costs"
          value={resilienceData.investment_resilience__monthly_expenses}
          onChange={essentialLivingCosts => {
            this.props.handleFieldChange(
              'investment_resilience__monthly_expenses',
              essentialLivingCosts
            );
          }}
        />
        <Error
          compact
          title={content.resilienceMessaging.monthlyExpensesError}
          active={this.state.errors.monthlyExpenses}
        />
        <Popup
          ref={c => {
            this.errorOnePopup = c;
          }}
          isNarrow
          closeButtonTop
          closeClickOutside
          title="Are you sure?"
          dataTest="discretionary-income-excess-popup"
          buttons={[
            {
              label: 'Back',
              onClick: () => {
                this.props.handleFieldChange(
                  'investment_resilience__discretionary_income_excess_popup',
                  'n/a'
                );
                this.errorOnePopup.toggle();
              },
              className: 'questions-component__btn-popup ',
              dataTest: 'discretionary-income-excess-popup-no',
            },
            {
              label: 'Confirm',
              onClick: async () => {
                this.props.handleFieldChange(
                  'investment_resilience__discretionary_income_excess_popup',
                  'yes'
                );
                await this.errorOnePopup.toggle();
                this.props.pagerProps.next({ skipValidation: true });
              },
              className: 'questions-component__btn-popup btn--primary',
              dataTest: 'discretionary-income-excess-popup-yes',
            },
          ]}
        >
          {reactHTMLParser(content.resilienceMessaging.popup1)}
        </Popup>
        <Popup
          ref={c => {
            this.errorTwoPopup = c;
          }}
          isNarrow
          closeButtonTop
          closeClickOutside
          title="Are you sure?"
          dataTest="low-monthly-discretionary-income-popup"
          buttons={[
            {
              label: 'Back',
              onClick: () => {
                this.props.handleFieldChange(
                  'investment_resilience__low_monthly_discretionary_income_popup',
                  'n/a'
                );
                this.errorTwoPopup.toggle();
              },
              className: 'questions-component__btn-popup ',
              dataTest: 'low-monthly-discretionary-income-popup-no',
            },
            {
              label: 'Confirm',
              onClick: async () => {
                this.props.handleFieldChange(
                  'investment_resilience__low_monthly_discretionary_income_popup',
                  'yes'
                );
                await this.errorTwoPopup.toggle();
                this.props.pagerProps.next({ skipValidation: true });
              },
              className: 'questions-component__btn-popup btn--primary',
              dataTest: 'low-monthly-discretionary-income-popup-yes',
            },
          ]}
        >
          {reactHTMLParser(content.resilienceMessaging.popup2)}
        </Popup>
        <Popup
          ref={c => {
            this.errorThreePopup = c;
          }}
          isNarrow
          closeButtonTop
          closeClickOutside
          title="Are you sure?"
          dataTest="discretionary-income-both-popup"
          buttons={[
            {
              label: 'Back',
              onClick: () => {
                this.props.handleFieldChange(
                  'investment_resilience__discretionary_income_both_popup',
                  'n/a'
                );
                this.errorThreePopup.toggle();
              },
              className: 'questions-component__btn-popup ',
              dataTest: 'discretionary-income-both-popup-no',
            },
            {
              label: 'Confirm',
              onClick: async () => {
                this.props.handleFieldChange(
                  'investment_resilience__discretionary_income_both_popup',
                  'yes'
                );
                await this.errorThreePopup.toggle();
                this.props.pagerProps.next({ skipValidation: true });
              },
              className: 'questions-component__btn-popup btn--primary',
              dataTest: 'discretionary-income-both-popup-yes',
            },
          ]}
        >
          {reactHTMLParser(content.resilienceMessaging.popup3)}
        </Popup>
        {!isMobile && <Tooltip />}
        {isMobile && (
          <Popup
            ref={c => {
              this.toolTipPopup = c;
            }}
            closeButtonTop
            closeClickOutside
            className={'resilience__tooltip-popup'}
            title={tooltip.title}
            buttons={[{ label: 'Close', onClick: this.toggleTooltip }]}
          >
            {tooltip.text}
          </Popup>
        )}
      </div>
    );
  }
}
