import React from 'react';
import PropTypes from 'prop-types';
import Pager from '../../../../../modules/elements/Pager';
import WealthWizards from '../../../../../modules/wealthwizards';
import Page1 from './Page1';
import Page2 from './Page2';

const ResilienceQuestionComponent = props => {
  const pagerOptions = {
    pages: [
      {
        pageContent: Page1,
        title: 'Resilience',
        style: {
          width: props.isMobile ? 400 : 550,
        },
        requiresValidation: true,
      },
      {
        pageContent: Page2,
        title: 'Resilience',
        style: {
          width: props.isMobile ? 400 : 550,
        },
        requiresValidation: true,
      },
    ],
    dataTest: 'resilience',
    onSetNextPage: props.onSetNextPage,
    parentProps: props,
    previousPage: `${WealthWizards.CONTEXT_ROUTE}/resilience/start`,
    nextPage: `${WealthWizards.CONTEXT_ROUTE}/resilience/playback`,
  };

  return (
    <div className="wrapper resilience">
      <Pager {...props} {...pagerOptions} />
    </div>
  );
};

ResilienceQuestionComponent.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  onSetNextPage: PropTypes.func.isRequired,
};

export default ResilienceQuestionComponent;
