import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PieChartWithLegend from '../../../../../modules/elements/PieChartWithLegend';

export default class FundBreakdown extends PureComponent {
  static propTypes = {
    content: PropTypes.object.isRequired,
    friendlyFundName: PropTypes.string.isRequired,
    pieChartValues: PropTypes.array.isRequired,
    factSheetUrl: PropTypes.string.isRequired,
  };

  render() {
    const { content, pieChartValues, friendlyFundName, factSheetUrl } = this.props;
    const {
      title,
      subtitle,
      labelHeader,
      popupLink,
      colors,
    } = content.adviceSummary.page2;

    const data = pieChartValues.map(value => ({
      value: value.value,
      color: colors[value.name],
      label: value.name,
    }));
    return (
      <div
        data-test="advice-summary-page-page-2"
        className="advice-summary-page advice-summary-page--column"
      >
        <h2 className="advice-summary-page__title">{title}</h2>
        <h3 className="advice-summary-page__subtitle">{subtitle}</h3>

        <div className="advice-summary-page__pie-chart-container">
          <PieChartWithLegend
            data={data}
            title={friendlyFundName}
            labelHeader={labelHeader}
            link={{
              label: popupLink,
              onClick: () => window.open(factSheetUrl, '_blank'),
            }}
          />
        </div>
      </div>
    );
  }
}
