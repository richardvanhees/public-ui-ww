import { connect } from 'react-redux';
import AdviceSummaryComponent from './AdviceSummaryComponent';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import { lookContentByKey } from '../../../../../modules/content/selectors';
import { individualSolution } from '../../../modules/solution-metadata/selector';
import { solutionMetadataUpdated } from '../../../modules/solution-metadata/actions';
import { publishToCrm } from '../../../modules/solution/actions';
import { setError } from '../../../../../modules/browser/actions';
import WealthWizards from '../../../../../modules/wealthwizards';
import { push } from 'react-router-redux';
import { alertBasedReferral } from '../../../modules/solution/selectors';

const contentKey = 'investment';

export const mapStateToProps = ({
  content,
  investmentGoal,
  solution,
  solutionMetadata,
}) => {
  const investmentIsaAllocations = solution.data.investmentIsaAllocations;
  return {
    content: lookContentByKey(contentKey, content),
    investmentGoal,
    riskLevel: solution.data.riskLevel,
    solutionId: solution.solution_id,
    growthRates: investmentIsaAllocations.growth_rates,
    expectedGrowth: investmentIsaAllocations.expectedGrowth.year10.rounded,
    pieChartValues: investmentIsaAllocations.pieChartValues,
    unitPriceSpread: investmentIsaAllocations.unitPriceSpread.toFixed(2),
    expectedGrowthChargePct: investmentIsaAllocations.expectedGrowthChargePct,
    costToYouRounded: investmentIsaAllocations.costToYouRounded.year1,
    file: investmentIsaAllocations.file,
    friendlyFundName: investmentIsaAllocations.name,
    solutionMetadata,
    solutionMetadatum: individualSolution(
      solutionMetadata,
      solution.solution_id
    ),
    alertBasedReferral: alertBasedReferral(solution),
    factSheetUrl: investmentIsaAllocations.factSheetUrl,
  };
};

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      solutionMetadataUpdated,
      onComplete: () => publishToCrm(),
      setError: msg => setError(msg),
      goToDashboard: () => push(`${WealthWizards.CONTEXT_ROUTE}/dashboard`),
    },
    dispatch
  );

export default () => {
  const AdviceSummaryContainer = compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  );
  return AdviceSummaryContainer(AdviceSummaryComponent);
};
