import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import AdviceSummaryContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './AdviceSummaryContainer';

test('<AdviceSummaryContainer />', assert => {
  const sandbox = sinon.sandbox.create();

  const saveFactFindRequestStub = sandbox.stub();
  const bindActionCreatorsStub = sandbox.stub();

  const dispatchStub = sandbox.stub();
  const propsStub = sandbox.stub();

  // check map state to props
  __RewireAPI__.__Rewire__('termsSelector', () => ({}));
  __RewireAPI__.__Rewire__('lookContentByKey', () => ({}));
  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestStub);

  const stateResult = mapStateToProps({
    solution: {
      data: {
        riskLevel: 1,
        investmentIsaAllocations: {
          expectedGrowth: { year10: { rounded: [100, 110, 120] } },
          growth_rates: [1, 2, 3],
          pieChartValues: [],
          file: 'file',
          name: 'name',
          costToYouRounded: { year1: [1, 2, 3] },
          expectedGrowthChargePct: 5,
          unitPriceSpread: 2,
          factSheetUrl: 'someUrl.com'
        },
      },
      solution_id: 'foo',
      alertBasedReferral: false,
    },
    solutionMetadata: {
      data: [],
    },
    content: {},
    terms: {},
    investmentGoal: {},
  });
  assert.deepEqual(
    stateResult,
    {
      content: {},
      investmentGoal: {},
      riskLevel: 1,
      solutionMetadatum: undefined,
      solutionId: 'foo',
      solutionMetadata: {
        data: [],
      },
      alertBasedReferral: false,
      expectedGrowth: [100, 110, 120],
      growthRates: [1, 2, 3],
      pieChartValues: [],
      file: 'file',
      friendlyFundName: 'name',
      costToYouRounded: [1, 2, 3],
      expectedGrowthChargePct: 5,
      unitPriceSpread: '2.00',
      factSheetUrl: 'someUrl.com'
    },
    'correct state built from props'
  );

  // check map dispatch to props
  __RewireAPI__.__Rewire__('bindActionCreators', bindActionCreatorsStub);

  const dispatchResult = mapDispatchToProps(dispatchStub, propsStub);
  assert.notEqual(
    bindActionCreatorsStub.args[0][0].onComplete,
    undefined,
    'onComplete is defined'
  );
  assert.notEqual(
    bindActionCreatorsStub.args[0][0].solutionMetadataUpdated,
    undefined,
    'solutionMetadataUpdated is defined'
  );

  __RewireAPI__.__ResetDependency__('termsSelector');
  __RewireAPI__.__ResetDependency__('lookContentByKey');
  __RewireAPI__.__ResetDependency__('saveFactFindRequest');

  __RewireAPI__.__ResetDependency__('bindActionCreators');

  assert.end();
});
