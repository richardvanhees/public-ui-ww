import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '../../../../../modules/elements/Table';
import ListBlock from '../../../../../modules/elements/ListBlock';
import Checkbox from '../../../../../modules/elements/Checkbox';
import Icon from '../../../../../modules/elements/Icon';
import Error from '../../../../../modules/elements/Error';
import Popup from '../../../../../modules/elements/Popup';
import R from 'ramda';
import ReactHTMLParser from 'react-html-parser';

export default class FeesAndCharges extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    riskLevel: PropTypes.number.isRequired,
    solutionId: PropTypes.string.isRequired,
    solutionMetadatum: PropTypes.object,
    solutionMetadataUpdated: PropTypes.func.isRequired,
    expectedGrowthChargePct: PropTypes.string.isRequired,
    costToYouRounded: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      readDocuments: R.pathOr(
        false,
        ['solutionMetadatum', 'read_documents'],
        props
      ),
      acceptedAdvice: R.pathOr(
        false,
        ['solutionMetadatum', 'accepted_advice'],
        props
      ),
      disabled: {
        read_documents: false,
        accepted_advice: false,
        ...props.solutionMetadatum,
      },
      errors: {
        readDocuments: false,
        acceptedAdvice: false,
      },
    };
  }

  togglePopup = (popupTitle, popupText) => {
    this.setState({
      ...this.state,
      popupTitle: typeof popupTitle === 'string' ? popupTitle : '',
      popupText: typeof popupText === 'string' ? popupText : '',
    });
    this.helpTextPopup.toggle();
  };

  updateTerms = () => {
    this.props.solutionMetadataUpdated &&
      this.props.solutionMetadataUpdated(this.props.solutionId, {
        readDocuments: this.state.readDocuments,
        acceptedAdvice: this.state.acceptedAdvice,
      });
  };

  validate = () => {
    const readDocuments = this.state.readDocuments;
    const acceptedAdvice = this.state.acceptedAdvice;

    if (!(readDocuments && acceptedAdvice)) {
      this.setState({
        ...this.state,
        errors: {
          readDocuments: !readDocuments,
          acceptedAdvice: !acceptedAdvice,
        },
      });
    } else {
      this.updateTerms();
      return true;
    }
    return false;
  };

  render() {
    const {
      content,
      riskLevel,
      solutionMetadatum,
      expectedGrowthChargePct,
      costToYouRounded,
    } = this.props;
    const { readDocuments, acceptedAdvice } = this.state;
    const {
      title,
      subtitle,
      feeTable,
      documentsHeader,
      documents,
      confirmRead,
      confirmInvestment,
      reviewServicePopup,
      feesAndChargesPopup,
    } = content.adviceSummary.page3;

    const feeTableWithDynamicValues = R.clone(feeTable);

    feeTableWithDynamicValues.rows[1][0] = (
      <span>
        {feeTable.rows[1][0]}
        <Icon
          className="advice-summary-page__info-icon"
          data-test={'review-icon'}
          name="info-circle"
          onClick={() =>
            this.togglePopup(reviewServicePopup.title, reviewServicePopup.body)
          }
        />
      </span>
    );

    feeTableWithDynamicValues.headerLabels[0] = (
      <span>
        {feeTable.headerLabels[0]}
        <Icon
          className="advice-summary-page__info-icon"
          data-test={'header-icon'}
          name="info-circle"
          onClick={() =>
            this.togglePopup(
              feesAndChargesPopup.title,
              feesAndChargesPopup.body
            )
          }
        />
      </span>
    );

    feeTableWithDynamicValues.rows[2][1] = `${expectedGrowthChargePct}%`;
    feeTableWithDynamicValues.rows[2][2] = `£${costToYouRounded[1].toLocaleString(
      'en-GB',
      { minimumFractionDigits: 2 }
    )}`;
    feeTableWithDynamicValues.footerRow[2] = `£${costToYouRounded[1].toLocaleString(
      'en-GB',
      { minimumFractionDigits: 2 }
    )}`;

    return (
      <div
        data-test="advice-summary-page-page-3"
        className="advice-summary-page advice-summary-page--column"
      >
        <h3 className="advice-summary-page__fees-title">{title}</h3>
        <h4 className="advice-summary-page__fees-subtitle">{subtitle}</h4>

        <div className="advice-summary-page__fees-container advice-summary-page__fees-container--first">
          <div className="advice-summary-page__fees-content advice-summary-page__fees-content--left">
            <Table {...feeTableWithDynamicValues} />
          </div>

          <div className="advice-summary-page__fees-content advice-summary-page__fees-content--right">
            <ListBlock className="advice-summary-page__documents-container">
              <div className="advice-summary-page__documents-header">
                {documentsHeader}
              </div>

              {documents.map(
                (document, i) =>
                  document.riskLevels.includes(riskLevel) && (
                    <a
                      href={document.url}
                      target="_blank"
                      className="advice-summary-page__documents-link"
                      key={`document-${i}`}
                    >
                      {document.label}
                    </a>
                  )
              )}
            </ListBlock>
          </div>
        </div>

        <div className="advice-summary-page__fees-container advice-summary-page__fees-container--last">
          <div className="advice-summary-page__fees-content">
            <ListBlock>
              <Checkbox
                label={confirmRead}
                onChange={val => this.setState({ readDocuments: val })}
                name={'confirmRead'}
                dataTest={'advice-summary-confirm-read'}
                className="advice-summary-page__fees-first-checkbox"
                type={'custom'}
                checked={readDocuments}
                disabled={
                  this.state.disabled.read_documents &&
                  solutionMetadatum.read_documents
                }
              />
              <Error
                compact
                inline
                active={this.state.errors.readDocuments}
                title={
                  "You need to confirm that you've read the documents before you can continue"
                }
                className="advice-summary-page__error-message"
              />
              <Checkbox
                label={confirmInvestment}
                onChange={val => this.setState({ acceptedAdvice: val })}
                name={'confirmInvestment'}
                dataTest={'advice-summary-confirm-investment'}
                type={'custom'}
                checked={acceptedAdvice}
                disabled={
                  this.state.disabled.accepted_advice &&
                  solutionMetadatum.accepted_advice
                }
              />
              <Error
                compact
                inline
                active={this.state.errors.acceptedAdvice}
                title={
                  'You need to accept the statement before you can continue'
                }
                className="advice-summary-page__error-message"
              />
            </ListBlock>
          </div>
        </div>
        <Popup
          ref={c => {
            this.helpTextPopup = c;
          }}
          closeButtonTop
          closeClickOutside
          title={this.state.popupTitle}
          buttons={[
            {
              label: 'Close',
              onClick: this.togglePopup,
            },
          ]}
        >
          {ReactHTMLParser(this.state.popupText)}
        </Popup>
      </div>
    );
  }
}
