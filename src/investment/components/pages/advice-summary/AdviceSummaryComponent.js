import React from 'react';
import WealthWizards from '../../../../../modules/wealthwizards';
import R from 'ramda';
import PropTypes from 'prop-types';

import Pager from '../../../../../modules/elements/Pager';
import Page1 from './Page1';
import FundBreakdown from './FundBreakdown';
import RiskIllustration from './RiskIllustration';
import FeesAndCharges from './FeesAndCharges';

const AdviceSummaryComponent = props => {
  if (isNaN(props.riskLevel)) {
    return <div />;
  }

  const cleanProps = R.omit('onComplete', props);

  let nextPage;
  if (props.alertBasedReferral) {
    nextPage = `${WealthWizards.CONTEXT_ROUTE}/alert-referral`;
  } else {
    nextPage = WealthWizards.THIRD_PARTY_PAYMENT_SCREEN_ENABLED
      ? `${WealthWizards.CONTEXT_ROUTE}/payment`
      : `${WealthWizards.CONTEXT_ROUTE}/advice-accepted`;
  }

  const pagerOptions = {
    pages: [
      {
        pageContent: Page1,
        title: 'Your investment summary',
        subtitle: "Here's a summary of our recommendation",
        nextLabel: 'I understand',
        style: {
          width: 950,
        },
      },
      {
        pageContent: FundBreakdown,
        title: 'Your investment summary',
        nextLabel: 'I understand',
        style: {
          width: 550,
        },
      },
      {
        pageContent: RiskIllustration,
        title: 'Your investment summary',
        nextLabel: 'I understand',
        style: {
          width: 850,
        },
      },
      {
        pageContent: FeesAndCharges,
        title: 'Your investment summary',
        nextLabel: 'I accept',
        style: {
          width: 850,
        },
        onComplete: props.onComplete,
        requiresValidation: true,
      },
    ],
    parentProps: cleanProps,
    dataTest: 'advice-summary',
    previousPage: `${WealthWizards.CONTEXT_ROUTE}/confirm-objective`,
    nextPage,
  };

  return (
    <div className="advice-summary">
      <Pager {...cleanProps} {...pagerOptions} />
    </div>
  );
};

AdviceSummaryComponent.propTypes = {
  onComplete: PropTypes.func,
  riskLevel: PropTypes.number,
  alertBasedReferral: PropTypes.bool.isRequired,
};

export default AdviceSummaryComponent;
