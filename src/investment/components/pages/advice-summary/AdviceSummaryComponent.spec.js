import test from 'tape';
import React from 'react';
import { Provider } from 'react-redux';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AdviceSummary, { __RewireAPI__ } from './AdviceSummaryComponent';
import content from '../../../../../content/investment/athena/investment';
import R from 'ramda';
import sinon from 'sinon';
configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<AdviceSummary>', assert => {
  assert.plan(1);
  assert.equals(
    typeof AdviceSummary.propTypes,
    'object',
    'PropTypes are defined'
  );
});

const sandbox = sinon.sandbox.create();
const onComplete = sandbox.stub();

const setup = (solutionMetadatum, monthly = 0, setPage) => {
  const fakeProps = {
    setPage: setPage || 3,
    riskLevel: 3,
    expectedGrowth: [100, 110, 120],
    growthRates: [1, 2, 3],
    expectedGrowthChargePct: 6,
    costToYouRounded: [1, 2, 3],
    investmentGoal: {
      length: '5 years',
      goal: 'buyFirstHome',
      amount: 500,
      monthly,
      icon: 'home',
      target: 0,
    },
    solutionMetadatum,
    onComplete,
    content,
    match: {
      params: {
        activePage: 4,
      },
      path: '',
    },
    goToDashboard: () => ({}),
    setError: () => ({}),
    pieChartValues: [
      {
        name: 'bonds',
        value: 61.3,
      },
      {
        name: 'equities',
        value: 19.2,
      },
      {
        name: 'property',
        value: 8.2,
      },
      {
        name: 'cash',
        value: 11.3,
      },
    ],
    file: 'filename',
    friendlyFundName: 'friendlyfundname',
  };

  const fakeStore = {
    getState: () => {},
    subscribe: () => null,
    dispatch: () => null,
  };

  return mount(
    <Provider store={fakeStore}>
      <AdviceSummary {...fakeProps} />
    </Provider>
  );
};

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup({});
  const target = context.find('[data-test="advice-summary-page-page-3"]');

  assert.equals(target.length, 1, 'when page 3 - should show page three');
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup({});
  let target = context.find('input.checkbox__checkbox').getElements();
  target = R.filter(t => !t.props.checked, target);

  assert.equals(
    target.length,
    2,
    'default - should not show confirmation checkboxes as unchecked'
  );
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup({
    solution_id: '1',
    accepted_advice: false,
    read_documents: false,
  });
  let target = context.find('input.checkbox__checkbox').getElements();
  target = R.filter(t => !t.props.disabled, target);

  assert.equals(
    target.length,
    2,
    'default - should show confirmation checkboxes as enabled'
  );
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup({
    solution_id: '1',
    accepted_advice: true,
    read_documents: true,
  });
  let target = context.find('input.checkbox__checkbox').getElements();
  target = R.filter(t => t.props.checked, target);

  assert.equals(
    target.length,
    2,
    'when previously accepted - should show confirmation checkboxes as checked'
  );
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup({
    solution_id: '1',
    accepted_advice: true,
    read_documents: true,
  });
  let target = context.find('input.checkbox__checkbox').getElements();
  target = R.filter(t => t.props.disabled, target);

  assert.equals(
    target.length,
    2,
    'when previously accepted - confirmation checkboxes should be disabled'
  );
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('WealthWizards', {
    THIRD_PARTY_PAYMENT_SCREEN_ENABLED: 1,
    CONTEXT_ROUTE: '/investment',
  });

  const context = setup({
    solution_id: '1',
    accepted_advice: true,
    read_documents: true,
  });

  const pagerComponent = context.find('Pager');
  assert.equal(
    pagerComponent.prop('nextPage'),
    '/investment/payment',
    'redirection to payment screen when THIRD_PARTY_PAYMENT_SCREEN_ENABLED = true'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  __RewireAPI__.__Rewire__('WealthWizards', {
    THIRD_PARTY_PAYMENT_SCREEN_ENABLED: 0,
    CONTEXT_ROUTE: '/investment',
  });

  const context = setup({
    solution_id: '1',
    accepted_advice: true,
    read_documents: true,
  });

  const pagerComponent = context.find('Pager');
  assert.equal(
    pagerComponent.prop('nextPage'),
    '/investment/advice-accepted',
    'redirection to advice-accepted screen when THIRD_PARTY_PAYMENT_SCREEN_ENABLED = false'
  );

  __RewireAPI__.__ResetDependency__('WealthWizards');
});

test('<AdviceSummary>', assert => {
  assert.plan(1);

  const context = setup(
    {
      solution_id: '1',
      accepted_advice: true,
      read_documents: true,
    },
    null,
    4
  );

  context.find('[data-test="next-btn-page-4"]').simulate('click');
  assert.equals(
    onComplete.callCount,
    1,
    'default - onComplete should be called'
  );
});

test('<AdviceSummary> show monthly value section when monthly value >= 1', assert => {
  assert.plan(2);
  const context = setup(
    {
      accepted_advice: true,
      read_documents: true,
    },
    1000
  );
  const multipleItemBlock = context.find(
    '.advice-summary-page-section-multiple__investment-item'
  );
  assert.equals(
    multipleItemBlock.length,
    4,
    'the monthly item blocks are visible'
  );
  assert.deepEquals(
    multipleItemBlock.get(3).props.children,
    ['£', '1,000'],
    'the correct monthly value is displayed'
  );
});

test('<AdviceSummary> DO NOT show monthly value section when monthly value <= 0', assert => {
  assert.plan(1);
  const context = setup(
    {
      accepted_advice: true,
      read_documents: true,
    },
    0
  );
  const multipleItemBlock = context.find(
    '.advice-summary-page-section-multiple__investment-item'
  );
  assert.equals(
    multipleItemBlock.length,
    0,
    'the monthly item blocks are NOT visible'
  );
});

test('<AdviceSummary> - correct next button title appears on page 3', assert => {
  assert.plan(2);

  const context = setup({});
  const target = context.find('[data-test="advice-summary-page-page-3"]');
  const nextButton = context
    .find('.btn--primary')
    .last()
    .text();

  assert.equals(target.length, 1, 'when page 3 - should show page three');
  assert.equals(nextButton, 'I accept', 'Button has correct Label');
});
