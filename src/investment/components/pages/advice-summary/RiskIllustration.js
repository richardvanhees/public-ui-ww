import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Spinner from '../../../../../modules/elements/Spinner';
import WealthWizards from '../../../../../modules/wealthwizards';
import axios from 'axios';

function loadExternalJs(js) {
  const script = document.createElement('script');
  const textNode = document.createTextNode(js);
  script.appendChild(textNode);
  document.body.appendChild(script);
}

const errorString =
  'We are currently unable to render your risk illustration, please try again later';
const idOfElToRenderGraph = 'risk-graph';

export default class RiskIllustration extends Component {
  static propTypes = {
    riskLevel: PropTypes.number.isRequired,
    setError: PropTypes.func.isRequired,
    goToDashboard: PropTypes.func.isRequired,
  };

  constructor() {
    super();
    this.state = {
      loaded: window.WealthWizardsRiskIllustration !== undefined,
    };
  }

  componentDidMount() {
    if (window.WealthWizardsRiskIllustration) {
      try {
        window.WealthWizardsRiskIllustration.load(
          // the id of the div where we render the app
          idOfElToRenderGraph,
          this.graphOptions()
        );
      } catch (e) {
        this.props.goToDashboard();
        this.props.setError(errorString);
      }
      return;
    }

    // go and fetch the Js to render from the microui
    axios({
      url: `${WealthWizards.RISK_ILLUSTRATION_MICRO_UI_BASE_URL}/app`,
    })
      .then(res => {
        loadExternalJs(res.data);

        this.setState({ loaded: true });

        // load the graph now the JS has been evaluated
        window.WealthWizardsRiskIllustration.load(
          idOfElToRenderGraph,
          this.graphOptions()
        );
      })
      .catch(() => {
        this.props.goToDashboard();
        this.props.setError(errorString);
      });
  }

  graphOptions() {
    const isIE = /MSIE|Trident|Edge/.test(window.navigator.userAgent);
    return {
      riskLevel: this.props.riskLevel,
      height: 400,
      width: 768,
      baseHref: isIE ? '' : '/investment/advice-summary/3',
    };
  }

  render() {
    return (
      <div>
        {!this.state.loaded && <Spinner visible />}
        <div id={idOfElToRenderGraph} />
      </div>
    );
  }
}
