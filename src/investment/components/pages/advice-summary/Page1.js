import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import R from 'ramda';
import interpolate from '../../../../../modules/utils/interpolate';
import rounder from '../../../../../modules/utils/round-to-significant-figures';
import * as images from '../../../../../assets/images/iw-images';

import Icon from '../../../../../modules/elements/Icon';
import ContentBlock from '../../../../../modules/elements/ContentBlock';
import Popup from '../../../../../modules/elements/Popup';

const dialPositions = [-81, -58, -28, 0, 29, 58, 82];

export default class Page1 extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    riskLevel: PropTypes.number.isRequired,
    investmentGoal: PropTypes.object.isRequired,
    expectedGrowth: PropTypes.array.isRequired,
    growthRates: PropTypes.array.isRequired,
    unitPriceSpread: PropTypes.string.isRequired,
  };

  constructor() {
    super();

    this.animate = this.animate.bind(this);

    this.state = {
      dialPosition: -81,
      riskLevel: 0,
    };
  }

  componentDidMount() {
    // Extra delay for our slow Microsoft friend :/
    const timeout = /Trident\/|MSIE/.test(window.navigator.userAgent)
      ? 1500
      : 500;
    setTimeout(this.animate, timeout);
  }

  animate() {
    this.setState({
      dialPosition: dialPositions[this.props.riskLevel - 1],
      riskLevel: this.props.riskLevel,
    });
  }

  render() {
    const {
      content,
      investmentGoal,
      riskLevel,
      growthRates,
      unitPriceSpread,
    } = this.props;
    const rawContent = content.adviceSummary.page1;

    const goalTypes = content.investmentGoals.types;
    const matchingGoalDefinition = R.find(
      R.propEq('value', investmentGoal.goal)
    )(goalTypes);
    const goal = matchingGoalDefinition
      ? ` ${matchingGoalDefinition.title.toLowerCase()}`
      : '';
    const matchedGoal = rawContent.durationMapping.find(
      mappedDuration => investmentGoal.length === mappedDuration.label
    );

    const years = matchedGoal ? matchedGoal.length : 0;

    const pageContent = interpolate(rawContent, {
      goal,
      riskLevel,
      investmentReturn: rounder(this.props.expectedGrowth[1]),
      investmentDeadline: moment()
        .add(years, 'years')
        .format('MMMM YYYY'),
      lowReturnValue: growthRates[0],
      realisticReturnValue: growthRates[1],
      optimisticReturnValue: growthRates[2],
      unitPriceSpread,
    });

    const investmentGoalText =
      pageContent.investmentGoal.toLowerCase().includes('other') ||
      pageContent.investmentGoal.toLowerCase().includes('just invest')
        ? 'Your investment'
        : pageContent.investmentGoal;
    const investmentIcon =
      pageContent.investmentGoal.toLowerCase().includes('other') ||
      pageContent.investmentGoal.toLowerCase().includes('just invest')
        ? 'chart-line'
        : matchingGoalDefinition.image;

    return (
      <div
        data-test="advice-summary-page-page-1"
        className="advice-summary-page"
      >
        <div className="advice-summary-page__container advice-summary-page__container--narrow">
          <ContentBlock className="advice-summary-page__investment">
            <div className="advice-summary-page__investment-icon">
              <Icon name={investmentIcon} />
            </div>
            <div className="advice-summary-page__investment-description">
              <span className="advice-summary-page__investment-item">
                {investmentGoalText}
              </span>
              {investmentGoal.monthly <= 0 && (
                <section className="advice-summary-page-section">
                  <span className="advice-summary-page-section__investment-item">
                    {pageContent.investmentAmount}
                  </span>
                  <span
                    className="advice-summary-page-section__investment-item"
                    data-test="amount-to-be-invested"
                  >
                    £{investmentGoal.amount.toLocaleString()}
                  </span>
                </section>
              )}
              {investmentGoal.monthly >= 1 && (
                <section className="advice-summary-page-section-multiple">
                  <span>
                    <span className="advice-summary-page-section-multiple__investment-item">
                      {pageContent.investmentStartingTitle}
                    </span>
                    <span className="advice-summary-page-section-multiple__investment-item">
                      £{investmentGoal.amount.toLocaleString()}
                    </span>
                  </span>
                  <span>
                    <span className="advice-summary-page-section-multiple__investment-item">
                      {pageContent.investmentMonthlyTitle}
                    </span>
                    <span className="advice-summary-page-section-multiple__investment-item">
                      £{investmentGoal.monthly.toLocaleString()}
                    </span>
                  </span>
                </section>
              )}
            </div>
          </ContentBlock>

          <ContentBlock
            title={pageContent.riskLevelTitle}
            className="advice-summary-page__risk-level"
          >
            <div className="advice-summary-page__risk-level-description">
              <div data-test="risk-level-description">
                {pageContent.riskLevelDescription}
              </div>
              <div className="advice-summary-page__link-container">
                <a
                  className="advice-summary-page__link"
                  onClick={() => this.riskLevelPopup.toggle()}
                >
                  {pageContent.riskLevelPopupLink}
                </a>
              </div>
              <div className="advice-summary-page__risk-level-dial">
                {[1, 2, 3, 4, 5, 6, 7].map(i => (
                  <span
                    key={`digit-${i}`}
                    className={`advice-summary-page__risk-level-dial-digit advice-summary-page__risk-level-dial-digit--${i} ${
                      this.state.riskLevel === i
                        ? 'advice-summary-page__risk-level-dial-digit--visible'
                        : ''
                      }`}
                  >
                    {i}
                  </span>
                ))}
                <img
                  className="advice-summary-page__risk-level-dial-src"
                  src={images.riskLevelDial}
                  alt={'risk level dial'}
                />
                <img
                  className="advice-summary-page__risk-level-dial-pointer"
                  style={{ transform: `rotate(${this.state.dialPosition}deg)` }}
                  src={images.riskLevelDialPointer}
                  alt={'risk level dial pointer'}
                />
              </div>
            </div>
          </ContentBlock>
        </div>
        <div className="advice-summary-page__container advice-summary-page__container--wide">
          <ContentBlock
            centerContent
            title={pageContent.growthTitle}
            style={{ justifyContent: 'center' }}
          >
            <div className="advice-summary-page__growth">
              <h3
                className="advice-summary-page__growth-subtitle"
                data-test="investment-return-subtitle"
              >
                {pageContent.growthSubtitle}
              </h3>
              <div className="advice-summary-page__growth-container">
                {[0, 1, 2].map(i => (
                  <div
                    className="advice-summary-page__growth-item"
                    key={`growth-item-${i}`}
                  >
                    <img
                      className="advice-summary-page__growth-image"
                      src={images[`return${i}`]}
                      alt={`investment return ${i}`}
                    />
                    <div
                      className={`advice-summary-page__growth-item-description ${
                        i === 1
                          ? 'advice-summary-page__growth-item-description--middle'
                          : ''
                        }`}
                    >
                      <span className="advice-summary-page__growth-level">
                        {pageContent.growthLevels[i]}
                      </span>
                      <span
                        className="advice-summary-page__growth-return"
                        data-test={`investment-return-${i}`}
                      >
                        £{rounder(
                        this.props.expectedGrowth[i]
                      ).toLocaleString()}
                      </span>
                    </div>
                  </div>
                ))}
              </div>
              <div className="advice-summary-page__growth-disclaimer">
                {pageContent.growthDisclaimer}
              </div>
              <div
                className="advice-summary-page__growth-popup-link"
                onClick={() => this.investmentGrowthPopup.toggle()}
              >
                {pageContent.growthPopupLink}
              </div>
            </div>
          </ContentBlock>
        </div>

        <Popup
          isError
          ref={c => {
            this.riskLevelPopup = c;
          }}
          closeButtonTop
          closeClickOutside
          title={pageContent.riskLevelPopup.title}
          buttons={[
            {
              label: 'Close',
              onClick: () => this.riskLevelPopup.toggle(),
            },
          ]}
        >
          <div className="advice-summary-page__risk-level-popup-body">
            <div className="advice-summary-page__risk-level-popup-icon">
              <img src={images.puzzleFull} alt={'completed puzzle'} />
            </div>
            <div className="advice-summary-page__risk-level-popup-content">
              {pageContent.riskLevelPopup.content}
            </div>
          </div>
        </Popup>

        <Popup
          ref={c => {
            this.investmentGrowthPopup = c;
          }}
          closeButtonTop
          closeClickOutside
          title={pageContent.growthPopup.title}
          buttons={[
            {
              label: 'Close',
              onClick: () => this.investmentGrowthPopup.toggle(),
            },
          ]}
        >
          <div className="advice-summary-page__growth-popup-content">
            {pageContent.growthPopup.content}
          </div>
        </Popup>
      </div>
    );
  }
}
