import { connect } from 'react-redux';
import ServicesDownComponent from './ServicesDownComponent';
import { compose } from 'recompose';

const mapStateToProps = () => ({});

const mapDispatchToProps = () => ({});

const ServicesDownContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default ServicesDownContainer(ServicesDownComponent);
