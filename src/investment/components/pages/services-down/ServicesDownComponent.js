import React from 'react';
import Button from '../../../../../modules/elements/Button';

export default () => (
  <div className="services-down">
    <p className="services-down__title">
      We are currently experiencing some technical issues, please try again
      later
    </p>
    <Button
      className={'btn--primary'}
      label="Retry"
      onClick={() => window.location.assign('/investment')}
      data-test={'services-down-retry'}
    />
  </div>
);
