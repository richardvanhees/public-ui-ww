import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import { lookContentByKey } from '../../../../../modules/content/selectors';
import { handleFieldChange } from '../../../modules/atr/actions';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import { transformStateForQuestionRange } from '../../../modules/atr/selectors';

import AtrQuestionComponent from './AtrQuestionComponent';

const contentKey = 'investment';

const mapStateToProps = ({ content, atr }) => ({
  content: lookContentByKey(contentKey, content),
  atr,
  userInput: transformStateForQuestionRange(
    lookContentByKey(contentKey, content),
    'atrQuestions',
    atr
  ),
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    handleFieldChange: (name, value) => handleFieldChange(name, value),
    onSetNextPage: () => saveFactFindRequest({ doInBackground: true }),
  },
  dispatch
);

export default () => {
  const NewAtrQuestionComponent = compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  );

  return NewAtrQuestionComponent(AtrQuestionComponent);
};
