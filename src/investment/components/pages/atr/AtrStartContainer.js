import { connect } from 'react-redux';
import AtrStartComponent from './AtrStartComponent';
import { push, goBack } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch => ({
  next: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/atr/question/1`)),
  back: () => dispatch(goBack()),
});

const NewAtrStartComponent = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default NewAtrStartComponent(AtrStartComponent);
