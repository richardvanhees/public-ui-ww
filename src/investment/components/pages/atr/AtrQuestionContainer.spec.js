import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import AtrQuestionContainer, { __RewireAPI__ } from './AtrQuestionContainer';
import { Provider } from 'react-redux';

test('AtrQuestionContainer', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const fakeStore = {
    getState: () => ({
      content: {
        data: {
          investment: {
            atrQuestions: [],
          },
        },
      },
      investmentGoal: {},
      aboutYou: {},
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  class FakeAtrQuestionComponent extends React.PureComponent {
    render() {
      return <div />;
    }
  }

  const saveFactFindRequestStub = sandbox.stub();

  __RewireAPI__.__Rewire__('AtrQuestionComponent', FakeAtrQuestionComponent);
  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestStub);

  const Target = AtrQuestionContainer();

  const target = mount(
    <Provider store={fakeStore}>
      <Target />
    </Provider>
  );

  const componentProps = target.find(FakeAtrQuestionComponent).props();

  componentProps.onSetNextPage();

  assert.deepEqual(
    saveFactFindRequestStub.args,
    [[{ doInBackground: true }]],
    'invoking customSetPageAction causes save in background action to occur'
  );

  __RewireAPI__.__ResetDependency__('AtrQuestionComponent');
});
