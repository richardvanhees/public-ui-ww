import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import { puzzleTopLeft } from '../../../../../assets/images/iw-images';

const AtrStartComponent = ({ content, next, back }) => (
  <div className="wrapper atr-start">
    <PageHeading
      title={content.atrStart.title}
      subTitle={content.atrStart.subTitle}
      image={puzzleTopLeft}
      smallSubTitle
    />

    <div className="center-desktop">
      <Instructions
        title={content.atrStart.instructionsTitle}
        items={content.atrStart.instructions}
        list
      />

      <PageNavigation
        next={next}
        back={back}
        testNext={'atr-start'}
        testBack={'atr-start'}
        noSideMargins
      />
    </div>
  </div>
);

AtrStartComponent.propTypes = {
  content: PropTypes.object.isRequired,
  next: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
};

export default AtrStartComponent;
