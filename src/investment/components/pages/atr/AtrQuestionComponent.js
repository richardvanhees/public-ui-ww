import React from 'react';
import PropTypes from 'prop-types';

import WealthWizards from '../../../../../modules/wealthwizards';
import { puzzleTopLeft } from '../../../../../assets/images/iw-images';

import Pager from '../../../../../modules/elements/Pager';
import Question from '../../../modules/elements/Question';

const AtrQuestionComponent = props => {
  const pagerOptions = {
    pages: props.content.atrQuestions.map(question => ({
        pageContent: Question,
        extraPropsToInject: {
          onChange: (key, answer) => props.handleFieldChange(key, answer),
          userInput: props.userInput,
          ...question,
        },
        titleImage: puzzleTopLeft,
        style: { width: 'max-content', padding: 0 },
      })
    ),
    previousPage: `${WealthWizards.CONTEXT_ROUTE}/atr/start`,
    nextPage: `${WealthWizards.CONTEXT_ROUTE}/atr/playback`,
    onSetNextPage: props.onSetNextPage,
  };

  return (
    <div className="wrapper atr-questions">
      <div className="questions-component__question-container">
        <Pager {...pagerOptions} {...props} />
      </div>
    </div>
  );
};

AtrQuestionComponent.propTypes = {
  content: PropTypes.object.isRequired,
  handleFieldChange: PropTypes.func.isRequired,
  userInput: PropTypes.object,
  onSetNextPage: PropTypes.func.isRequired,
};

export default AtrQuestionComponent;
