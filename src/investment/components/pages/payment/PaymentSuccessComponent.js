import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { checkMark } from '../../../../../assets/images/iw-images';

export default class PaymentSuccessComponent extends PureComponent {
  static propTypes = {
    content: PropTypes.object.isRequired,
    setSolutionPaid: PropTypes.func.isRequired,
  };

  componentDidMount = () => this.props.setSolutionPaid();

  render = () => {
    const { title, subtitle, nextTitle, nextDescription } = this.props.content.paymentSuccess;

    return (<div className="payment-success-container">
      <div className="payment-success-container__owl">
        <img
          className="payment-success-container__owl-img"
          src={checkMark}
          alt="success"
        />
      </div>

      <div className="payment-success-container__first payment-success-container__text-container">
        <h2
          className="payment-success-container__first-title"
          data-test="advice-accepted-heading"
        >
          {title}
        </h2>
        <p className="payment-success-container__first-text">
          {subtitle}
        </p>
      </div>
      <div className="payment-success-container__second payment-success-container__text-container">
        <h4 className="payment-success-container__second-title">
          {nextTitle}
        </h4>
        <p className="payment-success-container__second-text">
          {nextDescription}
        </p>
      </div>
    </div>);
  }

}
