import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import isMobile from '../../../../../modules/utils/is-mobile';

import PaymentSuccessComponent from './PaymentSuccessComponent';
import { lookContentByKey } from '../../../../../modules/content';
import { setSolutionPaid } from '../../../modules/solution/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, browser }) => ({
  content: lookContentByKey(contentKey, content),
  isMobile: isMobile(browser),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setSolutionPaid,
}, dispatch);

const PaymentContainer = compose(connect(mapStateToProps, mapDispatchToProps));

export default PaymentContainer(PaymentSuccessComponent);
