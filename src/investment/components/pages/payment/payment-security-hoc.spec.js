import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import securityHoc, { __RewireAPI__ } from './payment-security-hoc';
import { Provider } from 'react-redux';

test('payment-security-hoc', assert => {
  assert.plan(1);

  const fakeRequiredProps = {
    solution: {
      data: {
        paymentStatus: 'PAID',
      },
    },
  };

  const fakeStore = {
    getState: () => ({
      ...fakeRequiredProps,
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  const sandbox = sinon.sandbox.create();

  const pushFuncStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushFuncStub);

  const FakeComponent = () => <div />;

  const FakeComponentInstance = securityHoc(FakeComponent);

  const wrapper = mount(
    <Provider store={fakeStore}>
      <FakeComponentInstance {...fakeRequiredProps} />
    </Provider>
  );

  assert.deepEqual(pushFuncStub.callCount, 1, 'redirection done');

  __RewireAPI__.__ResetDependency__('push');
});

test('payment-security-hoc', assert => {
  assert.plan(1);

  const fakeRequiredProps = {
    solution: {
      data: {
        paymentStatus: 'CLAIMED',
      },
    },
  };

  const fakeStore = {
    getState: () => ({
      ...fakeRequiredProps,
    }),
    subscribe: () => null,
    dispatch: () => null,
  };

  const sandbox = sinon.sandbox.create();

  const pushFuncStub = sandbox.stub();

  __RewireAPI__.__Rewire__('push', pushFuncStub);

  const FakeComponent = () => <div />;

  const FakeComponentInstance = securityHoc(FakeComponent);

  const wrapper = mount(
    <Provider store={fakeStore}>
      <FakeComponentInstance {...fakeRequiredProps} />
    </Provider>
  );

  assert.deepEqual(
    pushFuncStub.callCount,
    0,
    'redirection not done because they havent paid yet'
  );

  __RewireAPI__.__ResetDependency__('push');
});
