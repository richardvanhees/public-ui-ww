import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import PaymentComponent from './PaymentComponent';

const sandbox = sinon.sandbox.create();
const investmentFulfillmentRequestStub = sandbox.stub();
const downloadRequestStub = sandbox.stub();
const redirectStub = sandbox.stub();
const showNotificationStub = sandbox.stub();
const goToDashboardStub = sandbox.stub();

const fakeRequiredProps = {
  content: {
    payment: {
      firstTitle: 'FIRST',
      firstText: 'first text',
      secondTitle: 'SECOND',
      secondText: 'second text',
      downloadButton: 'download',
      payButton: 'pay',
      popupTitle: 'popupTitle',
      popupContent: ['par 1', 'par 2'],
    },
  },
  investmentFulfillmentRequest: investmentFulfillmentRequestStub,
  solutionDownloaded: false,
  downloadRequest: downloadRequestStub,
  isMobile: false,
  isVulnerable: false,
  solutionMetadatum: {
    accepted_advice: true,
    read_documents: true,
  },
  goToDashboard: goToDashboardStub,
  isIpad: false,
};

test('PaymentComponent request', assert => {
  assert.plan(1);

  const wrapper = mount(
    <PaymentComponent
      {...{
        ...fakeRequiredProps,
        solutionMetadatum: {
          accepted_advice: false,
          read_documents: true,
        },
      }}
      solutionDownloaded
    />
  );

  assert.equals(
    goToDashboardStub.called,
    true,
    'redirected to dashboard when terms not agreed to'
  );
  sandbox.reset();
  assert.end();
});

test('PaymentComponent request', assert => {
  assert.plan(1);

  const wrapper = mount(
    <PaymentComponent
      {...{
        ...fakeRequiredProps,
        solutionMetadatum: {
          accepted_advice: true,
          read_documents: false,
        },
      }}
      solutionDownloaded
    />
  );

  assert.equals(
    goToDashboardStub.called,
    true,
    'redirected to dashboard when read docs is false'
  );
  sandbox.reset();
  assert.end();
});

test('PaymentComponent request', assert => {
  assert.plan(1);

  const wrapper = mount(
    <PaymentComponent {...fakeRequiredProps} solutionDownloaded />
  );

  wrapper.find('button[data-test="payment-pay-button"]').simulate('click');

  assert.equals(
    investmentFulfillmentRequestStub.called,
    true,
    'correct prop is called'
  );
  sandbox.reset();
  assert.end();
});

test('PaymentComponent download', assert => {
  assert.plan(1);

  const wrapper = mount(<PaymentComponent {...fakeRequiredProps} />);

  wrapper.find('button[data-test="payment-download-button"]').simulate('click');

  assert.equals(downloadRequestStub.called, true, 'downloadRequest is called');
  sandbox.reset();
  assert.end();
});

test('PaymentComponent not yet downloaded', assert => {
  assert.plan(1);

  const wrapper = mount(<PaymentComponent {...fakeRequiredProps} />);
  const result = wrapper.find('button.payment-container__buttons--pay-disabled')
    .length;

  assert.equals(result, 1, 'button is disabled');
  sandbox.reset();
  assert.end();
});

test('PaymentComponent - is vulnerable', assert => {
  assert.plan(2);

  const wrapper = mount(
    <PaymentComponent {...fakeRequiredProps} solutionDownloaded isVulnerable />
  );

  wrapper.find('button[data-test="payment-pay-button"]').simulate('click');
  assert.equals(
    investmentFulfillmentRequestStub.called,
    false,
    'popup is opened'
  );

  wrapper.find('button[data-test="continue"]').simulate('click');
  assert.equals(
    investmentFulfillmentRequestStub.called,
    true,
    'payment link is opened'
  );

  sandbox.reset();
  assert.end();
});
