import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import { checkMark } from '../../../../../assets/images/iw-images';
import Popup from '../../../../../modules/elements/Popup';

export default class PaymentComponent extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    investmentFulfillmentRequest: PropTypes.func.isRequired,
    downloadRequest: PropTypes.func.isRequired,
    isMobile: PropTypes.bool.isRequired,
    solutionDownloaded: PropTypes.bool.isRequired,
    isVulnerable: PropTypes.bool.isRequired,
    goToDashboard: PropTypes.func.isRequired,
    downloadReportNotification: PropTypes.func.isRequired,
    solutionMetadatum: PropTypes.object.isRequired,
    isIpad: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    if (
      !this.props.solutionMetadatum.accepted_advice ||
      !this.props.solutionMetadatum.read_documents
    ) {
      this.props.goToDashboard();
    }
  }

  proceed = () => {
    if (!this.props.solutionDownloaded) {
      return this.props.downloadReportNotification();
    } else if (this.props.isVulnerable) {
      this.popup.toggle();
    } else {
      this.props.investmentFulfillmentRequest();
    }
    return true;
  };

  render = () => {
    const {
      content,
      isMobile,
      isIpad,
      downloadRequest,
      solutionDownloaded,
      investmentFulfillmentRequest,
    } = this.props;
    const sectionContent = content.payment;

    return (
      <div className="payment-container">
        <div className="payment-container__owl">
          <img
            className="payment-container__owl-img"
            src={checkMark}
            alt="checkmark"
          />
        </div>
        <div className="payment-container__first payment-container__text-container">
          <h2
            className="payment-container__first-title"
            data-test="advice-accepted-heading"
          >
            {sectionContent.firstTitle}
          </h2>
          <p className="payment-container__first-text">
            {sectionContent.firstText}
          </p>
        </div>
        <div className="payment-container__second payment-container__text-container">
          <h4 className="payment-container__second-title">
            {sectionContent.secondTitle}
          </h4>
          <p className="payment-container__second-text">
            {sectionContent.secondText}
          </p>
        </div>
        <div className="payment-container__buttons">
          <Button
            dataTest="payment-download-button"
            className="payment-container__buttons--download"
            label={sectionContent.downloadButton}
            onClick={() => {
              const viewType = isMobile || isIpad ? { viewType: 'view' } : {};
              return downloadRequest(viewType);
            }}
          />
          <Button
            dataTest="payment-pay-button"
            className={`${
              solutionDownloaded
                ? 'payment-container__buttons--pay'
                : 'payment-container__buttons--pay-disabled'
            }`}
            label={sectionContent.payButton}
            onClick={this.proceed}
          />

          <Popup
            ref={c => {
              this.popup = c;
            }}
            closeClickOutside
            closeButtonTop
            isError
            title={sectionContent.popupTitle}
            buttons={[
              {
                label: 'Time to think',
                dataTest: 'time-to-think',
                onClick: () => {
                  this.popup.toggle();
                  this.props.goToDashboard();
                },
              },
              {
                label: 'Continue',
                dataTest: 'continue',
                type: 'primary',
                onClick: () => {
                  this.popup.toggle();
                  investmentFulfillmentRequest();
                },
              },
            ]}
          >
            {sectionContent.popupContent.map((paragraph, i) => (
              <p key={`paragraph-${i}`}>{paragraph}</p>
            ))}
          </Popup>
        </div>
      </div>
    );
  };
}
