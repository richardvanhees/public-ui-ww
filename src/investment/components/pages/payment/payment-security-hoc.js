import { connect } from 'react-redux';
import { compose } from 'recompose';
import withLifecycle from '@hocs/with-lifecycle';
import WealthWizards from '../../../../../modules/wealthwizards';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import R from 'ramda';

const mapStateToProps = ({ solution }) => ({
  solutionPaymentStatus: R.path(['data', 'paymentStatus'])(solution),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      pushFunc: push,
    },
    dispatch
  );

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withLifecycle({
    onDidMount({ pushFunc, solutionPaymentStatus }) {
      solutionPaymentStatus === 'PAID' &&
        pushFunc(`${WealthWizards.CONTEXT_ROUTE}/dashboard`);
    },
  })
);
