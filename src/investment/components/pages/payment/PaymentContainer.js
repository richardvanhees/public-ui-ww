import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import isMobile from '../../../../../modules/utils/is-mobile';
import PaymentComponent from './PaymentComponent';
import { lookContentByKey } from '../../../../../modules/content';
import {
  downloadReportRequest,
  paymentUidRequest,
} from '../../../modules/solution/actions';
import paymentSecurityHoc from './payment-security-hoc';
import {
  downloaded,
  individualSolution,
} from '../../../modules/solution-metadata/selector';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import rightTimeSelector from '../../../modules/right-time/selector';
import { showDownloadNotification } from '../../../../../modules/browser/actions';

const contentKey = 'investment';

const mapStateToProps = state => ({
  content: lookContentByKey(contentKey, state.content),
  isMobile: isMobile(state.browser),
  solutionDownloaded: downloaded(
    state.solutionMetadata,
    R.path(['solution', 'solution_id'])(state)
  ),
  isVulnerable: rightTimeSelector(state).isVulnerable,
  solutionMetadatum:
    individualSolution(
      state.solutionMetadata,
      R.path(['solution', 'solution_id'])(state)
    ) || {},
  isIpad: !!navigator.userAgent.match(/iPad/i),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      investmentFulfillmentRequest: paymentUidRequest,
      downloadRequest: data => downloadReportRequest(data),
      goToDashboard: () => push(`${WealthWizards.CONTEXT_ROUTE}/dashboard`),
      downloadReportNotification: showDownloadNotification,
    },
    dispatch
  );

const PaymentContainer = compose(connect(mapStateToProps, mapDispatchToProps));

export default paymentSecurityHoc(PaymentContainer(PaymentComponent));
