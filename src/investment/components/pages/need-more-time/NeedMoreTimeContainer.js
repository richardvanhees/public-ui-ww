import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import NeedMoreTimeComponent from './NeedMoreTimeComponent';
import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content }) => ({
  content: lookContentByKey(contentKey, content),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      next: () => saveFactFindRequest({ redirectPath: '/goal', doInBackground: true }),
      goToLanding: () => push(`${WealthWizards.CONTEXT_ROUTE}/`),
    },
    dispatch
  );

export default () => {
  const NeedMoreTimeContainer = compose(
    connect(mapStateToProps, mapDispatchToProps)
  );

  return NeedMoreTimeContainer(NeedMoreTimeComponent);
};
