import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Button from '../../../../../modules/elements/Button';
import PageHeading from '../../../../../modules/elements/PageHeading';

export default class NeedMoreTimeComponent extends PureComponent {
  static propTypes = {
    next: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    isNarrow: PropTypes.bool,
    goTolanding: PropTypes.func.isRequired,
  };

  render = () => {
    const { content } = this.props;
    const sectionContent = content.needMoreTime;

    return (
      <div className="need-more-time">
        <PageHeading title={sectionContent.title} />

        {sectionContent.paragraphs.map((paragraph, i) => (
          <p key={`paragraph-${i}`} className="need-more-time__paragraph">
            {paragraph}
          </p>
        ))}

        <div className="need-more-time__button-container">
          <Button
            dataTest="need-more-time-not-now"
            onClick={this.props.goToLanding}
            label={sectionContent.notNow}
            className="need-more-time__btn need-more-time__btn--restrict-line-height"
          />
          <Button
            dataTest="need-more-time-next"
            onClick={this.props.next}
            label={sectionContent.continue}
            type={'primary'}
            className="need-more-time__btn"
          />
        </div>
      </div>
    );
  };
}
