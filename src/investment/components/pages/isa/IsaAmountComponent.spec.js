import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import IsaAmountComponent from './IsaAmountComponent';
import { Provider } from 'react-redux';

const sandbox = sinon.sandbox.create();
const nextStub = sandbox.stub();
const backStub = sandbox.stub();
const setIsaAmountStub = sandbox.stub();
const referStub = sandbox.stub();
const excessStub = sandbox.stub();
const amendInvestmentStub = sandbox.stub();

import timekeeper from 'timekeeper';
import moment from 'moment';

const setup = (override = {}) => {
  let fakeProps = {
    content: {
      isaAmount: {
        singleTitle: 'SINGLETITLE',
        pluralTitle: 'PLURALTITLE',
        subTitle: 'SUBTITLE',
        contributionConfirmationPopupText: 'POPUPTEXT',
        changeAmountPopupText: 'POPUPTEXT1',
        isaTypes: 'ISATYPES',
        isaSingleType: 'ISASINGLETYPE',
        maxError: 'MAXERROR',
        minError: 'MINERROR',
      }
    },
    investmentGoal: {
      goal: "my fake goal",
      name: "fake name"
    },
    next: nextStub,
    back: backStub,
    excessContributionAmount: excessStub,
    isas: [
      {
        type: 'cash',
        amount: 0,
        in_use: 'yes',
      },
      {
        type: 'lifetime',
        amount: 0,
        in_use: 'no',
      },
      {
        type: 'innovation',
        amount: 0,
        in_use: 'no',
      },
    ],
    setIsaAmount: setIsaAmountStub,
    refer: referStub,
    amendInvestment: amendInvestmentStub,
    investmentGoalAmount: 0,
  };

  fakeProps = {
    ...fakeProps,
    ...override
  };

  return mount(
      <IsaAmountComponent {...fakeProps} />
  );
}

test('IsaAmountComponent initial state is correct from props passed', assert => {
  assert.plan(1);

  const context = setup();

  assert.deepEquals(
    context.state(),
    { showMaxError: false, showMinError: false },
    'check initial state'
  );
});

test('IsaAmountComponent one isa is listed', assert => {
  assert.plan(3);

  const context = setup();

  assert.equal(context.find('[data-test="currency-input-cash"]').length, 1);
  assert.equal(context.find('[data-test="currency-input-lifetime"]').length, 0);
  assert.equal(context.find('[data-test="currency-input-innovation"]').length, 0);
});

test('IsaAmountComponent contributed amount is greater remaining allowance (lump)', assert => {
  assert.plan(1);

  const context = setup({
    investmentGoalAmount: 8000,
    isas: [
      {
        type: 'cash',
        amount: 13000,
        in_use: 'yes',
      }
    ]
  });

  context.find('[data-test="next-btn-isa-capture-next"]').simulate('click');
  assert.equal(excessStub.called, true, 'it sets excessContributionAmount');
});

test('IsaAmountComponent contributed amount is greater remaining allowance (lump + monthly)', assert => {
  assert.plan(1);

  timekeeper.freeze(moment(`${moment().utc().year()}-06-05`, 'YYYY-MM-DD').utc().format());

  const context = setup({
    investmentGoalAmount: 6000,
    investmentGoalMonthly: 200,
    isas: [
      {
        type: 'cash',
        amount: 13000,
        in_use: 'yes',
      }
    ]
  });
  context.find('[data-test="next-btn-isa-capture-next"]').simulate('click');
  assert.equal(excessStub.called, true, 'it sets excessContributionAmount');

  timekeeper.reset();
});
