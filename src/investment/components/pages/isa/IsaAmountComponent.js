import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import { piggyBank } from '../../../../../assets/images/iw-images';
import PageHeading from '../../../../../modules/elements/PageHeading';
import interpolate from '../../../../../modules/utils/interpolate';
import Popup from '../../../../../modules/elements/Popup';
import ErrorComponent from '../../../../../modules/elements/Error';
import CurrencyInput from '../../../../../modules/elements/CurrencyInput';
import R from 'ramda';
import {
  calculateChangeAmount,
  calculateContributionSum,
} from '../../../modules/isa/isa-calculations';
import WealthWizards from '../../../../../modules/wealthwizards';

class IsaCaptureComponent extends Component {
  static propTypes = {
    next: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    isas: PropTypes.array.isRequired,
    setIsaAmount: PropTypes.func.isRequired,
    refer: PropTypes.func.isRequired,
    amendInvestment: PropTypes.func.isRequired,
    excessContributionAmount: PropTypes.func.isRequired,
    investmentGoalAmount: PropTypes.number.isRequired,
    investmentGoalMonthly: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    this.plurals = R.filter(R.propEq('in_use', 'yes'), props.isas).length > 1;
    this.contributionSum = calculateContributionSum(props.isas);

    this.changeAmount = calculateChangeAmount(
      props.investmentGoalAmount,
      this.contributionSum,
      props.investmentGoalMonthly
    );

    this.contributionConfirmationPopup = null;
    this.changeAmountPopup = null;

    this.state = {
      showMaxError: false,
      showMinError: false,
    };
  }

  componentDidUpdate = () => {
    this.contributionSum = calculateContributionSum(this.props.isas);
    this.changeAmount = calculateChangeAmount(
      this.props.investmentGoalAmount,
      this.contributionSum,
      this.props.investmentGoalMonthly
    );
  };

  setErrorStateOnInputs = () => {
    const newState = {};

    R.forEach(isa => {
      newState[`${isa.type}Error`] = isa.amount === 0;
    }, this.props.isas);

    this.setState(newState);
  };

  checkAllfilled = () =>
    !R.find(R.allPass([R.propEq('in_use', 'yes'), R.propEq('amount', 0)]))(
      this.props.isas
    );

  calculateInterpolateValues = () => {
    let values = {
      confirmationAmount: this.contributionSum,
      changeAmount: this.changeAmount,
      when: this.props.investmentGoalMonthly ? ' ' : ' today ',
    };
    if (this.plurals) {
      values = R.mergeAll([
        values,
        {
          pluralOne: 'values',
          pluralTwo: 'ISAs',
        },
      ]);
    } else {
      const isaInUse = R.find(R.propEq('in_use', 'yes'), this.props.isas) || {};
      const propType = isaInUse.type;
      values = R.mergeAll([
        values,
        {
          pluralOne: 'value',
          pluralTwo: 'ISA',
          singleISA: R.path(
            ['content', 'isaAmount', 'isaTypes', propType, 'titleLabel'],
            this.props
          ),
        },
      ]);
    }
    return values;
  };

  toggleContributionConfirmationPopup = () => {
    this.contributionConfirmationPopup.toggle();
  };

  toggleChangeAmountPopup = () => {
    this.changeAmountPopup.toggle();
  };

  proceed = () => {
    const overLimit = this.contributionSum > 20000;
    const uptoLimit = this.contributionSum === 20000;
    const zeroLimit = !this.checkAllfilled();
    const underMinContribution =
      20000 - this.contributionSum < WealthWizards.MIN_CONTRIBUTION;
    const beyondInvestment = this.changeAmount > 0;

    this.setErrorStateOnInputs();
    this.setState({ showMaxError: !zeroLimit && overLimit });
    this.setState({ showMinError: zeroLimit });

    if (overLimit || zeroLimit) return false;

    if (uptoLimit) {
      this.contributionConfirmationPopup.toggle();
      return false;
    }

    if (underMinContribution) {
      this.contributionConfirmationPopup.toggle();
      return false;
    }

    if (beyondInvestment) {
      this.props.excessContributionAmount(this.changeAmount);
      this.changeAmountPopup.toggle();
      return false;
    }

    return this.props.next();
  };

  render() {
    const {
      singleTitle,
      pluralTitle,
      subTitle,
      contributionConfirmationPopupText,
      changeAmountPopupText,
      isaTypes,
      isaSingleType,
      maxError,
      minError,
    } = interpolate(
      this.props.content.isaAmount,
      this.calculateInterpolateValues()
    );
    const { back, setIsaAmount, isas } = this.props;
    const { showMaxError, showMinError } = this.state;

    return (
      <div className="wrapper">
        <div className="center-desktop lower-vertical-spacing isa-amount__wrapper">
          <div className="isa-amount__title-container">
            <PageHeading
              title={this.plurals ? pluralTitle : singleTitle}
              titleClassName="isa-amount__title"
              subTitle={`(${subTitle})`}
              smallTitle
              smallSubTitle
              image={piggyBank}
              imageClassName="isa-amount__image"
              subTitleClassName="isa-amount__sub-title"
            />
          </div>
          <div className="isa-amount__input-container">
            {isas.map(
              isa =>
                isa.in_use === 'yes' && (
                  <CurrencyInput
                    className="isa-amount__currency-input"
                    labelClassName="isa-amount__currency-label"
                    label={
                      this.plurals
                        ? isaTypes[isa.type].inputLabel
                        : isaSingleType
                    }
                    dataTest={`currency-input-${isa.type}`}
                    key={isa.type}
                    value={isa.amount}
                    error={showMaxError || this.state[`${isa.type}Error`]}
                    onChange={v => {
                      setIsaAmount(isa.type, v);
                    }}
                  />
                )
            )}
          </div>

          <ErrorComponent
            title={maxError}
            compact
            active={showMaxError}
            className="error-message--isa-amount-max"
            type="lock-to-page-navigation-component"
          />

          <ErrorComponent
            title={minError}
            compact
            active={showMinError}
            className="error-message--isa-amount-min"
            type="lock-to-page-navigation-component"
          />

          <PageNavigation
            back={back}
            next={this.proceed}
            noTopMargin={showMaxError || showMinError}
            testBack={'isa-capture-back'}
            testNext={'isa-capture-next'}
          />

          <Popup
            ref={c => {
              this.contributionConfirmationPopup = c;
            }}
            closeButtonTop
            closeClickOutside
            isError
            title="Your ISA Investments"
            className="isa-amount__popup1"
            buttons={[
              {
                label: 'Yes',
                className: 'isa-amount__popup-button',
                onClick: () => {
                  this.props.refer(true);
                },
              },
              {
                label: 'No',
                onClick: this.toggleContributionConfirmationPopup,
                className: 'btn--primary',
              },
            ]}
          >
            {contributionConfirmationPopupText}
          </Popup>

          <Popup
            ref={c => {
              this.changeAmountPopup = c;
            }}
            closeButtonTop
            closeClickOutside
            isError
            layout="column"
            title="Your investment amount"
            className="isa-amount__popup2"
            dataTest="isa-amount-change-amount-popup"
            buttons={[
              {
                label: 'Back',
                className: 'isa-amount__popup-button',
                dataTest: 'change-amount-back',
                onClick: this.toggleChangeAmountPopup,
              },
              {
                label: 'Change amount',
                className: 'btn--primary',
                dataTest: 'change-amount-confirm',
                onClick: this.props.amendInvestment,
              },
            ]}
          >
            {changeAmountPopupText}
          </Popup>
        </div>
      </div>
    );
  }
}

export default IsaCaptureComponent;
