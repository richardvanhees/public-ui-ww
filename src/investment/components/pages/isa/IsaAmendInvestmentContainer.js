import { connect } from 'react-redux';
import InvestmentAmountComponent from '../investment/InvestmentAmountComponent';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { compose } from 'recompose';
import { lookContentByKey } from '../../../../../modules/content';
import {
  setInvestmentGoalAmount,
  setInvestmentMonthlyAmount,
} from '../../../modules/investment-goal/actions';
import {
  removeKey,
} from '../../../modules/final-checks/actions';
import { bindActionCreators } from 'redux';
import { setExcessContributionAmount } from '../../../modules/isa/actions';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, investmentGoal, isa, match }) => ({
  content: lookContentByKey(contentKey, content),
  investmentGoal,
  isas: isa.isas,
  monthlyContributions: WealthWizards.MONTHLY_CONTRIBUTIONS === 'true',
  excessContributionAmount: isa.excessContributionAmount,
  match,
  minContribution: WealthWizards.MIN_CONTRIBUTION,
  minMonthlyContribution: WealthWizards.MIN_MONTHLY_CONTRIBUTION,
  doRightTimeCheck: false,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      next: () =>
        saveFactFindRequest({
          redirectPath: '/final-checks/1',
          doInBackground: true,
        }),
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/isa/amount`),
      setExcessContributionAmount: value => setExcessContributionAmount(value),
      setAmount: setInvestmentGoalAmount,
      setMonthly: setInvestmentMonthlyAmount,
      resetMoneyToInvest: () => removeKey(
        'investment__money_to_invest_readily_available'
      ),
    },
    dispatch
  );

const IsaAmendInvestmentContainer = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default IsaAmendInvestmentContainer(InvestmentAmountComponent);
