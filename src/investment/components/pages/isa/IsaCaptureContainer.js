import { connect } from 'react-redux';
import IsaCaptureComponent from './IsaCaptureComponent';
import { goBack } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import { lookContentByKey } from '../../../../../modules/content/selectors';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import isMobile from '../../../../../modules/utils/is-mobile';

import * as isaActions from '../../../modules/isa/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, isa, browser, factFind }) => ({
  content: lookContentByKey(contentKey, content),
  isa,
  isMobile: isMobile(browser),
  isLoggedIn: factFind.isLoggedIn,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: goBack,
      next: () =>
        saveFactFindRequest({
          redirectPath: '/isa/amount',
          doInBackground: true,
        }),
      skip: () =>
        saveFactFindRequest({
          redirectPath: '/final-checks/1',
          doInBackground: true,
        }),
      setIsaInUse: (key, value) => isaActions.setIsaInUse(key, value),
      setContributedToIsas: value => isaActions.setContributedToIsas(value),
      setExistingStocksAndShares: value =>
        isaActions.setExistingStocksAndShares(value),
      resetIsas: () => isaActions.resetIsas(),
      refer: () =>
        saveFactFindRequest({
          redirectPath: '/referral',
          recordSolution: true,
        }),
    },
    dispatch
  );

const IsaCaptureContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default IsaCaptureContainer(IsaCaptureComponent);
