import { connect } from 'react-redux';
import IsaAmountComponent from './IsaAmountComponent';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';

import * as isaActions from '../../../modules/isa/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, isa, investmentGoal }) => ({
  content: lookContentByKey(contentKey, content),
  isas: isa.isas,
  investmentGoalAmount: investmentGoal.amount,
  investmentGoalMonthly: investmentGoal.monthly,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/isa/capture`),
      next: () =>
        saveFactFindRequest({
          redirectPath: '/final-checks/1',
          doInBackground: true,
        }),
      setIsaAmount: (key, value) => isaActions.setIsaAmount(key, value),
      excessContributionAmount: value =>
        isaActions.setExcessContributionAmount(value),
      refer: () =>
        saveFactFindRequest({
          redirectPath: '/referral',
          recordSolution: true,
        }),
      amendInvestment: () =>
        push(`${WealthWizards.CONTEXT_ROUTE}/isa/amend-investment`),
    },
    dispatch
  );

const IsaCaptureContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default IsaCaptureContainer(IsaAmountComponent);
