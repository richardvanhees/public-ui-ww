import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageNavigation from '../../../../../modules/elements/PageNavigation';
import AnswerGroup from '../../../../../modules/elements/AnswerGroup';
import { piggyBank } from '../../../../../assets/images/iw-images';
import Checkbox from '../../../../../modules/elements/Checkbox';
import PageHeading from '../../../../../modules/elements/PageHeading';
import interpolate from '../../../../../modules/utils/interpolate';
import Popup from '../../../../../modules/elements/Popup';
import R from 'ramda';
import Tooltip from '../../../../../modules/elements/Tooltip';
import Icon from '../../../../../modules/elements/Icon';
import ErrorComponent from '../../../../../modules/elements/Error';

class IsaCaptureComponent extends Component {
  static propTypes = {
    isa: PropTypes.object.isRequired,
    next: PropTypes.func.isRequired,
    skip: PropTypes.func.isRequired,
    back: PropTypes.func.isRequired,
    content: PropTypes.object.isRequired,
    setIsaInUse: PropTypes.func.isRequired,
    setExistingStocksAndShares: PropTypes.func.isRequired,
    setContributedToIsas: PropTypes.func.isRequired,
    resetIsas: PropTypes.func.isRequired,
    refer: PropTypes.func.isRequired,
    isMobile: PropTypes.bool.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.errorPopup = null;
    this.helpTextPopup = null;

    this.state = {
      showNoIsaSelectedError: false,
    };
  }

  setContributedToIsas = value => {
    this.props.setContributedToIsas(value);
    if (value === 'no') {
      this.props.skip();
    }
  };

  toggleTooltip = () => {
    if (!this.props.isMobile) return;
    this.helpTextPopup.toggle();
  };

  inUseCheck = () =>
    R.findIndex(R.propEq('in_use', 'yes'), this.props.isa.isas);

  proceed = () => {
    if (this.props.isa.hasExisitingStocksAndSharesIsa === 'yes') {
      this.toggleError();
    } else {
      if (this.props.isa.contributedToIsas === 'yes') {
        this.inUseCheck() === -1
          ? this.setState({ showNoIsaSelectedError: true })
          : this.setState({ showNoIsaSelectedError: false }, this.props.next);
      } else {
        this.props.skip();
      }
    }
  };

  toggleError = () => {
    this.errorPopup.toggle();
  };

  render() {
    const {
      title,
      subTitle,
      isaAnswers,
      isaTypesTitle,
      isaTypes,
      popupText,
      noIsaSelectedError,
    } = interpolate(this.props.content.isaCapture);
    const { tooltip } = this.props.content.isaCapture;
    const { showNoIsaSelectedError } = this.state;
    const {
      setIsaInUse,
      back,
      setExistingStocksAndShares,
      isa,
      isMobile,
    } = this.props;

    return (
      <div className="wrapper">
        <div className="center-desktop lower-vertical-spacing isa-capture__wrapper">
          <div className="isa-capture__title-container">
            <PageHeading
              titleClassName="isa-capture__title"
              title={title}
              subTitle={`(${subTitle})`}
              smallTitle
              smallSubTitle
              image={piggyBank}
              imageClassName="isa-capture__image"
              subTitleClassName="isa-capture__sub-title"
            />
            <span
              data-test="tooltip"
              onClick={() => this.toggleTooltip()}
              className="isa-capture__tooltip-container"
            >
              <Icon
                dataTip={`${tooltip.title}: ${tooltip.text}`}
                name="question-circle"
                className="isa-capture__tooltip-icon"
              />
            </span>
          </div>

          <div className="isa-capture__lower-container">
            <AnswerGroup
              wrapperClass="answer--iw-answer"
              dataTest={'isa-capture'}
              className="isa-capture__answer-group-container"
              answerKey="isa-contributed"
              answers={isaAnswers}
              selected={isa.contributedToIsas}
              answerWidth={100}
              onChange={({ answerValue }) => {
                this.setContributedToIsas(answerValue);
              }}
            />

            {isa.contributedToIsas &&
              isa.contributedToIsas === 'yes' && (
                <div className="isa-capture__checkbox-wrapper">
                  <div
                    className="isa-capture__checkbox-title"
                    data-test={'isa-capture-isa-types-title'}
                  >
                    {isaTypesTitle}
                  </div>
                  <div className="isa-capture__checkbox-container">
                    {isa.isas.map(_isa => (
                      <Checkbox
                        dataTest={`isa-capture-${_isa.type}`}
                        className="isa-capture__checkbox"
                        label={isaTypes[_isa.type]}
                        key={_isa.type}
                        type="custom"
                        checked={_isa.in_use === 'yes'}
                        onChange={v => {
                          setIsaInUse(_isa.type, v ? 'yes' : 'no');
                        }}
                        name={_isa.type}
                      />
                    ))}
                    <Checkbox
                      dataTest={'isa-capture-stocksAndShares'}
                      label="Stocks & shares ISA"
                      className="isa-capture__checkbox"
                      type="custom"
                      checked={isa.hasExisitingStocksAndSharesIsa === 'yes'}
                      onChange={v => {
                        setExistingStocksAndShares(v ? 'yes' : 'no');
                      }}
                      name="stocksAndShares"
                    />
                  </div>
                </div>
              )}
          </div>
          <ErrorComponent
            title={noIsaSelectedError}
            compact
            active={showNoIsaSelectedError}
            className="isa-capture__error"
            type="lock-to-page-navigation-component"
          />

          <PageNavigation
            back={isLoggedIn => back(isLoggedIn)}
            next={isa.contributedToIsas && this.proceed}
            noTopMargin={showNoIsaSelectedError}
            noSideMargins
            testBack={'isa-capture-back'}
            testNext={'isa-capture-next'}
          />

          <Popup
            ref={c => {
              this.errorPopup = c;
            }}
            closeButtonTop
            closeClickOutside
            isError
            className="isa-capture__popup"
            title="Stocks and Shares ISA"
            buttons={[
              {
                label: 'Yes',
                className: 'isa-capture__popup-button',
                dataTest: 'isa-capture-s-and-s-confirm',
                onClick: () => {
                  this.props.refer(true);
                },
              },
              {
                label: 'No',
                onClick: this.toggleError,
                className: 'btn--primary',
              },
            ]}
          >
            {popupText}
          </Popup>
          {!isMobile && <Tooltip />}
          {isMobile && (
            <Popup
              ref={c => {
                this.helpTextPopup = c;
              }}
              closeButtonTop
              closeClickOutside
              title={tooltip.popupTitle}
              buttons={[
                {
                  label: 'Close',
                  onClick: this.toggleTooltip,
                },
              ]}
            >
              {tooltip.text}
            </Popup>
          )}
        </div>
      </div>
    );
  }
}

export default IsaCaptureComponent;
