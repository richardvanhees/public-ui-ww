import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { mount } from 'enzyme';
import ActivateComponent from './ActivateComponent';

const sandbox = sinon.sandbox.create();
const exitPageStub = sandbox.stub();
const resendEmailStub = sandbox.stub();
const showNotificationStub = sandbox.stub();
const clearStateStub = sandbox.stub();

test('ActivateComponent SUCCESS', assert => {
  assert.plan(4);

  const fakeRequiredProps = {
    content: {
      activate: {
        title: "fake title",
        info: "fake info",
        popup: "fake pop up text"
      },
    },
    email: "fakeemail@fake.com",
    exitPage: exitPageStub,
    resendEmail: resendEmailStub,
    showNotification: showNotificationStub,
    clearState: clearStateStub,
  };

  const wrapper = mount(<ActivateComponent {...fakeRequiredProps} />);

  wrapper.find('[data-test="resend-email"]').simulate('click');

  assert.equals(resendEmailStub.called, true, 'activation email is resent');
  assert.equals(clearStateStub.called, true, 'state is cleared');
  assert.equals(showNotificationStub.called, true, 'showNotification is called');
  assert.equals(exitPageStub.called, false, 'exitPage is NOT called');

  assert.end();
});

test('ActivateComponent FAILURE', assert => {
  assert.plan(1);

  const newFakeRequiredProps = {
    content: {
      activate: {
        title: "fake title",
        info: "fake info",
        popup: "fake pop up text"
      },
    },
    exitPage: exitPageStub,
    resendEmail: resendEmailStub,
    showNotification: showNotificationStub,
    clearState: clearStateStub,
  };

  const wrapper = mount(<ActivateComponent {...newFakeRequiredProps} />);

  wrapper.find('[data-test="resend-email"]').simulate('click');

  assert.equals(exitPageStub.called, true, 'exitPage IS called as no email is present');

  assert.end();
});
