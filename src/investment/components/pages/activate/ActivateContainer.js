import { connect } from 'react-redux';
import ActivateComponent from './ActivateComponent';
import { lookContentByKey } from '../../../../../modules/content';
import { compose } from 'recompose';
import { push } from 'react-router-redux';
import WealthWizards from '../../../../../modules/wealthwizards';
import { setNotification } from '../../../../../modules/browser';
import { resendActivationEmail } from '../../../modules/login/actions';
import { resetState } from '../../../modules/fact-find/actions';

const contentKey = 'investment';

const mapStateToProps = ({ content, aboutYou }) => ({
  content: lookContentByKey(contentKey, content),
  email: aboutYou.email,
});

const mapDispatchToProps = dispatch => ({
  exitPage: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/login`)),
  resendEmail: email => dispatch(resendActivationEmail(email)),
  showNotification: (msg, type, flash) =>
    dispatch(setNotification(msg, type, flash)),
  clearState: () => dispatch(resetState()),
});

const NewActivateComponent = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default NewActivateComponent(ActivateComponent);
