import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  checkMark,
} from '../../../../../assets/images/iw-images';
import Button from '../../../../../modules/elements/Button';
import interpolate from '../../../../../modules/utils/interpolate';
import R from 'ramda';

class ActivateComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: props.email,
    };
  }

  componentDidMount() {
    this.props.clearState();
  }

  render() {
    const { content, exitPage, resendEmail, showNotification } = this.props;
    if (!this.state.email) {
      exitPage();
    }
    return (
    <div className="activate">
      <div className="activate-container">
        <div className="activate-container__owl">
          <img src={checkMark} alt="Mail sent" />
        </div>
        <h2>{content.activate.title}</h2>
        <p className="activate-container__info">
          {interpolate(content.activate.info, {
            email: this.state.email,
          })}
        </p>
        <Button
          label="Resend email"
          dataTest="resend-email"
          onClick={() => {
            showNotification(interpolate(R.path(['activate', 'notification'], content), {
              email: this.state.email,
            }), 'success', 20000);
            resendEmail(this.state.email);
            }
          }
        />
      </div>
    </div>
  ); }
}

ActivateComponent.propTypes = {
  content: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  email: PropTypes.string,
  exitPage: PropTypes.func,
  resendEmail: PropTypes.func,
  showNotification: PropTypes.func,
  clearState: PropTypes.func,
};

export default ActivateComponent;
