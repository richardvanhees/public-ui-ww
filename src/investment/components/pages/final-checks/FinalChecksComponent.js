import React, { Component } from 'react';
import PropTypes from 'prop-types';

import QuestionRange from '../../../../../modules/elements/QuestionRange';
import WealthWizards from '../../../../../modules/wealthwizards';
import { checklistChecksAnimated } from '../../../../../assets/images/iw-images';
import {
  multiQuestionHelper,
  userWantsToWait,
} from '../../../modules/final-checks/selectors';
import interpolate from '../../../../../modules/utils/interpolate';

import Tooltip from '../../../../../modules/elements/Tooltip';
import Popup from '../../../../../modules/elements/Popup';
import R from 'ramda';

export default class FinalChecksComponent extends Component {
  static propTypes = {
    content: PropTypes.object.isRequired,
    handleFieldChange: PropTypes.func.isRequired,
    customSetPageAction: PropTypes.func.isRequired,
    savingsMinimum: PropTypes.number.isRequired,
    finalChecks: PropTypes.object.isRequired,
    isMobile: PropTypes.bool.isRequired,
    contributedToIsas: PropTypes.bool.isRequired,
    removeKey: PropTypes.func.isRequired,
    onlyMonthlyInvestment: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      popupText: '',
    };
  }

  togglePopup = popupText => {
    if (!this.props.isMobile) return;

    this.setState({
      popupText: typeof popupText === 'string' ? popupText : '',
    });
    this.helpTextPopup.toggle();
  };

  generateFinalCheckQuestions = questions => {
    const answerKey = 'investment__money_to_invest_readily_available';
    return R.map((question) => {
      if (question.answerKey === answerKey) {
        return {
          ...question,
          skip: this.props.onlyMonthlyInvestment,
        };
      }
      return question;
    })(questions);
  };

  render() {
    const {
      content,
      handleFieldChange,
      removeKey,
      customSetPageAction,
      isMobile,
      savingsMinimum,
      finalChecks,
      contributedToIsas,
    } = this.props;
    const savingsMinimumValue = !!savingsMinimum
      ? `£${savingsMinimum.toLocaleString()} `
      : '3 months essential living costs ';
    const dynamicTooltipValue = !!savingsMinimum
      ? `Based on what you've told us, £${savingsMinimum.toLocaleString()} in savings is 3 months essential living costs for you.`
      : 'This is considered an emergency fund.';

    const nextPage = userWantsToWait(finalChecks) ? 'dashboard' : 'hid';
    return (
      <div className="wrapper final-checks">
        <div className="questions-component__question-container">
          <QuestionRange
            {...this.props}
            dataTest={'final-checks'}
            headerImage={checklistChecksAnimated}
            userInput={multiQuestionHelper(finalChecks)}
            questions={interpolate(
              this.generateFinalCheckQuestions(
                content.finalChecks.questions
              ), {
                savingsMinimum: savingsMinimumValue,
                dynamicTooltip: dynamicTooltipValue,
              })}
            previousPage={
              contributedToIsas
                ? `${WealthWizards.CONTEXT_ROUTE}/isa/amount`
                : `${WealthWizards.CONTEXT_ROUTE}/isa/capture`
            }
            nextPage={`${WealthWizards.CONTEXT_ROUTE}/${nextPage}`}
            onChange={(key, answer) => {
              handleFieldChange(answer.answerKey, answer.answerValue);
            }}
            onRemove={removeKey}
            customSetPageAction={customSetPageAction}
            tooltipPopupCallback={this.togglePopup}
            smallButtons
          />
        </div>
        {!isMobile && <Tooltip />}
        {isMobile && (
          <Popup
            ref={c => {
              this.helpTextPopup = c;
            }}
            closeButtonTop
            closeClickOutside
            buttons={[{ label: 'Close', onClick: this.togglePopup }]}
          >
            {this.state.popupText}
          </Popup>
        )}
      </div>
    );
  }
}
