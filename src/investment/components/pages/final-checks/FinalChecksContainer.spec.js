import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import FinalChecksContainer, {
  mapStateToProps,
  mapDispatchToProps,
  __RewireAPI__,
} from './FinalChecksContainer';
import proxyquire from 'proxyquire';

test('<FinalChecksContainer />', assert => {
  const sandbox = sinon.sandbox.create();
  const handleFieldChangeStub = sandbox.stub();
  const handleFieldChangeSpy = sandbox.spy(handleFieldChangeStub);
  const savingsMinimumStub = sandbox.stub();
  savingsMinimumStub.returns(3);
  const bindActionCreatorsStub = sandbox.stub();
  const lookContentByKeyStub = sandbox.stub();
  const fakeContent = { someContent: 'foo' };
  lookContentByKeyStub.returns(fakeContent);
  const dispatchStub = sandbox.stub();
  const onlyMonthlyInvestmentStub = sandbox.stub();
  onlyMonthlyInvestmentStub.returns(true);

  __RewireAPI__.__Rewire__('bindActionCreators', bindActionCreatorsStub);
  __RewireAPI__.__Rewire__('handleFieldChange', handleFieldChangeSpy);
  __RewireAPI__.__Rewire__('savingsMinimum', savingsMinimumStub);
  __RewireAPI__.__Rewire__('lookContentByKey', lookContentByKeyStub);
  __RewireAPI__.__Rewire__('saveFactFindRequest', lookContentByKeyStub);
  __RewireAPI__.__Rewire__('onlyMonthlyInvestment', onlyMonthlyInvestmentStub);

  // check map state to props
  let result;
  result = mapStateToProps({
    content: {
      data: {
        investment: fakeContent,
      },
    },
    resilience: {
      data: {
        0: {
          investment_resilience__monthly_expenses: 1,
        },
      },
    },
    finalChecks: {
      foo: 'yes',
    },
    browser: {},
    isa: { contributedToIsas: 'yes' },
  });

  assert.deepEqual(result.content, { someContent: 'foo' });
  assert.deepEqual(result.savingsMinimum, 3);
  assert.false(result.isMobile, 'isMobile resolves to false');
  assert.deepEqual(result.finalChecks, { foo: 'yes' });
  assert.true(result.onlyMonthlyInvestment, 'correctly defines investment goal amount');

  __RewireAPI__.__ResetDependency__('bindActionCreators');
  __RewireAPI__.__ResetDependency__('handleFieldChange');
  __RewireAPI__.__ResetDependency__('savingsMinimum');
  __RewireAPI__.__ResetDependency__('lookContentByKey');
  assert.end();
});

test('<FinalChecksContainer /> customSetPageAction', assert => {
  const sandbox = sinon.sandbox.create();
  const saveFactFindRequestStub = sandbox.stub();
  saveFactFindRequestStub.returns(true);
  const saveFactFindRequestSpy = sandbox.spy(saveFactFindRequestStub);

  const userShouldBeReferredStub = sandbox.stub();
  userShouldBeReferredStub.returns(false);

  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestSpy);
  __RewireAPI__.__Rewire__('setCompletedAt', 'some time');
  __RewireAPI__.__Rewire__('userShouldBeReferred', userShouldBeReferredStub);

  mapStateToProps({
    content: {
      data: { investment: {} },
    },
    resilience: {},
    finalChecks: {},
    browser: {},
    isa: { contributedToIsas: 'yes' },
  }).customSetPageAction('investment/dashboard');

  const expectedRequest = {
    redirectPath: 'investment/dashboard',
    doInBackground: true,
    withTimestamp: 'investment__completed_at',
    setCompletedAt: 'some time',
  };

  assert.plan(1);
  assert.true(
    saveFactFindRequestSpy.calledWith(expectedRequest),
    'saves completed at'
  );

  __RewireAPI__.__ResetDependency__('saveFactFindRequest');
  __RewireAPI__.__ResetDependency__('setCompletedAt');
  __RewireAPI__.__ResetDependency__('userShouldBeReferred');
  assert.end();
});

test('<FinalChecksContainer /> customSetPageAction', assert => {
  const sandbox = sinon.sandbox.create();
  const saveFactFindRequestStub = sandbox.stub();
  saveFactFindRequestStub.returns(true);
  const saveFactFindRequestSpy = sandbox.spy(saveFactFindRequestStub);

  const userShouldBeReferredStub = sandbox.stub();
  userShouldBeReferredStub.returns(false);

  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestSpy);
  __RewireAPI__.__Rewire__('setCompletedAt', 'some time');
  __RewireAPI__.__Rewire__('userShouldBeReferred', userShouldBeReferredStub);

  mapStateToProps({
    content: {
      data: { investment: {} },
    },
    resilience: {},
    finalChecks: {},
    browser: {},
    isa: { contributedToIsas: 'yes' },
  }).customSetPageAction('investment/final-checks/2');

  const expectedRequest = {
    redirectPath: 'investment/final-checks/2',
    doInBackground: true,
  };

  assert.plan(1);
  assert.true(
    saveFactFindRequestSpy.calledWith(expectedRequest),
    'saves without completed at'
  );

  __RewireAPI__.__ResetDependency__('saveFactFindRequest');
  __RewireAPI__.__ResetDependency__('setCompletedAt');
  __RewireAPI__.__ResetDependency__('userShouldBeReferred');
  assert.end();
});

test('<FinalChecksContainer /> customSetPageAction', assert => {
  const sandbox = sinon.sandbox.create();
  const saveFactFindRequestStub = sandbox.stub();
  saveFactFindRequestStub.returns(true);
  const saveFactFindRequestSpy = sandbox.spy(saveFactFindRequestStub);

  const userShouldBeReferredStub = sandbox.stub();
  userShouldBeReferredStub.returns(true);

  __RewireAPI__.__Rewire__('saveFactFindRequest', saveFactFindRequestSpy);
  __RewireAPI__.__Rewire__('setCompletedAt', 'some time');
  __RewireAPI__.__Rewire__('userShouldBeReferred', userShouldBeReferredStub);

  mapStateToProps({
    content: {
      data: { investment: {} },
    },
    resilience: {},
    finalChecks: {},
    browser: {},
    isa: { contributedToIsas: 'yes' },
  }).customSetPageAction('investment/final-checks/2');

  const expectedRequest = {
    redirectPath: '/referral',
    doInBackground: false,
    recordSolution: true,
  };

  assert.plan(1);
  assert.true(
    saveFactFindRequestSpy.calledWith(expectedRequest),
    'saves without completed at'
  );

  __RewireAPI__.__ResetDependency__('saveFactFindRequest');
  __RewireAPI__.__ResetDependency__('setCompletedAt');
  __RewireAPI__.__ResetDependency__('userShouldBeReferred');
  assert.end();
});
