import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FinalChecksComponent from './FinalChecksComponent';
import { lookContentByKey } from '../../../../../modules/content/selectors';
import { compose } from 'recompose';
import {
  fieldUpdated,
  setCompletedAt,
  removeKey,
} from '../../../modules/final-checks/actions';
import {
  savingsMinimum,
  userShouldBeReferred,
} from '../../../modules/final-checks/selectors';
import { saveFactFindRequest } from '../../../modules/fact-find/actions';
import R from 'ramda';
import isMobile from '../../../../../modules/utils/is-mobile';
import { onlyMonthlyInvestment } from '../../../modules/investment-goal/selector';

const contentKey = 'investment';

export const mapStateToProps = ({
  content,
  resilience,
  finalChecks,
  browser,
  isa,
  investmentGoal,
}) => ({
  savingsMinimum: savingsMinimum(resilience),
  content: lookContentByKey(contentKey, content),
  finalChecks,
  customSetPageAction: redirectPath => {
    let request = {
      redirectPath,
      doInBackground: true,
    };
    if (!!redirectPath.match('dashboard')) {
      request = R.mergeWith(R.concat, request, {
        withTimestamp: 'investment__completed_at',
        setCompletedAt,
      });
    }
    if (userShouldBeReferred(finalChecks)) {
      request.redirectPath = '/referral';
      request.recordSolution = true;
      request.doInBackground = false;
    }
    return saveFactFindRequest(request);
  },
  isMobile: isMobile(browser),
  contributedToIsas: isa.contributedToIsas === 'yes',
  onlyMonthlyInvestment: onlyMonthlyInvestment({ investmentGoal }),
});

export const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      handleFieldChange: (name, value) => fieldUpdated(name, value),
      removeKey,
    },
    dispatch
  );

export default () => {
  const FinalChecksContainer = compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )
  );

  return FinalChecksContainer(FinalChecksComponent);
};
