import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';
import { lookContentByKey } from '../../../../../modules/content';
import WealthWizards from '../../../../../modules/wealthwizards';
import { compose } from 'recompose';
import { login, setError } from '../../../modules/login/actions';
import { push } from 'react-router-redux';
import loginSelector from '../../../modules/login/login-selector';

const contentKey = 'investment';

const mapStateToProps = state => ({
  content: lookContentByKey(contentKey, state.content),
  loginError: loginSelector(state).loginError,
});

const mapDispatchToProps = dispatch => ({
  login: (username, password) => dispatch(login(username, password)),
  start: () => dispatch(push(`${WealthWizards.CONTEXT_ROUTE}`)),
  forgottenPassword: () =>
    dispatch(push(`${WealthWizards.CONTEXT_ROUTE}/forgotten-password`)),
  resetError: () => dispatch(setError('')),
});

const LoginContainer = compose(connect(mapStateToProps, mapDispatchToProps));

export default LoginContainer(LoginComponent);
