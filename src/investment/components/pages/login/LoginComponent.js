import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../../../../../modules/elements/Input';
import Button from '../../../../../modules/elements/Button';
import {
  characterLoginTitle,
  characterLoginWhiteTitle,
} from '../../../../../assets/images/iw-images';
import Error from '../../../../../modules/elements/Error';
import R from 'ramda';
import Icon from '../../../../../modules/elements/Icon';
import Footer from '../../../modules/elements/Footer';

class LoginComponent extends PureComponent {
  static propTypes = {
    content: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    forgottenPassword: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      errors: {
        email: false,
        password: false,
      },
      activated: R.equals(
        '?activated',
        R.pathOr(null, ['location', 'search'], props)
      ),
    };
  }

  componentWillUnmount() {
    this.props.resetError();
  }

  checkEmailIsValid(email) {
    return !email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  }

  validateForm(login) {
    if (this.checkEmailIsValid(this.state.username)) {
      this.setState({ errors: { email: true } });
    } else if (this.state.password.length <= 1) {
      this.setState({ errors: { email: false, password: true } });
    } else {
      this.setState({ errors: { email: false, password: false } });
      login(this.state.username, this.state.password);
    }
  }

  render() {
    const { login, start, forgottenPassword, loginError, content } = this.props;
    const { activated } = this.state;

    return (
      <div>
        <div className="login-wrapper">
          <div className="login-wrapper-mobile">
            <div className="login-wrapper-mobile__title">
              <img
                src={characterLoginWhiteTitle}
                alt={`${content.characterName} name`}
              />
            </div>
          </div>

          <div className="login-wrapper-container">
            <div className="login-container">
              <div className="login-container__name">
                <img src={characterLoginTitle} alt={content.characterName} />
              </div>

              <div className="login-form">
                {!activated && (
                  <div>
                    <h2 data-test="title-sign-in">Sign in</h2>
                    <p className="login-form__no-account">
                      Don't have an account?
                      <a
                        data-test="start-link"
                        className="login-form__start-journey"
                        onClick={start}
                      >
                        Start journey now
                      </a>
                    </p>
                  </div>
                )}
                {activated && (
                  <div className="login-form__activated-container">
                    <Icon
                      className="login-form__activated-icon"
                      name="check-circle"
                    />
                    <p className="login-form__activated-text">
                      {content.login.activated}
                    </p>
                  </div>
                )}
                <Input
                  dataTest="username"
                  onChange={e => this.setState({ username: e.target.value })}
                  label="Email address"
                />
                <Error
                  compact
                  title="You need to enter a valid email address"
                  active={this.state.errors.email}
                  type={this.state.errors.email ? 'active' : ''}
                />
                <Input
                  dataTest="password"
                  type="password"
                  onChange={e => this.setState({ password: e.target.value })}
                  label="Password"
                  onKeyPress={event => {
                    if (event.key === 'Enter') {
                      this.validateForm(login);
                    }
                  }}
                />
                <Error
                  compact
                  title="You need to enter your password"
                  active={this.state.errors.password}
                  type={this.state.errors.password ? 'active' : ''}
                />
                <Error
                  compact
                  title={loginError}
                  active={loginError}
                  type={loginError ? 'active' : ''}
                />
                <Button
                  label="Sign in"
                  dataTest="sign-in"
                  onClick={() => this.validateForm(login)}
                />
                <a
                  className="login-form__forgot-password"
                  data-test="forgotten-password-link"
                  onClick={forgottenPassword}
                >
                  Forgot password
                </a>
              </div>
            </div>
          </div>

          <p
            data-test="regulation-disclaimer"
            className="login-wrapper__disclaimer"
          >
            {content.adviserName} is authorised and regulated by the Financial
            Conduct Authority
          </p>
        </div>
        <div className="landing">
          <Footer className="landing-footer" />
        </div>
      </div>
    );
  }
}

export default LoginComponent;
