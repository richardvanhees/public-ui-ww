import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LoginComponent from './LoginComponent';
import configureMockStore from 'redux-mock-store';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

test('<LoginComponent> check validation of form fields', t => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    loginError: '',
    content: {
      activate: {
        popup: 'fake news',
      },
    },
    login: () => loginStub,
    start: () => startStub,
    forgottenPassword: () => forgotPasswordStub,
  };

  t.equals(typeof LoginComponent.propTypes, 'object', 'PropTypes are defined');

  const ShallowLoginComponent = shallow(
    <LoginComponent {...fakeRequiredProps} />
  );

  let testingState = {
    username: '',
    password: 'fakePass',
    errors: {
      email: false,
      password: false,
    },
  };

  ShallowLoginComponent.setState(testingState);

  t.deepEquals(
    ShallowLoginComponent.state(),
    {
      activated: false,
      errors: { email: false, password: false },
      password: 'fakePass',
      username: '',
    },
    'check initial state'
  );

  ShallowLoginComponent.instance().validateForm();

  t.deepEquals(
    ShallowLoginComponent.state(),
    {
      activated: false,
      errors: { email: true },
      password: 'fakePass',
      username: '',
    },
    'check state after validation with no email'
  );

  testingState = {
    username: 'fakeemail@ww.com',
    password: '',
    errors: {
      email: false,
      password: false,
    },
  };

  ShallowLoginComponent.setState(testingState);

  ShallowLoginComponent.instance().validateForm();

  t.deepEquals(
    ShallowLoginComponent.state(),
    {
      activated: false,
      errors: { email: false, password: true },
      password: '',
      username: 'fakeemail@ww.com',
    },
    'check state after validation with no password'
  );

  testingState = {
    username: 'fakeemail@ww.com',
    password: 'fakePass',
    errors: {
      email: false,
      password: false,
    },
  };

  ShallowLoginComponent.setState(testingState);

  ShallowLoginComponent.instance().validateForm(loginStub);

  t.deepEquals(
    ShallowLoginComponent.state(),
    {
      activated: false,
      errors: { email: false, password: false },
      password: 'fakePass',
      username: 'fakeemail@ww.com',
    },
    'check state after validation with correct log details'
  );
  t.equals(
    loginStub.callCount,
    1,
    'login is called after correct details passed into login form'
  );

  t.end();
});

test('investment <LoginComponent>', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    loginError: '',
    content: {
      activate: {
        popup: 'fake news',
      },
    },
    login: loginStub,
    start: () => startStub,
    forgottenPassword: () => forgotPasswordStub,
  };

  assert.plan(1);

  const testingState = {
    username: 'fakeemail@ww.com',
    password: 'fakePass',
    errors: {
      email: false,
      password: false,
    },
  };

  const ShallowLoginComponent = shallow(
    <LoginComponent {...fakeRequiredProps} />
  );

  ShallowLoginComponent.setState(testingState);

  ShallowLoginComponent.find('Input')
    .get(1)
    .props.onKeyPress({ key: 'Enter' });

  assert.equals(
    loginStub.callCount,
    1,
    'login is called onKeyPress in password field'
  );
});

test('investment <LoginComponent>', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startJourneyStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    content: {
      activate: {
        popup: 'fake news',
      },
    },
    login: loginStub,
    start: startJourneyStub,
    forgottenPassword: forgotPasswordStub,
  };

  assert.plan(2);

  const wrapper = shallow(<LoginComponent {...fakeRequiredProps} />);

  wrapper.find('.login-form__start-journey').simulate('click');
  wrapper.find('.login-form__forgot-password').simulate('click');

  assert.equals(
    startJourneyStub.called,
    true,
    'Journey started when clicked on button'
  );
  assert.equals(
    forgotPasswordStub.called,
    true,
    'Forgot password called when clicked on button'
  );
});

test('<LoginComponent> activated', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startJourneyStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    content: {
      activate: {
        popup: 'fake news',
      },
      login: {
        activated: 'donald trump approves this message',
      },
    },
    login: loginStub,
    start: startJourneyStub,
    forgottenPassword: forgotPasswordStub,
    location: {
      search: '?activated',
    },
  };

  assert.plan(1);

  const wrapper = shallow(<LoginComponent {...fakeRequiredProps} />);

  assert.equals(
    wrapper.find('.login-form__activated-container').length,
    1,
    'activation notification displayed'
  );
});

test('<LoginComponent> not activated', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startJourneyStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    content: {
      activate: {
        popup: 'fake news',
      },
      login: {
        activated: 'donald trump approves this message',
      },
    },
    login: loginStub,
    start: startJourneyStub,
    forgottenPassword: forgotPasswordStub,
    location: {
      search: '/',
    },
  };

  assert.plan(1);

  const wrapper = shallow(<LoginComponent {...fakeRequiredProps} />);

  assert.equals(
    wrapper.find('.login-form__activated-container').length,
    0,
    'activation notification not displayed'
  );
});

test('<LoginComponent>', assert => {
  const sandbox = sinon.sandbox.create();
  const loginStub = sandbox.stub();
  const startJourneyStub = sandbox.stub();
  const forgotPasswordStub = sandbox.stub();

  const fakeRequiredProps = {
    content: {
      adviserName: 'WW',
      activate: {
        popup: 'fake news',
      },
      login: {
        activated: 'donald trump approves this message',
      },
    },
    login: loginStub,
    start: startJourneyStub,
    forgottenPassword: forgotPasswordStub,
    location: {
      search: '/',
    },
  };

  assert.plan(1);

  const wrapper = shallow(<LoginComponent {...fakeRequiredProps} />);

  assert.equals(
    wrapper.find('p[data-test="regulation-disclaimer"]').text(),
    'WW is authorised and regulated by the Financial Conduct Authority',
    'adviser text correct'
  );
});
