import { connect } from 'react-redux';
import AdviceAcceptedComponent from './AdviceAcceptedComponent';
import { push } from 'react-router-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import WealthWizards from '../../../../../modules/wealthwizards';
import { lookContentByKey } from '../../../../../modules/content';
import { downloadReportRequest } from '../../../modules/solution';

const contentKey = 'investment';

const mapStateToProps = ({ content, solution }) => ({
  content: lookContentByKey(contentKey, content),
  isLoading: solution.isLoading,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      downloadReport: downloadReportRequest,
      back: () => push(`${WealthWizards.CONTEXT_ROUTE}/`),
    },
    dispatch
  );

const AdviceAcceptedContainer = compose(
  connect(mapStateToProps, mapDispatchToProps)
);

export default AdviceAcceptedContainer(AdviceAcceptedComponent);
