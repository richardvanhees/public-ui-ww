import React from 'react';
import PropTypes from 'prop-types';
import PageHeading from '../../../../../modules/elements/PageHeading';
import Instructions from '../../../../../modules/elements/InstructionsComponents/Instructions';
import PageNavigation from '../../../../../modules/elements/PageNavigation';

const AdviceAcceptedComponent = ({ content, downloadReport }) => {
  const sectionContent = content.adviceAccepted;

  return (
    <div className="advice-accepted wrapper">
      <PageHeading
        title={sectionContent.title}
        subTitle={sectionContent.subTitle}
        smallSubTitle
        icon={'check-circle'}
        dataTest={'advice-accepted'}
      />

      <Instructions
        list
        title={sectionContent.instructionsTitle}
        items={sectionContent.items}
      />

      <PageNavigation
        next={downloadReport}
        nextLabel={sectionContent.callToActionLabel}
        wideButtons
        noSideMargins
        testNext={'advice-accepted'}
      />
    </div>
  );
};

AdviceAcceptedComponent.propTypes = {
  downloadReport: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
  content: PropTypes.object.isRequired,
};

export default AdviceAcceptedComponent;
