import { configure, addDecorator } from '@storybook/react';
import { withInfo, setDefaults } from '@storybook/addon-info';

// automatically import all files ending in *.stories.js
const req = require.context('../modules/elements', true, /.stories.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

// addon-info
setDefaults({
  header: false, // Toggles display of header with component name and description
  inline: true, // Toggles display of header with component name and description
});

addDecorator((story, context) => withInfo(``)(story)(context));
configure(loadStories, module);
