const envalid = require('envalid');
const { str, bool, num } = envalid;

module.exports = {
  env: {
    CONTEXT_ROUTE: str({ default: '/money-wizard-beta' }),
    RECAPTCHA_KEY: str(),
    RECAPTCHA_SECRET: str(),
    RECAPTCHA_AUTOMATION_KEY: str(),
    RECAPTCHA_AUTOMATION_SECRET: str(),
    SHOW_FAKE_RECAPTCHA: bool({ default: false }),
    RECAPTCHA_AUTOMATION_FEATURE_ACTIVE: bool(),
    CONTENT_SERVICE_BASE_URL: str({
      default: 'http://content-service/content-service',
    }),
    CONTENT_SERVICE_VERSION: str({
      default: '2.0',
    }),
    CONTENT_SERVICE_TODOS_VERSION: str({
      default: '1.0',
    }),
    AUTH_SVC_URL: str({
      default: 'http://digital-auth-service/digital-auth-service',
    }),
    AUTH_SVC_API_KEY: str(),
    TODO_STORE_BASE_URL: str(),
    CHAT_STORE_BASE_URL: str(),
    DEFAULT_CHAT_PAUSE_IF_NOT_SPECIFIED: num({ default: 1000 }),
    EMAIL_SERVICE_URL: str({ default: 'http://email-service/email-service' }),
    EMAIL_SERVICE_API_KEY: str(),
    RISK_PROFILE_SVC_URL: str(),
    RISK_PROFILE_SVC_API_KEY: str(),
    MONEY_WIZARD_BETA_REG_EMAIL_TEMPLATE_NAME: str({
      default: 'Money Wizard Beta Registration Confirmation',
    }),
    INCOME_TAX_SVC_BASE_URL: str({
      default: 'https://income-tax-service.wealthwizards.io',
    }),
    STATE_PENSION_DATE_CALC_BASE_URL: str({
      default: 'https://state-pension-date.wealthwizards.io',
    }),
    INTERCOM_APP_ID: str({ default: 'false' }),
    FACT_FIND_SVC_URL: str({
      default: 'http://fact-find-service/fact-find-service',
    }),
    SOLUTION_STORE_URL: str(),
    HOTJAR_SITE_ID: str({ default: false }),
  },
  clientEnv: [
    'CONTEXT_ROUTE',
    'INCOME_TAX_SVC_BASE_URL',
    'STATE_PENSION_DATE_CALC_BASE_URL',
    'RECAPTCHA_KEY',
    'RECAPTCHA_AUTOMATION_KEY',
    'CONTENT_SERVICE_BASE_URL',
    'CONTENT_SERVICE_VERSION',
    'CONTENT_SERVICE_TODOS_VERSION',
    'TODO_STORE_BASE_URL',
    'AUTH_SVC_URL',
    'CHAT_STORE_BASE_URL',
    'SHOW_FAKE_RECAPTCHA',
    'DEFAULT_CHAT_PAUSE_IF_NOT_SPECIFIED',
    'INTERCOM_APP_ID',
    'FACT_FIND_SVC_URL',
    'SOLUTION_STORE_URL',
    'HOTJAR_SITE_ID',
  ],
};
