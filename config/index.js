const _ = require('lodash');
const path = require('path');
const envalid = require('envalid');
const { str, num } = envalid;

require('dotenv').config({ silent: true });

const productSpecificConfig = require(`./${process.env.PRODUCT_KEY}`);

const env = envalid.cleanEnv(process.env, Object.assign({}, {
  APP_PORT: num({ default: 80 }),
  PRODUCT_KEY: str({}),
  TOKEN_KEY_NAME: str({ default: 'wealthwizards' }),
}, productSpecificConfig.env));

const clientEnv = _.pick(env, [
  'APP_PORT',
  'PRODUCT_KEY',
  'TOKEN_KEY_NAME',
  'GA_TRACKING_ID',
  ...productSpecificConfig.clientEnv,
]);

module.exports = {
  paths: {
    BASE_DIR: path.resolve(__dirname, '../'),
    PUBLIC_DIR: 'dist',
  },
  env,
  clientEnv,
};
