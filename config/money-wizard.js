const envalid = require('envalid');
const { str, bool, num } = envalid;

module.exports = {
  env: {
    MONGODB_URL: str(),
    MONEY_WIZARD_API_KEY: str(),
    MONEY_WIZARD_API_KEY_MI_DATA: str(),
    EMAIL_SERVICE_URL: str(),
    EMAIL_SERVICE_API_KEY: str(),
    MW_PII_DATA_ENCRYPTION_KEY: str(),
    GA_TRACKING_ID: str(),
    CONTEXT_ROUTE: str({ default: '/money-wizard' }),
    WOOTRIC_NPS_NUMBER: str({ default: 'NPS-d6b41745' }),
    UPDATED_MW_PII_DATA_ENCRYPTION_KEY: str(),
    UPDATED_MW_PII_DATA_ENCRYPTION_IV: str(),
    HOTJAR_SITE_ID: str({ default: false }),
    CORRESPONDENCE_EMAIL: str({ default: 'thewizard@wealthwizards.com' }),
    SHARE_LINK: str({ default: 'https://www.wealthwizards.com/' }),
    ALTERNATIVE_PRIORITY_DESCRIPTIONS: bool(),
    TIPS_POPUP_TIME_TO_APPEAR: num({ default: 5000 }),
    PAT_DISABLED: str({ default: 'impellam,fco' }),
    FORCE_WW_TEMPLATE: str(),
    PAT_MAIL_ADDRESS: str(),
    MONEY_WIZARD_BETA_URL: str(),
  },
  clientEnv: [
    'CONTEXT_ROUTE',
    'WOOTRIC_NPS_NUMBER',
    'HOTJAR_SITE_ID',
    'CORRESPONDENCE_EMAIL',
    'SHARE_LINK',
    'ALTERNATIVE_PRIORITY_DESCRIPTIONS',
    'PAT_DISABLED',
    'TIPS_POPUP_TIME_TO_APPEAR',
    'FORCE_WW_TEMPLATE',
    'MONEY_WIZARD_BETA_URL',
  ],
};
