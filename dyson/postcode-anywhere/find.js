/* eslint-disable */
module.exports = {
  path: '/postcode-anywhere/find',
  method: 'GET',
  cache: true,
  delay: 0,
  template: (params, query) => {
    if (query.Container) {
      return [
        {
          "Id": "TESTID1",
          "Type": "Address",
          "Text": "1 Blossom Way",
          "Highlight": "0-14",
          "Description": "Blossom Way, Rugby"
        },
        {
          "Id": "TESTID2",
          "Type": "Address",
          "Text": "2 Blossom Way",
          "Highlight": "0-14",
          "Description": "Blossom Way, Rugby"
        },
        {
          "Id": "TESTID3",
          "Type": "Address",
          "Text": "3 Blossom Way",
          "Highlight": "0-14",
          "Description": "Blossom Way, Rugby"
        },
      ]
    }
    return [
      {
        "Id": "TESTID2",
        "Type": "Street",
        "Text": "Bossom Way",
        "Highlight": "0-14",
        "Description": "3 Addresses"
      },
      {
        "Id": "TESTID3",
        "Type": "Postcode",
        "Text": "CV22 3AB",
        "Highlight": "0-14",
        "Description": "5 Addresses"
      }
    ]
  }
};
