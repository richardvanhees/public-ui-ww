module.exports = {
  path: '/reset-password-service/v1/reset-password-request',
  method: 'POST',
  cache: true,
  delay: 0
};
