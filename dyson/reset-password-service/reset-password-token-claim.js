module.exports = {
  path: '/reset-password-service/v1/reset-password-token-claim',
  method: 'POST',
  cache: true,
  delay: 0
};
