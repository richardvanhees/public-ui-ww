module.exports = {
  path: '/payment',
  method: 'GET',
  cache: true,
  delay: 0,
  status: (req, res) => {
    return res.json(req.query);
  },
};
