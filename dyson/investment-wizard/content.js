const content = require('../../content/investment/athena/investment');

module.exports = {
  path: '/content-service/v2.0/content/investment-wizard/',
  method: 'GET',
  cache: true,
  delay: 0,
  status: (req, res) => {
    res.json(content).end();
  },
};
