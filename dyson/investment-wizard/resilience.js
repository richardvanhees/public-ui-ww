module.exports = {
  path: '/risk-profile/v1/financial-resilience',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      "profile": "Considerable",
      "score": 3,
      "raw_score": 10
    };
  },
};
