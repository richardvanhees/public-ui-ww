module.exports = {
  path: '/risk-profile/v1/attitude-to-risk',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      "alerts": [
        "string"
      ],
      "profile": "Confident",
      "score": 0,
      "raw_score": 0
    };
  },
};
