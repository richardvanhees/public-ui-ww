module.exports = {
  path: '/investment_fulfillment/v1/webhook/claim',
  method: 'GET',
  cache: true,
  delay: 0,
  template: () => {
    return {
      address: {
        line1: '17 Towpath Close',
        line2: 'Longford',
        postCode: 'CV6 6RG',
        town: 'Coventry',
      },
      customerName: { firstName: 'Gary', lastName: 'george', title: 'Mr' },
      customerPersonalDetails: {
        dateOfBirth: '1984-07-24',
        emailAddress: 'gary.george@wealthwizards.com',
        nationalInsuranceNumber: 'NL989876c',
        phoneNumber: '07967405777',
      },
      fundInvestments: [
        {
          collectionFrequency: 'MONTHLY',
          collectionStartDay: 1,
          collectionStartDayType: 'SPECIFIC_DAY',
          lumpsumAmount: 900,
          regularPaymentAmount: 500,
        },
      ],
      wrapper: 'STOCKS_AND_SHARES_ISA',
    };
  },
};
