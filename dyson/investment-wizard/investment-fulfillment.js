module.exports = {
  path: '/investment_fulfillment/v1/webhook',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      "uid": "123abc456def"
    };
  },
};
