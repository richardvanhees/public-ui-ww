module.exports = {
  path: '/risk-profile/v1/financial-flexibility',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      "profile": "Considerable",
      "score": 3,
      "raw_score": 10
    };
  },
};
