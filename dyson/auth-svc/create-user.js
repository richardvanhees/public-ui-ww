module.exports = {
  path: '/auth-service/v1/user',
  method: 'POST',
  cache: true,
  delay: 0,
  status: (req, res) => {
    // if (true) {
    //   return res.status(400).json({ code: 'USERNAME_ALREADY_EXISTS' }).end();
    // }
    return res.json({}).end();
  },
};
