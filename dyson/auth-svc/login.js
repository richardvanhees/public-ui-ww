module.exports = {
  path: '/auth-service/v1/oauth/token',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      "jwt": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InNkZmpzZ2ZAa2pzZGZnaGprcy5jb20iLCJ1bmlxdWVJZGVudGlmaWVyIjoic2RmanNnZkBranNkZmdoamtzLmNvbSIsInVzZXJUeXBlIjoidXNlciIsInRpbWVzdGFtcCI6MTUyMzYxNDIzODczNCwiaWF0IjoxNTIzNjE0MjI4LCJleHAiOjE1MjM2MTQ1Mjh9.Wf7gZig6imy74Vk5GBCmkaJrysOIgRuhCQkphctnl7UUksvj6wa3_sKh4arlgSAqs7ViF94aOdYcxBLU1txDV4AwqzU24guSTjlUf-MrUFyNtxdDiSR2rPddeyCmHfNib1G5m5CL20zDRUZad7_Mz3nN7aB0sgiT2IVEhT9r0hHcMAEWs9YCPBowKk5DHqJI5rPpVObdDTPIOL20n6PgZR9Qyyel6t4LDz5WaWlT5ag_LekzdlE5Ar-BSeIOPFSmrM9YVASDN0qB4ECwrsu2DfO27vFL-JpvOTK5XA6RiqY6lLniDjM6SgkeX2vM0tlrRnaYzqGH3DuG9lCzuRnyuQ",
      "refresh_token": "ea4009f0-3f02-11e8-b502-e91c179e2597"
    };
  },
};
