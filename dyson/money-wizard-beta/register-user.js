module.exports = {
  path: '/digital-auth-service/v1/magic-links/register',
  method: 'POST',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res
      .json({
        jwt:
          'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyVHlwZSI6InVzZXIiLCJ1bmlxdWVJZGVudGlmaWVyIjoiOTk5OTk5OTk5OTk5OSIsImlhdCI6MTU0MjgxMjA5MiwiZXhwIjoyMzIwNDEyMDkyfQ.JD-dZ0Q-8ve3999lVJhGK8jfjXScBAugrs774pA1HVVjxjptASJqwGQ4aWrtc3ph4PcW72-rGRHppW-qeuaJ7RYCGTwlGhGntGBp3p45JttFZfc9PRv9MI9LCToBe5xLp9kB_iw920LVWDN6Utphk8Mdf2rWL7Ggw8u3CvEAUxcKOZhEIJD_GU4TaiiwkcF2FaoEVmjF7epc5A0eW3VbxtIJDMQZKHFInHk7mmJorx1Tq6K_Outam9pfbUUcmMm19jbzxnEDxeq_TDJsdlxlz9ThILJ2u-vU99KlkuO1bn7b53JdsaDcDQXfUXF7hOcymvYdMvUudnCDp65K-4Cy5A',
        refresh_token: 'refresh_token',
      })
      .end();
  },
};
