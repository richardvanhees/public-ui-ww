module.exports = {
  path: '/todo-store/v1/todo',
  method: 'PATCH',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res.status(200).end();
  },
};
