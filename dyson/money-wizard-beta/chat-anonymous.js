const fs = require('fs');

module.exports = {
  path: '/chat-store/anonymous/chat',
  method: 'GET',
  cache: false,
  delay: 0,
  status: (req, res) => {
    // eslint-disable-next-line
    const content = JSON.parse(
      fs
        .readFileSync(
          `${process.cwd()}/content/money-wizard-beta/${req.query.todoId}.json`
        )
        .toString()
    );
    res
      .json({
        status: 'IN_PROGRESS',
        abVersion: 'a',
        todoId: req.query.todoId,
        chat: content,
      })
      .end();
  },
};
