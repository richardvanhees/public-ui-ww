module.exports = {
  path: '/solution-store/v1/user/documents',
  method: 'GET',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res
      .json({
        documents: [
          {
            capability: 'countdown',
            href:
              'http://localhost:3001/solution-store/v1/user/solution/12345/suitability-report',
            createdAt: '2012-01-01T12:12:12Z',
          },
          {
            capability: 'investment',
            href:
              'http://localhost:3001/solution-store/v1/user/solution/12345/suitability-report',
            createdAt: '2012-01-01T12:12:12Z',
          },
        ],
      })
      .end();
  },
};
