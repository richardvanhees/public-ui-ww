const fs = require('fs');

module.exports = {
  path: '/solution-store/v1/user/solution/suitability-report',
  method: 'GET',
  cache: false,
  delay: 0,
  status: (req, res) => {
    const file = fs.createReadStream(
      `${process.cwd()}/dyson/money-wizard-beta/dummy.pdf`
    );

    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=dummy.pdf');
    file.pipe(res);
    res.end();
  },
};
