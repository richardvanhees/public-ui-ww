module.exports = {
  path: '/todo-store/v1/todos',
  method: 'GET',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res
      .json({
        todos: [
          {
            id: 'registration',
            title: 'Registration',
            description: "Let's get you registered",
            status: 'COMPLETE',
            duration: 2,
          },
          {
            id: 'getToKnowYou',
            title: 'About you',
            description:
              'Tell us a bit about you, so we can make your To-Do items personal',
            status: 'AVAILABLE',
            duration: 3,
          },
          {
            id: 'consolidation',
            title: 'Consolidation',
            description: 'Stub Consolidation',
            status: 'AVAILABLE',
            duration: 3,
          },
          {
            id: 'moneyWizard',
            title: 'Money Wizard',
            description: 'Stub Money Wizard',
            status: 'AVAILABLE',
            duration: 3,
          },
          {
            id: 'pensionOptionsTriage',
            title: 'Pension Options Triage',
            description: 'Stub Pension Options Triage',
            status: 'AVAILABLE',
            duration: 3,
          },
          {
            id: 'healthCheck',
            title: 'Financial Health Check',
            description:
              'Help you identify your financial priorities and improve your financial wellbeing',
            status: 'AVAILABLE',
          },
          {
            id: 'openExternalType',
            title: 'Open external app',
            description: "Let's open an external application",
            status: 'AVAILABLE',
            href: 'http://google.com',
          },
        ],
      })
      .end();
  },
};
