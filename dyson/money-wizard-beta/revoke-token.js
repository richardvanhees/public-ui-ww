module.exports = {
  path: '/digital-auth-service/v1/oauth/revoke-token',
  method: 'POST',
  cache: true,
  delay: 0,
  status: (req, res) => {
    res.status(200).end();
  },
};
