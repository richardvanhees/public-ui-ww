module.exports = {
  path: '/todo-store/v1/defaults',
  method: 'GET',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res
      .json({
        todos: [
          {
            id: 'registration',
            title: 'Registration',
            description: "Let's get you registered",
            duration: 3,
          },
        ],
      })
      .end();
  },
};
