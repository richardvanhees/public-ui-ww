module.exports = {
  path: '/chat-store/v1/chat',
  method: 'PUT',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res.status(200).end();
  },
};
