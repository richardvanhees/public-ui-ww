module.exports = {
  path: '/email-service/v2/email',
  method: 'POST',
  cache: false,
  delay: 0,
  status: (req, res) => {
    res.status(200).end();
  },
};
