const fs = require('fs');

module.exports = {
  path: '/solution-store/v1/user/solution/:id/:report',
  method: 'GET',
  cache: false,
  status: (req, res) => {
    res.set('content-type', 'application/pdf');
    res.send(fs.readFileSync(`${__dirname}/sample-report.pdf`));
  },
};
