module.exports = {
  path: '/solution-store/v1/user/solution',
  method: 'POST',
  cache: true,
  delay: 0,
  template: () => {
    return {
      solution: {
        riskLevel: 4,
      },
    };
  },
};
