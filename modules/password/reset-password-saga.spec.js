import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { resetPassword, resetPasswordCall } from './reset-password-saga';
import { setError, setIsLoading } from '../browser/actions';

test('resetPassword - happy day', assert => {
  assert.plan(1);

  const testData = {
    token: '123abc',
    password: 'password',
  };

  const target = testSaga(resetPassword, testData)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(resetPasswordCall, testData.token, testData.password)
    .next()
    .put(setIsLoading(false))
    .next();

  assert.true(target.isDone(), 'resetPassword success');
});

test('resetPassword - bad day', assert => {
  assert.plan(1);

  const testData = {
    token: '123abc',
    password: 'password',
  };

  const target = testSaga(resetPassword, testData)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(resetPasswordCall, testData.token, testData.password)
    .throw({ response: { status: 500 } })
    .put(setIsLoading(false))
    .next()
    .put(setError('Something went wrong.'))
    .next();

  assert.true(target.isDone(), 'server error - 500');
});
