import { call, put, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../wealthwizards';
import passwordTypes from './types';
import { setError, setIsLoading } from '../browser/actions';
import axios from 'axios';

export const resetPasswordCall = (token, password) =>
  axios({
    method: 'POST',
    url: `${
      WealthWizards.RESET_PASSWORD_SVC_URL
    }/v1/reset-password-token-claim`,
    data: { token, password },
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  });

export function* resetPassword({ token, password }) {
  try {
    yield put(setIsLoading(true));
    yield call(resetPasswordCall, token, password);
    yield put(setIsLoading(false));
    return;
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(setError('Something went wrong.'));
  }
}

export default function* resetPasswordSaga() {
  yield takeLatest(passwordTypes.RESET_PASSWORD, resetPassword);
}
