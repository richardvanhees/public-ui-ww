import test from 'tape';
import sinon from 'sinon';
import { testSaga } from 'redux-saga-test-plan';
import {
  forgottenPassword,
  forgottenPasswordCall,
  __RewireAPI__,
} from './forgotten-password-saga';
import { GENERATE_PASSWORD_TOKEN } from './types';
import { setError, setIsLoading } from '../browser/actions';

test('forgottenPassword - happy day', assert => {
  assert.plan(2);

  const fakeToken = sinon.sandbox.create().stub();
  __RewireAPI__.__Rewire__('removeToken', fakeToken);

  const testData = {
    email: 'foo@bar.com',
  };

  const target = testSaga(forgottenPassword, testData)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(forgottenPasswordCall, testData.email)
    .next()
    .put(setIsLoading(false))
    .next();

  assert.equal(fakeToken.callCount, 1, 'reset token is called');
  assert.true(target.isDone(), 'forgottenPassword success');

  __RewireAPI__.__ResetDependency__('removeToken');
});

test('forgottenPassword  - bad day', assert => {
  assert.plan(2);

  const fakeToken = sinon.sandbox.create().stub();
  __RewireAPI__.__Rewire__('removeToken', fakeToken);

  const testData = {
    email: 'foo@bar.com',
  };

  const target = testSaga(forgottenPassword, testData)
    .next()
    .put(setIsLoading(true))
    .next()
    .call(forgottenPasswordCall, testData.email)
    .throw({ response: { status: 500 } })
    .put(setIsLoading(false))
    .next()
    .put(setError('Something went wrong.'))
    .next();

  assert.equal(fakeToken.callCount, 1, 'reset token is called');
  assert.true(target.isDone(), 'forgottenPassword server error');

  __RewireAPI__.__ResetDependency__('removeToken');
});
