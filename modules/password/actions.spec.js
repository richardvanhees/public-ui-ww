import test from 'tape';
import * as actions from './actions.js';
import types from './types';

test('PasswordActions generatePasswordToken', assert => {
  assert.plan(1);
  const result = actions.generatePasswordToken('foo@bar.com');
  assert.deepEqual(
    result,
    { type: types.GENERATE_PASSWORD_TOKEN, email: 'foo@bar.com' },
    'generatePasswordToken'
  );
});

test('PasswordActions resetPassword', assert => {
  assert.plan(1);
  const result = actions.resetPassword('token', 'password');
  assert.deepEqual(
    result,
    { type: types.RESET_PASSWORD, token: 'token', password: 'password' },
    'resetPassword'
  );
});
