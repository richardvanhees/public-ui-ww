import { call, put, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../wealthwizards';
import passwordTypes from './types';
import { setError, setIsLoading } from '../browser/actions';
import axios from 'axios';
import removeToken from '../utils/remove-token';

export const forgottenPasswordCall = email =>
  axios({
    method: 'POST',
    url: `${WealthWizards.RESET_PASSWORD_SVC_URL}/v1/reset-password-request`,
    data: { email_address: email },
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  });

export function* forgottenPassword({ email }) {
  try {
    removeToken();
    yield put(setIsLoading(true));
    yield call(forgottenPasswordCall, email);
    yield put(setIsLoading(false));
    return;
  } catch (error) {
    yield put(setIsLoading(false));
    yield put(setError('Something went wrong.'));
  }
}

export default function* forgottenPasswordSaga() {
  yield takeLatest(passwordTypes.GENERATE_PASSWORD_TOKEN, forgottenPassword);
}
