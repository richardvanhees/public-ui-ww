import passwordTypes from './types';

export const generatePasswordToken = email => ({
  type: passwordTypes.GENERATE_PASSWORD_TOKEN,
  email,
});

export const resetPassword = (token, password) => ({
  type: passwordTypes.RESET_PASSWORD,
  token,
  password,
});
