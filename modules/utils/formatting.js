export const capitalize = ([first, ...rest] = '') =>
  first ? first.toUpperCase() + rest.join('').toLowerCase() : '';

export const timeframeFormatter = timeframe => {
  const _tempSplit = timeframe.split('-');
  return _tempSplit.length > 1
    ? `${_tempSplit[0]} - ${_tempSplit[1]}`
    : timeframe;
};
