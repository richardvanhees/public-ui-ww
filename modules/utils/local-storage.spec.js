import sinon from 'sinon';
import sinonAsPromised from 'sinon-as-promised';
import test from 'tape';
import { setItem, getItem, removeItem, __RewireAPI__ } from './local-storage';
const LocalStorage = require('node-localstorage').LocalStorage;

test('local-storage', assert => {
  assert.plan(1);
  const sandbox = sinon.sandbox.create();

  setItem('key', 'some data');

  assert.equal(
    localStorage.getItem('key'),
    'some data',
    'getItem with local storage supported'
  );

  localStorage.removeItem('key');
});

test('local-storage', assert => {
  assert.plan(2);
  const sandbox = sinon.sandbox.create();

  const data = {};

  const fakeCookies = {
    set: (key, value) => {
      data[key] = value;
    },
    get: key => {
      return data[key];
    },
    remove: key => {
      delete data[key];
    },
  };

  __RewireAPI__.__Rewire__('Cookies', fakeCookies);

  delete global.localStorage;

  setItem('key', 'some data');

  global.localStorage = new LocalStorage('./scratch');

  assert.notEqual(
    localStorage.getItem('key'),
    'some data',
    'data not set in local storage'
  );

  assert.equal(fakeCookies.get('key'), 'some data', 'data set in cookies');

  __RewireAPI__.__ResetDependency__('Cookies');
});

test('local-storage', assert => {
  assert.plan(1);

  localStorage.setItem('key', 'data');

  removeItem('key');

  assert.equal(
    localStorage.getItem('key'),
    null,
    'removeItem correctly removes data'
  );
});

test('local-storage', assert => {
  assert.plan(1);
  const sandbox = sinon.sandbox.create();

  const data = {};

  const fakeCookies = {
    set: (key, value) => {
      data[key] = value;
    },
    get: key => {
      return data[key];
    },
    remove: key => {
      delete data[key];
    },
  };

  __RewireAPI__.__Rewire__('Cookies', fakeCookies);

  delete global.localStorage;

  fakeCookies.set('key', 'data');

  removeItem('key');

  global.localStorage = new LocalStorage('./scratch');

  assert.equal(fakeCookies.get('key'), undefined, 'data removed from cookies');

  __RewireAPI__.__ResetDependency__('Cookies');
});

test('local-storage', assert => {
  assert.plan(1);

  localStorage.setItem('key', 'data');

  assert.equal(
    getItem('key'),
    'data',
    'getItem correctly gets data from local storage'
  );
});

test('local-storage', assert => {
  assert.plan(1);

  delete global.localStorage;

  const data = {};
  const fakeCookies = {
    set: (key, value) => {
      data[key] = value;
    },
    get: key => {
      return data[key];
    },
    remove: key => {
      delete data[key];
    },
  };

  __RewireAPI__.__Rewire__('Cookies', fakeCookies);

  fakeCookies.set('key', 'data');

  assert.equal(
    getItem('key'),
    'data',
    'getItem correctly gets data from the cookies'
  );

  global.localStorage = new LocalStorage('./scratch');

  __RewireAPI__.__ResetDependency__('Cookies');
});
