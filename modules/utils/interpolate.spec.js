import test from 'tape';
import interpolate from './interpolate';

test('Interpolate', t => {
  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "This is a test string";
    const result = interpolate(testContent);

    const expectedNewContent = "This is a test string";

    t.equals(result, expectedNewContent, 'Interpolation of string');
    t.end();
  });

  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "This is a test string with {{testVariable}}";
    const result = interpolate(testContent, { testVariable: 'variable' });

    const expectedNewContent = "This is a test string with variable";

    t.equals(result, expectedNewContent, 'Interpolation of string with variable');
    t.end();
  });

  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "This is a test string with different variables: {{testVariable1}}, {{testVariable2}}";
    const result = interpolate(testContent, { testVariable1: 'one', testVariable2: 'two' });

    const expectedNewContent = "This is a test string with different variables: one, two";

    t.equals(result, expectedNewContent, 'Interpolation of string with two different variables');
    t.end();
  });

  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "This is a test string with equal variables: {{testVariable}}, {{testVariable}}";
    const result = interpolate(testContent, { testVariable: 'one' });

    const expectedNewContent = "This is a test string with equal variables: one, one";

    t.equals(result, expectedNewContent, 'Interpolation of string with two equal variables');
    t.end();
  });

  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "This is a test string with numeral variable: {{testCurrency}}{{testVariable}}";
    const result = interpolate(testContent, { testCurrency: '£', testVariable: 3000 });

    const expectedNewContent = "This is a test string with numeral variable: £3,000";

    t.equals(result, expectedNewContent, 'Interpolation of string with numeral variable');
    t.end();
  });

  t.test('String interpolation', t => {
    t.plan(1);
    const testContent = "<p>This is a test string with {{testVariable}} and HTML</p>";
    const result = interpolate(testContent, { testVariable: 'variable' });

    const expectedNewContent = '{"type":"p","key":"0","ref":null,"props":{"children":["This is a test string with variable and HTML"]},"_owner":null,"_store":{}}';

    t.equals(JSON.stringify(result), expectedNewContent, 'Interpolation of string with variable and HTML');
    t.end();
  });

  t.test('Array interpolation', t => {
    t.plan(1);
    const testContent = ["This", "Is", "A", "Test", "Array"];
    const result = interpolate(testContent);

    const expectedNewContent = ["This", "Is", "A", "Test", "Array"];

    t.deepEqual(result, expectedNewContent, 'Interpolation of array');
    t.end();
  });

  t.test('Array interpolation', t => {
    t.plan(1);
    const testContent = ["This", "Is", "A", "Test", "Array", "With", "{{testVariable}}"];
    const result = interpolate(testContent, { testVariable: 'Variable' });

    const expectedNewContent = ["This", "Is", "A", "Test", "Array", "With", "Variable"];

    t.deepEqual(result, expectedNewContent, 'Interpolation of array with variable');
    t.end();
  });

  t.test('Object interpolation', t => {
    t.plan(1);
    const testContent = {
      testContent1: "This is a",
      testContent2: "test object"
    };
    const result = interpolate(testContent);

    const expectedNewContent = {
      testContent1: "This is a",
      testContent2: "test object"
    };

    t.deepEqual(result, expectedNewContent, 'Interpolation of object');
    t.end();
  });

  t.test('Object interpolation', t => {
    t.plan(1);
    const testContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: "with {{testVariable}}"
    };
    const result = interpolate(testContent, { testVariable: 'variable' });

    const expectedNewContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: "with variable"
    };

    t.deepEqual(result, expectedNewContent, 'Interpolation of object with variable');
    t.end();
  });

  t.test('Object interpolation', t => {
    t.plan(1);
    const testContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: ["with", "array"]
    };
    const result = interpolate(testContent);

    const expectedNewContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: ["with", "array"]
    };

    t.deepEqual(result, expectedNewContent, 'Interpolation of object with array');
    t.end();
  });

  t.test('Object interpolation', t => {
    t.plan(1);
    const testContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: ["with", "array", "and", "{{count}}"],
      testContent4: "{{testVariable}}"
    };
    const result = interpolate(testContent, { count: 'multiple', testVariable: 'variables' });

    const expectedNewContent = {
      testContent1: "This is a",
      testContent2: "test object",
      testContent3: ["with", "array", "and", "multiple"],
      testContent4: "variables"
    };

    t.deepEqual(result, expectedNewContent, 'Interpolation of object with array and variables');
    t.end();
  });
});
