const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

test('that rounding is correct', t => {
  const rounder = require('./round-to-significant-figures');

  t.equals(rounder(0.567), 0.567);
  t.equals(rounder(1.333), 1.33);
  t.equals(rounder(99.6), 99.6);
  t.equals(rounder(99.4), 99.4);
  t.equals(rounder(10000005), 10000000);
  t.equals(rounder(12337.5678), 12300);
  t.equals(rounder(8239.12), 8230);
  t.equals(rounder(8239), 8230);
  t.equals(rounder(0.00006), 0.00006);

  t.end();
});
