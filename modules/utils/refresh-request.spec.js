import test from 'tape';
import co from 'co';
import sinon from 'sinon';
import { post, head, __RewireAPI__ } from './refresh-request';

test('refresh-request, post', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('axios', () =>
    Promise.reject({
      response: {
        status: 400,
        data: {
          code: 'another 400 error',
        },
      },
    })
  );

  const setErrorStub = sandbox.stub();
  setErrorStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setError', setErrorStub);

  co(function*() {
    try {
      yield post('http://prettydecent.com', 'Bearer', {}, 'application/json');
    } catch (e) {
      assert.deepEqual(e, {
        response: { data: { code: 'another 400 error' }, status: 400 },
      });
      __RewireAPI__.__ResetDependency__('axios');
      __RewireAPI__.__ResetDependency__('setError');
    }
  });
});

test('refresh-request, 401 error', assert => {
  assert.plan(3);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('axios', () =>
    Promise.reject({
      response: {
        status: 401,
        data: {
          code: 'another 401 error',
        },
      },
    })
  );

  const eventStub = sandbox.stub();
  __RewireAPI__.__Rewire__('event', eventStub);

  const setErrorStub = sandbox.stub();
  const refreshStub = sandbox.stub();
  setErrorStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setError', setErrorStub);
  __RewireAPI__.__Rewire__('refresh', refreshStub);

  co(function*() {
    try {
      yield post('http://prettydecent.com', 'Bearer', {}, 'application/json');
    } catch (e) {
      assert.deepEqual(e, {
        response: { data: { code: 'another 401 error' }, status: 401 },
      });
      assert.deepEqual(refreshStub.called, true, 'Refresh script called');
      assert.deepEqual(
        eventStub.args,
        [
          [
            {
              action: 'Response code: 401 url: http://prettydecent.com',
              category: 'HttpRequestAuthError',
            },
          ],
        ],
        'logs 401 error'
      );
      __RewireAPI__.__ResetDependency__('axios');
      __RewireAPI__.__ResetDependency__('setError');
      __RewireAPI__.__ResetDependency__('event');
    }
  });
});

test('refresh-request, 403 error', assert => {
  assert.plan(2);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('axios', () =>
    Promise.reject({
      response: {
        status: 403,
        data: {
          code: 'another 403 error',
        },
      },
    })
  );

  const setErrorStub = sandbox.stub();
  const refreshStub = sandbox.stub();
  setErrorStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setError', setErrorStub);
  __RewireAPI__.__Rewire__('refresh', refreshStub);

  co(function*() {
    try {
      yield post('http://prettydecent.com', 'Bearer', {}, 'application/json');
    } catch (e) {
      assert.deepEqual(e, {
        response: { data: { code: 'another 403 error' }, status: 403 },
      });
      assert.deepEqual(refreshStub.called, true, 'Refresh script called');
      __RewireAPI__.__ResetDependency__('axios');
      __RewireAPI__.__ResetDependency__('setError');
      __RewireAPI__.__ResetDependency__('event');
    }
  });
});

test('refresh-request, head', assert => {
  assert.plan(2);

  const sandbox = sinon.sandbox.create();

  __RewireAPI__.__Rewire__('axios', () =>
    Promise.reject({
      response: {
        status: 400,
        data: {
          code: 'another 400 error',
        },
      },
    })
  );

  const eventStub = sandbox.stub();
  __RewireAPI__.__Rewire__('event', eventStub);

  const setErrorStub = sandbox.stub();
  setErrorStub.returns({ type: 'foo' });

  __RewireAPI__.__Rewire__('setError', setErrorStub);

  co(function*() {
    try {
      yield head('http://prettydecent.com', 'Bearer', {});
    } catch (e) {
      assert.deepEqual(e, {
        response: { data: { code: 'another 400 error' }, status: 400 },
      });

      assert.deepEqual(
        eventStub.args,
        [
          [
            {
              action: 'Response code: 400 url: http://prettydecent.com',
              category: 'HttpRequestError',
            },
          ],
        ],
        'logs 401 error'
      );

      __RewireAPI__.__ResetDependency__('axios');
      __RewireAPI__.__ResetDependency__('setError');
      __RewireAPI__.__ResetDependency__('event');
    }
  });
});
