import R from 'ramda';

export default (content, investmentGoal) => {
  const iconTypes = content.investmentGoals.types;
  const matchingType = R.find(R.propEq('value', investmentGoal.goal))(
    iconTypes
  );

  return matchingType
    ? {
        ...investmentGoal,
        icon: matchingType.image,
      }
    : investmentGoal;
};
