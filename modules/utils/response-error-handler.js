import R from 'ramda';

export default R.either(R.path(['response', 'data']), R.identity);
