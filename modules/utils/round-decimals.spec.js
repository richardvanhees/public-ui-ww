const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

test('that rounding is correct', t => {
  const rounder = require('./round-decimals');

  t.equals(rounder(0.567, 1), 0.6);
  t.equals(rounder(0.567), 0.57);
  t.equals(rounder(0.567, 3), 0.567);
  t.equals(rounder(3678.8399999999992), 3678.84);

  t.end();
});
