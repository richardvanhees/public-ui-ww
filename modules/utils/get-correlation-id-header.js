import randomize from 'randomatic';

export default () => `${window.WealthWizards.UUID}-${randomize('Aa0', 12)}`;
