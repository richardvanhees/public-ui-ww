import request from 'axios';
import WealthWizards from '../../modules/wealthwizards';

const requestWrapper = (method, { url, route, data, auth, headers }) => {
  const baseUrl = `${WealthWizards.CONTEXT_ROUTE}`;
  const newUrl = url || baseUrl + route;

  return request({
    method,
    url: newUrl,
    data,
    json: true,
    headers: {
      ...headers,
      Authorization: auth,
      'content-type': 'application/json',
      Accept: 'application/json',
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
  });
};

export const get = data =>
  typeof data === 'string'
    ? requestWrapper('get', { route: data })
    : requestWrapper('get', data); // eslint-disable-line
export const post = data => requestWrapper('post', data);
export const putRequest = data => requestWrapper('put', data);
