export default (blob) => {
  if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    window.navigator.msSaveOrOpenBlob(blob, `Report_${new Date().getTime()}.pdf`);
    return;
  }
  window.location.assign(window.URL.createObjectURL(blob));
};
