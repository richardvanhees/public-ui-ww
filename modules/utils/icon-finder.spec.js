import test from 'tape';
import iconFinder from './icon-finder';

test('Icon finder', t => {
  t.test('Find icon', t => {
    t.plan(1);
    const fakeContent = {
      investmentGoals: {
        types: [
          {
            value: 'value',
            title: 'Fake type',
            image: 'fake-icon',
          },
        ],
      },
    };
    const fakeInvestmentGoal = {
      goal: 'value',
    };
    const result = iconFinder(fakeContent, fakeInvestmentGoal);

    const expectedNewContent = { ...fakeInvestmentGoal, icon: 'fake-icon' };

    t.deepEqual(result, expectedNewContent, 'Icon found');
    t.end();
  });

  t.test('Find icon', t => {
    t.plan(1);
    const fakeContent = {
      investmentGoals: {
        types: [
          {
            title: 'Fake type 2',
            image: 'fake-icon',
          },
        ],
      },
    };
    const fakeInvestmentGoal = {
      goal: 'Fake type',
    };
    const result = iconFinder(fakeContent, fakeInvestmentGoal);

    const expectedNewContent = { ...fakeInvestmentGoal };

    t.deepEqual(result, expectedNewContent, 'Icon not found');
    t.end();
  });
});
