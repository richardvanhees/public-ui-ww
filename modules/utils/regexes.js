export const POSTCODE_REGEX = /^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/;

export const emailIsValid = (email, maxLength = 60) =>
  email &&
  email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/) &&
  email.length <= maxLength;

export const ADDRESS_LINE_REGEX = /^[a-zA-Z0-9 '\-\/,]{1,30}$/;

export const validAddressLine = addressLine =>
  addressLine && addressLine.match(ADDRESS_LINE_REGEX);

export const optionalValidAddressLine = addressLine =>
  addressLine ? addressLine.match(ADDRESS_LINE_REGEX) : true;
