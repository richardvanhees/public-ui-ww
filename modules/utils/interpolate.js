import reactHTMLParser from 'react-html-parser';
import R from 'ramda';

const replacer = (string, rules) => {
  let newString = string;

  if (rules) {
    R.toPairs(rules).forEach(rule => {
      const value = rule[1];
      const regex = new RegExp(`{{${rule[0]}}}`, 'g');

      newString = newString.replace(
        regex,
        R.is(Number, value) ? value.toLocaleString() : value
      );
    });
  }

  newString = newString && reactHTMLParser(newString);
  return newString.length === 1 ? newString[0] : newString;
};

const interpolate = (content, rules) => {
  if (R.is(String, content)) {
    return replacer(content, rules);
  }

  if (R.is(Array, content)) {
    return content.map(item => interpolate(item, rules));
  }

  if (R.is(Object, content)) {
    const newContent = {};
    Object.keys(content).forEach(key => {
      newContent[key] = interpolate(content[key], rules);
    });
    return newContent;
  }

  return content;
};

export default interpolate;
