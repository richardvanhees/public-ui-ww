import WealthWizards from '../wealthwizards';

export default () => {
  const token = localStorage.getItem(WealthWizards.TOKEN_KEY_NAME);
  if (!token) return null;

  return JSON.parse(token).jwt;
};
