export default browser =>
  browser.breakpoint === 'stablet' || browser.breakpoint === 'phablet';
