module.exports = (number, sig = 3) => {
  const multiplier = Math.pow(
    10,
    sig - Math.floor(Math.log(number) / Math.LN10) - 1
  );
  return Math.floor(number * multiplier) / multiplier;
};
