import * as Cookies from 'js-cookie';

export const isLocalStorageSupported = () => {
  const testKey = 'test_ww';
  try {
    localStorage.setItem(testKey, '1');
    localStorage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
};

export const setItem = (key, data) => {
  if (isLocalStorageSupported()) {
    localStorage.setItem(key, data);
  } else {
    Cookies.set(key, data);
  }
};

export const removeItem = key => {
  if (isLocalStorageSupported()) {
    localStorage.removeItem(key);
  } else {
    Cookies.remove(key);
  }
};

export const getItem = key => {
  if (isLocalStorageSupported()) {
    return localStorage.getItem(key);
  }
  return Cookies.get(key);
};
