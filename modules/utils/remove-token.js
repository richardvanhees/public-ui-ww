import WealthWizards from '../wealthwizards';
import { removeItem } from './local-storage';

export default () => removeItem(WealthWizards.TOKEN_KEY_NAME);
