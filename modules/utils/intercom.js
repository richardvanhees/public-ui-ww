import WealthWizards from '../../modules/wealthwizards';

const buildIntercomUserData = userData => ({
  app_id: WealthWizards.INTERCOM_APP_ID,
  email: userData.email_address,
  name: `${userData.first_name} ${userData.last_name}`,
  company: {
    id: userData.employer,
    name: userData.employer,
  },
  user_hash: userData.intercom_identity,
  user_id: userData._id,
});

export const boot = args =>
  window.Intercom && window.Intercom('boot', buildIntercomUserData(args));
export const show = () => window.Intercom && window.Intercom('show');
export const shutdown = () => window.Intercom && window.Intercom('shutdown');
export const update = args =>
  window.Intercom && window.Intercom('update', buildIntercomUserData(args));
