import test from 'tape';
import { emailIsValid, validAddressLine } from './regexes';

test('emailIsValid', assert => {
  assert.true(
    emailIsValid(
      '12345@12345678901234567890123456789012345678901234567890.com'
    ),
    '60 chars'
  );

  assert.end();
});

test('emailIsValid', assert => {
  assert.false(
    emailIsValid(
      '123456@12345678901234567890123456789012345678901234567890.com'
    ),
    '61 chars'
  );

  assert.end();
});

test('validAddressLine', assert => {
  assert.true(
    validAddressLine('123456789012345678901234567890'),
    '30 chars'
  );
  assert.end();
});

test('validAddressLine', assert => {
  assert.false(
    validAddressLine('1234567890123456789012345678901'),
    '31 chars'
  );
  assert.end();
});

test('validAddressLine', assert => {
  assert.false(
    validAddressLine('123456789^012345678901234567890123456'),
    'disallowed char'
  );
  assert.end();
});
