import axios from 'axios';
import WealthWizards from '../wealthwizards';
import { put } from 'redux-saga/effects';
import { setItem, getItem } from './local-storage';
import R from 'ramda';
import { event } from 'react-ga';

/**
 * The following actions were being imported from invesmtent, to make this highly useful module
 * reusable i've copied them in here for now
 */
const RESET_RISK = 'modules/FACT_FIND/RESET_RISK';
const LOGOUT = 'login/LOGOUT';

const logout = (redirectAfterLoginIfPossible = false, redirectUrl) => ({
  type: LOGOUT,
  redirectAfterLoginIfPossible,
  redirectUrl,
});

const makeRequest = (
  method,
  url,
  auth,
  data,
  responseType = 'json',
  callback
) => {
  const options = {
    method,
    url,
    data: data.data,
    json: true,
    responseType,
    headers: {
      Authorization: auth,
      'content-type': 'application/json',
      Accept: 'application/json',
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
    timeout: WealthWizards.CLIENT_REQUEST_TIMEOUT,
  };
  if (!R.isNil(callback)) return axios(options).then(callback);
  return axios(options);
};

function* refresh() {
  const refreshToken = getItem('refresh_token');

  if (!refreshToken) {
    throw new Error('No refresh token available');
  }

  const { data } = yield axios({
    method: 'post',
    url: `${WealthWizards.AUTH_SVC_URL}/v1/oauth/refresh-token`,
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
    data: {
      refresh_token: refreshToken,
    },
    timeout: WealthWizards.CLIENT_REQUEST_TIMEOUT,
  });

  setItem('user_token', data.jwt);
  setItem('refresh_token', data.refresh_token);
}

function* requestWrapper(method, url, auth, data, responseType, callback) {
  try {
    return yield makeRequest(method, url, auth, data, responseType, callback);
  } catch (e) {
    const statusCode = R.pathOr(0, ['response', 'status'])(e);

    if (statusCode === 401 || statusCode === 403) {
      event({
        category: 'HttpRequestAuthError',
        action: `Response code: ${statusCode} url: ${url}`,
      });
      try {
        yield refresh();
        yield put({ type: RESET_RISK });
      } catch (err) {
        // handle case where it has been refreshed by another
        // parallel request
        if (auth === `Bearer ${getItem('user_token')}`) {
          yield put(logout(true));
          throw e;
        }
      }
      return yield makeRequest(
        method,
        url,
        `Bearer ${getItem('user_token')}`,
        data,
        responseType,
        callback
      );
    }

    event({
      category: 'HttpRequestError',
      action: `Response code: ${statusCode} url: ${url}`,
    });

    throw e;
  }
}

module.exports = {
  get: (url, auth, data, responseType, callback) =>
    requestWrapper('get', url, auth, data, responseType, callback),
  head: (url, auth, callback) =>
    requestWrapper('head', url, auth, {}, callback),
  post: (url, auth, data, responseType, callback) =>
    requestWrapper('post', url, auth, data, responseType, callback),
  patch: (url, auth, data, responseType, callback) =>
    requestWrapper('patch', url, auth, data, responseType, callback),
  putRequest: (url, auth, data, responseType, callback) =>
    requestWrapper('put', url, auth, data, responseType, callback),
  manualTokenRefresh: refresh,
};
