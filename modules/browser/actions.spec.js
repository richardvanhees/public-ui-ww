import test from 'tape';
import * as actions from './actions';
import { browserTypes } from './types';

test('BrowserActions', t => {
  let result;

  result = actions.changeBreakpoint('desktop');
  t.deepEqual(
    result,
    {
      type: browserTypes.CHANGED_BREAKPOINT,
      payload: { breakpoint: 'desktop' },
    },
    'changeBreakpoint'
  );

  t.end();
});

test('BrowserActions', assert => {
  assert.plan(1);

  const result = actions.setIsLoading(true);
  assert.deepEqual(
    result,
    { type: browserTypes.SET_IS_LOADING, loading: true },
    'loading set to true'
  );
});

test('BrowserActions', assert => {
  assert.plan(1);

  const result = actions.setIsFullLoading(true);
  assert.deepEqual(
    result,
    { type: browserTypes.SET_IS_FULL_LOADING, fullLoading: true },
    'fullLoading set to true'
  );
});

test('BrowserActions', assert => {
  assert.plan(1);

  const result = actions.setAlert('fake alert');
  assert.deepEqual(
    result,
    { type: browserTypes.SET_ALERT, alert: 'fake alert' },
    'SET_ALERT'
  );
});

test('BrowserActions', assert => {
  assert.plan(1);

  const result = actions.resetAlert();
  assert.deepEqual(result, { type: browserTypes.RESET_ALERT }, 'RESET_ALERT');
});
