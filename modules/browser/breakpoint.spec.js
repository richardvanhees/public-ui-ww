import test from 'tape';
import breakpoint from './breakpoint';

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 741;
  assert.equal(breakpoint(), 'ltablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 1023;
  assert.equal(breakpoint(), 'ltablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 1024;
  assert.equal(breakpoint(), 'desktop');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 740;
  assert.equal(breakpoint(), 'tablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 0;
  global.window.innerWidth = 740;
  assert.equal(breakpoint(), 'tablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 641;
  assert.equal(breakpoint(), 'tablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 481;
  assert.equal(breakpoint(), 'stablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 640;
  assert.equal(breakpoint(), 'stablet');
});

test('breakpoint', assert => {
  assert.plan(1);
  global.window.outerWidth = 480;
  assert.equal(breakpoint(), 'phablet');
});
