export default () => {
  const viewWidth = window.outerWidth || window.innerWidth;
  let breakpoint;

  if (viewWidth < 1024 && viewWidth > 740) breakpoint = 'ltablet';
  else if (viewWidth <= 740 && viewWidth > 640) breakpoint = 'tablet';
  else if (viewWidth <= 640 && viewWidth > 480) breakpoint = 'stablet';
  else if (viewWidth <= 480) breakpoint = 'phablet';
  else breakpoint = 'desktop';

  return breakpoint;
};
