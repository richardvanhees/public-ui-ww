import test from 'tape';
import reducer from './reducer.js';
import { browserTypes } from './types';

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.CHANGED_BREAKPOINT,
      payload: { breakpoint: 'desktop' },
    }
  );
  t.deepEqual(result, {
    breakpoint: 'desktop'
  }, 'CHANGED_BREAKPOINT');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.SET_ERROR,
      error: true,
      text: 'errorMsg',
    }
  );
  t.deepEqual(result, {
    error: true,
    errorMsg: 'errorMsg'
  }, 'SET_ERROR');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.RESET_ERROR,
    }
  );
  t.deepEqual(result, {
    error: false,
    errorMsg: ''
  }, 'RESET_ERROR');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.SET_NOTIFICATION,
      notificationMessage: 'notification',
      notificationType: 'type',
      flash: false
    }
  );
  t.deepEqual(result, {
    notification: {
      text: 'notification',
      type: 'type',
      visible: true,
      flash: false
    }
  }, 'SET_NOTIFICATION');

  t.end();
});


test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.RESET_NOTIFICATION,
    }
  );
  t.deepEqual(result, {
    notification: {
      visible: false
    }
  }, 'RESET_NOTIFICATION');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.SET_ALERT,
      alert: 'alert'
    }
  );
  t.deepEqual(result, {
    alert: 'alert'
  }, 'SET_ALERT');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.RESET_ALERT,
    }
  );
  t.deepEqual(result, {
    alert: null
  }, 'RESET_ALERT');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.SET_IS_LOADING,
      loading: true,
    }
  );
  t.deepEqual(result, {
    loading: true
  }, 'SET_IS_LOADING');

  t.end();
});

test('BrowserReducer', t => {
  let result;

  result = reducer(
    {},
    {
      type: browserTypes.SET_IS_FULL_LOADING,
      fullLoading: true,
    }
  );
  t.deepEqual(result, {
    fullLoading: true
  }, 'SET_IS_FULL_LOADING');

  t.end();
});


