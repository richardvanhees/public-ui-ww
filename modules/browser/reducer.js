import { browserTypes } from './types';

const defaultNotification = {
  visible: false,
  text: '',
  type: '',
};

const initialState = {
  breakpoint: 'desktop',
  notification: defaultNotification,
  loading: false,
  fullLoading: false,
  errorMsg: '',
};

const reducer = {
  [browserTypes.CHANGED_BREAKPOINT]: (state, action) => ({
    ...state,
    breakpoint: action.payload.breakpoint,
  }),
  [browserTypes.SET_ERROR]: (state, { text, error }) => ({
      ...state,
      error,
      errorMsg: text,
    }),
  [browserTypes.RESET_ERROR]: state => ({
    ...state,
    error: false,
    errorMsg: '',
  }),
  [browserTypes.SET_NOTIFICATION]: (state, action) => ({
    ...state,
    notification: {
      text: action.notificationMessage,
      type: action.notificationType,
      flash: action.flash,
      visible: true,
    },
  }),
  [browserTypes.RESET_NOTIFICATION]: state => ({
    ...state,
    notification: {
      ...state.notification,
      visible: false,
    },
  }),
  [browserTypes.SET_ALERT]: (state, { alert }) => ({
    ...state,
    alert,
  }),
  [browserTypes.RESET_ALERT]: state => ({
    ...state,
    alert: null,
  }),
  [browserTypes.SET_IS_LOADING]: (state, action) => ({
    ...state,
    loading: action.loading,
  }),
  [browserTypes.SET_IS_FULL_LOADING]: (state, action) => ({
    ...state,
    fullLoading: action.fullLoading,
  }),
};

export default (state = initialState, action = {}) =>
  reducer[action.type] ? reducer[action.type](state, action) : state;
