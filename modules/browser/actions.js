import { browserTypes } from './types';

export const changeBreakpoint = breakpoint => ({
  type: browserTypes.CHANGED_BREAKPOINT,
  payload: { breakpoint },
});

export const setError = errorMsg => ({
  type: browserTypes.SET_ERROR,
  text: errorMsg,
});

export const setNotification = (notificationMessage, notificationType, flash = false) => ({
  type: browserTypes.SET_NOTIFICATION,
  notificationMessage,
  notificationType,
  flash,
});

export const showDownloadNotification = () => ({
  type: browserTypes.SET_NOTIFICATION,
  notificationMessage: 'Please download your report before paying into your investment.',
  notificationType: 'warning',
  flash: 5000,
});

export const resetNotification = () => ({
  type: browserTypes.RESET_NOTIFICATION,
});

export const setAlert = alert => ({
  type: browserTypes.SET_ALERT,
  alert,
});

export const resetError = () => ({
  type: browserTypes.RESET_ERROR,
});

export const resetAlert = () => ({
  type: browserTypes.RESET_ALERT,
});

export const setIsLoading = loading => ({
  type: browserTypes.SET_IS_LOADING,
  loading,
});

export const setIsFullLoading = fullLoading => ({
  type: browserTypes.SET_IS_FULL_LOADING,
  fullLoading,
});
