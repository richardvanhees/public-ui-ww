import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import getContentRequestSaga, {
  getContentRequest,
  getContent,
  __RewireAPI__,
} from './get-content-request-saga';
import { getTenantContentTypeContent, getContentFromState } from '../selectors';
import {
  GET_CONTENT_REQUEST,
  getContentRequestSuccess,
  getContentRequestFailure,
  updateContent,
} from '../actions';
import { getFactFindRequest } from '../../../src/investment/modules/fact-find/actions';

test('getContentRequestSaga - Happy Day', assert => {
  assert.plan(1);

  const fakeContentType = 'fake content type';
  const fakeVersion = 'fake version';
  const fakeData = {
    [fakeContentType]: {
      version: fakeVersion,
    },
  };
  const fakeData2 = {
    version: fakeVersion,
  };
  const fakeResponse = {
    data: fakeData2,
    headers: {
      'content-type': fakeContentType,
    },
  };

  const target = testSaga(getContentRequest, { contentType: fakeContentType })
    .next()
    .select(getContentFromState)
    .next({})
    .call(getContent)
    .next(fakeResponse)
    .put.resolve(
      updateContent({ 'fake content type': { version: 'fake version' } })
    )
    .next()
    .put(getContentRequestSuccess())
    .next()
    .put(getFactFindRequest())
    .next();

  assert.true(
    target.isDone(),
    'getContentRequestSaga should GET content and put a success action on success'
  );
});

test('getContentRequestSaga - Happy Day with caching', assert => {
  assert.plan(1);

  const fakeContentType = 'fake content type';
  const fakeVersion = 'fake version';
  const fakeData = {
    [fakeContentType]: {
      version: fakeVersion,
    },
  };
  const fakeData2 = {
    version: fakeVersion,
  };
  const fakeResponse = {
    data: fakeData2,
    headers: {
      'content-type': fakeContentType,
    },
  };

  const target = testSaga(getContentRequest, { contentType: fakeContentType })
    .next()
    .select(getContentFromState)
    .next({ foo: 'test content' })
    .put(getContentRequestSuccess())
    .next()
    .put(getFactFindRequest())
    .next();

  assert.true(
    target.isDone(),
    'getContentRequestSaga should GET content and put a success action on success'
  );
});

test('getContentRequestSaga - Sad Day', assert => {
  assert.plan(1);

  const fakeContentType = 'fake content type';
  const fakeError = 'fake error';
  const fakeErrorToHandle = { error: fakeError };

  __RewireAPI__.__Rewire__('responseErrorHandler', err => err.error);

  const target = testSaga(getContentRequest, { contentType: fakeContentType })
    .next()
    .select(getContentFromState)
    .next({})
    .call(getContent)
    .throw(fakeErrorToHandle)
    .put(getContentRequestFailure(fakeError))
    .next();

  assert.true(
    target.isDone(),
    'getContentRequestSaga should GET content and put a failure action with the error on failure'
  );

  __RewireAPI__.__ResetDependency__('responseErrorHandler');
});

test('getContentRequestSaga', assert => {
  assert.plan(1);

  const fakeContentType = 'fake content type';
  const fakeContentUrl = 'fake context route';
  const fakeTenantKey = 'fake tenant key';
  const fakeVersion = 'v2.0';
  const fakeToken = { jwt: 'jwt' };
  const fakeCorrelationId = 'fake correlation id';
  const fakeTokenKeyName = 'fake token key name';

  localStorage.setItem(fakeTokenKeyName, fakeToken);

  __RewireAPI__.__Rewire__('WealthWizards', {
    CONTENT_SERVICE_BASE_URL: fakeContentUrl,
    CONTENT_SERVICE_VERSION: fakeVersion,
    TENANT_KEY: fakeTenantKey,
    TOKEN_KEY_NAME: fakeTokenKeyName,
  });

  __RewireAPI__.__Rewire__('getToken', () => fakeToken);

  __RewireAPI__.__Rewire__('getCorrelationIdHeader', () => fakeCorrelationId);

  __RewireAPI__.__Rewire__('axios', opts => {
    const actual = opts;
    const expected = {
      method: 'get',
      url: `${fakeContentUrl}/content-service/${fakeVersion}/content/investment-wizard`,
      headers: {
        tenant: 'fake tenant key',
      },
    };

    assert.deepEqual(
      actual,
      expected,
      'getContentRequestSaga getContent should call axios with expected options'
    );

    return Promise.resolve();
  });

  const target = getContent;

  target(fakeContentType).then(() => {
    localStorage.removeItem('token');

    __RewireAPI__.__ResetDependency__('getCorrelationIdHeader');
    __RewireAPI__.__ResetDependency__('axios');
    __RewireAPI__.__ResetDependency__('WealthWizards');
    __RewireAPI__.__ResetDependency__('getToken');
  });
});
