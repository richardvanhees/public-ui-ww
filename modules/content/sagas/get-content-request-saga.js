import { select, call, put, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../wealthwizards';
import axios from 'axios';
import { responseErrorHandler } from '../../utils';
import {
  GET_CONTENT_REQUEST,
  getContentRequestSuccess,
  getContentRequestFailure,
  updateContent,
} from '../actions';
import { getContentFromState } from '../selectors';
import R from 'ramda';
import { getFactFindRequest } from '../../../src/investment/modules/fact-find/actions';

export const getContent = () =>
  axios({
    method: 'get',
    url: `${WealthWizards.CONTENT_SERVICE_BASE_URL}/content-service/${
      WealthWizards.CONTENT_SERVICE_VERSION
    }/content/investment-wizard`,
    headers: {
      tenant: WealthWizards.TENANT_KEY,
    },
  });

export function* getContentRequest({ contentType }) {
  try {
    const existingContent = yield select(getContentFromState);
    if (R.isEmpty(existingContent)) {
      const { data } = yield call(getContent);
      yield put.resolve(updateContent({ [contentType]: data }));
    }

    yield put(getContentRequestSuccess());

    yield put(getFactFindRequest());
  } catch (error) {
    yield put(getContentRequestFailure(responseErrorHandler(error)));
  }
}

export default function* getContentRequestSaga() {
  yield takeLatest(GET_CONTENT_REQUEST, getContentRequest);
}
