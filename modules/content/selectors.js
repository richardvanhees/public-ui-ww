import R from 'ramda';

export const getTenantContentTypeContent = R.curry((contentType, state) =>
  R.pathOr({}, ['content', 'data', contentType], state)
);

export const lookContentByKey = (key, content) => content.data[key];

export const getContentFromState = state => state.content.data;
