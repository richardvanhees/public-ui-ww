import test from 'tape';
import { getTenantContentTypeContent, getContentFromState } from './selectors';

test('Module content selectors', t => {
  const testCases = [
    {
      description: 'Returns empty object when undefined in state',
      contentType: 'fakeContentType',
      state: {},
      expected: {},
    },
    {
      description: 'Returns empty object when undefined in state',
      contentType: 'fakeContentType',
      state: { content: {} },
      expected: {},
    },
    {
      description: 'Returns empty object when undefined in state',
      contentType: 'fakeContentType',
      state: { content: { data: {} } },
      expected: {},
    },
    {
      description: 'Returns object when in state',
      contentType: 'fakeContentType',
      state: { content: { data: { fakeContentType: 'some content' } } },
      expected: 'some content',
    },
  ];

  testCases.forEach(({ description, contentType, state, expected }) => {
    t.test('getTenantContentTypeContent', assert => {
      assert.plan(1);

      const target = getTenantContentTypeContent;
      const actual = target(contentType, state);

      assert.deepEquals(actual, expected, description);
    });
  });
});


test('getContentFromState - Content is returned', assert => {
  assert.plan(1);

  const expected = {data: {foo: "bar"}};
  const state = {content: expected};
  const target = getContentFromState(state);

  assert.deepEqual(target, {foo: "bar"}, 'it returns content from state');
});
