import test from 'tape';
import * as actions from './actions';

test('Module content actions', assert => {
  assert.plan(1);

  const fakeContent = 'fake content';

  const actual = actions.updateContent(fakeContent);
  const expected = { type: actions.UPDATE_CONTENT, content: fakeContent };

  assert.deepEqual(actual, expected, 'updateContent');
});

test('Module content actions', assert => {
  assert.plan(1);

  const fakeContentType = 'fake type';

  const actual = actions.getContentRequest(fakeContentType);
  const expected = {
    type: actions.GET_CONTENT_REQUEST,
    contentType: fakeContentType,
  };

  assert.deepEqual(actual, expected, 'getContentRequest');
});

test('Module content actions', assert => {
  assert.plan(1);

  const actual = actions.getContentRequestSuccess();
  const expected = { type: actions.GET_CONTENT_REQUEST_SUCCESS };

  assert.deepEqual(actual, expected, 'getContentRequestSuccess');
});

test('Module content actions', assert => {
  assert.plan(1);

  const fakeError = 'fake error';

  const actual = actions.getContentRequestFailure(fakeError);
  const expected = {
    type: actions.GET_CONTENT_REQUEST_FAILURE,
    error: fakeError,
  };

  assert.deepEqual(actual, expected, 'getContentRequestFailure');
});
