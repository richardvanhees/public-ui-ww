import { connect } from 'react-redux';
import { compose, branch, renderComponent } from 'recompose';
import withLifecycle from '@hocs/with-lifecycle';
import R from 'ramda';
import { getContentRequest } from '../actions';
import { getTenantContentTypeContent } from '../selectors';
import Spinner from '../../elements/Spinner';
import React from 'react';

const mapStateToProps = R.curry((contentType, state) => ({
  content: getTenantContentTypeContent(contentType, state),
}));

const mapDispatchToProps = R.curry((contentType, dispatch) => ({
  getContent() {
    return dispatch(getContentRequest(contentType));
  },
}));

const hasNoContent = props => !props.content || R.isEmpty(props.content);

const VisibleSpinner = () => <Spinner visible />;

export default contentType =>
  compose(
    connect(mapStateToProps(contentType), mapDispatchToProps(contentType)),
    withLifecycle({
      onDidMount({ getContent }) {
        getContent();
      },
    }),
    branch(hasNoContent, renderComponent(VisibleSpinner))
  );
