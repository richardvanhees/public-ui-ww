import test from 'tape';
import deepFreeze from 'deep-freeze';
import {
  GET_CONTENT_REQUEST,
  GET_CONTENT_REQUEST_SUCCESS,
  GET_CONTENT_REQUEST_FAILURE,
  UPDATE_CONTENT,
} from './actions';
import contentReducer from './reducers';

test('Module content reducers', assert => {
  assert.plan(1);

  const fakeState = 'a fake state';
  const target = contentReducer;

  const actual = target(fakeState, { type: 'cheese' });
  const expected = fakeState;

  assert.equal(
    actual,
    expected,
    'should return the state when no reducer is defined'
  );
});

test('Module content reducers', assert => {
  assert.plan(1);

  const fakeState = deepFreeze({ some: 'state' });
  const fakeAction = {
    type: GET_CONTENT_REQUEST,
    contentTypes: 'fakeContentType',
  };

  const actual = contentReducer(fakeState, fakeAction);
  const expected = {
    ...fakeState,
    isDownloading: true,
  };

  assert.deepEqual(
    actual,
    expected,
    'getContentRequest should set isDownloading to true'
  );
});

test('Module content reducers', assert => {
  assert.plan(1);

  const fakeState = deepFreeze({ some: 'state' });
  const fakeAction = {
    type: GET_CONTENT_REQUEST_SUCCESS,
  };

  const actual = contentReducer(fakeState, fakeAction);
  const expected = {
    ...fakeState,
    isDownloading: false,
  };

  assert.deepEqual(
    actual,
    expected,
    'getContentRequestSuccess should set isDownloading to false'
  );
});

test('Module content reducers', t => {
  const testCases = [
    {
      description: 'should set new content into empty state',
      state: deepFreeze({ some: 'state', data: {} }),
      action: {
        type: UPDATE_CONTENT,
        content: { fakeContentType: 'fake content' },
      },
      expected: { some: 'state', data: { fakeContentType: 'fake content' } },
    },
    {
      description:
        'should merge new content into state, not altering existing content',
      state: deepFreeze({
        some: 'state',
        data: { anotherContentType: 'another content' },
      }),
      action: {
        type: UPDATE_CONTENT,
        content: { fakeContentType: 'fake content' },
      },
      expected: {
        some: 'state',
        data: {
          anotherContentType: 'another content',
          fakeContentType: 'fake content',
        },
      },
    },
    {
      description: 'should over-ride existing content with new content',
      state: deepFreeze({
        some: 'state',
        data: { fakeContentType: 'old content' },
      }),
      action: {
        type: UPDATE_CONTENT,
        content: { fakeContentType: 'fake content' },
      },
      expected: { some: 'state', data: { fakeContentType: 'fake content' } },
    },
  ];

  testCases.forEach(({ description, state, action, expected }) => {
    t.test('updateContent', assert => {
      assert.plan(1);

      const target = contentReducer;
      const actual = target(state, action);

      assert.deepEqual(actual, expected, description);
    });
  });
});

test('Module content reducers', assert => {
  assert.plan(1);

  const fakeState = deepFreeze({ some: 'state' });
  const fakeError = 'fake error';
  const fakeAction = {
    type: GET_CONTENT_REQUEST_FAILURE,
    error: fakeError,
  };

  const actual = contentReducer(fakeState, fakeAction);
  const expected = {
    ...fakeState,
    isDownloading: false,
    error: fakeError,
  };

  assert.deepEqual(
    actual,
    expected,
    'getContentRequestFailure should set isDownloading to false and set error'
  );
});
