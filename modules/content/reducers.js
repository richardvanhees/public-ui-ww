import R from 'ramda';
import {
  GET_CONTENT_REQUEST,
  GET_CONTENT_REQUEST_SUCCESS,
  GET_CONTENT_REQUEST_FAILURE,
  UPDATE_CONTENT,
} from './actions';
import factFindTypes from '../../src/investment/modules/fact-find/types';

const getContentRequest = state => ({
  ...state,
  isDownloading: true,
});

const getContentRequestSuccess = state => ({
  ...state,
  isDownloading: false,
});

const updateContent = (state, { content }) =>
  R.evolve(
    {
      data: R.merge(R.__, content),
    },
    state
  );

const getContentRequestFailure = (state, { error }) => ({
  ...state,
  isDownloading: false,
  error,
});

const reducers = {
  [GET_CONTENT_REQUEST]: getContentRequest,
  [GET_CONTENT_REQUEST_SUCCESS]: getContentRequestSuccess,
  [GET_CONTENT_REQUEST_FAILURE]: getContentRequestFailure,
  [UPDATE_CONTENT]: updateContent,
  [factFindTypes.INITIAL_LOAD]: updateContent,
};

const INITIAL_STATE = {
  isDownloading: false,
  data: {},
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  const fn = reducers[action.type];
  return fn ? fn(state, action) : state;
}
