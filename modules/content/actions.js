export const UPDATE_CONTENT = 'modules/content/UPDATE_CONTENT';
export const GET_CONTENT_REQUEST = 'modules/content/GET_CONTENT_REQUEST';
export const GET_CONTENT_REQUEST_SUCCESS =
  'modules/content/GET_CONTENT_REQUEST_SUCCESS';
export const GET_CONTENT_REQUEST_FAILURE =
  'modules/content/GET_CONTENT_REQUEST_FAILURE';

export const updateContent = content => ({
  type: UPDATE_CONTENT,
  content,
});

export const getContentRequest = contentType => ({
  type: GET_CONTENT_REQUEST,
  contentType,
});

export const getContentRequestSuccess = () => ({
  type: GET_CONTENT_REQUEST_SUCCESS,
});

export const getContentRequestFailure = error => ({
  type: GET_CONTENT_REQUEST_FAILURE,
  error,
});
