export const SEND_POSTCODE_SEARCH_REQUEST =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_REQUEST';
export const SEND_POSTCODE_SEARCH_SUCCESS =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_SUCCESS';
export const SEND_POSTCODE_SEARCH_FAILURE =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_FAILURE';
export const SEND_POSTCODE_SEARCH_SELECT =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_SELECT';
export const SEND_POSTCODE_SEARCH_MAP_FIELDS =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_MAP_FIELDS';
export const SEND_POSTCODE_SEARCH_RESET =
  'ww-postcode-anywhere/SEND_POSTCODE_SEARCH_RESET';

export const sendPostcodeSearchRequest = (stateKey, postcodeSelector) => ({
  type: SEND_POSTCODE_SEARCH_REQUEST,
  stateKey,
  postcodeSelector,
});

export const sendPostcodeSearchSuccess = (
  stateKey,
  sendPostcodeSearchResponse
) => ({
  type: SEND_POSTCODE_SEARCH_SUCCESS,
  stateKey,
  sendPostcodeSearchResponse,
});

export const sendPostcodeSearchFailure = (stateKey, error) => ({
  type: SEND_POSTCODE_SEARCH_FAILURE,
  stateKey,
  error,
});

export const sendPostcodeSearchSelect = (
  stateKey,
  addressDetails,
  subStateKey,
  addressIndex
) => ({
  type: SEND_POSTCODE_SEARCH_SELECT,
  stateKey,
  addressDetails,
  subStateKey,
  addressIndex,
});

export const sendPostcodeSearchMapFields = (
  stateKey,
  fieldMappings,
  subStateKey,
  addressIndex
) => ({
  type: SEND_POSTCODE_SEARCH_MAP_FIELDS,
  stateKey,
  fieldMappings,
  subStateKey,
  addressIndex,
});

export const sendPostcodeSearchReset = stateKey => ({
  type: SEND_POSTCODE_SEARCH_RESET,
  stateKey,
});
