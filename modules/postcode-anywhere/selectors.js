import R from 'ramda';

export const getIsSearchingPostcode = (state, stateKey) =>
  R.pathOr(false, ['postcodeAnywhere', stateKey, 'isSearchingPostcode'], state);

export const getSendPostcodeSearchResponse = (state, stateKey) =>
  R.pathOr(
    undefined,
    ['postcodeAnywhere', stateKey, 'sendPostcodeSearchResponse'],
    state
  );

export const getApplicantPostcode = R.path([
  'factFind',
  'data',
  'personal__postcode',
]);

export const getDependantPostcode = R.path([
  'factFind',
  'data',
  'dependant__postcode',
]);
