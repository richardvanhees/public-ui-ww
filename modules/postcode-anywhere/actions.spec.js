import test from 'tape';
import * as actions from './actions';

test('Module postcode-anywhere actions', t => {
  t.test('sendPostcodeSearchRequest', assert => {
    assert.plan(1);

    const fakeSelector = 'fake selector';
    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchRequest(
      fakeStateKey,
      fakeSelector
    );

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_REQUEST,
      postcodeSelector: fakeSelector,
      stateKey: fakeStateKey,
    });
  });

  t.test('sendPostcodeSearchDone', assert => {
    assert.plan(1);

    const fakeSendPostcodeSearchResponse = [{ someFakeKey: 'fake value' }];
    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchSuccess(
      fakeStateKey,
      fakeSendPostcodeSearchResponse
    );

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_SUCCESS,
      sendPostcodeSearchResponse: fakeSendPostcodeSearchResponse,
      stateKey: fakeStateKey,
    });
  });

  t.test('sendPostcodeSearchFailure', assert => {
    assert.plan(1);

    const fakeError = {
      message: 'fake error message',
    };
    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchFailure(fakeStateKey, fakeError);

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_FAILURE,
      error: fakeError,
      stateKey: fakeStateKey,
    });
  });

  t.test('sendPostcodeSearchSelect', assert => {
    assert.plan(1);

    const fakeAddressDetails = {
      id: 'fake id',
      type: 'fake type',
    };
    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchSelect(
      fakeStateKey,
      fakeAddressDetails,
      'subStateKey',
      0
    );

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_SELECT,
      addressDetails: fakeAddressDetails,
      stateKey: fakeStateKey,
      subStateKey: 'subStateKey',
      addressIndex: 0,
    });
  });

  t.test('sendPostcodeSearchMapFields', assert => {
    assert.plan(1);

    const fakeFieldMappings = {
      personal__postcode: 'TE57 1NG',
    };
    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchMapFields(
      fakeStateKey,
      fakeFieldMappings,
      'subStateKey',
      0
    );

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_MAP_FIELDS,
      fieldMappings: fakeFieldMappings,
      stateKey: fakeStateKey,
      subStateKey: 'subStateKey',
      addressIndex: 0,
    });
  });

  t.test('sendPostcodeSearchReset', assert => {
    assert.plan(1);

    const fakeStateKey = 'fake state key';
    const result = actions.sendPostcodeSearchReset(fakeStateKey);

    assert.deepEqual(result, {
      type: actions.SEND_POSTCODE_SEARCH_RESET,
      stateKey: fakeStateKey,
    });
  });
});
