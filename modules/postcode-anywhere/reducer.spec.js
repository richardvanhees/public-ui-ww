import test from 'tape';
import deepFreeze from 'deep-freeze';
import {
  SEND_POSTCODE_SEARCH_REQUEST,
  SEND_POSTCODE_SEARCH_SUCCESS,
  SEND_POSTCODE_SEARCH_FAILURE,
  SEND_POSTCODE_SEARCH_SELECT,
  SEND_POSTCODE_SEARCH_RESET,
} from './actions';
import postcodeAnywhereReducer from './reducer';

test('Module postcode-anywhere reducers', t => {
  t.test('should return the UNDEFINED when no reducer is defined', assert => {
    assert.plan(1);

    const fake = 'a fake state';
    const target = postcodeAnywhereReducer;
    const result = target(fake, { type: 'cheese' });

    assert.equal(result.isSearchingPostcode, undefined);
  });

  t.test(
    'sendPostcodeSearchRequest: should set isSearchingPostcode to true',
    t => {
      const testCases = [
        {
          state: deepFreeze({ applicant: { isSearchingPostcode: false } }),
          action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'applicant' },
        },
        {
          state: deepFreeze({ dependant: { isSearchingPostcode: false } }),
          action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'dependant' },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: { isSearchingPostcode: true },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test('sendPostcodeSearchRequest: should omit error', t => {
    const testCases = [
      {
        state: deepFreeze({ applicant: { error: 'fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'applicant' },
      },
      {
        state: deepFreeze({ dependant: { error: 'fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'dependant' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.action.stateKey, assert => {
        assert.plan(1);
        const target = postcodeAnywhereReducer;
        const expected = {
          [testCase.action.stateKey]: { isSearchingPostcode: true },
        };
        const actual = target(testCase.state, testCase.action);
        assert.deepEqual(actual, expected);
      });
    });
  });

  t.test(
    'sendPostcodeSearchRequest: should omit sendPostcodeSearchResponse',
    t => {
      const testCases = [
        {
          state: deepFreeze({
            applicant: { sendPostcodeSearchResponse: 'fake error' },
          }),
          action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'applicant' },
        },
        {
          state: deepFreeze({
            dependant: { sendPostcodeSearchResponse: 'fake error' },
          }),
          action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'dependant' },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: { isSearchingPostcode: true },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test('sendPostcodeSearchRequest: should omit addressDetails', t => {
    const testCases = [
      {
        state: deepFreeze({ applicant: { addressDetails: 'fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'applicant' },
      },
      {
        state: deepFreeze({ dependant: { addressDetails: 'fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_REQUEST, stateKey: 'dependant' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.action.stateKey, assert => {
        assert.plan(1);
        const target = postcodeAnywhereReducer;
        const expected = {
          [testCase.action.stateKey]: { isSearchingPostcode: true },
        };
        const actual = target(testCase.state, testCase.action);
        assert.deepEqual(actual, expected);
      });
    });
  });

  t.test(
    'sendPostcodeSearchSuccess: should set isSearchingPostcode to false and set response',
    t => {
      const testCases = [
        {
          state: deepFreeze({ applicant: { isSearchingPostcode: true } }),
          action: {
            type: SEND_POSTCODE_SEARCH_SUCCESS,
            stateKey: 'applicant',
            sendPostcodeSearchResponse: 'fake search response',
          },
        },
        {
          state: deepFreeze({ dependant: { isSearchingPostcode: true } }),
          action: {
            type: SEND_POSTCODE_SEARCH_SUCCESS,
            stateKey: 'dependant',
            sendPostcodeSearchResponse: 'fake search response',
          },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: {
              isSearchingPostcode: false,
              sendPostcodeSearchResponse:
                testCase.action.sendPostcodeSearchResponse,
            },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test(
    'sendPostcodeSearchFailure: should set isSearchingPostcode to false and set error',
    t => {
      const testCases = [
        {
          state: deepFreeze({ applicant: { isSearchingPostcode: true } }),
          action: {
            type: SEND_POSTCODE_SEARCH_FAILURE,
            stateKey: 'applicant',
            error: 'fake error',
          },
        },
        {
          state: deepFreeze({ dependant: { isSearchingPostcode: true } }),
          action: {
            type: SEND_POSTCODE_SEARCH_FAILURE,
            stateKey: 'dependant',
            error: 'fake error',
          },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: {
              isSearchingPostcode: false,
              error: testCase.action.error,
            },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test('sendPostcodeSearchSelect: should set addressDetails', t => {
    const testCases = [
      {
        state: deepFreeze({ applicant: {} }),
        action: {
          type: SEND_POSTCODE_SEARCH_SELECT,
          stateKey: 'applicant',
          addressDetails: 'fake address details',
        },
      },
      {
        state: deepFreeze({ dependant: {} }),
        action: {
          type: SEND_POSTCODE_SEARCH_SELECT,
          stateKey: 'dependant',
          addressDetails: 'fake address details',
        },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.action.stateKey, assert => {
        assert.plan(1);
        const target = postcodeAnywhereReducer;
        const expected = {
          [testCase.action.stateKey]: {
            addressDetails: testCase.action.addressDetails,
          },
        };
        const actual = target(testCase.state, testCase.action);
        assert.deepEqual(actual, expected);
      });
    });
  });

  t.test(
    'sendPostcodeSearchReset: should set isSearchingPostcode to false',
    t => {
      const testCases = [
        {
          state: deepFreeze({ applicant: { isSearchingPostcode: true } }),
          action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'applicant' },
        },
        {
          state: deepFreeze({ dependant: { isSearchingPostcode: true } }),
          action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'dependant' },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: { isSearchingPostcode: false },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test('sendPostcodeSearchReset: should omit error', t => {
    const testCases = [
      {
        state: deepFreeze({ applicant: { error: ' fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'applicant' },
      },
      {
        state: deepFreeze({ dependant: { error: ' fake error' } }),
        action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'dependant' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.action.stateKey, assert => {
        assert.plan(1);
        const target = postcodeAnywhereReducer;
        const expected = {
          [testCase.action.stateKey]: { isSearchingPostcode: false },
        };
        const actual = target(testCase.state, testCase.action);
        assert.deepEqual(actual, expected);
      });
    });
  });

  t.test(
    'sendPostcodeSearchReset: should omit sendPostcodeSearchResponse',
    t => {
      const testCases = [
        {
          state: deepFreeze({
            applicant: { sendPostcodeSearchResponse: ' fake response' },
          }),
          action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'applicant' },
        },
        {
          state: deepFreeze({
            dependant: { sendPostcodeSearchResponse: ' fake response' },
          }),
          action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'dependant' },
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.action.stateKey, assert => {
          assert.plan(1);
          const target = postcodeAnywhereReducer;
          const expected = {
            [testCase.action.stateKey]: { isSearchingPostcode: false },
          };
          const actual = target(testCase.state, testCase.action);
          assert.deepEqual(actual, expected);
        });
      });
    }
  );

  t.test('sendPostcodeSearchReset: should omit addressDetails', t => {
    const testCases = [
      {
        state: deepFreeze({
          applicant: { addressDetails: ' fake address details' },
        }),
        action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'applicant' },
      },
      {
        state: deepFreeze({
          dependant: { addressDetails: ' fake address details' },
        }),
        action: { type: SEND_POSTCODE_SEARCH_RESET, stateKey: 'dependant' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.action.stateKey, assert => {
        assert.plan(1);
        const target = postcodeAnywhereReducer;
        const expected = {
          [testCase.action.stateKey]: { isSearchingPostcode: false },
        };
        const actual = target(testCase.state, testCase.action);
        assert.deepEqual(actual, expected);
      });
    });
  });
});
