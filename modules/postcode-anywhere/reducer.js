import R from 'ramda';

import {
  SEND_POSTCODE_SEARCH_REQUEST,
  SEND_POSTCODE_SEARCH_SUCCESS,
  SEND_POSTCODE_SEARCH_FAILURE,
  SEND_POSTCODE_SEARCH_SELECT,
  SEND_POSTCODE_SEARCH_RESET,
} from './actions';

const sendPostcodeSearchRequest = (state, { stateKey }) => ({
  ...state,
  [stateKey]: R.omit(
    ['error', 'sendPostcodeSearchResponse', 'addressDetails'],
    {
      ...state[stateKey],
      isSearchingPostcode: true,
    }
  ),
});

const sendPostcodeSearchSuccess = (
  state,
  { stateKey, sendPostcodeSearchResponse }
) => ({
  ...state,
  [stateKey]: {
    ...state[stateKey],
    isSearchingPostcode: false,
    sendPostcodeSearchResponse,
  },
});

const sendPostcodeSearchFailure = (state, { stateKey, error }) => ({
  ...state,
  [stateKey]: {
    ...state[stateKey],
    isSearchingPostcode: false,
    error,
  },
});

const sendPostcodeSearchSelect = (state, { stateKey, addressDetails }) => ({
  ...state,
  [stateKey]: {
    ...state[stateKey],
    addressDetails,
  },
});

const sendPostcodeSearchReset = (state, { stateKey }) => ({
  ...state,
  [stateKey]: R.omit(
    ['error', 'sendPostcodeSearchResponse', 'addressDetails'],
    {
      ...state[stateKey],
      isSearchingPostcode: false,
    }
  ),
});

const reducers = {
  [SEND_POSTCODE_SEARCH_REQUEST]: sendPostcodeSearchRequest,
  [SEND_POSTCODE_SEARCH_SUCCESS]: sendPostcodeSearchSuccess,
  [SEND_POSTCODE_SEARCH_FAILURE]: sendPostcodeSearchFailure,
  [SEND_POSTCODE_SEARCH_SELECT]: sendPostcodeSearchSelect,
  [SEND_POSTCODE_SEARCH_RESET]: sendPostcodeSearchReset,
};

const INITIAL_STATE = {
  applicant: {
    isSearchingPostcode: false,
  },
  dependant: {
    isSearchingPostcode: false,
  },
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  const fn = reducers[action.type];
  return fn ? fn(state, action) : state;
}
