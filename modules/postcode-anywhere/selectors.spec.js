import test from 'tape';
import {
  getIsSearchingPostcode,
  getSendPostcodeSearchResponse,
} from './selectors';

test('Module postcode-anywhere selectors', t => {
  const testCases = [
    {
      name: 'Returns false when undefined in state',
      stateKey: 'applicant',
      state: {},
      expected: false,
    },
    {
      name: 'Returns false when undefined in state',
      stateKey: 'dependant',
      state: {},
      expected: false,
    },
    {
      name: 'Returns false when undefined in state',
      stateKey: 'applicant',
      state: { postcodeAnywhere: { applicant: {} } },
      expected: false,
    },
    {
      name: 'Returns false when undefined in state',
      stateKey: 'dependant',
      state: { postcodeAnywhere: { dependant: {} } },
      expected: false,
    },
    {
      name: 'Returns false when false in state',
      stateKey: 'applicant',
      state: {
        postcodeAnywhere: { applicant: { isSearchingPostcode: false } },
      },
      expected: false,
    },
    {
      name: 'Returns false when false in state',
      stateKey: 'dependant',
      state: {
        postcodeAnywhere: { dependant: { isSearchingPostcode: false } },
      },
      expected: false,
    },
    {
      name: 'Returns true when true in state',
      stateKey: 'applicant',
      state: { postcodeAnywhere: { applicant: { isSearchingPostcode: true } } },
      expected: true,
    },
    {
      name: 'Returns true when true in state',
      stateKey: 'dependant',
      state: { postcodeAnywhere: { dependant: { isSearchingPostcode: true } } },
      expected: true,
    },
  ];

  testCases.forEach(testCase => {
    t.test('getIsSearchingPostcode', assert => {
      assert.plan(1);
      const target = getIsSearchingPostcode;
      const expected = testCase.expected;
      assert.equals(
        target(testCase.state, testCase.stateKey),
        expected,
        testCase.name
      );
    });
  });
});

test('Module postcode-anywhere selectors', t => {
  const testCases = [
    {
      name: 'Returns undefined when undefined in state',
      stateKey: 'applicant',
      state: {},
      expected: undefined,
    },
    {
      name: 'Returns undefined when undefined in state',
      stateKey: 'dependant',
      state: {},
      expected: undefined,
    },
    {
      name: 'Returns undefined when undefined in state',
      stateKey: 'applicant',
      state: { postcodeAnywhere: { applicant: {} } },
      expected: undefined,
    },
    {
      name: 'Returns undefined when undefined in state',
      stateKey: 'dependant',
      state: { postcodeAnywhere: { dependant: {} } },
      expected: undefined,
    },
    {
      name: 'Returns empty array when empty array in state',
      stateKey: 'applicant',
      state: {
        postcodeAnywhere: { applicant: { sendPostcodeSearchResponse: [] } },
      },
      expected: [],
    },
    {
      name: 'Returns empty array when empty array in state',
      stateKey: 'dependant',
      state: {
        postcodeAnywhere: { dependant: { sendPostcodeSearchResponse: [] } },
      },
      expected: [],
    },
    {
      name: 'Returns array when present in state',
      stateKey: 'applicant',
      state: {
        postcodeAnywhere: {
          applicant: { sendPostcodeSearchResponse: [{ key: 'value' }] },
        },
      },
      expected: [{ key: 'value' }],
    },
    {
      name: 'Returns array when present in state',
      stateKey: 'dependant',
      state: {
        postcodeAnywhere: {
          dependant: { sendPostcodeSearchResponse: [{ key: 'value' }] },
        },
      },
      expected: [{ key: 'value' }],
    },
  ];

  testCases.forEach(testCase => {
    t.test('getSendPostcodeSearchResponse', assert => {
      assert.plan(1);
      const target = getSendPostcodeSearchResponse;
      const expected = testCase.expected;
      assert.deepEqual(
        target(testCase.state, testCase.stateKey),
        expected,
        testCase.name
      );
    });
  });
});
