import { select, call, put, takeLatest } from 'redux-saga/effects';
import WealthWizards from '../../wealthwizards';
import request from 'axios';
import {
  SEND_POSTCODE_SEARCH_REQUEST,
  sendPostcodeSearchSuccess,
  sendPostcodeSearchFailure,
} from '../actions';

export const getTheUserInput = state => state.aboutYou.postcodeToSearchFor;

export const getPostcodeSearch = postcode => ({
  url:
    WealthWizards.POSTCODE_FIND_PATH ||
    `${WealthWizards.CONTEXT_ROUTE}/v1/postcode-find`,
  params: {
    Key: WealthWizards.POSTCODE_ANYWHERE_KEY,
    Text: postcode,
    Countries: 'GB',
  },
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export function* findRequest(action) {
  try {
    const postcode = yield select(getTheUserInput);
    const { data } = yield call(request, getPostcodeSearch(postcode));
    yield put(sendPostcodeSearchSuccess(action.stateKey, data));
  } catch (error) {
    yield put(sendPostcodeSearchFailure(action.stateKey, error));
  }
}

export default function* sendPostcodeSearchRequestSaga() {
  yield takeLatest(SEND_POSTCODE_SEARCH_REQUEST, findRequest);
}
