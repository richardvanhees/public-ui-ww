import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { put } from 'redux-saga/effects';
import fieldMappings from '../config/field-mappings-applicant.json';
import sendPostcodeSearchSelectSaga, {
  addressSelect,
  getMappings,
  formatResponse,
  mapFields,
  __RewireAPI__,
} from './send-postcode-search-select-saga';
import {
  SEND_POSTCODE_SEARCH_SELECT,
  sendPostcodeSearchSuccess,
  sendPostcodeSearchFailure,
  sendPostcodeSearchMapFields,
  sendPostcodeSearchReset,
} from '../actions';

test('sendPostcodeSearchSelectSaga', t => {
  t.test('sendPostcodeSearchSuccess', assert => {
    assert.plan(1);

    const fakeResponse = { data: 'fake response data' };

    const fakeAction = {
      addressDetails: {
        type: 'Postcode',
      },
      stateKey: 'aboutYou',
    };

    const target = testSaga(addressSelect, fakeAction)
      .next()
      .next(fakeResponse)
      .put(sendPostcodeSearchSuccess(fakeAction.stateKey, fakeResponse.data))
      .next();

    assert.true(target.isDone(), 'put sendPostcodeSearchSuccess');
  });

  t.test('sendPostcodeSearchMapFields', assert => {
    assert.plan(1);

    const fakeResponse = { data: 'fake response data' };

    const fakeAction = {
      addressDetails: {
        type: 'Address',
      },
      stateKey: 'aboutYou',
    };

    const fakeMappings = {
      personal__postcode: 'TE57 1NG',
    };

    const target = testSaga(addressSelect, fakeAction)
      .next()
      .next(fakeResponse)
      .call(getMappings, fakeAction.stateKey, fakeResponse.data)
      .next(fakeMappings)
      .all([
        put(sendPostcodeSearchMapFields(fakeAction.stateKey, fakeMappings)),
        put(sendPostcodeSearchReset(fakeAction.stateKey)),
      ])
      .next();

    assert.true(target.isDone(), 'put sendPostcodeSearchMapFields');
  });

  t.test('sendPostcodeSearchFailure', assert => {
    assert.plan(1);

    const fakeAction = {
      addressDetails: {
        type: 'Postcode',
      },
      stateKey: 'aboutYou',
    };

    const fakeError = 'fake error';
    const fakeErrorToHandle = { message: fakeError };

    const target = testSaga(addressSelect, fakeAction)
      .next()
      .throw(fakeErrorToHandle)
      .put(sendPostcodeSearchFailure(fakeAction.stateKey, fakeErrorToHandle))
      .next();
    assert.true(target.isDone(), 'put sendPostcodeSearchFailure');
  });

  t.test('formatResponse', t => {
    const testCases = [
      {
        name: 'All mappings used',
        response: {
          Line1: 'line 1',
          Line2: 'line 2',
          City: 'city',
          Province: 'province',
        },
        expected: {
          Line1: 'line 1',
          Line2: 'line 2',
          City: 'city',
          Province: 'province',
        },
      },
      {
        name: 'City replaces Line2 used if Line2 is empty',
        response: { Line1: 'line 1', Line2: '', City: 'city' },
        expected: { Line1: 'line 1', Line2: 'city', City: '' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.name, assert => {
        assert.plan(1);
        assert.deepEqual(formatResponse(testCase.response), testCase.expected);
      });
    });
  });

  t.test('mapFields', t => {
    const testCases = [
      {
        name: 'All mappings used',
        response: {
          Line1: 'line 1',
          Line2: 'line 2',
          City: 'city',
          Province: 'province',
        },
        expected: {
          Line1: 'line 1',
          Line2: 'line 2',
          City: 'city',
          Province: 'province',
        },
      },
      {
        name: 'City replaces Line2 used if Line2 is empty',
        response: { Line1: 'line 1', Line2: '', City: 'city' },
        expected: { Line1: 'line 1', Line2: 'city', City: '' },
      },
    ];

    testCases.forEach(testCase => {
      t.test(testCase.name, assert => {
        assert.plan(1);
        assert.deepEqual(formatResponse(testCase.response), testCase.expected);
      });
    });
  });
});
