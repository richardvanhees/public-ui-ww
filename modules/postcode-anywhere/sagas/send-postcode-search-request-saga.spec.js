import test from 'tape';
import { testSaga } from 'redux-saga-test-plan';
import { push } from 'react-router-redux';
import sinon from 'sinon';
import sendPostcodeSearchRequestSaga, {
  findRequest,
  __RewireAPI__,
} from './send-postcode-search-request-saga';
import {
  SEND_POSTCODE_SEARCH_REQUEST,
  sendPostcodeSearchSuccess,
  sendPostcodeSearchFailure,
} from '../actions';
import { getTheUserInput } from './send-postcode-search-request-saga';

test('sendPostcodeSearchRequestSaga', t => {
  t.test('sendPostcodeSearchSuccess', assert => {
    assert.plan(1);

    const fakeResponse = { data: 'fake response data' };

    const fakeAction = {
      postcodeSelector: sinon.spy(),
      stateKey: 'applicant',
    };
    const fakePostcode = 'TE57 1NG';

    const target = testSaga(findRequest, fakeAction)
      .next()
      .select(getTheUserInput)
      .next(fakePostcode)
      .next(fakeResponse)
      .put(sendPostcodeSearchSuccess(fakeAction.stateKey, fakeResponse.data))
      .next();

    assert.true(target.isDone(), 'put sendPostcodeSearchSuccess');
  });

  t.test('sendPostcodeSearchFailure', assert => {
    assert.plan(1);

    const fakeAction = {
      postcodeSelector: sinon.spy(),
      stateKey: 'applicant',
    };
    const fakePostcode = 'TE57 1NG';

    const fakeError = 'fake error';
    const fakeErrorToHandle = { message: fakeError };

    const target = testSaga(findRequest, fakeAction)
      .next()
      .select(getTheUserInput)
      .next(fakePostcode)
      .throw(fakeErrorToHandle)
      .put(sendPostcodeSearchFailure(fakeAction.stateKey, fakeErrorToHandle))
      .next();
    assert.true(target.isDone(), 'put sendPostcodeSearchFailure');
  });
});
