import { put, call, takeLatest, all } from 'redux-saga/effects';
import R from 'ramda';
import WealthWizards from '../../wealthwizards';
import request from 'axios';
import fieldMappingsApplicant from '../config/field-mappings-applicant.json';
import {
  SEND_POSTCODE_SEARCH_SELECT,
  sendPostcodeSearchSuccess,
  sendPostcodeSearchFailure,
  sendPostcodeSearchMapFields,
  sendPostcodeSearchReset,
} from '../actions';

export const formatResponse = R.when(
  x => R.isEmpty(x.Line2),
  R.pipe(x => R.assoc('Line2', x.City, x), x => R.assoc('City', '', x))
);

export const getFieldMappings = () => {
  const fieldMappings = fieldMappingsApplicant;
  return fieldMappings;
};

export const getMappings = (stateKey, retrieveResponse) => {
  const formattedResponse = formatResponse(retrieveResponse);
  const fieldMappings = getFieldMappings(stateKey);
  const fieldsMapped = {};

  Object.keys(fieldMappings).forEach(key => {
    // If no mapping defined for this PCA field
    if (!fieldMappings[key]) {
      return;
    }

    fieldsMapped[fieldMappings[key]] = formattedResponse[key];
  });

  return fieldsMapped;
};

export const findPostcode = (baseParams, id, text) => ({
  url:
    WealthWizards.POSTCODE_FIND_PATH ||
    `${WealthWizards.CONTEXT_ROUTE}/v1/postcode-find`,
  params: {
    ...baseParams,
    Text: text,
    Container: id,
  },
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const retrievePostcode = (baseParams, id) => ({
  url:
    WealthWizards.POSTCODE_RETRIEVE_PATH ||
    `${WealthWizards.CONTEXT_ROUTE}/v1/postcode-retrieve`,
  params: {
    ...baseParams,
    Id: id,
  },
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export function* addressSelect(action) {
  const addressDetails = action.addressDetails;
  const stateKey = action.stateKey;

  const baseParams = {
    Key: WealthWizards.POSTCODE_ANYWHERE_KEY,
    Countries: 'GB',
  };

  try {
    // We need to search again but with the Id
    if (addressDetails.type !== 'Address') {
      const { data } = yield call(
        request,
        findPostcode(baseParams, addressDetails.id, addressDetails.text)
      );
      yield put(sendPostcodeSearchSuccess(stateKey, data));
      return;
    }

    // We have an address selected so get the details for that and fire off the action
    const { data } = yield call(
      request,
      retrievePostcode(baseParams, addressDetails.id)
    );
    const mappings = yield call(getMappings, stateKey, data);
    yield all([
      put(
        sendPostcodeSearchMapFields(
          stateKey,
          mappings,
          action.subStateKey,
          action.addressIndex
        )
      ),
      put(sendPostcodeSearchReset(stateKey)),
    ]);
  } catch (error) {
    yield put(sendPostcodeSearchFailure(stateKey, error));
  }
}

export default function* sendPostcodeSearchSelectSaga() {
  yield takeLatest(SEND_POSTCODE_SEARCH_SELECT, addressSelect);
}
