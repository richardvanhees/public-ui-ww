import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ProgressBar from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  progress: 80
};

test('<ProgressBar>', t => {

  t.equals(typeof ProgressBar.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ProgressBar>', t => {

  t.equals(mount(<ProgressBar {...fakeRequiredProps} />).find('.progress-bar').length, 1, 'ProgressBar is rendered');

  t.end();
});
