import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ProgressBar extends Component {
  static propTypes = {
    color: PropTypes.string,
    progress: PropTypes.number.isRequired,
    width: PropTypes.number,
    randomTimeout: PropTypes.bool,
    className: PropTypes.string,
  };
  static defaultProps = {
    randomTimeout: true,
    color: '#444',
    className: '',
  };

  componentDidMount() {
    const { randomTimeout: random, progress } = this.props;
    this.innerProgressBar.style.width = 0;

    setTimeout(() => {
      this.innerProgressBar.style.width = `${progress}%`;
    }, random ? Math.floor(Math.random() * 300) + 500 : 0);
  }

  render() {
    const { color, width, className } = this.props;
    return (
      <div className={`progress-bar ${className}`} style={{ borderColor: color, width }}>
        <div
          ref={c => {
            this.innerProgressBar = c;
          }}
          className="progress-bar__inner"
          style={{ background: color }}
        />
      </div>
    );
  }
}
