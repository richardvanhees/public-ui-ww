import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { ContactWithoutRouter as Contact } from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  testActive: true,
};

test('<Contact>', assert => {
  assert.plan(1);

  const component = mount(<Contact {...fakeRequiredProps} />);
  component.find('.contact__button--phone').simulate('click');
  assert.equals(
    component.instance().state.phoneOpen &&
      !component.instance().state.emailOpen,
    true,
    'Phone content is open, email is closed'
  );
});

test('<Contact>', assert => {
  assert.plan(1);

  const component = mount(<Contact {...fakeRequiredProps} />);
  component.find('.contact__button--email').simulate('click');
  assert.equals(
    component.instance().state.emailOpen &&
      !component.instance().state.phoneOpen,
    true,
    'Email content is open, phone is closed'
  );
});
