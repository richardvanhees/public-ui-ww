import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';
import { withRouter } from 'react-router';
import ContactPhone from '../ContactPhone';
import ContactEmail from '../ContactEmail';

class Contact extends Component {
  static propTypes = {
    testActive: PropTypes.bool,
    history: PropTypes.object,
    nudgeTimer: PropTypes.number,
  };

  static defaultProps = {
    nudgeTimer: 60000,
  };

  constructor(props) {
    super(props);

    this.state = {
      phoneOpen: false,
      phoneTimesOpen: false,
      emailOpen: false,
      nudge: false,
    };
  }

  componentDidMount() {
    if (!this.props.testActive) {
      const timer = this.props.nudgeTimer;
      const nudgeFunc = () => {
        this.setState({ nudge: true });
      };

      let nudgeTimeout = setTimeout(nudgeFunc, timer);

      this.props.history.listen(() => {
        this.setState({ nudge: false });
        clearTimeout(nudgeTimeout);
        nudgeTimeout = setTimeout(nudgeFunc, timer);
      });
    }
  }

  render() {
    const { phoneOpen, phoneTimesOpen, emailOpen, nudge } = this.state;
    return (
      <div className="contact">
        <div className="contact__navigation">
          <div
            onClick={() => {
              this.setState({
                emailOpen: false,
                phoneOpen: !this.state.phoneOpen,
              });
            }}
            className={`contact__button contact__button--phone ${
              phoneOpen ? 'contact__button--active' : ''
            }`}
          >
            <Icon
              className={`contact__phone-icon ${
                nudge ? 'contact__phone-icon--nudge' : ''
              }`}
              name={'phone'}
              iconSet={'solid'}
            />
          </div>
          <div
            onClick={() => {
              this.setState({
                phoneOpen: false,
                emailOpen: !emailOpen,
              });
            }}
            className={`contact__button contact__button--email ${
              emailOpen ? 'contact__button--active' : ''
            }`}
          >
            <Icon name={'envelope'} />
          </div>
        </div>

        <div
          className={`contact__content contact__content--phone ${
            phoneOpen ? 'contact__content--visible' : ''
          } ${phoneTimesOpen ? 'contact__content--phone-times-open' : ''}`}
        >
          <ContactPhone
            phoneTimesOpen={value => this.setState({ phoneTimesOpen: value })}
            closeButtonAction={() => this.setState({ phoneOpen: false })}
          />
        </div>

        <div
          className={`contact__content contact__content--email ${
            emailOpen ? 'contact__content--visible' : ''
          }`}
        >
          <ContactEmail
            closeButtonAction={() => this.setState({ emailOpen: false })}
          />
        </div>
      </div>
    );
  }
}

export const ContactWithoutRouter = Contact;
export default withRouter(Contact);
