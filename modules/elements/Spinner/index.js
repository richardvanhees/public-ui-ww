import React from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-md-spinner';

const SpinnerComponent = ({ visible }) => (
  <div
    data-test={visible ? 'spinner-container' : ''}
    className={`spinner__container ${
      visible ? 'spinner__container--visible' : ''
    }`}
  >
    <Spinner
      size="70"
      style={{
        zIndex: 1000,
        left: '50%',
        top: '50%',
        marginLeft: '-35px',
        position: 'fixed',
      }}
      singleColor={'#0F7EAD'}
    />
  </div>
);

SpinnerComponent.propTypes = {
  visible: PropTypes.bool,
};

export default SpinnerComponent;
