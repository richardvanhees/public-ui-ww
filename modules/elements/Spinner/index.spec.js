import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Spinner from 'react-md-spinner';
import SpinnerComponent from './index';

configure({ adapter: new Adapter() });

test('<SpinnerComponent>', t => {
  t.equals(
    shallow(<SpinnerComponent />).find(Spinner).length,
    1,
    'Spinner is rendered correctly'
  );

  t.equals(
    !!shallow(<SpinnerComponent />)
      .find(Spinner)
      .props().singleColor,
    true,
    'Spinner consists of one colour'
  );

  t.equals(
    !!shallow(<SpinnerComponent visible={false}/>)
      .find('.spinner__container--visible')
      .length,
    false,
    'Spinner not visible on false'
  );

  t.equals(
    !!shallow(<SpinnerComponent visible={true}/>)
      .find('.spinner__container--visible')
      .length,
    true,
    'Spinner visible on true'
  );

  t.end();
});
