import React from 'react';
import { storiesOf } from '@storybook/react';

import Spinner from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Spinner', module)
  .add('Required', () => <Spinner visible />);
