import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default class ExplanationComponent extends Component {
  static propTypes = {
    title: PropTypes.string,
    children: PropTypes.node.isRequired,
    hideClose: PropTypes.bool,
    className: PropTypes.string,
  };

  static defaultProps = {
    classNames: '',
  };

  state = {
    visible: true,
  };

  toggleVisibility = () => this.setState({ visible: !this.state.visible });

  render() {
    const { children, title, hideClose, className } = this.props;
    const { visible } = this.state;

    return (
      <div className={`explanation ${visible ? '' : 'explanation--hidden'} ${className}`}>
        {!hideClose && (
          <div className="explanation__close" onClick={this.toggleVisibility}>
            <Icon name={'plus'} />
          </div>
        )}
        {title && <div className="explanation__title">{title}</div>}
        <div className={'explanation__content'}>{children}</div>
      </div>
    );
  }
}
