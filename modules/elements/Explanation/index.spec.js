import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Explanation from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Explanation>', t => {
  t.equals(typeof Explanation.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Explanation>', t => {
  const component = shallow(<Explanation>fakeChildren</Explanation>);

  t.equals(component.find('.explanation__close').length, 1, 'Close button is shown without hideClose prop');
  t.equals(component.find('.explanation__title').length, 0, 'Title is hidden without title prop');
  t.equals(component.find('.explanation--hidden').length, 0, 'Block is shown by default');

  component.find('.explanation__close').simulate('click');
  t.equals(component.find('.explanation--hidden').length, 1, 'Block is hidden after clicking close button');

  t.end();
});

test('<Explanation>', t => {
  const component = shallow(<Explanation hideClose>fakeChildren</Explanation>);

  t.equals(component.find('.explanation__close').length, 0, 'Close button is hidden with hideClose prop');

  t.end();
});

test('<Explanation>', t => {
  const component = shallow(<Explanation title={'fakeTitle'}>fakeChildren</Explanation>);

  t.equals(component.find('.explanation__title').length, 1, 'Title is shown with title prop');

  t.end();
});