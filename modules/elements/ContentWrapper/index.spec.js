import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContentWrapper from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<ContentWrapper>', t => {
  t.equals(typeof ContentWrapper.propTypes, 'object', 'PropTypes are defined');

  t.end();
});
