import React from 'react';
import PropTypes from 'prop-types';

const ContentWrapperComponent = ({ children, className }) => (
  <div className={`content-wrapper ${className}`}>{children}</div>
);

ContentWrapperComponent.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

ContentWrapperComponent.defaultProps = {
  className: '',
};

export default ContentWrapperComponent;
