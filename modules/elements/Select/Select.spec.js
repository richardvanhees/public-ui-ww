import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import Select, { __RewireAPI__ } from './index';

test('<Select/> component', t => {
  t.test('maps props', t => {
    t.plan(4);

    const fakeProps = {
      label: 'fake label',
      name: 'fake name',
      selectOptions: [],
    };

    const fakeSelectValue = 'Please Select';
    const target = shallow(<Select {...fakeProps} />);

    t.equals(target.find('.select__label').text(), fakeProps.label);
    t.equals(target.find('select').prop('name'), fakeProps.name);
    t.equals(target.find('select').prop('data-test'), fakeProps.name);
    t.equals(target.find('.select__inner-container').text(), fakeSelectValue);
  });

  t.test('maps an <option> for each item in the selectOptions array', t => {
    t.plan(14);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
    };

    const target = shallow(<Select {...fakeProps} />);

    const options = target.find('option');

    t.equal(options.get(0).props.label, 'Please Select');
    t.equal(options.get(0).props.value, '');

    options.filterWhere(n => n.key()).forEach((option, i) => {
      t.equal(option.key(), fakeProps.selectOptions[i].value);
      t.equal(option.prop('label'), fakeProps.selectOptions[i].label);
      t.equal(option.prop('value'), fakeProps.selectOptions[i].value);
      t.equal(option.text(), fakeProps.selectOptions[i].label);
    });
  });

  t.test(
    'makes each <option> value safe, whether number, string or false etc.',
    t => {
      const testCases = [
        {
          name: 'when value is 12',
          option: { label: 'fake label', value: 12 },
          expectedValue: 12,
        },
        {
          name: 'when value is 0',
          option: { label: 'fake label', value: 0 },
          expectedValue: 0,
        },
        {
          name: 'when value is 12.50',
          option: { label: 'fake label', value: 12.5 },
          expectedValue: 12.5,
        },
        {
          name: "when value is 'fake value'",
          option: { label: 'fake label', value: 'fake value' },
          expectedValue: 'fake value',
        },
        {
          name: 'when value is false',
          option: { label: 'fake label', value: false },
          expectedValue: false,
        },
        {
          name: 'when value is null',
          option: { label: 'fake label', value: null },
          expectedValue: undefined,
        },
        {
          name: 'when value is undefined',
          option: { label: 'fake label', value: undefined },
          expectedValue: undefined,
        },
      ];

      testCases.forEach(testCase => {
        t.test(testCase.name, assert => {
          assert.plan(1);

          const fakeProps = {
            selectOptions: [testCase.option],
          };

          const target = shallow(<Select {...fakeProps} />);
          const options = target.find('option');
          const option = options.get(1);

          assert.equal(option.props.value, testCase.expectedValue);
        });
      });
    }
  );

  t.test('sets initial value when prop defined', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
      initialValue: 'fake value 3',
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(target.find('select').prop('value'), 'fake value 3');
  });

  t.test('sets initial value to "" when prop undefined', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(target.find('select').prop('value'), '');
  });

  t.test('sets initial value to "" when prop null', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
      initialValue: null,
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(target.find('select').prop('value'), '');
  });

  t.test('sets initial value to false when prop false', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
      initialValue: false,
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(target.find('select').prop('value'), false);
  });

  t.test('sets initial value to 0 when prop 0', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [
        { label: 'fake label 1', value: 'fake value 1' },
        { label: 'fake label 2', value: 'fake value 2' },
        { label: 'fake label 3', value: 'fake value 3' },
      ],
      initialValue: 0,
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(target.find('select').prop('value'), 0);
  });

  t.test('that component obeys visible prop', t => {
    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: 'fake change handler',
    };

    const targetDefault = shallow(<Select {...fakeProps} />);
    const targetVisible = shallow(<Select {...fakeProps} visible={true} />);
    const targetNotVisible = shallow(<Select {...fakeProps} visible={false} />);

    t.notEqual(targetDefault.html(), null, 'Default rendered');
    t.notEqual(targetVisible.html(), null, 'Visible rendered');
    t.equals(targetNotVisible.html(), null, 'Not visible not rendered');

    t.end();
  });

  t.test('treats numbers as numbers', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: 'fake change handler',
      initialValue: '12.50',
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(
      target.find('select').prop('value'),
      12.5,
      'parses value as number when value is a number'
    );
  });

  t.test('treats non-numbers as non-numbers', t => {
    t.plan(1);

    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: 'fake change handler',
      isNumber: false,
      initialValue: '12.50%',
    };

    const target = shallow(<Select {...fakeProps} />);

    t.equal(
      target.find('select').prop('value'),
      '12.50%',
      'leaves value as string when value is not a number'
    );
  });

  t.test('treats numbers as numbers onchange', t => {
    t.plan(1);

    const fakeChangeHandler = (name, value) => {
      const actual = value;
      const expected = -0.5;

      t.equal(actual, expected, 'parses value as number when value is number');
    };

    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: fakeChangeHandler,
      isNumber: true,
      initialValue: '-0.50',
    };

    const target = shallow(<Select {...fakeProps} />);

    target.find('select').simulate('change', { target: { value: '-0.50' } });
  });

  t.test('treats non-numbers as non-numbers onchange', t => {
    t.plan(1);

    const fakeChangeHandler = (name, value) => {
      const actual = value;
      const expected = '5%';

      t.equal(actual, expected, 'parses value as number when value is number');
    };

    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: fakeChangeHandler,
      isNumber: true,
      initialValue: '5%',
    };

    const target = shallow(<Select {...fakeProps} />);

    target.find('select').simulate('change', { target: { value: '5%' } });
  });

  t.test('passes target.name to onchange handler', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeChangeHandler = name => {
      const actual = name;
      const expected = fakeName;

      t.equal(actual, expected, 'passes name to change handler');
    };

    const fakeProps = {
      label: 'fake label',
      selectOptions: [],
      onChange: fakeChangeHandler,
      initialValue: '12.50',
    };

    const target = shallow(<Select {...fakeProps} />);

    target
      .find('select')
      .simulate('change', { target: { name: fakeName, value: '123' } });
  });
});
