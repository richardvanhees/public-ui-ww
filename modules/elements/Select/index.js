import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';

const safeNumber = R.when(R.test(/^(?!\s*$)-?(0|[1-9]\d*)?(\.\d+)?$/), Number);

const safeValue = R.compose(
  R.cond([
    [R.equals(0), R.always(0)],
    [R.equals(false), R.always(false)],
    [R.isNil, R.always(undefined)],
    [R.T, R.identity],
  ]),
  safeNumber
);

const Select = props =>
  props.visible && (
    <div className="select__container">
      <label className="select__label">{props.label}</label>
      <div className="select__inner-container">
        {props.selectTitle}
        <select
          className="select__element"
          name={props.name}
          onChange={R.compose(
            R.tap(x => props.onChange(x.name, x.value)),
            R.evolve({
              value: safeNumber,
            }),
            R.propOr({}, 'target')
          )}
          value={
            R.isNil(props.initialValue) ? '' : safeNumber(props.initialValue)
          }
          data-test={props.name}
        >
          <option label="Please Select" value="" disabled>
            Please Select
          </option>
          {props.selectOptions.map(option => (
            <option
              key={option.value}
              label={option.label}
              value={safeValue(option.value)}
            >
              {option.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  );

Select.propTypes = {
  visible: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  selectTitle: PropTypes.string,
  selectOptions: PropTypes.array.isRequired,
  initialValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
};

Select.defaultProps = {
  visible: true,
  isNumber: false,
};

export default Select;
