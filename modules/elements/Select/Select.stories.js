import React from 'react';
import { storiesOf } from '@storybook/react';

import Select from './index';
import '../../../assets/styles/common/index.scss';

const exampleSelectOptions = [{ value: 'foo', label: 'Foo' }, { value: 'bar', label: 'Bar' }, { value: 'baz', label: 'Baz' }];

storiesOf('Select', module)
  .add('Required', () => <Select name="Example" selectOptions={exampleSelectOptions} />);
