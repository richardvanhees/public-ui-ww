import React, { Component } from 'react';
import PropTypes from 'prop-types';

/* eslint-disable no-nested-ternary */
export default class HoverTile extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    initialContent: PropTypes.node.isRequired,
    hoveredContent: PropTypes.node,
    style: PropTypes.object.isRequired,
    growthFactor: PropTypes.number.isRequired,
    selectable: PropTypes.bool,
    selected: PropTypes.bool.isRequired,
    index: PropTypes.number.isRequired,
    returnValueOnClick: PropTypes.any,
    dataTest: PropTypes.string.isRequired,
  };

  state = {
    hovered: false,
    selected: false,
  };

  onClick = () => {
    this.props.onClick(this.props.index, this.props.returnValueOnClick);
  };

  setStyle = () => this.state.hovered ?
    {
      ...this.props.style,
      width: this.props.style.width * this.props.growthFactor,
      height: this.props.style.height * this.props.growthFactor,
    } : this.props.style;

  render = () => {
    const { title, initialContent, hoveredContent, selectable, selected, dataTest, index } = this.props;
    const { hovered } = this.state;


    return (<button
      data-test={`${dataTest}-a${index}`}
      className={`
        hover-tile
        ${hovered ? (!!hoveredContent ? 'hover-tile--hover' : 'hover-tile--hover-without-hovered-content') : ''}
        ${selectable && selected ? 'hover-tile--selected' : ''}
        ${selectable && selected && hovered ? (!!hoveredContent ? 'hover-tile--selected-hover' : 'hover-tile--selected-hover-without-hovered-content') : ''}
      `}
      style={this.setStyle()}
      onClick={this.onClick}
      onMouseEnter={() => this.setState({ hovered: true })}
      onMouseLeave={() => this.setState({ hovered: false })}
    >
      <div className={`hover-tile__title ${hovered && !!hoveredContent ? 'hover-tile__title--hover' : ''}`}>{title}</div>
      <div className={`hover-tile__content-wrapper ${hovered && !!hoveredContent ? 'hover-tile__content-wrapper--hover' : ''}`}>
        <div className={`hover-tile__initial-content ${hovered && !!hoveredContent ? 'hover-tile__initial-content--hidden' : ''}`}>{initialContent}</div>
        {!!hoveredContent && <div className={`hover-tile__hovered-content ${hovered ? 'hover-tile__hovered-content--visible' : ''}`}>{hoveredContent}</div>}
      </div>
    </button>);
  }
}
