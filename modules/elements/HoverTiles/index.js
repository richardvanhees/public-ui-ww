import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HoverTile from './HoverTile';

export default class HoverTileGroup extends Component {
  static propTypes = {
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        initialContent: PropTypes.node.isRequired,
        hoveredContent: PropTypes.node,
        returnValueOnClick: PropTypes.any,
      })
    ),
    onSelect: PropTypes.func.isRequired,
    size: PropTypes.number,
    margin: PropTypes.number,
    maxRowLength: PropTypes.number,
    // fadeNonHovered: PropTypes.bool,
    growthFactor: PropTypes.number,
    selectable: PropTypes.bool,
    selected: PropTypes.any,
    dataTest: PropTypes.string.isRequired,
  };

  static defaultProps = {
    size: 200,
    margin: 5,
    maxRowLength: 3,
    // fadeNonHovered: false,
    growthFactor: 1.15,
    selectable: false,
  };

  state = {
    selectedTile: false,
  };

  onSelect = (index, toBeReturned) => {
    this.setState({ selectedTile: index });
    this.props.selectable && this.props.onSelect(toBeReturned);
  };

  render = () => {
    const { maxRowLength, size, growthFactor, margin, options, selectable, dataTest } = this.props;

    return (<div
      className="hover-tile-group"
      style={{
        width: maxRowLength * (size * growthFactor + margin * 2),
        minHeight: Math.ceil(options.length / maxRowLength) * (size + margin * 2),
        marginBottom: (size * growthFactor) - size,
        marginLeft: this.props.margin * -1,
        marginRight: this.props.margin * -1,
        maxWidth: `calc(100% + ${this.props.margin * 2}px)`,
      }}
    >
      {options.map((option, i) =>
        <HoverTile
          {...option}
          dataTest={dataTest}
          key={`tile-${i}`}
          index={i}
          growthFactor={growthFactor}
          selectable={selectable}
          selected={this.state.selectedTile === i || this.props.selected === option.title}
          onClick={this.onSelect}
          style={{
            width: size,
            height: size,
            margin,
          }}
        />
      )}
    </div>);
  }
}
