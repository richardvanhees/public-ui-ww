import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import HoverTiles from './index';
import Button from '../Button';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const onSelectStub = sandbox.stub();

const fakeRequiredProps = {
  options: [{
    title: 'fakeTitle 1',
    initialContent: 'fakeInitialContent 1',
    hoveredContent: 'fakeHoveredContent 1',
    returnValueOnClick: 'returnValue 1'
  }, {
    title: 'fakeTitle 2',
    initialContent: 'fakeInitialContent 2',
    returnValueOnClick: 'returnValue 2'
  }],
  onSelect: onSelectStub,
};


test('<HoverTiles>', t => {
  const component = mount(<HoverTiles {...fakeRequiredProps} />);
  t.equals(component.find('.hover-tile').length, 2, 'Correct amount of tiles is rendered');
  t.equals(component.find('.hover-tile__hovered-content').length, 1, 'Correct amount of hovered content is rendered');

  component.find('.hover-tile').at(0).simulate('click');
  t.equals(onSelectStub.called, false, 'onSelect is not called when selectable is false');

  t.end();
});

test('<HoverTiles>', t => {
  const component = mount(<HoverTiles {...fakeRequiredProps} selectable />);

  component.find('.hover-tile').at(0).simulate('click');
  t.equals(onSelectStub.called, true, 'onSelect is called when tile is clicked');
  t.deepEquals(onSelectStub.args, [['returnValue 1']], 'onSelect returns correct data');

  t.end();
});

test('<HoverTiles>', t => {
  const component = mount(<HoverTiles {...fakeRequiredProps} />);

  component.find('.hover-tile').at(0).simulate('mouseenter');
  t.equals(component.find('.hover-tile__hovered-content--visible').length, 1, 'hoveredContent is visible when tile is hovered');

  t.end();
});