import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';

import Input from './index';

const sandbox = sinon.sandbox.create();
const onClickStub = sandbox.stub();
const onChangeStub = sandbox.stub();
const onBlurStub = sandbox.stub();
const onKeyPressStub = sandbox.stub();

const fakeRequiredProps = {
  onClick: onClickStub,
  onChange: onChangeStub,
  onBlur: onBlurStub,
  label: 'testLabel',
};

test('<Input>', t => {
  t.equals(typeof Input.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(<Input {...fakeRequiredProps} />).find('.input-wrapper').length,
    1,
    'input-wrapper class is present is set as default'
  );

  const wrapper = mount(<Input {...fakeRequiredProps} />);

  wrapper.simulate('click');
  t.deepEqual(onClickStub.called, true, 'onClick is called at click');

  wrapper.find('.input-wrapper__input').simulate('change');
  t.deepEqual(onChangeStub.called, true, 'onChange is called at change event');

  wrapper.find('.input-wrapper__input').simulate('blur');
  t.deepEqual(onClickStub.called, true, 'onBlur is called on blur');

  t.end();
});