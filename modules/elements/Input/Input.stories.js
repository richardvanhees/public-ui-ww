import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Input from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Input', module)
  .add('Basic', () => <Input value="foo" onChange={action('onChange')} />)
  .add('Label', () => <Input value="bar" label="Example" onChange={action('onChange')} />);
