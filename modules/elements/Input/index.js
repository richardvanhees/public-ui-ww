import React from 'react';
import PropTypes from 'prop-types';

const Input = ({
  wrapperClassName,
  labelClassName,
  inputClassName,
  onClick,
  onChange,
  label,
  dataTest,
  hintText,
  value,
  type,
  error,
  onBlur,
  defaultValue,
  onKeyPress,
  autoFocus,
  enabled,
  autoComplete,
}) => (
  <div className={`input-wrapper ${wrapperClassName}`} onClick={onClick}>
    {label && (
      <p className={`input-wrapper__label ${labelClassName}`}>{label}</p>
    )}

    <input
      className={`input-wrapper__input input ${inputClassName} ${
        error
          ? `${inputClassName}--error input-wrapper__input--error input--error`
          : ''
      }`}
      data-test={dataTest}
      placeholder={hintText}
      onChange={onChange}
      onBlur={onBlur}
      onKeyPress={onKeyPress}
      value={value}
      defaultValue={defaultValue}
      enabled={enabled ? 'enabled' : null}
      type={type}
      ref={input => input && autoFocus && input.focus()}
      autoComplete={autoComplete}
    />
  </div>
);

Input.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  onBlur: PropTypes.func,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.number,
  ]),
  wrapperClassName: PropTypes.string,
  labelClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  hintText: PropTypes.string,
  dataTest: PropTypes.string,
  error: PropTypes.bool,
  defaultValue: PropTypes.string,
  autoFocus: PropTypes.bool,
  enabled: PropTypes.bool,
  autoComplete: PropTypes.string,
};

Input.defaultProps = {
  inputClassName: '',
  labelClassName: '',
  wrapperClassName: '',
  type: 'text',
  autoComplete: 'off',
  error: false,
  autoFocus: false,
};

export default Input;
