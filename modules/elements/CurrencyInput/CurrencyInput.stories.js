import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CurrencyInput from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('CurrencyInput', module)
  .add('Basic', () => <CurrencyInput onChange={action('onChange')} />)
  .add('Label', () => <CurrencyInput label="Example" onChange={action('onChange')} />);
