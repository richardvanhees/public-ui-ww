import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import R from 'ramda';

const unformatValue = value => {
  const regex = new RegExp(/[a-zA-Z,]/, 'g');
  return Number(value.replace(regex, ''));
};

const isValid = char => !R.isEmpty(R.match(/\d/)(char));

const clean = string => (
  R.compose(
    R.join(''),
    R.filter(isValid),
    R.split('')
  )(string)
);

const formatValue = value => {
  if (!value) return '';
  let [pounds, pence] = String(value).split('.');

  const regex = new RegExp(/(\d)(?=(\d{3})+(?!\d))/, 'g');
  pounds = clean(pounds);
  pounds = pounds.replace(regex, '$1,');

  if (pence && pence.length > 2) {
    pence = pence.slice(0, 2);
  }

  return R.reject(R.isNil, [pounds, pence]).join('.');
};

const buildState = value => ({
  value: formatValue(value),
});


class CurrencyInput extends Component {
  static propTypes = {
    label: PropTypes.string,
    currencySymbol: PropTypes.string,
    value: PropTypes.number,
    onClick: PropTypes.func,
    onChange: PropTypes.func,
    hintText: PropTypes.string,
    dataTest: PropTypes.string,
    error: PropTypes.bool,
    className: PropTypes.string,
    labelClassName: PropTypes.string,
    debounce: PropTypes.bool,
  };

  static defaultProps = {
    currencySymbol: '£',
    hintText: '',
    type: 'text',
    error: false,
    value: null,
    className: '',
    labelClassName: '',
    debounce: true,
  };

  constructor(props) {
    super(props);
    this.state = buildState(this.props.value);
  }

  componentDidMount() {
    if (this.props.debounce) {
      this.debounceOnChange = debounce(() => {
        if (typeof this.props.onChange === 'function') {
          this.props.onChange(unformatValue(this.state.value));
        }
      }, 500);
    }
  }

  externalUpdateValue = (value) => {
    this.setState(buildState(value));
    this.props.onChange(value);
  }

  update = event => {
    const newValue = event.target.value;
    this.setState(buildState(newValue));
    if (this.props.debounce) {
      this.debounceOnChange();
    } else {
      this.props.onChange(unformatValue(newValue));
    }
  };

  render() {
    const {
      className,
      label,
      currencySymbol,
      onClick,
      dataTest,
      hintText,
      error,
      labelClassName,
    } = this.props;
    return (
      <div className={`currency-input-wrapper ${className}`} onClick={onClick}>
        {label && (
          <div className={`currency-input-wrapper__label ${labelClassName}`}>
            {label}
          </div>
        )}
        <div className="currency-input-wrapper__container">
          <i className="currency-input-wrapper__icon">{currencySymbol}</i>
          <input
            className={`currency-input-wrapper__input ${
              error
                ? 'currency-input-wrapper__input--error input-wrapper__input--error input--error'
                : ''
            }`}
            label={label}
            data-test={dataTest}
            placeholder={hintText}
            onChange={this.update}
            value={this.state.value}
            pattern="[\d,.]*"
            type="text"
          />
        </div>
      </div>
    );
  }
}

export default CurrencyInput;
