import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';

import CurrencyInput from './index';

const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();
const onChangeSpy = sandbox.spy(onChangeStub);

const fakeRequiredProps = {
  onChange: onChangeSpy,
  label: 'testLabel',
  debounce: false,
};

test('<CurrencyInput>', unit => {
  unit.test('propTypes are defined', assert => {
    assert.plan(1);
    assert.equals(typeof CurrencyInput.propTypes, 'object', 'PropTypes are defined');
    assert.end();
  });

  unit.test('class is set', assert => {
    assert.plan(1);
    assert.equals(
      shallow(<CurrencyInput {...fakeRequiredProps} />).find(
        '.currency-input-wrapper'
      ).length,
      1,
      'currency-input-wrapper class is present is set as default'
    );
    assert.end();
  });

  unit.test('onChange called', assert => {
    assert.plan(1);

    const wrapper = mount(<CurrencyInput {...fakeRequiredProps} />);
    const target = wrapper.find('input');

    target.simulate('change', {target: {value: '10,000'}});

    assert.true(onChangeSpy.calledWith(10000), 'onChange called with number value');
    assert.end();
  });

  unit.test('value in input is formatted', assert => {
    assert.plan(1);

    const props = {
      value: 10000.12,
      ...fakeRequiredProps
    }

    const wrapper = mount(<CurrencyInput {...props} />);
    const target = wrapper.find('input').get(0);

    assert.deepEqual(target.props.value, '10,000.12');

    assert.end();
  });
})
