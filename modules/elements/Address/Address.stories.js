import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Address from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Address', module)
  .add('Basic', () => <Address onBlur={action('onComplete')} />)
  .add('Enter manually', () => <Address enterManually onBlur={action('onComplete')} />);
