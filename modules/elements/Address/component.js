import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';
import Postcode from '../Postcode';
import Error from '../Error';
import Button from '../Button';
import Icon from '../Icon';
import {
  POSTCODE_REGEX,
  validAddressLine,
  optionalValidAddressLine,
} from '../../utils/regexes';

export default class AddressComponent extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    addressLine1: PropTypes.string,
    addressLine2: PropTypes.string,
    addressLine3: PropTypes.string,
    addressPostCode: PropTypes.string,
    enterManually: PropTypes.bool,
    setAboutAdressLine1: PropTypes.func,
    setAboutAdressLine2: PropTypes.func,
    setAboutAdressLine3: PropTypes.func,
    setAboutPostCode: PropTypes.func,
    toggleManualInputs: PropTypes.func,
    showManualInputs: PropTypes.bool,
    onBlur: PropTypes.func,
  };

  static defaultProps = {
    wrapperClassName: '',
  };

  constructor() {
    super();
    this.state = {
      error: {
        addressLine1: null,
        addressLine2: null,
        addressLine3: null,
        addressPostCode: null,
      },
    };
    this.formValidates = this.formValidates.bind(this);
    this.resetFormError = this.resetFormError.bind(this);
  }

  resetFormError() {
    this.setState({
      error: {
        addressLine1: null,
        addressLine2: null,
        addressLine3: null,
        addressPostCode: null,
      },
    });
    return true;
  }

  formValidates() {
    let validates = true;

    const newState = {
      error: {
        addressLine1: null,
        addressLine2: null,
        addressLine3: null,
        addressPostCode: null,
      },
    };

    if (!validAddressLine(this.props.addressLine1)) {
      newState.error.addressLine1 = 'Please enter a valid address line 1';
      validates = false;
    } else if (!validAddressLine(this.props.addressLine2)) {
      newState.error.addressLine2 = 'Please enter a valid address line 2';
      validates = false;
    } else if (!optionalValidAddressLine(this.props.addressLine3)) {
      newState.error.addressLine3 = 'Please enter a valid address line 3';
      validates = false;
    } else if (
      !this.props.addressPostCode ||
      !this.props.addressPostCode.match(POSTCODE_REGEX)
    ) {
      newState.error.addressPostCode = 'Please enter a valid postcode';
      validates = false;
    }

    this.setState(newState);

    return validates;
  }

  render() {
    const {
      toggleManualInputs,
      wrapperClassName,
      label,
      dataTest,
      hintText,
      enterManually,
      sendPostcodeSearchRequest,
      isSearchingPostcode,
      sendPostcodeSearchResponse,
      sendPostcodeSearchSelect,
      sendPostcodeSearchReset,
      showManualInputs,
      setAboutAdressLine1,
      setAboutAdressLine2,
      setAboutAdressLine3,
      setAboutPostCode,
      addressLine1,
      addressLine2,
      addressLine3,
      addressPostCode,
      error,
      onBlur,
    } = this.props;

    return (
      <div
        className={`address-wrapper ${wrapperClassName}`}
        data-test={dataTest}
      >
        <Postcode
          textInputLabel={label}
          textInputName="personal__postcode"
          textInputPlaceholder={hintText}
          textInputInitialValue=""
          textInputOnChange={value => setAboutPostCode({ postCode: value })}
          textInputOnBlur={onBlur}
          buttonClassName="buttonLabel"
          buttonName="send-postcode-search-request"
          buttonTextInitial="Find address"
          buttonTextSearching="Searching..."
          isSearchingPostcode={isSearchingPostcode}
          sendPostcodeSearchRequest={sendPostcodeSearchRequest}
          sendPostcodeSearchResponse={sendPostcodeSearchResponse}
          sendPostcodeSearchSelect={sendPostcodeSearchSelect}
          sendPostcodeSearchReset={sendPostcodeSearchReset}
        />
        <Error
          compact
          title={error}
          active={error}
          style={{ marginTop: '5px' }}
        />

        {enterManually && (
          <div
            className="address-wrapper__manual-title-container"
            onClick={() => this.resetFormError() && toggleManualInputs()}
          >
            <p
              className="address-wrapper__manual-title-container__title"
              data-test="address-manual-link"
            >
              Enter manually{' '}
            </p>
          </div>
        )}
        {showManualInputs && (
          <div className="current-address__overlay">
            <div className="current-address__form-wrapper">
              <div
                data-test="popup__close-button"
                className={'popup__close-button'}
                onClick={() => this.resetFormError() && toggleManualInputs()}
              >
                <div className={'popup__close-icon'}><Icon name={'times-circle'} /></div>
              </div>

              <p
                className="current-address__title "
                data-test="current-address-title"
              >
                Add your current address
              </p>
              <div className="manual-input-container current-address__column">
                <div>
                  <Input
                    dataTest="address-line-1"
                    onChange={e =>
                      setAboutAdressLine1({ addressLine1: e.target.value })
                    }
                    label="Address line 1"
                    value={addressLine1}
                  />
                  <Error
                    compact
                    title={this.state.error.addressLine1}
                    active={this.state.error.addressLine1}
                  />
                </div>
                <div>
                  <Input
                    dataTest="address-line-2"
                    onChange={e =>
                      setAboutAdressLine2({ addressLine2: e.target.value })
                    }
                    label="Address line 2"
                    value={addressLine2}
                  />
                  <Error
                    compact
                    title={this.state.error.addressLine2}
                    active={this.state.error.addressLine2}
                  />
                </div>
                <div>
                  <Input
                    dataTest="address-line-3"
                    onChange={e =>
                      setAboutAdressLine3({ addressLine3: e.target.value })
                    }
                    label="Address line 3"
                    value={addressLine3}
                  />
                  <Error
                    compact
                    title={this.state.error.addressLine3}
                    active={this.state.error.addressLine3}
                  />
                </div>
                <div>
                  <Input
                    dataTest="address-postcode"
                    onChange={e =>
                      setAboutPostCode({ postCode: e.target.value })
                    }
                    label="Postcode"
                    value={addressPostCode}
                  />
                  <Error
                    compact
                    title={this.state.error.addressPostCode}
                    active={this.state.error.addressPostCode}
                  />
                </div>
                <Button
                  dataTest="add-current-address"
                  className={'btn btn--primary previous-address__btn'}
                  label="Add address"
                  onClick={() =>
                    this.formValidates() &&
                    this.resetFormError() &&
                    toggleManualInputs()
                  }
                />
              </div>
            </div>
          </div>
        )}
        {!showManualInputs &&
        addressLine1 && (
          <p>
            {addressLine1 ? 'Address' : ''}
            <br />
            {addressLine1}
            <br />
            {addressLine2}
            <br />
            {addressLine3}
            <br />
            {addressPostCode}
          </p>
        )}
      </div>
    );
  }
}
