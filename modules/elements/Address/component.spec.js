import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import AddressComponent from './component';

let fakeRequiredProps = {
  className: 'test',
  enterManually: true,
  isSearchingPostcode: false,
  showManualInputs: true,
  sendPostcodeSearchRequest: () => {},
  sendPostcodeSearchSelect: () => {},
  sendPostcodeSearchReset: () => {},
  addressLine1: 'addressLine1',
  addressLine2: 'addressLine2',
  addressLine3: 'addressLine3',
  addressPostCode: 'B475QX',
};

test('<AddressComponent>', t => {
  t.equals(
    typeof AddressComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.equals(
    shallow(<AddressComponent {...fakeRequiredProps} />).find(
      '.address-wrapper'
    ).length,
    1,
    'address-wrapper class is present is set as default'
  );

  t.equals(
    shallow(<AddressComponent {...fakeRequiredProps} />).find(
      '.address-wrapper__manual-title-container'
    ).length,
    1,
    '__manual-title-container is present when enterManually prop is true'
  );

  fakeRequiredProps.enterManually = false;

  t.equals(
    shallow(<AddressComponent {...fakeRequiredProps} />).find(
      '.address-wrapper__manual-title-container'
    ).length,
    0,
    '__manual-title-container is NOT present when enterManually prop is true'
  );

  t.equals(
    shallow(<AddressComponent {...fakeRequiredProps} />).find(
      '.manual-input-container'
    ).length,
    1,
    'manual-input-container is present when showManualInputs prop is true'
  );

  fakeRequiredProps.showManualInputs = false;

  t.equals(
    shallow(<AddressComponent {...fakeRequiredProps} />).find(
      '.manual-input-container'
    ).length,
    0,
    'manual-input-container is NOT present when showManualInputs prop is true'
  );

  t.end();
});

test('<Address />', assert => {
  assert.plan(1);

  const target = shallow(<AddressComponent {...fakeRequiredProps} />);
  assert.true(target.instance().formValidates(), 'form does validate');
});

test('<Address />', assert => {
  assert.plan(2);

  const target = shallow(
    <AddressComponent
      {...{
        ...fakeRequiredProps,
        addressLine1: '',
        addressLine2: '',
        addressLine3: '',
        addressPostCode: '',
      }}
    />
  );
  assert.false(target.instance().formValidates(), 'form does not validate');
  assert.deepEqual(
    target.instance().state,
    {
      error: {
        addressLine1: 'Please enter a valid address line 1',
        addressLine2: null,
        addressLine3: null,
        addressPostCode: null,
      },
    },
    'error strings are correct'
  );
});
test('<Address />', assert => {
  assert.plan(2);

  const target = shallow(
    <AddressComponent
      {...{
        ...fakeRequiredProps,
        addressLine1: 'addressLine1',
        addressLine2: '',
        addressLine3: '',
        addressPostCode: '',
      }}
    />
  );
  assert.false(target.instance().formValidates(), 'form does not validate');
  assert.deepEqual(
    target.instance().state,
    {
      error: {
        addressLine1: null,
        addressLine2: 'Please enter a valid address line 2',
        addressLine3: null,
        addressPostCode: null,
      },
    },
    'error strings are correct'
  );
});
test('<Address />', assert => {
  assert.plan(2);

  const target = shallow(
    <AddressComponent
      {...{
        ...fakeRequiredProps,
        addressLine1: 'addressLine1',
        addressLine2: 'addressLine2',
        addressLine3: '1234567890123456789012345678901',
        addressPostCode: '',
      }}
    />
  );
  assert.false(target.instance().formValidates(), 'form does not validate');
  assert.deepEqual(
    target.instance().state,
    {
      error: {
        addressLine1: null,
        addressLine2: null,
        addressLine3: 'Please enter a valid address line 3',
        addressPostCode: null,
      },
    },
    'error strings are correct'
  );
});
test('<Address />', assert => {
  assert.plan(2);

  const target = shallow(
    <AddressComponent
      {...{
        ...fakeRequiredProps,
        addressLine1: 'addressLine1',
        addressLine2: 'addressLine2',
        addressLine3: '',
        addressPostCode: '',
      }}
    />
  );
  assert.false(target.instance().formValidates(), 'form does not validate');
  assert.deepEqual(
    target.instance().state,
    {
      error: {
        addressLine1: null,
        addressLine2: null,
        addressLine3: null,
        addressPostCode: 'Please enter a valid postcode',
      },
    },
    'error strings are correct'
  );
});
