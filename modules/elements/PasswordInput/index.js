import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';
import Icon from '../Icon';
import Error from '../Error';

export default class PasswordInput extends Component {
  static propTypes = {
    onComplete: PropTypes.func.isRequired,
    isValid: PropTypes.func,
    errorsOnUpdate: PropTypes.bool,
    label: PropTypes.string,
    showErrors: PropTypes.bool,
  };

  static defaultProps = {
    isValid: () => {},
    label: 'Password',
    errorsOnUpdate: false,
    showErrors: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      password: '',
      showHideTitle: 'Show',
      passwordFieldType: 'password',
      hasErrors: false,
      errors: {
        password: false,
        password_reason: '',
        password_minChars: false,
        password_OneChar: false,
        password_OneNumber: false,
      },
    };
  }

  getPasswordErrorMsg = password => {
    let msg = '';
    if (password.length < 8) {
      msg = 'be 8 characters minimum';
    } else if (!/[a-zA-Z]/.test(password)) {
      msg = 'contain at least 1 letter';
    } else if (!/\d/.test(password)) {
      msg = 'contain at least 1 number';
    }
    return msg;
  };

  checkPasswordIsValid = password =>
    password.length >= 8 && /\d/.test(password) && /[a-zA-Z]/.test(password);

  showHidePass = () => {
    if (this.state.passwordFieldType === 'password') {
      this.setState({ passwordFieldType: 'text', showHideTitle: 'Hide' });
    } else if (this.state.passwordFieldType === 'text') {
      this.setState({ passwordFieldType: 'password', showHideTitle: 'Show' });
    }
  };

  validate = () => {
    this.validateErrors(this.state.password);
  };

  validateErrors = pass => {
    this.setState({
      errors: {
        ...this.state.errors,
        password_reason: this.getPasswordErrorMsg(pass),
      },
      hasErrors: !this.checkPasswordIsValid(pass),
    });
  };

  validateCheckboxes = pass => {
    this.setState({
      errors: {
        ...this.state.errors,
        password_minChars: pass.length >= 8,
        password_OneChar: /[a-zA-Z]/.test(pass),
        password_OneNumber: /\d/.test(pass),
      },
    });
  };

  proceed(password) {
    if (!this.checkPasswordIsValid(password)) {
      this.props.isValid(false);
    } else {
      this.props.isValid(true);
      this.props.onComplete(password);
    }
  }

  render() {
    return (
      <div className="password-input__container">
        <p className="show-hide-pass" onClick={() => this.showHidePass()}>
          {this.state.showHideTitle}
        </p>
        <Input
          dataTest="password-input"
          onChange={e => {
            const v = e.target.value;
            this.setState({ password: v });
            this.props.errorsOnUpdate && this.validateErrors(v);
            this.validateCheckboxes(v);
            this.proceed(v);
          }}
          label={this.props.label}
          value={this.state.password}
          type={this.state.passwordFieldType}
          wrapperClassName={'password-input__input'}
          error={this.props.showErrors && this.state.hasErrors}
        />
        {this.props.showErrors && <Error
          compact
          title={`Your password needs to ${this.state.errors.password_reason}`}
          active={this.state.hasErrors}
          type={this.state.hasErrors ? 'active' : ''}
        />}
        <div className="password-conditions">
          <div className="password-conditions__item">
            <Icon
              name="check"
              className={`password-conditions__check ${
                this.state.errors.password_minChars
                  ? 'password-conditions__check--checked'
                  : ''
              }`}
            />
            <p className="password-conditions__title">8 characters minimum</p>
          </div>
          <div className="password-conditions__item">
            <Icon
              name="check"
              className={`password-conditions__check ${
                this.state.errors.password_OneChar
                  ? 'password-conditions__check--checked'
                  : ''
              }`}
            />
            <p className="password-conditions__title">At least 1 letter</p>
          </div>
          <div className="password-conditions__item">
            <Icon
              name="check"
              className={`password-conditions__check ${
                this.state.errors.password_OneNumber
                  ? 'password-conditions__check--checked'
                  : ''
              }`}
            />
            <p className="password-conditions__title">At least 1 number</p>
          </div>
        </div>
      </div>
    );
  }
}
