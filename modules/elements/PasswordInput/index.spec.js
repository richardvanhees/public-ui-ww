import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';

import PasswordInput from './index';

const sandbox = sinon.sandbox.create();
const onCompleteStub = sandbox.stub();
const isValidStub = sandbox.stub();
const isValidSpy = sandbox.spy(isValidStub);

const fakeRequiredProps = {
  onComplete: onCompleteStub,
  isValid: isValidSpy,
  errorsOnUpdate: true,
};

test('<PasswordInput> propTypes', assert => {
  assert.plan(1);
  assert.equals(
    typeof PasswordInput.propTypes,
    'object',
    'PropTypes are defined'
  );
});

test('<PasswordInput> input box', assert => {
  assert.plan(1);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  const target = wrapper.find('.input-wrapper').length;
  assert.equals(target, 1, 'input-wrapper class is present is set as default');
});

test('<PasswordInput> input box - default change', assert => {
  assert.plan(2);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  wrapper.find('.input-wrapper__input input').simulate('change');

  assert.deepEqual(onCompleteStub.callCount, 0, 'onComplete is not called');
  assert.true(
    isValidSpy.calledWith(false),
    'isValidStub is called with false at change event'
  );
});

test('<PasswordInput> input box - minimum one char', assert => {
  assert.plan(3);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  wrapper
    .find('.input-wrapper__input input')
    .simulate('change', { target: { value: 'a' } });

  assert.deepEqual(onCompleteStub.callCount, 0, 'onComplete is not called');
  assert.true(
    isValidSpy.calledWith(false),
    'isValidStub is called with false at change event'
  );
  assert.equal(wrapper.find('.password-conditions__check--checked').length, 2);
});

test('<PasswordInput> input box - minimum one number', assert => {
  assert.plan(3);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  wrapper
    .find('.input-wrapper__input input')
    .simulate('change', { target: { value: 'a2' } });

  assert.deepEqual(onCompleteStub.callCount, 0, 'onComplete is not called');
  assert.true(
    isValidSpy.calledWith(false),
    'isValidStub is called with false at change event'
  );
  assert.equal(wrapper.find('.password-conditions__check--checked').length, 4);
});

test('<PasswordInput> input box - minimum one number (not supplied)', assert => {
  assert.plan(3);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  wrapper
    .find('.input-wrapper__input input')
    .simulate('change', { target: { value: 'aa' } });

  assert.deepEqual(onCompleteStub.callCount, 0, 'onComplete is not called');
  assert.true(
    isValidSpy.calledWith(false),
    'isValidStub is called with false at change event'
  );
  assert.equal(wrapper.find('.password-conditions__check--checked').length, 2);
});

test('<PasswordInput> input box - valid password', assert => {
  assert.plan(3);
  const wrapper = mount(<PasswordInput {...fakeRequiredProps} />);
  wrapper
    .find('.input-wrapper__input input')

    .simulate('change', { target: { value: 'aaaaaaa1' } });

  assert.deepEqual(onCompleteStub.callCount, 1, 'onComplete is called');
  assert.true(
    isValidSpy.calledWith(true),
    'isValidStub is called with false at change event'
  );
  assert.equal(wrapper.find('.password-conditions__check--checked').length, 6);
});

test('<PasswordInput> input box - invalid password, do not show errors', assert => {
  assert.plan(2);
  const fakeProps = {...fakeRequiredProps, errorsOnUpdate: false}
  const wrapper = mount(<PasswordInput {...fakeProps} />);
  wrapper
    .find('.input-wrapper__input input')
    .simulate('change', { target: { value: 'a' } });

  assert.true(
    isValidSpy.calledWith(false),
    'isValidStub is called with false at change event'
  );
  assert.equal(wrapper.find('.input--error').length, 0);
});
