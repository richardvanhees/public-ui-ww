import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import PasswordInput from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('PasswordInput', module)
  .addDecorator(story => (
    <div style={{ width: '400px' }}>
      {story()}
    </div>
  ))
  .add('Required', () => <PasswordInput onComplete={action('onComplete')} />)
  .add('isValid', () => <PasswordInput onComplete={action('onComplete')} isValid={action('isValid')} />)
  .add('pastOnly', () => <PasswordInput name="Example" pastOnly label="Example" dataTest="example-data-test" onChange={action('changed')} />);
