import React from 'react';
import * as images from '../../../assets/images/iw-images';

export default () => (
  <div className="image-loader" style={{ display: 'none' }}>
    {Object.values(images).map((image, i) => (
      <img
        key={`preloaded-image-${i}`}
        src={image}
        alt={`Preloaded ${image}`}
      />
    ))}
  </div>
);
