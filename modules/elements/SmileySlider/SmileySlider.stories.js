import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import SmileySlider from './index';
import '../../../assets/styles/common/index.scss';

const sampleData = [
  {
    value: 'poor',
    smiley: 'frown',
    colour: 'red',
  },
  {
    value: 'reasonable',
    smiley: 'meh',
    colour: 'blue',
  },
  {
    value: 'good',
    smiley: 'smile',
    colour: 'green',
  },
];

storiesOf('SmileySlider', module)
  .add('Required', () => (<SmileySlider values={sampleData} onChange={action('onChange')} />));
