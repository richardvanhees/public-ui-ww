import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import Icon from '../Icon';

const SmileySlider = ({ values, onChange, userInput }) => (
  <div className="smiley-slider">
    <Slider
      min={0}
      max={values.length - 1}
      defaultValue={
        userInput ? values.findIndex(item => item.value === userInput) : 1
      }
      handle={props => (
        <div
          className="smiley-slider__handle"
          style={{ left: `${props.offset}%` }}
        >
          <Icon
            style={{ color: values[props.value].colour }}
            className="smiley-slider__handle-icon"
            name={values[props.value].smiley}
            iconSet="regular"
          />
        </div>
      )}
      onAfterChange={val => onChange(values[val].value)}
    />
  </div>
);

SmileySlider.propTypes = {
  values: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      colour: PropTypes.string.isRequired,
      smiley: PropTypes.string.isRequired,
    })
  ),
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string, // Lint seems to want this one, even though it is not in use or passed, anywhere
  userInput: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
};

export default SmileySlider;
