import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SmileySlider from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  onChange: onChangeStub,
  values: [
    {
      value: 'poor',
      colour: '#4DB39A',
      smiley: 'frown',
    },
    {
      value: 'reasonable',
      colour: '#367DA9',
      smiley: 'meh',
    },
    {
      value: 'good',
      colour: '#844196',
      smiley: 'smile',
    },
  ],
};

test('<SmileySlider>', t => {
  const component = mount(<SmileySlider {...fakeRequiredProps} />);

  t.equals(typeof SmileySlider.propTypes, 'object', 'PropTypes are defined');

  component
    .find('.smiley-slider__handle')
    .first()
    .simulate('mouseup');
  t.deepEqual(
    onChangeStub.called,
    true,
    'onChange is called when handle is dragged'
  );

  t.end();
});
