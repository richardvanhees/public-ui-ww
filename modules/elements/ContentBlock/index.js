import React from 'react';
import PropTypes from 'prop-types';

const ContentBlock = ({ children, className, title, style, centerContent }) => (
  <div className={`content-block ${className}`} style={style}>
    {title && <h3 className="content-block__title">{title}</h3>}
    <div
      className={`content-block__content ${
        centerContent ? 'content-block__content--centered' : ''
      }`}
    >
      {children}
    </div>
  </div>
);

ContentBlock.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  title: PropTypes.string,
  style: PropTypes.object,
  centerContent: PropTypes.bool,
};

ContentBlock.defaultProps = {
  className: '',
  centerContent: false,
};

export default ContentBlock;
