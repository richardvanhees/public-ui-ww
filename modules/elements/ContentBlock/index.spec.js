import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContentBlock from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<ContentBlock>', t => {
  t.equals(typeof ContentBlock.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ContentBlock>', t => {
  const component = shallow(<ContentBlock>testChildren</ContentBlock>);

  t.equals(component.find('.content-block__title').length, 0, 'No title is rendered if not given');

  t.end();
});

test('<ContentBlock>', t => {
  const component = shallow(<ContentBlock title={'test'}>testChildren</ContentBlock>);

  t.equals(component.find('.content-block__title').length, 1, 'Title is rendered if given');

  t.end();
});

test('<ContentBlock>', t => {
  const component = shallow(<ContentBlock centerContent>testChildren</ContentBlock>);

  t.equals(component.find('.content-block__content--centered').length, 1, 'Content is centered if true');

  t.end();
});
