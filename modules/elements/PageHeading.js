import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

const PageHeading = ({
  title,
  subTitle,
  largerSpacing,
  image,
  smallTitle,
  smallSubTitle,
  imageClassName,
  icon,
  subTitleClassName,
  iconClassName,
  children,
  dataTest,
  titleClassName,
  iconSet,
}) => (
  <div
    className={`page-heading ${
      largerSpacing ? 'page-heading--larger-spacing' : ''
    } `}
  >
    {image && (
      <img
        className={`page-heading__image ${imageClassName}`}
        src={image}
        alt={'header'}
      />
    )}
    {icon && (
      <Icon name={icon} iconSet={iconSet} className={`page-heading__icon ${iconClassName}`} />
    )}
    <h1
      className={`page-heading__title ${
        smallTitle ? 'page-heading__title--small' : ''
      } ${titleClassName}`}
      data-test={dataTest ? `${dataTest}-heading` : 'page-heading'}
    >
      {title}
    </h1>
    <h3
      className={`page-heading__subtitle ${
        smallSubTitle ? 'page-heading__subtitle--small' : ''
      } ${subTitleClassName}`}
      data-test={dataTest ? `${dataTest}-subheading` : 'page-subheading'}
    >
      {subTitle || children}
    </h3>
  </div>
);

PageHeading.propTypes = {
  title: PropTypes.node,
  subTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  largerSpacing: PropTypes.bool,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  imageClassName: PropTypes.string,
  smallSubTitle: PropTypes.bool,
  smallTitle: PropTypes.bool,
  icon: PropTypes.string,
  subTitleClassName: PropTypes.string,
  iconClassName: PropTypes.string,
  children: PropTypes.node,
  dataTest: PropTypes.string,
  titleClassName: PropTypes.string,
  iconSet: PropTypes.string,
};

PageHeading.defaultProps = {
  imageClassName: '',
  subTitleClassName: '',
  iconClassName: '',
  titleClassName: '',
  iconSet: 'solid',
};

export default PageHeading;
