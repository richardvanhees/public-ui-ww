import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

const ContactDetails = ({ type, value, description }) => (
  <div className="contact-details">
    <div className="contact-details__icon">
      <Icon name={type === 'email' ? 'envelope' : type} />
    </div>
    <div className="contact-details__content">
      <a
        href={`${type === 'email' ? 'mailto' : 'tel'}:${value}`}
        className="contact-details__value"
      >
        {value}
      </a>
      <div className="contact-details__description">{description}</div>
    </div>
  </div>
);

ContactDetails.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  description: PropTypes.string,
};

export default ContactDetails;
