import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContactDetails from './index';
import Icon from '../Icon';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<ContactDetails>', t => {
  t.equals(typeof ContactDetails.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ContactDetails>', t => {
  const iconName = mount(<ContactDetails type={'email'} value={'test@email.com'} />)
    .find(Icon)
    .props()
    .name;

  t.equals(iconName, 'envelope', 'Envelope icon is shown when type is email');
  t.end();
});

test('<ContactDetails>', t => {
  const iconName = mount(<ContactDetails type={'phone'} value={'123456789'} />)
    .find(Icon)
    .props()
    .name;

  t.equals(iconName, 'phone', 'Phone icon is shown when type is phone');
  t.end();
});