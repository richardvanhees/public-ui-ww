import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PageNavigation from './index';
import sinon from 'sinon';
import Button from '../Button';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const actionStub = sandbox.stub();
const backStub = sandbox.stub();
const nextStub = sandbox.stub();

const fakeRequiredProps = {
  next: nextStub,
  back: backStub,
};

test('<PageNavigation>', t => {
  t.equals(typeof PageNavigation.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} />);
  wrapper.find(Button).first().simulate('click');

  t.equals(backStub.called, true, 'back called');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} />);
  wrapper.find(Button).last().simulate('click');

  t.equals(nextStub.called, true, 'next called');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} wideButtons />);

  t.equals(wrapper.find(Button).first().hasClass('btn--wider'), true, 'wider back button');
  t.equals(wrapper.find(Button).last().hasClass('btn--wider'), true, 'wider next button');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} noSideMargins />);

  t.equals(wrapper.find('.button-container').hasClass('button-container--no-side-margins'), true, 'No side margins');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} noTopMargin />);

  t.equals(wrapper.find('.button-container').hasClass('button-container--no-top-margin'), true, 'No top margin');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} hasDisabledState back={false} next={false} />);

  t.equals(wrapper.find(Button).first().hasClass('btn--disabled'), true, 'Disabled back button');
  t.equals(wrapper.find(Button).last().hasClass('btn--disabled'), true, 'Disabled next button');

  t.end();
});

test('<PageNavigation>', t => {
  const wrapper = mount(<PageNavigation {...fakeRequiredProps} centerButtons />);

  t.equals(wrapper.find('.button-container').hasClass('button-container--center-buttons'), true, 'Buttons are centered');

  t.end();
});