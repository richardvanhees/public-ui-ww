import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import Icon from '../Icon';

const PageNavigation = ({
  back,
  next,
  nextLabel,
  backLabel,
  wideButtons,
  noSideMargins,
  noTopMargin,
  testNext,
  testBack,
  className,
  hasDisabledState,
  centerButtons,
}) => (
  <div
    className={`
      button-container
      ${centerButtons ? 'button-container--center-buttons' : ''}
      ${noSideMargins ? 'button-container--no-side-margins' : ''}
      ${noTopMargin ? 'button-container--no-top-margin' : ''}
      ${className}
    `}
  >
    {(back || hasDisabledState) && (
      <Button
        className={`
          button-container__btn
          btn--outline
          ${wideButtons ? 'btn--wider' : ''}
          ${hasDisabledState && !back ? 'btn--disabled' : ''}
        `}
        label={backLabel}
        onClick={back}
        dataTest={`back-btn-${testBack}`}
      />
    )}
    {(next || hasDisabledState) && (
      <Button
        className={`
          button-container__btn
          ${!centerButtons ? 'button-container__right-button' : ''}
          btn--primary
          ${wideButtons ? 'btn--wider' : ''}
          ${hasDisabledState && !next ? 'btn--disabled' : ''}
        `}
        label={nextLabel}
        onClick={next}
        dataTest={`next-btn-${testNext}`}
      />
    )}
  </div>
);

PageNavigation.propTypes = {
  next: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  back: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  nextLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  backLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  noSideMargins: PropTypes.bool,
  noTopMargin: PropTypes.bool,
  wideButtons: PropTypes.bool,
  testNext: PropTypes.string,
  testBack: PropTypes.string,
  className: PropTypes.string,
  hasDisabledState: PropTypes.bool,
  centerButtons: PropTypes.bool,
};

PageNavigation.defaultProps = {
  nextLabel: (
    <div>
      Next <Icon name="angle-right" />
    </div>
  ),
  backLabel: (
    <div>
      <Icon name="angle-left" /> Back
    </div>
  ),
  noSideMargins: false,
  noTopMargin: false,
  className: '',
  hasDisabledState: false,
  centerButtons: false,
};

export default PageNavigation;
