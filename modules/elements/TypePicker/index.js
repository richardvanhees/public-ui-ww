import React from 'react';
import PropTypes from 'prop-types';

import LineItem from './LineItem';
import TypeSlider from '../TypeSlider';
import TypeDescription from './TypeDescription';

const TypePicker = ({
  types,
  descriptionIntro,
  selectedLevel,
  onChange,
  isMobile,
  level,
  readMore,
  popupToggle,
  manualOverrideEnabled,
}) => (
  <div className="type-picker">
    {isMobile && (
      <TypeSlider enabled={manualOverrideEnabled} labels={types} selected={selectedLevel} onChange={onChange} />
    )}

    {!isMobile && (
      <div className="type-picker__type-line">
        <div className="type-picker__connector" />
        <div className="type-picker__type-line__items">
          {types.map((type, i) => (
            <LineItem
              key={`type-line-item-${i}`}
              dataTest={`type-line-item-${i}`}
              label={type.name}
              selected={selectedLevel === type.name}
              enabled={manualOverrideEnabled}
              onClick={onChange}
            />
          ))}
        </div>
      </div>
    )}

    {types.map((type, i) => (
      <TypeDescription
        key={`type-description-${i}`}
        header={level}
        descriptionIntro={descriptionIntro}
        description={type.description}
        selected={selectedLevel === type.name}
        index={i}
        totalTypes={types.length}
        isMobile={isMobile}
        readMore={readMore}
        popupToggle={popupToggle}
      />
    ))}
  </div>
);

TypePicker.propTypes = {
  types: PropTypes.array.isRequired,
  selectedLevel: PropTypes.string.isRequired,
  descriptionIntro: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  isMobile: PropTypes.bool.isRequired,
  level: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  readMore: PropTypes.node,
  popupToggle: PropTypes.func.isRequired,
  manualOverrideEnabled: PropTypes.bool,
};

TypePicker.defaultProps = {
  manualOverrideEnabled: true,
};

export default TypePicker;
