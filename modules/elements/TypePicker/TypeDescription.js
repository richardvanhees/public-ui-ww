import React from 'react';
import PropTypes from 'prop-types';

const TypeDescription = ({
  index,
  totalTypes,
  descriptionIntro,
  header,
  description,
  selected,
  isMobile,
  readMore,
  popupToggle,
}) => {
  const offset = index + 1 === totalTypes ? 32 : index * 8;
  let top = !index ? 2 : 100 * (index / (totalTypes - 1));

  if (index + 1 === totalTypes) {
    top = 98;
  }

  return (
    selected && (
      <div className={'type-description'}>
        {!isMobile && (
          <div
            className={'type-description__arrow'}
            style={{ top: `calc(${top}% - ${offset}px)` }}
          />
        )}

        <div className={'type-description__description-intro'}>
          {descriptionIntro}
        </div>
        <div className={'type-description__header'}>{header}</div>
        <div className={'type-description__description'}>{description}</div>
        {readMore && (
          <a onClick={popupToggle} className={'type-description__read-more'}>
            Read more
          </a>
        )}
      </div>
    )
  );
};

TypeDescription.propTypes = {
  index: PropTypes.number.isRequired,
  totalTypes: PropTypes.number.isRequired,
  descriptionIntro: PropTypes.string,
  isMobile: PropTypes.bool.isRequired,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
    .isRequired,
  selected: PropTypes.bool,
  readMore: PropTypes.node,
  popupToggle: PropTypes.func.isRequired,
};

export default TypeDescription;
