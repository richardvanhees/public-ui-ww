import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class LineItem extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    dataTest: PropTypes.string.isRequired,
    selected: PropTypes.bool,
    enabled: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
  };

  static defaultProps = {
    enabled: true,
  };

  constructor(props) {
    super(props);

    this.circleClicked = this.circleClicked.bind(this);

    this.state = {
      hovered: false,
      clicked: false,
    };
  }

  circleClicked() {
    if (this.props.enabled) {
      this.props.onClick(this.props.label);
      this.setState({ hovered: false, clicked: true });
    }
  }

  render() {
    const { label, selected, enabled, dataTest } = this.props;
    const { hovered, clicked } = this.state;

    return (
      <div className={'line-item'}>
        <div
          className={`line-item__circle${
            selected || enabled ? '' : ' line-item__circle-disabled'
          }`}
          data-test={dataTest}
          onClick={this.circleClicked}
          onMouseEnter={() => enabled && this.setState({ hovered: !selected })}
          onMouseLeave={() =>
            enabled && this.setState({ hovered: false, clicked: false })
          }
        >
          <div
            className={`line-item__inner-circle ${
              selected ? 'line-item__inner-circle--selected' : ''
            } ${
              hovered && !clicked && enabled
                ? 'line-item__inner-circle--hovered'
                : ''
            }`}
          />
        </div>
        <div
          className={`line-item__label ${
            selected ? 'line-item__label--selected' : ''
          }`}
        >
          {label.charAt(0).toUpperCase() + label.slice(1)}
        </div>
      </div>
    );
  }
}
