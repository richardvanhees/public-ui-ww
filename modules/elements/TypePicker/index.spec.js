import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TypePicker from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<TypePicker>', t => {
  t.equals(typeof TypePicker.propTypes, 'object', 'PropTypes are defined');

  t.end();
});
