import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LineItem from './LineItem';

test('<LineItem>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onClickStub = sandbox.stub();

  const fakeRequiredProps = {
    label: 'label',
    dataTest: 'data test',
    selected: false,
    enabled: false,
    onClick: onClickStub,
  };

  const target = mount(<LineItem {...fakeRequiredProps} />);

  const wrapperDiv = target.find('div[data-test="data test"]');

  assert.equals(
    wrapperDiv.prop('className'),
    'line-item__circle line-item__circle-disabled',
    'disabled class set correctly'
  );
});

test('<LineItem>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onClickStub = sandbox.stub();

  const fakeRequiredProps = {
    label: 'label',
    dataTest: 'data test',
    selected: true,
    enabled: true,
    onClick: onClickStub,
  };

  const target = mount(<LineItem {...fakeRequiredProps} />);

  const wrapperDiv = target.find('div[data-test="data test"]');

  assert.equals(
    wrapperDiv.prop('className'),
    'line-item__circle',
    'disabled class not set'
  );
});
