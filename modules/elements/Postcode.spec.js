import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Postcode, { __RewireAPI__ } from './Postcode';
import _ from 'lodash';
import R from 'ramda';

test('<Postcode/> component', t => {
  const fakeBaseProps = {
    textInputVisible: true,
    textInputLabel: 'fake input label',
    textInputName: 'fake input name',
    textInputDataTest: 'fake input data test',
    textInputPlaceholder: 'fake input plcaeholder',
    textInputInitialValue: 'fake input initial value',
    textInputOnChange: sinon.spy(),
    textInputDisabled: false,
    buttonClassName: 'fake button class name',
    buttonName: 'fake button name',
    buttonDataTest: 'fake button data test',
    buttonTextInitial: 'fake button text initial',
    buttonTextSearching: 'fake button text searching',
    buttonVisible: true,
    isSearchingPostcode: false,
    sendPostcodeSearchRequest: sinon.spy(),
    sendPostcodeSearchReset: sinon.spy(),
  };

  t.test('Input: maps props (with dataTest)', t => {
    t.plan(6);

    const fakeProps = fakeBaseProps;
    const target = shallow(<Postcode {...fakeProps} />);

    t.equals(target.find('.itemLabel').text(), fakeProps.textInputLabel);
    t.equals(target.find('input').prop('name'), fakeProps.textInputName);
    t.equals(
      target.find('input').prop('placeholder'),
      fakeProps.textInputPlaceholder
    );
    t.equals(
      target.find('input').prop('data-test'),
      fakeProps.textInputDataTest
    );
    t.equals(
      target.find('input').prop('disabled'),
      fakeProps.textInputDisabled
    );
    t.equal(
      target.find('input').prop('value'),
      fakeProps.textInputInitialValue
    );
  });

  t.test('Input: maps props (without dataTest)', t => {
    t.plan(6);

    const fakeProps = R.omit('textInputDataTest', fakeBaseProps);
    const target = shallow(<Postcode {...fakeProps} />);

    t.equals(target.find('.itemLabel').text(), fakeProps.textInputLabel);
    t.equals(target.find('input').prop('name'), fakeProps.textInputName);
    t.equals(
      target.find('input').prop('placeholder'),
      fakeProps.textInputPlaceholder
    );
    t.equals(target.find('input').prop('data-test'), fakeProps.textInputName);
    t.equals(
      target.find('input').prop('disabled'),
      fakeProps.textInputDisabled
    );
    t.equal(
      target.find('input').prop('value'),
      fakeProps.textInputInitialValue
    );
  });

  t.test('Input: does not error when textInputOnChange prop not defined', t => {
    t.plan(1);

    const fakeProps = R.omit('textInputOnChange', fakeBaseProps);
    const target = shallow(<Postcode {...fakeProps} />);
    const value = 'expected value';

    t.doesNotThrow(
      () => target.find('input').simulate('change', { target: { value } }),
      'no error thrown'
    );
  });

  t.test('Input: calls textInputOnChange prop when defined', t => {
    const clock = sinon.useFakeTimers();
    const lodash = _;

    t.plan(2);

    const fakeProps = fakeBaseProps;

    __RewireAPI__.__Rewire__('_', lodash);

    const target = shallow(<Postcode {...fakeProps} />);

    target
      .find('input')
      .simulate('change', { target: { value: 'something else' } });

    const value = 'expected value';

    target.find('input').simulate('change', { target: { value } });

    clock.tick(499);

    t.equal(fakeProps.textInputOnChange.callCount, 0);

    clock.tick(501);

    t.equal(fakeProps.textInputOnChange.callCount, 1);

    clock.restore();

    __RewireAPI__.__ResetDependency__('_');
  });

  t.test('Input: refreshes state when props change for initial value', t => {
    t.plan(1);

    const fakeProps = fakeBaseProps;

    const target = shallow(<Postcode {...fakeProps} />);

    const expectedValue = 'expected value';

    target.setProps({ textInputInitialValue: expectedValue });

    t.equal(target.find('input').prop('value'), expectedValue);
  });

  t.test(
    'Input: doesnt refresh state when props change for initial value is identical',
    t => {
      t.plan(1);

      const fakeProps = fakeBaseProps;

      const target = shallow(<Postcode {...fakeProps} />);

      const expectedValue = 'expected value';

      target.setState({ textInputValue: expectedValue });

      target.setProps({
        textInputInitialValue: fakeProps.textInputInitialValue,
      });

      t.equal(target.find('input').prop('value'), expectedValue);
    }
  );

  t.test('Button: maps button props (with dataTest)', t => {
    t.plan(3);

    const fakeProps = fakeBaseProps;
    const target = shallow(<Postcode {...fakeProps} />);

    t.equals(
      target.find('.buttonContainer a').prop('className'),
      fakeProps.buttonClassName
    );
    t.equals(
      target.find('.buttonContainer a').prop('name'),
      fakeProps.buttonName
    );
    t.equals(
      target.find('.buttonContainer a').prop('data-test'),
      fakeProps.buttonDataTest
    );
  });

  t.test('Button: maps button props (without dataTest)', t => {
    t.plan(3);

    const fakeProps = R.omit('buttonDataTest', fakeBaseProps);
    const target = shallow(<Postcode {...fakeProps} />);

    t.equals(
      target.find('.buttonContainer a').prop('className'),
      fakeProps.buttonClassName
    );
    t.equals(
      target.find('.buttonContainer a').prop('name'),
      fakeProps.buttonName
    );
    t.equals(
      target.find('.buttonContainer a').prop('data-test'),
      fakeProps.buttonName
    );
  });

  t.test('Button: calls sendPostcodeSearchRequest prop when clicked', t => {
    t.plan(2);

    const fakeProps = fakeBaseProps;

    const target = shallow(<Postcode {...fakeProps} />);

    t.equal(fakeProps.sendPostcodeSearchRequest.callCount, 0);

    target.find('.buttonContainer a').simulate('click');

    t.equal(fakeProps.sendPostcodeSearchRequest.callCount, 1);
  });

  t.test(
    'Button: calls text is buttonTextSearching when isSearchingPostcode is true',
    t => {
      t.plan(2);

      const fakeProps = fakeBaseProps;

      const target = shallow(<Postcode {...fakeProps} />);

      t.equal(
        target.find('.buttonContainer a').text(),
        fakeProps.buttonTextInitial
      );

      target.setProps({ isSearchingPostcode: true });

      t.equal(
        target.find('.buttonContainer a').text(),
        fakeProps.buttonTextSearching
      );
    }
  );

  t.test('Dropdown: only displayed if sendPostcodeSearchResponse is set', t => {
    t.plan(2);

    const fakeProps = fakeBaseProps;

    const target = shallow(<Postcode {...fakeProps} />);

    t.equal(target.find('.inputDropdown').length, 0);

    target.setProps({
      sendPostcodeSearchResponse: [
        { Id: 9999, Type: 'fake type', Text: 'fake text' },
      ],
    });

    t.equal(target.find('.inputDropdown').length, 1);
  });

  t.test(
    'Dropdown: calls sendPostcodeSearchReset prop when close is clicked',
    t => {
      t.plan(2);

      const fakeProps = fakeBaseProps;

      const target = shallow(<Postcode {...fakeProps} />);

      target.setProps({
        sendPostcodeSearchResponse: [
          { Id: 9999, Type: 'fake type', Text: 'fake text' },
        ],
      });

      t.equal(fakeProps.sendPostcodeSearchReset.callCount, 0);

      target.find('.responseContainer .close-button').simulate('click');

      t.equal(fakeProps.sendPostcodeSearchReset.callCount, 1);
    }
  );

  t.test('component obeys visible prop', t => {
    const fakeProps = fakeBaseProps;

    const targetDefault = shallow(<Postcode {...fakeProps} />);
    const targetVisible = shallow(
      <Postcode {...fakeProps} textInputVisible={true} />
    );
    const targetNotVisible = shallow(
      <Postcode {...fakeProps} textInputVisible={false} />
    );

    t.notEqual(targetDefault.html(), null, 'Default rendered');
    t.notEqual(targetVisible.html(), null, 'Visible rendered');
    t.equals(targetNotVisible.html(), null, 'Not visible not rendered');

    t.end();
  });
});
