import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';

class Postcode extends PureComponent {
  static propTypes = {
    textInputVisible: PropTypes.bool,
    textInputLabel: PropTypes.string,
    textInputName: PropTypes.string.isRequired,
    textInputDataTest: PropTypes.string,
    textInputPlaceholder: PropTypes.string,
    textInputInitialValue: PropTypes.string,
    textInputOnChange: PropTypes.func,
    textInputOnBlur: PropTypes.func,
    textInputDisabled: PropTypes.bool,
    buttonClassName: PropTypes.string,
    buttonName: PropTypes.string.isRequired,
    buttonDataTest: PropTypes.string,
    buttonTextInitial: PropTypes.string,
    buttonTextSearching: PropTypes.string,
    buttonVisible: PropTypes.bool,
    isSearchingPostcode: PropTypes.bool.isRequired,
    sendPostcodeSearchRequest: PropTypes.func.isRequired,
    sendPostcodeSearchSelect: PropTypes.func.isRequired,
    sendPostcodeSearchResponse: PropTypes.array,
    sendPostcodeSearchReset: PropTypes.func.isRequired,
  };

  static defaultProps = {
    textInputVisible: true,
    buttonTextInitial: 'Find Address',
    buttonTextSearching: 'Searching...',
    buttonVisible: true,
  };

  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);

    this.state = { textInputValue: props.textInputInitialValue };
  }

  componentDidMount() {
    this.handleChangeDebounced = debounce(() => {
      if (typeof this.props.textInputOnChange === 'function') {
        this.props.textInputOnChange(this.state.textInputValue);
      }
    }, 500);
  }

  componentDidUpdate = prevProps => {
    const { props } = this;

    if (prevProps.textInputInitialValue !== props.textInputInitialValue) {
      this.setState({ textInputValue: props.textInputInitialValue });
    }
  };

  onChange = e => {
    this.setState({ textInputValue: e.target.value });
    this.handleChangeDebounced();
  };

  onClickDropdownItem = e => {
    const id = e.target.dataset.id;
    const type = e.target.dataset.type;
    const text = e.target.textContent;
    const { props } = this;
    props.sendPostcodeSearchSelect({ type, id, text });
  };

  onClickDropdownClose = () => {
    const { props } = this;
    props.sendPostcodeSearchReset();
  };

  getDropdownItemName = addressItem =>
    `${addressItem.Text} (${addressItem.Description})`;

  renderCloseButton = sendPostcodeSearchResponse => {
    if (typeof sendPostcodeSearchResponse === 'undefined') {
      return null;
    }

    if (!sendPostcodeSearchResponse) {
      return <span className="close">X</span>;
    }

    return (
      <span
        className="close-button call-to-action"
        onClick={this.onClickDropdownClose}
      >
        X
      </span>
    );
  };

  renderDropdown = sendPostcodeSearchResponse => {
    if (typeof sendPostcodeSearchResponse === 'undefined') {
      return null;
    }

    return (
      <div className="responseContainer">
        {this.renderCloseButton(this.props.sendPostcodeSearchResponse)}
        <ul className="inputDropdown">
          {!sendPostcodeSearchResponse ? (
            <li className="addressItem-empty">No results found</li>
          ) : (
            sendPostcodeSearchResponse.map(this.renderDropdownItem)
          )}
        </ul>
      </div>
    );
  };

  renderDropdownItem = addressItem => (
    <li
      key={addressItem.Id}
      className="addressItem"
      data-id={addressItem.Id}
      data-type={addressItem.Type}
      onClick={this.onClickDropdownItem}
      data-test={this.getDropdownItemName(addressItem)}
    >
      {this.getDropdownItemName(addressItem)}
    </li>
  );

  render() {
    const { props, state } = this;

    return (
      props.textInputVisible && (
        <div className="item postcode-container">
          <label className="itemLabel">{props.textInputLabel}</label>

          <div className="itemPostcode">
            <div className="controlContainer">
              <div className="inputContainer">
                <input
                  type="text"
                  name={props.textInputName}
                  placeholder={props.textInputPlaceholder}
                  onChange={this.onChange}
                  value={state.textInputValue || ''}
                  data-test={props.textInputDataTest || props.textInputName}
                  disabled={props.textInputDisabled}
                  onBlur={props.textInputOnBlur}
                />
              </div>
              {this.renderDropdown(props.sendPostcodeSearchResponse)}
            </div>

            {props.buttonVisible && (
              <div className="buttonContainer">
                <a
                  className={props.buttonClassName}
                  name={props.buttonName}
                  data-test={props.buttonDataTest || props.buttonName}
                  onClick={props.sendPostcodeSearchRequest}
                >
                  {props.isSearchingPostcode
                    ? props.buttonTextSearching
                    : props.buttonTextInitial}
                </a>
              </div>
            )}
          </div>
        </div>
      )
    );
  }
}

export default Postcode;
