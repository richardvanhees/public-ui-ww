import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';

import Answer from './index';
import sinon from 'sinon';
import { questions } from '../../../server/money-wizard/db/setup/questions_v1';

const answerData = questions.questions[0].answers[0];

const sandbox = sinon.sandbox.create();
const onHoverStub = sandbox.stub();
const onClickStub = sandbox.stub();

const fakeRequiredProps = {
  ...answerData,
  onClick: onClickStub,
};

test('<Answer>', t => {
  t.equals(typeof Answer.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(<Answer {...fakeRequiredProps} />).find('.answer--has-image')
      .length,
    0,
    'Image not rendered when no image provided or on mobile'
  );

  t.equals(
    shallow(<Answer {...fakeRequiredProps} hasImages />).find(
      '.answer--has-image'
    ).length,
    1,
    'Image rendered when image provided'
  );

  t.equals(
    shallow(<Answer {...fakeRequiredProps} />).find('.answer--faded').length,
    0,
    'Answer not faded out when fadedOut is false'
  );

  t.equals(
    shallow(<Answer {...fakeRequiredProps} selected />).find(
      '.answer--selected'
    ).length,
    1,
    '"Selected" style applied if answer is selected before'
  );

  t.equals(
    shallow(<Answer {...fakeRequiredProps} />).find('.answer--selected').length,
    0,
    'Regular style applied if answer is not selected'
  );

  const wrapper = shallow(<Answer {...fakeRequiredProps} />);
  wrapper.simulate('click');
  t.deepEqual(
    onClickStub.args,
    [[{ answerId: 0, weighting: ['3', '0', '0', '0', '-30', '3', '0', '3'] }]],
    'onClick is called with answerId'
  );

  t.end();
});
