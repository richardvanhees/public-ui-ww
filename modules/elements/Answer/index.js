import React, { Component } from 'react';
import { propTypes, defaultProps } from './model';
import * as images from '../../../assets/images/answer-images';
import Icon from '../Icon';

export default class AnswerComponent extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.setAnswer = this.setAnswer.bind(this);
    this.createAnswerString = this.createAnswerString.bind(this);
  }

  setAnswer() {
    if (this.props.weighting) {
      this.props.onClick({
        answerId: this.props.id,
        weighting: this.props.weighting,
      });
    } else if (this.props.answerKey) {
      this.props.onClick({
        answerId: this.props.id,
        answerKey: this.props.answerKey,
        answerValue: this.props.value,
      });
    } else if (this.props.iconName) {
      this.props.onClick({
        answerId: this.props.id,
        goal: this.props.title,
        icon: this.props.iconName,
      });
    } else {
      this.props.onClick({
        answerId: this.props.id,
        length: this.props.length,
        lengthValue: this.props.lengthValue,
      });
    }
  }

  createAnswerString = data => (
    <p style={{ margin: 0 }}>
      {data.top && <span className="answer__label-top">{data.top}</span>}
      {data.top && <br />}
      {data.middle && (
        <span className="answer__label-middle">{data.middle}</span>
      )}
      {data.middle && <br />}
      {data.btm && <span className="answer__label-bottom">{data.btm}</span>}
    </p>
  );

  render() {
    const {
      id,
      questionId,
      answer,
      image,
      hasImages,
      hasIcons,
      hasIconsSmall,
      iconName,
      iconSet,
      selected,
      style,
      doubleSize,
      multiLineAnswer,
      className,
      dataTest,
    } = this.props;
    const imageUrl = images[`q${questionId}a${id}`];
    return (
      <button
        className={`
          answer
          ${selected ? `answer--selected ${className}--selected` : ''}
          ${hasImages ? 'answer--has-image' : ''}
          ${hasIcons ? 'answer--has-icons' : ''}
          ${hasIconsSmall ? 'answer--has-icons-small' : ''}
          ${doubleSize ? 'answer--large' : ''}
          ${className}
        `}
        style={style}
        data-test={dataTest || `answer-${id + 1}`}
        onClick={this.setAnswer}
      >
        {hasImages && (
          <div
            className="answer__image"
            style={{ background: image ? `url(${imageUrl})` : 'none' }}
          />
        )}
        {hasIcons &&
          iconName && (
            <div className="answer__icon">
              <Icon name={iconName} iconSet={iconSet} />
            </div>
          )}
        <div
          className={`answer__label ${
            hasImages ? 'answer__label--has-image' : ''
          } ${multiLineAnswer ? 'answer__label--multiline' : ''}`}
        >
          {!multiLineAnswer ? answer : this.createAnswerString(multiLineAnswer)}
        </div>
      </button>
    );
  }
}
