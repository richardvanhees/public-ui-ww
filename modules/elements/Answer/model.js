import PropTypes from 'prop-types';

export const propTypes = {
  id: PropTypes.number.isRequired,
  answer: PropTypes.string,
  image: PropTypes.string,
  onHover: PropTypes.func,
  hasImages: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  style: PropTypes.object,
  answerKey: PropTypes.string,
  className: PropTypes.string,
  dataTest: PropTypes.string,
};

export const defaultProps = {
  className: '',
};
