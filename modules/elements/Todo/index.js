import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import checkmark from '../../../assets/images/iw-images/checkmark-no-bg.png';
import Icon from '../Icon';
import Button from '../Button';
import ProgressBar from '../ProgressBar';

const ProgressSection = ({
  title,
  description,
  duration,
  buttonLabel,
  progress,
  color,
  enabled,
  action,
  completedAt,
  dataTest,
  hideCompletionDate,
  hideduration,
}) => {
  const completedText = at => {
    if (!at) return 'date unknown';
    const m = time =>
      moment(time)
        .startOf('day')
        .format('DD/MM/YYYY');
    const then = m(at);
    if (then === m()) return 'today';
    return then;
  };

  return (
    <div className="todo">
      {!enabled && <div className="todo__overlay" />}

      <div className="todo__icon-container">
        <div className="todo__circle" />
        {progress === 100 && (
          <img className="todo__check" src={checkmark} alt={'checkmark'} />
        )}
      </div>

      <div className="todo__description-container">
        <div className="todo__title">{title}</div>
        {description && <div
          className="todo__description"
          data-test={`${dataTest}-description`}
          style={{ color }}
        >
          {description}
        </div>}
      </div>

      <div className="todo__progress-container">
        {!hideduration &&
          progress === 0 && (
            <div className="todo__duration">
              <Icon name={'clock'} iconSet={'regular'} /> {duration} minutes
            </div>
          )}

        {progress > 0 &&
          progress < 100 && (
            <ProgressBar
              className="todo__progress-bar"
              progress={progress}
              color={color}
            />
          )}

        {!hideCompletionDate &&
          progress === 100 && (
            <div className={'todo__completed'} style={{ borderColor: color }}>
              Completed {completedText(completedAt)}
            </div>
          )}

        {buttonLabel && (
          <Button
            label={buttonLabel}
            style={{
              color,
              border: `1px solid ${color}`,
            }}
            className="todo__button"
            onClick={enabled && action}
            invertOnHover
            dataTest={dataTest}
          />
        )}
      </div>
    </div>
  );
};

ProgressSection.propTypes = {
  title: PropTypes.node.isRequired,
  description: PropTypes.node,
  duration: PropTypes.number,
  buttonLabel: PropTypes.node,
  progress: PropTypes.number,
  color: PropTypes.string.isRequired,
  enabled: PropTypes.bool,
  action: PropTypes.func,
  completedAt: PropTypes.string,
  isMobile: PropTypes.bool,
  dataTest: PropTypes.string.isRequired,
  hideCompletionDate: PropTypes.bool,
  hideduration: PropTypes.bool,
};

ProgressSection.defaultProps = {
  enabled: true,
  hideCompletionDate: false,
  hideduration: false,
};

export default ProgressSection;
