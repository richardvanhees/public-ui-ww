import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Todo from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Todo>', t => {

  t.equals(typeof Todo.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Todo>', t => {

  t.equals(shallow(<Todo {...fakeRequiredProps} />).find('.todo').length, 1, 'Todo is rendered');

  t.end();
});
