import React from 'react';
import PropTypes from 'prop-types';

const ListBlock = ({ children, centerContent, className }) => (
  <div
    className={`list-block ${
      centerContent ? 'list-block--center-content' : ''
    } ${className}`}
  >
    {children}
  </div>
);

ListBlock.propTypes = {
  children: PropTypes.node.isRequired,
  centerContent: PropTypes.bool,
  className: PropTypes.string,
};

ListBlock.defaultProps = {
  className: '',
};

export default ListBlock;
