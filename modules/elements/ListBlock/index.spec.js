import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ListBlock from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<ListBlock>', t => {
  t.equals(typeof ListBlock.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ListBlock>', t => {
  const component = shallow(<ListBlock centerContent>fakeChildren</ListBlock>);

  t.equals(component.find('.list-block--center-content').length, 1, ' Content is centered with prop centerContent');

  t.end();
});
