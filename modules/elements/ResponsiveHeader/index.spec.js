import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';

import ResponsiveHeader from './index';

const sandbox = sinon.sandbox.create();

test('<ResponsiveHeader>', assert => {
  assert.plan(1);

  const target = mount(<ResponsiveHeader isMobile={true} />);
  assert.equals(target.find('.mobile-header').length, 1, 'mobile header shown');
});


test('<ResponsiveHeader>', assert => {
  assert.plan(1);

  const target = mount(<ResponsiveHeader isMobile={false} />)
  assert.equals(target.find('.header').length, 1, 'desktop header shown');
});
