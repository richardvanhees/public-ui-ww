import React from 'react';
import PropTypes from 'prop-types';

import Header from '../Header';
import MobileHeader from '../MobileHeader';

const ResponsiveHeaderComponent = (props) => (
  (props.isMobile) ? <MobileHeader {...props} /> : <Header {...props} />
);

ResponsiveHeaderComponent.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

export default ResponsiveHeaderComponent;
