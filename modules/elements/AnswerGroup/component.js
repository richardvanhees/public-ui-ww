import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Answer from '../Answer';

export default class AnswerGroup extends Component {
  static propTypes = {
    answers: PropTypes.array.isRequired,
    dataTest: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    answerKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    questionId: PropTypes.number,
    selected: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
    hasImages: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    answerPairCount: PropTypes.number,
    answerWidth: PropTypes.number,
    answerMargin: PropTypes.number,
    isMobile: PropTypes.bool,
    noImageStacked: PropTypes.bool,
    smallButtons: PropTypes.bool,
    forceAnswerWidth: PropTypes.bool,
    wrapperClass: PropTypes.string,
  };

  static defaultProps = {
    answerPairCount: 1,
    answerWidth: 260,
    answerMargin: 10,
    forceAnswerWidth: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      answer: null,
      answerRows: [],
      windowWidth: window.outerWidth || window.innerWidth,
      answerWidth: props.smallButtons ? 160 : props.answerWidth,
    };
  }

  componentDidMount() {
    this.pairAnswersForWrapping();
    window.addEventListener('resize', () =>
      this.setState({ windowWidth: window.outerWidth || window.innerWidth })
    );
  }

  setFlexDirection() {
    if (this.props.isMobile && !this.props.forceAnswerWidth) return 'column';
    if (this.props.noImageStacked && !this.props.hasImages) return 'column';
    return 'row';
  }

  setWidth() {
    const {
      answers,
      isMobile,
      answerMargin,
      answerPairCount,
      noImageStacked,
      hasImages,
    } = this.props;
    const { answerWidth } = this.state;

    if (isMobile) {
      return '';
    }
    if (noImageStacked && !hasImages) {
      return '';
    }

    if (
      answerMargin * 2 * answers.length + answerWidth * answers.length + 40 >
      this.state.windowWidth
    ) {
      return (
        answerMargin * (2 * answerPairCount) + answerWidth * answerPairCount
      );
    }

    return '';
  }

  pairAnswersForWrapping() {
    const answerRows = [];
    let tempArray = [];

    this.props.answers.forEach((answer, index) => {
      tempArray.push(answer);

      if (
        index % this.props.answerPairCount === this.props.answerPairCount - 1 ||
        index === this.props.answers.length - 1
      ) {
        answerRows.push(tempArray);
        tempArray = [];
      }
    });

    this.setState({ answerRows });
  }

  render() {
    const {
      dataTest,
      answerKey,
      questionId,
      hasImages,
      onChange,
      selected,
      isMobile,
      answerPairCount,
      answerMargin,
      forceAnswerWidth,
      wrapperClass,
    } = this.props;
    const { answerWidth } = this.state;

    return (
      <div
        className="answer-group"
        style={{
          flexDirection: this.setFlexDirection(),
          width: this.setWidth(),
        }}
      >
        {this.state.answerRows.map((answerRow, rowIndex) => (
          <div
            className="answer-group__row"
            key={`answer-row-${rowIndex}`}
            style={{ flexDirection: this.setFlexDirection() }}
          >
            {answerRow.map((answer, i) => {
              const answerIndex = rowIndex * answerPairCount + i;
              return (
                <Answer
                  className={wrapperClass}
                  {...answer}
                  key={`answer-${answerIndex}`}
                  id={answerIndex}
                  dataTest={`${dataTest}-a${answerIndex + 1}`}
                  answerKey={answerKey}
                  questionId={questionId}
                  selected={
                    selected === answerIndex || selected === answer.value
                  }
                  hasImages={hasImages && !isMobile}
                  onClick={e => {
                    this.setState({ answer: e.answerId });
                    onChange(e);
                  }}
                  style={{
                    width: isMobile && !forceAnswerWidth ? '' : answerWidth,
                    margin: isMobile ? '' : answerMargin,
                  }}
                />
              );
            })}
          </div>
        ))}
      </div>
    );
  }
}
