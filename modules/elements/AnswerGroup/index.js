import { connect } from 'react-redux';
import AnswerGroupComponent from './component';
import isMobile from '../../utils/is-mobile';

const mapStateToProps = ({ browser }) => ({
  isMobile: isMobile(browser),
});

export default connect(mapStateToProps)(AnswerGroupComponent);
