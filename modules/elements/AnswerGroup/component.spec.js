import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import AnswerGroup from './component';
import Answer from '../Answer';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  answers: [{ id: 0 }, { id: 1 }],
  dataTest: 'test',
  onChange: onChangeStub,
};

test('<AnswerGroup>', t => {
  const component = mount(<AnswerGroup {...fakeRequiredProps} />);

  t.equals(typeof AnswerGroup.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    component.find(Answer).length,
    fakeRequiredProps.answers.length,
    'Amount of answers in object is equal to amount of answers rendered'
  );

  t.end();
});
