import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from './Page';
import R from 'ramda';

export default class Pager extends Component {
  static propTypes = {
    pages: PropTypes.arrayOf(PropTypes.shape({
      pageContent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
      titleImage: PropTypes.string,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      nextLabel: PropTypes.oneOfType([PropTypes.node, PropTypes.string, PropTypes.number]),
      style: PropTypes.object,
      requiresValidation: PropTypes.bool,
      navigationHasDisabledState: PropTypes.bool,
    })).isRequired,
    previousPage: PropTypes.string,
    nextPage: PropTypes.string,
    customNextPage: PropTypes.func,
    parentProps: PropTypes.object,
    match: PropTypes.object.isRequired,
    setPage: PropTypes.func,
    flexPage: PropTypes.bool,
    afterLastPage: PropTypes.func,
    onSetNextPage: PropTypes.func,
  };

  constructor(props) {
    super(props);

    if (!props.match.params.activePage) {
      console.error('The route should contain /:activePage in order to use the Pager (Check readme in modules/elements/Pager)'); // eslint-disable-line no-console
    }
  }

  navigate = async direction => {
    const {
      match,
      pages,
      setPage,
      nextPage,
      customNextPage,
      previousPage,
      afterLastPage,
      onSetNextPage,
    } = this.props;

    let path = match.path.replace('/:activePage', '');
    Object.keys(match.params).forEach(param => {
      path = path.replace(new RegExp(`:${param}`, 'g'), match.params[param]);
    });
    const activePage = parseInt(match.params.activePage, 10);

    if (direction === 'next') {
      const canContinue =
        afterLastPage && activePage === pages.length
          ? await afterLastPage()
          : true;

      if (canContinue) {
        onSetNextPage && onSetNextPage();
        activePage === pages.length // eslint-disable-line no-nested-ternary
          ? !!customNextPage ? customNextPage() : setPage(nextPage)
          : setPage(`${path}/${activePage + 1}`);
      }
    }
    if (direction === 'back') {
      activePage === 1
        ? setPage(previousPage)
        : setPage(`${path}/${activePage - 1}`);
    }
  };

  render = () => {
    const { pages, match } = this.props;
    const activePage = parseInt(match.params.activePage, 10);

    return (
      <div className="pager">
        {pages.map((page, i) => (
          <Page
            {...page}
            requiresValidation={page.requiresValidation}
            parentProps={R.omit(['pages', 'setPage', 'nextPage', 'previousPage'], this.props)}
            key={`page-${i}`}
            activePage={activePage}
            navigate={this.navigate}
            totalPages={pages.length}
            currentPage={i + 1}
          />
        ))}
      </div>
    );
  }
}
