import test from 'tape';
import React, { Component } from 'react';
import { configure, mount } from 'enzyme';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import Pager, { __RewireAPI__ } from './index';
import Page from './Page';
import CubeCounter from '../CubeCounter';
import Button from '../Button';
import sinon from 'sinon';

class fakePageContent extends Component {
  render() {
    return (<div>FakeContent</div>);
  }
}

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const setPageStub = sandbox.stub();
const testStub = sandbox.stub();

const fakeStore = {
  getState: () => {
  },
  subscribe: () => null,
  dispatch: () => null,
};

const fakeRequiredProps = {
  setPage: setPageStub,
  testFunc: testStub,
  pages: [
    {
      pageContent: fakePageContent,
      title: 'fakeTitle',
      subtitle: "fakeSubTitle",
      nextLabel: 'fakeNextLabel',
      navigationTimeout: 0,
    }, {
      pageContent: fakePageContent,
      title: 'fakeTitle2',
      subtitle: "fakeSubTitle2",
      nextLabel: 'fakeNextLabel2',
      navigationTimeout: 0,
    }
  ],
  dataTest: 'fakeDataTest',
  nextPage: 'fakeNextPage',
  match: {
    params: { activePage: 1 },
    path: 'fakePath'
  },
};

test('<Pager>', t => {
  t.equals(typeof Pager.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Pager>', t => {
  const wrapper = mount(
    <Provider store={fakeStore}>
      <Pager {...fakeRequiredProps} />
    </Provider>
  );

  t.equals(wrapper.find(Page).length, fakeRequiredProps.pages.length, 'Amount of pages rendered is equal to amount of pages in props');

  t.end();
});

test('<Pager>', t => {
  const wrapper = mount(
    <Provider store={fakeStore}>
      <Pager {...fakeRequiredProps} />
    </Provider>
  ).find(Page).at(0);

  t.equals(wrapper.find('.page__title').get(0).props.children, 'fakeTitle', 'Correct title is rendered');
  t.equals(wrapper.find('.page__subtitle').get(0).props.children, 'fakeSubTitle', 'Correct subTitle is rendered');
  t.equals(wrapper.find(Button).at(1).find('.btn').get(0).props.children, 'fakeNextLabel', 'Correct nextLabel is rendered');
  t.equals(wrapper.find(CubeCounter).find('.cube-counter__cube').length, fakeRequiredProps.pages.length, 'Correct amount of progress cubes is rendered');
  t.equals(wrapper.find(CubeCounter).find('.cube-counter__cube--active').length, 1, 'Correct amount of active progress cubes is rendered');

  t.end();
});

test('<Pager>', t => {
  const wrapper = mount(
    <Provider store={fakeStore}>
      <Pager {...fakeRequiredProps} />
    </Provider>
  );

  __RewireAPI__.__Rewire__('push', setPageStub);

  wrapper.find(Page).at(0).find(Button).at(1).simulate('click');

  t.equals(setPageStub.calledWith('fakePath/2'), true, 'Next page rendered');

  __RewireAPI__.__ResetDependency__('push');

  t.end();
});

test('<Pager>', t => {
  const wrapper = mount(
    <Provider store={fakeStore}>
      <Pager {...fakeRequiredProps} match={{ params: { activePage: 2 }, path: 'fakePath' }} />
    </Provider>
  );

  __RewireAPI__.__Rewire__('push', setPageStub);

  wrapper.find(Page).at(1).find(Button).at(1).simulate('click');

  t.equals(setPageStub.calledWith('fakeNextPage'), true, 'Next screen rendered');

  __RewireAPI__.__ResetDependency__('push');

  t.end();
});
