import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import PagerComponent from './component';

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  setPage: route => dispatch(push(route)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PagerComponent);
