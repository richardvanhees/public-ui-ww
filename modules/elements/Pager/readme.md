## Pager

This readme describes how to use the Pager.

### Setup
The route of the pager view should be in the following format: `{url}/{component}/:activePage`. As the URL is public facing, the first page will be number 1, not 0.  
All functions, dispatches and the state will be defined in the container of the pager, and NOT in the container of the children.
The props will automatically be passed to its children by adding `{...this.props}` to the JSX element. 

### Minimum example main component
```javascript
import React, { Component } from 'react';
import Pager from 'path/to/Pager';
 
import ExampleSectionComponent from './ExampleSectionComponent';
import ExampleSectionComponent2 from './ExampleSectionComponent2';
 
export default class ExampleMainComponent extends Component {
  pagerOptions = {
    pages: [
      {
        pageContent: ExampleSectionComponent,
        title: 'Example title',
      }, {
        pageContent: <ExampleSectionComponent2 />,
        title: 'Example title 2',
      }
    ],
    previousPage: `${baseUrl}/previous-page`,
    nextPage: `${baseUrl}/next-page`,
  };
  
  constructor(props) {
    super(props);
  }
 
  render() {
    return (
      <div className="wrapper">
        <Pager {...this.props} {...this.pagerOptions} />
      </div>
    );
  }
}
 
``` 
It is also possible to extend the `pagerOptions` object with `...this.props`, instead of adding them seperately in the JSX for the pager.

### pagerOptions
The pagerOptions object can use the following options:
  
**pages - required**  
An array of objects with content (more on this later)

**previousPage**    
The page that should be navigated to when clicked on the back button on the first page.

**nextPage**  
The page that should be navigated to when clicked on the next button on the last page.

**parentProps - deprecated**    
Originally for passing the props of the main component to its children. No longer used.

**match - required**  
React-router module function used to set the url (this should be in the `this.props` of the component you're using the Pager in).

**setPage**  
Shortcut function to set the page. This is provided by the pager itself and does not need to be set by the dev. 
It is possible to overwrite it. Make sure the following format is used: `setPage: route => dispatch(push(route))`

**flexPage**  
In case you would like the children elements to be rendered as `display: flex`, instead of `display: block`. 
The child will have `flex-direction: column`.

**onSetNextPage**
A function that is called just before the page navigation takes place (onPagerWillNavigateToNextPage-ish?)

**afterLastPage**  
A final independent check or function call when clicked on the next button on the last page. 
Return a `true` or `false` to make it pass or fail. This function is a global pager function, not directly related
to any of the children. If you want to make a popup appear for example, the popup needs to be on the same page as
the pager, not on one of its children.


### Page options  
The pages can use the following options:

**pageContent - required**  
This is the component that will be rendered on a pager page. this can be done through a variable import:
```javascript
import MyComponent from './MyComponent';
// ...
 
  pagerOptions = {
    pages: [{
      pageContent: MyComponent      
    }]
  };
  
//...
```
Or by using JSX directly:
```javascript
// ...
 
pagerOptions = {
  pages: [{
    pageContent: <div>This is my component</div>      
  }]
};
 
```
It is not recommended to insert your component as `pageContent: <MyComponent />`. As the parent props are not directly 
inserted on this line, PropType checks will fail; React will initially try to render this component, but its props are
missing here - those are added in later, when rendering the whole pager (see `extraPropsToInject`). 

**extraPropsToInject**  
An object of props you would otherwise probably add as `<Component prop={'fakeProp'} />`. This object is used
to prevent PropType errors when the pager is trying to render a node like example 2 in the pageContent section.
These errors only occur if an imported component is used in JSX style, not for simple HTML, like the example. This
object is useful for a loop using imported components (Check RightTimeComponent).

**titleImage**  
An image that will be rendered at the top of the page. The height is set at 80px.

**title**  
The title of the page.

**subtitle**  
The sub title of the page.

**nextLabel**  
The label that will appear on the nextLabel. This can be a string or JSX. It defaults to `Next >` if left empty.

**style**  
An optional style object that will be applied to the child's wrapper. You can set a specific fixed width for example. 
The max-width will always be 100% of the screen though, which can be overridden as well.

**requiresValidation**  
If set to `true`, the pager will look for the `validate = () => {};` method in the child's component.
Return a `true` or `false` in that validate function to make it pass or fail.

**navigationHasDisabledState**  
If set to `true`, the next button will be disabled rather than not shown when `showBackButton` is `false` in the page. (more on that later)

**centerButtons**
If set to `true`, the page navigation buttons will be centered on the page.


### pagerProps
All children will get an extra prop injected: `pagerProps`. The functions in this prop will allow the dev to access certain
pager functionality when clicking other buttons than the navigation (which is built in). The following functions are available:

**pagerProps.next({ skipValidation: bool })**  
Go to the next page in the pager, or go to the page defined as `nextPage` if it's the last page. When using `await` for this function,
it will wait until the fade out animation is done and before fade in animation has started (useful for showing something only after animation, 
like showNext()). If skipValidation is true, it will always go to the next page.

**pagerProps.back()**  
Go to the previous page in the pager, or go the page defined as `previousPage` if it's the first page. When using `await` for this function,
it will wait until the fade out animation is done and before fade in animation has started (useful for showing something only after animation, like showBack()).

**pagerProps.showNext() / pagerProps.enableNext()**    
Show the next button or enable the next button when `navigationHasDisabledState` is set to `true`. By default, the button is shown/enabled.

**pagerProps.showBack() / pagerProps.enableBack()**    
Show the back button.

**pagerProps.hideNext() / pagerProps.disableNext()**    
Hide the next button or disable the next button when `navigationHasDisabledState` is set to `true`.

**pagerProps.hideBack() / pagerProps.disableBack()**    
Hide the back button

The enable and show functions do the exact same thing, but have been created to be able to use function names that
make more sense in a certain context.
