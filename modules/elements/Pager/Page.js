import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PageNavigation from '../PageNavigation';
import CubeCounter from '../CubeCounter';

export default class Page extends Component {
  static propTypes = {
    pageContent: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.func,
      PropTypes.object,
    ]).isRequired,
    navigate: PropTypes.func.isRequired,
    totalPages: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    activePage: PropTypes.number.isRequired,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    nextLabel: PropTypes.string,
    style: PropTypes.object,
    requiresValidation: PropTypes.bool,
    parentProps: PropTypes.object,
    extraPropsToInject: PropTypes.object,
    onComplete: PropTypes.func,
    navigationTimeout: PropTypes.number,
    centerButtons: PropTypes.bool,
    navigationHasDisabledState: PropTypes.bool,
    titleImage: PropTypes.string,
  };

  static defaultProps = {
    navigationTimeout: 200,
    centerButtons: false,
    navigationHasDisabledState: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      active: props.activePage === props.currentPage,
      visible: props.activePage === props.currentPage,
      visited: props.currentPage < props.activePage,
      upcoming: props.currentPage > props.activePage,
      showNext: true,
      showBack: true,
    };
  }

  componentDidUpdate = async prevProps => {
    if (prevProps.activePage !== this.props.activePage) {
      this.clearTimeouts({ resetActivity: false });

      (this.props.currentPage !== this.props.activePage) &&
      await this.setInactive();

      if (this.props.currentPage === this.props.activePage && !this.state.active) {
        this.setActiveTimeout = await setTimeout(this.setActive, this.props.navigationTimeout);
      }
    }
  };

  componentWillUnmount = () => {
    this.clearTimeouts({ resetActivity: true });
  };

  setInactive = direction =>
    new Promise(async resolve => {
      const { navigationTimeout, currentPage, activePage } = this.props;

      await this.setState({
        visible: false,
        visited: currentPage < activePage || direction === 'next',
        upcoming: currentPage > activePage || direction === 'back',
      });

      this.setInactiveTimeout = setTimeout(() => {
        this.setState({ active: false });
        resolve();
      }, navigationTimeout);
    });

  setActive = () =>
    new Promise(async resolve => {
      const { currentPage, activePage, navigationTimeout } = this.props;
      await this.setState({ active: true });

      this.afterActiveTimeout = setTimeout(() => {
        this.setState({
          visible: true,
          visited: currentPage < activePage,
          upcoming: currentPage > activePage,
        });
        resolve();
      }, navigationTimeout);
    });

  setPage = async (direction, skipValidation) => {
    if (this.state.visible && this.canContinue(direction, skipValidation)) {
      if (this.props.onComplete && direction === 'next') {
        this.props.onComplete();
      }

      this.props.navigate(direction);
    }
  };

  clearTimeouts = ({ resetActivity }) => {
    resetActivity && clearTimeout(this.setInactiveTimeout);
    clearTimeout(this.afterActiveTimeout);
    clearTimeout(this.setActiveTimeout);
    resetActivity && clearTimeout(this.navigationTimeout);
    resetActivity && clearTimeout(this.nextTimeout);
    resetActivity && clearTimeout(this.backTimeout);
  };

  canContinue = (direction, skipValidation) => {
    if (skipValidation) return true;
    if (this.props.requiresValidation && direction === 'next') return this.page.validate();
    return true;
  };

  render = () => {
    const {
      pageContent: PageContent,
      totalPages,
      currentPage,
      title,
      subtitle,
      nextLabel,
      style,
      parentProps,
      centerButtons,
      navigationHasDisabledState,
      titleImage,
      extraPropsToInject,
    } = this.props;
    const { active, visible, visited, upcoming } = this.state;

    const isReactElement = React.isValidElement(PageContent);
    const pagerProps = {
      next: (options = {}) => {
        const { skipValidation = false } = options;
        return new Promise(resolve => {
          this.setPage('next', skipValidation);
          this.nextTimeout = setTimeout(resolve, this.props.navigationTimeout);
        });
      },
      back: () =>
        new Promise(resolve => {
          this.setPage('back');
          this.backTimeout = setTimeout(resolve, this.props.navigationTimeout);
        }),
      showNext: () => this.setState({ showNext: true }),
      enableNext: () => this.setState({ showNext: true }),
      showBack: () => this.setState({ showBack: true }),
      enableBack: () => this.setState({ showBack: true }),
      hideNext: () => this.setState({ showNext: false }),
      disableNext: () => this.setState({ showNext: false }),
      hideBack: () => this.setState({ showBack: false }),
      disableBack: () => this.setState({ showBack: false }),
    };

    return (
      <div
        className={`page ${upcoming ? 'page--upcoming' : ''} ${
          visited ? 'page--visited' : ''
          } ${active ? 'page--active' : ''} ${
          visible ? 'page--visible' : ''
          }`}
        style={style}
      >
        {titleImage && <div className="page__title-image"><img src={titleImage} alt={title} /></div>}
        {title && <h1 className="page__title">{title}</h1>}
        {subtitle && <h2 className="page__subtitle">{subtitle}</h2>}
        <CubeCounter total={totalPages} current={currentPage - 1} />

        {/*
        Cloning the element, because we cannot modify the props of a react element.
        We can however clone it, inject props in its clone and render that. Native react props, like ref, are retained.
        */}

        {isReactElement ? (
          React.cloneElement(PageContent, { pagerProps, ...parentProps, ...extraPropsToInject })
        ) : (
          <PageContent
            ref={c => {
              this.page = c;
            }}
            pagerProps={pagerProps}
            {...parentProps}
            {...extraPropsToInject}
          />
        )}

        <div
          className={'page__navigation'}
        >
          <PageNavigation
            next={this.state.showNext && (() => this.setPage('next'))}
            back={this.state.showBack && (() => this.setPage('back'))}
            nextLabel={nextLabel}
            centerButtons={centerButtons}
            noSideMargins
            testNext={`page-${currentPage}`}
            testBack={`page-${currentPage}`}
            hasDisabledState={navigationHasDisabledState}
          />
        </div>
      </div>
    );
  };
}
