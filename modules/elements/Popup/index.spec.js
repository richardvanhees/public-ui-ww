import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';

import PopupComponent from './';

const fakeRequiredProps = {
  visible: true,
};

test('<PopupComponent>', t => {
  t.equals(typeof PopupComponent.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(
      <PopupComponent {...fakeRequiredProps}>FakeContent</PopupComponent>
    ).find('.popup__title').length,
    0,
    'Title is hidden if not provided'
  );

  t.equals(
    shallow(
      <PopupComponent {...fakeRequiredProps} title={'fakeTitle'}>
        FakeContent
      </PopupComponent>
    ).find('.popup__title').length,
    1,
    'Title is shown if provided'
  );

  t.equals(
    shallow(
      <PopupComponent {...fakeRequiredProps}>FakeContent</PopupComponent>
    ).find('.popup__button-bar').length,
    0,
    'Button bar is hidden if not provided'
  );

  t.equals(
    shallow(
      <PopupComponent
        {...fakeRequiredProps}
        buttons={[
          {
            label: 'fakeButton',
            onClick: () => {},
          },
        ]}
      >
        FakeContent
      </PopupComponent>
    ).find('.popup__button-bar').length,
    1,
    'Buttons are shown if provided'
  );

  t.equals(
    shallow(
      <PopupComponent {...fakeRequiredProps} isError>
        FakeContent
      </PopupComponent>
    ).find('.popup--is-error').length,
    1,
    'Popup type is error if isError is true'
  );

  t.end();
});

test('<PopupComponent>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onClickToCloseStub = sandbox.stub();

  const additionalProps = {
    title: 'title',
    closeButtonTop: true,
    closeClickOutside: true,
    onClickToClose: onClickToCloseStub,
  };

  const allProps = {
    ...fakeRequiredProps,
    ...additionalProps,
  };

  const target = mount(<PopupComponent {...allProps}>Stuff</PopupComponent>);

  const closeButton = target.find('div[data-test="popup__close-button"]');

  closeButton.simulate('click');

  assert.equals(
    onClickToCloseStub.callCount,
    1,
    'onClickToClose function called when close button clicked'
  );
});

test('<PopupComponent>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onClickToCloseStub = sandbox.stub();

  const additionalProps = {
    title: 'title',
    closeButtonTop: true,
    closeClickOutside: true,
    onClickToClose: onClickToCloseStub,
  };

  const allProps = {
    ...fakeRequiredProps,
    ...additionalProps,
  };

  const target = mount(<PopupComponent {...allProps}>Stuff</PopupComponent>);

  const closeButton = target.find('div[data-test="popup-overlay__handler"]');

  closeButton.simulate('click');

  assert.equals(
    onClickToCloseStub.callCount,
    1,
    'onClickToClose function called when overlay clicked'
  );
});
