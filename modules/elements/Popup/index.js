import React, { Component } from 'react';
import classNames from 'classnames';

import Button from '../Button';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default class Popup extends Component {
  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    isError: PropTypes.bool,
    isNarrow: PropTypes.bool,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    buttons: PropTypes.array,
    closeButtonTop: PropTypes.bool,
    closeClickOutside: PropTypes.bool,
    children: PropTypes.node.isRequired,
    onClose: PropTypes.func,
    onClickToClose: PropTypes.func,
    layout: PropTypes.string,
    dataTest: PropTypes.string,
    visible: PropTypes.bool,
    centeredButtons: PropTypes.bool,
  };

  static defaultProps = {
    layout: 'row',
    className: '',
    centeredButtons: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      visible: !!props.visible,
      fadingOut: false,
    };
  }

  componentWillUnmount() {
    document.body.style.overflow = '';
  }

  setClassNames = () => {
    const { isError, isNarrow, className } = this.props;
    const { visible, fadingOut } = this.state;

    return classNames(
      {
        popup: true,
        'popup--visible': visible,
        'popup--is-error': isError || isNarrow,
        'popup--fading-out': fadingOut,
      },
      className
    );
  };

  toggle = show => new Promise(resolve => {
    this.state.visible && this.props.onClose && this.props.onClose();
    this.state.visible && this.setState({ fadingOut: true });

    const newVisibility =
      typeof show === 'boolean' ? show : !this.state.visible;

    document.body.style.overflow = newVisibility ? 'hidden' : '';

    // Wait for animation when closing
    setTimeout(() => {
      this.setState({
        visible: newVisibility,
        fadingOut: false,
      });
      resolve();
    }, this.state.visible ? 200 : 0);
  });

  render() {
    const {
      title,
      children,
      buttons,
      closeButtonTop,
      closeClickOutside,
      isError,
      isNarrow,
      layout,
      onClickToClose,
      dataTest,
      centeredButtons,
    } = this.props;
    const { visible, fadingOut } = this.state;

    return (
      <div
        className={`popup-overlay ${visible ? 'popup-overlay--visible' : ''} ${
          fadingOut ? 'popup-overlay--fading-out' : ''
          }`}
        data-test={dataTest}
      >
        {closeClickOutside && (
          <div
            data-test="popup-overlay__handler"
            className={'popup-overlay__handler'}
            onClick={() => {
              closeClickOutside && this.toggle(false);
              closeClickOutside && onClickToClose && onClickToClose();
            }}
          />
        )}

        <div className={this.setClassNames()}>
          {(title || closeButtonTop) && (
            <div className={'popup__title'} data-test="popup-title">
              {(title || (!title && isError)) && (
                <div className={'popup__label'}>{title || 'Error'}</div>
              )}
              {closeButtonTop && (
                <div
                  data-test="popup__close-button"
                  className={'popup__close-button'}
                  onClick={() => {
                    this.toggle(false);
                    onClickToClose && onClickToClose();
                  }}
                >
                  <div className={'popup__close-icon'}>
                    <Icon name={'times-circle'} />
                  </div>
                </div>
              )}
            </div>
          )}

          <div
            className={`
              popup__content
              ${isError ? 'popup__content--is-error' : ''}
              ${isNarrow ? 'popup__content--is-narrow' : ''}
              ${
              !buttons || (buttons && !buttons.length)
                ? 'popup__content--no-button-bar'
                : ''
              }
            `}
            style={{ flexDirection: layout }}
            data-test={`${dataTest}-content`}
          >
            {layout === 'column' ? children : <div>{children}</div>}
          </div>

          {buttons && (
            <div
              className={`popup__button-bar ${
                centeredButtons ? 'popup__button-bar--centered' : ''
                }`}
            >
              {buttons.map((button, i) => (
                <Button
                  dataTest={button.dataTest || 'popup-close-btn'}
                  key={`popup-button-${i}`}
                  {...button}
                  className={`${button.className} popup__button`}
                />
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}
