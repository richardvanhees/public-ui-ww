import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Popup from './index';
import '../../../assets/styles/common/index.scss';

let requiredPopup = null;
let buttonsPopup = null;

const exampleButtons = [
  { label: 'foo', onClick: action('FooButton') },
  { label: 'bar', onClick: action('BarButton') },
];

storiesOf('Popup', module)
  .add('Basic', () => (<div>
    <button onClick={() => { requiredPopup.toggle(); }}>Pop it up</button>
    <Popup title="Example" closeClickOutside ref={c => { requiredPopup = c; }}>
      <span>Children text</span>
    </Popup>
  </div>)
  )
  .add('Buttons', () => (<div>
    <button onClick={() => { buttonsPopup.toggle(); }}>Pop it up</button>
    <Popup buttons={exampleButtons} title="Example" closeClickOutside ref={c => { buttonsPopup = c; }}>
      <span>Children text</span>
    </Popup>
  </div>)
  );
