import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CloseButton from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const onClickStub = sandbox.stub();

const fakeRequiredProps = {
  onClick: onClickStub,
};

test('<CloseButton>', assert => {
  assert.plan(1);

  const component = mount(<CloseButton {...fakeRequiredProps} />);
  component.simulate('click');
  assert.equals(onClickStub.called, true, 'onClick is called on click');

  assert.end();
});
