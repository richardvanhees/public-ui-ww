import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CloseButton from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('CloseButton', module)
  .add('Required', () => <CloseButton onClick={action('onClick')} />);
