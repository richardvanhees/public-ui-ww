import React from 'react';
import PropTypes from 'prop-types';

const CloseButton = ({ onClick, style, className }) => (
  <div className={`close-button ${className}`} onClick={onClick} style={style}>
    <div className={'close-button__icon'}>+</div>
  </div>
);

CloseButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
};

CloseButton.defaultProps = {
  className: '',
};

export default CloseButton;
