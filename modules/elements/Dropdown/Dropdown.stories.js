import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Dropdown from './index';
import '../../../assets/styles/common/index.scss';

const exampleOptions = [
  { label: 'foo', value: 1 },
  { label: 'bar', value: 2 },
  { label: 'baz', value: 3 },
];

storiesOf('Dropdown', module)
  .addDecorator(story => (
    <div style={{ width: '400px' }}>
      {story()}
    </div>
  ))
  .add('Required', () => <Dropdown options={exampleOptions} dataTest="example-data-test" onChange={action('onChange')} />);
