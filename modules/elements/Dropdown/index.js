import React from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';

const Dropdown = ({
  options,
  nullLabel,
  onChange,
  style,
  className,
  userInput,
  dataTest,
  double,
}) => (
  <div className={`dropdown ${className}`} style={style}>
    <select
      data-test={`dropdown-${dataTest}`}
      className={`dropdown__select ${double ? 'dropdown__select--double' : ''}`}
      onChange={e =>
        e.target.value === nullLabel ? onChange(null) : onChange(e.target.value)
      }
      defaultValue={userInput || null}
    >
      <option
        className="dropdown__option dropdown__option--nulllabel"
        value={null}
        selected={!userInput}
      >
        {nullLabel}
      </option>
      {options.map((option, i) => {
        const value = R.is(Object, option) ? option.value : option;
        const label = R.is(Object, option)
          ? option.label || option.value
          : option;

        return (
          <option
            key={`dropdown-option-${value}-${i}`}
            className="dropdown__option"
            value={value}
          >
            {label}
          </option>
        );
      })}
    </select>
  </div>
);

Dropdown.propTypes = {
  nullLabel: PropTypes.string,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired,
      })
    ),
    PropTypes.arrayOf(
      PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    ),
  ]).isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  userInput: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  dataTest: PropTypes.string.isRequired,
  double: PropTypes.bool,
};

Dropdown.defaultProps = {
  nullLabel: 'Select...',
};

export default Dropdown;
