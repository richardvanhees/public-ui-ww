import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Dropdown from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  options: [{ value: 'testValue', label: 'testLabel' }],
  onChange: onChangeStub,
};

test('<Dropdown>', t => {
  const component = shallow(<Dropdown {...fakeRequiredProps} />);

  t.equals(typeof Dropdown.propTypes, 'object', 'PropTypes are defined');

  component.find('select').simulate('change', { target: { value: 'test' } });
  t.deepEqual(
    onChangeStub.called,
    true,
    'onChange is called when clicked on an option'
  );

  t.end();
});
