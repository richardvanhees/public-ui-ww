import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DatePicker from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('DatePicker', module)
  .add('Required', () => <DatePicker name="Example" dataTest="example-data-test" onChange={action('onChange')} />)
  .add('with label', () => <DatePicker name="Example" label="Example" dataTest="example-data-test" onChange={action('onChange')} />)
  .add('pastOnly', () => <DatePicker name="Example" pastOnly dataTest="example-data-test" onChange={action('onChange')} />);
