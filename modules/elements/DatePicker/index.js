import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

const acceptedFormats = [
  'DD/MM/YYYY',
  'D/M/YYYY',
  'D-M-YYYY',
  'DD-MM-YYYY',
  'DD.MM.YYYY',
  'D.M.YYYY',
  'DDMMYYYY',
];

export default class DatePicker extends Component {
  static propTypes = {
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    initialValue: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    updatedDisplayValue: PropTypes.func,
    userInput: PropTypes.string,
    dataTest: PropTypes.string.isRequired,
    onBlur: PropTypes.func,
    /** Autocomplete to dates in the past only */
    pastOnly: PropTypes.bool,
    enabled: PropTypes.bool,
    displayFormat: PropTypes.string,
    storedFormat: PropTypes.string,
    className: PropTypes.string,
  };

  static defaultProps = {
    pastOnly: false,
    enabled: true,
    displayFormat: 'DD/MM/YYYY',
    storedFormat: 'YYYY-MM-DD',
    className: 'input-wrapper__input',
  };

  constructor(props) {
    super(props);

    this.state = {
      date: '',
      changed: false,
    };

    this.inputElement = null;
  }

  componentDidMount = () => {
    this.setInitialDateState(this.props.userInput || this.props.initialValue);
  };

  componentDidUpdate = prevProps => {
    if (this.props.initialValue !== prevProps.initialValue) {
      this.setInitialDateState(this.props.initialValue);
    }
  };

  setInitialDateState = value => {
    const date = moment(value, this.props.storedFormat);
    const displayValue = date.format(this.props.displayFormat);
    this.setState({
      date: !date.isValid() ? '' : displayValue,
    });

    this.props.updatedDisplayValue && this.props.updatedDisplayValue(displayValue);
  };

  generateDate = value => {
    const date = moment(value, acceptedFormats);
    if (this.props.pastOnly && date.isAfter()) {
      return date.subtract(100, 'years');
    }
    return date;
  };

  handleChange = e => {
    if (this.state.date !== e.target.value) {
      this.setState({
        date: e.target.value,
        changed: true,
      });
    }
  };

  handleBlur = event => {
    if (!this.state.changed) {
      return;
    }

    const date = this.generateDate(event.target.value);
    const displayValue = date.format(this.props.displayFormat);

    this.setState({
      date: date.isValid() ? displayValue : '',
      changed: false,
    });

    if (date.isValid() && !!this.props.updatedDisplayValue) {
      this.props.updatedDisplayValue(displayValue);
    }

    this.props.onChange(date.isValid() ? date.format(this.props.storedFormat) : null);
    this.props.onBlur &&
    this.props.onBlur(date.isValid() ? date.format(this.props.storedFormat) : null);
  };

  handleFocus = event => {
    event.target.select();
  };

  focus = () => {
    this.inputElement.focus();
  };

  render = () => {
    const { label, name, dataTest, enabled, displayFormat, className } = this.props;

    return (
      <div className="input-wrapper">
        {label && <p className="input-wrapper__label">{label}</p>}
        <input
          ref={el => {
            this.inputElement = el;
          }}
          className={`${className} input`}
          name={name}
          value={this.state.date}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          type="tel"
          placeholder={displayFormat}
          onFocus={this.handleFocus}
          data-test={dataTest}
          disabled={!enabled}
        />
      </div>
    );
  }
}
