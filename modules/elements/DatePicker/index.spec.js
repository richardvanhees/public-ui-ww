import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import DatePicker from './index';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();
const onChangeSpy = sinon.spy(onChangeStub)

let fakeRequiredProps = {
  name: 'test',
  onChange: onChangeSpy,
  dataTest: 'test'
};

test('<DatePicker>', assert => {
  assert.plan(1);
  assert.equals(typeof DatePicker.propTypes, 'object', 'PropTypes are defined');
});

test('<DatePicker>', assert => {
  assert.plan(1);
  const wrapper = mount(<DatePicker {...fakeRequiredProps} />);
  const target = wrapper.find('.input-wrapper').length;
  assert.equals(target, 1, 'input-wrapper class is present is set as default');
});

test('<DatePicker>', assert => {
  assert.plan(1);
  const wrapper = mount(<DatePicker {...fakeRequiredProps} />);
  const target = wrapper.find('[data-test="test"]');

  target.simulate('change', { target: { value: '111187' } });
  target.simulate('blur');

  assert.true(
    onChangeSpy.calledWith('1987-11-11'),
    'onChangeSpy is called with correct date'
  );
});

test('<DatePicker>', assert => {
  assert.plan(1);

  fakeRequiredProps = {
    ...fakeRequiredProps,
    pastOnly: true
  };

  const wrapper = mount(<DatePicker {...fakeRequiredProps} />);
  const target = wrapper.find('[data-test="test"]');

  target.simulate('change', { target: { value: '17/11/68' } });
  target.simulate('blur');

  assert.true(
    onChangeSpy.calledWith('1968-11-17'),
    'onChangeSpy is called with correct date not in the future'
  );
});

test('<DatePicker>', assert => {
  assert.plan(1);

  fakeRequiredProps = {
    ...fakeRequiredProps,
    pastOnly: false
  };

  const wrapper = mount(<DatePicker {...fakeRequiredProps} />);
  const target = wrapper.find('[data-test="test"]');

  target.simulate('change', { target: { value: '17/11/68' } });
  target.simulate('blur');

  assert.true(
    onChangeSpy.calledWith('2068-11-17'),
    'onChangeSpy is called with correct date not in the future'
  );
});
