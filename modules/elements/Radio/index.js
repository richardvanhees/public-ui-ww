import React from 'react';
import PropTypes from 'prop-types';
import reactHTMLParser from 'react-html-parser';

const Input = ({
  wrapperClassName,
  labelClassName,
  onChange,
  items,
  groupName,
  selected,
}) => (
  <div className={`radio-wrapper ${wrapperClassName}`}>
    {items.map(item => (
      <label className="radio-wrapper__item" key={item.id}>
        <input
          className="radio-wrapper__input"
          type="radio"
          name={groupName}
          value={item.id}
          data-test={item.id}
          checked={selected === item.id ? 'checked' : ''}
          onChange={e => onChange(e.target.value)}
        />
        {item.label && (
          <p className={`radio-wrapper__label ${labelClassName}`}>
            {reactHTMLParser(item.label)}
          </p>
        )}
      </label>
    ))}
  </div>
);

Input.propTypes = {
  className: PropTypes.string,
  groupName: PropTypes.string,
  onChange: PropTypes.func,
  wrapperClassName: PropTypes.string,
  labelClassName: PropTypes.string,
  items: PropTypes.array,
  selected: PropTypes.string,
};

Input.defaultProps = {
  inputClassName: '',
  labelClassName: '',
  wrapperClassName: '',
};

export default Input;
