import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';

import Radio from './index';

const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  onChange: onChangeStub,
  label: 'testLabel',
  items: [
    {
      id: 'investment__hid_borrowing_above_30_short_term',
      label: 'This is short term, I can repay it and i dont need any help',
    },
  ],
};

test('<Radio>', t => {
  t.equals(typeof Radio.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(<Radio {...fakeRequiredProps} />).find('.radio-wrapper').length,
    1,
    'Radio-wrapper class is present is set as default'
  );

  const wrapper = mount(<Radio {...fakeRequiredProps} />);

  wrapper.find('.radio-wrapper__input').simulate('change');
  t.deepEqual(onChangeStub.called, true, 'onChange is called at change event');

  t.end();
});
