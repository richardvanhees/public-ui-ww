import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import PieChartWithLegend from './index';
import '../../../assets/styles/common/index.scss';

const sampleData = [
  { value: 50, color: '#ff0000', label: 'red' }, { value: 25, color: '#00ff00', label: 'green' }, { value: 25, color: '#0000ff', label: 'blue' },
];

const sampleLink = {
  label: 'example',
  onClick: action('onClick'),
};

storiesOf('PieChartWithLegend', module)
  .addDecorator(story => (
    <div style={{ width: '600px' }}>
      {story()}
    </div>
  ))
  .add('Required', () => <PieChartWithLegend data={sampleData} link={sampleLink} />)
  .add('Description', () => <PieChartWithLegend data={sampleData} description="Description of pie chart" link={sampleLink} />);
