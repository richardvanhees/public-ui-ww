import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PieChart from '../PieChart';

export default class PieChartWithLegend extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
      })
    ).isRequired,
    title: PropTypes.string,
    size: PropTypes.number,
    lineWidth: PropTypes.number,
    labelHeader: PropTypes.string,
    valueHeader: PropTypes.string,
    description: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    link: PropTypes.shape({
      label: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
    }),
    tooltipTimeout: PropTypes.number,
    wedgeZoomTimeout: PropTypes.number,
  };

  static defaultProps = {
    size: 200,
    lineWidth: 20,
  };

  render() {
    const {
      data,
      title,
      labelHeader,
      valueHeader,
      description,
      link,
      size,
      lineWidth,
    } = this.props;

    return (
      <div className="pie-chart-with-legend">
        <div className="pie-chart-with-legend__graph-container">
          <PieChart data={data} size={size} lineWidth={lineWidth} />
          {title && (
            <span className="pie-chart-with-legend__label" data-test="pie-chart-label">
              {title}
            </span>
          )}
        </div>

        <div className="pie-chart-with-legend__legend">
          <div className="pie-chart-with-legend__legend-header">
            <span className="pie-chart-with-legend__label-header">
              {labelHeader}
            </span>
            <span className="pie-chart-with-legend__value-header">
              {valueHeader}
            </span>
          </div>
          <div className="pie-chart-with-legend__legend-table">
            {data.map((type, i) => (
              <div
                className="pie-chart-with-legend__legend-item"
                key={`legend-item-${i}`}
              >
                <div className="pie-chart-with-legend__legend-label-container">
                  <div
                    className="pie-chart-with-legend__legend-color"
                    style={{ background: type.color }}
                  />
                  <div
                    className="pie-chart-with-legend__legend-label"
                    data-test={`pie-chart-legend-label-${i}`}
                  >
                    {type.label}
                  </div>
                </div>
                <div
                  className="pie-chart-with-legend__legend-value"
                  data-test={`pie-chart-legend-value-${i}`}
                >
                  {type.value}%
                </div>
              </div>
            ))}
          </div>

          <div
            className="pie-chart-with-legend__legend-popup-link"
            onClick={link.onClick}
          >
            {link.label}
          </div>
          <div className="pie-chart-with-legend__legend-description">
            {description}
          </div>
        </div>
      </div>
    );
  }
}
