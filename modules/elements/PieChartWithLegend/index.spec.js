import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PieChartWithLegend from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<PieChartWithLegend>', t => {
  t.equals(
    typeof PieChartWithLegend.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.end();
});
