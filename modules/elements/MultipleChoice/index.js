import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Answer from '../Answer';

export default class MultipleChoice extends Component {
  static propTypes = {
    values: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired,
        icon: PropTypes.string,
        label: PropTypes.string,
      })
    ).isRequired,
    userInput: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    answerClassName: PropTypes.string,
    style: PropTypes.object,
  };

  static defaultProps = {
    className: '',
    answerClassName: '',
    style: {},
  };

  constructor() {
    super();

    this.state = {
      answer: null,
    };
  }

  render() {
    const { values, className, answerClassName, style } = this.props;

    return (
      <div className={`multiple-choice ${className}`} style={style}>
        {values.map((value, index) => (
          <Answer
            answer={value.label}
            className={`multiple-choice__answer ${answerClassName}`}
            key={`multiple-choice-${index}`}
            hasIcons
            iconName={value.icon}
            iconSet={value.iconSet}
            id={index}
            onClick={({ answerId }) => {
              this.setState({ answer: answerId });
              this.props.onChange(value.value);
            }}
            selected={this.props.userInput === value.value}
          />
        ))}
      </div>
    );
  }
}
