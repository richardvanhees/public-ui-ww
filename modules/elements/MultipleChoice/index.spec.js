import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MultipleChoice from './index';
import sinon from 'sinon';
import Answer from '../Answer';

const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  values: [
    { value: 'testValue1', icon: 'testIcon1' },
    { value: 'testValue2', icon: 'testIcon2' },
    { value: 'testValue3', icon: 'testIcon3' },
  ],
  onChange: onChangeStub,
};

test('<MultipleChoice>', t => {
  const component = mount(<MultipleChoice {...fakeRequiredProps} />);

  t.equals(typeof MultipleChoice.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    component.find(Answer).length,
    fakeRequiredProps.values.length,
    'Amount of answers in object is equal to amount of answers rendered'
  );

  component
    .find(Answer)
    .first()
    .simulate('click');
  t.equals(
    component.state().answer,
    0,
    'Hovered answer id in state is equal to hovered Answer element'
  );

  t.end();
});
