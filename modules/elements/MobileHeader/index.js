import React, { Component } from 'react';
import Icon from '../Icon';
import PropTypes from 'prop-types';
import defaultImage from '../../../assets/images/logos/inline.png';

import ContactPhone from '../ContactPhone';
import ContactEmail from '../ContactEmail';

export default class MobileHeaderComponent extends Component {
  static propTypes = {
    anonymous: PropTypes.bool,
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    imageClassName: PropTypes.string,
    offline: PropTypes.bool,
    home: PropTypes.func,
    navigation: PropTypes.shape({
      buttons: PropTypes.arrayOf(
        PropTypes.shape({
          onClick: PropTypes.func,
          label: PropTypes.string.isRequired,
          type: PropTypes.string,
        })
      ),
    }),
  };

  static defaultProps = {
    anonymous: false,
    offline: false,
    image: defaultImage,
    imageClassName: '',
  };

  constructor() {
    super();
    this.state = {
      menu: 'closed',
      contactUs: 'closed',
      phoneTimes: 'closed',
    };
  }

  valueToggle = value => (value === 'open' ? 'closed' : 'open');

  toggleContactUs = () => {
    this.setState({
      ...this.state,
      contactUs: this.valueToggle(this.state.contactUs),
    });
  };

  toggleMenu = () => {
    this.setState({
      contactUs: 'closed',
      menu: this.valueToggle(this.state.menu),
    });
  };

  togglePhoneTimes = () => {
    this.setState({
      ...this.state,
      phoneTimes: this.valueToggle(this.state.phoneTimes),
    });
  };

  render() {
    const {
      anonymous,
      image,
      navigation,
      imageClassName,
      offline,
      home,
    } = this.props;
    const { menu, contactUs, phoneTimes } = this.state;

    return (
      <div className="mobile-header">
        <div onClick={home} className={`mobile-header__logo ${imageClassName}`}>
          <img className="mobile-header__logo-src" src={image} alt="logo" />
        </div>

        {anonymous && (
          <div className="header__top-message header__top-message--anonymous">
            <Icon className="header__icon" name="lock" />
            Anonymous
          </div>
        )}

        {offline && (
          <div className="mobile-header__top-message mobile-header__top-message--offline">
            <Icon className="mobile-header__icon" name="exclamation-triangle" />
            You are offline
          </div>
        )}

        <div onClick={this.toggleMenu} className="mobile-header__hamburger">
          <Icon name={`${menu === 'open' ? 'times' : 'bars'}`} />
        </div>

        <div
          className={`mobile-header__menu-container mobile-header__menu-container--${menu}`}
        >
          <div className="mobile-header__menu-items">
            <a className="mobile-header__link" onClick={this.toggleContactUs}>
              Contact us{' '}
              <Icon
                className={`mobile-header__link-icon mobile-header__link-icon--${contactUs}`}
                name={'angle-down'}
              />
            </a>

            <div
              className={`mobile-header__contact-us mobile-header__contact-us--${contactUs}-${phoneTimes}`}
            >
              <ContactPhone
                className="mobile-header__contact-us-phone"
                phoneTimesOpen={this.togglePhoneTimes}
                withIcon
              />
              <ContactEmail
                className="mobile-header__contact-us-email"
                withIcon
              />
            </div>
            {navigation &&
              navigation.buttons.map((link, i) => (
                <a
                  key={`header-link-${i}`}
                  data-test={link.dataTest}
                  className="mobile-header__link"
                  onClick={link.onClick}
                >
                  {link.label}
                </a>
              ))}
          </div>
        </div>
      </div>
    );
  }
}
