import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import MobileHeaderComponent from './index';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const homeStub = sandbox.stub();

const fakeRequiredProps = {
  home: homeStub,
  anonymous: false,
  offline: false,
};

test('<MobileHeaderComponent>', t => {
  t.equals(
    shallow(<MobileHeaderComponent />).find('.mobile-header').length,
    1,
    'Header is rendered correctly'
  );

  t.end();
});

test('<MobileHeaderComponent> - Logo click', t => {
  const wrapper = mount(<MobileHeaderComponent {...fakeRequiredProps} />);

  wrapper.find('.mobile-header__logo').simulate('click');

  t.equals(homeStub.called, true, 'Logo function triggered');

  t.end();
});

test('<MobileHeaderComponent> - Image classname', t => {
  const wrapper = mount(<MobileHeaderComponent {...fakeRequiredProps} imageClassName={'test-class'} />);

  t.equals(wrapper.find('.mobile-header__logo').hasClass('test-class'), true, 'Image classname applied');

  t.end();
});

test('<MobileHeaderComponent> - Header messages', t => {
  const wrapper = mount(<MobileHeaderComponent {...fakeRequiredProps} anonymous offline />);

  t.equals(wrapper.find('.header__top-message--anonymous').length, 1, 'Anonymous message rendered');
  t.equals(wrapper.find('.mobile-header__top-message--offline').length, 1, 'Offline message rendered');

  t.end();
});

test('<MobileHeaderComponent> - Navigation', t => {
  const buttonStub = sandbox.stub();
  const navigation = {
    buttons: [{
      label: 'testLabel',
      onClick: buttonStub
    }]
  };

  const wrapper = mount(<MobileHeaderComponent {...fakeRequiredProps} navigation={navigation} />);

  wrapper.find('.mobile-header__link').last().simulate('click');

  t.equals(wrapper.find('.mobile-header__link').length, 2, 'Buttons rendered');
  t.equals(buttonStub.called, true, 'Nav button clicked');

  t.end();
});

test('<MobileHeaderComponent> - UI toggles', t => {
  const wrapper = mount(<MobileHeaderComponent {...fakeRequiredProps} />);

  wrapper.find('.mobile-header__link').simulate('click');
  t.equals(wrapper.state().contactUs, 'open', 'contactUs toggle works');

  wrapper.find('.mobile-header__hamburger').simulate('click');
  t.equals(wrapper.state().contactUs, 'closed', 'hamburger toggle works');

  t.end();
});

