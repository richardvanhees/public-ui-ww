import test from 'tape';
import React from 'react';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';

import PreviousAddressComponent from './component';

let fakeRequiredProps = {
  wrapperClassName: '',
  showPreviousAddressForm: false,
  previousAddressArray: [
    {
      addressLine1: '123 fake street',
      addressLine2: 'line 2',
      addressLine3: 'line 3',
      addressPostcode: 'CV2 Fake',
      timeAtMonths: '7',
      timeAtYears: '2',
      timeAtLength: '2 year(s), 7 month(s)',
    },
  ],
  updatePreviousAddressArray: () => {},
  getTimeAtLength: () => {},
  togglePreviousAddressForm: () => {},
  currentAddressTime: {
    year: '',
    month: '',
  },
};

let initialState = {
  previousAddressLine1: '',
  previousAddressLine2: '',
  previousAddressLine3: '',
  previousPostcode: '',
  timeAtMonths: null,
  timeAtYears: null,
  timeAtLength: '',
  editingPreviousAddressIndex: null,
  editingPreviousAddress: false,
  error: {
    addressLine1: false,
    addressLine2: false,
    addressPostCode: false,
    addressMoveInDate: false,
    errorMsg: '',
  },
};

test('<PreviousAddressComponent>', t => {
  t.equals(
    typeof PreviousAddressComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  const target = mount(<PreviousAddressComponent {...fakeRequiredProps} />);
  target.setState(initialState);

  t.equals(
    target.html(),
    '<div class="previous-address "><div class="previous-address__container"><p>Previous Addresses</p><div class="previous-address__item">123 fake street ...<div class="previous-address__btn-container"><div class="previous-address__edit-btn"><i data-tip="" class="fas fa-edit "></i></div><div class="previous-address__delete-btn"><i data-tip="" class="fas fa-trash-alt "></i></div></div></div></div></div>',
    'previous addresses listed out correctly from props'
  );

  target.instance().editAddress(0);
  t.deepEquals(
    target.state(),
    {
      editingPreviousAddress: true,
      editingPreviousAddressIndex: 0,
      error: {
        addressLine1: false,
        addressLine2: false,
        addressMoveInDate: false,
        addressPostCode: false,
        errorMsg: '',
      },
      previousAddressLine1: '123 fake street',
      previousAddressLine2: 'line 2',
      previousAddressLine3: 'line 3',
      previousPostcode: 'CV2 Fake',
      timeAtLength: '2 year(s), 7 month(s)',
      timeAtMonths: '7',
      timeAtYears: '2',
    },
    'localstate updated with correct values from props array when index targeted via edit func'
  );

  t.deepEquals(
    target.props().previousAddressArray,
    [
      {
        addressLine1: '123 fake street',
        addressLine2: 'line 2',
        addressLine3: 'line 3',
        addressPostcode: 'CV2 Fake',
        timeAtMonths: '7',
        timeAtYears: '2',
        timeAtLength: '2 year(s), 7 month(s)',
      },
    ],
    'address array correctly contains previous address array'
  );

  target.instance().deleteAddress(0);

  t.deepEquals(
    target.props().previousAddressArray,
    [],
    'address correctly DELETED from previous address array prop'
  );

  t.end();
});

test('<PreviousAddressComponent>', assert => {
  const augmentedProps = {
    ...fakeRequiredProps,
    lastPostcodeMappingTimestamp: 'this has changed',
    previousAddressArray: [
      {
        timeAtMonths: '4',
        timeAtYears: '2016',
        addressLine1: 'addressLine1',
        addressLine2: 'addressLine2',
        addressLine3: 'addressLine3',
        addressPostcode: 'addressPostcode',
      },
    ],
  };

  const target = mount(<PreviousAddressComponent {...augmentedProps} />);

  assert.deepEqual(
    target.state(),
    {
      editingPreviousAddress: false,
      editingPreviousAddressIndex: null,
      error: {
        addressLine1: false,
        addressLine2: false,
        addressMoveInDate: false,
        addressPostCode: false,
        errorMsg: '',
      },
      lastPostcodeMappingTimestamp: 'this has changed',
      previousAddressLine1: 'addressLine1',
      previousAddressLine2: 'addressLine2',
      previousAddressLine3: 'addressLine3',
      previousPostcode: 'addressPostcode',
      timeAtLength: '',
      timeAtMonths: null,
      timeAtYears: null,
    },
    'getDerivedStateFromProps backports props into state'
  );

  assert.end();
});

// test('<PreviousAddressComponent>', assert => {
//   assert.plan(2);

//   const props = {
//     ...fakeRequiredProps,
//     showPreviousAddressForm: true,
//     currentAddressTime: {
//       year: '2017',
//       month: '1',
//     },
//     previousAddressArray: [],
//   };

//   const target = mount(<PreviousAddressComponent {...props} />);
//   target.setState(initialState);

//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartMonth'),
//     '1',
//     'uses currentAddressTime when array empty'
//   );
//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartYear'),
//     '2017',
//     'uses currentAddressTime when array empty'
//   );
// });

// test('x<PreviousAddressComponent>', assert => {
//   assert.plan(2);

//   const props = {
//     ...fakeRequiredProps,
//     showPreviousAddressForm: true,
//     currentAddressTime: {
//       year: '2017',
//       month: '1',
//     },
//     previousAddressArray: [
//       {
//         timeAtMonths: '4',
//         timeAtYears: '2016',
//       },
//     ],
//   };

//   const target = mount(<PreviousAddressComponent {...props} />);
//   target.setState(initialState);

//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartMonth'),
//     '4',
//     'gets last item in previous address array'
//   );
//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartYear'),
//     '2016'
//   );
// });

// test('<PreviousAddressComponent>', assert => {
//   assert.plan(4);

//   const props = {
//     ...fakeRequiredProps,
//     showPreviousAddressForm: true,
//     currentAddressTime: {
//       year: '2017',
//       month: '1',
//     },
//     previousAddressArray: [
//       {
//         timeAtMonths: '7',
//         timeAtYears: '2016',
//       },
//       {
//         timeAtMonths: '8',
//         timeAtYears: '2016',
//       },
//       {
//         timeAtMonths: '9',
//         timeAtYears: '2016',
//       },
//     ],
//   };

//   const target = mount(<PreviousAddressComponent {...props} />);
//   target.setState({ ...initialState, editingPreviousAddress: true });

//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartMonth'),
//     '8',
//     'when editing, gets correct month'
//   );
//   assert.equal(
//     target.find('TimeAtAddressComponent').prop('previousStartYear'),
//     '2016',
//     'when editing, gets correct year'
//   );
//   assert.equal(
//     target.find('Icon[data-test="previous-edit-2"]').prop('className'),
//     '',
//     'disabled class not set on last item in row'
//   );
//   assert.equal(
//     target.find('Icon[data-test="previous-edit-1"]').prop('className'),
//     'previous-address__edit-btn-disabled',
//     'disabled class is set on row -1'
//   );
// });
