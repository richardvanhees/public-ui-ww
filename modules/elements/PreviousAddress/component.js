import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';
import R from 'ramda';
import TimeAtAddress from '../TimeAtAddress';
import Button from '../Button';
import moment from 'moment';
import Error from '../Error';
import Icon from '../Icon';
import Postcode from '../../../modules/elements/Postcode';
import {
  POSTCODE_REGEX,
  validAddressLine,
  optionalValidAddressLine,
} from '../../utils/regexes';

export default class PreviousAddressComponent extends PureComponent {
  static propTypes = {
    wrapperClassName: PropTypes.string,
    showPreviousAddressForm: PropTypes.bool.isRequired,
    previousAddressArray: PropTypes.array.isRequired,
    updatePreviousAddressArray: PropTypes.func.isRequired,
    getTimeAtLength: PropTypes.func.isRequired,
    clearPendingRecords: PropTypes.func.isRequired,
    togglePreviousAddressForm: PropTypes.func.isRequired,
    currentAddressTime: PropTypes.object.isRequired,
    lastPostcodeMappingTimestamp: PropTypes.number,
  };

  static defaultProps = {
    wrapperClassName: '',
    previousAddressArray: [],
  };

  static getDerivedStateFromProps(props, state) {
    const indexOfPreviousAddress = R.is(
      Number,
      state.editingPreviousAddressIndex
    )
      ? state.editingPreviousAddressIndex
      : props.previousAddressArray.length - 1;

    if (
      props.previousAddressArray &&
      props.previousAddressArray[indexOfPreviousAddress] &&
      // if this has changed we know postcode anywhere has returned and updated the redux state
      // now we have to backport it into the local state
      props.lastPostcodeMappingTimestamp !== state.lastPostcodeMappingTimestamp
    ) {
      const previousAddress =
        props.previousAddressArray[indexOfPreviousAddress];

      return {
        ...state,
        lastPostcodeMappingTimestamp: props.lastPostcodeMappingTimestamp,
        previousAddressLine1: previousAddress.addressLine1,
        previousAddressLine2: previousAddress.addressLine2,
        previousAddressLine3: previousAddress.addressLine3
          ? previousAddress.addressLine3
          : state.previousAddressLine3,
        previousPostcode: previousAddress.addressPostcode,
      };
    }
    return null;
  }

  constructor() {
    super();

    this.state = this.initialState();
  }

  initialState() {
    return {
      previousAddressLine1: '',
      previousAddressLine2: '',
      previousAddressLine3: '',
      previousPostcode: '',
      timeAtMonths: null,
      timeAtYears: null,
      timeAtLength: '',
      editingPreviousAddressIndex: null,
      editingPreviousAddress: false,
      error: {
        addressLine1: false,
        addressLine2: false,
        addressPostCode: false,
        addressMoveInDate: false,
        errorMsg: '',
      },
    };
  }

  getTimeForPreviousAddress() {
    let time = '';
    if (this.props.previousAddressArray.length < 1) {
      time = moment(
        `${this.props.currentAddressTime.year}-${
          this.props.currentAddressTime.month
        }-01`,
        'YYYY-MM-DD'
      ).utc();
    } else {
      const thePreviousAddress = this.props.previousAddressArray.slice(-1)[0];
      time = moment(
        `${thePreviousAddress.timeAtYears}-${
          thePreviousAddress.timeAtMonths
        }-01`,
        'YYYY-MM-DD'
      ).utc();
    }
    return time;
  }

  setTimeAtLength(year, month) {
    if (year === null && month === null) {
      this.setState({ timeAtLength: '' });
    } else {
      const time = this.getTimeForPreviousAddress();
      const timeLivedAt = this.props.getTimeAtLength(year, month, time);
      this.setState({ timeAtLength: timeLivedAt });
    }
  }

  setTimeAtMonths(month) {
    if (!month) {
      this.setState({ timeAtMonths: null });
      this.setTimeAtLength(null, null);
    } else {
      this.setState({ timeAtMonths: month });
      this.setTimeAtLength(this.state.timeAtYears, month);
    }
  }

  setTimeAtYears(year) {
    if (!year) {
      this.setState({ timeAtYears: null });
      this.setTimeAtLength(null, null);
    } else {
      this.setState({ timeAtYears: year });
      this.setTimeAtLength(year, this.state.timeAtMonths);
    }
  }

  validateAddressSubmitted() {
    let validAddress = true;
    if (!validAddressLine(this.state.previousAddressLine1)) {
      validAddress = false;
      this.setState({
        error: {
          addressLine1: true,
          addressLine2: false,
          addressLine3: false,
          addressPostCode: false,
          addressMoveInDate: false,
          errorMsg: 'Please enter a valid address line 1',
        },
      });
    } else if (!validAddressLine(this.state.previousAddressLine2)) {
      validAddress = false;
      this.setState({
        error: {
          addressLine1: false,
          addressLine2: true,
          addressLine3: false,
          addressPostCode: false,
          addressMoveInDate: false,
          errorMsg: 'Please enter a valid address line 2',
        },
      });
    } else if (!optionalValidAddressLine(this.state.previousAddressLine3)) {
      validAddress = false;
      this.setState({
        error: {
          addressLine1: false,
          addressLine2: false,
          addressLine3: true,
          addressPostCode: false,
          addressMoveInDate: false,
          errorMsg: 'Please enter a valid address line 3',
        },
      });
    } else if (
      !this.state.previousPostcode ||
      !this.state.previousPostcode.match(POSTCODE_REGEX)
    ) {
      validAddress = false;
      this.setState({
        error: {
          addressLine1: false,
          addressLine2: false,
          addressLine3: false,
          addressPostCode: true,
          addressMoveInDate: false,
          errorMsg: 'Please enter a valid postcode',
        },
      });
    } else if (!this.state.timeAtMonths || !this.state.timeAtYears) {
      validAddress = false;
      this.setState({
        error: {
          addressLine1: false,
          addressLine2: false,
          addressLine3: false,
          addressMoveInDate: true,
          errorMsg: 'Please enter your move in date',
        },
      });
    } else {
      this.setState({
        error: {
          addressLine1: false,
          addressLine2: false,
          addressLine3: false,
          addressMoveInDate: false,
          errorMsg: '',
        },
      });
    }
    return validAddress;
  }

  addAddress() {
    if (this.validateAddressSubmitted()) {
      const newAddress = {
        addressLine1: this.state.previousAddressLine1,
        addressLine2: this.state.previousAddressLine2,
        addressLine3: this.state.previousAddressLine3,
        addressPostcode: this.state.previousPostcode,
        timeAtMonths: this.state.timeAtMonths,
        timeAtYears: this.state.timeAtYears,
        timeAtLength: this.state.timeAtLength,
        pending: false,
      };

      // make sure we kill off any pending records
      // that postcode anywhere has injected but the user has not actually clicked ok on
      const newAddressArray = [
        ...(this.props.previousAddressArray.filter(
          ({ pending }) => pending === false
        ) || []),
        newAddress,
      ];

      this.props.updatePreviousAddressArray(newAddressArray);

      this.setState({
        previousAddressLine1: '',
        previousAddressLine2: '',
        previousAddressLine3: '',
        previousPostcode: '',
        timeAtMonths: null,
        timeAtYears: null,
        timeAtLength: '',
        editingPreviousAddress: false,
      });

      this.props.togglePreviousAddressForm(this.props.showPreviousAddressForm);
    }
  }

  editAddress(i) {
    const selectedAddress = this.props.previousAddressArray[i];
    this.setState({
      previousAddressLine1: selectedAddress.addressLine1,
      previousAddressLine2: selectedAddress.addressLine2,
      previousAddressLine3: selectedAddress.addressLine3,
      previousPostcode: selectedAddress.addressPostcode,
      timeAtMonths: selectedAddress.timeAtMonths,
      timeAtYears: selectedAddress.timeAtYears,
      timeAtLength: selectedAddress.timeAtLength,
      editingPreviousAddress: true,
      editingPreviousAddressIndex: i,
    });
  }

  deleteAddress(i) {
    const newAddressArray = this.props.previousAddressArray;
    newAddressArray.splice(i, 1);
    this.props.updatePreviousAddressArray(newAddressArray);
  }

  updateAddress() {
    if (this.validateAddressSubmitted()) {
      const addressIndex = this.state.editingPreviousAddressIndex;
      const updatedAddress = {
        addressLine1: this.state.previousAddressLine1,
        addressLine2: this.state.previousAddressLine2,
        addressLine3: this.state.previousAddressLine3,
        addressPostcode: this.state.previousPostcode,
        timeAtMonths: this.state.timeAtMonths,
        timeAtYears: this.state.timeAtYears,
        timeAtLength: this.state.timeAtLength,
        pending: false,
      };
      const addressArray = this.props.previousAddressArray;
      addressArray[addressIndex] = updatedAddress;
      this.props.updatePreviousAddressArray(addressArray);
      this.setState({
        previousAddressLine1: '',
        previousAddressLine2: '',
        previousAddressLine3: '',
        previousPostcode: '',
        timeAtMonths: null,
        timeAtYears: null,
        timeAtLength: '',
        editingPreviousAddress: false,
        editingPreviousAddressIndex: null,
      });
    }
  }

  render() {
    const {
      wrapperClassName,
      showPreviousAddressForm,
      previousAddressArray,
      togglePreviousAddressForm,
      clearPendingRecords,
    } = this.props;

    const editing = this.state.editingPreviousAddress;
    return (
      <div className={`previous-address ${wrapperClassName}`}>
        {(showPreviousAddressForm || editing) && (
          <div className="previous-address__overlay">
            <div
              className="previous-address__form-wrapper"
              onClick={e => e.stopPropagation()}
            >
              <p
                className="previous-address__title "
                data-test="previous-address-title"
              >
                {editing ? 'Edit a previous address' : 'Add a previous address'}
              </p>
              <div
                data-test="popup__close-button"
                className={'popup__close-button previous-address__close-btn'}
                onClick={() => {
                  this.setState(this.initialState());
                  if (!editing) {
                    togglePreviousAddressForm(showPreviousAddressForm);
                    // ensure we nuke this record in the redux state
                    clearPendingRecords();
                  }
                }}
              >
                <div className={'popup__close-icon'} ><Icon name={'times-circle'} /></div>
              </div>
              <Postcode
                textInputLabel={''}
                textInputName="personal__postcode"
                textInputPlaceholder={'Postcode'}
                textInputInitialValue=""
                textInputOnChange={value =>
                  this.props.setPostcodeToSearchFor({ postCode: value })
                }
                buttonClassName="buttonLabel"
                buttonName="send-postcode-search-request"
                buttonTextInitial="Find address"
                buttonTextSearching="Searching..."
                isSearchingPostcode={this.props.isSearchingPostcode}
                sendPostcodeSearchRequest={this.props.sendPostcodeSearchRequest}
                sendPostcodeSearchResponse={
                  this.props.sendPostcodeSearchResponse
                }
                sendPostcodeSearchSelect={address =>
                  this.props.sendPostcodeSearchSelect(
                    address,
                    this.state.editingPreviousAddressIndex ||
                      this.props.previousAddressArray.length
                  )
                }
                sendPostcodeSearchReset={this.props.sendPostcodeSearchReset}
              />
              <div className="previous-address__column">
                <div>
                  <Input
                    dataTest="previous-address-line-1"
                    onChange={e =>
                      this.setState({ previousAddressLine1: e.target.value })
                    }
                    label="Address line 1"
                    value={this.state.previousAddressLine1}
                    wrapperClassName="previous-address__input"
                  />
                  <Error
                    compact
                    title={this.state.error.errorMsg}
                    active={this.state.error.addressLine1}
                  />
                </div>
                <div>
                  <Input
                    dataTest="previous-address-line-2"
                    onChange={e =>
                      this.setState({ previousAddressLine2: e.target.value })
                    }
                    label="Address line 2"
                    value={this.state.previousAddressLine2}
                    wrapperClassName="previous-address__input"
                  />
                  <Error
                    compact
                    title={this.state.error.errorMsg}
                    active={this.state.error.addressLine2}
                  />
                </div>
                <div>
                  <Input
                    dataTest="previous-address-line-3"
                    onChange={e =>
                      this.setState({ previousAddressLine3: e.target.value })
                    }
                    label="Address line 3"
                    value={this.state.previousAddressLine3}
                    wrapperClassName="previous-address__input"
                  />
                  <Error
                    compact
                    title={this.state.error.errorMsg}
                    active={this.state.error.addressLine3}
                  />
                </div>
                <div>
                  <Input
                    dataTest="previous-address-postcode"
                    onChange={e =>
                      this.setState({ previousPostcode: e.target.value })
                    }
                    label="Postcode"
                    value={this.state.previousPostcode}
                    wrapperClassName="previous-address__input"
                  />
                  <Error
                    compact
                    title={this.state.error.errorMsg}
                    active={this.state.error.addressPostCode}
                  />
                </div>
                <div>
                  <TimeAtAddress
                    setTimeAtMonths={val => this.setTimeAtMonths(val)}
                    setTimeAtYears={val => this.setTimeAtYears(val)}
                    timeAtLength={this.state.timeAtLength}
                    timeAtMonths={this.state.timeAtMonths}
                    timeAtYears={this.state.timeAtYears}
                  />
                  <Error
                    compact
                    title={this.state.error.errorMsg}
                    active={this.state.error.addressMoveInDate}
                  />
                </div>

                <Button
                  dataTest={`${
                    editing ? 'edit-previous-address' : 'add-previous-address'
                  }`}
                  className={'btn btn--primary previous-address__btn'}
                  label={`${editing ? 'Update Address' : 'Add address'}`}
                  onClick={() => {
                    if (editing) {
                      this.updateAddress();
                    } else {
                      this.addAddress();
                    }
                  }}
                />
              </div>
            </div>
          </div>
        )}
        <div className="previous-address__container">
          {previousAddressArray.length > 0 && <p>Previous Addresses</p>}
          {previousAddressArray.map((address, i) => (
            <div className="previous-address__item" key={i}>
              {address.addressLine1} ...
              <div className="previous-address__btn-container">
                <div
                  className="previous-address__edit-btn"
                  onClick={() => this.editAddress(i)}
                >
                  <Icon disabled name="edit" data-test={`previous-edit-${i}`} />
                </div>
                <div
                  className="previous-address__delete-btn"
                  onClick={() => this.deleteAddress(i)}
                >
                  <Icon name="trash-alt" />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
