import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import OptInPopup from './index';
import sinon from 'sinon';
import Checkbox from '../Checkbox';
import Input from '../Input';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const actionStub = sandbox.stub();
const onChangeStub = sandbox.stub();
const prizeDrawOptInActionStub = sandbox.stub();
const closeAndSubmitStub = sandbox.stub();
const openTermsAndConditionsStub = sandbox.stub();

const fakeRequiredProps = {
  title: 'fakeTitle',
  description: 'fakeDescription',
  type: 0,
  image: 'fakeImage',
  callToAction: 'fakeLabel',
  action: actionStub,
  onChange: onChangeStub,
  firstOptInLabel: 'fakeLabel',
  prizeDrawLabel: 'fakeLabel',
  prizeDrawOptInAction: prizeDrawOptInActionStub,
  closeAndSubmit: closeAndSubmitStub,
  openTermsAndConditions: openTermsAndConditionsStub
};

test('<OptInPopup>', t => {
  t.equals(typeof OptInPopup.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} />);
  wrapper.instance().toggle();

  t.equals(wrapper.instance().state.visible, true, 'Toggle sets state');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} />);
  wrapper.setState({ visible: true });

  t.equals(wrapper.find('.opt-in-popup--visible').length, 1, 'Popup is visible on state change');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} />);
  wrapper.setState({ visible: true });

  wrapper.find('.opt-in-popup-overlay__handler').simulate('click');
  wrapper.find('.opt-in-popup__close-button').simulate('click');

  t.equals(closeAndSubmitStub.callCount, 2, 'close handler called');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} />);
  wrapper.setState({ visible: true });

  wrapper.find(Input).find('input').simulate('change', { val: 'testText' });

  t.equals(onChangeStub.called, true, 'onChange called');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} />);
  wrapper.setState({ visible: true });

  wrapper.find(Checkbox).first().find('input').simulate('change', { val: true });

  t.equals(actionStub.called, true, 'Action called');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} prizeDrawEnabled />);
  wrapper.setState({ visible: true });

  wrapper.find(Checkbox).at(1).find('input').simulate('change', { val: true });

  t.equals(prizeDrawOptInActionStub.called, true, 'prizeDrawOptInAction called');

  t.end();
});

test('<OptInPopup>', t => {
  const wrapper = mount(<OptInPopup {...fakeRequiredProps} prizeDrawEnabled />);
  wrapper.setState({ visible: true });

  wrapper.find('.opt-in-popup__tandc-link-container').simulate('click');

  t.equals(openTermsAndConditionsStub.called, true, 'openTermsAndConditions called');

  t.end();
});
