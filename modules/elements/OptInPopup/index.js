import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import Input from '../Input';
import Checkbox from '../Checkbox';
import * as optInImages from '../../../assets/images/opt-in-images';
import Error from '../Error';
import Icon from '../Icon';
import reactHTMLParser from 'react-html-parser';

export default class OptInPopup extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    type: PropTypes.number.isRequired,
    callToAction: PropTypes.string,
    cancel: PropTypes.string,
    image: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    email: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
    prizeDrawEnabled: PropTypes.bool,
    firstOptInLabel: PropTypes.string.isRequired,
    prizeDrawLabel: PropTypes.string,
    termsAndConditionsLinkText: PropTypes.string,
    prizeDrawOptInAction: PropTypes.func,
    closeAndSubmit: PropTypes.func.isRequired,
    openTermsAndConditions: PropTypes.func.isRequired,
    transparentBackground: PropTypes.bool,
    optInChecked: PropTypes.bool,
    prizeDrawChecked: PropTypes.bool,
  };
  static defaultProps = {
    callToAction: 'Just do it!',
    cancel: 'nah...',
    title: 'How about some extra cash?',
    description:
      "Sign up for the newsletter! Now! We'll send you loads of tips and tools to make you rich!",
    type: 0,
    prizeDrawEnabled: true,
    image: 'donkey',
    transparentBackground: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      visible: false,
    };
  }

  toggle = () => {
    this.setState({
      visible: !this.state.visible,
    });
  };

  render() {
    const { visible } = this.state;
    const {
      title,
      firstOptInLabel,
      prizeDrawLabel,
      callToAction,
      description,
      image,
      action,
      email,
      onChange,
      error,
      prizeDrawEnabled,
      prizeDrawOptInAction,
      cancel,
      closeAndSubmit,
      openTermsAndConditions,
      termsAndConditionsLinkText,
      transparentBackground,
      optInChecked,
      prizeDrawChecked,
    } = this.props;

    return (
      <div
        className={`opt-in-popup-overlay ${
          visible ? 'opt-in-popup-overlay--visible' : ''
          } ${transparentBackground ? 'opt-in-popup-overlay--transparent' : ''}`}
      >
        <div
          className={'opt-in-popup-overlay__handler'}
          onClick={closeAndSubmit}
        />

        <div
          className={`opt-in-popup ${visible ? 'opt-in-popup--visible' : ''}`}
        >
          <div
            className={'opt-in-popup__close-button'}
            onClick={closeAndSubmit}
          >
            <div className={'opt-in-popup__close-icon'}><Icon name={'times-circle'} /></div>
          </div>

          <div className="opt-in-popup__content">
            <div className="opt-in-popup__top-container">
              <div className="opt-in-popup__image">
                <img
                  className="opt-in-popup__image-src"
                  src={optInImages[image]}
                  alt={`opt-in ${image}`}
                />
              </div>
              <div className="opt-in-popup__text">
                <div className="opt-in-popup__header">{title}</div>
                <div className="opt-in-popup__description">{description}</div>
              </div>
            </div>
            <div className="opt-in-popup__inputs-container">
              <Input
                inputClassName="opt-in-popup__email-input"
                wrapperClassName="opt-in-popup__email-wrapper"
                data-test={'opt-in-email'}
                type={'email'}
                defaultValue={email}
                error={!!error}
                onChange={e => {
                  onChange(e.target.value);
                }}
              />
              <Error compact active={!!error} title={error} />
              <Checkbox
                label={firstOptInLabel}
                onChange={action}
                checked={optInChecked}
                name={'opt-in-1'}
                dataTest={'opt-in-1'}
                type={'custom'}
                iconSet="legacy"
              />
              {prizeDrawEnabled && (
                <Checkbox
                  label={prizeDrawLabel}
                  onChange={prizeDrawOptInAction}
                  checked={prizeDrawChecked}
                  name={'opt-in-2'}
                  dataTest={'opt-in-2'}
                  type={'custom'}
                  iconSet="legacy"
                />
              )}
              {prizeDrawEnabled && (
                <div
                  className="opt-in-popup__tandc-link-container"
                  onClick={openTermsAndConditions}
                >
                  {reactHTMLParser(termsAndConditionsLinkText)}
                </div>
              )}


            </div>
          </div>

          <div className="opt-in-popup__button-bar">
            <Button
              className={'btn--primary opt-in-popup__action'}
              label={callToAction}
              onClick={closeAndSubmit}
              data-test={'opt-in-accept'}
            />
            <Button
              className={'btn--no-border opt-in-popup__cancel'}
              label={cancel}
              onClick={closeAndSubmit}
              data-test={'opt-in-cancel'}
            />
          </div>
        </div>
      </div>
    );
  }
}
