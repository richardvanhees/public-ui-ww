import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import interpolate from '../../utils/interpolate';

export default class NotificationComponent extends PureComponent {
  static propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    visible: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired,
    flash: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  };

  static defaultProps = {
    visible: false,
    flash: false,
    customContent: {},
  };

  componentDidUpdate = () => {
    const time = R.is(Number, this.props.flash) ? this.props.flash : 2000;
    if (this.props.visible && this.props.flash) {
      setTimeout(() => {
        this.props.close();
      }, time);
    }
  };

  iconLookup = type => {
    const iconTypes = {
      error: 'exclamation-triangle',
      warning: 'flag',
      success: 'check-circle',
    };
    return iconTypes[type];
  };

  render() {
    const { type, text, close, visible, customContent } = this.props;
    const { action, customText } = customContent;
    const menuBar = document.getElementsByClassName('mobile-header')[0];
    const positionClassName =
      menuBar && menuBar.getBoundingClientRect().top < 0 ? 'fixed' : 'normal';
    return (
      <div
        onClick={action ? action : close} // eslint-disable-line no-unneeded-ternary
        className={`notification notification--${type} ${
          visible
            ? `notification--show-${positionClassName}`
            : `notification--hide-${positionClassName}`
        }`}
      >
        <span className={`fa fa-${this.iconLookup(type)} notification__icon`} />
        <span className="notification__text">
          {customText ? interpolate(customText) : text}
        </span>
      </div>
    );
  }
}
