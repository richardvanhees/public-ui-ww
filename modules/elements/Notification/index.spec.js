import test from 'tape';
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import NotificationComponent from './index';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const closeStub = sandbox.stub();
const customClickStub = sandbox.stub();

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  type: 'error',
  text: 'fakeText',
  visibile: false,
  close: closeStub,
};

test('<NotificationComponent>', t => {
  const wrapper = mount(<NotificationComponent {...fakeRequiredProps} />);

  wrapper.find('.notification').simulate('click');

  t.equals(
    typeof NotificationComponent.propTypes,
    'object',
    'PropTypes are defined'
  );
  t.equals(closeStub.called, true, 'Close called on click');
  t.equals(
    wrapper.find('.notification--error').length,
    1,
    'Type specific class applied'
  );
  t.equals(
    wrapper.find('.notification--hide-normal').length,
    1,
    'Notification not visible by default'
  );

  t.end();
});

test('<NotificationComponent>', t => {
  const wrapper = mount(
    <NotificationComponent {...fakeRequiredProps} visible />
  );

  t.equals(
    wrapper.find('.notification--show-normal').length,
    1,
    'Notification visible'
  );

  t.equals(
    wrapper.find('.notification__text').text(),
    'fakeText',
    'Notification visible with expected text'
  );

  t.end();
});

test('<NotificationComponent>', t => {
  const fakeProps = {
    type: 'error',
    text: 'fakeText',
    visibile: false,
    close: closeStub,
    customContent: {
      customText: 'Try our new version with custom content',
      action: customClickStub,
    },
  };

  const wrapper = mount(<NotificationComponent {...fakeProps} visible />);

  t.equals(
    wrapper.find('.notification--show-normal').length,
    1,
    'Notification visible'
  );

  t.equals(
    wrapper.find('.notification__text').text(),
    'Try our new version with custom content',
    'Notification visible with expected text'
  );

  wrapper.find('.notification--show-normal').simulate('click');

  t.equals(customClickStub.called, true, 'custom action called on click');

  t.end();
});
