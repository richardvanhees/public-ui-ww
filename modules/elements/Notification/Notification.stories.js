import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Notification from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Notification', module)
  .add('Type: success', () => <Notification type="success" text="Example success." visible close={action('close')} />)
  .add('Type: warning', () => <Notification type="warning" text="Example warning." visible close={action('close')} />)
  .add('Type: error', () => <Notification type="error" text="Example error." visible close={action('close')} />);
