import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ProgressCircle extends Component {
  static propTypes = {
    size: PropTypes.number,
    value: PropTypes.number.isRequired,
    strokeWidth: PropTypes.number,
    randomTimeout: PropTypes.bool,
    color: PropTypes.string.isRequired,
  };

  static defaultProps = {
    size: 280,
    value: 40,
    strokeWidth: 10,
    randomTimeout: false,
  };

  componentDidMount() {
    const { randomTimeout: random, strokeWidth } = this.props;
    this.meter.style.strokeWidth = strokeWidth - 1;

    setTimeout(() => {
      this.meter.style.strokeDashoffset = Math.max(0, this.getProgressPosition());
      this.meter.style.strokeWidth = strokeWidth;
    }, random ? Math.floor(Math.random() * 300) + 500 : 0);
  }

  getProgressPosition() {
    const { size, strokeWidth, value } = this.props;
    const length = (size - strokeWidth) * Math.PI;
    return length * ((100 - value) / 100);
  }

  render() {
    const { size, strokeWidth, color } = this.props;
    const circlePath = `
        M ${size / 2}, ${size / 2}
        m 0, -${(size / 2) - (strokeWidth / 2)}
        a ${(size / 2) - (strokeWidth / 2)},${(size / 2) - (strokeWidth / 2)} 0 1,0 0,${size - strokeWidth}
        a ${(size / 2) - (strokeWidth / 2)},${(size / 2) - (strokeWidth / 2)} 0 1,0 0,-${size - strokeWidth}
      `;

    return (
      <svg
        className="progress-circle"
        xmlns="http://www.w3.org/2000/svg"
        height={size}
        width={size}
        viewBox={`0 0 ${size} ${size}`}
      >
        <path
          ref={c => {
            this.progress = c;
          }}
          className="progress-circle__path progress-circle__path--background"
          stroke="#fff"
          d={circlePath}
          fill="none"
          strokeWidth={strokeWidth - 1}
        />
        <path
          ref={c => {
            this.meter = c;
          }}
          className="progress-circle__path progress-circle__path--meter"
          stroke={color}
          d={circlePath}
          fill="none"
          strokeWidth={strokeWidth}
          strokeDasharray={(size - strokeWidth) * Math.PI}
          strokeDashoffset={(size - strokeWidth) * Math.PI}
        />
      </svg>);
  }
}
