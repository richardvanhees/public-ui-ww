import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ProgressCircle from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<ProgressCircle>', t => {

  t.test('Check if PropTypes are defined', t => {
    t.equals(typeof ProgressCircle.propTypes, 'object', 'PropTypes are defined');

    t.end();
  });

  t.test('Check circle path when empty', t => {
    const component = mount(<ProgressCircle value={0} randomTimeout={false} />);

    const pathLength = component.instance().getProgressPosition();

    t.deepEqual(pathLength, 848.2300164692441, 'Path is correct when empty');

    t.end();
  });

  t.test('Check circle path when 50%', t => {
    const component = mount(<ProgressCircle value={50} randomTimeout={false} />);

    const pathLength = component.instance().getProgressPosition();

    t.deepEqual(pathLength, 424.11500823462205, 'Path is correct when 50%');

    t.end();
  });

  t.test('Check circle path when 75%', t => {
    const component = mount(<ProgressCircle value={75} randomTimeout={false} />);

    const pathLength = component.instance().getProgressPosition();

    t.deepEqual(pathLength, 212.05750411731103, 'Path is correct when 75%');

    t.end();
  });

  t.test('Check circle path when full', t => {
    const component = mount(<ProgressCircle value={100} randomTimeout={false} />);

    const pathLength = component.instance().getProgressPosition();

    t.deepEqual(pathLength, 0, 'Path is correct when full');

    t.end();
  });

});


