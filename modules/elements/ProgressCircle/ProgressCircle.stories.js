import React from 'react';
import { storiesOf } from '@storybook/react';

import ProgressCircle from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('ProgressCircle', module)
  .addDecorator(story => (
    <div style={{ backgroundColor: '#f3f3f3', width: '280px' }}>
      {story()}
    </div>
  ))
  .add('Required', () => <ProgressCircle value={83} color={'#000'} />);
