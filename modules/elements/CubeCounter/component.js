import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';

export default class CubeCounterComponent extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    total: PropTypes.oneOfType([PropTypes.number, PropTypes.arrayOf(PropTypes.bool)]).isRequired,
    current: PropTypes.number.isRequired,
    icon: PropTypes.string,
  };

  getTotalCubes = () => R.is(Array, this.props.total) ? this.props.total.length : this.props.total;

  renderQuestionCounter = () => {
    const cubes = [];
    const { current, total } = this.props;
    const totalCubes = this.getTotalCubes();

    for (let i = 0; i < totalCubes; i++) {
      cubes.push(
        <div
          key={`q${current}-cube${i}`}
          className={`
            cube-counter__cube
            ${i <= current ? 'cube-counter__cube--active' : ''}
            ${total[i] === false ? 'cube-counter__cube--hidden' : ''}
          `}
        />
      );
    }
    return cubes;
  };

  render = () =>
    <div className="cube-counter">
      <div className="cube-counter__inner">
        {this.renderQuestionCounter()}
      </div>
    </div>
}
