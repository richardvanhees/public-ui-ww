import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';

import CubeCounterComponent from './component';

const sandbox = sinon.sandbox.create();

const fakeRequiredProps = {
  total: [true, true, false, true],
  current: 1,
};

test('<CubeCounterComponent>', t => {
  t.equals(
    typeof CubeCounterComponent.propTypes,
    'object',
    'PropTypes are defined'
  );

  t.end();
});

test('<CubeCounterComponent>', t => {
  const component = shallow(<CubeCounterComponent {...fakeRequiredProps} />)
    .find('.cube-counter');

  t.equals(component.length, 1, 'counter exists');

  t.end();
});

test('<CubeCounterComponent>', t => {
  const component = shallow(<CubeCounterComponent {...fakeRequiredProps} />)
    .find('.cube-counter__cube');

  t.equals(component.length, 4, 'Amount of rendered cubes is correct');

  t.end();
});

test('<CubeCounterComponent>', t => {
  const component = shallow(<CubeCounterComponent {...fakeRequiredProps} />)
    .find('.cube-counter__cube--active');

  t.equals(component.length, 2, 'Amount of active cubes is correct');

  t.end();
});

test('<CubeCounterComponent>', t => {
  const component = shallow(<CubeCounterComponent {...fakeRequiredProps} />)
    .find('.cube-counter__cube--hidden');

  t.equals(component.length, 1, 'Amount of hidden cubes is correct');

  t.end();
});

test('<CubeCounterComponent>', t => {
  const localProps = {
    ...fakeRequiredProps,
    total: 4,
  };

  const component = shallow(<CubeCounterComponent {...localProps} />)
    .find('.cube-counter__cube');

  t.equals(component.length, 4, 'Amount of rendered cubes is correct');

  t.end();
});
