import React from 'react';
import ReactTooltip from 'react-tooltip';

const Tooltip = () => (
  <ReactTooltip delayShow={400} className="react-tooltip" type="light" border html />
);

export default Tooltip;
