import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TypeSlider from '../TypeSlider';
import Error from '../Error';

export default class SliderWithInputComponent extends Component {
  static propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    inputLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    onChange: PropTypes.func.isRequired,
    stepSize: PropTypes.number,
    selected: PropTypes.number,
    userInput: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.number,
      PropTypes.string,
    ]),
    dataTest: PropTypes.string,
    minErrorText: PropTypes.string,
    maxErrorText: PropTypes.string,
    showErrors: PropTypes.bool,
  };

  static defaultProps = {
    stepSize: 500,
  };

  constructor(props) {
    super(props);

    this.isValid = this.isValueValid(props.userInput || props.min);

    this.state = {
      amount: (props.userInput || props.min),
      errors: {
        showError: false,
        errorMsg: '',
      },
    };
  }

  componentDidMount() {
    const { userInput } = this.props;

    if (userInput) {
      this.slider.setValue(userInput);
    }
  }

  onChange = (val) => {
    const newVal = parseInt(val, 10);

    this.setAmount(newVal);

    if (this.isValueValid(newVal)) {
      this.slider.setValue(newVal);
    } else if (newVal >= this.props.max) {
      this.slider.setValue(this.props.max);
    }

    this.props.onChange(newVal);
  }

  setAmount = (value) => {
    const valid = this.isValueValid(value);
    this.isValid = valid;
    this.setState({ amount: value });
  }

  isValueValid = (value) => (this.props.min <= value && value <= this.props.max);
  formatAmount = (amount) => `£ ${amount.toLocaleString()}`;

  clearError = () => {
    this.setState({
      errors: {
        showError: false,
      },
    });
  }

  validate = () => {
    const isMinInvalid = this.state.amount < this.props.min;
    const isMaxInvalid = this.state.amount > this.props.max;

    if (isMinInvalid || isMaxInvalid) {
      this.setState({
        errors: {
          showError: true,
          errorMsg:
            isMinInvalid
              ? this.props.minErrorText
              : this.props.maxErrorText,
        },
      });
      return false;
    }

    this.setState({ errors: { showError: false, errorMsg: '' } });
    return true;
  }

  render() {
    const {
      min,
      max,
      inputLabel,
      stepSize,
      selected,
      dataTest,
      showErrors,
    } = this.props;

    const displayValue = this.formatAmount(this.state.amount);

    return (
      <div>
        <div className="slider-with-input">
          <TypeSlider
            ref={c => {
              this.slider = c;
            }}
            selected={selected}
            onChange={this.setAmount}
            steps={stepSize}
            minValue={min}
            maxValue={max}
            hideDots
            onRelease={this.onChange}
          />
          <div className="slider-with-input__input-wrapper">
            <input
              className="slider-with-input__input slider-with-input__input--pound input"
              type="text"
              pattern="\d*"
              value={displayValue}
              onChange={({ target }) => {
                const targetValue = parseInt(
                  target.value.replace(/[^\d\s:]/g, ''),
                  10
                );
                this.onChange(isNaN(targetValue) ? 0 : targetValue);
              }}
              data-test={`slider-input-${dataTest}`}
            />
          </div>
          {inputLabel && (
            <div className="slider-with-input__input-label">{inputLabel}</div>
          )}
        </div>
        <div className="slider-with-input_error-wrapper">
          <Error
            compact
            noPadding
            title={this.state.errors.errorMsg}
            active={showErrors && this.state.errors.showError}
          />
        </div>
      </div>
    );
  }
}
