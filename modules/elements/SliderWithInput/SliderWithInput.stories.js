import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import SliderWithInput from './index';
import '../../../assets/styles/common/index.scss';

let slider = null;

storiesOf('SliderWithInput', module)
  .add('Required', () => <SliderWithInput
    min={0}
    max={1500}
    onChange={action('onChange')}
  />)
  .add('stepSize', () => <SliderWithInput
    stepSize={500}
    min={500}
    max={20000}
    minErrorText="Too low"
    maxErrorText="Too high"
    showErrors onChange={action('onChange')}
  />)
  .add('external validate', () => (<div>
      <button onClick={() => { slider.validate(); }}>show errors</button>
      <button onClick={() => { slider.clearError(); }}>clear errors</button>
      <SliderWithInput
        ref={c => { slider = c; }}
        stepSize={500}
        min={500}
        max={20000}
        showErrors
        minErrorText="Too low"
        maxErrorText="Too high"
      />
    </div>));
