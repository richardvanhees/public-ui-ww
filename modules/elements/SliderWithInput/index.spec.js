import test from 'tape';
import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SliderWithInput from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();
const validMinStub = sandbox.stub();

const fakeRequiredProps = {
  onChange: onChangeStub,
  max: 2,
  min: 0,
  inputLabel: 'test',
};

test('<SliderWithInput>', t => {
  const component = mount(<SliderWithInput {...fakeRequiredProps} />);

  t.equals(typeof SliderWithInput.propTypes, 'object', 'PropTypes are defined');

  component
    .find('.rc-slider-handle')
    .first()
    .simulate('mouseup');
  t.deepEqual(
    onChangeStub.called,
    true,
    'onChange is called when handle is dragged'
  );

  component
    .find('.slider-with-input__input')
    .first()
    .simulate('keyup');
  t.deepEqual(onChangeStub.called, true, 'onChange is called on user input');

  t.end();
});

test('<SliderWithInput>', t => {
  const fakeProps = {
    ...fakeRequiredProps,
    min: 10,
    max: 100,
    userInput: 1,
    showErrors: true,
  };

  const component = mount(<SliderWithInput {...fakeProps} />);
  t.equals(component.instance().isValid, false, 'slider is invalid');

  t.end();
});
