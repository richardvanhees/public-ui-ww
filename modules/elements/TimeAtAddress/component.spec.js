import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TimeLivedAt from './component';

configure({ adapter: new Adapter() });
import timekeeper from 'timekeeper';
import moment from 'moment';

let fakeRequiredProps = {
  dataTest: '',
  setTimeAtMonths: () => {},
  setTimeAtYears: () => {},
  timeAtLength: '1 year(s), 5 month(s)',
  timeAtMonths: '5',
  timeAtYears: '1',
};

test('<TimeLivedAt> html returned as expected', t => {
  const ShallowTimeLivedAt = shallow(<TimeLivedAt {...fakeRequiredProps} />);

  t.equals(
    ShallowTimeLivedAt.html(),
    '<div class="time-at-address " data-test=""><p class="time-at-address__title">When did you move here</p><div class="dropdown time-at-address__dropdown time-at-address__dropdown--first"><select data-test="dropdown-time-at-address-month" class="dropdown__select dropdown__select--double"><option class="dropdown__option dropdown__option--nulllabel">Select...</option><option class="dropdown__option" value="1">Jan</option><option class="dropdown__option" value="2">Feb</option><option class="dropdown__option" value="3">Mar</option><option class="dropdown__option" value="4">Apr</option><option selected="" class="dropdown__option" value="5">May</option><option class="dropdown__option" value="6">Jun</option><option class="dropdown__option" value="7">Jul</option><option class="dropdown__option" value="8">Aug</option><option class="dropdown__option" value="9">Sep</option><option class="dropdown__option" value="10">Oct</option><option class="dropdown__option" value="11">Nov</option><option class="dropdown__option" value="12">Dec</option></select></div><div class="dropdown time-at-address__dropdown"><select data-test="dropdown-time-at-address-year" class="dropdown__select dropdown__select--double"><option class="dropdown__option dropdown__option--nulllabel">Select...</option><option class="dropdown__option" value="2018">2018</option><option class="dropdown__option" value="2017">2017</option><option class="dropdown__option" value="2016">2016</option><option class="dropdown__option" value="2015">2015</option><option class="dropdown__option" value="2014">2014</option><option class="dropdown__option" value="2013">2013</option><option class="dropdown__option" value="2012">2012</option><option class="dropdown__option" value="2011">2011</option><option class="dropdown__option" value="2010">2010</option><option class="dropdown__option" value="2009">2009</option><option class="dropdown__option" value="2008">2008</option><option class="dropdown__option" value="2007">2007</option><option class="dropdown__option" value="2006">2006</option><option class="dropdown__option" value="2005">2005</option><option class="dropdown__option" value="2004">2004</option><option class="dropdown__option" value="2003">2003</option><option class="dropdown__option" value="2002">2002</option><option class="dropdown__option" value="2001">2001</option><option class="dropdown__option" value="2000">2000</option><option class="dropdown__option" value="1999">1999</option><option class="dropdown__option" value="1998">1998</option><option class="dropdown__option" value="1997">1997</option><option class="dropdown__option" value="1996">1996</option><option class="dropdown__option" value="1995">1995</option><option class="dropdown__option" value="1994">1994</option><option class="dropdown__option" value="1993">1993</option><option class="dropdown__option" value="1992">1992</option><option class="dropdown__option" value="1991">1991</option><option class="dropdown__option" value="1990">1990</option><option class="dropdown__option" value="1989">1989</option><option class="dropdown__option" value="1988">1988</option><option class="dropdown__option" value="1987">1987</option><option class="dropdown__option" value="1986">1986</option><option class="dropdown__option" value="1985">1985</option><option class="dropdown__option" value="1984">1984</option><option class="dropdown__option" value="1983">1983</option><option class="dropdown__option" value="1982">1982</option><option class="dropdown__option" value="1981">1981</option><option class="dropdown__option" value="1980">1980</option><option class="dropdown__option" value="1979">1979</option><option class="dropdown__option" value="1978">1978</option><option class="dropdown__option" value="1977">1977</option><option class="dropdown__option" value="1976">1976</option><option class="dropdown__option" value="1975">1975</option><option class="dropdown__option" value="1974">1974</option><option class="dropdown__option" value="1973">1973</option><option class="dropdown__option" value="1972">1972</option><option class="dropdown__option" value="1971">1971</option><option class="dropdown__option" value="1970">1970</option><option class="dropdown__option" value="1969">1969</option></select></div></div>',
    'html returned correctly'
  );

  t.end();
});
