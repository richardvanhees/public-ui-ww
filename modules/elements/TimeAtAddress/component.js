import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Dropdown from '../Dropdown';
import moment from 'moment';
import R from 'ramda';

export const addressMonths = [
  { value: 1, label: 'Jan' },
  { value: 2, label: 'Feb' },
  { value: 3, label: 'Mar' },
  { value: 4, label: 'Apr' },
  { value: 5, label: 'May' },
  { value: 6, label: 'Jun' },
  { value: 7, label: 'Jul' },
  { value: 8, label: 'Aug' },
  { value: 9, label: 'Sep' },
  { value: 10, label: 'Oct' },
  { value: 11, label: 'Nov' },
  { value: 12, label: 'Dec' },
];

export default class TimeAtAddressComponent extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    setTimeAtMonths: PropTypes.func,
    setTimeAtYears: PropTypes.func,
    previousStartMonth: PropTypes.string,
    previousStartYear: PropTypes.string,
  };

  static defaultProps = {
    wrapperClassName: '',
    showTimeAtAddress: false,
  };

  getAddressYears(startYear) {
    const addressYears = [];
    const year = startYear || moment().year();
    const earliestYear = moment().year() - 50;
    for (let i = year; i > earliestYear; i--) {
      addressYears.push(i.toString());
    }
    return addressYears;
  }

  render() {
    const {
      wrapperClassName,
      dataTest,
      setTimeAtMonths,
      setTimeAtYears,
      timeAtLength,
      timeAtMonths,
      timeAtYears,
      showTimeAtAddress,
    } = this.props;

    let addressYears = this.getAddressYears(null);
    let monthsForDropDown = R.clone(addressMonths);

    return (
      <div
        className={`time-at-address ${wrapperClassName}`}
        data-test={dataTest}
      >
        <p className="time-at-address__title">When did you move here</p>
        <Dropdown
          options={monthsForDropDown}
          dataTest="time-at-address-month"
          onChange={val => setTimeAtMonths(val)}
          double
          className="time-at-address__dropdown time-at-address__dropdown--first"
          userInput={timeAtMonths}
        />
        <Dropdown
          options={addressYears}
          dataTest="time-at-address-year"
          onChange={val => setTimeAtYears(val)}
          double
          className="time-at-address__dropdown"
          userInput={timeAtYears}
        />
        {showTimeAtAddress && (
          <p className="time-at-address__time">
            Time at address: {timeAtLength}
          </p>
        )}
      </div>
    );
  }
}
