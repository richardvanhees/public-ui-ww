import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FacebookShareButton, FacebookIcon, LinkedinShareButton, LinkedinIcon, TwitterShareButton, TwitterIcon, WhatsappShareButton, WhatsappIcon } from 'react-share';

export default class Share extends PureComponent {
  static propTypes = {
    shareLink: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div className="share">

        <FacebookShareButton
          url={this.props.shareLink}
          quote="here is a quote"
          className="share__item"
        >
          <FacebookIcon size={30} round />
        </FacebookShareButton>

        <TwitterShareButton
          url={this.props.shareLink}
          title="Money Wizard"
          className="share__item"
        >
          <TwitterIcon size={30} round />
        </TwitterShareButton>

        <LinkedinShareButton
          url={this.props.shareLink}
          className="share__item"
        >
          <LinkedinIcon size={30} round />
        </LinkedinShareButton>

        <WhatsappShareButton
          url={this.props.shareLink}
          subject="I just used Money Wizard"
          body="body"
          className="share__item"
        >
          <WhatsappIcon size={30} round />
        </WhatsappShareButton>
      </div>
    );
  }
}
