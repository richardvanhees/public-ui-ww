import React from 'react';
import Icon from '../Icon';
import PropTypes from 'prop-types';
import defaultImage from '../../../assets/images/logos/inline.png';
import WealthWizards from '../../wealthwizards';

import Button from '../Button';
import Share from './Share';

const HeaderComponent = ({
                           anonymous,
                           image,
                           navigation,
                           imageClassName,
                           offline,
                           home,
                           shareButtons,
                         }) => (
  <div className={`header ${navigation ? 'header--has-navigation' : ''}`}>
    <div onClick={home} className={`header__logo ${imageClassName}`}>
      <img className="header__logo-src" src={image} alt="logo" data-test="header-logo-link" />
    </div>
    {anonymous && (
      <div className="header__top-message header__top-message--anonymous">
        <Icon className="header__icon" name="lock" />
        Anonymous
      </div>
    )}

    {offline && (
      <div className="header__top-message header__top-message--offline">
        <Icon className="header__icon" name="exclamation-triangle" />
        You are offline
      </div>
    )}

    {navigation && (
      <div className="header__navigation">
        {navigation.buttons.map((button, i) => (
          <Button
            key={`header-button-${i}`}
            label={button.label}
            className={`header__button btn ${
              button.type
                ? `btn--${button.type} header__button--regular`
                : 'btn--no-border header__button--divider'
              }`}
            onClick={button.onClick}
            dataTest={button.dataTest}
          >
            {button.label}
          </Button>
        ))}
      </div>
    )}

    {shareButtons && (
      <div className="header__share-buttons">
        <Share shareLink={WealthWizards.SHARE_LINK} />
      </div>
    )}
  </div>
);

HeaderComponent.propTypes = {
  anonymous: PropTypes.bool,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  imageClassName: PropTypes.string,
  offline: PropTypes.bool,
  home: PropTypes.func,
  shareButtons: PropTypes.bool,
  navigation: PropTypes.shape({
    buttons: PropTypes.arrayOf(
      PropTypes.shape({
        // route: PropTypes.string,
        onClick: PropTypes.func,
        label: PropTypes.string.isRequired,
        type: PropTypes.string,
      })
    ),
  }),
};
HeaderComponent.defaultProps = {
  anonymous: false,
  offline: false,
  image: defaultImage,
  imageClassName: '',
  shareButtons: false,
};

export default HeaderComponent;
