import test from 'tape';
import React from 'react';
import { shallow, mount } from 'enzyme';
import HeaderComponent from './index';
import Button from '../Button';
import sinon from 'sinon';

const sandbox = sinon.sandbox.create();
const fakeButtonStub = sandbox.stub();
const homeStub = sandbox.stub();


test('<HeaderComponent>', t => {
  t.equals(
    shallow(<HeaderComponent />).find('.header').length,
    1,
    'Header is rendered correctly'
  );

  t.end();
});

test('<HeaderComponent>', t => {
  const component = mount(<HeaderComponent anonymous />);

  t.equals(component.find('.header__top-message--anonymous').length, 1, 'Anonymous message is displayed');

  t.end();
});

test('<HeaderComponent>', t => {
  const component = mount(<HeaderComponent offline />);

  t.equals(component.find('.header__top-message--offline').length, 1, 'Offline message is displayed');

  t.end();
});

test('<HeaderComponent>', t => {
  const fakeNavigation = {
    buttons: [{
      label: 'fakeLabel',
      type: 'primary',
      onClick: fakeButtonStub
    }, {
      label: 'fakeLabel',
      onClick: fakeButtonStub
    }]
  };

  const component = mount(<HeaderComponent navigation={fakeNavigation} />);
  component.find('button').first().simulate('click');

  t.equals(component.find(Button).length, 2, 'Navigation is rendered');
  t.equals(fakeButtonStub.called, true, 'Navigation button clicked');

  t.end();
});

test('<HeaderComponent>', t => {
  const component = mount(<HeaderComponent home={homeStub} />);
  component.find('.header__logo').simulate('click');

  t.equals(homeStub.called, true, 'Clicked on logo');

  t.end();
});

test('<HeaderComponent>', t => {
  const component = mount(<HeaderComponent shareButtons />);

  t.equals(component.find('.share').length, 1, 'Share buttons rendered');

  t.end();
});
