import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PieChart extends Component {
  static propTypes = {
    size: PropTypes.number,
    className: PropTypes.string,
    data: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
        value: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired,
      })
    ).isRequired,
    strokeWidth: PropTypes.number,
    randomTimeout: PropTypes.bool,
    fixedStartPosition: PropTypes.bool,
    direction: PropTypes.oneOf(['cw', 'ccw']),
    wedgeGrow: PropTypes.number,
    wedgeGrowDelay: PropTypes.number,
    showTooltipOnHover: PropTypes.bool,
    invertWedge: PropTypes.bool,
    tooltipDelay: PropTypes.number,
    shadow: PropTypes.bool,
    centerClassName: PropTypes.string,
  };

  static defaultProps = {
    data: [
      { label: 'b', value: 15, color: '#eee' },
      { label: 'a', value: 85, color: '#009fd5' },
    ],

    // chart options
    className: '',
    centerClassName: '',
    size: 200,
    fixedStartPosition: false,
    randomTimeout: false,
    shadow: false,
    strokeWidth: 20,
    direction: 'cw', // wip, does not work with tooltips yet

    // wedge options
    invertWedge: false,
    wedgeGrow: 5,
    wedgeGrowDelay: 100,

    // tooltip options
    showTooltipOnHover: true,
    tooltipDelay: 400,
  };

  constructor(props) {
    super(props);

    const { size, strokeWidth, data } = props;

    if (strokeWidth * 2 > size) {
      console.error(`PieChart: Current stroke width of ${strokeWidth} is larger than the allowed stroke width of ${size / 2} (size / 2).`); // eslint-disable-line no-console
    }
    if (data.length === 1 && data[0].value > 100) {
      console.error(`A value of ${data[0].value} is invalid when using a single value. Only use percentages up to 100.`); // eslint-disable-line no-console
    }

    this.state = {
      tooltipVisible: false,
      tooltipX: 0,
      tooltipY: 0,
      tooltipColor: '',
      tooltipLabel: '',
      tooltipValue: '',
      tooltipEnabled: props.data.length > 1 ? props.showTooltipOnHover : false,
      strokeWidth: props.size / 2,
      lastHoveredWedgeIndex: null,
    };
  }

  componentDidMount = () => this.animate();

  onWedgeLeave = e => {
    // If it's not a string, we are sure it's not the tooltip, IE hack
    const classToCheck = typeof e.relatedTarget.className === 'string' ?
      e.relatedTarget.className :
      e.relatedTarget.className.baseVal;
    const relatedTargetIsTooltip = classToCheck.includes('pie-chart__tooltip');

    if (!relatedTargetIsTooltip) {
      this.hideTooltip();
      this.resetWedgeSize(this.state.lastHoveredWedgeIndex);
    }
  };

  onWedgeEnter = i => {
    this.setState({ lastHoveredWedgeIndex: i });
    if (this.state.tooltipEnabled) {
      this.setWedgeSize(i);
      this.setTooltip(i);
    }
  };

  getValueStartPositionAsPercentage = i => {
    const { data } = this.props;
    const totalValue = data.reduce((total, current) => total + current.value, 0);
    const startValue = data.reduce((total, current, index) => total + (i < index ? current.value : 0), 0);

    return (startValue / totalValue) * 100;
  };

  getValuePercentage = i => {
    const { data } = this.props;
    const totalValue = data.length === 1 ? 100 : data.reduce((total, current) => total + current.value, 0);
    return (data[i].value / totalValue) * 100;
  };

  getValueLength = i => {
    const { size } = this.props;
    const { strokeWidth } = this.state;
    const length = (size - strokeWidth) * Math.PI;
    return length * ((100 - this.getValuePercentage(i)) / 100);
  };

  getValueRotation = i => `rotate(-${360 * (this.getValueStartPositionAsPercentage(i) / 100)}deg)`;

  getValueRotationForIE = i => `rotate(-${360 * (this.getValueStartPositionAsPercentage(i) / 100)} ${this.getCanvasSize() / 2} ${this.getCanvasSize() / 2})`;

  getCanvasSize = () => this.props.invertWedge ? this.props.size : this.props.size * (1 + ((this.props.wedgeGrow / 100) * 2));

  getWedgeGrowth = () => this.props.invertWedge ? 1 - (this.props.wedgeGrow / 100) : 1 + (this.props.wedgeGrow / 100);

  setWedgeSize = i => {
    this.wedgeTimeout = setTimeout(() => {
      this.meters[i].style.transform = `${this.meters[i].style.transform} scale(${this.getWedgeGrowth()})`;
    }, this.props.wedgeGrowDelay);
  };

  setTooltip = i => {
    clearTimeout(this.tooltipTimeout);

    const { size, data, tooltipDelay, wedgeGrow } = this.props;
    const { strokeWidth } = this.props;

    const radius = (size * this.getWedgeGrowth()) / 2 - (strokeWidth / 2);
    const degrees = (this.getValueStartPositionAsPercentage(i) + this.getValuePercentage(i) / 2) * 3.6;

    const newX = strokeWidth / 2 + radius + wedgeGrow + radius * Math.cos(2 * Math.PI * (degrees / 360 - 0.25));
    const newY = strokeWidth / 2 + radius + wedgeGrow + radius * Math.sin(2 * Math.PI * (degrees / 360 - 0.25));

    this.tooltipTimeout = setTimeout(() => {
      this.setState({
        tooltipX: newX,
        tooltipY: newY,
        tooltipVisible: true,
        tooltipColor: data[i].color,
        tooltipLabel: data[i].label,
        tooltipValue: data[i].value,
      });
    }, tooltipDelay);
  };

  animate = () => {
    const { randomTimeout: random, fixedStartPosition } = this.props;
    const { strokeWidth } = this.state;

    this.meters.forEach((meter, i) => {
      meter.style.strokeWidth = strokeWidth - 1; // eslint-disable-line no-param-reassign
      fixedStartPosition && (meter.style.transform = this.getValueRotation(i)); // eslint-disable-line no-param-reassign
    });

    setTimeout(() => {
      this.meters.forEach((meter, i) => {
        meter.style.strokeDashoffset = Math.max(0, this.getValueLength(i)); // eslint-disable-line no-param-reassign
        meter.style.strokeWidth = strokeWidth; // eslint-disable-line no-param-reassign
        !fixedStartPosition && (meter.style.transform = this.getValueRotation(i)); // eslint-disable-line no-param-reassign

        setTimeout(() => {
          this.meters.forEach(currentMeter => {
            currentMeter.style.transition = 'all .1s ease-in-out'; // eslint-disable-line no-param-reassign
          });
        }, 850);
      });
    }, random ? Math.floor(Math.random() * 300) + 500 : 250);
  };

  meters = [];

  resetWedgeSize = i => {
    clearTimeout(this.wedgeTimeout);
    const regexp = new RegExp(`scale\\(${this.getWedgeGrowth()}\\)`, 'g');
    this.meters[i].style.transform = this.meters[i].style.transform.replace(regexp, '');
  };

  hideTooltip = () => {
    clearTimeout(this.tooltipTimeout);
    this.setState({ tooltipVisible: false });
  };

  definePath = () => {
    const { size } = this.props;
    const { strokeWidth } = this.state;
    return `
        M ${this.getCanvasSize() / 2}, ${this.getCanvasSize() / 2}
        m 0, -${(size / 2) - (strokeWidth / 2)}
        a ${(size / 2) - (strokeWidth / 2)},${(size / 2) - (strokeWidth / 2)} 0 1,0 0,${size - strokeWidth}
        a ${(size / 2) - (strokeWidth / 2)},${(size / 2) - (strokeWidth / 2)} 0 1,0 0,-${size - strokeWidth}
      `;
  };

  render() {
    const { size, data, direction, className, shadow, fixedStartPosition, showTooltipOnHover, centerClassName } = this.props;
    const { strokeWidth } = this.state;
    const { tooltipX, tooltipY, tooltipColor, tooltipLabel, tooltipValue, tooltipVisible } = this.state;
    const circlePath = this.definePath();

    return (
      <div className={`pie-chart ${className}`} style={{ width: this.getCanvasSize(), height: this.getCanvasSize() }}>
        <svg
          className={`pie-chart__chart ${direction === 'ccw' ? 'pie-chart__chart--inverted' : ''} ${shadow ? 'pie-chart__chart--shadow' : ''}`}
          xmlns="http://www.w3.org/2000/svg"
          height={this.getCanvasSize()}
          width={this.getCanvasSize()}
          viewBox={`0 0 ${this.getCanvasSize()} ${this.getCanvasSize()}`}
        >
          <path
            ref={c => {
              this.progress = c;
            }}
            className="pie-chart__path pie-chart__path--background"
            stroke="#fffff"
            d={circlePath}
            fill="none"
            strokeWidth={1}
          />

          {data.map((set, i) =>
            <path
              ref={c => {
                this.meters[i] = c;
              }}
              className={`pie-chart__path pie-chart__path--meter ${fixedStartPosition ? 'pie-chart__path--fixed' : ''}`}
              stroke={set.color}
              d={circlePath}
              fill="none"
              key={`pie-chart-meter-${i}`}
              transform={this.getValueRotationForIE(i)}
              onMouseEnter={() => this.onWedgeEnter(i)}
              onMouseLeave={e => this.onWedgeLeave(e)}
              onClick={() => {
                // Leave this empty function here to initialize the ability to interact with svg's on ios/safari
                // If this is not triggered, the svg will be rendered without that ability, which
                // causes the mouseEnter/Leave to not work. #JustSafariThings
              }}
              strokeWidth={strokeWidth}
              strokeDasharray={(size - strokeWidth) * Math.PI}
              strokeDashoffset={(size - strokeWidth) * Math.PI}
            />
          )}
        </svg>
        {showTooltipOnHover && <div
          className={`pie-chart__tooltip ${tooltipVisible ? 'pie-chart__tooltip--visible' : ''}`}
          style={{ top: tooltipY, left: tooltipX }}
          onMouseLeave={e => this.onWedgeLeave(e)}
        >
          <div
            className="pie-chart__tooltip-color"
            style={{ background: tooltipColor }}
          />
          <div className="pie-chart__tooltip-label">
            {tooltipLabel}: {tooltipValue}
          </div>
          <div className="pie-chart__tooltip-arrow" />
        </div>}

        <div
          className={`pie-chart__center-circle ${centerClassName}`}
          style={{
            height: size - this.props.strokeWidth * 2,
            width: size - this.props.strokeWidth * 2,
          }}
        />

      </div>);
  }
}
