import React from 'react';
import { storiesOf } from '@storybook/react';

import PieChart from './index';
import '../../../assets/styles/common/index.scss';

const sampleData = [
  { value: 25, color: '#009fd5', label: 'Value 1' },
  { value: 25, color: '#0079a2', label: 'Value 2' },
  { value: 50, color: '#00536f', label: 'Value 3' },
];
const singleValue = [
  { value: 70, label: 'progress', color: '#009fd5' },
];

storiesOf('PieChart', module)
  .add('Required', () => <PieChart data={sampleData} />)
  .add('CCW', () => <PieChart data={sampleData} direction={'ccw'} />)
  .add('Single value (progress)', () => <PieChart data={singleValue} />)
  .add('Fixed start positions', () => <PieChart data={sampleData} fixedStartPosition />)
  .add('Full pie chart', () => <PieChart data={sampleData} fullPieChart />)
  .add('Full - inverted wedge', () => <PieChart data={sampleData} fullPieChart invertWedge />)
  .add('Full - wedge grow', () => <PieChart data={sampleData} fullPieChart wedgeGrow={20} />);
