import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';
import PieChart from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  data: [
    { value: 50, label: 'fakeValue 1', color: '#333' },
    { value: 25, label: 'fakeValue 2', color: '#666' }
  ]
};

test('<PieChart>', t => {

  t.equals(typeof PieChart.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<PieChart>', t => {
  let wrapper = mount(<PieChart {...fakeRequiredProps} />);
  t.equals(wrapper.find('.pie-chart__path--fixed').length, 0, 'FixedStartPosition is not applied');

  wrapper = mount(<PieChart {...fakeRequiredProps} fixedStartPosition />);
  t.equals(wrapper.find('.pie-chart__path--fixed').length > 0, true, 'FixedStartPosition is applied');

  t.end();
});

test('<PieChart>', t => {
  let wrapper = mount(<PieChart {...fakeRequiredProps} />);
  t.equals(wrapper.find('.pie-chart__tooltip').length, 1, 'Tooltip is rendered');

  wrapper = mount(<PieChart {...fakeRequiredProps} showTooltipOnHover={false} />);
  t.equals(wrapper.find('.pie-chart__tooltip').length, 0, 'Tooltip is not rendered');

  t.end();
});