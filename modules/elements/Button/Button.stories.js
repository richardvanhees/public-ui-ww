import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Button', module)
  .add('Required', () => <Button label="Example" onClick={action('onClick')} />)
  .add('invertOnHover', () => <Button style={{ borderColor: 'blue', color: 'blue' }} invertOnHover label="Example" onClick={action('onClick')} />)
  .add('Type: primary', () => <Button type="primary" label="Example" onClick={action('onClick')} />)
  .add('Type: outline', () => <Button type="outline" label="Example" onClick={action('onClick')} />)
  .add('Type: no-border', () => <Button type="no-border" label="Example" onClick={action('onClick')} />);
