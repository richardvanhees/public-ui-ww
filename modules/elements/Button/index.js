import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component {
  static propTypes = {
    className: PropTypes.string,
    dataTest: PropTypes.string,
    onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]).isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    type: PropTypes.oneOf(['primary', 'secondary', 'outline', 'no-border']),
    style: PropTypes.object,
    invertOnHover: PropTypes.bool,
    wide: PropTypes.bool,
  };
  static defaultProps = {
    className: '',
    invertOnHover: false,
    wide: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      hovered: false,
      backgroundColor: '#fff',
      textColor: '#000',
    };
  }

  componentDidMount() {
    this.setColor();
  }

  setColor = (hovered = false) => {
    const backgroundColor = window.getComputedStyle(this.button, null).getPropertyValue('background-color');
    const textColor = window.getComputedStyle(this.button, null).getPropertyValue('color');

    if (this.props.invertOnHover) {
      this.setState({
        hovered,
        backgroundColor: textColor,
        color: backgroundColor,
      });
    }
  };

  render() {
    const { className, label, dataTest, invertOnHover, style, wide } = this.props;
    let { type, onClick } = this.props;
    const { backgroundColor, color } = this.state;

    const hoveredStyle = invertOnHover && this.state.hovered ? {
      backgroundColor,
      color,
    } : {};

    const newStyle = {
      ...style,
      ...hoveredStyle,
    };

    if (type && (className.includes('btn--primary') || className.includes('btn--outline') || className.includes('btn--no-border'))) {
      throw Error('Do not use classNames btn--primary, btn--outline or btn--no-border in combination with type prop.');
    }

    if (!type && !className.includes('btn--primary') && !className.includes('btn--outline')) {
      type = 'outline';
    }

    if (onClick === false) {
      onClick = () => {
      };
    }

    return (
      <button
        ref={c => {
          this.button = c;
        }}
        className={`btn ${className} ${type ? `btn--${type}` : ''} ${wide ? 'btn--wider' : ''}`}
        onClick={onClick}
        data-test={dataTest}
        style={newStyle}
        onMouseEnter={() => this.setColor(true)}
        onMouseLeave={() => this.setState({ hovered: false })}
      >
        {label}
      </button>
    );
  }
}
