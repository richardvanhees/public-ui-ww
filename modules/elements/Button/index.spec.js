import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import sinon from 'sinon';

import ButtonComponent from './index';

const sandbox = sinon.sandbox.create();
const onClickStub = sandbox.stub();

const fakeRequiredProps = {
  onClick: onClickStub,
  label: 'testLabel',
};

test('<ButtonComponent>', t => {
  t.equals(typeof ButtonComponent.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    mount(<ButtonComponent {...fakeRequiredProps} />).find('.btn--outline')
      .length,
    1,
    'Outline is set as default'
  );

  t.equals(
    mount(
      <ButtonComponent {...fakeRequiredProps} className={`btn--primary`} />
    ).find('.btn--outline').length,
    0,
    'Outline class is hidden if primary is used'
  );

  const wrapper = mount(<ButtonComponent {...fakeRequiredProps} />);
  wrapper.simulate('click');
  t.deepEqual(onClickStub.called, true, 'onClick is called at click');

  t.equals(
    mount(
      <ButtonComponent {...fakeRequiredProps} type={`primary`} />
    ).find('.btn--outline').length,
    0,
    'Outline class is hidden if primary type is used'
  );

  const component = mount(
    <ButtonComponent {...fakeRequiredProps} invertOnHover style={{ color: '#f00', backgroundColor: '#00f' }} />
  ).find('button').simulate('mouseenter');

  t.equals(
    component.instance().style._values['background-color'],
    'rgb(255, 0, 0)',
    'Background color is used as text color when invertOnHover'
  );

  t.equals(
    component.instance().style._values['color'],
    'rgb(0, 0, 255)',
    'Text color is used as background when invertOnHover'
  );

  t.end();
});
