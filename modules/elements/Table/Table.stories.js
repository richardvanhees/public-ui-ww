import React from 'react';
import { storiesOf } from '@storybook/react';

import Table from './index';
import '../../../assets/styles/common/index.scss';

const exampleRows = [
  ['foo', 1, 2, 3, 4],
  ['bar', 5, 6, 7, 8],
  ['baz', 9, 1, 2, 3],
];
const exampleHeaderLabels = ['text', 'number0', 'number1', 'number2', 'number3'];

storiesOf('Table', module)
  .add('Required', () => <Table rows={exampleRows} headerLabels={exampleHeaderLabels} />);
