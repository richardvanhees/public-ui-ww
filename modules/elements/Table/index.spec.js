import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Table from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Table>', t => {
  t.equals(typeof Table.propTypes, 'object', 'PropTypes are defined');

  t.end();
});
