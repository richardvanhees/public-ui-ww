import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import ContactPhone from './index';

test('<ContactPhone>', assert => {
  assert.plan(1);

  const component = mount(<ContactPhone />);
  component.find('.contact-phone__opening-hours').simulate('click');
  assert.equals(
    component.instance().state.phoneTimesOpen,
    true,
    'Opening times are shown on click'
  );
});
