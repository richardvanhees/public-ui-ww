import React, { Component } from 'react';
import Icon from '../Icon';
import moment from 'moment';
import PropTypes from 'prop-types';
import CloseButton from '../CloseButton';

export default class ContactPhone extends Component {

  static propTypes = {
    phoneTimesOpen: PropTypes.func,
    closeButtonAction: PropTypes.func,
    className: PropTypes.string,
    withIcon: PropTypes.bool,
  };

  static defaultProps = {
    className: '',
    withIcon: false,
  };

  constructor() {
    super();

    this.state = {
      phoneTimesOpen: false,
    };
  }

  setPhoneAvailabilityLine = () => {
    const d = moment();
    const format = 'hh:mm';
    const beforeWork = moment('09:00', format);
    const afterWork = moment('17:30', format);

    if (d.get('day') > 0 && d.get('day') <= 5) {
      if (d.isBetween(beforeWork, afterWork)) {
        return {
          color: '#008E2D',
          text: 'Our line is open until 17:30 today',
        };
      } else if (d.isBefore(beforeWork)) {
        return {
          color: '#4E585E',
          text: 'Our line is open at 9am today',
        };
      } else if (d.isAfter(afterWork) && !(d.get('day') === 5)) {
        return {
          color: '#4E585E',
          text: 'Our line is open at 9am tomorrow',
        };
      }
    }

    if ((d.get('day') === 5 && d.isAfter(afterWork)) || d.get('day') === 6) {
      return {
        color: '#4E585E',
        text: 'Our line is open at 9am Monday',
      };
    }

    if (d.get('day') === 0) {
      return {
        color: '#4E585E',
        text: 'Our line is open at 9am tomorrow',
      };
    }
    return null;
  };

  render() {
    const { phoneTimesOpen } = this.state;
    return (
      <div className={`contact-phone ${this.props.className}`}>
        {this.props.closeButtonAction && <CloseButton
          className="contact-phone__content-close"
          onClick={this.props.closeButtonAction}
        />}
        <div
          className={`contact-phone__content contact-phone__content--phone ${phoneTimesOpen ? 'contact-phone__content--phone-times-open' : ''}`}
        >
          <div className="contact-phone__content-inner">
            <h3 className="contact-phone__title">Need help?</h3>
            <a
              className="contact-phone__link contact-phone__link--phone-number"
              href={'tel:01926671469'}
            >
              <Icon
                name={'phone'}
                className={`${this.props.withIcon ? 'contact-phone__title-icon' : ''}`}
              /> 01926 671469
            </a>

            <div
              className="contact-phone__phone-available"
              style={{ color: this.setPhoneAvailabilityLine().color }}
            >
              {this.setPhoneAvailabilityLine().text}
            </div>
            <a
              className="contact-phone__opening-hours"
              onClick={() => {
                this.setState({
                  phoneTimesOpen: !phoneTimesOpen,
                }, () => {
                  if (this.props.phoneTimesOpen) this.props.phoneTimesOpen(this.state.phoneTimesOpen);
                });
              }}
            >
              View opening hours{' '}
              <Icon
                className={`contact-phone__opening-hours-icon ${
                  phoneTimesOpen ? 'contact-phone__opening-hours-icon--active' : ''
                }`}
                name={'angle-down'}
              />
            </a>

            <div
              className={`contact-phone__hours-table ${
                phoneTimesOpen ? 'contact-phone__hours-table--visible' : ''
              }`}
            >
              <div className="contact-phone__hours-table-inner">
                <div className="contact-phone__hours-row">
                  <div className="contact-phone__hours-label">Monday to Friday</div>
                  <div className="contact-phone__hours-time">09:00 - 17:30</div>
                </div>
                <div className="contact-phone__hours-row">
                  <div className="contact-phone__hours-label">
                    Saturday and Sunday
                  </div>
                  <div className="contact-phone__hours-time">Closed</div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}
