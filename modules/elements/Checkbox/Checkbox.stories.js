import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Checkbox from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Checkbox', module)
  .add('Required', () => <Checkbox label="Example" name="example" dataTest="example-data-test" onChange={action('onChange')} />)
  .add('Disabled', () => <Checkbox disabled label="Example" name="example" dataTest="example-data-test" onChange={action('onChange')} />)
  .add('Checked', () => <Checkbox checked label="Example" name="example" dataTest="example-data-test" onChange={action('onChange')} />)
  .add('Type: custom', () => <Checkbox type="custom" label="Example" name="example" dataTest="example-data-test" onChange={action('onChange')} />);
