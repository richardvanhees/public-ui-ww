import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default class Checkbox extends Component {
  static propTypes = {
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    dataTest: PropTypes.string.isRequired,
    type: PropTypes.string,
    className: PropTypes.string,
    iconSet: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    type: 'default',
    className: '',
    checked: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      checked: props.checked,
    };
  }

  componentDidUpdate = prevProps =>
    prevProps.checked !== this.props.checked &&
    this.setState({ checked: this.props.checked });

  toggleCheck = checked => this.props.onChange(checked);

  render() {
    const { label, name, dataTest, type, className, iconSet, disabled } = this.props;

    return (
      <div
        className={`checkbox ${
          type !== 'default' ? `checkbox--${type}` : ''
          } ${className}`}
      >
        <label
          className={`checkbox__container ${
            type !== 'default' ? `checkbox__container--${type}` : ''
            }`}
          data-test={dataTest}
        >
          <input
            className={`checkbox__checkbox ${
              type !== 'default' ? `checkbox__checkbox--${type}` : ''
              }`}
            type={'checkbox'}
            checked={this.state.checked}
            name={name}
            onChange={e => this.toggleCheck(e.target.checked)}
            disabled={disabled}
          />

          {type === 'custom' && (
            <div className="checkbox__custom-box">
              {this.state.checked && <Icon iconSet={iconSet} name={'check'} />}
            </div>
          )}

          <span
            className={`checkbox__label ${
              type !== 'default' ? `checkbox__label--${type}` : ''
              }`}
          >
            {label}
          </span>
        </label>
      </div>
    );
  }
}
