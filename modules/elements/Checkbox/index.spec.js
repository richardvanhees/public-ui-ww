import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Checkbox from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });
const sandbox = sinon.sandbox.create();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  onChange: onChangeStub,
  label: 'testLabel',
  name: 'testName',
  dataTest: 'testDataTest'
};

test('<Checkbox>', t => {
  t.equals(typeof Checkbox.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Checkbox>', t => {

  mount(<Checkbox {...fakeRequiredProps} />)
    .find('.checkbox__checkbox')
    .simulate('change');

  t.equals(onChangeStub.called,
    true,
    'onChange triggered'
  );

  t.end();
});

test('<Checkbox>', t => {
  const component = mount(<Checkbox {...fakeRequiredProps} checked />)
    .find('.checkbox__checkbox');

  t.equals(component.instance().checked,
    true,
    'Checked by default if checked prop is passed'
  );

  t.end();
});