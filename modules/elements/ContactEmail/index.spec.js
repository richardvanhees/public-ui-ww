import test from 'tape';
import React from 'react';
import { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContactEmail from './index';
import sinon from 'sinon';


const sandbox = sinon.sandbox.create();
const closeStub = sandbox.stub();
const fakeRequiredProps = {
  closeButtonAction: closeStub
};

test('<ContactEmail>', assert => {
  assert.plan(1);

  const component = mount(<ContactEmail {...fakeRequiredProps} />);
  component.find('CloseButton').simulate('click');
  assert.equals(
    closeStub.callCount, 1,
    'content close exists, can be clicked'
  );
});

test('<ContactEmail>', assert => {
  assert.plan(1);

  const component = mount(<ContactEmail />);
  const target = component.find('CloseButton')
  assert.equals(
    target.length, 0,
    'content close not displayed'
  );
});
