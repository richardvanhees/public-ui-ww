import React from 'react';
import PropTypes from 'prop-types';
import CloseButton from '../CloseButton';
import Icon from '../Icon';

const ContactEmail = ({
  closeButtonAction,
  className,
  withIcon,
}) => (
<div className={`contact-email ${className}`}>
  {closeButtonAction && <CloseButton
    className="contact-email__content-close"
    onClick={closeButtonAction}
  />}
  <div className="contact-email__content-inner">
    <h3 className="contact-email__title">
      {withIcon && <Icon
        name="envelope"
        className="contact-email__title-icon"
      />}
      Drop us an email
    </h3>
    <a
      className="contact-email__link contact-email__link--email"
      href={'mailto:thewizard@wealthwizards.com'}
    >
      thewizard@wealthwizards.com
    </a>
    <div className="contact-email__email-description">
      We'll do our best to reply to you within one working day.
    </div>
  </div>
</div>
);

ContactEmail.propTypes = {
  closeButtonAction: PropTypes.func,
  className: PropTypes.string,
  withIcon: PropTypes.bool,
};

ContactEmail.defaultProps = {
  className: '',
  withIcon: false,
};

export default ContactEmail;
