import PropTypes from 'prop-types';

export const propTypes = {
  description: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.any,
  style: PropTypes.object,
  active: PropTypes.bool,
  inline: PropTypes.bool,
  compact: PropTypes.bool,
  dark: PropTypes.bool,
  className: PropTypes.string,
};

export const defaultProps = {
  type: 'outline',
  style: {},
  active: false,
  compact: false,
  inline: false,
  className: '',
  dark: false,
};
