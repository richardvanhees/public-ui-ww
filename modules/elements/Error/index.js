import React, { PureComponent } from 'react';
import { propTypes, defaultProps } from './model';
import Icon from '../Icon';
import classNames from 'classnames';

export default class Error extends PureComponent {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor() {
    super();
    this.setClassNames = this.setClassNames.bind(this);
  }

  setClassNames() {
    const { type, active, compact, noPadding, inline, dark, center } = this.props;
    const errorType = type ? `error-message--${type}` : null;

    if (type === 'active' && active === true) {
      console.error('Please do not use type="active" and active prop at the same time'); // eslint-disable-line no-console
    }

    return classNames({
      'error-message': true,
      [errorType]: !!errorType,
      'error-message--active': active,
      'error-message--compact': compact,
      'error-message--inline': inline,
      'error-message--inline-active': inline && active,
      'error-message--no-padding': noPadding,
      'error-message--dark': dark,
      'error-message--center': center,
    });
  }

  render() {
    const { description, title, style, compact, className, dark, dataTest } = this.props;

    return compact ? (
      <div className={`${this.setClassNames()} ${className}`} style={style} data-test={dataTest}>
        <div className="error-message__body error-message__body--compact">
          <Icon name="exclamation" iconSet="solid" style={(dark) ? { display: 'none' } : {}} />{' '}
          <p className="error-message__title error-message__title--compact">
            {title}
          </p>
        </div>
      </div>
    ) : (
      <div className={`${this.setClassNames()} ${className}`} style={style} data-test={dataTest}>
        {title && <div className="error-message__title">{title}</div>}
        <div className="error-message__infobox">
          <div className="error-message__icon">
            <Icon name="exclamation-triangle" />
          </div>
          <div
            className="error-message__description"
            data-test="error-msg-description"
          >
            {description}
          </div>
        </div>
      </div>
    );
  }
}
