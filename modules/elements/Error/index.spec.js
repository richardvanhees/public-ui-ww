import test from 'tape';
import React from 'react';
import { shallow } from 'enzyme';

import ErrorComponent from './index';

const fakeRequiredProps = {
  description: 'FakeDescription',
};

test('<ErrorComponent>', t => {
  t.equals(typeof ErrorComponent.propTypes, 'object', 'PropTypes are defined');

  t.equals(
    shallow(<ErrorComponent {...fakeRequiredProps} />).find(
      '.error-message__title'
    ).length,
    0,
    'Title is hidden if not provided'
  );

  t.equals(
    shallow(<ErrorComponent {...fakeRequiredProps} title={`TestTitle`} />).find(
      '.error-message__title'
    ).length,
    1,
    'Title is shown if provided'
  );

  t.equals(
    shallow(<ErrorComponent {...fakeRequiredProps} />).find(
      '.error-message--outline'
    ).length,
    1,
    'Outline is the default type'
  );

  t.equals(
    shallow(<ErrorComponent {...fakeRequiredProps} type={`testType`} />).find(
      '.error-message--testType'
    ).length,
    1,
    'Style of error is set with type'
  );

  t.equals(
    shallow(<ErrorComponent {...fakeRequiredProps}  dark />).find(
      '.error-message--dark'
    ).length,
    1,
    'Style of dark is set with type'
  );

  t.end();
});
