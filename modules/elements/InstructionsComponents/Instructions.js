import React from 'react';
import PropTypes from 'prop-types';

import InstructionsItem from './InstructionsItem';
import ContactDetails from '../ContactDetails';

const Instructions = ({
                        title,
                        items,
                        list,
                        bullet,
                        bulletClassName,
                        wide,
                        contactDetails,
                        className,
                        listClassName,
                        itemClassName,
                      }) => (
  <div
    className={`instructions ${wide ? 'instructions--wide' : ''} ${className}`}
  >
    {title && <h3 className="instructions__title">{title}</h3>}
    <div className="instructions__items">
      {list && (
        <ul
          className={`instructions__list ${listClassName}`}
          style={{ marginBottom: 0 }}
        >
          {items.map((instruction, i) => (
            <li
              key={`instruction-item-${i}`}
              className={`instructions__list-item ${itemClassName}`}
              data-test={`instructions-item-${i}`}
            >
              {!bullet && (
                <span className="instructions__list-number">{i + 1}.</span>
              )}
              {bullet && (
                <span
                  className={`instructions__list-custom-bullet ${bulletClassName}`}
                >
                  {bullet}
                </span>
              )}
              <span className="instruction__list-label">{instruction}</span>
            </li>
          ))}
        </ul>
      )}

      {!list &&
      items.map((item, i) => (
        <InstructionsItem key={`start-item-${i}`} {...item} />
      ))}
    </div>
    {contactDetails && (
      <div className="instructions__contact-details">
        {contactDetails.map((detail, i) => (
          <ContactDetails {...detail} key={`detail-${i}`} />
        ))}
      </div>
    )}
  </div>
);

Instructions.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.object])).isRequired,
  list: PropTypes.bool,
  bullet: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
    PropTypes.number,
  ]),
  bulletClassName: PropTypes.string,
  wide: PropTypes.bool,
  contactDetails: PropTypes.array,
  className: PropTypes.string,
  listClassName: PropTypes.string,
  itemClassName: PropTypes.string,
};

Instructions.defaultProps = {
  bulletClassName: '',
  className: '',
  listClassName: '',
  itemClassName: '',
};

export default Instructions;
