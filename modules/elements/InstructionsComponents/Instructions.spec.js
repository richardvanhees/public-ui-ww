import test from 'tape';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Instructions from './Instructions';
import InstructionsItem from './InstructionsItem';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {
  items: ['item1', 'item2']
};

test('<Instructions>', t => {
  t.equals(typeof Instructions.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Instructions>', t => {
  const component = mount(<Instructions {...fakeRequiredProps} list />);

  t.equals(component.find('.instructions__list-item').length, 2, 'List items rendered is equal to amount of items');

  t.end();
});

test('<Instructions>', t => {
  const component = mount(<Instructions {...fakeRequiredProps} wide list />);

  t.equals(component.find('.instructions--wide').length, 1, 'Wide class is applied to component');

  t.end();
});

test('<Instructions>', t => {
  const fakeContactDetails = [{
    type: 'phone',
    value: '123'
  }];
  const component = mount(<Instructions {...fakeRequiredProps} contactDetails={fakeContactDetails} list />);

  t.equals(component.find('.instructions__contact-details').length, 1, 'Contact details are rendered');

  t.end();
});

test('<Instructions>', t => {
  const fakeLocalProps = {
    items: [{
      title: 'fakeTitle',
      text: 'fakeText'
    }, {
      title: 'fakeTitle2',
      text: 'fakeText2'
    }]
  };
  const component = mount(<Instructions {...fakeLocalProps} />);

  t.equals(component.find(InstructionsItem).length, 2, 'InstructionsItems are rendered');

  t.end();
});

