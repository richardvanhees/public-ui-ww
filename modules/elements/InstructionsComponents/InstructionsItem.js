import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';
import * as images from '../../../assets/images/iw-images';

const InstructionsItem = ({ title, text, icon, image, color }) => (
  <div className="instructions__item-container">
    <div className={'instructions__icon'}>
      {icon && <Icon style={{ color }} name={icon} />}
      {image && (
        <img
          className="instructions__icon-src"
          src={images[image]}
          alt={'instructions icon'}
        />
      )}
    </div>

    <div className="instructions__sub-container">
      <h4 className="instructions__item-title">{title}</h4>
      <p className="instructions__item-text">{text}</p>
    </div>
  </div>
);

InstructionsItem.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string,
  image: PropTypes.string,
  color: PropTypes.string,
};

export default InstructionsItem;
