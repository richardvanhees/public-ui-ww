import React from 'react';
import PropTypes from 'prop-types';

const MoreInfo = ({
  link,
}) => (
  !!link ? (<div className="more-info">
    <p className="more-info__text">
      To find out more information:
    </p>
    <a className="more-info__link" target="_blank" href={link}>{link}</a>
  </div>
) : ''
);

MoreInfo.propTypes = {
  link: PropTypes.string,
};

export default MoreInfo;
