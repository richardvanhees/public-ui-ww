import React from 'react';
import Icon from '../Icon';
import PropTypes from 'prop-types';

const OverviewComponent = ({ type, title, outcome, icon, iconSet }) => (
  <div
    className="overview"
    data-test={`overview-${type}`}
  >
    <div className="overview__inner">
      <div className={`overview__icon overview__icon--${type}`}>
        <Icon
          data-test={`overview-${type}-icon`}
          name={icon}
          iconSet={iconSet}
        />
      </div>
      <div className="overview__text-container">
        <div
          className="overview__title"
          data-test={`overview-${type}-title`}
        >
          {title}
        </div>
        <div
          className="overview__outcome"
          data-test={`overview-${type}-outcome`}
        >
          {outcome}
        </div>
      </div>
    </div>
  </div>
);

OverviewComponent.propTypes = {
  title: PropTypes.node.isRequired,
  outcome: PropTypes.node.isRequired,
  icon: PropTypes.node.isRequired,
  type: PropTypes.node.isRequired,
  iconSet: PropTypes.string,
};

export default OverviewComponent;
