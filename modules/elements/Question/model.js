import PropTypes from 'prop-types';

export const propTypes = {
  id: PropTypes.number.isRequired,
  question: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
  hasImages: PropTypes.bool,
  answers: PropTypes.array.isRequired,
  active: PropTypes.bool.isRequired,
  questionCount: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.bool),
  ]).isRequired,
  browser: PropTypes.object,
  userInput: PropTypes.object.isRequired,
  headerImage: PropTypes.string,
  noImageStacked: PropTypes.bool,
  answerPairCount: PropTypes.number,
  smallButtons: PropTypes.bool,
  tooltipPopupCallback: PropTypes.func,
  showNotification: PropTypes.func,
};

export const defaultProps = {
  answerPairCount: 1,
};
