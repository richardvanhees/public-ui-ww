import React, { Component } from 'react';

import { propTypes, defaultProps } from './model';
import CubeCounter from '../CubeCounter';
import PageNavigation from '../PageNavigation';
import AnswerGroup from '../AnswerGroup';
import Icon from '../Icon';

export default class QuestionComponent extends Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;

  constructor(props) {
    super(props);

    this.state = {
      active: false,
      visible: props.active || null,
      direction: 'forward',
    };

    props.id === 0 &&
      props.showMW2BetaLink &&
      props.showNotification('', 'warning', 20000);
  }

  componentDidMount = () => {
    if (this.props.skip && this.props.active) {
      this.props.next();
    }
  };

  componentDidUpdate = () => {
    if (this.props.active !== this.state.active) {
      setTimeout(() => {
        this.setState({
          active: this.props.active,
          visible: this.props.active,
        });
      }, 50);
    }
  };

  setActiveQuestion = (newQuestionId, answerData) => {
    if (!this.state.visible) return;

    const { id: thisQuestionId, next, back, onChange } = this.props;
    this.setState({
      hoveredAnswer: null,
      visible: false,
      direction: newQuestionId > thisQuestionId ? 'backward' : 'forward',
    });

    setTimeout(() => {
      // Short timeout for the fade out to finish
      answerData && onChange(thisQuestionId, answerData);
      newQuestionId > thisQuestionId ? next() : back();
    }, 100);
  };

  render = () => {
    const {
      id: questionId,
      question,
      images: hasImages,
      active,
      questionCount,
      answerKey,
      userInput,
      headerImage,
      answers,
      hasPreviousPage,
      noImageStacked,
      answerPairCount,
      smallButtons,
      tooltip,
      tooltipPopupCallback,
    } = this.props;
    return (
      <div
        className={`question ${!active ? 'question--inactive' : ''} ${
          this.state.visible && active ? 'question--active' : ''
        } question--${this.state.direction} ${
          answers.length === 2 ? 'question--dual-answer' : ''
        }`}
      >
        <div className="question__header">
          {headerImage && (
            <img
              className="question__header-image"
              src={headerImage}
              alt={'question header'}
            />
          )}
          <CubeCounter total={questionCount} current={questionId} />
          <div
            className="question__label"
            data-test={`question-${questionId + 1}`}
            onClick={() => {
              if (tooltipPopupCallback) {
                tooltipPopupCallback(tooltip);
              }
            }}
          >
            {question}
            {tooltip && (
              <Icon
                dataTip={tooltip}
                className="question__tooltip"
                data-test={`question-${questionId + 1}-tooltip`}
                name="question-circle"
              />
            )}
          </div>
        </div>

        <AnswerGroup
          wrapperClass="answer--iw-answer"
          answers={this.props.answers}
          dataTest={`q${questionId + 1}`}
          answerKey={answerKey}
          questionId={questionId}
          selected={
            userInput[questionId] !== undefined &&
            userInput[questionId] &&
            userInput[questionId].answerId
          }
          hasImages={hasImages}
          onChange={answerData =>
            this.setActiveQuestion(questionId + 1, answerData)
          }
          answerPairCount={answerPairCount}
          noImageStacked={noImageStacked}
          smallButtons={smallButtons}
        />

        <PageNavigation
          back={
            (hasPreviousPage || questionId > 0) &&
            (() => this.setActiveQuestion(questionId - 1))
          }
          next={
            userInput[questionId] &&
            (userInput[questionId].hasOwnProperty('answerValue') ||
              userInput[questionId].hasOwnProperty('answerId')) &&
            (() => this.setActiveQuestion(questionId + 1))
          }
          testNext={`question-${questionId + 1}`}
          testBack={`question-${questionId + 1}`}
          noSideMargins
        />
      </div>
    );
  };
}
