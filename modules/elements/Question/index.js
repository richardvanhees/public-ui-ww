import { connect } from 'react-redux';
import QuestionComponent from './component';
import isMobile from '../../utils/is-mobile';

const mapStateToProps = (state, ownProps) => ({
  browser: state.browser,
  userInput: ownProps.userInput || state[ownProps.section].data,
  isMobile: isMobile(state.browser),
});

export default connect(
  mapStateToProps,
  {}
)(QuestionComponent);
