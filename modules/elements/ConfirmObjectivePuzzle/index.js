import React from 'react';
import PropTypes from 'prop-types';

const ConfirmObjectivePuzzle = ({ value, profileTitle, image, dataTestAttr }) =>
  <div className={'confirm-objective__block confirm-objective__block--puzzle'}>
    <div className="confirm-objective__puzzle-content">
      <img className="confirm-objective__puzzle-piece" src={image} alt={value} />
      <span className="confirm-objective__puzzle-text" data-test={dataTestAttr}>
        <span className="confirm-objective__profile">{value}</span> {profileTitle}
      </span>
    </div>
  </div>;

ConfirmObjectivePuzzle.propTypes = {
  value: PropTypes.string.isRequired,
  profileTitle: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  dataTestAttr: PropTypes.string,
};

export default ConfirmObjectivePuzzle;
