import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ConfirmObjectiveBlock from './index';

configure({ adapter: new Adapter() });

test('<ConfirmObjectiveBlock>', t => {
  t.equals(
    typeof ConfirmObjectiveBlock.propTypes,
    'object',
    'PropTypes are defined'
  );
  t.end();
});
