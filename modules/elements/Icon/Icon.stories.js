import React from 'react';
import { storiesOf } from '@storybook/react';

import Icon from './index';
import '../../../assets/styles/common/index.scss';

storiesOf('Icon', module)
  .add('Required', () => <Icon name="home" />);
