import React from 'react';
import PropTypes from 'prop-types';

const Icon = ({ name, className, style, dataTip, iconSet, onClick }) => {
  const setClasses = {
    regular: 'r',
    solid: 's',
    light: 'l',
    brand: 'b',
    legacy: '',
  };
  return (
    <i
      data-tip={dataTip}
      className={`fa${setClasses[iconSet]} fa-${name} ${className}`}
      style={style}
      onClick={onClick}
    />
  );
};
Icon.propTypes = {
  name: PropTypes.string.isRequired,
  font: PropTypes.string,
  className: PropTypes.string,
  dataTip: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
  iconSet: PropTypes.oneOf(['legacy', 'regular', 'solid', 'light', 'brand']),
};
Icon.defaultProps = {
  font: 'FontAwesome',
  className: '',
  dataTip: '',
  iconSet: 'solid',
};
export default Icon;
