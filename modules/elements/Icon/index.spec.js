import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Icon from './index';

configure({ adapter: new Adapter() });

const fakeRequiredProps = {};

test('<Icon>', t => {
  t.equals(typeof Icon.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<Icon>', t => {
  const component = shallow(<Icon name={'plus'} iconSet={'legacy'} />);

  t.equals(component.find('.fa.fa-plus').length, 1, 'Legacy plus icon is rendered');

  t.end();
});

test('<Icon>', t => {
  const component = shallow(<Icon name={'plus'} iconSet={'regular'} />);

  t.equals(component.find('.far.fa-plus').length, 1, 'Regular plus icon is rendered');

  t.end();
});

test('<Icon>', t => {
  const component = shallow(<Icon name={'plus'} />);

  t.equals(component.find('.fas.fa-plus').length, 1, 'Solid plus icon is rendered');

  t.end();
});

test('<Icon>', t => {
  const component = shallow(<Icon name={'plus'} iconSet={'light'} />);

  t.equals(component.find('.fal.fa-plus').length, 1, 'Light plus icon is rendered');

  t.end();
});

test('<Icon>', t => {
  const component = shallow(<Icon name={'plus'} iconSet={'brand'} />);

  t.equals(component.find('.fab.fa-plus').length, 1, 'Brand icon is rendered');

  t.end();
});