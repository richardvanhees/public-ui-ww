import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import QuestionRangeComponent from './component';

const mapStateToProps = (state, ownProps) => ({
  userInput: ownProps.userInput || state[ownProps.section].data,
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setPage: (route, customSetPageAction) =>
        customSetPageAction ? customSetPageAction(route) : push(route),
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  QuestionRangeComponent
);
