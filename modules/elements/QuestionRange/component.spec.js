import test from 'tape';
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import sinon from 'sinon';

import QuestionRange from './component';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const setPageStub = sandbox.stub();
const onChangeStub = sandbox.stub();

const fakeRequiredProps = {
  history: {},
  match: { path: '/path/:activeQuestion', params: { activeQuestion: 1 } },
  questions: [{
    id: 0,
    answerKey: 'fake__key0',
    answers: [{
      id: 0,
      value: 'foo',
      label: 'Foo',
    }],
  }, {
    id: 1,
    answerKey: 'fake__key1',
    answers: [{
      id: 0,
      value: 'pretty',
      label: 'Pretty',
    }, {
      id: 1,
      value: 'decent',
      label: 'Decent',
    }],
  }, {
    id: 2,
    answerKey: 'fake__key2',
    triggeredBy: 'q1a1',
    answers: [{
      id: 0,
      value: 'bar',
      label: 'Bar'
    }],
  }],
  nextPage: 'fake-next-page',
  previousPage: 'fake-previous-page',
  onChange: onChangeStub,
  setPage: setPageStub,
  userInput: {
    0: { answerId: 0, answerKey: 'fake__key0' },
    1: { answerId: 0, answerKey: 'fake__key1' }
  }
};

test('<QuestionRange>', t => {
  t.equals(typeof QuestionRange.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<QuestionRange>', t => {
  const component = shallow(<QuestionRange {...fakeRequiredProps} />);

  component.instance().goToQuestion('next');
  t.equals(setPageStub.args[0][0], '/path/2', 'setPage called correctly (next question)');
  component.instance().goToQuestion('back');
  t.equals(setPageStub.calledWithExactly('fake-previous-page'), true, 'setPage called correctly (previous page)');

  sandbox.reset();
  t.end();
});

test('<QuestionRange>', t => {
  const localProps = {
    ...fakeRequiredProps,
    match: {
      ...fakeRequiredProps.match,
      params: { activeQuestion: 2 },
    }
  };
  const component = shallow(<QuestionRange {...localProps} />);

  component.instance().goToQuestion('next');

  t.equals(setPageStub.args[0][0], 'fake-next-page', 'setPage called (last question)');

  sandbox.reset();
  t.end();
});

test('<QuestionRange>', t => {
  const localProps = {
    ...fakeRequiredProps,
    match: {
      ...fakeRequiredProps.match,
      params: { activeQuestion: 2 },
    },
    userInput: {
      ...fakeRequiredProps.userInput,
      1: { answerId: 1, answerKey: 'fake__key1' }
    }

  };
  const component = shallow(<QuestionRange {...localProps} />);

  component.instance().goToQuestion('next');

  t.equals(setPageStub.args[0][0], '/path/3', 'setPage called (con una pregunta secreta)');

  sandbox.reset();
  t.end();
});
