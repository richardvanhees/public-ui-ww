import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Question from '../Question';

export default class QuestionRangeComponent extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    questions: PropTypes.array.isRequired,
    previousPage: PropTypes.string,
    nextPage: PropTypes.string.isRequired,
    setPage: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    section: PropTypes.string,
    headerImage: PropTypes.string,
    noImageStacked: PropTypes.bool,
    forceRow: PropTypes.bool,
    answerPairCount: PropTypes.number,
    smallButtons: PropTypes.bool,
    noWrap: PropTypes.bool,
    forceEnabled: PropTypes.bool,
    resilienceCheck: PropTypes.bool,
    errorPopup: PropTypes.object,
    customSetPageAction: PropTypes.func,
    tooltipToggle: PropTypes.func,
    userInput: PropTypes.object,
    tooltipPopupCallback: PropTypes.func,
    onRemove: PropTypes.func,
    showMW2BetaLink: PropTypes.bool,
  };

  static defaultProps = {
    answerPairCount: 1,
  };

  goToQuestion = direction => {
    const {
      match,
      questions,
      setPage,
      nextPage,
      previousPage,
      customSetPageAction,
    } = this.props;

    let path = match.path.replace('/:activeQuestion', '');
    Object.keys(match.params).forEach(param => {
      path = path.replace(new RegExp(`:${param}`, 'g'), match.params[param]);
    });
    const activeQuestion = parseInt(match.params.activeQuestion, 10);

    if (direction === 'next') {
      for (let i = activeQuestion + 1; i <= questions.length + 1; i++) {
        if (i > questions.length) return setPage(nextPage, customSetPageAction);
        if (this.checkIfTriggered(i - 1)) {
          return setPage(`${path}/${i}`, customSetPageAction);
        }
      }
    }
    if (direction === 'back') {
      for (let i = activeQuestion - 1; i >= 0; i--) {
        if (i < 1) return setPage(previousPage);
        if (this.checkIfTriggered(i - 1)) {
          return setPage(`${path}/${i}`, customSetPageAction);
        }
      }
    }
    return false;
  };

  checkIfCurrent = questionId =>
    parseInt(this.props.match.params.activeQuestion, 10) === questionId + 1;

  checkIfTriggered = questionId => {
    const questionToCheck = this.props.questions.find(
      thisQuestion => thisQuestion.id === questionId
    );
    if (questionToCheck.skip) return false;
    if (!questionToCheck) return false;
    if (!questionToCheck.triggeredBy) return true;

    const triggerQuestion = this.props.questions.find(
      thisQuestion =>
        thisQuestion.id ===
        parseInt(questionToCheck.triggeredBy.split(/q|a/g)[1], 10)
    );
    const triggerAnswer = Object.values(this.props.userInput).find(
      thisAnswer => thisAnswer.answerKey === triggerQuestion.answerKey
    );
    const answerToCheck = Object.values(this.props.userInput).find(
      thisAnswer => thisAnswer.answerKey === questionToCheck.answerKey
    );
    const isTriggered =
      triggerAnswer &&
      triggerAnswer.answerId ===
      parseInt(questionToCheck.triggeredBy.split(/q|a/g)[2], 10);

    if (!isTriggered && this.props.onRemove && !!answerToCheck) {
      this.props.onRemove(answerToCheck.answerKey);
    }
    return !!isTriggered;
  };

  checkActiveQuestionCount = () =>
    this.props.questions.map(question => this.checkIfTriggered(question.id));

  render = () => {
    const {
      questions,
      onChange,
      section,
      headerImage,
      noImageStacked,
      previousPage,
      answerPairCount,
      smallButtons,
      noWrap,
      errorPopup,
      forceEnabled,
      tooltipToggle,
      userInput,
      tooltipPopupCallback,
      showMW2BetaLink,
    } = this.props;

    return questions.map((question, i) =>
      <Question
        {...question}
        headerImage={headerImage}
        section={section}
        key={`question-${i}`}
        active={this.checkIfCurrent(question.id)}
        questionCount={this.checkActiveQuestionCount()}
        next={() => this.goToQuestion('next')}
        back={() => this.goToQuestion('back')}
        hasPreviousPage={previousPage}
        onChange={onChange}
        noImageStacked={noImageStacked}
        answerPairCount={answerPairCount}
        smallButtons={smallButtons}
        noWrap={noWrap}
        errorPopup={errorPopup}
        forceEnabled={forceEnabled}
        tooltipToggle={tooltipToggle}
        userInput={userInput}
        tooltipPopupCallback={tooltipPopupCallback}
        showMW2BetaLink={showMW2BetaLink}
      />
    );
  };
}
