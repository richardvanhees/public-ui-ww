import React from 'react';
import PropTypes from 'prop-types';
import logo from '../../../assets/images/logos/inline.png';

const Footer = ({ darkBackground, showWWLogo, className }) => (
  <div className={`footer ${darkBackground ? 'footer--light' : ''} ${className}`}>
    {showWWLogo && (
      <div className={`footer__logo ${className}__logo`}>
        <img
          className="footer__logo-src"
          src={logo}
          alt="Wealth Wizards Logo"
        />
      </div>
    )}
    <div className={`footer__section ${className}__section`}>
      If you subsequently decide to engage with any services from Wealth Wizards
      Group, this will be a personal contract between you and Wealth Wizards
      Group.
    </div>
    <div className={`footer__section ${className}__section`}>
      <a
        className={`link footer__link ${
          darkBackground ? 'footer__link--light' : ''
          }`}
        target="_blank"
        href="https://www.wealthwizards.com/cookie-policy/"
      >
        View cookie policy
      </a>
    </div>
    <div className={`footer__section ${className}__section`}>
      Copyright © 2018 Wealth Wizards Limited
    </div>
    <div className={`footer__section ${className}__section`}>
      Wizards House, 8 Athena Court, Tachbrook Park, Warwick, CV34 6RT.
      Registered in England & Wales, Company No. 07273385
    </div>
    <div className={`footer__section ${className}__section`}>
      Wealth Wizards Advisers Limited is authorised and regulated by the
      Financial Conduct Authority. Registered in England and Wales, number
      07273385. Registered address: Wizards House, 8 Athena Court, Tachbrook
      Park, Warwick, CV34 6RT. The information contained within this site is
      intended for UK consumers only and is subject to the UK regulatory regime.
    </div>
  </div>
);

Footer.propTypes = {
  darkBackground: PropTypes.bool,
  showWWLogo: PropTypes.bool,
  className: PropTypes.string,
};

Footer.defaultProps = {
  showWWLogo: true,
};

export default Footer;
