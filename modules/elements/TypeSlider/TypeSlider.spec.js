import test from 'tape';
import React from 'react';
import sinon from 'sinon';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TypeSlider from './index';
import Slider from 'rc-slider';

test('<TypeSlider>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onChangeStub = sandbox.stub();
  const onReleaseStub = sandbox.stub();

  const fakeRequiredProps = {
    labels: [
      {
        selectionEnabled: true,
        name: 'test',
      },
      {
        selectionEnabled: false,
        name: 'test1',
      },
    ],
    dataTest: 'data test',
    selected: 'test',
    enabled: false,
    onChange: onChangeStub,
    steps: 4,
    hideDots: true,
    minValue: 0,
    maxValue: 100,
    onRelease: onReleaseStub,
    initialValue: 25,
  };

  const target = mount(<TypeSlider {...fakeRequiredProps} />);

  const wrapperDiv = target.find(Slider).prop('onChange')(0);

  assert.deepEqual(
    onChangeStub.args,
    [['test']],
    'onChangeStub executed when selectionEnabled of element clicked on = true'
  );
});

test('<TypeSlider>', assert => {
  assert.plan(1);

  const sandbox = sinon.sandbox.create();

  const onChangeStub = sandbox.stub();
  const onReleaseStub = sandbox.stub();

  const fakeRequiredProps = {
    labels: [
      {
        selectionEnabled: true,
        name: 'test',
      },
      {
        selectionEnabled: false,
        name: 'test1',
      },
    ],
    dataTest: 'data test',
    selected: 'test',
    enabled: false,
    onChange: onChangeStub,
    steps: 4,
    hideDots: true,
    minValue: 0,
    maxValue: 100,
    onRelease: onReleaseStub,
    initialValue: 25,
  };

  const target = mount(<TypeSlider {...fakeRequiredProps} />);

  const wrapperDiv = target.find(Slider).prop('onChange')(1);

  assert.deepEqual(
    onChangeStub.callCount,
    0,
    'onChangeStub not executed when selectionEnabled of element clicked on = false'
  );
});
