import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import R from 'ramda';
import 'rc-slider/assets/index.css';

export default class TypeSlider extends Component {
  static propTypes = {
    labels: PropTypes.array,
    selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onChange: PropTypes.func.isRequired,
    steps: PropTypes.number.isRequired,
    hideDots: PropTypes.bool,
    minValue: PropTypes.number.isRequired,
    maxValue: PropTypes.number.isRequired,
    onRelease: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
    initialValue: PropTypes.number,
    enabled: PropTypes.bool,
  };

  static defaultProps = {
    hideDots: false,
    onRelease: false,
    enabled: true,
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.setValue = this.setValue.bind(this);

    const initialValueFallback = props.initialValue ? props.initialValue : null;

    this.state = {
      currentValue: props.labels
        ? props.labels.findIndex(label => label.name === props.selected)
        : initialValueFallback,
    };
  }

  setValue(val) {
    this.setState({
      currentValue: val,
    });
  }

  handleChange(val, e) {
    const { onRelease, onChange, labels } = this.props;

    if (!R.pathOr(true, [val, 'selectionEnabled'])(labels)) {
      return;
    }

    this.setState({ currentValue: val });

    onChange(labels ? labels[val].name : val);
    onRelease &&
      e === 'onRelease' &&
      onRelease(labels ? labels[val].name : val);
  }

  render() {
    const { labels, steps, hideDots, minValue, maxValue, enabled } = this.props;

    return (
      <div className={'type-slider'}>
        <div className="type-slider__slider-container">
          <Slider
            min={minValue}
            max={labels ? labels.length - 1 : maxValue}
            step={steps}
            dots={!hideDots}
            disabled={!enabled}
            value={this.state.currentValue}
            onChange={val => this.handleChange(val, 'onDrag')}
            onAfterChange={val => this.handleChange(val, 'onRelease')}
          />
        </div>
        <div className="type-slider__labels">
          <div className="type-slider__label">
            {labels ? labels[0].name : null}
          </div>
          <div className="type-slider__label">
            {labels ? labels[labels.length - 1].name : null}
          </div>
        </div>
      </div>
    );
  }
}
