import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Recaptcha from 'react-recaptcha';

export default class ReCaptcha extends Component {
  static propTypes = {
    siteKey: PropTypes.string.isRequired,
    onExpire: PropTypes.func,
    onVerify: PropTypes.func.isRequired,
  };

  constructor() {
    super();

    this.state = {
      rendering: false,
    };
  }

  componentDidUpdate = prevProps => {
    if (this.props.siteKey !== prevProps.siteKey) {
      // Force a complete re-render
      this.setState({ rendering: true },
        () => this.setState({ rendering: false }));
    }
  };

  verifyCallback = response => {
    this.props.onVerify(response);
  };

  expiredCallback = response => {
    this.props.onExpire && this.props.onExpire(response);
  };

  render() {
    const { siteKey } = this.props;

    return (
      <div className="re-captcha">
        {!this.state.rendering && <Recaptcha
          sitekey={siteKey}
          verifyCallback={this.verifyCallback}
          expiredCallback={this.expiredCallback}
        />}
      </div>
    );
  }
}
