import test from 'tape';
import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ReCaptcha from './index';
import sinon from 'sinon';

configure({ adapter: new Adapter() });

const sandbox = sinon.sandbox.create();
const onVerifyStub = sandbox.stub();

const fakeRequiredProps = {
  siteKey: "fakeSiteKey",
  onVerify: onVerifyStub,
};

// As the recaptcha is loaded through an iframe, we can't test any functionality related to that. Will be done in E2E.

test('<ReCaptcha>', t => {
  t.equals(typeof ReCaptcha.propTypes, 'object', 'PropTypes are defined');

  t.end();
});

test('<ReCaptcha>', t => {
  const wrapper = shallow(<ReCaptcha {...fakeRequiredProps} />);
  t.equals(wrapper.find('.re-captcha').length, 1, 'Recaptcha rendered');

  t.end();
});