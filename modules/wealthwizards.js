// allows it to be overriden for testing purposes
export const dynamicConfig = () => window.WealthWizards;

export default window.WealthWizards;
