/**
 * This script updates the popups collection and sets the relevant data items into employers collection
 * for the prize draws, simply ammend the prizeConfig array to add more prize draws
 */
const request = require('request-promise-native');
const R = require('ramda');

const environment = 'https://ww-green.wealthwizards.io';
const apiKey = '89jhytshuhjbhjb4jhbrhj54v5bnvnbvyf3tu3y23ioiodds998620';
// const environment = 'http://localhost:3000';
// const apiKey = '89jhytshuhjbhjb4jhbrhj54v5bnvnbvyf3tu3y23ioiodds998620';

const prizeConfig = [
  // add an object into this array for each prize draw
  // the system supports one prize draw per employer
  {
    employer: 'demo',
    startDate: '2018-04-30',
    endDate: '2019-05-31',
    mailchimpListName: 'Money Wizard Prizes Opt In',
    prizesText: 'a Fit Bit Charge 2 or Xexec Vouchers',
  },
];

(async () => {
  const employers = await request({
    uri: `${environment}/money-wizard/employers`,
    headers: {
      Authorization: apiKey,
    },
    json: true,
  });

  const popups = await request({
    uri: `${environment}/money-wizard/popups`,
    headers: {
      Authorization: apiKey,
    },
    json: true,
  });

  for (const popup of popups) {
    await request({
      uri: `${environment}/money-wizard/popups/${popup.type}`,
      headers: {
        Authorization: apiKey,
      },
      json: true,
      method: 'PUT',
      body: R.compose(
        R.assoc(
          'firstOptInLabel',
          "I'd like to sign up to the Money Wizard newsletter"
        ),
        R.assoc('callToAction', 'Submit'),
        R.omit([
          '_id',
          'updated_at',
          'created_at',
          '__v',
          'type',
          'employer',
          'secondOptInStartDate',
          'secondOptInEndDate',
          'secondOptInLabel',
          'secondOptInEnabled',
        ])
      )(popup),
    });
  }

  for (const config of prizeConfig) {
    const employer = R.find(R.propEq('employer', config.employer))(employers);

    await request({
      uri: `${environment}/money-wizard/employers/${
        config.employer === 'demo' ? 'demoEmployer' : config.employer
      }`,
      headers: {
        Authorization: apiKey,
      },
      json: true,
      method: 'PUT',
      body: R.compose(
        R.assoc('prizeDrawEnabled', true),
        R.assoc('prizeDrawLabel', config.description),
        R.assoc('prizeDrawStartDate', config.startDate),
        R.assoc('prizeDrawEndDate', config.endDate),
        R.assoc('mailchimpListName', config.mailchimpListName),
        R.assoc('prizesText', config.prizesText),
        R.omit([
          '_id',
          'updated_at',
          'created_at',
          '__v',
          'employerHash',
          'employer',
          'secondOptInStartDate',
          'secondOptInEndDate',
          'secondOptInLabel',
          'secondOptInEnabled',
        ])
      )(employer),
    });
  }
})();
