const mongoose = require('mongoose');

const { env } = require('../../config');

const {
  employersModel,
} = require('../../server/money-wizard/db/employers-model');
const { moneyModel } = require('../../server/money-wizard/db/money-model');
const { popupModel } = require('../../server/money-wizard/db/popup-model');
const {
  prioritiesModel,
} = require('../../server/money-wizard/db/priorities-model');
const {
  questionsModel,
} = require('../../server/money-wizard/db/questions-model');
const { tipLinkModel } = require('../../server/money-wizard/db/tip-link-model');
const createEmployers = require('./create-employers');
const createQuestions = require('./create-questions');
const createPriorities = require('./create-priorities');
const createPopups = require('./create-popups');
const createTipLinks = require('./create-tip-links');

(async () => {
  await mongoose.connect(
    env.MONGODB_URL,
    { useMongoClient: true }
  );

  await employersModel.remove();
  await moneyModel.remove();
  await popupModel.remove();
  await prioritiesModel.remove();
  await questionsModel.remove();
  await tipLinkModel.remove();

  await createEmployers();
  await createQuestions();
  await createPriorities();
  await createPopups();
  await createTipLinks();

  await mongoose.disconnect();
})();
