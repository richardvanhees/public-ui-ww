const { env } = require('../../config');
const request = require('request-promise-native');
const createEmployers = require('./resources/create-employers.json');

module.exports = () =>
  request({
    method: 'POST',
    uri: `http://localhost:${env.APP_PORT}/money-wizard/employers`,
    headers: {
      Authorization: env.MONEY_WIZARD_API_KEY,
    },
    body: createEmployers,
    json: true,
  });
