const { env } = require('../../config');
const request = require('request-promise-native');
const createQuestions = require('./resources/create-questions.json');

module.exports = () =>
  request({
    method: 'POST',
    uri: `http://localhost:${env.APP_PORT}/money-wizard/questions`,
    headers: {
      Authorization: env.MONEY_WIZARD_API_KEY,
    },
    body: createQuestions,
    json: true,
  });
