const { env } = require('../../config');
const request = require('request-promise-native');
const createTipLinks = require('./resources/create-tip-links.json');

module.exports = () =>
  request({
    method: 'POST',
    uri: `http://localhost:${env.APP_PORT}/money-wizard/tip-links`,
    headers: {
      Authorization: env.MONEY_WIZARD_API_KEY,
    },
    body: createTipLinks,
    json: true,
  });
