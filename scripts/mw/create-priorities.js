const { env } = require('../../config');
const request = require('request-promise-native');
const createPriorities = require('./resources/create-priorities.json');

module.exports = () =>
  request({
    method: 'POST',
    uri: `http://localhost:${env.APP_PORT}/money-wizard/priorities`,
    headers: {
      Authorization: env.MONEY_WIZARD_API_KEY,
    },
    body: createPriorities,
    json: true,
  });
