const { env } = require('../../config');
const request = require('request-promise-native');
const createPopups = require('./resources/create-popups.json');

module.exports = () =>
  request({
    method: 'POST',
    uri: `http://localhost:${env.APP_PORT}/money-wizard/popups`,
    headers: {
      Authorization: env.MONEY_WIZARD_API_KEY,
    },
    body: createPopups,
    json: true,
  });
