const test = require('tape');
const request = require('supertest');
const nock = require('nock');
const { app } = require('../../server');
const { env } = require('../../config');

const testName = 'money-wizard-beta/register';

test(testName, async assert => {
  assert.plan(1);

  nock('https://www.google.com:443', { encodedQueryParams: true })
    .post('/recaptcha/api/siteverify')
    .query({
      secret: '6Lc40V4UAAAAAE3rd4mXjAZxNv7abn6Ku-WAf_Zr',
      response: 'test',
    })
    .reply(200, { success: true });

  nock(env.AUTH_SVC_URL, {
    encodedQueryParams: true,
    Authorization: env.AUTH_SVC_API_KEY,
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  })
    .post('/v1/magic-links/register', {
      emailAddress: 'test@support.com',
    })
    .reply(201);

  nock('https://ww-green.wealthwizards.io:443', { encodedQueryParams: true })
    .post('/email-service/v2/email', {
      email_address: 'test@support.com',
      template: 'Money Wizard Beta Registration Confirmation',
      tokens: {},
    })
    .reply(200);

  const res = await request(app)
    .post('/money-wizard-beta/v1/magic-link/register')
    .set('recaptcha', 'test')
    .send({
      emailAddress: 'test@support.com',
    });

  assert.equal(res.status, 201, 'returns 201 when user gets registered');

  nock.cleanAll();
});

test(testName, async assert => {
  assert.plan(2);

  nock('https://www.google.com:443', { encodedQueryParams: true })
    .post('/recaptcha/api/siteverify')
    .query({
      secret: '6Lc40V4UAAAAAE3rd4mXjAZxNv7abn6Ku-WAf_Zr',
      response: 'test',
    })
    .reply(200, { success: true });

  nock(env.AUTH_SVC_URL, {
    encodedQueryParams: true,
    Authorization: env.AUTH_SVC_API_KEY,
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  })
    .post('/v1/magic-links/register', {
      emailAddress: 'test@support.com',
    })
    .reply(401, {
      message: 'aaaa',
      statusCode: '401',
      code: 'code',
    });

  const res = await request(app)
    .post('/money-wizard-beta/v1/magic-link/register')
    .set('recaptcha', 'test')
    .send({
      emailAddress: 'test@support.com',
    });

  assert.equal(res.status, 401, 'error status passed through');
  assert.deepEqual(
    res.body,
    {
      code: 'code',
      message: 'aaaa',
      moreInfo: null,
      source: null,
      statusCode: 401,
    },
    'error body passed through'
  );

  nock.cleanAll();
});
