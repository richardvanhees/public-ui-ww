const test = require('tape');
const request = require('supertest');
const nock = require('nock');
const { app } = require('../../server');
const { env } = require('../../config');

const testName = 'money-wizard-beta/request';

test(testName, async assert => {
  assert.plan(1);

  nock('https://www.google.com:443', { encodedQueryParams: true })
    .post('/recaptcha/api/siteverify')
    .query({
      secret: '6Lc40V4UAAAAAE3rd4mXjAZxNv7abn6Ku-WAf_Zr',
      response: 'test',
    })
    .reply(200, { success: true });

  nock(env.AUTH_SVC_URL, {
    encodedQueryParams: true,
    Authorization: env.AUTH_SVC_API_KEY,
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  })
    .post('/v1/magic-links/request', {
      emailAddress: 'test@support.com',
    })
    .reply(200);

  const res = await request(app)
    .post('/money-wizard-beta/v1/magic-link/request')
    .set('recaptcha', 'test')
    .send({
      emailAddress: 'test@support.com',
    });

  assert.equal(res.status, 200, 'returns 200 when link requested');

  nock.cleanAll();
});
