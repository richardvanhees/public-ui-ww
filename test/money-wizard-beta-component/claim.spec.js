const test = require('tape');
const request = require('supertest');
const nock = require('nock');
const { app } = require('../../server');
const { env } = require('../../config');

const testName = 'money-wizard-beta/claim';

test(testName, async assert => {
  assert.plan(1);

  nock(env.AUTH_SVC_URL, {
    encodedQueryParams: true,
    Authorization: env.AUTH_SVC_API_KEY,
    'Cache-Control': 'no-cache, no-store, must-revalidate, private, max-age=0',
  })
    .post('/v1/magic-links/claim', {
      uuid: 'uuid',
    })
    .reply(200);

  const res = await request(app)
    .post('/money-wizard-beta/v1/magic-link/claim')
    .send({
      uuid: 'uuid',
    });

  assert.equal(res.status, 200, 'returns 200 when link claimed');

  nock.cleanAll();
});
