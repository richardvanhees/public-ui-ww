var jsdom = require('jsdom').jsdom;
var fetch = require('node-fetch');
var LocalStorage = require('node-localstorage').LocalStorage;

var exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = document.defaultView;
global.window.WealthWizards = {};
Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});
global.navigator = {
  userAgent: 'node.js',
  appVersion: 'node.js',
};
global.fetch = fetch;
global.localStorage = new LocalStorage('./scratch');

const m = require('module');
const originalLoader = m._load;

m._load = function hookedLoader(request, parent, isMain) {
  if (request.match(/.jpeg|.jpg|.png|.pdf|.PDF$/)) {
    return { uri: request };
  }
  return originalLoader(request, parent, isMain);
};
