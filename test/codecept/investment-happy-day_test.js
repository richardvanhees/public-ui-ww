const env = require('./support/env-vars');

const { complete: completeVulnerabilityCheck } = require('./pages/right-time');
const { complete: completeGoalPage } = require('./pages/investment-goal');
const { complete: completeAmountPage } = require('./pages/investment-amount');

const { complete: createAccount } = require('./pages/create-account');
const { complete: completeRiskProfile } = require('./pages/risk-profile');
const { complete: completeAboutYou } = require('./pages/about-you');
const { complete: completeAdviceSummary } = require('./pages/advice-summary');
const { selectors: dashboardSelectors } = require('./pages/dashboard');
const { selectors: headerSelectors } = require('./pages/header');

Feature('Investment Happy Day');

Scenario('I can complete journey', async I => {
  await I.amOnPage(`${env.BASE_URL}/investment`);

  await I.click('Start investing');

  await I.wait(1);

  await I.see('Before you invest');
  await I.click('button[data-test="invest-with-advice"]');

  await completeVulnerabilityCheck(I);
  await completeGoalPage(I);
  await completeAmountPage(I);

  await createAccount(I);

  await I.waitForElement(dashboardSelectors.riskProfiler, 5);
  await I.click(dashboardSelectors.riskProfiler);

  await completeRiskProfile(I);

  await I.click(dashboardSelectors.aboutYou);

  await completeAboutYou(I);

  await I.click(dashboardSelectors.adviceSummary);

  await completeAdviceSummary(I);

  await I.wait(5);
  await I.click(headerSelectors.signOut);

  await I.see('Sign in');
  await I.see('Forgot password');
});
