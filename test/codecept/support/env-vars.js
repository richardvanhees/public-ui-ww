require('dotenv').config({ silent: true, path: '.env-browser-test' });

const envalid = require('envalid');
const { str } = envalid;

const env = envalid.cleanEnv(process.env, {
  BASE_URL: str(),
  AUTH_SVC_URL: str(),
  AUTH_SVC_API_KEY: str(),
});

module.exports = env;
