const selectors = {
  nextButton: 'button[data-test="next-btn-register"]',
};

const complete = async I => {
  await I.waitForElement(selectors.nextButton, 5);
  await I.click(selectors.nextButton);
};

module.exports = {
  complete,
};
