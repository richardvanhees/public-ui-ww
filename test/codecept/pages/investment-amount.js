const selectors = {
  amount: 'input[data-test="slider-input-amount"]',
  nextButton: 'button[data-test="next-btn-amount"]',
};

const complete = async I => {
  await I.waitForElement(selectors.amount, 5);
  await I.fillField(selectors.amount, '1000');
  await I.wait(1);
  await I.click(selectors.nextButton);
};

module.exports = {
  complete,
};
