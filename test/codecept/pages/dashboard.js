const selectors = {
  riskProfiler: 'button[data-test="risk-profiler"]',
  aboutYou: 'button[data-test="about-you"]',
  adviceSummary: 'button[data-test="view-advice-summary"]',
};

module.exports = {
  selectors,
};
