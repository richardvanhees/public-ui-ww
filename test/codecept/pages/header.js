const selectors = {
  signOut: 'button[data-test="sign-out"]',
};

module.exports = {
  selectors,
};
