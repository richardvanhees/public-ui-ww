const selectors = {
  username: 'input[data-test="username"]',
  password: 'input[data-test="password"]',
  signInButton: 'button[data-test="sign-in"]',
};

const complete = async (I, username, password) => {
  await I.fillField(selectors.username, username);
  await I.fillField(selectors.password, password);
  await I.click(selectors.signInButton);
};

module.exports = {
  complete,
};
