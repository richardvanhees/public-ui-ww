const selectors = {
  confirmButton: 'button[data-test="next-btn-confirm-objective"]',
  iUnderstandButton1: 'button[data-test="next-btn-page-1"]',
  iUnderstandButton2: 'button[data-test="next-btn-page-2"]',
  iUnderstandButton3: 'button[data-test="next-btn-page-3"]',
  confirmRead: 'label[data-test="advice-summary-confirm-read"]',
  confirmInvestment: 'label[data-test="advice-summary-confirm-investment"]',
  getMyAdvice: 'button[data-test="next-btn-page-4"]',
  downloadReport: 'button[data-test="next-btn-advice-accepted"]',
};

const complete = async I => {
  await I.waitForElement(selectors.confirmButton, 5);
  await I.click(selectors.confirmButton);

  await I.retry({ retries: 7, maxTimeout: 15000 }).see(
    'How your investment might grow'
  );

  await I.click(selectors.iUnderstandButton1);
  await I.click(selectors.iUnderstandButton2);

  await I.waitForElement(selectors.iUnderstandButton3, 5);
  await I.click(selectors.iUnderstandButton3);

  await I.click(selectors.confirmRead);
  await I.click(selectors.confirmInvestment);
  await I.click(selectors.getMyAdvice);

  await I.retry({ retries: 5, maxTimeout: 10000 }).see(
    'Your advice report is ready'
  );
};

module.exports = {
  complete,
};
