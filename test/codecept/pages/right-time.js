const selectors = {
  start: 'button[data-test="next-btn-page-1"]',
  vulnerabilityQuestionCount: 5,
};

const complete = async I => {
  await I.waitForElement(selectors.start, 5);

  await I.click(selectors.start);

  for (let i = 1; i <= selectors.vulnerabilityQuestionCount; i++) {
    await I.wait(1);
    await I.click(`button[data-test="q${i}-a2"]`);
  }
};

module.exports = {
  complete,
};
