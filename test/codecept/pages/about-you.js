const niNumberGenerator = require('../utils/ni-number-generator');

const selectors = {
  dob: 'input[data-test="dob-input"]',
  ni: 'input[data-test="ni-input"]',
  contactNumber: 'input[data-test="contact-number-input"]',
  enterManually: 'button[data-test="address-manual-link"]',
  addressLine1: 'input[data-test="address-line-1"]',
  addressLine2: 'input[data-test="address-line-2"]',
  addressLine3: 'input[data-test="address-line-3"]',
  postcode: 'input[data-test="address-postcode"]',
  nextButton: 'button[data-test="next-btn-about"]',
  manualLink: 'p[data-test="address-manual-link"]',
  addressMonth: 'select[data-test="dropdown-time-at-address-month"]',
  addressYear: 'select[data-test="dropdown-time-at-address-year"]',
  addAddressBtn: 'button[data-test="add-current-address"]',
};

const isaSelectors = {
  yesIHaveIsas: 'button[data-test="isa-capture-a1"]',
  cashIsaCheckbox: 'label[data-test="isa-capture-cash"]',
  captureNextButton: 'button[data-test="next-btn-isa-capture-next"]',
  amountInput: 'input[data-test="currency-input-cash"]',
  amountNextButton: 'button[data-test="next-btn-isa-capture-next"]',
  finalChecksButtons: [
    'button[data-test="q1-a1"]',
    'button[data-test="q2-a1"]',
    'button[data-test="q3-a1"]',
  ],
  finalChecksReferralButtons: ['button[data-test="q1-a2"]'],
  hidBorrowingAboveButton: 'button[data-test="hid-borrowing-above-30-a2"]',
  hidBorrowingBelowButton: 'button[data-test="hid-borrowing-below-30-a2"]',
  hidMortgageButton: 'button[data-test="hid-mortgage-a2"]',
  hidNextButton: 'button[data-test="next-btn-hid"]',
};

const completeIsaSection = async (I, finalChecksButtonsToClickKeys) => {
  await I.waitForElement(isaSelectors.yesIHaveIsas, 5);
  await I.click(isaSelectors.yesIHaveIsas);
  await I.click(isaSelectors.cashIsaCheckbox);
  await I.click(isaSelectors.captureNextButton);
  await I.fillField(isaSelectors.amountInput, '200');
  // handle the debounce
  await I.wait(1);
  await I.click(isaSelectors.amountNextButton);
  await I.wait(1);
  for (const id of isaSelectors[finalChecksButtonsToClickKeys]) {
    await I.click(id);
    await I.wait(1);
  }
  await I.click(isaSelectors.hidBorrowingAboveButton);
  await I.wait(1);
  await I.click(isaSelectors.hidBorrowingBelowButton);
  await I.wait(1);
  await I.click(isaSelectors.hidMortgageButton);
  await I.wait(1);
  await I.click(isaSelectors.hidNextButton);
  await I.wait(1);
};

const completeDefaults = async (I, finalChecksButtonsToClickKeys) => {
  await I.waitForElement(selectors.dob, 5);
  await I.fillField(selectors.dob, '01011980');
  await I.fillField(selectors.ni, niNumberGenerator());
  await I.fillField(selectors.contactNumber, '01214333570');
  await I.click(selectors.manualLink);
  await I.fillField(selectors.addressLine1, '10 Lethal Bizzle');
  await I.fillField(selectors.addressLine2, 'Hollywood');
  await I.fillField(selectors.addressLine3, 'London');
  await I.fillField(selectors.postcode, 'B328NZ');
  await I.click(selectors.addAddressBtn);
  await I.selectOption(selectors.addressMonth, 'Jan');
  await I.selectOption(selectors.addressYear, '2005');
  await I.click(selectors.nextButton);
  await completeIsaSection(I, finalChecksButtonsToClickKeys);
};

const complete = async I => {
  await completeDefaults(I, 'finalChecksButtons');
};

const getReferredOnFinalChecks = async I => {
  await completeDefaults(I, 'finalChecksReferralButtons');

  await I.retry({ retries: 5, maxTimeout: 10000 }).see("We'll be in touch");
  await I.retry({ retries: 5, maxTimeout: 10000 }).see('What happens next?');
  await I.retry({ retries: 5, maxTimeout: 10000 }).see(
    "We'll get back to you within one working day"
  );
};

module.exports = {
  complete,
  getReferredOnFinalChecks,
};
