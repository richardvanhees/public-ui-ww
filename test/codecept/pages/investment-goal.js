const selectors = {
  goal: 'div[data-test="goal-a2"]',
};

const complete = async I => {
  await I.waitForElement(selectors.goal, 5);
  await I.click(selectors.goal);
};

module.exports = {
  complete,
};
