const uuid = require('uuid');
const { complete: signIn } = require('./sign-in');
const env = require('../support/env-vars');
const activateByEmail = require('../utils/activate-by-email');

const selectors = {
  title: 'select[data-test="dropdown-title-input"]',
  firstName: 'input[data-test="first-name-input"]',
  lastName: 'input[data-test="last-name-input"]',
  email: 'input[data-test="email-input"]',
  password: 'input[data-test="password-input"]',
  checkbox: 'label[data-test="checkbox-privacy-policy"]',
  lgEmployee: 'label[data-test="checkbox-lg-employee"]',
  recaptcha: '#g-recaptcha',
  submit: 'button[data-test="next-btn-register"]',
};

const complete = async I => {
  const email = `demo_${uuid.v4()}@wealthwizards.io`;
  const password = 'password123';

  await I.waitForElement(selectors.title, 5);
  await I.selectOption(selectors.title, 'Mr');
  await I.fillField(selectors.firstName, 'Pretty');
  await I.fillField(selectors.lastName, 'Decent');
  await I.fillField(selectors.email, email);
  await I.fillField(selectors.password, password);
  await I.click(selectors.checkbox);
  await I.click(selectors.lgEmployee);
  I.click(selectors.recaptcha);
  I.wait(3);
  await I.click(selectors.submit);

  // on activation page
  await I.retry({ retries: 10, maxTimeout: 15000 }).see('Resend email');

  // activate via the back door
  await activateByEmail(email);

  await I.amOnPage(`${env.BASE_URL}/investment/login`);

  await signIn(I, email, password);

  // verify account creation has occurred and they are on the dashboard
  await I.retry({ retries: 10, maxTimeout: 15000 }).see('My goal');

  // this is a hack to get around the fact that there seems to be a delay
  // before the clicks events are added
  await I.wait(3);

  return {
    email,
    password,
  };
};

module.exports = {
  complete,
};
