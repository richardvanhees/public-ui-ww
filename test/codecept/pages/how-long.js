const selectors = {
  tile1: 'button[data-test="length-a0"]',
  nextButton: 'button[data-test="next-btn-length"]',
};

const complete = async I => {
  await I.click(selectors.tile1);
  await I.click(selectors.nextButton);
};

module.exports = {
  complete,
};
