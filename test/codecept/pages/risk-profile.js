const selectors = {
  start: 'button[data-test="next-btn-atr-start"]',
  atrAnswerCount: 12,
  confirmAtr: 'button[data-test="popup-close-btn"]',
  finishAtr: 'button[data-test="next-btn-atr-playback"]',
  investmentExperienceStart: 'button[data-test="next-btn-experience-start"]',
  ieAnswerCount: 8,
  confirmIE: 'button[data-test="popup-close-btn"]',
  finishIE: 'button[data-test="next-btn-experience-playback"]',
  flexibilityStart: 'button[data-test="next-btn-flexibility-start"]',
  flexibilityAnswerCount: 5,
  finishFlexibility: 'button[data-test="next-btn-flexibility-playback"]',
  resilienceStart: 'button[data-test="next-btn-resilience-start"]',
  grossIncome: 'input[data-test="investment_resilience__gross_income"]',
  livingCosts: 'input[data-test="investment_resilience__monthly_expenses"]',
  investmentSignificance:
    'select[data-test="dropdown-investment_resilience__investment_significance"]',
  occupation: 'select[data-test="dropdown-investment_resilience__occupation"]',
  goodHealth: 'button[data-test="answer-3"]',
  resiliencePage1NextButton: 'button[data-test="next-btn-page-1"]',
  resiliencePage2NextButton: 'button[data-test="next-btn-page-2"]',
  confirmResilience: 'button[data-test="next-btn-resilience-playback"]',
};

const atr = async I => {
  await I.see('How to identify your attitude to risk');
  await I.waitForElement(selectors.start, 5);

  await I.retry({ retries: 3, maxTimeout: 10000 }).click(selectors.start);

  // risky profile on screen selections
  await I.click('button[data-test="q1-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q2-a1"]');
  await I.wait(1);
  await I.click('button[data-test="q3-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q4-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q5-a1"]');
  await I.wait(1);
  await I.click('button[data-test="q6-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q7-a1"]');
  await I.wait(1);
  await I.click('button[data-test="q8-a1"]');
  await I.wait(1);
  await I.click('button[data-test="q9-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q10-a5"]');
  await I.wait(1);
  await I.click('button[data-test="q11-a1"]');
  await I.wait(1);
  await I.click('button[data-test="q12-a5"]');

  await I.wait(1);

  await I.retry({ retries: 3, maxTimeout: 10000 }).click(selectors.confirmAtr);

  await I.click(selectors.finishAtr);
  await I.wait(1);
};

const investmentExperience = async I => {
  await I.waitForElement(selectors.investmentExperienceStart, 5);

  await I.click(selectors.investmentExperienceStart);

  // Considerable profile
  for (let i = 1; i <= selectors.ieAnswerCount; i++) {
    await I.click(`button[data-test="q${i}-a1"]`);
    await I.wait(1);
  }

  await I.wait(1);
  await I.retry({ retries: 3, maxTimeout: 10000 }).click(selectors.confirmIE);

  await I.click(selectors.finishIE);
  await I.wait(1);
};

// const flexibility = async I => {
//   await I.waitForElement(selectors.flexibilityStart, 5);

//   await I.click(selectors.flexibilityStart);

//   for (let i = 1; i <= selectors.flexibilityAnswerCount; i++) {
//     await I.click(`button[data-test="q${i}-a1"]`);
//   }

//   await I.click(selectors.finishFlexibility);
// };

const resilience = async I => {
  await I.waitForElement(selectors.resilienceStart, 5);

  await I.click(selectors.resilienceStart);

  // currency inputs can be slower to respond
  await I.wait(1);

  await I.fillField(selectors.grossIncome, '20000');

  await I.wait(3);

  await I.fillField(selectors.livingCosts, '1000');

  await I.wait(3);

  await I.click(selectors.resiliencePage1NextButton);

  await I.selectOption(selectors.investmentSignificance, 'very_significant');
  await I.selectOption(
    selectors.occupation,
    'manager_director_senior_official'
  );
  await I.click(selectors.goodHealth);
  await I.click(selectors.resiliencePage2NextButton);

  await I.wait(3);

  await I.click(selectors.confirmResilience);

  await I.wait(3);
};

const complete = async I => {
  await atr(I);
  await investmentExperience(I);
  // await flexibility(I);
  await resilience(I);
};

module.exports = {
  complete,
};
