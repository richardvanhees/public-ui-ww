const request = require('request-promise-native');
const env = require('../support/env-vars');

module.exports = async email =>
  request({
    uri: `${env.AUTH_SVC_URL}/v1/activate-by-email`,
    method: 'POST',
    body: {
      username: email,
    },
    json: true,
    headers: {
      Authorization: env.AUTH_SVC_API_KEY,
    },
  });
