const hidden = I => {
  I.retry({ retries: 20, maxTimeout: 60000 }).dontSeeElement(
    'div[data-test="spinner-container"]'
  );
};

module.exports = {
  hidden,
};
