const pickARandomLetter = () => {
  const alphabet = [
    'A',
    'B',
    'C',
    'E',
    'G',
    'H',
    'J',
    'K',
    'L',
    'M',
    'N',
    'P',
    'O',
    'R',
    'S',
    'T',
    'W',
    'X',
    'Y',
    'Z',
  ];
  return alphabet[Math.floor(Math.random() * 18 + 1)];
};

const prefixValid = prefix =>
  prefix !== 'BG' &&
  prefix !== 'GB' &&
  prefix !== 'NK' &&
  prefix !== 'KN' &&
  prefix !== 'TN' &&
  prefix !== 'NT' &&
  prefix !== 'ZZ' &&
  prefix[1] !== 'O';

module.exports = () => {
  let niNumber = pickARandomLetter() + pickARandomLetter();
  while (!prefixValid(niNumber)) {
    niNumber = pickARandomLetter() + pickARandomLetter();
  }

  for (let i = 0; i < 6; i++) {
    niNumber += Math.floor(Math.random() * 9 + 1);
  }
  return `${niNumber}D`;
};
