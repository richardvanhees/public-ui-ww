const test = require('tape');
const request = require('supertest');
const { app } = require('../../../server');
const {
  employersModel: Employers,
} = require('../../../server/money-wizard/db/employers-model');
const {
  popupModel: Popup,
} = require('../../../server/money-wizard/db/popup-model');
const mongoose = require('mongoose');
const moment = require('moment');

test('get a single popup', async assert => {
  assert.plan(3);

  await Employers.remove();
  await Popup.remove();

  const payload = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc('2000-01-01').toDate(),
    prizeDrawEndDate: moment.utc('2022-01-01').toDate(),
    prizesText: 'a car',
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(payload);

  await Popup.create({
    type: 1,
    image: 'donkey',
    title: 'Hey, you!',
    description: 'description',
    firstOptInLabel: 'firstOptInLabel',
    callToAction: 'Submit',
    cancel: 'no thanks',
  });

  const res = await request(app).get('/money-wizard/employers/ww/popups/1');

  assert.equal(res.status, 200, '200 returned');
  assert.true(res.body.prizeDrawEnabled, 'prizeDrawEnabled');
  assert.equal(
    res.body.prizeDrawLabel,
    'I’d like to be entered in to the competition to win a car. This competition will close on January 1st 2022.',
    'prizeDrawLabel set'
  );
});

test('get a single popup', async assert => {
  assert.plan(3);

  await Employers.remove();
  await Popup.remove();

  const payload = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc().toDate(),
    prizeDrawEndDate: moment.utc('2022-01-01').toDate(),
    prizesText: 'a car',
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(payload);

  await Popup.create({
    type: 1,
    image: 'donkey',
    title: 'Hey, you!',
    description: 'description',
    firstOptInLabel: 'firstOptInLabel',
    callToAction: 'Submit',
    cancel: 'no thanks',
  });

  const res = await request(app).get('/money-wizard/employers/ww/popups/1');

  assert.equal(res.status, 200, '200 returned when it starts today');
  assert.true(res.body.prizeDrawEnabled, 'prizeDrawEnabled');
  assert.equal(
    res.body.prizeDrawLabel,
    'I’d like to be entered in to the competition to win a car. This competition will close on January 1st 2022.',
    'prizeDrawLabel set'
  );
});

test('get a single popup', async assert => {
  assert.plan(3);

  await Employers.remove();
  await Popup.remove();

  const payload = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc('2000-01-01').toDate(),
    prizeDrawEndDate: moment
      .utc('2028-05-16')
      .endOf('day')
      .toDate(),
    prizesText: 'a car',
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(payload);

  await Popup.create({
    type: 1,
    image: 'donkey',
    title: 'Hey, you!',
    description: 'description',
    firstOptInLabel: 'firstOptInLabel',
    callToAction: 'Submit',
    cancel: 'no thanks',
  });

  const res = await request(app).get('/money-wizard/employers/ww/popups/1');

  assert.equal(res.status, 200, '200 returned when it ends today');
  assert.true(res.body.prizeDrawEnabled, 'prizeDrawEnabled');
  assert.equal(
    res.body.prizeDrawLabel,
    'I’d like to be entered in to the competition to win a car. This competition will close on May 16th 2028.',
    'prizeDrawLabel set'
  );
});

test('get a single popup', async assert => {
  assert.plan(1);

  await Employers.remove();
  await Popup.remove();

  const payload = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc('2000-01-01').toDate(),
    prizeDrawEndDate: moment.utc('2001-01-01').toDate(),
    prizesText: 'a car',
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(payload);

  await Popup.create({
    type: 1,
    image: 'donkey',
    title: 'Hey, you!',
    description: 'description',
    firstOptInLabel: 'firstOptInLabel',
    callToAction: 'Submit',
    cancel: 'no thanks',
  });

  const res = await request(app).get('/money-wizard/employers/ww/popups/1');

  assert.false(res.body.prizeDrawEnabled, 'false when promotion was in past');
});

test('get a single popup', async assert => {
  assert.plan(1);

  await Employers.remove();
  await Popup.remove();

  const res = await request(app).get('/money-wizard/employers/ww/popups/1');

  assert.equal(res.status, 404, 'should return a 404');
});
