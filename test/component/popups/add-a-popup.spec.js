const test = require('tape');
const request = require('supertest');
const { app } = require('../../../server');
const {
  employersModel: Employers,
} = require('../../../server/money-wizard/db/employers-model');
const {
  popupModel: Popup,
} = require('../../../server/money-wizard/db/popup-model');
const mongoose = require('mongoose');
const moment = require('moment');
const { env } = require('../../../config/index');

test('create a popup', async assert => {
  assert.plan(1);

  await Employers.remove();
  await Popup.remove();

  const res = await request(app)
    .post('/money-wizard/popups')
    .set('Authorization', env.MONEY_WIZARD_API_KEY)
    .send({
      popups: [
        {
          image: 'donkey',
          type: 1,
          title: 'Hey, you!',
          description: 'description',
          firstOptInLabel: 'firstOptInLabel',
          callToAction: 'Submit',
          cancel: 'no thanks',
        },
      ],
      version: 1,
    });

  assert.equal(res.status, 201, '201 created');
});
