const test = require('tape');
const request = require('supertest');
const {
  app
} = require('../../server');
const {
  prioritiesModel: Priorities
} = require('../../server/money-wizard/db/priorities-model');
const cleardownDB = () => Priorities.remove();
const co = require('co');
const mongoose = require('mongoose');

test('get answers', (t) => {

  t.test('create db entry of priorities and verify we can retrive them', async(t) => {
    t.plan(2);

      await cleardownDB();

      const payload = {
        "priorities": [
          "Building up Savings",
          "Investigating money for the long term",
          "Insuring your finances for the unexpected",
          "Saving for retirement",
          "Preparing for retirement",
          "Managing regular spending",
          "Reducing debt",
          "Buying a home"
        ],
        "version": 1
      };

      await Priorities.create(payload);

      const res = await request(app)
        .get('/money-wizard/priorities/1')

      t.deepEqual(res.body[0].priorities, [
        "Building up Savings",
        "Investigating money for the long term",
        "Insuring your finances for the unexpected",
        "Saving for retirement",
        "Preparing for retirement",
        "Managing regular spending",
        "Reducing debt",
        "Buying a home"
      ]);
      t.equal(res.body[0].version, 1);

});

});
