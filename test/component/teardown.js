const test = require('tape');
const { app } = require('../../server');

test('teardown', (t) => {
  app.shutdown();
  t.end();
});
