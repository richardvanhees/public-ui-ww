const test = require('tape');
const proxyquire = require('proxyquire');
const request = require('supertest');
const nock = require('nock');
const MoneyModel = require('../../server/money-wizard/db/money-model').moneyModel;
const co = require('co');
const migrationScript = require('../../db-migrations/changesets/001-encrypt-with-iv')
const {
  encryptString
} = require('ww-utils').encryption;
const {
  encrypt: encryptTheOldWay
} = require('ww-utils').resourceId;
const MongoClient = require('mongodb').MongoClient;
const {
  MONGODB_URL,
  MW_PII_DATA_ENCRYPTION_KEY,
  UPDATED_MW_PII_DATA_ENCRYPTION_KEY,
  UPDATED_MW_PII_DATA_ENCRYPTION_IV,
} = require('../../config').env;
const { app } = require('../../server');

test('003-encrypt-with-iv', (t) => {
  t.test('successful encryption occurs', async assert => {
    assert.plan(3);

    const email = 'mark@support.com';
    const employer = 'csc';

    await MoneyModel.remove();

    await new MoneyModel({
      email: encryptTheOldWay(email, MW_PII_DATA_ENCRYPTION_KEY),
      employer: 'csc',
      questions: [{ foo: 'bar' }],
      consentedPrivacyPolicy: true,
      wwFinancialPriorities: [{ foo: 'bar' }],
      userFinancialPriorities: [{ foo: 'bar' }],
      questionVersion: '1.1',
      emailSent: true,
      template: 'test',
      password: 'fakePassword',
    }).save();

    let dbRef;

    const done = async() => {
      const moneyUsers = await MoneyModel.find();

      assert.equal(moneyUsers.length, 1);

      const user = moneyUsers[0];

      // check it has actually encrypted it
      assert.equal(user.email, encryptString(email, UPDATED_MW_PII_DATA_ENCRYPTION_IV, UPDATED_MW_PII_DATA_ENCRYPTION_KEY));

      dbRef.close();
    };

    MongoClient.connect(MONGODB_URL, function (err, db) {
      assert.equal(null, err);

      dbRef = db;

      //clear down locks
      dbRef.collection('encryption_locks').remove();

      migrationScript.up.bind({
        db,
      })(done);
    });
  });
});
