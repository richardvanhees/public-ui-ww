// app.get(
//   '/answers/:employer/:csv',
//   nocache(),
//   apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY_MI_DATA),
//   getAnswers
// );

const test = require('tape');
const request = require('supertest');
const { app } = require('../../server');
const { env } = require('../../config');

test('download mi csv', async assert => {
  assert.plan(1);

  const res = await request(app)
    .get('/money-wizard/answers/unknownEmployer/downloadCSV')
    .set('Authorization', 'wrong');

  assert.equal(res.status, 401, '401 returned when wrong api key sent');
});
