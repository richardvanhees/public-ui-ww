const test = require('tape');
const request = require('supertest');
const {
  app
} = require('../../server');
const {
  questionsModel: Questions
} = require('../../server/money-wizard/db/questions-model');
const cleardownQuestionsDB = () => Questions.remove();
const co = require('co');
const mongoose = require('mongoose');

test('get answers', (t) => {

  t.test('create db entry of questions and verify we can retrive them', async(t) => {
    t.plan(2);

      await cleardownQuestionsDB();

      const payload = {
        "questions": [{
          "id": 0,
          "images": false,
          "question": "How old are you?",
          "answers": [{
              "id": 0,
              "answer": "Under 26",
              "weighting": ["3", "1", "1", "1", "-30", "3", "2", "1"]
            },
            {
              "id": 1,
              "answer": "26 to 35",
              "weighting": ["3", "2", "3", "2", "-30", "3", "2", "3"]
            },
            {
              "id": 2,
              "answer": "36 to 50",
              "weighting": ["2", "3", "2", "3", "-30", "2", "2", "2"]
            },
            {
              "id": 3,
              "answer": "Over 50",
              "weighting": ["1", "3", "1", "3", "3", "2", "1", "1"]
            }
          ]
        }],
        "version": 1
      };

      await Questions.create(payload);

      const res = await request(app)
        .get('/money-wizard/questions/1')

      t.deepEqual(res.body[0].questions, [{
        "id": 0,
        "images": false,
        "question": "How old are you?",
        "answers": [{
            "id": 0,
            "answer": "Under 26",
            "weighting": ["3", "1", "1", "1", "-30", "3", "2", "1"]
          },
          {
            "id": 1,
            "answer": "26 to 35",
            "weighting": ["3", "2", "3", "2", "-30", "3", "2", "3"]
          },
          {
            "id": 2,
            "answer": "36 to 50",
            "weighting": ["2", "3", "2", "3", "-30", "2", "2", "2"]
          },
          {
            "id": 3,
            "answer": "Over 50",
            "weighting": ["1", "3", "1", "3", "3", "2", "1", "1"]
          }
        ]
      }]);
      t.equal(res.body[0].version, 1);

  });

});
