const test = require('tape');
const request = require('supertest');
const { app } = require('../../server');
const {
  moneyModel: MoneyUser,
} = require('../../server/money-wizard/db/money-model');
const mongoose = require('mongoose');
const moment = require('moment');
const {
  employersModel: Employers,
} = require('../../server/money-wizard/db/employers-model');

const fakeAnswers = {
  email: 'sdfhcjskldfhfkd@fdjfghs.com',
  employer: 'demoEmployer',
  template: 'Money Wizard (non Pension Wizard user)',
  popupType: 3,
  consentedPrivacyPolicy: true,
  questions: {
    userInput: {
      '0': {
        answerId: 2,
        weighting: ['1', '3', '3', '3', '0', '0', '0', '0'],
      },
      '1': {
        answerId: 1,
        weighting: ['0', '0', '3', '0', '0', '3', '1', '1'],
      },
      '2': {
        answerId: 1,
        weighting: ['1', '1', '1', '1', '0', '0', '0', '0'],
      },
      '3': {
        answerId: 1,
        weighting: ['0', '0', '0', '0', '0', '1', '1', '0'],
      },
      '4': {
        answerId: 1,
        weighting: ['0', '0', '1', '0', '0', '0', '0', '0'],
      },
      '5': {
        answerId: 1,
        weighting: ['3', '0', '0', '0', '0', '3', '0', '3'],
      },
      '6': {
        answerId: 1,
        weighting: ['0', '1', '1', '1', '0', '0', '0', '0'],
      },
      '7': {
        answerId: 1,
        weighting: ['0', '0', '0', '1', '0', '0', '0', '0'],
      },
      '8': {
        answerId: 1,
        weighting: ['3', '1', '0', '1', '0', '3', '1', '0'],
      },
    },
  },
  wwFinancialPriorities: [
    { id: 5, name: 'Managing regular spending', score: 10, orderId: 0 },
    { id: 2, name: 'Protecting from the unexpected', score: 9, orderId: 1 },
    { id: 0, name: 'Building up savings', score: 8, orderId: 2 },
    { id: 3, name: 'Saving into pensions', score: 7, orderId: 3 },
    {
      id: 1,
      name: 'Investing money for the long term',
      score: 6,
      orderId: 4,
    },
    { id: 7, name: 'Buying a home', score: 4, orderId: 5 },
    { id: 6, name: 'Reducing debt', score: 3, orderId: 6 },
    { id: 4, name: 'Preparing for retirement', score: 0, orderId: 7 },
  ],
  userFinancialPriorities: [
    { id: 5, name: 'Managing regular spending', score: 10, orderId: 0 },
    { id: 2, name: 'Protecting from the unexpected', score: 9, orderId: 1 },
    { id: 0, name: 'Building up savings', score: 8, orderId: 2 },
    { id: 3, name: 'Saving into pensions', score: 7, orderId: 3 },
    {
      id: 1,
      name: 'Investing money for the long term',
      score: 6,
      orderId: 4,
    },
    { id: 7, name: 'Buying a home', score: 4, orderId: 5 },
    { id: 6, name: 'Reducing debt', score: 3, orderId: 6 },
    { id: 4, name: 'Preparing for retirement', score: 0, orderId: 7 },
  ],
  questionVersion: 1,
  password: 'fakePassword',
  emailSent: false,
  optIn: true,
  optInEmail: 'sdfhcjskldfhfkd@fdjfghs.com',
};

test('save answers', async assert => {
  assert.plan(1);

  await Employers.remove();
  await MoneyUser.remove();

  const employer = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc('2000-01-01').toDate(),
    prizeDrawEndDate: moment.utc('2022-01-01').toDate(),
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(employer);

  const res = await request(app)
    .post('/money-wizard/employers/ww/answers')
    .send({ ...fakeAnswers });

  assert.equal(res.status, 200, '200 returned');
});

test('update answers', async assert => {
  assert.plan(5);

  await Employers.remove();
  await MoneyUser.remove();

  const employer = {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
    ],
    mailTemplate: 'testTemplate',
    questionsVersion: 1,
    prioritiesVersion: 2,
    popupType: 1,
    prizeDrawEnabled: true,
    prizeDrawLabel: 'A description of the prize',
    prizeDrawStartDate: moment.utc('2000-01-01').toDate(),
    prizeDrawEndDate: moment.utc('2022-01-01').toDate(),
    canLogin: true,
    canContactPAT: true,
  };

  await Employers.create(employer);

  const res = await request(app)
    .put('/money-wizard/employers/ww/answers')
    .send({ ...fakeAnswers, prizeDrawOptIn: true });

  assert.equal(res.status, 200, '200 returned');
  assert.equal(
    res.body.data.prizeDrawOptIns.length,
    1,
    'prizeDrawOptIn recorded'
  );
  assert.equal(
    res.body.data.prizeDrawOptIns[0].label,
    employer.prizeDrawLabel,
    'prizeDrawLabel recorded'
  );
  assert.equal(
    res.body.data.prizeDrawOptIns[0].startDate,
    '2000-01-01T00:00:00.000Z',
    'prizeDrawStartDate recorded'
  );
  assert.equal(
    res.body.data.prizeDrawOptIns[0].endDate,
    '2022-01-01T00:00:00.000Z',
    'prizeDrawEndDate recorded'
  );
});
