const test = require('tape');
const request = require('supertest');
const {
  app
} = require('../../server');
const {
  employersModel: Employers
} = require('../../server/money-wizard/db/employers-model');
const cleardownDB = () => Employers.remove();
const co = require('co');
const mongoose = require('mongoose');

test('get employers', (t) => {

  t.test('create db entry for an employer and verify we can retrieve it', async (t) => {
    t.plan(2);

    await cleardownDB();

    const payload = {
      employer: "ww",
      employerHash: "ad57484016654da87125db86f4227ea3",
      tipsLinks: [],
      bespokeLinks: [],
      mailTemplate: 'testTemplate',
      questionsVersion: 1,
      prioritiesVersion: 2,
      popupType: 1,
      canLogin: true,
      canContactPAT: true,
    };

    await Employers.create(payload);

    const res = await request(app)
      .get('/money-wizard/employers');

    t.equal(res.body[0].employer, "ww");
    t.equal(res.body[0].employerHash, "ad57484016654da87125db86f4227ea3");

  });
});
