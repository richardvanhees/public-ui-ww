const express = require('express');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const app = express();

const passphrase = 'C+RpZWfA5b8W@uq3HP%9csrpq9bhL2e2q_qH+kA#R7N!';

const key = fs.readFileSync(`${__dirname}/private.key`).toString();

const token = jwt.sign(
  { userType: 'user', uniqueIdentifier: '9999999999999' },
  {
    key,
    passphrase,
  },
  {
    algorithm: 'RS256',
    expiresIn: '9000 days',
  }
);

console.log(token);
