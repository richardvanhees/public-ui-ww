const http = require('http')

const retry = (times, fn) => {
    let attempts = 0

    const run = () => {
        fn( err => {
            if (err) {
                attempts++
                if (attempts >= times) {
                    process.exit(-1)
                }
                console.log(`failed attempt ${attempts}. Retrying...`)
                return setTimeout(run, 5000)
            }
            process.exit(0)
        })
    }

    run()
}

retry(10, cb => {
    console.log('checking if server is up')
    http.get('http://localhost.wealthwizards.io', res => {
        cb()
    }).on('error', e => {
        cb(e)
    })
})