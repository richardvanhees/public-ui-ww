const {
  MongoClient,
} = require('mongodb');
const { env } = require('../config');
const mm = require('mongodb-migrations');
const logger = require('ww-logging').logger();
const moment = require('moment');

const lockName = 'migration';

const run = async() => {
  if (env.PRODUCT_KEY !== 'money-wizard') {
    return process.exit(0);
  }

  const db = await MongoClient.connect(env.MONGODB_URL);

  const locks = db.collection('migration_locks');

  locks.createIndex({
    lock_name: 1,
  }, {
    unique: true,
  });

  locks.deleteOne({
    lock_name: lockName,
    timestamp: {
      $lt: moment.utc().subtract(45, 'minutes').toDate(),
    },
  });

  try {
    await locks.insertOne({
      lock_name: 'migration',
      timestamp: moment.utc().toDate(),
    });
  } catch (e) {
    logger.inProdEnv('Node could not establish lock, exiting', e);
    await db.close();
    // marking this as ok to startup since other node is likely to have it
    process.exit(0);
  }

  const migrator = new mm.Migrator({
    url: env.MONGODB_URL,
  });

  return migrator.runFromDir(`${__dirname}/changesets`, (e) => {
    if (e) {
      logger.wakeMeUpInTheMiddleOfTheNight(e);
      process.exit(1);
    }
    locks.deleteOne({
      lock_name: lockName,
    });
    db.close();
    process.exit(0);
  });
};

run();
