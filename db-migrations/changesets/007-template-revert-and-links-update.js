const { env } = require('../../config');
const logger = require('ww-logging').logger();

module.exports.id = 'template-revert-and-links-update';

const bespokeLinks = {
  jlp: [
    {
      "type": "link",
      "priorityId": 0,
      "url": "http://partnerintranet.co.uk/content/itr/home/being-a-partner/benefits/retailcure.html",
      "caption": "Partner Choice",
      "emailDescriptionStart": "Saving money every month to help pay for treats could be much easier ",
      "emailCaption": "with a clear plan",
      "emailDescriptionEnd": " – make yours today."
    },
    {
      "type": "link",
      "priorityId": 3,
      "url": "https://www.legalandgeneral.com/workplacebenefits/microsites/jlp/",
      "caption": "Your Defined Contribution Pension",
      "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
      "emailCaption": "starting early",
      "emailDescriptionEnd": " could help make it a brighter one."
    },
    {
      "type": "link",
      "priorityId": 6,
      "url": "https://www.payplan.com/",
      "caption": "Free online debt advice",
      "emailDescriptionStart": "If you’re facing unaffordable debts, ",
      "emailCaption": "seeking free advice",
      "emailDescriptionEnd": " can be the simplest way of tackling them without the worry."
    }
  ],
  jlp2: [
    {
      "type": "link",
      "priorityId": 0,
      "url": "http://partnerintranet.co.uk/content/itr/home/being-a-partner/benefits/retailcure.html",
      "caption": "Partner Choice",
      "emailDescriptionStart": "Saving money every month to help pay for treats could be much easier ",
      "emailCaption": "with a clear plan",
      "emailDescriptionEnd": " – make yours today."
    },
    {
      "type": "link",
      "priorityId": 3,
      "url": "https://www.legalandgeneral.com/workplacebenefits/microsites/jlp/",
      "caption": "Your Defined Contribution Pension",
      "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
      "emailCaption": "starting early",
      "emailDescriptionEnd": " could help make it a brighter one."
    },
    {
      "type": "link",
      "priorityId": 6,
      "url": "https://www.payplan.com/",
      "caption": "Free online debt advice",
      "emailDescriptionStart": "If you’re facing unaffordable debts, ",
      "emailCaption": "seeking free advice",
      "emailDescriptionEnd": " can be the simplest way of tackling them without the worry."
    }
  ],
  unilever: [
    {
      "type": "intro",
      "priorityId": 1,
      "content": "Choosing the right option often means better returns on your investment. Do what’s best for you, and help your money grow:"
    },
    {
      "type": "link",
      "emailCaption": "Sharebuy (UK Only)",
      "caption": "Sharebuy",
      "url": "http://inside.unilever.com/services/UKEI/HR-UK/Guides/Unilever%20ShareBuy%20Brochure.pdf",
      "priorityId": 1
    },
    {
      "type": "link",
      "emailCaption": "Share in Our Future",
      "caption": "SIOF",
      "url": "http://shareinourfuture.com/",
      "priorityId": 1
    },
    {
      "type": "link",
      "emailCaption": "Aviva - My Money",
      "caption": "Corporate ISA",
      "url": "http://www.friendslife.co.uk/mymoney/usave",
      "priorityId": 1
    },
    {
      "type": "link",
      "emailDescriptionEnd": " could help make it a brighter one.",
      "emailCaption": "starting early",
      "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
      "caption": "Main Pension Site",
      "url": "http://www.uukpf.co.uk/",
      "priorityId": 3
    },
    {
      "type": "intro",
      "priorityId": 5,
      "content": "Before you start spending, consider taking a look at the discounts and rewards available to you as a Unilever employee. They could help you stretch your budget that little bit further."
    },
    {
      "type": "link",
      "emailCaption": "Interest Free Loans",
      "caption": "Season ticket interest free loans",
      "url": "http://inside.unilever.com/services/UKEI/HR-UK/Policies/UK%20Season%20Ticket%20Loan%20Policy%20Dec%202016.pdf",
      "priorityId": 5
    },
    {
      "type": "link",
      "emailCaption": "Discounts & Rewards",
      "caption": "My Unilever Discounts",
      "url": "https://myunileverdiscounts.rewardgateway.co.uk/",
      "priorityId": 5
    },
    {
      "type": "link",
      "emailCaption": "Money Matters",
      "caption": "Financial Education Site",
      "url": "http://www.unilever.faife.co.uk/",
      "priorityId": 5
    },
    {
      "type": "link",
      "emailDescriptionEnd": ".",
      "emailCaption": "Money Matters",
      "emailDescriptionStart": "If you’re facing unaffordable debts, take a look at the debt section of the knowledge library on ",
      "caption": "Financial Education Site",
      "url": "http://www.unilever.faife.co.uk/",
      "priorityId": 6
    },
    {
      "type": "link",
      "emailDescriptionEnd": ".",
      "emailCaption": "most out of your money",
      "emailDescriptionStart": "Finding the right home can be stressful. Give yourself the best chance of success by understanding how to get the ",
      "caption": "Financial Education Site",
      "url": "http://www.unilever.faife.co.uk/",
      "priorityId": 7
    }
  ]
};


const setBespokeLinks = employer => bespokeLinks[employer] || [];

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const employersCollection = self.db.collection('employers');

      const employers = await employersCollection.find({}).toArray();

      for (const employer of employers) {
        try {
          const { _id, employer: employerName } = employer;

          await employersCollection.findOneAndUpdate({ _id }, {
            $set: {
              "mailTemplate": "Money Wizard",
              "bespokeLinks": setBespokeLinks(employerName)
            },
          }, {
            returnOriginal: false,
          });

          logger.inProdEnv(`Updated employer with id ${_id}`);
        } catch (e) {
          console.log(e);
          logger.inProdEnv(
            `Failed to update employer user with id ${employer._id}`, e); // eslint-disable-line no-underscore-dangle
        }
      }

      logger.inProdEnv('Completed updating employers');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};

