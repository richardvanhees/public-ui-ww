const { env } = require('../../config');
const logger = require('ww-logging').logger();

module.exports.id = 'change-employer-model';

const bespokeLinks = {
  jlp: [{
    "priorityId": 0,
    "url": "https://www.partnerchoice.co.uk/listing/2899/retailcure",
    "caption": "Partner Choice",
    "emailDescriptionStart": "Saving money every month to help pay for treats could be much easier ",
    "emailCaption": "with a clear plan",
    "emailDescriptionEnd": " – make yours today."
  }, {
    "priorityId": 3,
    "url": "https://www.legalandgeneral.com/workplacebenefits/microsites/jlp/",
    "caption": "Your Defined Contribution Pension",
    "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
    "emailCaption": "starting early",
    "emailDescriptionEnd": " could help make it a brighter one."
  }, {
    "priorityId": 6,
    "url": "https://www.payplan.com/",
    "caption": "Free online debt advice",
    "emailDescriptionStart": "If you’re facing unaffordable debts, ",
    "emailCaption": "seeking free advice",
    "emailDescriptionEnd": " can be the simplest way of tackling them without the worry."
  }],
  jlp2: [{
    "priorityId": 0,
    "url": "https://www.partnerchoice.co.uk/listing/2899/retailcure",
    "caption": "Partner Choice",
    "emailDescriptionStart": "Saving money every month to help pay for treats could be much easier ",
    "emailCaption": "with a clear plan",
    "emailDescriptionEnd": " – make yours today."
  }, {
    "priorityId": 3,
    "url": "https://www.legalandgeneral.com/workplacebenefits/microsites/jlp/",
    "caption": "Your Defined Contribution Pension",
    "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
    "emailCaption": "starting early",
    "emailDescriptionEnd": " could help make it a brighter one."
  }, {
    "priorityId": 6,
    "url": "https://www.payplan.com/",
    "caption": "Free online debt advice",
    "emailDescriptionStart": "If you’re facing unaffordable debts, ",
    "emailCaption": "seeking free advice",
    "emailDescriptionEnd": " can be the simplest way of tackling them without the worry."
  }]
};

const setBespokeLinks = employer => !bespokeLinks[employer] ? [] : bespokeLinks[employer];

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const employersCollection = self.db.collection('employers');

      const employers = await employersCollection.find({}).toArray();

      for (const employer of employers) {
        try {
          const { email, _id, } = employer;

          await employersCollection.findOneAndUpdate({ _id }, {
            $set: {
              tipsLinks: [],
              bespokeLinks: setBespokeLinks(employer.employer),
              mailTemplate: 'Money Wizard'
            },
          }, {
            returnOriginal: false,
          });

          logger.inProdEnv(`Updated employer with id ${_id}`);
        } catch (e) {
          console.log(e);
          logger.inProdEnv(
            `Failed to update employer user with id ${employer._id}`, e); // eslint-disable-line no-underscore-dangle
        }
      }

      logger.inProdEnv('Completed updating employers');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};

