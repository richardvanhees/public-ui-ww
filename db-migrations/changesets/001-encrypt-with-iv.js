const co = require('co');
const logger = require('ww-logging').logger();
const { env } = require('../../config');
const { resourceId, encryption } = require('ww-utils');

module.exports.id = 'encrypt-with-iv';

const decryptThenEncryptWithNewKey = email => {
  if (!email) {
    return email;
  }

  const decrypted = resourceId.decrypt(email,
    env.MW_PII_DATA_ENCRYPTION_KEY);

  return encryption.encryptString(
    decrypted,
    env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
    env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
  );
};

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const moneyUsersCollection = self.db.collection('moneyUsers');

      const moneyUsers = await moneyUsersCollection.find({}).toArray();

      for (const user of moneyUsers) {
        try {
          const {
            email,
            _id,
          } = user;

          await moneyUsersCollection.findOneAndUpdate({
            _id,
          }, {
            $set: {
              email: decryptThenEncryptWithNewKey(email),
            },
          }, {
            returnOriginal: false,
          });

          logger.inProdEnv(`Encrypted money wizard user with id ${_id}`);
        } catch (e) {
          logger.inProdEnv(
            `Failed to encrypt money wizard user with id ${user._id}`, e); // eslint-disable-line no-underscore-dangle
        }
      }

      logger.inProdEnv('Completed encryption of money wizard users');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};
