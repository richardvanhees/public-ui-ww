const { env } = require('../../config');
const logger = require('ww-logging').logger();

module.exports.id = 'migrate-to-new-template';

const setMailTemplate = () =>
  featureFlags[employer];

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const employersCollection = self.db.collection('employers');

      const employers = await employersCollection.find({}).toArray();

      for (const employer of employers) {
        try {
          const { _id, } = employer;

          await employersCollection.findOneAndUpdate({ _id }, {
            $set: {
              "mailTemplate": "Money Wizard - TEMPLATE FOR MIGRATION",
            },
          }, {
            returnOriginal: false,
          });

          logger.inProdEnv(`Updated employer with id ${_id}`);
        } catch (e) {
          console.log(e);
          logger.inProdEnv(
            `Failed to update employer user with id ${employer._id}`, e); // eslint-disable-line no-underscore-dangle
        }
      }

      logger.inProdEnv('Completed updating employers');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};

