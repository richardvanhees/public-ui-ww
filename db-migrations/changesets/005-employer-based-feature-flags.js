const { env } = require('../../config');
const logger = require('ww-logging').logger();

module.exports.id = 'employer-based-feature-flags';

const featureFlags = {
  impellam: {
    canContactPAT: false,
    canLogin: true,
  },
  fco: {
    canContactPAT: false,
    canLogin: true,
  },
  unilever: {
    canContactPAT: true,
    canLogin: false,
  }
};

const setFeatureFlags = employer =>
  !featureFlags[employer] ? {
    canContactPAT: true,
    canLogin: true
  } : featureFlags[employer];

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const employersCollection = self.db.collection('employers');

      const employers = await employersCollection.find({}).toArray();

      for (const employer of employers) {
        try {
          const { _id, } = employer;

          await employersCollection.findOneAndUpdate({ _id }, {
            $set: {
              ...setFeatureFlags(employer.employer),
            },
          }, {
            returnOriginal: false,
          });

          logger.inProdEnv(`Updated employer with id ${_id}`);
        } catch (e) {
          console.log(e);
          logger.inProdEnv(
            `Failed to update employer user with id ${employer._id}`, e); // eslint-disable-line no-underscore-dangle
        }
      }

      logger.inProdEnv('Completed updating employers');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};

