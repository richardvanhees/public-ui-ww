const logger = require('ww-logging').logger();

module.exports.id = 'make-employer-unique';

const indexName = 'employer_1';
const collectionName = 'employers';


module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const employersCollection = self.db.collection('employers');

      await employersCollection.dropIndex(indexName);

      await employersCollection.createIndex(
        { employer: 1 },
        { name: indexName, unique: true }
      );

      logger.inProdEnv('Completed making employers unique');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    done();
  } catch (error) {
    done(error);
  }
};
