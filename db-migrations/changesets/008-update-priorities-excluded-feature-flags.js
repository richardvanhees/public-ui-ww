const { env } = require('../../config');
const logger = require('ww-logging').logger();

module.exports.id = 'update-priorities-excluded-feature-flags';

const priorities = () => [
  {
    "top_tip": "Set goals and targets",
    "id": 0,
    "name": "Building up savings",
    "image": "q8a0",
    "tipsLinks": [
      {
        "emailDescriptionEnd": " – make yours today.",
        "emailCaption": "with a clear plan",
        "emailDescriptionStart": "Saving money every month to help pay for treats or unexpected costs could be much easier ",
        "caption": "Savings calculator",
        "url": "https://www.moneyadviceservice.org.uk/en/tools/savings-calculator"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Putting aside some money every month to help you pay for unexpected costs or treats.",
    "long_description": "Whether it’s for a new car, family holiday or just a rainy day, savings can give you quick and easy access to money when you need it.",
    "list": [],
    "link_header": "Help to build up savings"
  },
  {
    "top_tip": "Find the right choice for you",
    "id": 1,
    "name": "Investing money for the long term",
    "image": "q4a2",
    "tipsLinks": [
      {
        "emailDescriptionEnd": ", and help your money grow.",
        "emailCaption": "Find the best for you",
        "emailDescriptionStart": "Choosing the right option often means better returns on your investment. ",
        "caption": "Guide to investing",
        "url": "https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Once you’ve built up some savings, you can look for better returns on your money to help it grow over time.",
    "long_description": "Investing can help you grow your money over a period of around five years or more. Your options can include stocks and shares, property or dedicated investment funds.",
    "list": [],
    "link_header": "Help to invest money for the long term"
  },
  {
    "top_tip": "Ensure you're covered",
    "id": 2,
    "name": "Protecting from the unexpected",
    "image": "q4a0",
    "tipsLinks": [
      {
        "emailDescriptionEnd": ", you and your family could be a little better off should anything happen.",
        "emailCaption": "making sure you’re protected",
        "emailDescriptionStart": "No-one likes to consider the unexpected. But, by ",
        "caption": "Safeguard your future",
        "url": "http://protecting-your-world.moneyadviceservice.org.uk/"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Making sure you and your family are financially protected in the event of illness, unemployment or death.",
    "long_description": "No-one likes to consider the unexpected. But, by making sure you’re protected, you and your family could be a little better off should anything happen.",
    "list": [],
    "link_header": "Help to protect from the unexpected"
  },
  {
    "top_tip": "Start contributing early",
    "id": 3,
    "name": "Saving into pensions",
    "image": "q7a0",
    "tipsLinks": [
      {
        "emailDescriptionEnd": " could help make it a brighter one.",
        "emailCaption": "starting early",
        "emailDescriptionStart": "Regularly putting money into a pension gives you an income for the future, and ",
        "caption": "Pension Calculator",
        "url": "https://www.moneyadviceservice.org.uk/en/tools/pension-calculator"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Regularly putting aside some money into a pension to give you an income when you retire.",
    "long_description": "Starting to save in to your pension early often gives you a better chance of a more comfortable retirement, so don’t wait to make your plan.",
    "list": [],
    "link_header": "Help to save into pensions"
  },
  {
    "top_tip": "Understand your options",
    "id": 4,
    "name": "Preparing for retirement",
    "image": "q7a0",
    "tipsLinks": [
      {
        "emailDescriptionEnd": " can help you make sure you’re on track for the income you want.",
        "emailCaption": "Understanding your retirement options",
        "caption": "Pension Wise",
        "url": "https://www.pensionwise.gov.uk/en"
      }
    ],
    "alternative_descriptions": [
      {
        "triggered_by": [
          "q0a4"
        ],
        "excludedFeatureFlags": [
          "canContactPAT"
        ],
        "short_description": "Take a look at your entire financial picture with our team of in-house chartered financial planners",
        "long_description": "<p>Take a look at your entire financial picture with our team of in-house chartered financial planners.</p><p><a target='_blank' href='https://mailchi.mp/wealthwizards/personal-advice'>Contact us</a></p>"
      }
    ],
    "short_description": "When you’re close to retirement, working out the best way of providing an income when you retire.",
    "long_description": "If you’re approaching retirement age and aren’t sure of your options, there’s free advice available. It can help with your decisions and check you’re on track for the income you want.",
    "list": [],
    "link_header": "Help to prepare for retirement"
  },
  {
    "top_tip": "Create a budget",
    "id": 5,
    "name": "Managing regular spending",
    "image": "q3a2",
    "tipsLinks": [
      {
        "emailDescriptionEnd": " can be the simplest way of spotting potential savings.",
        "emailCaption": "creating a budget",
        "emailDescriptionStart": "If you want to track your spending, ",
        "caption": "Building your budget",
        "url": "https://mandrillapp.com/track/click/30990860/www.citizensadvice.org.uk?p=eyJzIjoiSWR6QXFuZVlSb1dIQlY0R2d2XzhpMnpaaTRRIiwidiI6MSwicCI6IntcInVcIjozMDk5MDg2MCxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5jaXRpemVuc2FkdmljZS5vcmcudWtcXFwvZGVidC1hbmQtbW9uZXlcXFwvYnVkZ2V0aW5nXFxcL2J1ZGdldGluZ1xcXC93b3JrLW91dC15b3VyLWJ1ZGdldFxcXC9cIixcImlkXCI6XCIzMDNjNWEzZjgyM2E0NDc2YmE1YTQ4NGU3YzBiYzA1ZlwiLFwidXJsX2lkc1wiOltcIjExNThjZDZkYzE2NWZlMDhmYzA0OTM4NDhkODYwY2JhMDgxNTdkNGJcIl19In0"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Keeping a track of your spending and making sure this is less than you earn.",
    "long_description": "If you find yourself struggling to manage your day to day spending, a budget can be the quickest way to take back control of your money.",
    "list": [],
    "link_header": "Help to budget"
  },
  {
    "top_tip": "Get help and advice",
    "id": 6,
    "name": "Reducing debt",
    "image": "q6a3",
    "tipsLinks": [
      {
        "emailDescriptionEnd": " can be the simplest way of tackling them without the worry.",
        "emailCaption": "seeking free advice",
        "emailDescriptionStart": "If you’re facing unaffordable debts, ",
        "caption": "Free online debt advice",
        "url": "https://mandrillapp.com/track/click/30990860/www.stepchange.org?p=eyJzIjoiQTRaTngwMk1XMUhsdklQZmhTQjJKWks2WHJnIiwidiI6MSwicCI6IntcInVcIjozMDk5MDg2MCxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5zdGVwY2hhbmdlLm9yZ1xcXC9EZWJ0cmVtZWR5LmFzcHhcIixcImlkXCI6XCIzMDNjNWEzZjgyM2E0NDc2YmE1YTQ4NGU3YzBiYzA1ZlwiLFwidXJsX2lkc1wiOltcIjRlZmQwZjUxMzcxMTMwOGNjZDY4YzgzMDQzYzU3ZDU1NzMwNGQxYzNcIl19In0"
      }
    ],
    "alternative_descriptions": [],
    "short_description": "Recognising your debts so that you can pay them off over time.",
    "long_description": "If you’re behind on repayments this could damage your credit rating and affect your ability to borrow in the future. If you’re struggling, there’s plenty of free advice at hand.",
    "list": [],
    "link_header": "Help to reduce debt"
  },
  {
    "top_tip": "Research everything",
    "id": 7,
    "name": "Buying a home",
    "image": "q5a1",
    "tipsLinks": [
      {
        "emailDescriptionEnd": ", you can give yourself the best chance of success.",
        "emailCaption": "researching your options",
        "emailDescriptionStart": "Finding the right home can be stressful. By ",
        "caption": "Home buyer's guide",
        "url": "https://mandrillapp.com/track/click/30990860/www.citizensadvice.org.uk?p=eyJzIjoiSDUwUTNvWWdZYlozeFB4T1pxckotb24weWFJIiwidiI6MSwicCI6IntcInVcIjozMDk5MDg2MCxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3d3dy5jaXRpemVuc2FkdmljZS5vcmcudWtcXFwvaG91c2luZ1xcXC9tb3ZpbmctYW5kLWltcHJvdmluZy15b3VyLWhvbWVcXFwvYnV5aW5nLWEtaG9tZVxcXC9cIixcImlkXCI6XCIzMDNjNWEzZjgyM2E0NDc2YmE1YTQ4NGU3YzBiYzA1ZlwiLFwidXJsX2lkc1wiOltcIjcyODRlNDEyYjA2MmQwZWNkN2VjYjU1N2NlZTc3NGNhNzQ3MmU0ZjFcIl19In0"
      }
    ],
    "alternative_descriptions": [],
    "long_description": "Buying a home can be stressful. With so many steps and costs involved, extensive research can give you the best chance of success.",
    "short_description": "Getting your mind and money ready for buying a new home.",
    "list": [],
    "link_header": "Help to buy a home"
  }
];

module.exports.up = function (done) { // eslint-disable-line func-names
  try {
    const self = this;
    (async () => {
      const prioritiesCollection = self.db.collection('priorities');

      const newPriorities = await prioritiesCollection.findOne({ version: 3 });

      console.log(newPriorities);

      prioritiesCollection.findOneAndUpdate({ version: 3 }, { ...newPriorities, priorities: priorities() });

      logger.inProdEnv('Completed updating priorities version 3');

      return done();
    })();
  } catch (error) {
    done(error);
  }
};

module.exports.down = function (done) {
  try {
    // no-op
    done();
  } catch (error) {
    done(error);
  }
};

