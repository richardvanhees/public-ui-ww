const R = require('ramda');

module.exports = (questions, userInput) => {
  const getScoreWeighting = ([id, value]) => {
    const question = R.find(R.propEq('id', Number(id)))(questions);
     if (!question) return 0;
    return R.pathOr(0, ['answers', value.answerId, 'scoreWeighting'], question);
  };

  return R.compose(
    R.sum,
    R.map(getScoreWeighting),
    R.toPairs,
  )(userInput);
};
