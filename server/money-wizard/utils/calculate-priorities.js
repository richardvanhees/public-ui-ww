module.exports = (weightingOptions, questions, userInput) => {
  const priorities = weightingOptions.map((item, i) => ({
    id: item.id,
    name: item.name,
    score: 0,
    orderId: i,
  }));

  Object.keys(userInput)
    .map(item => questions[item].answers[userInput[item].answerId].weighting)
    .forEach(item => {
      priorities.forEach((priority, index) => {
        if (priorities[index]) {
          priorities[index].score += parseFloat(item[index], 10);
        }
      });
    });

  priorities.sort((a, b) => b.score > a.score).forEach((item, index) => {
    item.orderId = index; // eslint-disable-line no-param-reassign
  });

  return priorities.filter(item => item.score >= 0);
};
