const test = require('tape');
const calculatePriorities = require('./calculate-priorities');
const priorities_v1 = require('../db/setup/priorities_v1');

test('test that priorities are correctly generated', t => {
  t.plan(1);

  const weightingOptions = priorities_v1.priorities.priorities;

  const theQuestions = [
    {
      answers: [
        {
          weighting: ['3', '1', '1', '1', '-30', '3', '2', '1'],
          answer: 'Under 26',
          id: 0,
        },
        {
          weighting: ['3', '2', '3', '2', '-30', '3', '2', '3'],
          answer: '26 to 35',
          id: 1,
        },
        {
          weighting: ['2', '3', '2', '3', '-30', '2', '2', '2'],
          answer: '36 to 50',
          id: 2,
        },
        {
          weighting: ['1', '3', '1', '3', '3', '2', '1', '1'],
          answer: 'Over 50',
          id: 3,
        },
      ],
      question: 'How old are you?',
      images: false,
      id: 0,
    },
    {
      answers: [
        {
          weighting: ['2', '2', '1', '3', '3', '2', '1', '2'],
          image: '../images/',
          answer: "I'm single",
          id: 0,
        },
        {
          weighting: ['3', '1', '3', '3', '3', '3', '3', '2'],
          image: '../images/',
          answer: "I'm single with children",
          id: 1,
        },
        {
          weighting: ['2', '2', '2', '2', '2', '2', '2', '2'],
          image: '../images/',
          answer: 'I have a partner',
          id: 2,
        },
        {
          weighting: ['3', '2', '3', '2', '2', '3', '3', '2'],
          image: '../images/',
          answer: 'I have a partner and children',
          id: 3,
        },
      ],
      question: "What's your living status?",
      images: true,
      id: 1,
    },
    {
      answers: [
        {
          weighting: ['3', '1', '2', '1', '2', '3', '2', '1'],
          image: '../images/',
          answer: 'I work part time on a contract basis',
          id: 0,
        },
        {
          weighting: ['3', '2', '2', '2', '2', '2', '1', '1'],
          image: '../images/',
          answer: 'I work full time on a contract basis',
          id: 1,
        },
        {
          weighting: ['2', '2', '2', '3', '3', '2', '2', '1'],
          image: '../images/',
          answer: 'I work part time in a permanent role',
          id: 2,
        },
        {
          weighting: ['2', '2', '2', '3', '3', '1', '1', '1'],
          image: '../images/',
          answer: 'I work full time in a permanent role',
          id: 3,
        },
      ],
      question: 'What is your employment status?',
      images: true,
      id: 2,
    },
    {
      answers: [
        {
          weighting: ['3', '1', '2', '1', '1', '3', '3', '1'],
          image: '../images/',
          answer: 'I spend more than I earn each month',
          id: 0,
        },
        {
          weighting: ['2', '1', '2', '1', '1', '2', '2', '1'],
          image: '../images/',
          answer: 'I spend everything I earn, but nothing more',
          id: 1,
        },
        {
          weighting: ['2', '2', '2', '2', '2', '1', '1', '2'],
          image: '../images/',
          answer: 'I always have some money left over each month',
          id: 2,
        },
      ],
      question: 'How would you describe your spending habits each month?',
      images: true,
      id: 3,
    },
    {
      answers: [
        {
          weighting: ['3', '1', '2', '1', '1', '3', '3', '1'],
          image: '../images/',
          answer: "I'd panic, I have no savings whatsoever",
          id: 0,
        },
        {
          weighting: ['2', '1', '1', '1', '1', '2', '2', '1'],
          image: '../images/',
          answer: "I might struggle, I have less than 3 months' wages saved up",
          id: 1,
        },
        {
          weighting: ['1', '3', '1', '2', '2', '1', '1', '2'],
          image: '../images/',
          answer: "I'd probably cope, I have a good emergency fund",
          id: 2,
        },
      ],
      question: 'How would you handle a large unexpected bill?',
      images: true,
      id: 4,
    },
    {
      answers: [
        {
          weighting: ['2', '2', '2', '2', '2', '2', '2', '-30'],
          image: '../images/',
          answer: 'I own my own home',
          id: 0,
        },
        {
          weighting: ['3', '3', '1', '1', '1', '3', '1', '3'],
          image: '../images/',
          answer: 'I would like to buy my own home',
          id: 1,
        },
        {
          weighting: ['2', '2', '2', '1', '1', '2', '1', '1'],
          image: '../images/',
          answer: "I'm not interested in buying a home",
          id: 2,
        },
      ],
      question: 'What is your home ownership situation?',
      images: true,
      id: 5,
    },
    {
      answers: [
        {
          weighting: ['2', '3', '2', '3', '3', '1', '-30', '2'],
          image: '../images/',
          answer: "I don't have any debts",
          id: 0,
        },
        {
          weighting: ['2', '3', '2', '3', '3', '2', '1', '2'],
          image: '../images/',
          answer: 'I repay my debts on time',
          id: 1,
        },
        {
          weighting: ['2', '1', '2', '1', '1', '2', '2', '1'],
          image: '../images/',
          answer: 'I sometimes have difficulty paying off debts',
          id: 2,
        },
        {
          weighting: ['1', '1', '2', '1', '1', '3', '3', '1'],
          image: '../images/',
          answer: "I'm in significant debt",
          id: 3,
        },
      ],
      question: "What's your debt situation?",
      images: true,
      id: 6,
    },
    {
      answers: [
        {
          weighting: ['2', '2', '1', '2', '3', '1', '1', '1'],
          image: '../images/',
          answer: "I've always paid into a company pension",
          id: 0,
        },
        {
          weighting: ['2', '2', '2', '3', '2', '1', '1', '1'],
          image: '../images/',
          answer:
            "I've had some periods where I haven't contributed to my pension",
          id: 1,
        },
        {
          weighting: ['1', '1', '2', '3', '1', '2', '2', '1'],
          image: '../images/',
          answer: "I'm not interested in saving into a pension",
          id: 2,
        },
      ],
      question: 'How would you describe your pension planning?',
      images: true,
      id: 7,
    },
    {
      answers: [
        {
          weighting: ['1', '2', '1', '2', '3', '1', '1', '3'],
          image: '../images/',
          answer: 'I save regularly and am focused on building my savings up',
          id: 0,
        },
        {
          weighting: ['1', '2', '2', '2', '2', '1', '1', '2'],
          image: '../images/',
          answer: "I want to save but haven't done so recently",
          id: 1,
        },
        {
          weighting: ['2', '3', '2', '2', '1', '2', '2', '1'],
          image: '../images/',
          answer: "I haven't been able to save",
          id: 2,
        },
      ],
      question:
        'Which of these best describes your position on saving for the future?',
      images: true,
      id: 8,
    },
  ];

  const userInput = {
    '0': {
      answerId: 0,
    },
    '1': {
      answerId: 2,
    },
    '2': {
      answerId: 3,
    },
    '3': {
      answerId: 1,
    },
    '4': {
      answerId: 2,
    },
    '5': {
      answerId: 0,
    },
    '6': {
      answerId: 1,
    },
    '7': {
      answerId: 1,
    },
    '8': {
      answerId: 1,
    },
  };

  const thePriorities = calculatePriorities(
    weightingOptions,
    theQuestions,
    userInput
  );

  t.deepEquals(thePriorities, [
    {
      id: 3,
      name: 'Saving into pensions',
      orderId: 0,
      score: 19,
    },
    {
      id: 1,
      name: 'Investing money for the long term',
      orderId: 1,
      score: 18,
    },
    {
      id: 0,
      name: 'Building up savings',
      orderId: 2,
      score: 17,
    },
    {
      id: 2,
      name: 'Protecting from the unexpected',
      orderId: 3,
      score: 16,
    },
    {
      id: 5,
      name: 'Managing regular spending',
      orderId: 4,
      score: 15,
    },
    {
      id: 6,
      name: 'Reducing debt',
      orderId: 5,
      score: 13,
    },
  ]);
});
