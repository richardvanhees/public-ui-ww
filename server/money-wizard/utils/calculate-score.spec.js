const test = require('tape');
const calculateScore = require('./calculate-score');
const { questions } = require('../db/setup/questions_v1').questions;

test('score is correctly generated', t => {
  t.plan(1);

  const input = {
    '3': {
      answerId: 2,
    },
    '4': {
      answerId: 2,
    },
    '6': {
      answerId: 0,
    },
    '7': {
      answerId: 0,
    },
    '8': {
      answerId: 0,
    },
  };

  const result = calculateScore(
    questions,
    input
  );

  t.deepEquals(result, 100, 'highest score');
});


test('score is correctly generated', t => {
  t.plan(1);

  const input = {
    '3': {
      answerId: 0,
    },
    '4': {
      answerId: 1,
    },
    '6': {
      answerId: 1,
    },
    '7': {
      answerId: 0,
    },
    '8': {
      answerId: 0,
    },
  };

  const result = calculateScore(
    questions,
    input
  );

  t.deepEquals(result, 60, 'somewhere inbetween');
});


test('score is correctly generated', t => {
  t.plan(1);

  const input = {
    '1': {
      answerId: 2,
    },
    '2': {
      answerId: 2,
    },
    '5': {
      answerId: 0,
    }
  };

  const result = calculateScore(
    questions,
    input
  );

  t.deepEquals(result, 0, 'no score, questions not measured');
});
