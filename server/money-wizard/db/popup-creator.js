const { popupModel: Popups } = require('./popup-model');

module.exports = payload => Popups.create(payload);
