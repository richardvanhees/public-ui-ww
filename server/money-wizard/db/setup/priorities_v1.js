/* eslint-disable */
export const priorities = {
  version: 1,
  priorities: [
    {
      link_header: 'Help to build up savings',
      list: [],
      long_description:
        'Saving is about putting money aside either for a specific purpose like a holiday or to have money available for unexpected costs such as your car breaking down.  Savings are usually held in cash based products such as an easy access savings account with a bank or building society. This means you can access your money quickly if needed.',
      short_description:
        'Putting aside some money every month to help you pay for unexpected costs or treats.',
      alternative_descriptions: [],
      image: 'q8a0',
      name: 'Building up savings',
      id: 0,
    },
    {
      link_header: 'Help to invest money for the long term',
      list: [],
      long_description:
        'Investing is all about looking for ways to make your savings grow more over a long period of time – usually 5 years or more. This might include investing in stocks and shares, property or investment funds. There are risks involved with investing so getting advice is recommended.',
      short_description:
        'Once you’ve built up some savings, you can look for better returns on your money to help it grow over time.',
      alternative_descriptions: [],
      image: 'q4a2',
      name: 'Investing money for the long term',
      id: 1,
    },
    {
      link_header: 'Help to protect from the unexpected',
      list: [],
      long_description:
        'Protecting yourself from the unexpected is important. Even though it’s not pleasant to think about, making the right decisions now could make events such as redundancy, illness or death less traumatic for you and your family. First, you should understand the level of cover offered by your employer, you may already be entitled to some form of protection such as sick pay, healthcare or life cover.',
      short_description:
        'Making sure you and your family are financially protected in the event of illness, unemployment or death.',
      alternative_descriptions: [],
      image: 'q4a0',
      name: 'Protecting from the unexpected',
      id: 2,
    },
    {
      link_header: 'Help to save into pensions',
      list: [],
      long_description:
        'Saving into a pension is about regularly saving a part of your salary so that you can get an income when you retire.  You will get tax relief on what you pay into your pension, and your employer will normally pay in too. This ‘free money’ is worth making the most of.  Starting early is always a good idea when it comes to saving for retirement.',
      short_description:
        'Regularly putting aside some money into a pension to give you an income when you retire.',
      alternative_descriptions: [],
      image: 'q7a0',
      name: 'Saving into pensions',
      id: 3,
    },
    {
      link_header: 'Help to prepare for retirement',
      list: [],
      long_description:
        'As you approach retirement, there are many ways of getting your finances sorted ready for your new way of life. Pensions are now more flexible than you might think so understanding your options is vital. Financial advice can help you make the most suitable decision for you and your finances.',
      short_description:
        'When you’re close to retirement, working out the best way of providing an income when you retire.',
      alternative_descriptions: [],
      image: 'q7a0',
      name: 'Preparing for retirement',
      id: 4,
    },
    {
      link_header: 'Help to budget',
      list: [],
      long_description:
        'Some people have difficulty managing their day to day spending. Feeling in control of what you spend might free up some money to spend on other opportunities. The best place to start is by using a budgeting tool to break down your spending to look at exactly where your money is going and where you might be able to make savings.',
      short_description:
        'Keeping a track of your spending and making sure this is less than you earn.',
      alternative_descriptions: [],
      image: 'q3a2',
      name: 'Managing regular spending',
      id: 5,
    },
    {
      link_header: 'Help to reduce debt',
      list: [],
      long_description:
        'Most people have some form of debt. Having lots of high interest or unaffordable debt can be worrying. Debts can get out of control if they’re not managed properly and getting behind on your repayments could damage your credit rating. This could affect your ability to borrow in the future. If you’re in this situation, you should seek help.',
      short_description:
        'Recognising your debts so that you can pay them off over time.',
      alternative_descriptions: [],
      image: 'q6a3',
      name: 'Reducing debt',
      id: 6,
    },
    {
      link_header: 'Help to buy a home',
      list: [],
      long_description:
        "Buying a home can be one of the most exciting times in a person's life, but getting onto the property ladder is not always easy.  Equally, selling a home and finding a new property has its own challenges.  There are many steps involved, so careful research and planning is needed.",
      short_description:
        'Getting your mind and money ready for buying a new home.',
      alternative_descriptions: [],
      image: 'q5a1',
      name: 'Buying a home',
      id: 7,
    },
  ],
};
