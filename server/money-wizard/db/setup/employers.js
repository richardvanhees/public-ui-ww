/* eslint-disable */
const employers = [
  {
    employer: 'ww',
    employerHash: 'ad57484016654da87125db86f4227ea3',
    mailTemplate: 'Money Wizard (Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'instant',
    employerHash: 'bcbf4d67f34365b2658b63de6db82638',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'Visa',
    employerHash: '89fc0d6fe12b0e0c1af5c7a0373435a6',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'SouthEastWater',
    employerHash: 'a4fa2482a5bc8755a5bc4c1f610715af',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'HastingsDirect',
    employerHash: '7257ea19eb3102e446431a554088ad04',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'UniversityOfCambridge',
    employerHash: '7be6db8f4728bb78b998462015ab4567',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
  {
    employer: 'Caesars',
    employerHash: '5d86f4450a172ea29dc615d2df74a8f1',
    mailTemplate: 'Money Wizard (non Pension Wizard user)',
    tipsLinks: [
      {
        id: 0,
        links: [
          {
            label: 'Save or invest?',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/should-i-save-or-invest',
          },
          {
            label: 'Savings calculator',
            url:
              'https://www.moneyadviceservice.org.uk/en/tools/savings-calculator',
          },
        ],
      },
      {
        id: 1,
        links: [
          {
            label: 'Guide to investing',
            url:
              'https://www.moneyadviceservice.org.uk/en/articles/investing-beginners-guide',
          },
        ],
      },
      {
        id: 2,
        links: [
          {
            label: 'Making a will',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/making-a-will',
          },
          {
            label: 'Guide to financial protection',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/life-and-protection-insurance',
          },
        ],
      },
      {
        id: 3,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 4,
        links: [
          {
            label: 'Pension Wizard',
            url: 'https://apps.wealthwizards.io/pension-wizard/',
          },
          {
            label: 'Your pension. Your future.',
            url: 'https://apps.wealthwizards.io/pension-education',
          },
        ],
      },
      {
        id: 5,
        links: [
          {
            label: 'Budgeting tool',
            url:
              'https://www.citizensadvice.org.uk/debt-and-money/budgeting/budgeting/work-out-your-budget/budgeting-tool/',
          },
        ],
      },
      {
        id: 6,
        links: [
          {
            label: 'Debts and borrowing',
            url:
              'https://www.moneyadviceservice.org.uk/en/categories/debt-and-borrowing',
          },
          {
            label: 'Debt advice',
            url: 'https://www.stepchange.org/Debtremedy.aspx',
          },
        ],
      },
      {
        id: 7,
        links: [
          {
            label: 'Buying a home',
            url:
              'http://help.wootric.com/email-surveys/how-to-send-email-surveys-using-your-preferred-email-platform',
          },
        ],
      },
    ],
  },
];
