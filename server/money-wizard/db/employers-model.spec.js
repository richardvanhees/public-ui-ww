const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const timekeeper = require('timekeeper');

test('Employer model', t => {
  t.test('creates a new mongoose schema', assert => {
    assert.plan(12);

    timekeeper.freeze(new Date());

    function fakeSchema(schema, options) {
      assert.deepEqual(schema.employer, {
        type: String,
        required: true,
        index: true,
        unique: true,
      });
      assert.deepEqual(schema.employerHash, {
        type: String,
        required: true,
        index: true,
      });
      assert.deepEqual(schema.mailTemplate, { type: String, required: true });
      assert.deepEqual(schema.prioritiesVersion, {
        type: Number,
        required: true,
      });
      assert.deepEqual(schema.questionsVersion, {
        type: Number,
        required: true,
      });
      assert.deepEqual(schema.popupType, { type: Number, required: true });
      assert.deepEqual(schema.canLogin, { type: Boolean, required: true });
      assert.deepEqual(schema.canContactPAT, { type: Boolean, required: true });
      assert.deepEqual(schema.disablePopup, { type: Boolean, required: false, default: false });
      assert.deepEqual(schema.disableEmail, { type: Boolean, required: false, default: false });

      assert.deepEqual(options, {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
      });
    }

    const target = proxyquire('./employers-model', {
      mongoose: {
        Schema: fakeSchema,
        SchemaTypes: {},
        model(name, schema, collection) {
          return `${name}:${schema.constructor.name}:${collection}`;
        },
      },
    });

    assert.equal(target.employersModel, 'Employers:fakeSchema:employers');

    timekeeper.reset();
    assert.end();
  });
});
