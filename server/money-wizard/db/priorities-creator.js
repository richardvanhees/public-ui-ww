const { prioritiesModel: Priorities } = require('./priorities-model');

module.exports = payload => Priorities.create(payload);
