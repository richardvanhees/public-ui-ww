require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const popupSchemaDefinition = {
  type: { type: Number, required: true, unique: true },
  image: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  callToAction: { type: String, required: true },
  cancel: { type: String, required: true },
  firstOptInLabel: { type: String, required: true },
};

const popupSchema = new mongoose.Schema(popupSchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  popupSchemaDefinition,
  popupModel: mongoose.model('Popups', popupSchema, 'popups'),
};
