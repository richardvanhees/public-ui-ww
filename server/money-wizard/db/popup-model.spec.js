const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const timekeeper = require('timekeeper');

test('Popup model', t => {
  t.test('Creates a new mongoose schema for Popups', assert => {
    assert.plan(8);

    timekeeper.freeze(new Date());

    function fakeSchema(schema, options) {
      assert.deepEqual(
        schema.type,
        { type: Number, required: true, unique: true },
        'type'
      );
      assert.deepEqual(schema.image, { type: String, required: true }, 'image');
      assert.deepEqual(schema.title, { type: String, required: true }, 'title');
      assert.deepEqual(
        schema.description,
        { type: String, required: true },
        'description'
      );
      assert.deepEqual(
        schema.callToAction,
        { type: String, required: true },
        'callToAction'
      );
      assert.deepEqual(
        schema.cancel,
        { type: String, required: true },
        'cancel'
      );
      assert.deepEqual(
        options,
        { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
        'options'
      );
    }

    const target = proxyquire('./popup-model', {
      mongoose: {
        Schema: fakeSchema,
        SchemaTypes: {},
        model(name, schema, collection) {
          return `${name}:${schema.constructor.name}:${collection}`;
        },
      },
    });

    assert.equal(
      target.popupModel,
      'Popups:fakeSchema:popups',
      'popupModel correct'
    );

    timekeeper.reset();
  });
});
