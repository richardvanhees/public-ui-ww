const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('Tiplink creator', t => {
  t.test('creates a new tiplink', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeClient = {
      id: 0,
      url: 'url',
      caption: 'caption',
      emailDescriptionStart: 'emailDescriptionStart',
      emailDescriptionEnd: 'emailDescriptionEnd'
    };
    const expected = {
      id: 0,
      url: 'url',
      caption: 'caption',
      emailDescriptionStart: 'emailDescriptionStart',
      emailDescriptionEnd: 'emailDescriptionEnd'
    };

    const target = proxyquire('./tip-link-creator', {
      './tip-link-model': {
        tipLinkModel: {
          create: fakeDocument => {
            t.deepEqual(fakeDocument, expected, 'create called');
          },
        },
      },
    });

    target(fakeClient);
  });
});
