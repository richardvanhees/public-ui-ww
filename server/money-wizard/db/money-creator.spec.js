const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('money creator', t => {
  t.test('creates a new change model', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeClient = {
      name: fakeName,
    };
    const expected = {
      name: fakeName,
    };

    const target = proxyquire('./money-creator', {
      './money-model': {
        moneyModel: {
          create: fakeDocument => {
            t.deepEqual(fakeDocument, expected, 'create called');
          },
        },
      },
    });

    target(fakeClient);
  });
});
