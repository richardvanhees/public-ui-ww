require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const questionsSchemaDefinition = {
  questions: { type: Array, required: true },
  version: { type: Number, index: true, unique: true, required: true },
};

const questionsSchema = new mongoose.Schema(questionsSchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  questionsSchemaDefinition,
  questionsModel: mongoose.model('Questions', questionsSchema, 'questions'),
};
