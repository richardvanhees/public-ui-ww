require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const moneySchemaDefinition = {
  email: { type: String, required: true, index: true },
  employer: { type: String, required: true, index: true },
  questions: { type: Array, required: true },
  consentedPrivacyPolicy: { type: Boolean, required: true },
  wwFinancialPriorities: { type: Array, required: true },
  userFinancialPriorities: { type: Array, required: true },
  questionVersion: { type: String, required: true },
  emailSent: { type: Boolean, required: true },
  template: { type: String, required: true },
  popupType: { type: Number },
  optInEmail: { type: String },
  optIn: { type: Boolean },
  prizeDrawOptIns: { type: Array },
  password: { type: String, required: true },
};

const moneySchema = new mongoose.Schema(moneySchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  moneySchemaDefinition,
  moneyModel: mongoose.model('MoneyUsers', moneySchema, 'moneyUsers'),
};
