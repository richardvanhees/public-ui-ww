const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const timekeeper = require('timekeeper');

test('Tiplink model', t => {
  t.test('Creates a new mongoose schema for Tiplinks', assert => {
    assert.plan(7);

    timekeeper.freeze(new Date());

    function fakeSchema(schema, options) {
      assert.deepEqual(schema.id, { type: Number, required: true, unique: true }, 'id');
      assert.deepEqual(schema.url, { type: String, required: true }, 'url');
      assert.deepEqual(schema.caption, { type: String, required: true }, 'caption');
      assert.deepEqual(schema.emailDescriptionStart, { type: String }, 'emailDescriptionStart');
      assert.deepEqual(schema.emailDescriptionEnd, { type: String }, 'emailDescriptionEnd');
      assert.deepEqual(schema.emailCaption, { type: String, required: true }, 'emailCaption');
    }

    const target = proxyquire('./tip-link-model', {
      mongoose: {
        Schema: fakeSchema,
        SchemaTypes: {},
        model(name, schema, collection) {
          return `${name}:${schema.constructor.name}:${collection}`;
        },
      },
    });

    assert.equal(
      target.tipLinkModel,
      'Tiplinks:fakeSchema:tiplinks',
      'tiplinkModel correct'
    );

    timekeeper.reset();
  });
});
