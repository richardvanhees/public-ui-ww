const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('questions creator', t => {
  t.test('creates a new questions model', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeClient = {
      name: fakeName,
    };
    const expected = {
      name: fakeName,
    };

    const target = proxyquire('./questions-creator', {
      './questions-model': {
        questionsModel: {
          create: fakeDocument => {
            t.deepEqual(fakeDocument, expected, 'create called');
          },
        },
      },
    });

    target(fakeClient);
  });
});
