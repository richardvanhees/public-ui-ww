const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('Popup creator', t => {
  t.test('creates a new popup model', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeClient = {
      name: fakeName,
    };
    const expected = {
      name: fakeName,
    };

    const target = proxyquire('./popup-creator', {
      './popup-model': {
        popupModel: {
          create: fakeDocument => {
            t.deepEqual(fakeDocument, expected, 'create called');
          },
        },
      },
    });

    target(fakeClient);
  });
});
