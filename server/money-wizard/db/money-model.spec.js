const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const timekeeper = require('timekeeper');

test('Money model', t => {
  t.test('creates a new mongoose schema', assert => {
    assert.plan(9);

    timekeeper.freeze(new Date());

    function fakeSchema(schema, options) {
      assert.deepEqual(schema.consentedPrivacyPolicy, {
        type: Boolean,
        required: true,
      });
      assert.deepEqual(schema.email, {
        type: String,
        required: true,
        index: true,
      });
      assert.deepEqual(schema.employer, {
        type: String,
        required: true,
        index: true,
      });
      assert.deepEqual(schema.questions, { type: Array, required: true });
      assert.deepEqual(schema.wwFinancialPriorities, {
        type: Array,
        required: true,
      });
      assert.deepEqual(schema.userFinancialPriorities, {
        type: Array,
        required: true,
      });
      assert.deepEqual(schema.password, {
        type: String,
        required: true,
      });
      assert.deepEqual(options, {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
      });
    }

    const target = proxyquire('./money-model', {
      mongoose: {
        Schema: fakeSchema,
        SchemaTypes: {},
        model(name, schema, collection) {
          return `${name}:${schema.constructor.name}:${collection}`;
        },
      },
    });

    assert.equal(target.moneyModel, 'MoneyUsers:fakeSchema:moneyUsers');

    timekeeper.reset();
  });
});
