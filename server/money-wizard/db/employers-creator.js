const { employersModel: Employers } = require('./employers-model');

module.exports = payload => Employers.create(payload);
