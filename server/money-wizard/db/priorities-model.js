require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const prioritiesSchemaDefinition = {
  priorities: { type: Array, required: true },
  version: { type: Number, index: true, unique: true, required: true },
};

const prioritiesSchema = new mongoose.Schema(prioritiesSchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  prioritiesSchemaDefinition,
  prioritiesModel: mongoose.model('Priorities', prioritiesSchema, 'priorities'),
};
