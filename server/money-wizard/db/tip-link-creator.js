const { tipLinkModel: Tiplinks } = require('./tip-link-model');

module.exports = payload => Tiplinks.create(payload);
