require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const tipLinkSchemaDefinition = {
  id: { type: Number, required: true, unique: true },
  url: { type: String, required: true },
  caption: { type: String, required: true },
  emailDescriptionStart: { type: String },
  emailDescriptionEnd: { type: String },
  emailCaption: { type: String, required: true },
};

const tipLinkSchema = new mongoose.Schema(tipLinkSchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  tipLinkSchemaDefinition,
  tipLinkModel: mongoose.model('Tiplinks', tipLinkSchema, 'tiplinks'),
};
