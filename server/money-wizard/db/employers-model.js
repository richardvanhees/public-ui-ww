require('mongoose-type-email');
const mongoose = require('mongoose');

mongoose.Promise = Promise;

const employersSchemaDefinition = {
  employer: { type: String, required: true, unique: true, index: true },
  employerHash: { type: String, required: true, index: true },
  mailTemplate: { type: String, required: true },
  // tipsLinks: { type: Array, required: false }, // default tipsLinks have been moved to priorities => Only bespokeLinks in employer model
  bespokeLinks: { type: Array, required: false },
  questionsVersion: { type: Number, required: true },
  prioritiesVersion: { type: Number, required: true },
  popupType: { type: Number, required: true },
  prizeDrawEnabled: { type: Boolean, required: false, default: false },
  prizeDrawLabel: { type: String, required: false },
  prizeDrawStartDate: { type: Date, required: false },
  prizeDrawEndDate: { type: Date, required: false },
  mailchimpListName: { type: String, required: false },
  prizesText: { type: String, required: false },
  canLogin: { type: Boolean, required: true },
  canContactPAT: { type: Boolean, required: true },
  showMW2BetaLink: { type: Boolean, required: false, default: false },
  disablePopup: { type: Boolean, required: false, default: false },
  disableEmail: { type: Boolean, required: false, default: false },
};

const employersSchema = new mongoose.Schema(employersSchemaDefinition, {
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
});

module.exports = {
  employersSchemaDefinition,
  employersModel: mongoose.model('Employers', employersSchema, 'employers'),
};
