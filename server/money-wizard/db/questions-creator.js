const { questionsModel: Questions } = require('./questions-model');

module.exports = payload => Questions.create(payload);
