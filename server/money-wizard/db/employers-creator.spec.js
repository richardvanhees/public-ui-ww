const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();

test('employer creator', t => {
  t.test('creates a new employers model', t => {
    t.plan(1);

    const fakeName = 'fake name';

    const fakeClient = {
      name: fakeName,
    };
    const expected = {
      name: fakeName,
    };

    const target = proxyquire('./employers-creator', {
      './employers-model': {
        employersModel: {
          create: fakeDocument => {
            t.deepEqual(fakeDocument, expected, 'create called');
          },
        },
      },
    });

    target(fakeClient);
  });
});
