const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const timekeeper = require('timekeeper');

test('Question model', t => {
  t.test('Creates a new mongoose schema for Questions', assert => {
    assert.plan(4);

    timekeeper.freeze(new Date());

    function fakeSchema(schema, options) {
      assert.deepEqual(schema.questions, { type: Array, required: true });
      assert.deepEqual(schema.version, {
        type: Number,
        required: true,
        unique: true,
        index: true,
      });
      assert.deepEqual(options, {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
      });
    }

    const target = proxyquire('./questions-model', {
      mongoose: {
        Schema: fakeSchema,
        SchemaTypes: {},
        model(name, schema, collection) {
          return `${name}:${schema.constructor.name}:${collection}`;
        },
      },
    });

    assert.equal(target.questionsModel, 'Questions:fakeSchema:questions');

    timekeeper.reset();
  });
});
