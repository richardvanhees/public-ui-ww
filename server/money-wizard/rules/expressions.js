const nameExp = /^[A-Za-z\-"'" ]*$/;
const dobExp = /^\d\d\d\d-\d\d-\d\d$/;
const genderExp = /^(m|f)$/;
const niExp = /^[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFM]{0,1}$/;

/**
 * Postcode Regexp came from this government document:
 * https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/488478/Bulk_Data_Transfer_-_additional_validation_valid_from_12_November_2015.pdf
 */
// eslint-disable-next-line max-len
const postcodeExp = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/;

module.exports = {
  nameExp,
  dobExp,
  genderExp,
  niExp,
  postcodeExp,
};
