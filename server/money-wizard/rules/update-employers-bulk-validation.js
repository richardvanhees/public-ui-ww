const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.array().items(
  ExtendedJoi.object().keys({
    employers: ExtendedJoi.array().required(),
    data: ExtendedJoi.object().required(),
    I_WANT_TO_UPDATE_ALL_EMPLOYERS: ExtendedJoi.bool(),
  })
);

module.exports = createValidationRules;
