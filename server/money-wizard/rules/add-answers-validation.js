const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  consentedPrivacyPolicy: ExtendedJoi.any().required(),
  emailSent: ExtendedJoi.any().required(),
  email: ExtendedJoi.string().required(),
  employer: ExtendedJoi.string().required(),
  questions: ExtendedJoi.any().required(),
  template: ExtendedJoi.string().required(),
  wwFinancialPriorities: ExtendedJoi.any().required(),
  userFinancialPriorities: ExtendedJoi.any().required(),
  questionVersion: ExtendedJoi.number().required(),
  popupType: ExtendedJoi.number(),
  optInEmail: ExtendedJoi.string(),
  optIn: ExtendedJoi.bool(),
  password: ExtendedJoi.string(),
});

module.exports = createValidationRules;
