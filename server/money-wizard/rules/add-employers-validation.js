const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  employers: Joi.array().items(
    Joi.object().keys({
      employer: ExtendedJoi.string().required(),
      employerHash: ExtendedJoi.string().required(),
      tipsLinks: ExtendedJoi.any(), // default tipsLinks have been moved to priorities => Only bespokeLinks in employer model
      bespokeLinks: ExtendedJoi.array()
        .items(
          ExtendedJoi.object().keys({
            url: ExtendedJoi.string(),
            type: ExtendedJoi.string(),
            content: ExtendedJoi.string(),
            caption: ExtendedJoi.string(),
            emailDescriptionStart: ExtendedJoi.string(),
            emailCaption: ExtendedJoi.string(),
            emailDescriptionEnd: ExtendedJoi.string(),
            priorityId: ExtendedJoi.number().required(),
            includeDefault: ExtendedJoi.bool(),
          })
        )
        .required(),
      mailTemplate: ExtendedJoi.string().required(),
      questionsVersion: ExtendedJoi.number().required(),
      prioritiesVersion: ExtendedJoi.number().required(),
      popupType: ExtendedJoi.number().required(),

      prizesText: ExtendedJoi.string(),
      mailchimpListName: ExtendedJoi.string(),
      prizeDrawEndDate: ExtendedJoi.string(),
      prizeDrawStartDate: ExtendedJoi.string(),
      prizeDrawEnabled: ExtendedJoi.bool(),
      secondOptInEndDate: ExtendedJoi.string(),
      secondOptInStartDate: ExtendedJoi.string(),
      secondOptInLabel: ExtendedJoi.string(),
      secondOptInEnabled: ExtendedJoi.bool(),

      // Feature flags
      canLogin: ExtendedJoi.bool().required(),
      canContactPAT: ExtendedJoi.bool().required(),
      showMW2BetaLink: ExtendedJoi.bool(),
    })
  ),
});

module.exports = createValidationRules;
