const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  email: ExtendedJoi.string().required(),
  password: ExtendedJoi.string().required(),
});

module.exports = createValidationRules;
