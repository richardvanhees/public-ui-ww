const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  tiplinks: Joi.array()
    .max(50)
    .items(
      Joi.object().keys({
        id: ExtendedJoi.number().required(),
        url: ExtendedJoi.string().required(),
        caption: ExtendedJoi.string().required(),
        emailDescriptionStart: ExtendedJoi.string(),
        emailDescriptionEnd: ExtendedJoi.string(),
        emailCaption: ExtendedJoi.string().required(),
      })
    ),
});

module.exports = createValidationRules;

