const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);
const popupSchema = require('./update-popups-validation');

const createValidationRules = ExtendedJoi.object().keys({
  popups: Joi.array()
    .max(50)
    .items(popupSchema),
  version: ExtendedJoi.number().required(),
});

module.exports = createValidationRules;
