const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  tipsLinks: ExtendedJoi.array(),
  bespokeLinks: ExtendedJoi.array().items(
    ExtendedJoi.object().keys({
      url: ExtendedJoi.string().required(),
      caption: ExtendedJoi.string().required(),
      emailDescriptionStart: ExtendedJoi.string(),
      emailCaption: ExtendedJoi.string().required(),
      emailDescriptionEnd: ExtendedJoi.string(),
      priorityId: ExtendedJoi.number().required(),
      includeDefault: ExtendedJoi.bool(),
    })
  ),
  mailTemplate: ExtendedJoi.string(),
  questionsVersion: ExtendedJoi.number(),
  prioritiesVersion: ExtendedJoi.number(),
  popupType: ExtendedJoi.number(),
  optInEmail: ExtendedJoi.string(),
  optIn: ExtendedJoi.bool(),
  prizeDrawLabel: ExtendedJoi.string(),
  prizeDrawEnabled: ExtendedJoi.boolean(),
  prizeDrawStartDate: ExtendedJoi.date(),
  prizeDrawEndDate: ExtendedJoi.date(),
  mailchimpListName: ExtendedJoi.string(),
  prizesText: ExtendedJoi.string(),

  // Feature flags
  canLogin: ExtendedJoi.bool(),
  canContactPAT: ExtendedJoi.bool(),
  showMW2BetaLink: ExtendedJoi.bool(),
});

module.exports = createValidationRules;
