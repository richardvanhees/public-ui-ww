const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  priorities: ExtendedJoi.any().required(),
  version: ExtendedJoi.number().required(),
});

module.exports = createValidationRules;
