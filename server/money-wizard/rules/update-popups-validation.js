const Joi = require('joi');
const Extensions = require('joi-date-extensions');
const ExtendedJoi = Joi.extend(Extensions);

const createValidationRules = ExtendedJoi.object().keys({
  image: ExtendedJoi.string(),
  title: ExtendedJoi.string(),
  description: ExtendedJoi.string(),
  callToAction: ExtendedJoi.string(),
  cancel: ExtendedJoi.string(),
  type: ExtendedJoi.number(),
  firstOptInLabel: ExtendedJoi.string(),
});

module.exports = createValidationRules;
