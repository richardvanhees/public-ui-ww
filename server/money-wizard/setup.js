const mongoose = require('mongoose');
mongoose.Promise = Promise;
const { env } = require('../../config');
const { MONGODB_URL } = env;

const setup = () => mongoose.connect(MONGODB_URL, { useMongoClient: true });

const teardown = () => mongoose.disconnect();

module.exports = {
  setup,
  teardown,
};
