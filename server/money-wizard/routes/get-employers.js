const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { employersModel: Employers } = require('../db/employers-model');

module.exports = (req, res, next) => {
  co(function* getEmployers() {
    // eslint-disable-line
    const data = yield Employers.find({})
      .lean()
      .exec();

    return res.status(201).send(data); // eslint-disable-line
  }).catch(err => {
    req.log.error(err);
    next(
      new ApiError(
        'Internal server error - failed to get employers',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
