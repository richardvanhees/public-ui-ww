const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');

test('error getting questions', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  const encryptStub = sandbox.stub();
  const decryptStub = sandbox.stub();
  const reqStub = sandbox.stub();
  const errorStub = sandbox.stub();
  reqStub.log = {
    error: errorStub,
  };
  const resStub = sandbox.stub();
  const nextStub = sandbox.stub();
  const findStub = sandbox.stub();
  findStub.rejects({});

  const Questions = proxyquire('./get-questions', {
    'ww-utils': {
      ErrorCodes: { INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR' },
      ApiError: function() {
        apiErrorStub;
      },
    },
    '../db/questions-model': { find: findStub },
  });

  Questions(reqStub, resStub, nextStub);

  setImmediate(() => {
    t.equals(errorStub.callCount, 1);
    t.deepEquals(errorStub.args, [[{}]]);
    t.end();
  });
});
