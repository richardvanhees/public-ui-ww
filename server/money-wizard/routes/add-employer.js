const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const employerCreator = require('../db/employers-creator');

module.exports = (req, res, next) => {
  co(function* addEmployer() {
    // eslint-disable-line
    for (let i = 0; i < req.body.employers.length; i++) {
      const employer = req.body.employers[i];
      yield employerCreator(employer); // eslint-disable-line
    }

    return res.json({
      success: true,
      status: 201,
    });
  }).catch(err => {
    next(
      new ApiError(
        `Internal server error - failed to add employer(s) ${err}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
