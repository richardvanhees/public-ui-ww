const { ErrorCodes, ApiError } = require('ww-utils');
const { employersModel: Employers } = require('../db/employers-model');

/* eslint-disable consistent-return */
module.exports = async (req, res, next) => {
  try {
    for (let i = 0; i < req.body.length; i++) {
      const batch = req.body[i];

      for (let n = 0; n < batch.employers.length; n++) {
        const employer = batch.employers[n];

        if (employer === 'allEmployers') {
          if (batch.I_WANT_TO_UPDATE_ALL_EMPLOYERS) {
            await Employers.update({}, batch.data, { multi: true });
          } else {
            next(new ApiError(
              'Please add the key "I_WANT_TO_UPDATE_ALL_EMPLOYERS": true to the batch to confirm the request',
              ErrorCodes.FORBIDDEN,
              400
            ));
            return false;
          }
        } else {
          await Employers.findOneAndUpdate({ employer }, batch.data)
            .lean()
            .exec();
        }
      }
    }

    return res.status(200).send(`The selected employers have been updated`); // eslint-disable-line
  } catch (e) {
    next(
      new ApiError(
        'Internal server error - failed to update employers',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
