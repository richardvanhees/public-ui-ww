const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { employersModel: Employers } = require('../db/employers-model');

module.exports = (req, res, next) => {
  co(function* checkEmployerHash() {
    // eslint-disable-line
    const data = yield Employers.find({
      employerHash: req.params.hash,
    })
      .lean()
      .exec();

    return res.status(201).send(data); // eslint-disable-line
  }).catch(err => {
    req.log.error(err);
    next(
      new ApiError(
        'Internal server error - failed to check employer hash',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
