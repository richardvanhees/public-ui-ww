const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');
const questions = require('../db/setup/questions_v1').questions;

test('score generation', t => {
  t.test('returns score', t => {
      const sandbox = sinon.sandbox.create();
    const reqStub = sandbox.stub();
    const resStub = sandbox.stub();
    const setStub = sandbox.stub();
    const statusStub = sandbox.stub();
    statusStub.send = (result) => {
      t.deepEquals(result, {score: 10}, 'correct response');
      return true;
    };
    const statusReturnStub = sandbox.stub();
    const sendStub = sandbox.stub();
    resStub.set = setStub;
    resStub.status = sandbox.stub();
    resStub.status.returns(statusStub);

    statusStub.withArgs(200).returns(statusReturnStub);

    const errorStub = sandbox.stub();
    reqStub.body = {
      "questionsVersion": 1,
      "userInput": {
        "0": {"answerId": 0 },
        "1": {"answerId": 2 },
        "2": {"answerId": 3 },
        "3": {"answerId": 1 },
        "4": {"answerId": 2 },
        "5": {"answerId": 0 },
        "6": {"answerId": 1 },
        "7": {"answerId": 1 },
        "8": {"answerId": 1 }
      }
    };
    const nextStub = sandbox.stub();

    const questionsFindStub = sandbox.stub();
    const questionsLeanStub = sandbox.stub();
    const questionsExecStub = sandbox.stub();
    questionsFindStub.withArgs({
      version: 1
    }).returns({ lean: questionsLeanStub });
    questionsLeanStub.returns({ exec: questionsExecStub });
    questionsExecStub.returns(questions);

    const generateScore = proxyquire('./generate-score', {
      '../db/questions-model': {
        questionsModel: {
          findOne: questionsFindStub,
        },
      },
      '../utils/calculate-score': () => (10),
    });

    const response = generateScore(reqStub, resStub, nextStub);

    t.end();
  });
});
