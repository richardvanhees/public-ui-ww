const { ErrorCodes, ApiError, encryption } = require('ww-utils');
const { moneyModel: MoneyUsers } = require('../db/money-model');
const { questionsModel: Questions } = require('../db/questions-model');
const { prioritiesModel: Priorities } = require('../db/priorities-model');
const json2csv = require('json2csv');
const { encryptString, decryptString } = encryption;
const { env } = require('../../../config');

module.exports = async (req, res, next) => {
  try {
    const fields = ['employer', 'created_at'];
    const fieldNames = ['Employer', 'Created'];

    if (req.query.includeEmail) {
      fields.push('email');
      fieldNames.push('Email');
    }

    const questionSets = await Questions.find({})
      .lean()
      .exec();

    const prioritiesSets = await Priorities.find({})
      .lean()
      .exec();

    const questions = {};
    const needs = {};
    let fieldCounter = 0;

    for (const questionSet of questionSets) {
      for (const q of questionSet.questions) {
        questions[q.id] = q;
        fieldNames.push(q.question);
        fields.push(`${fieldCounter++}`);
      }
      break;
    }

    fieldNames.push('Priority 1');
    fieldNames.push('Priority 2');
    fieldNames.push('Priority 3');
    fields.push('Priority 1');
    fields.push('Priority 2');
    fields.push('Priority 3');

    const users = await MoneyUsers.find({
      employer: req.params.employer,
      email: {
        $not: new RegExp(
          encryptString(
            'demo@wealthwizards.com',
            env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
            env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
          )
        ),
      },
    })
      .lean()
      .exec();

    const transformedData = [];

    for (const d of users) {
      // we now have question sets with versions
      const questionsForVersion = questionSets.find(
        q => q.version === parseInt(d.questionVersion, 10)
      );

      const td = {};
      td.created_at = d.created_at;
      td.employer = d.employer;

      if (req.query.includeEmail) {
        td.email = decryptString(
          d.email,
          env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
          env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
        );
      }

      const theirPriorities = d.userFinancialPriorities[0];

      // legacy support
      const theirLegacyCompatiblePriorities = theirPriorities.priorities
        ? theirPriorities.priorities
        : d.userFinancialPriorities;

      for (
        let index = 0;
        index < theirLegacyCompatiblePriorities.length;
        index++
      ) {
        const p = theirLegacyCompatiblePriorities[index];
        if (index === 0) {
          td['Priority 1'] = p.name;
        }
        if (index === 1) {
          td['Priority 2'] = p.name;
        }
        if (index === 2) {
          td['Priority 3'] = p.name;
        }
        needs[index] = p.name;
        td[`Need ${p.id}`] = p.name;
        td[`Need ${p.id} - them`] = index + 1;
      }

      const ourPriorities = d.wwFinancialPriorities[0];

      // legacy support
      const ourLegacyCompatiblePriorities = ourPriorities.priorities
        ? ourPriorities.priorities
        : d.wwFinancialPriorities;

      for (
        let index = 0;
        index < ourLegacyCompatiblePriorities.length;
        index++
      ) {
        const p = ourLegacyCompatiblePriorities[index];
        td[`Need ${p.id} - us`] = index + 1;
      }

      if (!d.questions || !d.questions.length) {
        continue;
      }

      // legacy support
      const q = d.questions[0].userInput || d.questions[0].questions;

      for (const id of Object.keys(q)) {
        const answerId = q[id].answerId;
        td[id] = questionsForVersion.questions[id].answers[answerId].answer;
      }

      transformedData.push(td);
    }

    // we take the last set of priorities as the ids havent changed
    const sortedPriorities = prioritiesSets[
      prioritiesSets.length - 1
    ].priorities.sort((x, y) => (x.id > y.id ? 1 : 0));

    for (const priority of sortedPriorities) {
      if (!fields.includes(`Need ${priority.id} - them`)) {
        fields.push(`Need ${priority.id} - us`);
        fields.push(`Need ${priority.id} - them`);
        fieldNames.push(`${priority.name} - default`);
        fieldNames.push(`${priority.name} - adjusted`);
      }
    }

    if (req.params.csv === 'downloadCSV') {
      try {
        const csv = json2csv({ data: transformedData, fields, fieldNames });
        res.setHeader('Content-disposition', 'attachment; filename=data.csv');
        res.set('Content-Type', 'text/csv');
        return res.status(200).send(csv);
      } catch (err) {
        return next(
          new ApiError(
            'Internal server error - failed to create csv',
            ErrorCodes.INTERNAL_SERVER_ERROR,
            500
          )
        );
      }
    } else {
      return res.status(201).send({
        original: users,
        transformed: transformedData,
      });
    }
  } catch (e) {
    return next(
      new ApiError(
        'Internal server error - failed to get answers: ',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
