const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const encryption = require('ww-utils').encryption;

// Setup
const sandbox = sinon.sandbox.create();
const apiErrorStub = sandbox.stub();
const fakeReq = {
  ...sandbox.stub(),
  params: {
    employer: 'fakeEmployer'
  },
  body: {
    email: 'fakeEmail',
    password: 'fakePassword'
  }
};
const fakeRes = sandbox.stub();
fakeRes.status = sandbox.stub();
const fakeSend = sandbox.stub();
const fakeNext = sandbox.stub();
const fakeFindOne = sandbox.stub();
const fakeFindOneAndRemove = sandbox.stub();
const fakeLean = sandbox.stub();
const fakeExec = sandbox.stub();

fakeRes.status.returns({ send: fakeSend });
fakeFindOne.returns({ lean: fakeLean });
fakeLean.returns({ exec: fakeExec });

const target = proxyquire('./delete-answers', {
  'ww-utils': {
    ErrorCodes: {
      INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR'
    },
    ApiError: apiErrorStub,
    encryption,
  },
  '../../db/money-model': {
    moneyModel: {
      findOneAndRemove: fakeFindOneAndRemove
    }
  },
  '../../../../config': {
    env: {
      UPDATED_MW_PII_DATA_ENCRYPTION_KEY: "12345678901234567890123456789012",
      UPDATED_MW_PII_DATA_ENCRYPTION_IV: "1234567890123456",
    },
  },
});

// Tests

test('delete - success', t => {
  fakeFindOneAndRemove.returns('fakeUser');

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeSend.args, [['User fakeEmail has been deleted']], 'Deleted success');
    sandbox.reset();
    t.end();
  })();

});

test('delete - user not found', t => {
  fakeFindOneAndRemove.returns(null);

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeSend.args, [['User fakeEmail was not found']], 'Login failed validation');
    sandbox.reset();
    t.end();
  })();

});