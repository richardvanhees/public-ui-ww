const { ErrorCodes, ApiError } = require('ww-utils');
const R = require('ramda');
const { moneyModel: MoneyUsers } = require('../../db/money-model');
const { encryptString } = require('ww-utils').encryption;
const { env } = require('../../../../config');

/* eslint-disable consistent-return */
module.exports = async (req, res, next) => {
  try {
    // eslint-disable-line
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const data = R.evolve({
      email: curriedEncrypt,
    })(req.body);

    const result = await MoneyUsers.findOneAndRemove({
      email: data.email,
    });

    result ?
      res.status(200).send(`User ${req.body.email} has been deleted`) :
      res.status(400).send(`User ${req.body.email} was not found`);
  } catch (e) {
    next(
      new ApiError(
        `Internal server error - failed to delete user ${req.body.email}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
