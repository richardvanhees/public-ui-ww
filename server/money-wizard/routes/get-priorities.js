const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { prioritiesModel: Priorities } = require('../db/priorities-model');

module.exports = (req, res, next) => {
  co(function* getPriorities() {
    // eslint-disable-line
    const data = yield Priorities.find({
      version: req.params.version,
    })
      .lean()
      .exec();

    return res.status(201).send(data); // eslint-disable-line
  }).catch(err => {
    req.log.error(err);
    next(
      new ApiError(
        'Internal server error - failed to get priorities',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
