const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const R = require('ramda');
const { encryptString } = require('ww-utils').encryption;
const { env } = require('../../../config');

const { moneyModel: MoneyUsers } = require('../db/money-model');

module.exports = (req, res, next) => {
  co(function* checkOptIn() {
    // eslint-disable-line
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const data = R.evolve({
      email: curriedEncrypt,
    })(req.body);

    const result = yield MoneyUsers.find({ optInEmail: data.email })
      .lean()
      .exec();

    if (result.length) {
      next(
        new ApiError(
          'We’ve already signed this email up!',
          ErrorCodes.RESOURCE_EXISTS,
          200
        )
      );
    }

    return res.status(200).send({
      statusCode: 200,
      message: 'Okidoki!',
    }); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to check opt in email',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
