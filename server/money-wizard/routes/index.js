const { apiKeyValidator, requestValidator } = require('ww-utils');
const { env } = require('../../../config');
const nocache = require('nocache');

const questionValidation = require('../rules/add-questions-validation');
const getQuestions = require('./get-questions');
const addQuestions = require('./add-questions');
const updateQuestions = require('./update-questions');

const employersValidation = require('../rules/add-employers-validation');
const updateEmployerValidation = require('../rules/update-employer-validation');
const updateEmployersBulkValidation = require('../rules/update-employers-bulk-validation');
const addEmployer = require('./add-employer');
const updateEmployer = require('./update-employer');
const updateEmployersBulk = require('./update-employers-bulk');
const getEmployers = require('./get-employers');

const prioritiesValidation = require('../rules/add-priorities-validation');
const getPriorities = require('./get-priorities');
const addPriorities = require('./add-priorities');
const updatePriorities = require('./update-priorities');

const answersValidation = require('../rules/add-answers-validation');
const loginValidation = require('../rules/login-validation');
const getAnswers = require('./get-answers');
const addAnswers = require('./add-answers');
const updateAnswers = require('./update-answers');
const deleteAnswers = require('./answers/delete-answers');
const login = require('./login');

const addPopupValidation = require('../rules/add-popups-validation');
const updatePopupValidation = require('../rules/update-popups-validation');
const {
  getPopups,
  addPopups,
  updatePopup,
  getSinglePopup,
} = require('./popups');

const tipLinkValidation = require('../rules/add-tip-links-validation');
const addTipLinks = require('./tiplinks/add-tip-links');
const getTipLinks = require('./tiplinks/get-tip-links');

const generatePriorities = require('./generate-priorities');
const checkEmailExists = require('./check-email');
const checkOptIn = require('./check-opt-in');
const checkEmployerExists = require('./check-employer-hash');
const sendEmail = require('./send-email');
const sendPatMail = require('./pat/send-pat-mail');
const generateScore = require('./generate-score');

module.exports = app => {
  app.get('/questions/:version', nocache(), getQuestions);

  app.post(
    '/questions',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: questionValidation }),
    addQuestions
  );

  app.put(
    '/questions/:version',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    updateQuestions
  );

  app.post('/generate-priorities', generatePriorities);
  app.post('/generate-score', generateScore);

  app.get('/priorities/:version', nocache(), getPriorities);

  app.post(
    '/priorities',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: prioritiesValidation }),
    addPriorities
  );

  app.post('/login', requestValidator({ body: loginValidation }), login);

  app.put(
    '/priorities/:version',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    updatePriorities
  );

  app.get('/employers', nocache(), getEmployers);

  app.post(
    '/employers',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: employersValidation }),
    addEmployer
  );

  app.put(
    '/employers',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: updateEmployersBulkValidation }),
    updateEmployersBulk
  );

  app.put(
    '/employers/:employer',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: updateEmployerValidation }),
    updateEmployer
  );

  app.get(
    '/answers/:employer/:csv',
    nocache(),
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY_MI_DATA),
    getAnswers
  );

  app.post(
    '/employers/:employer/answers',
    requestValidator({ body: answersValidation }),
    addAnswers
  );

  app.delete(
    '/employers/:employer/answers',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    deleteAnswers
  );

  app.put('/employers/:employer/answers', updateAnswers);

  app.get(
    '/popups',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    getPopups
  );

  app.get('/employers/:employer/popups/:popupType', getSinglePopup);

  app.post(
    '/popups',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: addPopupValidation }),
    addPopups
  );

  app.put(
    '/popups/:popupType',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: updatePopupValidation }),
    updatePopup
  );

  app.post('/check-email/', nocache(), checkEmailExists);

  app.post('/check-opt-in/', nocache(), checkOptIn);

  app.get('/check-employer-hash/:hash', nocache(), checkEmployerExists);

  app.post('/send-email', sendEmail);

  app.post('/send-pat-mail', sendPatMail);

  app.post(
    '/tip-links',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    requestValidator({ body: tipLinkValidation }),
    addTipLinks
  );

  app.get(
    '/tip-links',
    apiKeyValidator('Authorization', env.MONEY_WIZARD_API_KEY),
    getTipLinks
  );
};
