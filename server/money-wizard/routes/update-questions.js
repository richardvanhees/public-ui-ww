const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { questionsModel: Questions } = require('../db/questions-model');

module.exports = (req, res, next) => {
  co(function* updateQuestions() {
    // eslint-disable-line
    yield Questions.findOneAndUpdate({ version: req.params.version }, req.body)
      .lean()
      .exec();

    return res
      .status(201)
      .send(`Version ${req.params.version} questions updated`); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to update questions',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
