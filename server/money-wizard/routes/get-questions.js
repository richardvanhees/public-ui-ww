const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { questionsModel: Questions } = require('../db/questions-model');

module.exports = (req, res, next) => {
  co(function* getQuestions() {
    // eslint-disable-line
    const data = yield Questions.find({
      version: req.params.version,
    })
      .lean()
      .exec();

    return res.status(201).send(data); // eslint-disable-line
  }).catch(err => {
    req.log.error(err);
    next(
      new ApiError(
        'Internal server error - failed to get questions',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
