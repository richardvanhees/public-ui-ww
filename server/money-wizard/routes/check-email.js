const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { encryptString } = require('ww-utils').encryption;
const R = require('ramda');

const { env } = require('../../../config');
const { moneyModel: MoneyUsers } = require('../db/money-model');

module.exports = (req, res, next) => {
  co(function* checkEmail() {
    // eslint-disable-line
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const data = R.evolve({
      email: curriedEncrypt,
    })(req.body);

    const result = yield MoneyUsers.find(data)
      .lean()
      .exec();

    return res.status(201).send(result); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to check email',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
