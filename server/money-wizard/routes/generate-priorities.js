const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');

const { questionsModel: Questions } = require('../db/questions-model');
const { prioritiesModel: Priorities } = require('../db/priorities-model');
const calculatePriorities = require('../utils/calculate-priorities');

module.exports = (req, res, next) => {
  co(function* generatePriorities() {
    // eslint-disable-line
    const userInput = req.body.userInput;

    const questionsData = yield Questions.find({
      version: req.body.questionsVersion,
    })
      .lean()
      .exec();

    const prioritiesData = yield Priorities.find({
      version: req.body.prioritiesVersion,
    })
      .lean()
      .exec();

    const weightingOptions = prioritiesData[0].priorities;
    const questions = questionsData[0].questions;

    const priorities = calculatePriorities(
      weightingOptions,
      questions,
      userInput
    );

    return res.status(201).send(priorities); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to generate priorities',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
