const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const encryption = require('ww-utils').encryption;

// Setup
const sandbox = sinon.sandbox.create();
const fakeApiError = sandbox.stub();
const fakeReq = sandbox.stub();
const fakeRes = sandbox.stub();
fakeRes.status = sandbox.stub();
const fakeSend = sandbox.stub();
const fakeNext = sandbox.stub();
const fakeFindOneAndUpdate = sandbox.stub();
const fakeUpdate = sandbox.stub();
const fakeLean = sandbox.stub();
const fakeExec = sandbox.stub();

fakeRes.status.returns({ send: fakeSend });
fakeFindOneAndUpdate.returns({ lean: fakeLean });
fakeLean.returns({ exec: fakeExec });

const target = proxyquire('./update-employers-bulk', {
  'ww-utils': {
    ErrorCodes: {
      INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      FORBIDDEN: 'FORBIDDEN',
    },
    ApiError: fakeApiError,
    encryption,
  },
  '../db/employers-model': {
    employersModel: {
      findOneAndUpdate: fakeFindOneAndUpdate,
      update: fakeUpdate,
    }
  },
});

// Tests

test('Update employers - success', t => {
  fakeReq.body = [{
    employers: ['fakeEmployer1', 'fakeEmployer2'],
    data: { fakeKey: 'fakeValue' }
  }];
  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeExec.callCount, fakeReq.body[0].employers.length, 'Correct amount of employers have been updated');
    t.deepEquals(fakeSend.args, [['The selected employers have been updated']], 'Update success');
    sandbox.reset();
    t.end();
  })();
});

test('Update all employers - success', t => {
  fakeReq.body = [{
    employers: ['allEmployers'],
    I_WANT_TO_UPDATE_ALL_EMPLOYERS: true,
    data: { fakeKey: 'fakeValue' }
  }];

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeUpdate.called, true, 'Employers have been updated');
    t.deepEquals(fakeSend.args, [['The selected employers have been updated']], 'Update success');
    sandbox.reset();
    t.end();
  })();
});

test('Update all employers - fail', t => {
  fakeReq.body = [{
    employers: ['allEmployers'],
    data: { fakeKey: 'fakeValue' }
  }];

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeUpdate.called, false, 'Employers have not been updated');
    t.deepEquals(fakeApiError.args[0][1], 'FORBIDDEN', 'Update failed');
    sandbox.reset();
    t.end();
  })();
});
