const { ErrorCodes, ApiError } = require('ww-utils');

const { questionsModel: Questions } = require('../db/questions-model');
const calculateScore = require('../utils/calculate-score');

module.exports = async (req, res, next) => {
    try {
      const userInput = req.body.userInput;
      const questionsData = await Questions.findOne({
        version: req.body.questionsVersion,
      })
      .lean()
      .exec();

      const questions = questionsData.questions;

      const scoreValue = calculateScore(
        questions,
        userInput
      );

      res.status(201).send({score: scoreValue}); // eslint-disable-line
  } catch (e) {
    next(
      new ApiError(
        'Internal server error - failed to generate score',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
