const {
  ErrorCodes,
  ApiError,
} = require('ww-utils');
const co = require('co');
const R = require('ramda');
const {
  encryptString,
} = require('ww-utils').encryption;
const moment = require('moment');
const bcrypt = require('bcryptjs');

const moneyCreator = require('../db/money-creator');
const {
  env,
} = require('../../../config/index');

module.exports = (req, res, next) => {
  co(function* addAnswers() {
    // eslint-disable-line
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const hashString = x => bcrypt.hashSync(x, 10);

    const data = R.evolve({
      email: curriedEncrypt,
      password: hashString,
      userFinancialPriorities: x => [{ created_at: moment().utc().toDate(), priorities: x }],
      wwFinancialPriorities: x => [{ created_at: moment().utc().toDate(), priorities: x }],
      questions: x => [{ created_at: moment().utc().toDate(), questions: x }],
    })(req.body);

    yield moneyCreator(data); // eslint-disable-line

    return res.json({
      success: true,
      status: 201,
    });
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to add answers',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
