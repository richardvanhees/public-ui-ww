const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const encryption = require('ww-utils').encryption;

// Setup
const sandbox = sinon.sandbox.create();
const apiErrorStub = sandbox.stub();
const fakeReq = sandbox.stub();
const fakeRes = sandbox.stub();
fakeRes.status = sandbox.stub();
const fakeSend = sandbox.stub();
const fakeNext = sandbox.stub();
const fakeFind = sandbox.stub();
const fakeLean = sandbox.stub();
const fakeExec = sandbox.stub();

fakeRes.status.returns({ send: fakeSend });
fakeFind.returns({ lean: fakeLean });
fakeLean.returns({ exec: fakeExec });

const target = proxyquire('./get-tip-links', {
  'ww-utils': {
    ErrorCodes: {
      INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR'
    },
    ApiError: apiErrorStub,
  },
  '../../db/tip-link-model': {
    tipLinkModel: {
      find: fakeFind
    }
  },
});

// Tests

test('get tiplinks - success', t => {
  fakeExec.returns('fakeData');

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeSend.args, [['fakeData']], 'Get tiplinks success');
    sandbox.reset();
    t.end();
  })();

});

test('get tiplinks - failed', t => {
  fakeExec.throws(Error);

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.args[0][1], 'INTERNAL_SERVER_ERROR', 'Get tiplinks failed');
    sandbox.reset();
    t.end();
  })();

});