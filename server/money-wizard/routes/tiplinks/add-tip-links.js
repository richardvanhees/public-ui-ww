const { ErrorCodes, ApiError } = require('ww-utils');
const tipLinkCreator = require('../../db/tip-link-creator');

/* eslint-disable consistent-return */
module.exports = async (req, res, next) => {
  try {
    for (let i = 0; i < req.body.tiplinks.length; i++) {
      const tiplink = req.body.tiplinks[i];
      await tipLinkCreator(tiplink); // eslint-disable-line
    }

    return res.json({
      success: true,
      status: 201,
    });
  } catch (err) {
    next(
      new ApiError(
        `Internal server error - failed to add tiplink(s) ${err}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
