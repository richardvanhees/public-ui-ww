const { ErrorCodes, ApiError } = require('ww-utils');
const { tipLinkModel: TipLinks } = require('../../db/tip-link-model');

/* eslint-disable consistent-return */
module.exports = async (req, res, next) => {
  try {
    const data = await TipLinks.find()
      .lean()
      .exec();

    return res.status(201).send(data); // eslint-disable-line
  } catch (err) {
    next(
      new ApiError(
        `Internal server error - failed to get tiplinks: ${JSON.stringify(
          err
        )}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
