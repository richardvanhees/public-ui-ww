const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const encryption = require('ww-utils').encryption;

// Setup
const sandbox = sinon.sandbox.create();
const apiErrorStub = sandbox.stub();
const fakeReq = {
  ...sandbox.stub(),
  body: {
    tiplinks: [{
      id: 0,
      url: 'fakeUrl',
      caption: 'fakeCaption',
      emailDescriptionStart: 'fakeEmailDescriptionStart',
      emailDescriptionEnd: 'fakeEmailDescriptionEnd',
      emailCaption: 'emailCaption'
    }]
  }
};
const fakeRes = sandbox.stub();
const fakeJson = sandbox.stub();
fakeRes.status = sandbox.stub();
fakeRes.json = fakeJson;
const fakeNext = sandbox.stub();
const fakeTipLinkCreator = sandbox.stub();

const target = proxyquire('./add-tip-links', {
  'ww-utils': {
    ErrorCodes: {
      INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
    },
    ApiError: apiErrorStub,
  },
  '../../db/tip-link-creator': fakeTipLinkCreator
});

// Tests

test('tiplink creation - success', t => {
  fakeTipLinkCreator.returns(true);

  (async function testFunction() {
    try {
      await target(fakeReq, fakeRes, fakeNext);
    } catch (e) {
      console.log(e);
    }

    t.deepEquals(fakeJson.args, [[{ status: 201, success: true }]], 'Create tiplink success');
    sandbox.reset();
    t.end();
  })();

});

test('tiplink creation - failed', t => {
  fakeTipLinkCreator.throws(Error);

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.args[0][1], 'INTERNAL_SERVER_ERROR', 'add tips failed');
    sandbox.reset();
    t.end();
  })();

});