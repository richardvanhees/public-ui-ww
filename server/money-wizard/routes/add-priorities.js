const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const prioritiesCreator = require('../db/priorities-creator');

module.exports = (req, res, next) => {
  co(function* addPriorities() {
    // eslint-disable-line
    yield prioritiesCreator(req.body); // eslint-disable-line

    return res.json({
      success: true,
      status: 201,
    });
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to add priorities',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
