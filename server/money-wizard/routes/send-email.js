const { env } = require('../../../config');
const request = require('request-promise-native');

const sendEmail = (email, template, tokens) => {
  const mandrillTags = JSON.parse(env.MANDRILL_TAGS_ENABLED)
    ? ['mw-follow-up']
    : ['mw-test-tag'];

  return request({
    method: 'post',
    uri: `${env.EMAIL_SERVICE_URL}/v2/email`,
    body: {
      tags: mandrillTags,
      email_address: email,
      template,
      tokens,
      merge_language: 'handlebars',
    },
    json: true,
    headers: {
      Authorization: `${env.EMAIL_SERVICE_API_KEY}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });
};

module.exports = async (req, res) => {
  try {
    // eslint-disable-line
    await sendEmail(req.body.email, req.body.template, req.body.tokens);
  } catch (e) {
    // the front end should carry on, if email sending is down, it cant do anything about it
    req.log.toInvestigateTomorrow('Failed to send email', e);
  }
  return res.json({
    success: true,
    status: 201,
  });
};
