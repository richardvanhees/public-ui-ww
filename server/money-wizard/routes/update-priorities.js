const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { prioritiesModel: Priorities } = require('../db/priorities-model');

module.exports = (req, res, next) => {
  co(function* updatePriorities() {
    // eslint-disable-line

    yield Priorities.findOneAndUpdate({ version: req.params.version }, { priorities: req.body })
      .lean()
      .exec();

    return res
      .status(201)
      .send(`Version ${req.params.version} priorities updated`); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to update priorities',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
