const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { employersModel: Employers } = require('../db/employers-model');

module.exports = (req, res, next) => {
  co(function* updateEmployer() {
    // eslint-disable-line
    yield Employers.findOneAndUpdate(
      { employer: req.params.employer },
      req.body
    )
      .lean()
      .exec();

    return res.status(200).send(`${req.params.employer} Employer updated`); // eslint-disable-line
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to update employer',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
