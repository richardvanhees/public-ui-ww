const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { env } = require('../../../../config');
const request = require('request-promise-native');

const sendPatMail = tokens =>
  request({
    method: 'post',
    uri: `${env.EMAIL_SERVICE_URL}/v2/email`,
    body: {
      email_address: `${env.PAT_MAIL_ADDRESS}`,
      template: 'PAT Contact Request',
      tokens,
      merge_language: 'handlebars',
    },
    json: true,
    headers: {
      Authorization: `${env.EMAIL_SERVICE_API_KEY}`,
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });

module.exports = (req, res, next) => {
  co(function* sendEmailFunc() {
    // eslint-disable-line
    yield sendPatMail(req.body.tokens);
    return res.json({
      success: true,
      status: 201,
    });
  }).catch(err => {
    req.log.toInvestigateTomorrow(err);
    next(
      new ApiError(
        'Internal server error - failed to send email',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
