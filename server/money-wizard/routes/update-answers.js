const { ErrorCodes, ApiError } = require('ww-utils');
const R = require('ramda');
const moment = require('moment');
const { moneyModel: MoneyUsers } = require('../db/money-model');
const { encryptString } = require('ww-utils').encryption;
const { env } = require('../../../config/index');
const { employersModel: Employers } = require('../db/employers-model');

/* eslint-disable consistent-return */
module.exports = async (req, res, next) => {
  try {
    // eslint-disable-line
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const data = R.evolve({
      email: curriedEncrypt,
      optInEmail: curriedEncrypt,
    })(req.body);

    if (data.userFinancialPriorities && data.wwFinancialPriorities && data.questions) {
      data.$push = {
        userFinancialPriorities: { created_at: moment().utc().toDate(), priorities: data.userFinancialPriorities },
        wwFinancialPriorities: { created_at: moment().utc().toDate(), priorities: data.wwFinancialPriorities },
        questions: { created_at: moment().utc().toDate(), questions: data.questions },
      };
    }

    const employer = await Employers.findOne({
      employer: req.params.employer,
    }).lean();

    if (!employer) {
      return next(
        new ApiError(
          'Employer configuration not found',
          ErrorCodes.RESOURCE_NOT_FOUND,
          404
        )
      );
    }

    const fieldsToUpdate = { ...R.omit(['userFinancialPriorities', 'wwFinancialPriorities', 'questions', 'password'], data) };

    if (
      data.prizeDrawOptIn &&
      employer.prizeDrawStartDate &&
      employer.prizeDrawEndDate
    ) {
      if (
        moment
          .utc()
          .isSameOrAfter(
            moment.utc(employer.prizeDrawStartDate).startOf('day')
          ) &&
        moment
          .utc()
          .isSameOrBefore(moment.utc(employer.prizeDrawEndDate).endOf('day'))
      ) {
        fieldsToUpdate.$push = {
          prizeDrawOptIns: {
            startDate: employer.prizeDrawStartDate,
            endDate: employer.prizeDrawEndDate,
            label: employer.prizeDrawLabel,
            mailchimpListName: employer.mailchimpListName,
            prizesText: employer.prizesText,
            processed: false,
          },
        };
      }
    }

    MoneyUsers.findOneAndUpdate(
      {
        email: data.email,
      },
      fieldsToUpdate,
      {
        upsert: true,
        new: true,
      },
      (err, updatedUser) => {
        res.json({
          statusCode: 200,
          statusMessage: `The answers for ${req.body.email} have been updated`,
          data: updatedUser,
        });
      }
    );
  } catch (e) {
    next(
      new ApiError(
        `Internal server error - failed to update answers for user ${
          req.body.email
          }`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
