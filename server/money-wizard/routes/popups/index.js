const getPopups = require('./get-popups');
const addPopups = require('./add-popups');
const updatePopup = require('./update-popup');
const getSinglePopup = require('./get-single-popup');

module.exports = {
  getPopups,
  addPopups,
  updatePopup,
  getSinglePopup,
};
