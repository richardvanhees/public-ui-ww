const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const popupCreator = require('../../db/popup-creator');

module.exports = (req, res, next) => {
  co(function* addPopups() {
    // eslint-disable-line
    for (let i = 0; i < req.body.popups.length; i++) {
      const popup = req.body.popups[i];
      yield popupCreator(popup); // eslint-disable-line
    }

    return res.status(201).json({
      success: true,
      status: 201,
    });
  }).catch(err => {
    next(
      new ApiError(
        `Internal server error - failed to add popups: ${JSON.stringify(err)}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
