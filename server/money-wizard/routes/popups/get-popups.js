const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { popupModel: Popups } = require('../../db/popup-model');

module.exports = (req, res, next) => {
  co(function* getPopups() {
    // eslint-disable-line
    const data = yield Popups.find()
      .lean()
      .exec();

    const popup = data.length === 1 ? data[0] : data;

    return res.status(201).send(popup); // eslint-disable-line
  }).catch(err => {
    next(
      new ApiError(
        `Internal server error - failed to get priorities: ${JSON.stringify(
          err
        )}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
