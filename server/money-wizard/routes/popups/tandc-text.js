const moment = require('moment');

module.exports = ({ startDate, endDate, prizesText }) => `
<div>

<p><span>By submitting your email address in
connection with your entry to this competition, you agree that Wealth Wizards
Advisers Limited and its third-party service providers may use your email
address to contact you if you win this competition and for related
administrative purposes. This does not waiver your anonymity with your
employer, Wealth Wizards Advisers Limited will contact you regarding your prize
directly.</span></p>

<p><span</span></p>

<ol>
 <li><span>Promotion open to users
     of Money Wizard only, excluding anyone professionally connected with this
     promotion.</span></li>
 <li><span>To enter, simply tick the box next to the sentence that reads I'd like to be entered in to the
     competition to win ${prizesText} and you will be entered into a prize
     draw.</span></li>
 <li><span
     >One entry will be
     registered per user that has been validly submitted before the competition
     closing date.&nbsp; Automated or bulk entries will not be accepted.</span></li>
 <li><span>Start Date: ${moment
   .utc(startDate)
   .format('MMMM Do YYYY')}</span></li>
 <li><span>Closing Date: ${moment
   .utc(endDate)
   .format('MMMM Do YYYY')}</span></li>
 <li><span
     >There is ${prizesText} available to win in this competition.</span></li>
 <li><span
     >The winner will be the
     first entrant drawn at random, from all valid submitted entries.&nbsp; The
     winners will be selected and notified by email within 14 days of the
     closing date.</span></li>
 <li><span
     >The prize will be sent
     in the post to the winner following such notification within a further 14
     days.</span></li>
 <li><span
     >In the event that any
     winner refuses acceptance of the prize or, for whatever reason, cannot
     accept the prize, or if the Promoter is unable to make contact with any
     winner within 14 days of the draw, the Promoter reserves the right to
     re-draw the prize and award it to an alternative winner.</span></li>
 <li><span
     >The selection of the
     winning entries will be supervised by an independent person.&nbsp; The
     result of the prize draw is final and binding in all matters and no
     correspondence will be entered into.</span></li>
 <li><span
     >Proof of submitting your
     entry is not proof of receipt of an entry by the Promoter.&nbsp; The
     Promoter is not responsible for any entries that fail to be submitted for
     any technical reason whatsoever.&nbsp; Entries submitted by any other
     means (including by email and post) will be rejected.</span></li>
 <li><span
     >The Promoter reserves
     the right to remove or to refuse any submission for any reason and these
     will not be entered into the prize draw.&nbsp; </span></li>
 <li><span
     >In ticking the box to
     enter this competition, you acknowledge that you, not the Promoter, are
     responsible for your submission.</span></li>
 <li><span
     >The prize is as stated,
     is non-transferable and no cash alternative is available.&nbsp; The
     Promoter reserves the right to award an alternative prize of equal or
     greater value, should the advertised prize or any part of it become
     unavailable for any reason.</span></li>
 <li><span
     >The Promoter reserves
     the right to cancel, amend, terminate or temporarily suspend this
     promotion in the event of any unforeseen circumstances or technical reason
     outside its reasonable control, with no liability to any entrants or third
     parties.</span></li>
 <li><span>By taking part in the
     promotion, entrants agree to be bound by these terms and conditions.&nbsp;
     Any breach of these terms and conditions by the winner may result in
     forfeiture of their prize.</span></li>
 <li><span
     >This promotion and these
     terms and conditions are governed by English law and subject to the
     exclusive jurisdiction of the English courts.</span></li>
</ol>

<p ><span</span></p>

<p ><span>Promoter: Wealth Wizards Advisers Limited,
Wizards House, 8 Athena Court, <span>Tachbrook</span> Park, <span>Leamington</span> Spa, CV34 6RT</span></p>

<p ><span</span></p>

<p ><span</span></p>

</div>
`;
