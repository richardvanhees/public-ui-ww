const { ErrorCodes, ApiError } = require('ww-utils');
const moment = require('moment');
const { popupModel: Popups } = require('../../db/popup-model');
const { employersModel: Employers } = require('../../db/employers-model');
const tandcText = require('./tandc-text');

module.exports = async (req, res, next) => {
  try {
    const data = await Popups.findOne({
      type: parseInt(req.params.popupType, 10),
    });

    if (!data) {
      return next(
        new ApiError(
          'Popup configuration not found',
          ErrorCodes.RESOURCE_NOT_FOUND,
          404
        )
      );
    }

    const popup = data.toJSON();

    const employer = await Employers.findOne({
      employer: req.params.employer,
    }).lean();

    if (!employer) {
      return next(
        new ApiError(
          'Employer configuration not found',
          ErrorCodes.RESOURCE_NOT_FOUND,
          404
        )
      );
    }

    if (employer.prizeDrawStartDate && employer.prizeDrawEndDate) {
      if (
        moment
          .utc()
          .isSameOrAfter(
            moment.utc(employer.prizeDrawStartDate).startOf('day')
          ) &&
        moment
          .utc()
          .isSameOrBefore(moment.utc(employer.prizeDrawEndDate).endOf('day'))
      ) {
        popup.prizeDrawEnabled = true;
        popup.prizesText = employer.prizesText;
        popup.prizeDrawLabel = `I’d like to be entered in to the competition to win ${
          employer.prizesText
        }. This competition will close on ${moment
          .utc(employer.prizeDrawEndDate)
          .format('MMMM Do YYYY')}.`;

        popup.termsAndConditionsText = tandcText({
          startDate: employer.prizeDrawStartDate,
          endDate: employer.prizeDrawEndDate,
          prizesText: employer.prizesText,
        });
        popup.termsAndConditionsLinkText =
          'Full terms and conditions can be found <a>here</a>';
        popup.termsAndConditionsTitle = 'Terms and conditions';
      } else {
        popup.prizeDrawEnabled = false;
      }
    } else {
      popup.prizeDrawEnabled = false;
    }

    return res.status(200).send(popup); // eslint-disable-line
  } catch (e) {
    return next(
      new ApiError(
        'Internal server error - failed to get priorities',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
