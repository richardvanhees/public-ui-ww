const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const { popupModel: Popup } = require('../../db/popup-model');

module.exports = (req, res, next) => {
  co(function* updatePopup() {
    // eslint-disable-line
    yield Popup.findOneAndUpdate({ type: req.params.popupType }, req.body)
      .lean()
      .exec();

    return res.status(200).send(`Popup ${req.params.popupType} updated`); // eslint-disable-line
  }).catch(err => {
    next(
      new ApiError(
        `Internal server error - failed to update popup: ${JSON.stringify(
          err
        )}`,
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
