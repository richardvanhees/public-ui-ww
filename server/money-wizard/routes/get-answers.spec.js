const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');

test('One question leads to export of three priorities', async t => {
  const sandbox = sinon.sandbox.create();
  const reqStub = sandbox.stub();
  const resStub = sandbox.stub();
  const setHeaderStub = sandbox.stub();
  const setStub = sandbox.stub();
  const statusStub = sandbox.stub();
  const statusReturnStub = sandbox.stub();
  const sendStub = sandbox.stub();
  resStub.setHeader = setHeaderStub;
  resStub.set = setStub;
  resStub.status = statusStub;

  const expectedHeaders = [
    'Employer',
    'Created',
    'q1',
    'Priority 1',
    'Priority 2',
    'Priority 3',
    'p1 - default',
    'p1 - adjusted',
    'p2 - default',
    'p2 - adjusted',
    'p3 - default',
    'p3 - adjusted',
  ];

  const employer = 'e1';
  const created_at_1 = '01/01/1970';
  const created_at_2 = '01/01/1980';
  const priority_1 = 'p1';
  const priority_2 = 'p2';
  const priority_3 = 'p3';
  const question_1 = 'q1';
  const answer_1 = 'a1';
  const answer_1_id = '0';

  const expectedRows = [
    [
      employer,
      created_at_1,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
    [
      employer,
      created_at_2,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
  ];

  let expectedResult = '';
  for (const col of expectedHeaders) {
    expectedResult += `"${col}",`;
  }
  expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  for (const row of expectedRows) {
    for (const cell of row) {
      if (typeof cell === 'number') {
        expectedResult += `${cell},`;
      } else {
        expectedResult += `"${cell}",`;
      }
    }
    expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  }
  expectedResult = expectedResult.slice(0, -1);

  statusReturnStub.send = csv => {
    t.equals(csv, expectedResult, 'CSV export works');
  };

  statusStub.withArgs(200).returns(statusReturnStub);

  const errorStub = sandbox.stub();
  reqStub.params = {
    employer: 'test-employer',
    csv: 'downloadCSV',
  };
  reqStub.query = {};
  const nextStub = sandbox.stub();

  const prioritiesFindStub = sandbox.stub();
  const prioritiesLeanStub = sandbox.stub();
  const prioritiesExecStub = sandbox.stub();
  prioritiesFindStub.withArgs({}).returns({ lean: prioritiesLeanStub });
  prioritiesLeanStub.returns({ exec: prioritiesExecStub });
  prioritiesExecStub.returns([
    {
      priorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
    },
  ]);

  const questionsFindStub = sandbox.stub();
  const questionsLeanStub = sandbox.stub();
  const questionsExecStub = sandbox.stub();
  questionsFindStub.withArgs({}).returns({ lean: questionsLeanStub });
  questionsLeanStub.returns({ exec: questionsExecStub });
  questionsExecStub.returns([
    {
      version: 1,
      questions: [
        {
          id: answer_1_id,
          question: question_1,
          answers: {
            '0': {
              answer: answer_1,
            },
          },
        },
      ],
    },
  ]);

  const usersFindStub = sandbox.stub();
  const usersLeanStub = sandbox.stub();
  const usersExecStub = sandbox.stub();
  usersFindStub.returns({ lean: usersLeanStub });
  usersLeanStub.returns({ exec: usersExecStub });
  usersExecStub.returns([
    {
      questionVersion: '1',
      created_at: created_at_1,
      employer,
      userFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      wwFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 2, name: priority_2 },
            { id: 3, orderId: 1, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
    {
      created_at: created_at_2,
      questionVersion: '1',
      employer,
      userFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      wwFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
  ]);

  const downloadCSV = proxyquire('./get-answers', {
    '../db/questions-model': {
      questionsModel: {
        find: questionsFindStub,
      },
    },
    '../db/money-model': {
      moneyModel: {
        find: usersFindStub,
      },
    },
    '../db/priorities-model': {
      prioritiesModel: {
        find: prioritiesFindStub,
      },
    },
    'ww-utils': {
      encryption: {
        encryptString: () => 'foo',
      },
      ErrorCodes: require('ww-utils').ErrorCodes,
      ApiError: require('ww-utils').ApiError,
    },
    '../../../config': {
      env: {
        UPDATED_MW_PII_DATA_ENCRYPTION_IV: 'UPDATED_MW_PII_DATA_ENCRYPTION_IV',
        UPDATED_MW_PII_DATA_ENCRYPTION_KEY:
          'UPDATED_MW_PII_DATA_ENCRYPTION_KEY',
      },
    },
  });

  await downloadCSV(reqStub, resStub, nextStub);

  t.true(
    setHeaderStub.calledWith(
      'Content-disposition',
      'attachment; filename=data.csv'
    ),
    'header set correctly'
  );

  t.true(
    setStub.calledWith('Content-Type', 'text/csv'),
    'content-type set correctly'
  );
  t.end();
});

test('get answers - include email', async t => {
  const sandbox = sinon.sandbox.create();
  const reqStub = sandbox.stub();
  const resStub = sandbox.stub();
  const setHeaderStub = sandbox.stub();
  const setStub = sandbox.stub();
  const statusStub = sandbox.stub();
  const statusReturnStub = sandbox.stub();
  const sendStub = sandbox.stub();
  resStub.setHeader = setHeaderStub;
  resStub.set = setStub;
  resStub.status = statusStub;

  const expectedHeaders = [
    'Employer',
    'Created',
    'Email',
    'q1',
    'Priority 1',
    'Priority 2',
    'Priority 3',
    'p1 - default',
    'p1 - adjusted',
    'p2 - default',
    'p2 - adjusted',
    'p3 - default',
    'p3 - adjusted',
  ];

  const employer = 'e1';
  const created_at_1 = '01/01/1970';
  const created_at_2 = '01/01/1980';
  const priority_1 = 'p1';
  const priority_2 = 'p2';
  const priority_3 = 'p3';
  const question_1 = 'q1';
  const answer_1 = 'a1';
  const answer_1_id = '0';

  const expectedRows = [
    [
      employer,
      created_at_1,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
    [
      employer,
      created_at_2,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
  ];

  let expectedResult =
    '"Employer","Created","Email","q1","Priority 1","Priority 2","Priority 3","p1 - default","p1 - adjusted","p2 - default","p2 - adjusted","p3 - default","p3 - adjusted"\n"e1","01/01/1970","decrypted","a1","p1","p2","p3",1,1,2,2,3,3\n"e1","01/01/1980","decrypted","a1","p1","p2","p3",1,1,2,2,3,3';

  statusReturnStub.send = csv => {
    t.equals(csv, expectedResult, 'CSV export works');
  };

  statusStub.withArgs(200).returns(statusReturnStub);

  const errorStub = sandbox.stub();
  reqStub.params = {
    employer: 'test-employer',
    csv: 'downloadCSV',
  };
  reqStub.query = { includeEmail: 1 };
  const nextStub = sandbox.stub();

  const prioritiesFindStub = sandbox.stub();
  const prioritiesLeanStub = sandbox.stub();
  const prioritiesExecStub = sandbox.stub();
  prioritiesFindStub.withArgs({}).returns({ lean: prioritiesLeanStub });
  prioritiesLeanStub.returns({ exec: prioritiesExecStub });
  prioritiesExecStub.returns([
    {
      priorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
    },
  ]);

  const questionsFindStub = sandbox.stub();
  const questionsLeanStub = sandbox.stub();
  const questionsExecStub = sandbox.stub();
  questionsFindStub.withArgs({}).returns({ lean: questionsLeanStub });
  questionsLeanStub.returns({ exec: questionsExecStub });
  questionsExecStub.returns([
    {
      version: 1,
      questions: [
        {
          id: answer_1_id,
          question: question_1,
          answers: {
            '0': {
              answer: answer_1,
            },
          },
        },
      ],
    },
  ]);

  const usersFindStub = sandbox.stub();
  const usersLeanStub = sandbox.stub();
  const usersExecStub = sandbox.stub();
  usersFindStub.returns({ lean: usersLeanStub });
  usersLeanStub.returns({ exec: usersExecStub });
  usersExecStub.returns([
    {
      questionVersion: '1',
      created_at: created_at_1,
      employer,
      userFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      wwFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 2, name: priority_2 },
            { id: 3, orderId: 1, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
    {
      created_at: created_at_2,
      questionVersion: '1',
      employer,
      userFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      wwFinancialPriorities: [
        {
          priorities: [
            { id: 1, orderId: 0, name: priority_1 },
            { id: 2, orderId: 1, name: priority_2 },
            { id: 3, orderId: 2, name: priority_3 },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
  ]);

  const downloadCSV = proxyquire('./get-answers', {
    '../db/questions-model': {
      questionsModel: {
        find: questionsFindStub,
      },
    },
    '../db/money-model': {
      moneyModel: {
        find: usersFindStub,
      },
    },
    '../db/priorities-model': {
      prioritiesModel: {
        find: prioritiesFindStub,
      },
    },
    'ww-utils': {
      encryption: {
        encryptString: () => 'foo',
        decryptString: () => 'decrypted',
      },
      ErrorCodes: require('ww-utils').ErrorCodes,
      ApiError: require('ww-utils').ApiError,
    },
    '../../../config': {
      env: {
        UPDATED_MW_PII_DATA_ENCRYPTION_IV: 'UPDATED_MW_PII_DATA_ENCRYPTION_IV',
        UPDATED_MW_PII_DATA_ENCRYPTION_KEY:
          'UPDATED_MW_PII_DATA_ENCRYPTION_KEY',
      },
    },
  });

  await downloadCSV(reqStub, resStub, nextStub);

  t.true(
    setHeaderStub.calledWith(
      'Content-disposition',
      'attachment; filename=data.csv'
    ),
    'header set correctly'
  );

  t.true(
    setStub.calledWith('Content-Type', 'text/csv'),
    'content-type set correctly'
  );
  t.end();
});

test('One question leads to export of three priorities - LEGACY datastructure', async t => {
  const sandbox = sinon.sandbox.create();
  const reqStub = sandbox.stub();
  const resStub = sandbox.stub();
  const setHeaderStub = sandbox.stub();
  const setStub = sandbox.stub();
  const statusStub = sandbox.stub();
  const statusReturnStub = sandbox.stub();
  const sendStub = sandbox.stub();
  resStub.setHeader = setHeaderStub;
  resStub.set = setStub;
  resStub.status = statusStub;

  const expectedHeaders = [
    'Employer',
    'Created',
    'q1',
    'Priority 1',
    'Priority 2',
    'Priority 3',
    'p1 - default',
    'p1 - adjusted',
    'p2 - default',
    'p2 - adjusted',
    'p3 - default',
    'p3 - adjusted',
  ];

  const employer = 'e1';
  const created_at_1 = '01/01/1970';
  const created_at_2 = '01/01/1980';
  const priority_1 = 'p1';
  const priority_2 = 'p2';
  const priority_3 = 'p3';
  const question_1 = 'q1';
  const answer_1 = 'a1';
  const answer_1_id = '0';

  const expectedRows = [
    [
      employer,
      created_at_1,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
    [
      employer,
      created_at_2,
      answer_1,
      priority_1,
      priority_2,
      priority_3,
      1, // "Need 1 - us",
      1, // "Need 1 - them",
      2, // "Need 2 - us",
      2, // "Need 2 - them",
      3, // "Need 3 - us",
      3, //"Need 3 - them",
    ],
  ];

  let expectedResult = '';
  for (const col of expectedHeaders) {
    expectedResult += `"${col}",`;
  }
  expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  for (const row of expectedRows) {
    for (const cell of row) {
      if (typeof cell === 'number') {
        expectedResult += `${cell},`;
      } else {
        expectedResult += `"${cell}",`;
      }
    }
    expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  }
  expectedResult = expectedResult.slice(0, -1);

  statusReturnStub.send = csv => {
    t.equals(csv, expectedResult, 'CSV export works');
  };

  statusStub.withArgs(200).returns(statusReturnStub);

  const errorStub = sandbox.stub();
  reqStub.params = {
    employer: 'test-employer',
    csv: 'downloadCSV',
  };
  reqStub.query = {};
  const nextStub = sandbox.stub();

  const prioritiesFindStub = sandbox.stub();
  const prioritiesLeanStub = sandbox.stub();
  const prioritiesExecStub = sandbox.stub();
  prioritiesFindStub.withArgs({}).returns({ lean: prioritiesLeanStub });
  prioritiesLeanStub.returns({ exec: prioritiesExecStub });
  prioritiesExecStub.returns([
    {
      priorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
    },
  ]);

  const questionsFindStub = sandbox.stub();
  const questionsLeanStub = sandbox.stub();
  const questionsExecStub = sandbox.stub();
  questionsFindStub.withArgs({}).returns({ lean: questionsLeanStub });
  questionsLeanStub.returns({ exec: questionsExecStub });
  questionsExecStub.returns([
    {
      version: 1,
      questions: [
        {
          id: answer_1_id,
          question: question_1,
          answers: {
            '0': {
              answer: answer_1,
            },
          },
        },
      ],
    },
  ]);

  const usersFindStub = sandbox.stub();
  const usersLeanStub = sandbox.stub();
  const usersExecStub = sandbox.stub();
  usersFindStub.returns({ lean: usersLeanStub });
  usersLeanStub.returns({ exec: usersExecStub });
  usersExecStub.returns([
    {
      questionVersion: '1',
      created_at: created_at_1,
      employer,
      userFinancialPriorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
      wwFinancialPriorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 2, name: priority_2 },
        { id: 3, orderId: 1, name: priority_3 },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
    {
      created_at: created_at_2,
      questionVersion: '1',
      employer,
      userFinancialPriorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
      wwFinancialPriorities: [
        { id: 1, orderId: 0, name: priority_1 },
        { id: 2, orderId: 1, name: priority_2 },
        { id: 3, orderId: 2, name: priority_3 },
      ],
      questions: [
        {
          questions: {
            '0': { answerId: 0 },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
    },
  ]);

  const downloadCSV = proxyquire('./get-answers', {
    '../db/questions-model': {
      questionsModel: {
        find: questionsFindStub,
      },
    },
    '../db/money-model': {
      moneyModel: {
        find: usersFindStub,
      },
    },
    '../db/priorities-model': {
      prioritiesModel: {
        find: prioritiesFindStub,
      },
    },
    'ww-utils': {
      encryption: {
        encryptString: () => 'foo',
      },
      ErrorCodes: require('ww-utils').ErrorCodes,
      ApiError: require('ww-utils').ApiError,
    },
    '../../../config': {
      env: {
        UPDATED_MW_PII_DATA_ENCRYPTION_IV: 'UPDATED_MW_PII_DATA_ENCRYPTION_IV',
        UPDATED_MW_PII_DATA_ENCRYPTION_KEY:
          'UPDATED_MW_PII_DATA_ENCRYPTION_KEY',
      },
    },
  });

  await downloadCSV(reqStub, resStub, nextStub);

  t.true(
    setHeaderStub.calledWith(
      'Content-disposition',
      'attachment; filename=data.csv'
    ),
    'header set correctly'
  );

  t.true(
    setStub.calledWith('Content-Type', 'text/csv'),
    'content-type set correctly'
  );
  t.end();
});

test('Order of user priorities is exported correctly', async t => {
  const sandbox = sinon.sandbox.create();
  const reqStub = sandbox.stub();
  const resStub = sandbox.stub();
  const setHeaderStub = sandbox.stub();
  const setStub = sandbox.stub();
  const statusStub = sandbox.stub();
  const statusReturnStub = sandbox.stub();
  const sendStub = sandbox.stub();
  resStub.setHeader = setHeaderStub;
  resStub.set = setStub;
  resStub.status = statusStub;

  const expectedHeaders = [
    'Employer',
    'Created',
    'Question 0',
    'Question 1',
    'Question 2',
    'Question 3',
    'Question 4',
    'Question 5',
    'Question 6',
    'Question 7',
    'Priority 1',
    'Priority 2',
    'Priority 3',
    'Building up savings - default',
    'Building up savings - adjusted',
    'Investing money for the long term - default',
    'Investing money for the long term - adjusted',
    'Protecting from the unexpected - default',
    'Protecting from the unexpected - adjusted',
    'Saving into pensions - default',
    'Saving into pensions - adjusted',
    'Preparing for retirement - default',
    'Preparing for retirement - adjusted',
    'Managing regular spending - default',
    'Managing regular spending - adjusted',
    'Buying a home - default',
    'Buying a home - adjusted',
  ];

  const expectedRows = [
    [
      'ww',
      '2017-11-28T08:36:15.933Z',
      'Question 0 - Answer 2',
      'Question 1 - Answer 3',
      'Question 2 - Answer 3',
      'Question 3 - Answer 2',
      'Question 4 - Answer 2',
      'Question 5 - Answer 0',
      'Question 6 - Answer 0',
      'Question 7 - Answer 1',
      'Buying a home',
      'Building up savings',
      'Managing regular spending',
      4,
      2,
      2,
      7,
      3,
      5,
      1,
      6,
      6,
      4,
      5,
      3,
      7,
      1,
    ],
  ];

  let expectedResult = '';

  for (const col of expectedHeaders) {
    expectedResult += `"${col}",`;
  }
  expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  for (const row of expectedRows) {
    for (const cell of row) {
      if (typeof cell === 'number') {
        expectedResult += `${cell},`;
      } else {
        expectedResult += `"${cell}",`;
      }
    }
    expectedResult = `${expectedResult.trim().slice(0, -1)}\n`;
  }
  expectedResult = expectedResult.slice(0, -1);

  statusReturnStub.send = csv => {
    t.equals(csv, expectedResult, 'CSV export works');
  };

  statusStub.withArgs(200).returns(statusReturnStub);

  const errorStub = sandbox.stub();
  reqStub.params = {
    employer: 'test-employer',
    csv: 'downloadCSV',
  };
  reqStub.query = {};
  const nextStub = sandbox.stub();

  const prioritiesFindStub = sandbox.stub();
  const prioritiesLeanStub = sandbox.stub();
  const prioritiesExecStub = sandbox.stub();
  prioritiesFindStub.withArgs({}).returns({ lean: prioritiesLeanStub });
  prioritiesLeanStub.returns({ exec: prioritiesExecStub });
  prioritiesExecStub.returns([
    {
      priorities: [
        {
          orderId: 0,
          score: 12,
          name: 'Saving into pensions',
          id: 3,
        },
        {
          orderId: 1,
          score: 10,
          name: 'Investing money for the long term',
          id: 1,
        },
        {
          orderId: 2,
          score: 9,
          name: 'Protecting from the unexpected',
          id: 2,
        },
        {
          orderId: 3,
          score: 5,
          name: 'Building up savings',
          id: 0,
        },
        {
          orderId: 4,
          score: 1,
          name: 'Managing regular spending',
          id: 5,
        },
        {
          orderId: 5,
          score: 0,
          name: 'Preparing for retirement',
          id: 4,
        },
        {
          orderId: 6,
          score: 0,
          name: 'Buying a home',
          id: 7,
        },
      ],
    },
  ]);

  const questionsFindStub = sandbox.stub();
  const questionsLeanStub = sandbox.stub();
  const questionsExecStub = sandbox.stub();
  questionsFindStub.withArgs({}).returns({ lean: questionsLeanStub });
  questionsLeanStub.returns({ exec: questionsExecStub });
  questionsExecStub.returns([
    {
      version: 1,
      questions: [
        {
          id: '0',
          question: 'Question 0',
          answers: {
            '0': {
              answer: 'Question 0 - Answer 0',
            },
            '1': {
              answer: 'Question 0 - Answer 1',
            },
            '2': {
              answer: 'Question 0 - Answer 2',
            },
            '3': {
              answer: 'Question 0 - Answer 3',
            },
          },
        },
        {
          id: '1',
          question: 'Question 1',
          answers: {
            '0': {
              answer: 'Question 1 - Answer 0',
            },
            '1': {
              answer: 'Question 1 - Answer 1',
            },
            '2': {
              answer: 'Question 1 - Answer 2',
            },
            '3': {
              answer: 'Question 1 - Answer 3',
            },
          },
        },
        {
          id: '2',
          question: 'Question 2',
          answers: {
            '0': {
              answer: 'Question 2 - Answer 0',
            },
            '1': {
              answer: 'Question 2 - Answer 1',
            },
            '2': {
              answer: 'Question 2 - Answer 2',
            },
            '3': {
              answer: 'Question 2 - Answer 3',
            },
          },
        },
        {
          id: '3',
          question: 'Question 3',
          answers: {
            '0': {
              answer: 'Question 3 - Answer 0',
            },
            '1': {
              answer: 'Question 3 - Answer 1',
            },
            '2': {
              answer: 'Question 3 - Answer 2',
            },
            '3': {
              answer: 'Question 3 - Answer 3',
            },
          },
        },
        {
          id: '4',
          question: 'Question 4',
          answers: {
            '0': {
              answer: 'Question 4 - Answer 0',
            },
            '1': {
              answer: 'Question 4 - Answer 1',
            },
            '2': {
              answer: 'Question 4 - Answer 2',
            },
            '3': {
              answer: 'Question 4 - Answer 3',
            },
          },
        },
        {
          id: '5',
          question: 'Question 5',
          answers: {
            '0': {
              answer: 'Question 5 - Answer 0',
            },
            '1': {
              answer: 'Question 5 - Answer 1',
            },
            '2': {
              answer: 'Question 5 - Answer 2',
            },
            '3': {
              answer: 'Question 5 - Answer 3',
            },
          },
        },
        {
          id: '6',
          question: 'Question 6',
          answers: {
            '0': {
              answer: 'Question 6 - Answer 0',
            },
            '1': {
              answer: 'Question 6 - Answer 1',
            },
            '2': {
              answer: 'Question 6 - Answer 2',
            },
            '3': {
              answer: 'Question 6 - Answer 3',
            },
          },
        },
        {
          id: '7',
          question: 'Question 7',
          answers: {
            '0': {
              answer: 'Question 7 - Answer 0',
            },
            '1': {
              answer: 'Question 7 - Answer 1',
            },
            '2': {
              answer: 'Question 7 - Answer 2',
            },
            '3': {
              answer: 'Question 7 - Answer 3',
            },
          },
        },
      ],
    },
  ]);

  const usersFindStub = sandbox.stub();
  const usersLeanStub = sandbox.stub();
  const usersExecStub = sandbox.stub();
  usersFindStub.returns({ lean: usersLeanStub });
  usersLeanStub.returns({ exec: usersExecStub });
  usersExecStub.returns([
    {
      _id: '5a1d1fff74308a0019fa6ded',
      questionVersion: '1',
      updated_at: '2017-11-28T08:36:15.933Z',
      created_at: '2017-11-28T08:36:15.933Z',
      email: 'peter.mccready@wealthwizards.com',
      employer: 'ww',
      consentedPrivacyPolicy: true,
      questionVersion: '1',
      emailSent: true,
      userFinancialPriorities: [
        {
          priorities: [
            {
              orderId: 6,
              score: 0,
              name: 'Buying a home',
              id: 7,
            },
            {
              orderId: 3,
              score: 5,
              name: 'Building up savings',
              id: 0,
            },
            {
              orderId: 4,
              score: 1,
              name: 'Managing regular spending',
              id: 5,
            },
            {
              orderId: 5,
              score: 0,
              name: 'Preparing for retirement',
              id: 4,
            },
            {
              orderId: 2,
              score: 9,
              name: 'Protecting from the unexpected',
              id: 2,
            },
            {
              orderId: 0,
              score: 12,
              name: 'Saving into pensions',
              id: 3,
            },
            {
              orderId: 1,
              score: 10,
              name: 'Investing money for the long term',
              id: 1,
            },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      wwFinancialPriorities: [
        {
          priorities: [
            {
              orderId: 0,
              score: 12,
              name: 'Saving into pensions',
              id: 3,
            },
            {
              orderId: 1,
              score: 10,
              name: 'Investing money for the long term',
              id: 1,
            },
            {
              orderId: 2,
              score: 9,
              name: 'Protecting from the unexpected',
              id: 2,
            },
            {
              orderId: 3,
              score: 5,
              name: 'Building up savings',
              id: 0,
            },
            {
              orderId: 4,
              score: 1,
              name: 'Managing regular spending',
              id: 5,
            },
            {
              orderId: 5,
              score: 0,
              name: 'Preparing for retirement',
              id: 4,
            },
            {
              orderId: 6,
              score: 0,
              name: 'Buying a home',
              id: 7,
            },
          ],
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      questions: [
        {
          questions: {
            '0': {
              answerId: 2,
            },
            '1': {
              answerId: 3,
            },
            '2': {
              answerId: 3,
            },
            '3': {
              answerId: 2,
            },
            '4': {
              answerId: 2,
            },
            '5': {
              answerId: 0,
            },
            '6': {
              answerId: 0,
            },
            '7': {
              answerId: 1,
            },
          },
          created_at: '09/07/18 07:45:00 UTC',
        },
      ],
      __v: 0,
    },
  ]);

  const downloadCSV = proxyquire('./get-answers', {
    '../db/questions-model': {
      questionsModel: {
        find: questionsFindStub,
      },
    },
    '../db/money-model': {
      moneyModel: {
        find: usersFindStub,
      },
    },
    '../db/priorities-model': {
      prioritiesModel: {
        find: prioritiesFindStub,
      },
    },
    'ww-utils': {
      encryption: {
        encryptString: () => 'foo',
      },
      ErrorCodes: require('ww-utils').ErrorCodes,
      ApiError: require('ww-utils').ApiError,
    },
    '../../../config': {
      env: {
        UPDATED_MW_PII_DATA_ENCRYPTION_IV: 'UPDATED_MW_PII_DATA_ENCRYPTION_IV',
        UPDATED_MW_PII_DATA_ENCRYPTION_KEY:
          'UPDATED_MW_PII_DATA_ENCRYPTION_KEY',
      },
    },
  });

  downloadCSV(reqStub, resStub, nextStub);

  setImmediate(() => {
    t.true(
      setHeaderStub.calledWith(
        'Content-disposition',
        'attachment; filename=data.csv'
      ),
      'header set correctly'
    );
    t.true(
      setStub.calledWith('Content-Type', 'text/csv'),
      'content-type set correctly'
    );
    t.end();
  });
});
