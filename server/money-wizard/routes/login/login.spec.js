const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
const encryption = require('ww-utils').encryption;

// Setup
const sandbox = sinon.sandbox.create();
const apiErrorStub = sandbox.stub();
const fakeReq = {
  ...sandbox.stub(),
  body: {
    email: 'fakeEmail',
    password: 'fakePassword'
  }
};
const fakeRes = sandbox.stub();
fakeRes.status = sandbox.stub();
const fakeSend = sandbox.stub();
const fakeNext = sandbox.stub();
const fakeFindOne = sandbox.stub();
const fakeLean = sandbox.stub();
const fakeExec = sandbox.stub();
const fakeCompareSync = sandbox.stub();

fakeRes.status.returns({ send: fakeSend });
fakeFindOne.returns({ lean: fakeLean });
fakeLean.returns({ exec: fakeExec });
fakeCompareSync.returns(true);

const target = proxyquire('./index', {
  'ww-utils': {
    ErrorCodes: {
      INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      FAILED_VALIDATION: 'FAILED_VALIDATION',
    },
    ApiError: apiErrorStub,
    encryption,
  },
  'bcryptjs': {
    compareSync: fakeCompareSync,
  },
  '../../db/money-model': {
    moneyModel: {
      findOne: fakeFindOne
    }
  },
  '../../../../config': {
    env: {
      UPDATED_MW_PII_DATA_ENCRYPTION_KEY: "12345678901234567890123456789012",
      UPDATED_MW_PII_DATA_ENCRYPTION_IV: "1234567890123456",
    },
  },
});

// Tests

test('login - success', t => {
  fakeExec.returns({
    email: 'fakeEmail',
    userFinancialPriorities: 'fakePriorities',
    password: 'fakePassword',
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeSend.args, [[{ email: 'fakeEmail', userFinancialPriorities: 'fakePriorities' }]], 'Login success');
    sandbox.reset();
    t.end();
  })();

});

test('login - failed validation', t => {
  fakeExec.returns();

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.args[0][1], 'FAILED_VALIDATION', 'Login failed validation');
    sandbox.reset();
    t.end();
  })();

});

test('login - server error', t => {
  fakeLean.returns(false);
  fakeExec.returns(false);

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.args[0][1], 'INTERNAL_SERVER_ERROR', 'Login server error');
    sandbox.reset();
    t.end();
  })();

});