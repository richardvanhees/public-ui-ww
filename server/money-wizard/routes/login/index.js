const { ErrorCodes, ApiError } = require('ww-utils');
const { moneyModel: MoneyUsers } = require('../../db/money-model');
const { env } = require('../../../../config');
const { encryptString } = require('ww-utils').encryption;
const R = require('ramda');
const bcrypt = require('bcryptjs');

module.exports = async (req, res, next) => {
  try {
    const curriedEncrypt = x =>
      encryptString(
        x,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_IV,
        env.UPDATED_MW_PII_DATA_ENCRYPTION_KEY
      );

    const result = await MoneyUsers.findOne({
      email: curriedEncrypt(req.body.email),
    })
      .lean()
      .exec();

    if (result && bcrypt.compareSync(req.body.password, result.password)) {
      return res.status(201).send(R.omit(['password'], result)); // eslint-disable-line
    }

    next(
      new ApiError(
        'Internal server error - login details incorrect or user not found',
        ErrorCodes.FAILED_VALIDATION,
        400
      )
    );
    return true;
  } catch (e) {
    next(
      new ApiError(
        'Internal server error - failed to login',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
    return false;
  }
};
