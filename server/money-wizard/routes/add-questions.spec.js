const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

test('test that happy day question payload is inserted into db', t => {
  t.plan(3);

  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();

  const fakeResponse = {
    success: true,
    status: 201,
  };

  const fakePayload = {
    weightingOptions: ['one', 'two', 'three'],
    questions: [{ question1: 'answers' }, { question2: 'answers2' }],
    version: 1,
  };

  const fakeReq = {
    body: fakePayload,
    wwKeys: {
      id: 'fake id',
    },
  };
  const fakeRes = {};
  const fakeNext = () => 'next called';
  const target = proxyquire('./add-questions', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: function() {
        apiErrorStub;
      },
    },
    '../db/questions-creator': payload => {
      t.deepEquals(payload.weightingOptions, ['one', 'two', 'three']);
      t.deepEquals(payload.questions, [
        { question1: 'answers' },
        { question2: 'answers2' },
      ]);
      t.equals(payload.version, 1);
      return Promise.resolve(fakeResponse);
    },
  });

  target(fakeReq, fakeRes, fakeNext);
});
