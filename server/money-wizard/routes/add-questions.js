const { ErrorCodes, ApiError } = require('ww-utils');
const co = require('co');
const questionCreator = require('../db/questions-creator');

module.exports = (req, res, next) => {
  co(function* addQuestions() {
    // eslint-disable-line
    yield questionCreator(req.body); // eslint-disable-line

    return res.json({
      success: true,
      status: 201,
    });
  }).catch(() => {
    next(
      new ApiError(
        'Internal server error - failed to add questions',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });
};
