const { moneyModel: MoneyUser } = require('./db/money-model');

module.exports = [
  {
    type: 'mongo',
    name: 'money wizard db',
    model: MoneyUser,
  },
];
