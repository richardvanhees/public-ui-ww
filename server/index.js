require('hpropagate')();
const { paths, env, clientEnv } = require('../config');
const path = require('path');
const express = require('express');
const helmet = require('helmet');
const handlebars = require('express-handlebars');
const responseTime = require('response-time');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const wwLogging = require('ww-logging');
const wwMonitoring = require('ww-monitoring');
const bodyParser = require('body-parser');
const moment = require('moment');

const { CONTEXT_ROUTE } = env;
const { BASE_DIR, PUBLIC_DIR } = paths;

require(`./${env.PRODUCT_KEY}/setup`).setup();

const app = express();
const contextRoute = express.Router();

app.use((req, res, next) => {
  res.set('Service-Worker-Allowed', '/');
  next();
});

app.use(
  express.static(CONTEXT_ROUTE, {
    immutable: true,
    setHeaders: res => {
      res.set('Cache-Control', 'no-cache');
    },
  })
);
contextRoute.use(helmet());
contextRoute.use(wwLogging.requestId);
contextRoute.use(wwLogging.requestLogger);

wwMonitoring.ping(app, CONTEXT_ROUTE);
wwMonitoring.health(app, require(`./${env.PRODUCT_KEY}/health`), CONTEXT_ROUTE);

app.use(
  `${CONTEXT_ROUTE}/assets`,
  express.static('assets', { redirect: false })
);

contextRoute.use(responseTime());
contextRoute.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(
  bodyParser.json({
    limit: '5mb',
  })
);
contextRoute.use(cookieParser());

app.engine(
  'html',
  handlebars({
    helpers: {
      toJson: object => JSON.stringify(object),
    },
  })
);
app.set('view engine', 'html');

require(`./${env.PRODUCT_KEY}/routes`)(contextRoute);

contextRoute.use(
  '/assets',
  express.static(path.join(BASE_DIR, PUBLIC_DIR, '/assets'), {
    redirect: false,
  })
);

contextRoute.get('/server-timestamp', (req, res) => {
  res.json({ server_timestamp: moment.utc().format('YYYY-MM-DDTHH:mm:ssZ') });
});

// fully client-side rendered
contextRoute.get('*', (req, res) => {
  const pathToIndex = path.join(
    BASE_DIR,
    PUBLIC_DIR,
    `${env.PRODUCT_KEY}.html`
  );

  return res.render(pathToIndex, {
    clientEnv: JSON.stringify(clientEnv),
    CONTEXT_ROUTE,
  });
});

contextRoute.use(wwLogging.errorHandler);

app.use(CONTEXT_ROUTE, contextRoute);

app.shutdown = () => {
  require(`./${env.PRODUCT_KEY}/setup`).teardown(); // eslint-disable-line global-require
};

module.exports = {
  app,
};
