const { env } = require('../../config');

module.exports = [
  {
    type: 'service',
    name: 'solution-store',
    url: `${env.SOLUTION_STORE_URL}/ping`,
  },
  {
    type: 'service',
    name: 'reset-password-service',
    url: `${env.RESET_PASSWORD_SVC_URL}/ping`,
  },
  {
    type: 'service',
    name: 'auth-service',
    url: `${env.AUTH_SVC_URL}/ping`,
  },
  {
    type: 'service',
    name: 'fact-find-service',
    url: `${env.FACT_FIND_SVC_URL}/ping`,
  },
  {
    type: 'service',
    name: 'content-service',
    url: `${env.CONTENT_SERVICE_BASE_URL}/ping`,
  },
];
