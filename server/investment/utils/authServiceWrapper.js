const request = require('axios');
const { env } = require('../../../config');

const requestWrapper = (method, data) => {
  const baseUrl = `${env.AUTH_SVC_URL}/v1`;

  return request({
    method,
    url: `${baseUrl}${data.route}`,
    data: data.data,
    json: true,
    headers: {
      ...data.headers,
      Authorization: env.AUTH_SVC_API_KEY,
      'content-type': 'application/json',
      Accept: 'application/json',
    },
  });
};

module.exports = {
  get: data =>
    typeof data === 'string'
      ? requestWrapper('get', { route: data })
      : requestWrapper('get', data), // eslint-disable-line
  post: data => requestWrapper('post', data),
  putRequest: data => requestWrapper('put', data),
};
