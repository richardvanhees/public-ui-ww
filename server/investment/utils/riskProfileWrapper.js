const request = require('axios');
const { env } = require('../../../config');

const requestWrapper = (method, data) => {
  const baseUrl = `${env.RISK_PROFILE_SVC_URL}/v1`;

  return request({
    method,
    url: `${baseUrl}${data.route}?tenant=${env.TENANT_KEY}&capability=investment`,
    data: data.data,
    json: true,
    headers: {
      ...data.headers,
      Authorization: env.RISK_PROFILE_SVC_API_KEY,
      'content-type': 'application/json',
      Accept: 'application/json',
      'Cache-Control':
        'no-cache, no-store, must-revalidate, private, max-age=0',
    },
  });
};

module.exports = {
  get: data =>
    typeof data === 'string'
      ? requestWrapper('get', { route: data })
      : requestWrapper('get', data), // eslint-disable-line
  post: data => requestWrapper('post', data),
  putRequest: data => requestWrapper('put', data),
};
