const tokenCreator = require('../../../test/token-creator');

const buildHtmlToInjectToken = token => `
<html>
  <head>
    <script type="text/javascript">
      window.localStorage.setItem('wealthwizards', JSON.stringify({ jwt: '${token}' }));
      window.location.assign('/investment');
    </script>
  </head>
  <body>
  </body>
</html>
`;

module.exports = (req, res) => {
  try {
    res.set('content-type', 'text/html');
    res.send(buildHtmlToInjectToken(tokenCreator()));
  } catch (e) {
    res.status(500).end();
  }
};
