const { ErrorCodes, ApiError } = require('ww-utils');
const request = require('request-promise-native');

module.exports = async (req, res, next) => {
  try {
    const { callback } = req.query;
    const data = await request({
      uri: decodeURIComponent(callback),
      json: true,
    });
    res.status(201).send(data);
  } catch (error) {
    next(
      new ApiError(
        'Internal server error',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
