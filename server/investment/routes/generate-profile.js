const { ErrorCodes, ApiError } = require('ww-utils');
const { post } = require('../utils/riskProfileWrapper');
const moment = require('moment');

module.exports = async (riskProfileRoute, req, res, next) => {
  try {
    const { data } = await post({
      route: `/${riskProfileRoute}`,
      data: req.body,
    });
    res.status(201).send({
      ...data,
      generated_at: moment.utc().format('YYYY-MM-DDTHH:mm:ssZ'),
    }); // eslint-disable-line
  } catch (error) {
    req.log.toInvestigateTomorrow(error);
    next(
      new ApiError(
        'Unable to generate profile',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  }
};
