const { ErrorCodes, ApiError } = require('ww-utils');
const { post } = require('../utils/authServiceWrapper');
const R = require('ramda');

module.exports = async (req, res, next) => {
  try {
    const { data } = await post({
      route: '/user',
      data: req.body,
    });

    return res.status(201).send(data); // eslint-disable-line
  } catch (error) {
    if (
      R.path(['response', 'data', 'code'])(error) === 'USERNAME_ALREADY_EXISTS'
    ) {
      return next(new ApiError('Username exists', 'USERNAME_ALREADY_EXISTS', 400));
    }
    return next(new ApiError('Internal server error', ErrorCodes.INTERNAL_SERVER_ERROR, 500));
  }
};
