const nocache = require('nocache');
const express = require('express');
const { find, retrieve } = require('./postcode-anywhere-middleware');
const generateProfile = require('./generate-profile');
const createUser = require('./create-user');
const getToken = require('./get-token');
const validateRecaptcha = require('../../middlewares/validate-recaptcha');
const activateUser = require('./activate-user');
const generatePaymentTestHarnessData = require('./payment-test-harness');

module.exports = app => {
  const staticFilesToExpose = [
    { route: '/manifest.json', path: `${__dirname}/pwa/manifest.json` },
    { route: '/service-worker.js', path: `${__dirname}/pwa/service-worker.js` },
    { route: '/192x192.png', path: `${__dirname}/pwa/icons/192x192.png` },
    { route: '/512x512.png', path: `${__dirname}/pwa/icons/512x512.png` },
    {
      route: '/apple-touch-icon-144x144.png',
      path: `${__dirname}/pwa/icons/apple-touch-icon-144x144.png`,
    },
    {
      route: '/apple-touch-icon-152x152.png',
      path: `${__dirname}/pwa/icons/apple-touch-icon-152x152.png`,
    },
    {
      route: '/favicon-32x32.png',
      path: `${__dirname}/pwa/icons/favicon-32x32.png`,
    },
    {
      route: '/favicon-16x16.png',
      path: `${__dirname}/pwa/icons/favicon-16x16.png`,
    },
    {
      route: '/mstile-144x144.png',
      path: `${__dirname}/pwa/icons/mstile-144x144.png`,
    },
  ];

  staticFilesToExpose.forEach(({ route, path }) =>
    app.use(route, express.static(path))
  );

  if (process.env.NODE_ENV !== 'production') {
    const devLogin = require('./dev-login'); // eslint-disable-line
    app.get('/dev-login', devLogin);
  }

  app.get('/v1/postcode-find', nocache(), find);
  app.get('/v1/postcode-retrieve', nocache(), retrieve);

  app.post('/v1/attitude-to-risk', (req, res, next) =>
    generateProfile('attitude-to-risk', req, res, next)
  );

  app.post('/v1/investment-experience', (req, res, next) =>
    generateProfile('investment-experience', req, res, next)
  );

  app.post('/v1/financial-flexibility', (req, res, next) =>
    generateProfile('financial-flexibility', req, res, next)
  );

  app.post('/v1/financial-resilience', (req, res, next) =>
    generateProfile('financial-resilience', req, res, next)
  );

  app.get('/v1/payment-test-harness', (req, res, next) =>
    generatePaymentTestHarnessData(req, res, next)
  );

  app.post('/v1/user', nocache(), validateRecaptcha, createUser);

  app.post('/v1/token', nocache(), getToken);

  app.get('/v1/activate-user/:id', nocache(), activateUser);
};
