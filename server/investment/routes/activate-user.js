const { ErrorCodes, ApiError } = require('ww-utils');
const { post } = require('../utils/authServiceWrapper');

module.exports = async (req, res, next) => {
  try {
    const code = req.params.id;
    await post({
      route: '/activate-user',
      data: { code },
    });
    return res.redirect('/investment/login?activated');
  } catch (error) {
    return next(new ApiError('Unable to activate account', ErrorCodes.INTERNAL_SERVER_ERROR, 500));
  }
};

