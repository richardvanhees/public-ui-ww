const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');

test('Payment test harness', async assert => {
  const sandbox = sinon.sandbox.create();

  const responseData = {
    foo: 'Bar',
  };

  const apiErrorStub = sandbox.stub();
  const requestStub = sandbox.stub();
  requestStub.resolves(responseData);

  const fakeReq = {
    query: {
      callback: '123',
    },
    body: {},
  };

  const statusStub = sandbox.stub();
  statusStub.returns(requestStub);

  requestStub.end = sandbox.stub();
  requestStub.end.returns(requestStub);

  requestStub.send = sandbox.stub();
  requestStub.send.returns(requestStub);

  const fakeRes = {
    status: statusStub,
  };
  const fakeNext = () => 'next called';

  const target = proxyquire('./payment-test-harness', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: function () {
        apiErrorStub();
      },
    },
    'request-promise-native': requestStub,
  });

  await target(fakeReq, fakeRes, fakeNext);

  assert.deepEqual(requestStub.args, [
    [
      {
        json: true,
        uri: '123',
      },
    ],
  ]);

  assert.end();
});
