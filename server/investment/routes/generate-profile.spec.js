const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');

test('Profiles are generated', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  const postStub = sandbox.stub();
  postStub.resolves({ data: 'some fake data' });

  const fakeResponse = {
    success: true,
    status: 201,
  };

  const fakePayload = {
    work_more: 'yes',
    defer_income: 'yes',
    contribute_more: 'no',
    other_resources: 'yes',
    reduce_income: 'no',
    family_support: 'no',
  };

  const fakeReq = {
    body: fakePayload,
  };
  const fakeRes = {};
  const fakeNext = () => 'next called';

  const target = proxyquire('./generate-profile', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: function() {
        apiErrorStub();
      },
    },
    '../utils/riskProfileWrapper': {
      post: postStub,
    },
  });

  target(fakeReq, fakeRes, fakeNext);

  t.end();
});
