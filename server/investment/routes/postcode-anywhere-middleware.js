const co = require('co');
const { ApiError, ErrorCodes } = require('ww-utils');
const {
  CaptureInteractiveFind,
  CaptureInteractiveRetrieve,
} = require('ww-postcode-anywhere');
const { env } = require('../../../config');

const find = (req, res, next) =>
  co(function* findAddress() {
    // eslint-disable-line
    const response = yield CaptureInteractiveFind(
      Object.assign({}, req.query, {
        Key: env.POSTCODE_ANYWHERE_KEY,
      })
    );
    res.json(response);
  }).catch(() => {
    next(
      new ApiError(
        'Unable to process request',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });

const retrieve = (req, res, next) =>
  co(function* retrieveAddress() {
    // eslint-disable-line
    const response = yield CaptureInteractiveRetrieve(
      Object.assign({}, req.query, {
        Key: env.POSTCODE_ANYWHERE_KEY,
      })
    );
    res.json(response);
  }).catch(() => {
    next(
      new ApiError(
        'Unable to process request',
        ErrorCodes.INTERNAL_SERVER_ERROR,
        500
      )
    );
  });

module.exports = {
  find,
  retrieve,
};
