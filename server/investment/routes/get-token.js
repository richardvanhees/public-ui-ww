const { ErrorCodes, ApiError } = require('ww-utils');
const { post } = require('../utils/authServiceWrapper');
const R = require('ramda');

module.exports = async (req, res, next) => {
  try {
    const { data } = await post({
      route: '/oauth/token',
      data: req.body,
    });

    res.status(201).send(data); // eslint-disable-line
  } catch (e) {
    next(
      new ApiError(
        e.message,
        R.pathOr(ErrorCodes.INTERNAL_SERVER_ERROR, ['body', 'code'])(e),
        e.statusCode
      )
    );
  }
};
