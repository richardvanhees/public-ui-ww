const { app } = require('./index');
const { env } = require('../config');
const { APP_PORT, CONTEXT_ROUTE } = env;

app.listen(APP_PORT, err => {
  if (err) {
    console.error(err); // eslint-disable-line no-console
  }
  // eslint-disable-next-line no-console
  console.info(
    // eslint-disable-line no-console
    `Listening on port ${APP_PORT} with context route of ${CONTEXT_ROUTE}`
  );
});
