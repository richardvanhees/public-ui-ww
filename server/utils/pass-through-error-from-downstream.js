const { ErrorCodes, ApiError } = require('ww-utils');
const R = require('ramda');

module.exports = e =>
  new ApiError(
    R.pathOr(e.message, ['response', 'body', 'message'])(e),
    R.pathOr(ErrorCodes.INTERNAL_SERVER_ERROR, ['response', 'body', 'code'])(e),
    e.statusCode
  );
