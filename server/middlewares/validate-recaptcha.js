const request = require('request-promise-native');
const { ApiError } = require('ww-utils');
const { env } = require('../../config');

const callToVerifyRecaptcha = ({ sitekey, secret }) =>
  request({
    method: 'post',
    uri: `https://www.google.com/recaptcha/api/siteverify?secret=${secret}&response=${sitekey}`,
    json: true,
  });

module.exports = async (req, res, next) => {
  try {
    if (env.SHOW_FAKE_RECAPTCHA) {
      return next();
    }

    const keys =
      RegExp(/demo_[A-Za-z0-9\-]*(@wealthwizards.io)/g).test(
        req.body.username
      ) && env.RECAPTCHA_AUTOMATION_FEATURE_ACTIVE
        ? {
            sitekey: req.headers.recaptcha,
            secret: env.RECAPTCHA_AUTOMATION_SECRET,
          }
        : {
            sitekey: req.headers.recaptcha,
            secret: env.RECAPTCHA_SECRET,
          };

    const response = await callToVerifyRecaptcha(keys);

    if (response.success) {
      return next();
    }
    return next(
      new ApiError('Recaptcha verification failed', 'RECAPTCHA_FAILED', 500)
    );
  } catch (e) {
    return next(new ApiError('Recaptcha failed', 'RECAPTCHA_FAILED', 500));
  }
};
