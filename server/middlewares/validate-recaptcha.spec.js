const test = require('tape');
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');
require('sinon-as-promised');

test('validate recaptcha', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();

  const fakeReq = {
    ...sandbox.stub(),
    headers: {
      recaptcha: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    },
    body: {
      username: 'fakeEmail@wealthwizards.io'
    }
  };
  const fakeRes = sandbox.stub();
  const fakeNext = sandbox.stub();

  const target = proxyquire('./validate-recaptcha', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: apiErrorStub,
    },
    '../../config': {
      env: {
        RECAPTCHA_SECRET: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
      }
    },
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeNext.args, [[]], 'Next called');
    sandbox.reset();
  })();

  t.end();
});

test('validate recaptcha', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  apiErrorStub.returns({ error: 'error' });

  const fakeReq = {
    ...sandbox.stub(),
    headers: {
      recaptcha: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    },
    body: {
      username: 'fakeEmail@wealthwizards.io'
    }
  };
  const fakeRes = sandbox.stub();
  const fakeNext = sandbox.stub();

  const target = proxyquire('./validate-recaptcha', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: apiErrorStub,
    },
    '../../config': {
      env: {
        RECAPTCHA_SECRET: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWf',
      }
    },
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.called, true, 'Verification error');
    sandbox.reset();
  })();

  t.end();
});

test('validate recaptcha', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  apiErrorStub.returns({ error: 'error' });

  const fakeReq = {
    ...sandbox.stub(),
    headers: {
      recaptcha: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    },
    body: {
      username: 'demo_1234@wealthwizards.io'
    }
  };
  const fakeRes = sandbox.stub();
  const fakeNext = sandbox.stub();

  const target = proxyquire('./validate-recaptcha', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: apiErrorStub,
    },
    '../../config': {
      env: {
        RECAPTCHA_SECRET: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
        RECAPTCHA_AUTOMATION_SECRET: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
      }
    },
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(fakeNext.args, [[]], 'Applied automation keys');
    sandbox.reset();
  })();

  t.end();
});

test('validate recaptcha', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  apiErrorStub.returns({ error: 'error' });

  const fakeReq = {
    ...sandbox.stub(),
    headers: {
      recaptcha: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    },
    body: {
      username: 'fakeEmail@wealthwizards.io'
    }
  };
  const fakeRes = sandbox.stub();
  const fakeNext = sandbox.stub();

  const target = proxyquire('./validate-recaptcha', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: apiErrorStub,
    },
    '../../config': {
      env: {
        RECAPTCHA_SECRET: 'fake6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
      }
    },
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.called, true, 'Invalid keys');
    sandbox.reset();
  })();

  t.end();
});

test('validate recaptcha', t => {
  const sandbox = sinon.sandbox.create();

  const apiErrorStub = sandbox.stub();
  apiErrorStub.returns({ error: 'error' });

  const fakeReq = {
    ...sandbox.stub(),
    headers: {
      recaptcha: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
    },
    body: {
      username: 'fakeEmail@wealthwizards.io'
    }
  };
  const fakeRes = sandbox.stub();
  const fakeNext = sandbox.stub();

  const target = proxyquire('./validate-recaptcha', {
    'ww-utils': {
      ErrorCodes: {
        INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
      },
      ApiError: apiErrorStub,
    },
    '../../config': {
      env: {
        RECAPTCHA_SECRET: '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe',
      }
    },
    'request-promise-native': () => Promise.reject({
      statusCode: 500
    }),
  });

  (async function testFunction() {
    await target(fakeReq, fakeRes, fakeNext);

    t.deepEquals(apiErrorStub.called, true, 'Endpoint error');
    sandbox.reset();
  })();

  t.end();
});