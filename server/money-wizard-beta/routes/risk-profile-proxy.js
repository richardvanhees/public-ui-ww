const request = require('request-promise-native');
const { env } = require('../../../config');
const passThroughErrorFromDownstream = require('../../utils/pass-through-error-from-downstream');

module.exports = async (req, res, next) => {
  try {
    const response = await request({
      uri: `${env.RISK_PROFILE_SVC_URL}/v1/${req.body.profileType}`,
      json: true,
      method: 'POST',
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
        Authorization: env.RISK_PROFILE_SVC_API_KEY,
      },
      body: req.body,
      qs: {
        // TODO make configurable
        tenant: 'athena',
        capability: 'investment',
      },
    });
    res.json(response).end();
  } catch (e) {
    next(passThroughErrorFromDownstream(e));
  }
};
