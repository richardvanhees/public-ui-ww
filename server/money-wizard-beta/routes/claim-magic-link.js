const request = require('request-promise-native');
const { env } = require('../../../config');
const passThroughErrorFromDownstream = require('../../utils/pass-through-error-from-downstream');

/**
 * User is sent email with link to app in it.
 * on clicking the link it opens a react route
 * which grabs the uuid in the query param, makes an ajax request here
 * and then puts the token response in local storage
 * Going directly here from clicking link in email will mean we'll have to do an express
 * redirect and pass jwt and refresh token in url params
 * which is a no no
 */
module.exports = async (req, res, next) => {
  try {
    const loginResponse = await request({
      uri: `${env.AUTH_SVC_URL}/v1/magic-links/claim`,
      json: true,
      method: 'POST',
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
        Authorization: env.AUTH_SVC_API_KEY,
      },
      body: {
        uuid: req.body.uuid,
      },
    });
    // return the jwt et al
    res.json(loginResponse).end();
  } catch (e) {
    next(passThroughErrorFromDownstream(e));
  }
};
