const nocache = require('nocache');
const validateRecaptcha = require('../../middlewares/validate-recaptcha');
const registerMagicLinkUser = require('./register-magic-link-user');
const requestMagicLink = require('./request-for-magic-link');
const claimMagicLink = require('./claim-magic-link');
const riskProfileProxy = require('./risk-profile-proxy');

module.exports = app => {
  app.post(
    '/v1/magic-link/register',
    nocache(),
    validateRecaptcha,
    registerMagicLinkUser
  );
  app.post('/v1/magic-link/request', nocache(), requestMagicLink);
  app.post('/v1/magic-link/claim', nocache(), claimMagicLink);
  app.post('/v1/risk-profile-proxy', nocache(), riskProfileProxy);
};
