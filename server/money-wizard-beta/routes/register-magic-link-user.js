const request = require('request-promise-native');
const { env } = require('../../../config');
const passThroughErrorFromDownstream = require('../../utils/pass-through-error-from-downstream');

module.exports = async (req, res, next) => {
  try {
    const registerResponse = await request({
      uri: `${env.AUTH_SVC_URL}/v1/magic-links/register`,
      json: true,
      method: 'POST',
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
        Authorization: env.AUTH_SVC_API_KEY,
      },
      body: {
        emailAddress: req.body.emailAddress,
      },
    });

    try {
      await request({
        method: 'post',
        uri: `${env.EMAIL_SERVICE_URL}/v2/email`,
        body: {
          email_address: req.body.emailAddress.trim().toLowerCase(),
          template: env.MONEY_WIZARD_BETA_REG_EMAIL_TEMPLATE_NAME,
          tokens: {},
        },
        json: true,
        headers: {
          Authorization: `${env.EMAIL_SERVICE_API_KEY}`,
          'Content-Type': 'application/json',
          Accept: 'application/json',
          'Cache-Control':
            'no-cache, no-store, must-revalidate, private, max-age=0',
        },
      });
    } catch (e) {
      req.log.toInvestigateTomorrow(
        'Failure to send money-wizard-beta registration email',
        e
      );
    }

    res
      .status(201)
      .json(registerResponse)
      .end();
  } catch (e) {
    next(passThroughErrorFromDownstream(e));
  }
};
