const request = require('request-promise-native');
const { env } = require('../../../config');
const passThroughErrorFromDownstream = require('../../utils/pass-through-error-from-downstream');

module.exports = async (req, res, next) => {
  try {
    await request({
      uri: `${env.AUTH_SVC_URL}/v1/magic-links/request`,
      json: true,
      method: 'POST',
      headers: {
        'Cache-Control':
          'no-cache, no-store, must-revalidate, private, max-age=0',
        Authorization: env.AUTH_SVC_API_KEY,
      },
      body: {
        emailAddress: req.body.emailAddress,
      },
    });
    res.status(200).end();
  } catch (e) {
    next(passThroughErrorFromDownstream(e));
  }
};
