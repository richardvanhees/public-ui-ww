const { env } = require('../../config');

module.exports = [
  {
    type: 'service',
    name: 'auth-service',
    url: `${env.AUTH_SVC_URL}/ping`,
  },
  {
    type: 'service',
    name: 'chat-store',
    url: `${env.CHAT_STORE_BASE_URL}/ping`,
  },
  {
    type: 'service',
    name: 'todo-store',
    url: `${env.TODO_STORE_BASE_URL}/ping`,
  },
  {
    type: 'service',
    name: 'email-service',
    url: `${env.EMAIL_SERVICE_URL}/ping`,
  },
];
