FROM quay.io/wealthwizards/dev-toolbox AS install

LABEL build.target=install

COPY .npmrc yarn.lock package.json /usr/src/app/
WORKDIR /usr/src/app
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
RUN yarn --pure-lockfile --ignore-engines --check-files

COPY . /usr/src/app/

ENTRYPOINT ["yarn"]

FROM install AS build
ARG PUSH_CDN=false
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY

ENV PUSH_CDN ${PUSH_CDN}
ENV AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
ENV AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}

LABEL build.target=build

RUN yarn build

FROM quay.io/wealthwizards/ww-base-node:alpine-8 AS prod

LABEL build.target=prod

ENV NODE_ENV=production

COPY .npmrc .yarnrc yarn.lock package.json /usr/src/app/
WORKDIR /usr/src/app
RUN yarn --production --pure-lockfile --ignore-engines

COPY --from=build /usr/src/app/config config
COPY --from=build /usr/src/app/server server
COPY --from=build /usr/src/app/dist dist
COPY --from=build /usr/src/app/assets assets
COPY --from=build /usr/src/app/db-migrations db-migrations
COPY --from=build /usr/src/app/content content

EXPOSE 80

ENTRYPOINT ["yarn"]
CMD ["start"]
