# Public ui

This repo houses investment wizard, money wizard 1 and money wizard 2. They are all front end, public facing apps.

## Getting started

Create a .env in the root of this project and place the relevant environment variables in that file.

- For MW: copy the contents of .env-mw into your .env
- For IW: copy the contents of .env-iw into your .env
- For MW beta copy the contents of .env-money-wizard-beta into your .env

Note, for MW 1 <b>alone</b> you will need a local mongo setup. You can start your mongo with the command: mongod

### Populating your local MW 1 database

Ensure you've copied the contents of .env-mw into your .env file, then:

- start the app by running: sudo make dev-local
- then in another terminal window run: yarn populate:mw

### Starting the apps

Run: sudo make dev-local. The apps will live on:

- http://localhost.wealthwizards.io/investment
- http://localhost.wealthwizards.io/money-wizard-beta
- http://localhost:3000/money-wizard

### Nothing appears

Check the nohup.out file in the root of the project for an error

### Using the MW API

Richard van Hees has written a nice guide here: https://wealthwizards.atlassian.net/wiki/spaces/TS/pages/1143341073/Money+Wizard

More information about it in code can be found in the script/mw folder

### Story book for the UI components

Running the storybook instance

```
yarn storybook
```

If you don't have it installed globally

```
yarn global add @storybook/react
```

### Google Recaptcha

We use the account `wealthwizards99@gmail.com` for the recaptcha feature. We use the given real pair for our 'online' environments. E2E tests and local environments will use the key pair mentioned above.

### Money Wizard Beta - developing locally

If you get sick of the recaptcha and want to work offline using the mock server known as dyson then set these additional env vars:

```
CHAT_STORE_BASE_URL=http://localhost:3001/chat-store
TODO_STORE_BASE_URL=http://localhost:3001/todo-store
SHOW_FAKE_RECAPTCHA=1
AUTH_SVC_URL=http://localhost:3001/digital-auth-service
EMAIL_SERVICE_URL=http://localhost:3001/email-service
```

and then restart

### Getting test users for investment wizard

Clone the ww-smoke-test repo and run one of the following commands which will spit out an email address which you can login with using a password of password123:

```
# creates user who has registered and not completed fact find
TEST_ENVIRONMENT=GREEN yarn athena-fresh
# creates user who has registered and completed fact find and thus ready to generate a solution
TEST_ENVIRONMENT=GREEN yarn athena-complete-fact-find
```
