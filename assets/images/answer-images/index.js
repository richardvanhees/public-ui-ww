export q1a0 from './5964_Moreish_WW_Illust_Q1_Family-1.png';
export q1a1 from './5964_Moreish_WW_Illust_Q1_Family-2.png';
export q1a2 from './5964_Moreish_WW_Illust_Q1_Family-3.png';
export q1a3 from './5964_Moreish_WW_Illust_Q1_Family-4.png';

export q2a0 from './5964_Moreish_WW_Illust_Q2_Employment-5.png';
export q2a1 from './5964_Moreish_WW_Illust_Q2_Employment-6.png';
export q2a2 from './5964_Moreish_WW_Illust_Q2_Employment-7.png';
export q2a3 from './5964_Moreish_WW_Illust_Q2_Employment-8.png';

export q3a0 from './5964_Moreish_WW_Illust_Q3_Spending-9.png';
export q3a1 from './5964_Moreish_WW_Illust_Q3_Spending-10.png';
export q3a2 from './5964_Moreish_WW_Illust_Q3_Spending-11.png';

export q4a0 from './5964_Moreish_WW_Illust_Q4_Unexpected-12.png';
export q4a1 from './5964_Moreish_WW_Illust_Q4_Unexpected-13.png';
export q4a2 from './5964_Moreish_WW_Illust_Q4_Unexpected-14.png';

export q5a0 from './5964_Moreish_WW_Illust_Q5_Ownership-15.png';
export q5a1 from './5964_Moreish_WW_Illust_Q5_Ownership-16.png';
export q5a2 from './5964_Moreish_WW_Illust_Q5_Ownership-17.png';

export q6a0 from './5964_Moreish_WW_Illust_Q6_Debt-21.png';
export q6a1 from './5964_Moreish_WW_Illust_Q6_Debt-22.png';
export q6a2 from './5964_Moreish_WW_Illust_Q6_Debt-23.png';
export q6a3 from './5964_Moreish_WW_Illust_Q6_Debt-24.png';

export q7a0 from './5964_Moreish_WW_Illust_Q7_Pension-18.png';
export q7a1 from './5964_Moreish_WW_Illust_Q7_Pension-19.png';
export q7a2 from './5964_Moreish_WW_Illust_Q7_Pension-20.png';

export q8a0 from './5964_Moreish_WW_Illust_Q8_Savings-25.png';
export q8a1 from './5964_Moreish_WW_Illust_Q8_Savings-26.png';
export q8a2 from './5964_Moreish_WW_Illust_Q8_Savings-27.png';
