export splash from './splash.png';
export face from './face.svg';
export red from './red.svg';
export blue from './blue.svg';
export inbox from './inbox.svg';
export back from './back.svg';
export profile from './profile.png';
export logo from './logo.png';
export error from './error.png';
export signInIcon from './sign-in-icon.png';
export avatarMan from './avatar-man.png';
