export placeholder from './placeholder.png';

export puzzleFull from './puzzle-full.png';
export puzzleTopLeft from './puzzle-top-left.png';
export puzzleTopRight from './puzzle-top-right.png';
export puzzleBottomLeft from './puzzle-bottom-left.png';
export puzzleBottomRight from './puzzle-bottom-right.png';

export puzzlePieceTopLeft from './puzzle-piece-top-left.png';
export puzzlePieceTopRight from './puzzle-piece-top-right.png';
export puzzlePieceBottomLeft from './puzzle-piece-bottom-left.png';
export puzzlePieceBottomRight from './puzzle-piece-bottom-right.png';

export riskLevelDial from './risk-level-dial.png';
export riskLevelDialPointer from './risk-level-dial-pointer.png';

export return0 from './return-0.png';
export return1 from './return-1.png';
export return2 from './return-2.png';


export checklistChecksAnimated from './checklist_checks_animated.gif';
export checklist from './checklist_checks.png';
export tick1 from './tick-1.png';
export tick2 from './tick-2.png';
export tick3 from './tick-3.png';
export tick4 from './tick-4.png';

export piggyBank from './piggybank.gif';

export owlLeftHandUp from './owl-left-hand.png';
export owlRightHandUp from './owl-right-hand.png';
export owlClipboard from './owl-clipboard.png';
export owlWand from './owl-with-wand.png';
export checkMark from './check-mark.png';
export owlWithGlasses from './owl-with-glasses.svg';

export resetPass from './reset-password.png';

export characterLogo from './iw-logo-col-xparent.png';
export characterLoginTitle from './iw-logo-col-xparent.png';
export characterLoginWhiteTitle from './iw-logo-col-xparent.png';
export characterLoginMagic from './ilana-magic.png';

export plant from './plant.png';
export celebrate from './celebrate.png';
export partyPopper from './party-popper.svg';

export checkmark from './checkmark.png';
export exclaim from './exclaim.png';
export iwLogo from './iw-logo-col-xparent.png';
export mma from './mma.png';
export plantInhands from './daniel-hjalmarsson-269425-unsplash.jpg';
export ppa from './ppa.png';
export ukpa from './ukpa.jpg';
export wif from './wif.png';
export wwLogoMono from './ww-logo-mono-xparent.png'
