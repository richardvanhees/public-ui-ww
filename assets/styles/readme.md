 ## Styling ##
 
 This readme explains the guidelines of the style folder structure.
 
 #### Project folder structure #### 
  Preferably, each JS component will have its own SCSS file. 
  In some cases, for example when having sub-components in your components, a separate file is not necessary.
  
  As expected, the base styling for each component can be found in the `base` folder.
  Every breakpoint folder will only contain **exceptions** on the files in the `base` folder. 
  This means that it is not possible for a stylesheet to be present in a breakpoint folder only.
  Every exception is also valid for the breakpoints lower than the breakpoint the exception was added for.
  
  The media queries are defined in the `index.scss` files of the `common` folder and of each project.
  
  As the variables are needed in every other file imported, we need to make sure that `base/variables.scss` will always be the first imported file in `index.scss`.  
 
 #### Common ####
 The common folder contains all shared base styling of the shared components.
 You will find an `index.scss` file and a folder per breakpoint (if necessary).
 The variables for these breakpoints can be found in `common/base/variables.scss`.  
 
 #### Project folder ####
 The folders for the projects are nearly the same as `common`.
 The main difference is that these folders only contain **exceptions** for the components in `common` and project specific components.
 The `index.scss` file also contains the `common` variables.