{
  "id": "moneyWizard",
  "priorities": [
    {
      "id": 0,
      "name": "Building up savings",
      "todosToGenerate": ["buildingUpSavings"]
    },
    {
      "id": 1,
      "name": "Investing money for the long term",
      "todosToGenerate": ["investForTheLongTerm"]
    },
    {
      "id": 2,
      "name": "Protecting from the unexpected",
      "todosToGenerate": ["protectingFromTheUnexpected"]
    },
    {
      "id": 3,
      "name": "Saving into pensions",
      "todosToGenerate": ["savingIntoPensions"]
    },
    {
      "id": 4,
      "name": "Preparing for retirement",
      "todosToGenerate": ["preparingForRetirement"]
    },
    {
      "id": 5,
      "name": "Managing regular spending",
      "todosToGenerate": ["managingRegularSpending"]
    },
    {
      "id": 6,
      "name": "Reducing debt",
      "todosToGenerate": ["reducingDebt"]
    },
    {
      "id": 7,
      "name": "Buying a home",
      "todosToGenerate": ["buyingAHome"]
    }
  ],
  "questions": [
    {
      "id": "1",
      "answerType": "multipleChoice",
      "content": "How old are you?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "choices": [
        {
          "id": "under26",
          "label": "Under 26",
          "weighting": [3.03, 0.02, 0.06, 0.05, -30.06, 3.07, 0.08, 3.01]
        },
        {
          "id": "26To35",
          "label": "26 to 35",
          "weighting": [3.03, 0.02, 1.06, 1.05, -30.06, 3.07, 1.08, 3.01]
        },
        {
          "id": "36To45",
          "label": "36 to 45",
          "weighting": [1.03, 3.02, 3.06, 3.05, 0.04, 0.07, 0.08, 1.01]
        },
        {
          "id": "46To55",
          "label": "46 to 55",
          "weighting": [0.03, 3.02, 1.06, 3.05, 1.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "over55",
          "label": "Over 55",
          "weighting": [0.03, 3.02, 0.06, 3.05, 3.04, 0.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "2",
      "answerType": "multipleChoice",
      "content": "What's your family situation?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "choices": [
        {
          "id": "single",
          "label": "I'm single",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q1_Family-1.png",
          "weighting": [1.03, 0.02, 0.06, 0.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "singleWithChildren",
          "label": "I'm single with children",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q1_Family-2.png",
          "weighting": [0.03, 0.02, 3.06, 0.05, 0.04, 3.07, 1.08, 1.01]
        },
        {
          "id": "hasPartner",
          "label": "I have a partner",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q1_Family-3.png",
          "weighting": [0.03, 0.02, 1.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "hasPartnerHasChildren",
          "label": "I have a partner and children",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q1_Family-4.png",
          "weighting": [0.03, 0.02, 3.06, 1.05, 0.04, 1.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "3",
      "answerType": "multipleChoice",
      "content": "What is your employment status?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "choices": [
        {
          "id": "parttimeContract",
          "label": "I work part time on a contract basis",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q2_Employment-5.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "fulltimeContract",
          "label": "I work full time on a contract basis",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q2_Employment-6.png",
          "weighting": [1.03, 1.02, 1.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "parttimePerm",
          "label": "I work part time in a permanent role",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q2_Employment-7.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "fulltimePerm",
          "label": "I work full time in a permanent role",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q2_Employment-8.png",
          "weighting": [1.03, 1.02, 0.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "4",
      "answerType": "multipleChoice",
      "content": "How would you describe your spending habits each month?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": "Thanks ✅",
      "choices": [
        {
          "id": "spendsMore",
          "label": "I spend more than I earn each month",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q3_Spending-9.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 3.07, 3.08, 0.01]
        },
        {
          "id": "spendsEverything",
          "label": "I spend everything I earn, but nothing more",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q3_Spending-10.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 1.07, 1.08, 0.01]
        },
        {
          "id": "moneyLeftover",
          "label": "I always have money left over each month",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q3_Spending-11.png",
          "weighting": [1.03, 1.02, 1.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "5",
      "answerType": "multipleChoice",
      "content": "How would you handle a large unexpected bill?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": null,
      "choices": [
        {
          "id": "panic",
          "label": "I would panic, I have no savings whatsoever",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 3.07, 3.08, 0.01]
        },
        {
          "id": "struggle",
          "label": "I might struggle, I have less than 3 months' wages saved up",
          "weighting": [0.03, 0.02, 1.06, 0.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "cope",
          "label": "I would probably cope, I have a good emergency fund",
          "weighting": [1.03, 1.02, 1.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "6",
      "answerType": "multipleChoice",
      "content": "What is your home ownership situation?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": null,
      "choices": [
        {
          "id": "own",
          "label": "I own my own home",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q5_Ownership-15.png",
          "weighting": [0.03, 1.02, 1.06, 1.05, 0.04, 1.07, 0.08, -30.09]
        },
        {
          "id": "wantsToOwn",
          "label": "I'd like to buy my own home",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q5_Ownership-16.png",
          "weighting": [3.03, 0.02, 0.06, 0.05, 0.04, 3.07, 0.08, 3.01]
        },
        {
          "id": "notInterested",
          "label": "I'm not interested in buying a home",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q5_Ownership-17.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 0.07, 0.08, -30.09]
        }
      ]
    },
    {
      "id": "7",
      "answerType": "multipleChoice",
      "content": "What's your debt situation?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": "Just a couple more questions...",
      "choices": [
        {
          "id": "noDebt",
          "label": "I don't have any debts",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q6_Debt-21.png",
          "weighting": [1.03, 3.02, 0.06, 3.05, 0.04, 0.07, -30.02, 0.01]
        },
        {
          "id": "repays",
          "label": "I repay my debts on time",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q6_Debt-22.png",
          "weighting": [0.03, 1.02, 1.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "sometimesDifficult",
          "label": "I sometimes have difficulty paying off debts",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q6_Debt-23.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 3.07, 3.08, 0.01]
        },
        {
          "id": "significantDebt",
          "label": "I'm in significant debt",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q6_Debt-24.png",
          "weighting": [0.03, -30.08, 0.06, 0.05, -30.06, 3.07, 3.08, 0.01]
        }
      ]
    },
    {
      "id": "8",
      "answerType": "multipleChoice",
      "content": "How would you describe your pension planning?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": null,
      "choices": [
        {
          "id": "always",
          "label": "I've always paid into a pension",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q7_Pension-18.png",
          "weighting": [1.03, 0.02, 0.06, 1.05, 1.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "notAlways",
          "label": "I haven't always contributed into my pension",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q7_Pension-19.png",
          "weighting": [0.03, 0.02, 0.06, 1.05, 0.04, 0.07, 0.08, 0.01]
        },
        {
          "id": "notInterested",
          "label": "I'm not interested in saving into a pension",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q7_Pension-20.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 0.07, 0.08, 0.01]
        }
      ]
    },
    {
      "id": "8.1",
      "answerType": "multipleChoice",
      "content": "How is your planning for retirement?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": null,
      "choices": [
        {
          "id": "notReally",
          "label": "It’s a long way off and I’m not really thinking about it"
        },
        {
          "id": "startingToThink",
          "label": "I’d like to retire within the next 10 years",
          "todosToGenerate": ["pensionOptionsTriage"]
        },
        {
          "id": "hopingToRetireInNextFewYearsNoPlan",
          "label": "I’d like to retire soon but don’t have a clear plan",
          "todosToGenerate": ["pensionOptionsTriage"]
        },
        {
          "id": "retireInNextSixMonths",
          "label": "I’d like to retire in the next 6 months",
          "todosToGenerate": ["pensionOptionsTriage"]
        }
      ]
    },
    {
      "id": "9",
      "answerType": "multipleChoice",
      "content": "Finally, which of these best describes your position on saving for the future?",
      "avatar": "john",
      "answerTemplate": "{{answer}}",
      "pause": 1000,
      "contentToShowAfterAnswer": null,
      "choices": [
        {
          "id": "regularFocused",
          "label": "I save regularly and am focused on building up my savings",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q8_Savings-25.png",
          "weighting": [3.03, 3.02, 0.06, 3.05, 0.04, 0.07, 0.08, 0.01],
          "endState": true
        },
        {
          "id": "wantsToSave",
          "label": "I want to save, but haven't done so recently",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q8_Savings-26.png",
          "weighting": [3.03, 1.02, 0.06, 1.05, 0.04, 1.07, 0.08, 0.01],
          "endState": true
        },
        {
          "id": "hasntSaved",
          "label": "I haven't been able to save",
          "imgPath": "/money-wizard-beta/assets/images/money-wizard-beta/answerImages/Q8_Savings-27.png",
          "weighting": [0.03, 0.02, 0.06, 0.05, 0.04, 3.07, 1.08, 0.01],
          "endState": true
        }
      ]
    }
  ]
}
