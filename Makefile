#This file defines the common targets across our Node.js services
#note MAKEFILE_SUDO_COMMAND will be sudo -n when run from jenkinsfile
#you shouldnt need to set it on a mac for example

NODE_ENV ?= development
TAG = $$(git log -1 --pretty=%h --abbrev=10)
DOCKER_COMPOSE ?= docker-compose
DOCKER_COMPOSE_RUN_DEV_BUILD = ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} run --rm dev-build
DOCKER_COMPOSE_BUILD = ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} build
DOCKER_COMPOSE_DOWN = ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} down
PRODUCT_KEY?=investment

all: clean-up install lint unit-test build component-test

install:
	${DOCKER_COMPOSE_BUILD} dev-build
.PHONY: install

lint:
	${DOCKER_COMPOSE_RUN_DEV_BUILD} lint
.PHONY: lint

dependency-check:
	# ${DOCKER_COMPOSE_RUN_DEV_BUILD} dependency-check
.PHONY: lint

unit-test:
	${DOCKER_COMPOSE_RUN_DEV_BUILD} test:unit
.PHONY: unit-test

build:
ifeq (${PUSH_CDN}, true)
	@${DOCKER_COMPOSE_BUILD} --build-arg PUSH_CDN=true --build-arg AWS_ACCESS_KEY_ID=${CDN_AWS_ACCESS_KEY_ID} --build-arg AWS_SECRET_ACCESS_KEY=${CDN_AWS_SECRET_ACCESS_KEY} prod-build
else
	@${DOCKER_COMPOSE_BUILD} prod-build
endif
.PHONY: build

component-test: build
	${DOCKER_COMPOSE_DOWN} && ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} -f docker-compose.yml -f docker-compose-ct.yml run --rm dev-build
	${DOCKER_COMPOSE_DOWN} && ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} -f docker-compose.yml -f docker-compose-ct-money-wizard-beta.yml run --rm dev-build
	#${DOCKER_COMPOSE_DOWN} && ${MAKEFILE_SUDO_COMMAND} COMPOSE_TAG=${TAG} ${DOCKER_COMPOSE} -f docker-compose.yml -f docker-compose-ct-ui.yml run --rm dev-build
.PHONY: component-test

component-test-ui-local:
	killall -9 node || true
	yarn build
	yarn start-investment
	yarn test:component:ui
.PHONY: component-test-ui-local

release:
	@npm version patch
.PHONY: release

clean-up:
	${DOCKER_COMPOSE_DOWN} -v --remove-orphans || true
.PHONY: clean-up

dev-local:
	killall -9 node || true
	rm -rf dist
	nohup yarn test:stubs -- 3001 &
	nohup ./node_modules/nodemon/bin/nodemon.js --legacy-watch --watch server &
	NODE_ENV=development ./node_modules/.bin/webpack --config webpack.config.js --hide-modules --bail --devtool eval
.PHONY: dev-local

# Browserstack build
dev-local-bs:
	killall -9 node || true
	rm -rf dist
	yarn build
	nohup yarn test:stubs -- 3001 &
	yarn start
.PHONY: dev-local-bs

prod:
	killall -9 node || true
	rm -rf dist
	yarn build
	yarn start
.PHONY: prod
