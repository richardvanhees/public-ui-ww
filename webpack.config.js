require('dotenv').config({ silent: true });

// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin;

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const DynamicCdnWebpackPlugin = require('dynamic-cdn-webpack-plugin');
const S3Plugin = require('webpack-s3-plugin');
const sourceMap = process.env.NODE_ENV === 'development';
const packageJson = require(`${process.cwd()}${path.sep}package.json`);

module.exports = {
  mode: process.env.NODE_ENV,
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        exclude: [/\.min\.js$/gi],
        sourceMap,
        uglifyOptions: {
          compress: {
            pure_getters: true,
          },
        },
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
        vendor: {
          chunks: 'all',
          name: 'vendor',
          enforce: true,
          test(module) {
            const regex = /\/node_modules\//;

            if (
              module.context &&
              module.context.indexOf('react-d3-components') > -1
            ) {
              return false;
            }

            if (
              module.nameForCondition &&
              regex.test(module.nameForCondition())
            ) {
              return true;
            }
            for (const chunk of module.chunksIterable) {
              if (chunk.name && regex.test(chunk.name)) {
                return true;
              }
            }
            return false;
          },
        },
      },
    },
  },
  watch: process.env.NODE_ENV === 'development',
  watchOptions: {
    ignored: /node_modules/,
    poll: 1000,
  },
  entry: {
    'money-wizard': [path.join(__dirname, 'src/money-wizard/index.js')],
    investment: [path.join(__dirname, 'src/investment/index.js')],
    'money-wizard-beta': [path.join(__dirname, 'src/money-wizard-beta/index.js')],
    // do not remove babel polyfill, it is required for IE 11
    vendor: ['babel-polyfill'],
  },
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: 'assets/[name].[hash].js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Money Wizard',
      filename: 'money-wizard.html',
      template: 'src/money-wizard/index.html',
      chunks: ['vendor', 'money-wizard'],
    }),
    new HtmlWebpackPlugin({
      title: 'Investment Wizard',
      filename: 'investment.html',
      template: 'src/investment/index.html',
      chunks: ['vendor', 'investment'],
    }),
    new HtmlWebpackPlugin({
      title: 'Money Wizard Beta',
      filename: 'money-wizard-beta.html',
      template: 'src/money-wizard-beta/index.html',
      chunks: ['vendor', 'money-wizard-beta'],
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
    new webpack.ProvidePlugin({
      Promise:
        'imports-loader?this=>global!exports-loader?global.Promise!es6-promise',
    }),
    new MiniCssExtractPlugin({
      filename: 'assets/css/[name]-[hash].css',
      chunkFilename: 'assets/css/[name]-[id]-[hash].css',
    }),
    function () {
      this.hooks.watchRun.tapAsync('rebuild-watcher', (compiler, callback) => {
        console.log(`Built: ${new Date()}`);
        callback();
      });
    },
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    // this pulls out the node modules that can be fetches from unpkg
    new DynamicCdnWebpackPlugin(),
    //new BundleAnalyzerPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              minimize: sourceMap,
              sourceMap,
            },
          },
          {
            loader: 'resolve-url-loader',
            options: {
              sourceMap,
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              minimize: sourceMap,
              sourceMap,
            },
          },
          {
            loader: 'resolve-url-loader',
            options: {
              sourceMap,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              implementation: require('dart-sass'),
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|woff2?(\?v=[0-9]\.[0-9]\.[0-9])?)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]',
              outputPath: 'assets/fonts/',
              useRelativePath: true,
            },
          },
        ],
      },
      {
        test: /\.(jpg|png|svg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[hash].[ext]',
              outputPath: 'assets/img/',
            },
          },
        ],
      },
      {
        test: /\.pdf/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash].[ext]',
          outputPath: 'assets/files/',
          mimetype: 'application/pdf',
        },
      },
    ],
  },
  resolve: {
    modules: ['node_modules'],
    extensions: ['*', '.js', '.jsx', 'json'],
  },
  externals: {
    wealthwizards: 'WealthWizards',
  },
};

if (process.env.PUSH_CDN === 'true') {
  /* Build and deploy for CDN */
  module.exports.output.publicPath = `https://cdn.wealthwizards.io/assets/${
    packageJson.name
  }/${packageJson.version}/`;
  module.exports.plugins.push(
    new S3Plugin({
      basePath: `assets/${packageJson.name}/${packageJson.version}/assets`,
      directory: path.resolve('dist/assets'),
      s3Options: {
        accessKeyId: `${process.env.AWS_ACCESS_KEY_ID}`,
        secretAccessKey: `${process.env.AWS_SECRET_ACCESS_KEY}`,
        region: 'eu-west-1',
      },
      s3UploadOptions: {
        Bucket: 'cdn-wealthwizards-293630037826',
      },
    })
  );
}
